.class public final Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private channel_:Ljava/lang/Object;

.field private from_:Ljava/lang/Object;

.field private payload_:Lcom/google/protobuf/ByteString;

.field private sendOnDisconnect_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method private buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->access$6402(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->access$6502(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->payload_:Lcom/google/protobuf/ByteString;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->access$6602(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->sendOnDisconnect_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->sendOnDisconnect_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->access$6702(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Z)Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->access$6802(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;I)I

    return-object v1
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v2

    if-eq v1, v2, :cond_3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->hasChannel()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getChannel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->setChannel(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->hasFrom()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getFrom()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->setFrom(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    :cond_1
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->hasPayload()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getPayload()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->setPayload(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->hasSendOnDisconnect()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getSendOnDisconnect()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->setSendOnDisconnect(Z)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    :cond_3
    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_4
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->sendOnDisconnect_:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->sendOnDisconnect_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearChannel()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getChannel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearFrom()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getFrom()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearPayload()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getPayload()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final clearSendOnDisconnect()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->sendOnDisconnect_:Z

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getChannel()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public final getFrom()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getPayload()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getSendOnDisconnect()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->sendOnDisconnect_:Z

    return v0
.end method

.method public final hasChannel()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFrom()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSendOnDisconnect()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setChannel(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->channel_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setFrom(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->from_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setPayload(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->payload_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final setSendOnDisconnect(Z)Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;->sendOnDisconnect_:Z

    return-object p0
.end method
