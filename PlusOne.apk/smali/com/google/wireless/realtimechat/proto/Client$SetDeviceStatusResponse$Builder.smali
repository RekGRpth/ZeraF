.class public final Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;",
        "Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponseOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

.field private timestamp_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-void
.end method

.method private buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->access$44302(Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->timestamp_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->timestamp_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->access$44402(Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;J)J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->access$44502(Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;I)I

    return-object v1
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 3

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    move-result-object v2

    if-eq v1, v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->hasStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->hasTimestamp()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->getTimestamp()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->setTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    :cond_1
    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    goto :goto_0

    :sswitch_2
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->timestamp_:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->timestamp_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearStatus()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final clearTimestamp()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->timestamp_:J

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->timestamp_:J

    return-wide v0
.end method

.method public final hasStatus()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTimestamp()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final setTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SetDeviceStatusResponse$Builder;->timestamp_:J

    return-object p0
.end method
