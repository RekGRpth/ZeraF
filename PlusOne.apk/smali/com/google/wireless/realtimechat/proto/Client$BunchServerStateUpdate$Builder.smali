.class public final Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdateOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdateOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

.field private eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

.field private groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

.field private invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

.field private membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

.field private migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

.field private newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

.field private presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

.field private receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

.field private tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

.field private typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    return-void
.end method

.method static synthetic access$78900()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 5

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v2

    if-eq v1, v2, :cond_a

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasChatMessage()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v4

    if-eq v3, v4, :cond_b

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    :goto_0
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasPresence()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getPresence()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_c

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v4

    if-eq v3, v4, :cond_c

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    :goto_1
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasTyping()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getTyping()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_d

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v4

    if-eq v3, v4, :cond_d

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    :goto_2
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_2
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasReceipt()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_e

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v4

    if-eq v3, v4, :cond_e

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    :goto_3
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_3
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasMembershipChange()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_f

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v4

    if-eq v3, v4, :cond_f

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    :goto_4
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_4
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasGroupConversationRename()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_10

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v4

    if-eq v3, v4, :cond_10

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    :goto_5
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_5
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasTileEvent()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_11

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v4

    if-eq v3, v4, :cond_11

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    :goto_6
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_6
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasMigration()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_12

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v4

    if-eq v3, v4, :cond_12

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    :goto_7
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_7
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasInvalidateLocalCache()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getInvalidateLocalCache()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_13

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v4

    if-eq v3, v4, :cond_13

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    :goto_8
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x100

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_8
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasNewConversation()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getNewConversation()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_14

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v4

    if-eq v3, v4, :cond_14

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$NewConversation;)Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$NewConversation;)Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    :goto_9
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_9
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasEventMetadata()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v1

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_15

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v3

    if-eq v2, v3, :cond_15

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    invoke-static {v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    :goto_a
    iget v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    :cond_a
    return-object v0

    :cond_b
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    goto/16 :goto_0

    :cond_c
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    goto/16 :goto_1

    :cond_d
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    goto/16 :goto_2

    :cond_e
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    goto/16 :goto_3

    :cond_f
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    goto/16 :goto_4

    :cond_10
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    goto/16 :goto_5

    :cond_11
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    goto/16 :goto_6

    :cond_12
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    goto/16 :goto_7

    :cond_13
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    goto/16 :goto_8

    :cond_14
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    goto :goto_9

    :cond_15
    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    goto :goto_a
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasChatMessage()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setChatMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasPresence()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getPresence()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setPresence(Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasTyping()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getTyping()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setTyping(Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasReceipt()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    :cond_4
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setReceipt(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasMembershipChange()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    :cond_5
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setMembershipChange(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasGroupConversationRename()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    :cond_6
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setGroupConversationRename(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasTileEvent()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    :cond_7
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setTileEvent(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    :sswitch_8
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasMigration()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    :cond_8
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setMigration(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    :sswitch_9
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasInvalidateLocalCache()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getInvalidateLocalCache()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    :cond_9
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setInvalidateLocalCache(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    :sswitch_a
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasNewConversation()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getNewConversation()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$NewConversation;)Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;

    :cond_a
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setNewConversation(Lcom/google/wireless/realtimechat/proto/Client$NewConversation;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    :sswitch_b
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->hasEventMetadata()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    :cond_b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->setEventMetadata(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79102(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79202(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$Presence;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79302(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$Typing;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79402(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79502(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79602(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79702(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79802(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration;

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$79902(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x200

    :cond_9
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$80002(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$NewConversation;)Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    and-int/lit16 v3, v0, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x400

    :cond_a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$80102(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->access$80202(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearChatMessage()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearInvalidateLocalCache()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearMigration()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearNewConversation()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearPresence()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearReceipt()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearTileEvent()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearTyping()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v0

    return-object v0
.end method

.method public final getEventMetadata()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    return-object v0
.end method

.method public final getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    return-object v0
.end method

.method public final getInvalidateLocalCache()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    return-object v0
.end method

.method public final getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    return-object v0
.end method

.method public final getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    return-object v0
.end method

.method public final getNewConversation()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    return-object v0
.end method

.method public final getPresence()Lcom/google/wireless/realtimechat/proto/Client$Presence;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    return-object v0
.end method

.method public final getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    return-object v0
.end method

.method public final getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    return-object v0
.end method

.method public final getTyping()Lcom/google/wireless/realtimechat/proto/Client$Typing;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    return-object v0
.end method

.method public final hasChatMessage()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEventMetadata()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGroupConversationRename()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInvalidateLocalCache()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMembershipChange()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMigration()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNewConversation()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPresence()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceipt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTileEvent()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTyping()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setChatMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setChatMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setEventMetadata(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setEventMetadata(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->eventMetadata_:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setGroupConversationRename(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setGroupConversationRename(Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setInvalidateLocalCache(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setInvalidateLocalCache(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMembershipChange(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMembershipChange(Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMigration(Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Migration$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMigration(Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Migration;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setNewConversation(Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$NewConversation$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setNewConversation(Lcom/google/wireless/realtimechat/proto/Client$NewConversation;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->newConversation_:Lcom/google/wireless/realtimechat/proto/Client$NewConversation;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setPresence(Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setPresence(Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Presence;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setReceipt(Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setReceipt(Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTileEvent(Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTileEvent(Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTyping(Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTyping(Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Typing;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->bitField0_:I

    return-object p0
.end method
