.class public final Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NewConversationResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;,
        Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private clientConversation_:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

.field private conversationClientId_:Ljava/lang/Object;

.field private inviteType_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private participantError_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;",
            ">;"
        }
    .end annotation
.end field

.field private receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

.field private recentMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

.field private recipientId_:Lcom/google/protobuf/LazyStringList;

.field private senderId_:Ljava/lang/Object;

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->conversationClientId_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->clientConversation_:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->senderId_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->inviteType_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recentMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;)V

    return-void
.end method

.method static synthetic access$23702(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p1
.end method

.method static synthetic access$23802(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->conversationClientId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$23902(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->clientConversation_:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    return-object p1
.end method

.method static synthetic access$24002(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->senderId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$24100(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$24102(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$24202(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    return-object p1
.end method

.method static synthetic access$24302(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->inviteType_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    return-object p1
.end method

.method static synthetic access$24402(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recentMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object p1
.end method

.method static synthetic access$24500(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$24502(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$24602(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    return p1
.end method

.method private getConversationClientIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->conversationClientId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->conversationClientId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    return-object v0
.end method

.method private getSenderIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->senderId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->senderId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->access$23500()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->access$23500()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getClientConversation()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->clientConversation_:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    return-object v0
.end method

.method public final getConversationClientId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->conversationClientId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->conversationClientId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    return-object v0
.end method

.method public final getInviteType()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->inviteType_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    return-object v0
.end method

.method public final getParticipantError(I)Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    return-object v0
.end method

.method public final getParticipantErrorCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantErrorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    return-object v0
.end method

.method public final getParticipantErrorOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Client$ParticipantErrorOrBuilder;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$ParticipantErrorOrBuilder;

    return-object v0
.end method

.method public final getParticipantErrorOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$ParticipantErrorOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    return-object v0
.end method

.method public final getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    return-object v0
.end method

.method public final getRecentMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recentMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object v0
.end method

.method public final getRecipientId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getRecipientIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getRecipientIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getSenderId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->senderId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->senderId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x1

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->getNumber()I

    move-result v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/lit8 v2, v4, 0x0

    :cond_1
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    const/4 v4, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getConversationClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v6, :cond_3

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->clientConversation_:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    invoke-static {v6, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-ne v4, v7, :cond_4

    const/4 v4, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getSenderIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getRecipientIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v7, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_7

    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_7
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_8

    const/16 v4, 0xa

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->inviteType_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_8
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_9

    const/16 v4, 0xb

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recentMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_9
    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final hasClientConversation()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationClientId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviteType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceipt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasRecentMessage()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSenderId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStatus()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getSerializedSize()I

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getConversationClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->clientConversation_:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v4, :cond_3

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getSenderIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->participantError_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_6

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->inviteType_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse$InviteType;->getNumber()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_7
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_8

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->recentMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_8
    return-void
.end method
