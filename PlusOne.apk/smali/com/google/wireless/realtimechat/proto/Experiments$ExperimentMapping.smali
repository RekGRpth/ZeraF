.class public final Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Experiments.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMappingOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Experiments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ExperimentMapping"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private disabledAllUser_:Z

.field private disabledDomain_:Lcom/google/protobuf/LazyStringList;

.field private disabledEmail_:Lcom/google/protobuf/LazyStringList;

.field private disabledId_:Lcom/google/protobuf/LazyStringList;

.field private disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

.field private disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

.field private disabledRegexId_:Lcom/google/protobuf/LazyStringList;

.field private enabledAllUser_:Z

.field private enabledDomain_:Lcom/google/protobuf/LazyStringList;

.field private enabledEmail_:Lcom/google/protobuf/LazyStringList;

.field private enabledId_:Lcom/google/protobuf/LazyStringList;

.field private enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

.field private enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

.field private enabledRegexId_:Lcom/google/protobuf/LazyStringList;

.field private experiment_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledAllUser_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledAllUser_:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;-><init>(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledAllUser_:Z

    return p1
.end method

.method static synthetic access$1702(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledAllUser_:Z

    return p1
.end method

.method static synthetic access$1802(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$900(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->access$100()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    return-object v0
.end method

.method public final getDisabledAllUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledAllUser_:Z

    return v0
.end method

.method public final getDisabledDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getDisabledEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getDisabledId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getDisabledRegexDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledRegexDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledRegexDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getDisabledRegexEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledRegexEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledRegexEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getDisabledRegexId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledRegexIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledRegexIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getEnabledAllUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledAllUser_:Z

    return v0
.end method

.method public final getEnabledDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getEnabledEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getEnabledId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getEnabledRegexDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledRegexDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledRegexDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getEnabledRegexEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledRegexEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledRegexEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getEnabledRegexId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledRegexIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledRegexIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getExperiment(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getExperimentCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getExperimentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 6

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v0, 0x0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getExperimentList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_4
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledRegexIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_5
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledRegexIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_6
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledEmailList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_7
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_7

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_7
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledEmailList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_8
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_8

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_8
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledRegexEmailList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_9
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_9

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    :cond_9
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledRegexEmailList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_a
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_a

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_a
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledDomainList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_b
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_b

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_b
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledDomainList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_c
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_c

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    :cond_c
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledRegexDomainList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_d
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_d

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    :cond_d
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledRegexDomainList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    const/16 v4, 0xe

    iget-boolean v5, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledAllUser_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_e
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_f

    const/16 v4, 0xf

    iget-boolean v5, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledAllUser_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_f
    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public final hasDisabledAllUser()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEnabledAllUser()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getSerializedSize()I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_3
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_4
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    :goto_6
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    :goto_7
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    const/4 v0, 0x0

    :goto_8
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_8
    const/4 v0, 0x0

    :goto_9
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_9
    const/4 v0, 0x0

    :goto_a
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_a
    const/4 v0, 0x0

    :goto_b
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_b
    const/4 v0, 0x0

    :goto_c
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_c

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_c
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v3, :cond_d

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledAllUser_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_d
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_e

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledAllUser_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_e
    return-void
.end method
