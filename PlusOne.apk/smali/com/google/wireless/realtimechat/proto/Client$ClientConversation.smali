.class public final Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$ClientConversationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientConversation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private conversationClientId_:Ljava/lang/Object;

.field private conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

.field private createdAt_:J

.field private firstEventTimestamp_:J

.field private hidden_:Z

.field private id_:Ljava/lang/Object;

.field private inactiveParticipant_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

.field private lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private muted_:Z

.field private name_:Ljava/lang/Object;

.field private needsAccept_:Z

.field private offTheRecord_:Z

.field private participantId_:Lcom/google/protobuf/LazyStringList;

.field private participant_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

.field private unreadCount_:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->offTheRecord_:Z

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->muted_:Z

    iput-wide v3, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->unreadCount_:J

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->needsAccept_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    iput-wide v3, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->createdAt_:J

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hidden_:Z

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    iput-wide v3, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->firstEventTimestamp_:J

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;-><init>(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;)V

    return-void
.end method

.method static synthetic access$3502(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Data$ConversationType;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    return-object p1
.end method

.method static synthetic access$3702(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->offTheRecord_:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$3802(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$3902(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->muted_:Z

    return p1
.end method

.method static synthetic access$4102(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->unreadCount_:J

    return-wide p1
.end method

.method static synthetic access$4200(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4302(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->needsAccept_:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4502(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->createdAt_:J

    return-wide p1
.end method

.method static synthetic access$4602(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hidden_:Z

    return p1
.end method

.method static synthetic access$4702(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object p1
.end method

.method static synthetic access$4802(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4902(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    return-object p1
.end method

.method static synthetic access$5002(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->firstEventTimestamp_:J

    return-wide p1
.end method

.method static synthetic access$5202(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object p1
.end method

.method static synthetic access$5302(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    return p1
.end method

.method private getConversationClientIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    return-object v0
.end method

.method private getIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getNameBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->access$3300()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->access$3300()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getConversationClientId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getConversationMetadata()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    return-object v0
.end method

.method public final getCreatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->createdAt_:J

    return-wide v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    return-object v0
.end method

.method public final getFirstEventTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->firstEventTimestamp_:J

    return-wide v0
.end method

.method public final getHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hidden_:Z

    return v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getInactiveParticipant(I)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getInactiveParticipantCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getInactiveParticipantList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    return-object v0
.end method

.method public final getInactiveParticipantOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;

    return-object v0
.end method

.method public final getInactiveParticipantOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    return-object v0
.end method

.method public final getInviter()Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getLastMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object v0
.end method

.method public final getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    return-object v0
.end method

.method public final getMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->muted_:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getNeedsAccept()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->needsAccept_:Z

    return v0
.end method

.method public final getOffTheRecord()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->offTheRecord_:Z

    return v0
.end method

.method public final getParticipant(I)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getParticipantCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getParticipantIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getParticipantList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    return-object v0
.end method

.method public final getParticipantOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;

    return-object v0
.end method

.method public final getParticipantOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Data$ParticipantOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 10

    const/16 v9, 0x10

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/lit8 v2, v4, 0x0

    :cond_1
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v6, :cond_2

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->getNumber()I

    move-result v4

    invoke-static {v6, v4}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v7, :cond_3

    iget-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->offTheRecord_:Z

    invoke-static {v7, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getParticipantIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-ne v4, v8, :cond_5

    const/4 v4, 0x6

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_5
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_6

    const/4 v4, 0x7

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_6
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-ne v4, v9, :cond_7

    iget-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->muted_:Z

    invoke-static {v8, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_7
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_8

    const/16 v4, 0x9

    iget-wide v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->unreadCount_:J

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_8
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_9

    const/16 v5, 0xa

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_9
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_a

    const/16 v4, 0xb

    iget-boolean v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->needsAccept_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_a
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_b

    const/16 v4, 0xc

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_b
    const/4 v1, 0x0

    :goto_3
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_c

    const/16 v5, 0xd

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_c
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_d

    const/16 v4, 0xe

    iget-wide v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->createdAt_:J

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_d
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_e

    const/16 v4, 0xf

    iget-boolean v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hidden_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_e
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_f

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v9, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_f
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_10

    const/16 v4, 0x11

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_10
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_11

    const/16 v4, 0x12

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_11
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_12

    const/16 v4, 0x13

    iget-wide v5, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->firstEventTimestamp_:J

    invoke-static {v4, v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_12
    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Data$ConversationType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    return-object v0
.end method

.method public final getUnreadCount()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->unreadCount_:J

    return-wide v0
.end method

.method public final hasConversationClientId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationMetadata()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCreatedAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFirstEventTimestamp()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHidden()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviter()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLastMessage()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLastPreviewEvent()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMuted()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasName()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNeedsAccept()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasOffTheRecord()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUnreadCount()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 7
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getSerializedSize()I

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_2

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->offTheRecord_:Z

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_5

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v6, :cond_6

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->muted_:Z

    invoke-virtual {p1, v5, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_6
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_7

    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->unreadCount_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_7
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_8

    const/16 v2, 0xa

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_9

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->needsAccept_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_9
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_a
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_b

    const/16 v2, 0xd

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_b
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_c

    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->createdAt_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_c
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_d

    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hidden_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_d
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_e

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v6, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_e
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_f

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_f
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_10

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_10
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_11

    const/16 v1, 0x13

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->firstEventTimestamp_:J

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_11
    return-void
.end method
