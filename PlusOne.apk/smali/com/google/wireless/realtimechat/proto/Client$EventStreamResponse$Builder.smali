.class public final Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;",
        "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponseOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private earliest_:J

.field private event_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;",
            ">;"
        }
    .end annotation
.end field

.field private latest_:J

.field private more_:Z

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-void
.end method

.method static synthetic access$36500()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureEventIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->addEvent(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    goto :goto_0

    :sswitch_3
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->more_:Z

    goto :goto_0

    :sswitch_4
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->earliest_:J

    goto :goto_0

    :sswitch_5
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->latest_:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method


# virtual methods
.method public final addAllEvent(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addEvent(ILcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addEvent(ILcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addEvent(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEvent(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->conversationId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$36702(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->event_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$36802(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x2

    :cond_2
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->more_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->more_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$36902(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;Z)Z

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x4

    :cond_3
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->earliest_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->earliest_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$37002(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;J)J

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x8

    :cond_4
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->latest_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->latest_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$37102(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;J)J

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x10

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$37202(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$37302(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 3

    const-wide/16 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->more_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->earliest_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->latest_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearConversationId()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearEarliest()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->earliest_:J

    return-object p0
.end method

.method public final clearEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearLatest()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->latest_:J

    return-object p0
.end method

.method public final clearMore()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->more_:Z

    return-object p0
.end method

.method public final clearStatus()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getConversationId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getEarliest()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->earliest_:J

    return-wide v0
.end method

.method public final getEvent(I)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    return-object v0
.end method

.method public final getEventCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getLatest()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->latest_:J

    return-wide v0
.end method

.method public final getMore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->more_:Z

    return v0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final hasConversationId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEarliest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLatest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMore()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStatus()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->hasConversationId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->setConversationId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    :cond_2
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->event_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$36800(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->event_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$36800(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getMore()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->setMore(Z)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->hasEarliest()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getEarliest()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->setEarliest(J)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->hasLatest()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getLatest()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->setLatest(J)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->event_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->access$36800(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public final setConversationId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setEarliest(J)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->earliest_:J

    return-object p0
.end method

.method public final setEvent(ILcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEvent(ILcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->ensureEventIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->event_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setLatest(J)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->latest_:J

    return-object p0
.end method

.method public final setMore(Z)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->more_:Z

    return-object p0
.end method

.method public final setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method
