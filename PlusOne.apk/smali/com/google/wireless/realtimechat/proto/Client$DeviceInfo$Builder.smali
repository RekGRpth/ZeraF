.class public final Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$DeviceInfoOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;",
        "Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$DeviceInfoOrBuilder;"
    }
.end annotation


# instance fields
.field private androidId_:J

.field private appId_:Ljava/lang/Object;

.field private bitField0_:I

.field private clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private createdAt_:J

.field private enabled_:Z

.field private markedForDeletionAt_:J

.field private phoneNumber_:Ljava/lang/Object;

.field private pushEnabled_:Z

.field private token_:Lcom/google/protobuf/ByteString;

.field private type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

.field private updatedAt_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-void
.end method

.method static synthetic access$52400()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 4

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v2

    if-eq v1, v2, :cond_a

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasType()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setEnabled(Z)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_1
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasPushEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getPushEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setPushEnabled(Z)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasToken()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getToken()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setToken(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_3
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasPhoneNumber()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setPhoneNumber(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_4
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasCreatedAt()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getCreatedAt()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setCreatedAt(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_5
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasUpdatedAt()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getUpdatedAt()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setUpdatedAt(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_6
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasMarkedForDeletionAt()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getMarkedForDeletionAt()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setMarkedForDeletionAt(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_7
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasAppId()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getAppId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setAppId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_8
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasAndroidId()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getAndroidId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setAndroidId(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    :cond_9
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->hasClientVersion()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getClientVersion()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v1

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_b

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v3

    if-eq v2, v3, :cond_b

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {v2}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    :goto_0
    iget v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    :cond_a
    return-object v0

    :cond_b
    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    goto :goto_0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    goto :goto_0

    :sswitch_2
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->enabled_:Z

    goto :goto_0

    :sswitch_3
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->pushEnabled_:Z

    goto :goto_0

    :sswitch_4
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_5
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->createdAt_:J

    goto :goto_0

    :sswitch_7
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->updatedAt_:J

    goto :goto_0

    :sswitch_8
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->markedForDeletionAt_:J

    goto :goto_0

    :sswitch_9
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->androidId_:J

    goto/16 :goto_0

    :sswitch_b
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->hasClientVersion()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->getClientVersion()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->setClientVersion(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;-><init>(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$52602(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->enabled_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->enabled_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$52702(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Z)Z

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->pushEnabled_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->pushEnabled_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$52802(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Z)Z

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->token_:Lcom/google/protobuf/ByteString;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$52902(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53002(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->createdAt_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->createdAt_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53102(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->updatedAt_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->updatedAt_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53202(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->markedForDeletionAt_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->markedForDeletionAt_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53302(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53402(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x200

    :cond_9
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->androidId_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->androidId_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53502(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J

    and-int/lit16 v3, v0, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x400

    :cond_a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53602(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->access$53702(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 4

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->enabled_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->pushEnabled_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->createdAt_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->updatedAt_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->markedForDeletionAt_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->androidId_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearAndroidId()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->androidId_:J

    return-object p0
.end method

.method public final clearAppId()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getAppId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearClientVersion()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearCreatedAt()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->createdAt_:J

    return-object p0
.end method

.method public final clearEnabled()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->enabled_:Z

    return-object p0
.end method

.method public final clearMarkedForDeletionAt()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->markedForDeletionAt_:J

    return-object p0
.end method

.method public final clearPhoneNumber()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearPushEnabled()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->pushEnabled_:Z

    return-object p0
.end method

.method public final clearToken()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getToken()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final clearType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    return-object p0
.end method

.method public final clearUpdatedAt()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->updatedAt_:J

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAndroidId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->androidId_:J

    return-wide v0
.end method

.method public final getAppId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getClientVersion()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method public final getCreatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->createdAt_:J

    return-wide v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->enabled_:Z

    return v0
.end method

.method public final getMarkedForDeletionAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->markedForDeletionAt_:J

    return-wide v0
.end method

.method public final getPhoneNumber()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getPushEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->pushEnabled_:Z

    return v0
.end method

.method public final getToken()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    return-object v0
.end method

.method public final getUpdatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->updatedAt_:J

    return-wide v0
.end method

.method public final hasAndroidId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAppId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasClientVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCreatedAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEnabled()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMarkedForDeletionAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhoneNumber()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPushEnabled()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasToken()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUpdatedAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setAndroidId(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->androidId_:J

    return-object p0
.end method

.method public final setAppId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->appId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setClientVersion(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->build()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setClientVersion(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setCreatedAt(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->createdAt_:J

    return-object p0
.end method

.method public final setEnabled(Z)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->enabled_:Z

    return-object p0
.end method

.method public final setMarkedForDeletionAt(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->markedForDeletionAt_:J

    return-object p0
.end method

.method public final setPhoneNumber(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->phoneNumber_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setPushEnabled(Z)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->pushEnabled_:Z

    return-object p0
.end method

.method public final setToken(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->token_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final setType(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    return-object p0
.end method

.method public final setUpdatedAt(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->updatedAt_:J

    return-object p0
.end method
