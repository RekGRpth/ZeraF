.class public final enum Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;
.super Ljava/lang/Enum;
.source "Buzz.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ApplicationError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum ACCESS_DENIED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum BACKEND_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum BUZZ_HEADER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum COMPRESSION_TYPE_NOT_SUPPORTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum EXPECTED_RECIPIENT_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum EXPECTED_SENDER_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum NON_EXISTENT_RECIPIENT:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum NOT_IMPLEMENTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum PREPICKED_SERVER_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum RECIPIENT_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum RECIPIENT_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum RECIPIENT_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum SENDER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum SENDER_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum SENDER_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum SERVER_OVERLOADED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum STANZA_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum TASK_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum UNEXPECTED_RECIPIENT_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field public static final enum UNEXPECTED_SENDER_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "EXPECTED_RECIPIENT_TYPE_NOT_FOUND"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->EXPECTED_RECIPIENT_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "UNEXPECTED_RECIPIENT_TYPE_FOUND"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->UNEXPECTED_RECIPIENT_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "EXPECTED_SENDER_TYPE_NOT_FOUND"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->EXPECTED_SENDER_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "UNEXPECTED_SENDER_TYPE_FOUND"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->UNEXPECTED_SENDER_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "RECIPIENT_ERROR"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "SENDER_ERROR"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "RECIPIENT_JID_ERROR"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "RECIPIENT_GATEWAY_ERROR"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "SENDER_JID_ERROR"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "SENDER_GATEWAY_ERROR"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "BUZZ_HEADER_ERROR"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->BUZZ_HEADER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "BACKEND_ERROR"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->BACKEND_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "NON_EXISTENT_RECIPIENT"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->NON_EXISTENT_RECIPIENT:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "COMPRESSION_TYPE_NOT_SUPPORTED"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->COMPRESSION_TYPE_NOT_SUPPORTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "STANZA_ERROR"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->STANZA_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "ACCESS_DENIED"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->ACCESS_DENIED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "TASK_NOT_REACHABLE"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->TASK_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "PREPICKED_SERVER_NOT_REACHABLE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->PREPICKED_SERVER_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "NOT_IMPLEMENTED"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->NOT_IMPLEMENTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const-string v1, "SERVER_OVERLOADED"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SERVER_OVERLOADED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->EXPECTED_RECIPIENT_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->UNEXPECTED_RECIPIENT_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->EXPECTED_SENDER_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->UNEXPECTED_SENDER_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->BUZZ_HEADER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->BACKEND_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->NON_EXISTENT_RECIPIENT:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->COMPRESSION_TYPE_NOT_SUPPORTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->STANZA_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->ACCESS_DENIED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->TASK_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->PREPICKED_SERVER_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->NOT_IMPLEMENTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SERVER_OVERLOADED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->$VALUES:[Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError$1;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError$1;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->EXPECTED_RECIPIENT_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->UNEXPECTED_RECIPIENT_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->EXPECTED_SENDER_TYPE_NOT_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->UNEXPECTED_SENDER_TYPE_FOUND:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->RECIPIENT_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_JID_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SENDER_GATEWAY_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_a
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->BUZZ_HEADER_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_b
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->BACKEND_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_c
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->NON_EXISTENT_RECIPIENT:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_d
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->COMPRESSION_TYPE_NOT_SUPPORTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_e
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->STANZA_ERROR:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_f
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->ACCESS_DENIED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_10
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->TASK_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_11
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->PREPICKED_SERVER_NOT_REACHABLE:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_12
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->NOT_IMPLEMENTED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    :pswitch_13
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->SERVER_OVERLOADED:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->$VALUES:[Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    invoke-virtual {v0}, [Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingResult$ApplicationError;->value:I

    return v0
.end method
