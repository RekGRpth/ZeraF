.class public final Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequestOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HangoutInviteReplyRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;,
        Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private instanceId_:J

.field private inviteId_:Ljava/lang/Object;

.field private inviteeStatus_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

.field private inviterId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private phoneNumber_:Ljava/lang/Object;

.field private stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteId_:Ljava/lang/Object;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteeStatus_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->phoneNumber_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviterId_:Ljava/lang/Object;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->instanceId_:J

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;-><init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;)V

    return-void
.end method

.method static synthetic access$61502(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61602(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteeStatus_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    return-object p1
.end method

.method static synthetic access$61702(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->phoneNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61802(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviterId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$61902(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->instanceId_:J

    return-wide p1
.end method

.method static synthetic access$62002(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object p1
.end method

.method static synthetic access$62102(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    return-object v0
.end method

.method private getInviteIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getInviterIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviterId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviterId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getPhoneNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->phoneNumber_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->phoneNumber_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;->access$61300()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;->access$61300()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    return-object v0
.end method

.method public final getInstanceId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->instanceId_:J

    return-wide v0
.end method

.method public final getInviteId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getInviteeStatus()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteeStatus_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    return-object v0
.end method

.method public final getInviterId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviterId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviterId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getPhoneNumber()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->phoneNumber_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->phoneNumber_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getInviteIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteeStatus_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;->getNumber()I

    move-result v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_5

    const/4 v2, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getInviterIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_6

    const/4 v2, 0x6

    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->instanceId_:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getStubbyInfo()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object v0
.end method

.method public final hasInstanceId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviteId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviteeStatus()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviterId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhoneNumber()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStubbyInfo()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getInviteIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->inviteeStatus_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest$InviteeStatus;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getInviterIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->instanceId_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_5
    return-void
.end method
