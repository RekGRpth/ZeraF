.class public final Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Data$TimeRangeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TimeRange"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private end_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private start_:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v1, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    iput-wide v1, v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->start_:J

    iput-wide v1, v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->end_:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;-><init>(Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;)V

    return-void
.end method

.method static synthetic access$4302(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->start_:J

    return-wide p1
.end method

.method static synthetic access$4402(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->end_:J

    return-wide p1
.end method

.method static synthetic access$4502(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->access$4100()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->access$4100()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    return-object v0
.end method

.method public final getEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->end_:J

    return-wide v0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->start_:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->end_:J

    invoke-static {v5, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getStart()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->start_:J

    return-wide v0
.end method

.method public final hasEnd()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStart()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->start_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->end_:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    return-void
.end method
