.class public final Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PushMessagePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private channel_:Ljava/lang/Object;

.field private from_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private payload_:Lcom/google/protobuf/ByteString;

.field private sendOnDisconnect_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->payload_:Lcom/google/protobuf/ByteString;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->sendOnDisconnect_:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload$Builder;)V

    return-void
.end method

.method static synthetic access$6402(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6502(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$6602(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .param p1    # Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->payload_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$6702(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->sendOnDisconnect_:Z

    return p1
.end method

.method static synthetic access$6802(Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    return p1
.end method

.method private getChannelBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    return-object v0
.end method

.method private getFromBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final getChannel()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->channel_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;

    return-object v0
.end method

.method public final getFrom()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->from_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getPayload()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->payload_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getSendOnDisconnect()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->sendOnDisconnect_:Z

    return v0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getChannelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->payload_:Lcom/google/protobuf/ByteString;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->sendOnDisconnect_:Z

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final hasChannel()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFrom()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSendOnDisconnect()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getChannelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->getFromBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->payload_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$PushMessagePayload;->sendOnDisconnect_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_3
    return-void
.end method
