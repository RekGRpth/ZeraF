.class public final Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HangoutInviteResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private inviteId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private requestError_:Ljava/lang/Object;

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->inviteId_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->requestError_:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;)V

    return-void
.end method

.method static synthetic access$59602(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p1
.end method

.method static synthetic access$59702(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->inviteId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59802(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->requestError_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59902(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    return-object v0
.end method

.method private getInviteIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->inviteId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->inviteId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getRequestErrorBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->requestError_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->requestError_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->access$59400()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->access$59400()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    return-object v0
.end method

.method public final getInviteId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->inviteId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->inviteId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getRequestError()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->requestError_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->requestError_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->getNumber()I

    move-result v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getInviteIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    const/4 v2, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getRequestErrorBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final hasInviteId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasRequestError()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStatus()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getInviteIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getRequestErrorBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    return-void
.end method
