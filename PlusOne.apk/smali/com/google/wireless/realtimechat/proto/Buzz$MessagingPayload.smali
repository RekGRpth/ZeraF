.class public final Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MessagingPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$Builder;,
        Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$PAYLOAD_TYPE;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private payloadType_:I

.field private payload_:Lcom/google/protobuf/ByteString;

.field private recipient_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;",
            ">;"
        }
    .end annotation
.end field

.field private sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payload_:Lcom/google/protobuf/ByteString;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payloadType_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload$Builder;)V

    return-void
.end method

.method static synthetic access$5500(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$5502(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$5602(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object p1
.end method

.method static synthetic access$5702(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .param p1    # Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payload_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$5802(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payloadType_:I

    return p1
.end method

.method static synthetic access$5902(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-object p1
.end method

.method static synthetic access$6002(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

    return-object v0
.end method


# virtual methods
.method public final getBuzzHeader()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;

    return-object v0
.end method

.method public final getPayload()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payload_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getPayloadType()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payloadType_:I

    return v0
.end method

.method public final getRecipient(I)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    return-object v0
.end method

.method public final getRecipientCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getRecipientList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    return-object v0
.end method

.method public final getRecipientOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientDataOrBuilder;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientDataOrBuilder;

    return-object v0
.end method

.method public final getRecipientOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientDataOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    return-object v0
.end method

.method public final getSender()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->memoizedSerializedSize:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    :goto_0
    return v2

    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {v5, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    :cond_2
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payload_:Lcom/google/protobuf/ByteString;

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v1, v3

    :cond_3
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_4

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payloadType_:I

    invoke-static {v6, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v1, v3

    :cond_4
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_5

    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    :cond_5
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->memoizedSerializedSize:I

    move v2, v1

    goto :goto_0
.end method

.method public final hasBuzzHeader()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPayloadType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSender()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->getSerializedSize()I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->recipient_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payload_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->payloadType_:I

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingPayload;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    return-void
.end method
