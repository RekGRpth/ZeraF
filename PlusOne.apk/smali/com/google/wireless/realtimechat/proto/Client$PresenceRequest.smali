.class public final Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$PresenceRequestOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PresenceRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private reciprocate_:Z

.field private stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

.field private type_:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->type_:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->conversationId_:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->reciprocate_:Z

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;-><init>(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;)V

    return-void
.end method

.method static synthetic access$8702(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;)Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->type_:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    return-object p1
.end method

.method static synthetic access$8802(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->conversationId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$8902(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->reciprocate_:Z

    return p1
.end method

.method static synthetic access$9002(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object p1
.end method

.method static synthetic access$9102(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    return p1
.end method

.method private getConversationIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->conversationId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;->access$8500()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;)Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;->access$8500()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;)Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getConversationId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->conversationId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->conversationId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    return-object v0
.end method

.method public final getReciprocate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->reciprocate_:Z

    return v0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->type_:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;->getNumber()I

    move-result v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->reciprocate_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getStubbyInfo()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object v0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->type_:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    return-object v0
.end method

.method public final hasConversationId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReciprocate()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStubbyInfo()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->type_:Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Presence$Type;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->reciprocate_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    return-void
.end method
