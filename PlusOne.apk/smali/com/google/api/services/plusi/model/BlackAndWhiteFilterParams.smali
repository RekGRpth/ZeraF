.class public final Lcom/google/api/services/plusi/model/BlackAndWhiteFilterParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "BlackAndWhiteFilterParams.java"


# instance fields
.field public brightness:Ljava/lang/Float;

.field public contrast:Ljava/lang/Float;

.field public grainSliderSoftness:Ljava/lang/Float;

.field public grainSliderStrength:Ljava/lang/Float;

.field public style:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
