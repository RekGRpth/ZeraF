.class public final Lcom/google/api/services/plusi/model/FinancialQuoteJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "FinancialQuoteJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/FinancialQuote;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/FinancialQuoteJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/FinancialQuoteJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FinancialQuoteJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/FinancialQuoteJson;->INSTANCE:Lcom/google/api/services/plusi/model/FinancialQuoteJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/FinancialQuote;

    const/16 v1, 0x17

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "afterHoursPrice"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "afterHoursPriceChange"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "afterHoursPriceChangePercent"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "afterHoursQuoteTime"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "dataSource"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "dataSourceDisclaimerUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "exchange"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "exchangeTimezone"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "imageUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "isAfterHours"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "isPreMarket"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "localizedAfterHoursQuoteTimestamp"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "localizedQuoteTimestamp"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "name"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "price"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "priceChange"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "priceChangeNumber"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "priceChangePercent"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "priceCurrency"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "quoteTime"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "tickerSymbol"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "url"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/FinancialQuoteJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/FinancialQuoteJson;->INSTANCE:Lcom/google/api/services/plusi/model/FinancialQuoteJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/FinancialQuote;

    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->afterHoursPrice:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->afterHoursPriceChange:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->afterHoursPriceChangePercent:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->afterHoursQuoteTime:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->dataSource:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->dataSourceDisclaimerUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->exchange:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->exchangeTimezone:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->imageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->isAfterHours:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->isPreMarket:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->localizedAfterHoursQuoteTimestamp:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->localizedQuoteTimestamp:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->price:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->priceChange:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->priceChangeNumber:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->priceChangePercent:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->priceCurrency:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->quoteTime:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->tickerSymbol:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FinancialQuote;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/FinancialQuote;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FinancialQuote;-><init>()V

    return-object v0
.end method
