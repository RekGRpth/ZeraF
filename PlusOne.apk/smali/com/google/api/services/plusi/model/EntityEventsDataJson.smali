.class public final Lcom/google/api/services/plusi/model/EntityEventsDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityEventsDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityEventsData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityEventsDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EntityEventsDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityEventsDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityEventsDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityEventsDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/EntityEventsData;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/EntityEventsDataAccessRequestJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "accessRequest"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/PlusEventJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "accessRequestPlusEvent"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/EntityEventsDataChangedFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "changedFields"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/PlusEventJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "deletedPlusEvent"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "eventActivityId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/InviteeSummaryJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "inviteeSummary"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/EntityEventsDataPeopleListJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "photoUploaders"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/PlusPhotoAlbumJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "plusPhotoAlbum"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plusi/model/EntityEventsDataRenderEventsDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "renderEventsData"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityEventsDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/EntityEventsDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityEventsDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/EntityEventsData;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->accessRequest:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->accessRequestPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->changedFields:Lcom/google/api/services/plusi/model/EntityEventsDataChangedFields;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->deletedPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->eventActivityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->inviteeSummary:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->photoUploaders:Lcom/google/api/services/plusi/model/EntityEventsDataPeopleList;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->plusPhotoAlbum:Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityEventsData;->renderEventsData:Lcom/google/api/services/plusi/model/EntityEventsDataRenderEventsData;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EntityEventsData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityEventsData;-><init>()V

    return-object v0
.end method
