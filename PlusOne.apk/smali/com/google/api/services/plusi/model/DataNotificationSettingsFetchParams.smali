.class public final Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataNotificationSettingsFetchParams.java"


# instance fields
.field public fetchAlternateEmailAddress:Ljava/lang/Boolean;

.field public fetchPlusPageSettings:Ljava/lang/Boolean;

.field public fetchSettingsDescription:Ljava/lang/Boolean;

.field public fetchWhoCanNotifyMe:Ljava/lang/Boolean;

.field public fetchWhoCanNotifyMeGames:Ljava/lang/Boolean;

.field public settingsType:Ljava/lang/String;

.field public typeGroupToFetch:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
