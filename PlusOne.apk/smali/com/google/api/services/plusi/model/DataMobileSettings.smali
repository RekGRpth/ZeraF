.class public final Lcom/google/api/services/plusi/model/DataMobileSettings;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataMobileSettings.java"


# instance fields
.field public country:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataMobileSettingsCountry;",
            ">;"
        }
    .end annotation
.end field

.field public deliveryWindow:Lcom/google/api/services/plusi/model/DataMobileSettingsDeliveryWindow;

.field public emailFrequency:Ljava/lang/String;

.field public hasPushTarget:Ljava/lang/Boolean;

.field public isInternationalSmsServiceNumber:Ljava/lang/Boolean;

.field public isNumberAdded:Ljava/lang/Boolean;

.field public isSmsEnabled:Ljava/lang/Boolean;

.field public isSmsPostingDisabled:Ljava/lang/Boolean;

.field public mobileNotificationType:Ljava/lang/String;

.field public mobileNumber:Ljava/lang/String;

.field public pin:Ljava/lang/String;

.field public selectedCountry:Lcom/google/api/services/plusi/model/DataMobileSettingsCountry;

.field public smsFrequency:Ljava/lang/String;

.field public smsServiceNumber:Ljava/lang/String;

.field public userRegionCodeFromPhoneNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
