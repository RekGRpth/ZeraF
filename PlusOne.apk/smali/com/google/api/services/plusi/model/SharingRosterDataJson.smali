.class public final Lcom/google/api/services/plusi/model/SharingRosterDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "SharingRosterDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/SharingRosterData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/SharingRosterDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SharingRosterDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SharingRosterDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/SharingRosterDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/SharingRosterDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/SharingRosterData;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ApplicationSharingPolicyJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "applicationPolicy"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/SharingTargetJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "targets"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/SharingRosterDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/SharingRosterDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/SharingRosterDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/SharingRosterData;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SharingRosterData;->applicationPolicy:Lcom/google/api/services/plusi/model/ApplicationSharingPolicy;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SharingRosterData;->targets:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SharingRosterData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SharingRosterData;-><init>()V

    return-object v0
.end method
