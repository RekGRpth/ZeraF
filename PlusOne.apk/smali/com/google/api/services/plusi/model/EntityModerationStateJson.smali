.class public final Lcom/google/api/services/plusi/model/EntityModerationStateJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntityModerationStateJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntityModerationState;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntityModerationStateJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EntityModerationStateJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityModerationStateJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntityModerationStateJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityModerationStateJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/EntityModerationState;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "commentId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "state"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "updateId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntityModerationStateJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/EntityModerationStateJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntityModerationStateJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/EntityModerationState;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityModerationState;->commentId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityModerationState;->state:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntityModerationState;->updateId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EntityModerationState;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntityModerationState;-><init>()V

    return-object v0
.end method
