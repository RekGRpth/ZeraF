.class public final Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotosOfUserRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosOfUserRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "approvedResumeToken"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "maxResults"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "photoOptions"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "returnApproved"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "returnSuggested"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "returnUnapproved"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "suggestedResumeToken"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "unapprovedResumeToken"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosOfUserRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->approvedResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->maxResults:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->returnApproved:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->returnSuggested:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->returnUnapproved:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->suggestedResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;->unapprovedResumeToken:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosOfUserRequest;-><init>()V

    return-object v0
.end method
