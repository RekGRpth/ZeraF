.class public final Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotosInAlbumResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataAlbumJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "album"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/TileJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "albumTile"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "errorCode"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "featuredPhoto"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "isDownloadable"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/DataUserJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "owner"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/DataPhotoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "photo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->albumTile:Lcom/google/api/services/plusi/model/Tile;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->errorCode:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->featuredPhoto:Lcom/google/api/services/plusi/model/DataPhoto;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->isDownloadable:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->owner:Lcom/google/api/services/plusi/model/DataUser;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->photo:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;-><init>()V

    return-object v0
.end method
