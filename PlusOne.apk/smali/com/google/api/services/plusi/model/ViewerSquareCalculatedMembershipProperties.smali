.class public final Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ViewerSquareCalculatedMembershipProperties.java"


# instance fields
.field public canInviteToSquare:Ljava/lang/Boolean;

.field public canJoin:Ljava/lang/Boolean;

.field public canRequestToJoin:Ljava/lang/Boolean;

.field public canSeeMemberList:Ljava/lang/Boolean;

.field public canSeePosts:Ljava/lang/Boolean;

.field public canShareSquare:Ljava/lang/Boolean;

.field public isAdmin:Ljava/lang/Boolean;

.field public isMember:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
