.class public final Lcom/google/api/services/plusi/model/EntityPhotosData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntityPhotosData.java"


# instance fields
.field public album:Lcom/google/api/services/plusi/model/EntityAlbumData;

.field public numFaces:Ljava/lang/Integer;

.field public numPhotos:Ljava/lang/Integer;

.field public numPhotosDeleted:Ljava/lang/Integer;

.field public numTagged:Ljava/lang/Integer;

.field public numTagsRemoved:Ljava/lang/Integer;

.field public numVideos:Ljava/lang/Integer;

.field public photo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public photoCrop:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntityPhotoCrop;",
            ">;"
        }
    .end annotation
.end field

.field public photoIdsWithTaggees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public taggeeOid:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public users:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;"
        }
    .end annotation
.end field

.field public xsrfTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
