.class public final Lcom/google/api/services/plusi/model/SuggestedSquare;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SuggestedSquare.java"


# instance fields
.field public numPeopleInCommon:Ljava/lang/Integer;

.field public peopleInCommon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareMember;",
            ">;"
        }
    .end annotation
.end field

.field public score:Ljava/lang/Double;

.field public viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
