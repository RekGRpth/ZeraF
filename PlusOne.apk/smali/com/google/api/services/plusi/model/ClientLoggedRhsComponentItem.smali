.class public final Lcom/google/api/services/plusi/model/ClientLoggedRhsComponentItem;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientLoggedRhsComponentItem.java"


# instance fields
.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public col:Ljava/lang/Integer;

.field public connectSiteId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public gamesLabelId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public hangout:Lcom/google/api/services/plusi/model/ClientLoggedHangout;

.field public localWriteReviewClusterId:Ljava/lang/String;

.field public localWriteReviewInfo:Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;

.field public person:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public row:Ljava/lang/Integer;

.field public square:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedSquare;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionInfo:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
