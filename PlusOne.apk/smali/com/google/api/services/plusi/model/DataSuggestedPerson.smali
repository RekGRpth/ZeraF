.class public final Lcom/google/api/services/plusi/model/DataSuggestedPerson;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSuggestedPerson.java"


# instance fields
.field public debugInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public explanation:Lcom/google/api/services/plusi/model/DataSugggestionExplanation;

.field public friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

.field public friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

.field public member:Lcom/google/api/services/plusi/model/DataCirclePerson;

.field public paidPromotionInfo:Lcom/google/api/services/plusi/model/PaidPromotionSuggestedPersonInfo;

.field public relevantCircleId:Lcom/google/api/services/plusi/model/DataCircleId;

.field public score:Ljava/lang/Double;

.field public shownCount:Ljava/lang/Integer;

.field public suggestionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
