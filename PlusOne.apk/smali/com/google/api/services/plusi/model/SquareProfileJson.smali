.class public final Lcom/google/api/services/plusi/model/SquareProfileJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "SquareProfileJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/SquareProfile;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/SquareProfileJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SquareProfileJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SquareProfileJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/SquareProfileJson;->INSTANCE:Lcom/google/api/services/plusi/model/SquareProfileJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/SquareProfile;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "aboutText"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "abuseAppealState"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "abuseState"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "abuseType"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "bannerUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "isDomainRestricted"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/PlaceJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "location"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "name"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "photoUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/SquareRelatedLinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "relatedLinkList"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/SquareRelatedLinksJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "relatedLinks"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "tagline"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/SquareProfileJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/SquareProfileJson;->INSTANCE:Lcom/google/api/services/plusi/model/SquareProfileJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/SquareProfile;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->aboutText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->abuseAppealState:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->abuseState:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->abuseType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->bannerUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->isDomainRestricted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->location:Lcom/google/api/services/plusi/model/Place;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->photoUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->relatedLinkList:Lcom/google/api/services/plusi/model/SquareRelatedLinks;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->relatedLinks:Lcom/google/api/services/plusi/model/SquareRelatedLinks;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareProfile;->tagline:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SquareProfile;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SquareProfile;-><init>()V

    return-object v0
.end method
