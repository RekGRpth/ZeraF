.class public final Lcom/google/api/services/plusi/model/ViewerSquareJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ViewerSquareJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ViewerSquare;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ViewerSquareJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ViewerSquareJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ViewerSquareJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ViewerSquareJson;->INSTANCE:Lcom/google/api/services/plusi/model/ViewerSquareJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ViewerSquare;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipPropertiesJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "calculatedMembershipProperties"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "numPeopleInCommon"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/SquareMemberJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "peopleInCommon"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/SquareJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "square"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStatsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "squareActivityStats"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStatsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "squareMemberStats"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/SquareNotificationOptionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "squareNotificationOptions"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/ViewerSquareStreamListJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "streams"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "viewerMembershipStatus"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "viewerNotificationSettings"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "volume"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ViewerSquareJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ViewerSquareJson;->INSTANCE:Lcom/google/api/services/plusi/model/ViewerSquareJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ViewerSquare;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->calculatedMembershipProperties:Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->numPeopleInCommon:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->peopleInCommon:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->squareActivityStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->squareMemberStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->squareNotificationOptions:Lcom/google/api/services/plusi/model/SquareNotificationOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerMembershipStatus:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerNotificationSettings:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ViewerSquare;->volume:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ViewerSquare;-><init>()V

    return-object v0
.end method
