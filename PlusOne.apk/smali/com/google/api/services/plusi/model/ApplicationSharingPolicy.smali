.class public final Lcom/google/api/services/plusi/model/ApplicationSharingPolicy;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ApplicationSharingPolicy.java"


# instance fields
.field public allowSquares:Ljava/lang/Boolean;

.field public allowedGroupType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public applicationId:Ljava/lang/String;

.field public showUnderageWarnings:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
