.class public final Lcom/google/api/services/plusi/model/UploadMediaResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "UploadMediaResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public photo:Lcom/google/api/services/plusi/model/DataPhoto;

.field public quota:Lcom/google/api/services/plusi/model/DataStorageQuota;

.field public scottyInfo:Lcom/google/api/services/plusi/model/ScottyInfo;

.field public setProfilePhotoSucceeded:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
