.class public final Lcom/google/api/services/plusi/model/CreateCircleResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "CreateCircleResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/CreateCircleResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/CreateCircleResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/CreateCircleResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CreateCircleResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/CreateCircleResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/CreateCircleResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/CreateCircleResponse;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "category"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/DataCircleIdJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "circleId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/DataCirclePersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "circlePerson"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "nameSortKey"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/DataRevertCookieJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "revertCookie"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "versionInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/CreateCircleResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/CreateCircleResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/CreateCircleResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->category:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->circlePerson:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->nameSortKey:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->revertCookie:Lcom/google/api/services/plusi/model/DataRevertCookie;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CreateCircleResponse;->versionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/CreateCircleResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CreateCircleResponse;-><init>()V

    return-object v0
.end method
