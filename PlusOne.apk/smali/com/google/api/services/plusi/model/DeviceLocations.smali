.class public final Lcom/google/api/services/plusi/model/DeviceLocations;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DeviceLocations.java"


# instance fields
.field public deviceLocation:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DeviceLocation;",
            ">;"
        }
    .end annotation
.end field

.field public lastSavedMetadata:Lcom/google/api/services/plusi/model/Metadata;

.field public metadata:Lcom/google/api/services/plusi/model/Metadata;

.field public sharingEnabled:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
