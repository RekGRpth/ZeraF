.class public final Lcom/google/api/services/plusi/model/DataSuggestionAction;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSuggestionAction.java"


# instance fields
.field public accepted:Ljava/lang/Boolean;

.field public actionType:Ljava/lang/String;

.field public circleId:Lcom/google/api/services/plusi/model/DataCircleId;

.field public suggestedEntityId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedEntityId;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionContext:Ljava/lang/String;

.field public suggestionId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleMemberId;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionUi:Ljava/lang/String;

.field public timeUsec:Ljava/math/BigInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
