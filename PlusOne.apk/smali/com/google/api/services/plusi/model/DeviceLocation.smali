.class public final Lcom/google/api/services/plusi/model/DeviceLocation;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DeviceLocation.java"


# instance fields
.field public accuracyMeters:Ljava/lang/Integer;

.field public altitudeMeters:Ljava/lang/Long;

.field public displayName:Ljava/lang/String;

.field public lat:Ljava/lang/Double;

.field public lng:Ljava/lang/Double;

.field public mapImageUrl:Ljava/lang/String;

.field public mapImageUrlFullBleed:Ljava/lang/String;

.field public mapImageUrlMobile:Ljava/lang/String;

.field public timestampUsec:Ljava/lang/Long;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
