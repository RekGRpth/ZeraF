.class public final Lcom/google/api/services/plusi/model/CategoryProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "CategoryProto.java"


# instance fields
.field public categoryLabel:Ljava/lang/String;

.field public gvertical:Lcom/google/api/services/plusi/model/GVerticalProto;

.field public showClosedZippyEllipsis:Ljava/lang/Boolean;

.field public zippyClosedItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/CategoryProtoItem;",
            ">;"
        }
    .end annotation
.end field

.field public zippyOpenedItem:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/CategoryProtoItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
