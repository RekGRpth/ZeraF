.class public final Lcom/google/api/services/plusi/model/HotelPriceProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "HotelPriceProto.java"


# instance fields
.field public checkin:Ljava/lang/String;

.field public checkinHr:Ljava/lang/String;

.field public checkout:Ljava/lang/String;

.field public checkoutHr:Ljava/lang/String;

.field public conversionDisclaimerLink:Ljava/lang/String;

.field public formattedLowestBasePrice:Ljava/lang/String;

.field public formattedLowestPrice:Ljava/lang/String;

.field public formattedLowestRawBasePrice:Ljava/lang/String;

.field public formattedLowestRawPrice:Ljava/lang/String;

.field public formattedPrefixCurrency:Ljava/lang/String;

.field public formattedSuffixCurrency:Ljava/lang/String;

.field public hotelLoggingData:Lcom/google/api/services/plusi/model/HotelPriceProtoHotelLoggingData;

.field public owner:Lcom/google/api/services/plusi/model/HotelPriceProtoHotelPartner;

.field public partner:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/HotelPriceProtoHotelPartner;",
            ">;"
        }
    .end annotation
.end field

.field public taxesInclusive:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
