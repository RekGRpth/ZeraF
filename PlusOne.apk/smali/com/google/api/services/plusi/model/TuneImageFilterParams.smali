.class public final Lcom/google/api/services/plusi/model/TuneImageFilterParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "TuneImageFilterParams.java"


# instance fields
.field public ambience:Ljava/lang/Float;

.field public brightness:Ljava/lang/Float;

.field public contrast:Ljava/lang/Float;

.field public controlPoints:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;",
            ">;"
        }
    .end annotation
.end field

.field public fillLight:Ljava/lang/Float;

.field public hue:Ljava/lang/Float;

.field public saturation:Ljava/lang/Float;

.field public tint:Ljava/lang/Float;

.field public warmth:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
