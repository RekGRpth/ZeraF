.class public final Lcom/google/api/services/plusi/model/SquareMember;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SquareMember.java"


# instance fields
.field public displayName:Ljava/lang/String;

.field public isPlusPage:Ljava/lang/Boolean;

.field public membershipStatus:Ljava/lang/String;

.field public obfuscatedGaiaId:Ljava/lang/String;

.field public photoUrl:Ljava/lang/String;

.field public plusPageMemberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

.field public tagLine:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
