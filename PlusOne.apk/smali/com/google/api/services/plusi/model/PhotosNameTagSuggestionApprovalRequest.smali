.class public final Lcom/google/api/services/plusi/model/PhotosNameTagSuggestionApprovalRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosNameTagSuggestionApprovalRequest.java"


# instance fields
.field public approve:Ljava/lang/Boolean;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public ownerId:Ljava/lang/String;

.field public photoId:Ljava/lang/String;

.field public shapeId:Ljava/lang/String;

.field public taggeeId:Ljava/lang/String;

.field public taggeeName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
