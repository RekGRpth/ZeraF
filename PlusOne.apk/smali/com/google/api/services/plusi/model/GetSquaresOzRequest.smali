.class public final Lcom/google/api/services/plusi/model/GetSquaresOzRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetSquaresOzRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public consistentRead:Ljava/lang/Boolean;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public includePeopleInCommon:Ljava/lang/Boolean;

.field public squareType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
