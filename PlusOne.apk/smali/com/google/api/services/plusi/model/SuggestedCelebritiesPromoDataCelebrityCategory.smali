.class public final Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoDataCelebrityCategory;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SuggestedCelebritiesPromoDataCelebrityCategory.java"


# instance fields
.field public category:Ljava/lang/String;

.field public categoryName:Ljava/lang/String;

.field public people:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewPerson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
