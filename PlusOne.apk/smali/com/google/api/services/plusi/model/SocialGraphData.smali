.class public final Lcom/google/api/services/plusi/model/SocialGraphData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SocialGraphData.java"


# instance fields
.field public blocked:Ljava/lang/Boolean;

.field public circleData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleData;",
            ">;"
        }
    .end annotation
.end field

.field public circlePerson:Lcom/google/api/services/plusi/model/DataCirclePerson;

.field public muted:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
