.class public final Lcom/google/api/services/plusi/model/ViewerSquare;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ViewerSquare.java"


# instance fields
.field public calculatedMembershipProperties:Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;

.field public numPeopleInCommon:Ljava/lang/Integer;

.field public peopleInCommon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareMember;",
            ">;"
        }
    .end annotation
.end field

.field public square:Lcom/google/api/services/plusi/model/Square;

.field public squareActivityStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

.field public squareMemberStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;

.field public squareNotificationOptions:Lcom/google/api/services/plusi/model/SquareNotificationOptions;

.field public streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

.field public viewerMembershipStatus:Ljava/lang/String;

.field public viewerNotificationSettings:Ljava/lang/String;

.field public volume:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
