.class public final Lcom/google/api/services/plusi/model/EditModerationStateRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EditModerationStateRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public entityState:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntityModerationState;",
            ">;"
        }
    .end annotation
.end field

.field public fbsVersionInfo:Ljava/lang/String;

.field public renderContext:Lcom/google/api/services/plusi/model/RenderContext;

.field public streamId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
