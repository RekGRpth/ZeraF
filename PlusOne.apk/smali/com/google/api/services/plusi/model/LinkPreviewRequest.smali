.class public final Lcom/google/api/services/plusi/model/LinkPreviewRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LinkPreviewRequest.java"


# instance fields
.field public callToActionDeepLinkId:Ljava/lang/String;

.field public callToActionLabel:Ljava/lang/String;

.field public callToActionLabelDeprecated:Ljava/lang/String;

.field public callToActionUrl:Ljava/lang/String;

.field public clientId:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public content:Ljava/lang/String;

.field public deepLinkId:Ljava/lang/String;

.field public embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

.field public enableTracing:Ljava/lang/Boolean;

.field public fallbackToUrl:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public isInteractivePost:Ljava/lang/Boolean;

.field public useBlackboxPreviewData:Ljava/lang/Boolean;

.field public useEmbedV2:Ljava/lang/Boolean;

.field public useSmallPreviews:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
