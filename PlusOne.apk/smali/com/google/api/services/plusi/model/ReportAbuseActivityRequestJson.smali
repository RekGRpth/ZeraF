.class public final Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ReportAbuseActivityRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataAbuseReportJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "abuseReport"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "isUndo"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "itemId"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/RenderContextJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "renderContext"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->isUndo:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->itemId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->renderContext:Lcom/google/api/services/plusi/model/RenderContext;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;-><init>()V

    return-object v0
.end method
