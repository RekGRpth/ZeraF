.class public final Lcom/google/api/services/plusi/model/PlaceReviewJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PlaceReviewJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PlaceReview;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PlaceReviewJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PlaceReviewJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PlaceReviewJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PlaceReviewJson;->INSTANCE:Lcom/google/api/services/plusi/model/PlaceReviewJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PlaceReview;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/EmbedsPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "author"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "dateModified"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "dateModifiedMilliseconds"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "itemReviewed"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/PlaceReviewMetadataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "meta"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "name"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "price"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "reviewBody"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/RatingJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "reviewRating"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "url"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PlaceReviewJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PlaceReviewJson;->INSTANCE:Lcom/google/api/services/plusi/model/PlaceReviewJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PlaceReview;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->author:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->dateModified:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->dateModifiedMilliseconds:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->itemReviewed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->meta:Lcom/google/api/services/plusi/model/PlaceReviewMetadata;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->price:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewRating:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlaceReview;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PlaceReview;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PlaceReview;-><init>()V

    return-object v0
.end method
