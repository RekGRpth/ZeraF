.class public final Lcom/google/api/services/plusi/model/MediaProtoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "MediaProtoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/MediaProto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/MediaProtoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/MediaProtoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MediaProtoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/MediaProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/MediaProtoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/MediaProto;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "authorName"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/LatLngProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "latLng"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "originalIndex"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "reviewId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "sourceId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "sourceName"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/MediaProtoThumbnailJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "thumbnail"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/MediaProtoThumbnailJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "thumbnails"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "title"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/UserMediaProtoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "userMedia"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/MediaProtoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/MediaProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/MediaProtoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/MediaProto;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->authorName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->latLng:Lcom/google/api/services/plusi/model/LatLngProto;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->originalIndex:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->reviewId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->sourceId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->sourceName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->thumbnail:Lcom/google/api/services/plusi/model/MediaProtoThumbnail;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->thumbnails:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaProto;->userMedia:Lcom/google/api/services/plusi/model/UserMediaProto;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/MediaProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MediaProto;-><init>()V

    return-object v0
.end method
