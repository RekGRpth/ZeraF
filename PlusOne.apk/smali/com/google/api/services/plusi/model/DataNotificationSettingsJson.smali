.class public final Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataNotificationSettingsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataNotificationSettings;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DataNotificationSettings;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataNotificationSettingsEmailJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "alternateEmail"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataNotificationSettingsNotificationsSettingsCategoryInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "categoryInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "defaultDestinationId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "deliveryOption"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "emailAddress"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "emailAddressState"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "emailDeliveryOption"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "isEmailAddressEditable"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "showEntitiesSettings"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOptionJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "smsDeliveryOption"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "smsDestinationId"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "uncircledCanNotify"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "userInstalledFountain"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataNotificationSettingsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->alternateEmail:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->categoryInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->defaultDestinationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->deliveryOption:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->emailAddress:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->emailAddressState:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->emailDeliveryOption:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->isEmailAddressEditable:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->showEntitiesSettings:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->smsDeliveryOption:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->smsDestinationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->uncircledCanNotify:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataNotificationSettings;->userInstalledFountain:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettings;-><init>()V

    return-object v0
.end method
