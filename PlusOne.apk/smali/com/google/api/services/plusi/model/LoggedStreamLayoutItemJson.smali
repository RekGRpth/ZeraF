.class public final Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedStreamLayoutItemJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "col"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "itemEstimatedHeight"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "itemHeight"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "rank"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "superpost"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "xPosition"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "yPosition"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedStreamLayoutItemJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->activityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->col:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->itemEstimatedHeight:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->itemHeight:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->rank:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->superpost:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->xPosition:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;->yPosition:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedStreamLayoutItem;-><init>()V

    return-object v0
.end method
