.class public final Lcom/google/api/services/plusi/model/ActionTargetJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ActionTargetJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ActionTarget;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ActionTargetJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActionTargetJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActionTargetJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ActionTargetJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActionTargetJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ActionTarget;

    const/16 v1, 0x60

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/AclDetailsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "aclDetails"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "actionSource"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/ActivityDetailsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "activityDetails"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/LoggedAutoCompleteJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "autoComplete"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/LoggedBillboardImpressionJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "billboardImpression"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/LoggedBillboardPromoActionJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "billboardPromoAction"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "categoryId"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/LoggedCircleJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "circle"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plusi/model/LoggedCircleMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "circleMember"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "commentId"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "connectSiteId"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "deprecatedCircleId"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "deprecatedSettingsNotificationType"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "entityTypeId"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "externalUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "featureHintType"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-class v3, Lcom/google/api/services/plusi/model/LoggedFrameJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "frame"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "gadgetId"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "gadgetPlayId"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "gaiaId"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-class v3, Lcom/google/api/services/plusi/model/LoggedIntrCelebsClickJson;

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string v3, "intrCelebsClick"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string v3, "iphFlowId"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string v3, "iphStepId"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string v3, "isUnreadNotification"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-string v3, "labelId"

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-class v3, Lcom/google/api/services/plusi/model/LoggedLocalWriteReviewInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string v3, "localWriteReviewInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string v3, "notificationId"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string v3, "notificationSlot"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-class v3, Lcom/google/api/services/plusi/model/NotificationTypesJson;

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string v3, "notificationTypes"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-string v3, "notificationWidgetPostReloadBuildLabel"

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-string v3, "notificationWidgetPreReloadBuildLabel"

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-string v3, "notificationWidgetUpTimeBeforeReload"

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-string v3, "numUnreadNotifications"

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-string v3, "page"

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-class v3, Lcom/google/api/services/plusi/model/LoggedPeopleViewComponentJson;

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-string v3, "peopleViewComponent"

    aput-object v3, v1, v2

    const/16 v2, 0x35

    const-string v3, "photoAlbumId"

    aput-object v3, v1, v2

    const/16 v2, 0x36

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x37

    const-string v3, "photoAlbumIdDeprecated"

    aput-object v3, v1, v2

    const/16 v2, 0x38

    const-string v3, "photoAlbumType"

    aput-object v3, v1, v2

    const/16 v2, 0x39

    const-string v3, "photoCount"

    aput-object v3, v1, v2

    const/16 v2, 0x3a

    const-string v3, "photoFilterPresetId"

    aput-object v3, v1, v2

    const/16 v2, 0x3b

    sget-object v3, Lcom/google/api/services/plusi/model/ActionTargetJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x3c

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/16 v2, 0x3d

    const-string v3, "photoIndexInPost"

    aput-object v3, v1, v2

    const/16 v2, 0x3e

    const-string v3, "plusEventId"

    aput-object v3, v1, v2

    const/16 v2, 0x3f

    const-string v3, "previousNumUnreadNotifications"

    aput-object v3, v1, v2

    const/16 v2, 0x40

    const-string v3, "profileData"

    aput-object v3, v1, v2

    const/16 v2, 0x41

    const-string v3, "promoType"

    aput-object v3, v1, v2

    const/16 v2, 0x42

    const-class v3, Lcom/google/api/services/plusi/model/PromotedYMLImpressionJson;

    aput-object v3, v1, v2

    const/16 v2, 0x43

    const-string v3, "promotedYmlInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x44

    const-string v3, "questionsOneboxQuery"

    aput-object v3, v1, v2

    const/16 v2, 0x45

    const-string v3, "region"

    aput-object v3, v1, v2

    const/16 v2, 0x46

    const-class v3, Lcom/google/api/services/plusi/model/RelatedTopicTargetInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x47

    const-string v3, "relatedTopicTargetInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x48

    const-string v3, "reviewId"

    aput-object v3, v1, v2

    const/16 v2, 0x49

    const-class v3, Lcom/google/api/services/plusi/model/LoggedRhsComponentJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4a

    const-string v3, "rhsComponent"

    aput-object v3, v1, v2

    const/16 v2, 0x4b

    const-class v3, Lcom/google/api/services/plusi/model/LoggedRibbonClickJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4c

    const-string v3, "ribbonClick"

    aput-object v3, v1, v2

    const/16 v2, 0x4d

    const-class v3, Lcom/google/api/services/plusi/model/LoggedRibbonOrderJson;

    aput-object v3, v1, v2

    const/16 v2, 0x4e

    const-string v3, "ribbonOrder"

    aput-object v3, v1, v2

    const/16 v2, 0x4f

    const-class v3, Lcom/google/api/services/plusi/model/SettingsNotificationTypeJson;

    aput-object v3, v1, v2

    const/16 v2, 0x50

    const-string v3, "settingsNotificationType"

    aput-object v3, v1, v2

    const/16 v2, 0x51

    const-class v3, Lcom/google/api/services/plusi/model/LoggedShareboxInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x52

    const-string v3, "shareboxInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x53

    const-string v3, "shortcutTask"

    aput-object v3, v1, v2

    const/16 v2, 0x54

    const-class v3, Lcom/google/api/services/plusi/model/SocialadsContextJson;

    aput-object v3, v1, v2

    const/16 v2, 0x55

    const-string v3, "socialadsContext"

    aput-object v3, v1, v2

    const/16 v2, 0x56

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSquareJson;

    aput-object v3, v1, v2

    const/16 v2, 0x57

    const-string v3, "square"

    aput-object v3, v1, v2

    const/16 v2, 0x58

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x59

    const-string v3, "suggestionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x5a

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfoJson;

    aput-object v3, v1, v2

    const/16 v2, 0x5b

    const-string v3, "suggestionSummary"

    aput-object v3, v1, v2

    const/16 v2, 0x5c

    const-string v3, "tab"

    aput-object v3, v1, v2

    const/16 v2, 0x5d

    const-string v3, "updateStreamPosition"

    aput-object v3, v1, v2

    const/16 v2, 0x5e

    const-class v3, Lcom/google/api/services/plusi/model/VolumeChangeJson;

    aput-object v3, v1, v2

    const/16 v2, 0x5f

    const-string v3, "volumeChange"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ActionTargetJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ActionTargetJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActionTargetJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ActionTarget;

    const/16 v0, 0x41

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->aclDetails:Lcom/google/api/services/plusi/model/AclDetails;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->actionSource:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->activityDetails:Lcom/google/api/services/plusi/model/ActivityDetails;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->activityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->autoComplete:Lcom/google/api/services/plusi/model/LoggedAutoComplete;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->billboardImpression:Lcom/google/api/services/plusi/model/LoggedBillboardImpression;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->billboardPromoAction:Lcom/google/api/services/plusi/model/LoggedBillboardPromoAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->categoryId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->circle:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->circleMember:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->commentId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->connectSiteId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->deprecatedCircleId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->deprecatedSettingsNotificationType:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->entityTypeId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->externalUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->featureHintType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->frame:Lcom/google/api/services/plusi/model/LoggedFrame;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->gadgetId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->gadgetPlayId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->gaiaId:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->intrCelebsClick:Lcom/google/api/services/plusi/model/LoggedIntrCelebsClick;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->iphFlowId:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->iphStepId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->isUnreadNotification:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->labelId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->localWriteReviewInfo:Lcom/google/api/services/plusi/model/LoggedLocalWriteReviewInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationSlot:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationTypes:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationWidgetPostReloadBuildLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationWidgetPreReloadBuildLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationWidgetUpTimeBeforeReload:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->numUnreadNotifications:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->page:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->peopleViewComponent:Lcom/google/api/services/plusi/model/LoggedPeopleViewComponent;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoAlbumId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoAlbumIdDeprecated:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoAlbumType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoCount:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoFilterPresetId:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoId:Ljava/math/BigInteger;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->photoIndexInPost:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->plusEventId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->previousNumUnreadNotifications:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->profileData:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->promoType:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->promotedYmlInfo:Lcom/google/api/services/plusi/model/PromotedYMLImpression;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->questionsOneboxQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->region:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->relatedTopicTargetInfo:Lcom/google/api/services/plusi/model/RelatedTopicTargetInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->reviewId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->rhsComponent:Lcom/google/api/services/plusi/model/LoggedRhsComponent;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->ribbonClick:Lcom/google/api/services/plusi/model/LoggedRibbonClick;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->ribbonOrder:Lcom/google/api/services/plusi/model/LoggedRibbonOrder;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->settingsNotificationType:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->shareboxInfo:Lcom/google/api/services/plusi/model/LoggedShareboxInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->shortcutTask:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->socialadsContext:Lcom/google/api/services/plusi/model/SocialadsContext;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->square:Lcom/google/api/services/plusi/model/LoggedSquare;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->suggestionInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->suggestionSummary:Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfo;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->tab:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->updateStreamPosition:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActionTarget;->volumeChange:Lcom/google/api/services/plusi/model/VolumeChange;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActionTarget;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActionTarget;-><init>()V

    return-object v0
.end method
