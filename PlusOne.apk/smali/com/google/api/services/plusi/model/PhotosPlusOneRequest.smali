.class public final Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosPlusOneRequest.java"


# instance fields
.field public activityId:Ljava/lang/String;

.field public albumId:Ljava/lang/Long;

.field public authkey:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public deprecatedPhotoId:Ljava/lang/Long;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public isPlusOne:Ljava/lang/Boolean;

.field public ownerId:Ljava/lang/String;

.field public photoId:Ljava/lang/String;

.field public returnPlusOneResult:Ljava/lang/Boolean;

.field public squareId:Ljava/lang/String;

.field public squareStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
