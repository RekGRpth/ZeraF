.class public final Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "RegisterDeviceParamsDestinationSettingsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/AppleDestinationSettingsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "appleDestinationSettings"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;->appleDestinationSettings:Lcom/google/api/services/plusi/model/AppleDestinationSettings;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;-><init>()V

    return-object v0
.end method
