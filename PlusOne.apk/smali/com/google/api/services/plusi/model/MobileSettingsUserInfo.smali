.class public final Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MobileSettingsUserInfo.java"


# instance fields
.field public deviceLocationAcl:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DeviceLocationAcl;",
            ">;"
        }
    .end annotation
.end field

.field public displayName:Ljava/lang/String;

.field public emailAddress:Ljava/lang/String;

.field public obfuscatedGaiaId:Ljava/lang/String;

.field public photoIsSilhouette:Ljava/lang/Boolean;

.field public photoUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
