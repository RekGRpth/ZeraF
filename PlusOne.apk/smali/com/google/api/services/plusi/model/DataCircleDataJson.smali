.class public final Lcom/google/api/services/plusi/model/DataCircleDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataCircleDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataCircleData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataCircleDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataCircleDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCircleDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataCircleDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataCircleDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DataCircleData;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataCircleIdJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "circleId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataCirclePropertiesJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "circleProperties"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataContinuationTokenJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "continuationToken"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "deleted"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataCircleDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DataCircleDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataCircleDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DataCircleData;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCircleData;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCircleData;->continuationToken:Lcom/google/api/services/plusi/model/DataContinuationToken;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataCircleData;->deleted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataCircleData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCircleData;-><init>()V

    return-object v0
.end method
