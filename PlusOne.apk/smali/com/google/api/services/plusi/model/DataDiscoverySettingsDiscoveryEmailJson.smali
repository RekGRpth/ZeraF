.class public final Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataDiscoverySettingsDiscoveryEmailJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "alwaysDiscoverable"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "discoverable"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "removable"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "source"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "value"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "verified"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmailJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;->alwaysDiscoverable:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;->discoverable:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;->removable:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;->source:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;->value:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;->verified:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;-><init>()V

    return-object v0
.end method
