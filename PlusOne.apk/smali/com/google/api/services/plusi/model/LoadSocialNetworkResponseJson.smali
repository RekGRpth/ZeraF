.class public final Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoadSocialNetworkResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/DataPersonListJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "personList"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/DataSystemGroupsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "systemGroups"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/DataViewerCirclesJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "viewerCircles"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoadSocialNetworkResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->personList:Lcom/google/api/services/plusi/model/DataPersonList;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->systemGroups:Lcom/google/api/services/plusi/model/DataSystemGroups;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;->viewerCircles:Lcom/google/api/services/plusi/model/DataViewerCircles;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadSocialNetworkResponse;-><init>()V

    return-object v0
.end method
