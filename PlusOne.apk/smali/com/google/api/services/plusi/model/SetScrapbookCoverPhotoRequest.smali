.class public final Lcom/google/api/services/plusi/model/SetScrapbookCoverPhotoRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SetScrapbookCoverPhotoRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public galleryPhoto:Ljava/lang/Boolean;

.field public offset:Ljava/lang/Integer;

.field public ownerId:Ljava/lang/String;

.field public photoId:Ljava/lang/String;

.field public rotation:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
