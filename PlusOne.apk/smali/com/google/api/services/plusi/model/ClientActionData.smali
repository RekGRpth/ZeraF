.class public final Lcom/google/api/services/plusi/model/ClientActionData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientActionData.java"


# instance fields
.field public aclDetails:Lcom/google/api/services/plusi/model/ClientAclDetails;

.field public autoComplete:Lcom/google/api/services/plusi/model/ClientLoggedAutoComplete;

.field public autoCompleteQuery:Ljava/lang/String;

.field public billboardImpression:Lcom/google/api/services/plusi/model/ClientLoggedBillboardImpression;

.field public billboardPromoAction:Lcom/google/api/services/plusi/model/ClientLoggedBillboardPromoAction;

.field public circle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircle;",
            ">;"
        }
    .end annotation
.end field

.field public circleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public gadgetId:Ljava/lang/String;

.field public intrCelebsClick:Lcom/google/api/services/plusi/model/ClientLoggedIntrCelebsClick;

.field public labelId:Ljava/lang/String;

.field public localWriteReviewInfo:Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;

.field public obfuscatedGaiaId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public photoAlbumId:Ljava/lang/String;

.field public photoId:Ljava/lang/String;

.field public plusEventId:Ljava/lang/String;

.field public promotedYmlImpression:Lcom/google/api/services/plusi/model/ClientLoggedPromotedYMLImpression;

.field public realtimeUpdate:Lcom/google/api/services/plusi/model/ClientLoggedRealtimeUpdate;

.field public rhsComponent:Lcom/google/api/services/plusi/model/ClientLoggedRhsComponent;

.field public ribbonClick:Lcom/google/api/services/plusi/model/ClientLoggedRibbonClick;

.field public ribbonOrder:Lcom/google/api/services/plusi/model/ClientLoggedRibbonOrder;

.field public shareboxInfo:Lcom/google/api/services/plusi/model/ClientLoggedShareboxInfo;

.field public socialadsInfo:Lcom/google/api/services/plusi/model/SocialadsContext;

.field public square:Lcom/google/api/services/plusi/model/ClientLoggedSquare;

.field public streamLayout:Lcom/google/api/services/plusi/model/LoggedStreamLayout;

.field public suggestionInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionSummaryInfo:Lcom/google/api/services/plusi/model/ClientLoggedSuggestionSummaryInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
