.class public final Lcom/google/api/services/plusi/model/PlusEventFeatureFlags;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlusEventFeatureFlags.java"


# instance fields
.field public broadcast:Ljava/lang/Boolean;

.field public canDuplicateInvitees:Ljava/lang/Boolean;

.field public canSendMessage:Ljava/lang/Boolean;

.field public hangout:Ljava/lang/Boolean;

.field public hideGuestList:Ljava/lang/Boolean;

.field public openEventAcl:Ljava/lang/Boolean;

.field public openPhotoAcl:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
