.class public final Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "OutOfBoxFieldJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/OutOfBoxField;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;->INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/OutOfBoxField;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxActionJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "action"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxErrorJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "error"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "image"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "input"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxTextFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "text"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;->INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxFieldJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/OutOfBoxField;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxField;->error:Lcom/google/api/services/plusi/model/OutOfBoxError;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxField;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxField;-><init>()V

    return-object v0
.end method
