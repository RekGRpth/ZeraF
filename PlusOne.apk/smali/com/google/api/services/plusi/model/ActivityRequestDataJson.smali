.class public final Lcom/google/api/services/plusi/model/ActivityRequestDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ActivityRequestDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ActivityRequestData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ActivityRequestDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityRequestDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityRequestDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ActivityRequestDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActivityRequestDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ActivityRequestData;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "activityCountOnly"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/ActivityFiltersJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "activityFilters"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/FieldRequestOptionsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "fieldRequestOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "maxResults"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "shownActivitiesBlob"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/UpdateFilterJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "updateFilter"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ActivityRequestDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ActivityRequestDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/ActivityRequestDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ActivityRequestData;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityCountOnly:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityRequestData;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityRequestData;->maxResults:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityRequestData;->shownActivitiesBlob:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ActivityRequestData;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityRequestData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityRequestData;-><init>()V

    return-object v0
.end method
