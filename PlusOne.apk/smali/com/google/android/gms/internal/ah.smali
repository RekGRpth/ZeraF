.class public final Lcom/google/android/gms/internal/ah;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/gms/wallet/MaskedWallet;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Parcel;I)V
    .locals 4

    invoke-static {p1}, Lcom/google/android/gms/internal/ac;->b(Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->mVersionCode:I

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->b(Landroid/os/Parcel;II)V

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->iss:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->aud:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->iat:J

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->exp:J

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;IJ)V

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->typ:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->googleTransactionId:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->merchantTransactionId:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->email:Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILjava/lang/String;)V

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->paymentDescriptions:[Ljava/lang/String;

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;I[Ljava/lang/String;)V

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->billingAddress:Lcom/google/android/gms/wallet/Address;

    invoke-static {p1, v1, v2, p2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;I)V

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/wallet/MaskedWallet;->shippingAddress:Lcom/google/android/gms/wallet/Address;

    invoke-static {p1, v1, v2, p2}, Lcom/google/android/gms/internal/ac;->a(Landroid/os/Parcel;ILandroid/os/Parcelable;I)V

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/ac;->c(Landroid/os/Parcel;I)V

    return-void
.end method


# virtual methods
.method public final bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    new-instance v1, Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/MaskedWallet;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/bm;->g(Landroid/os/Parcel;)I

    move-result v2

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-ge v0, v2, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const v3, 0xffff

    and-int/2addr v3, v0

    packed-switch v3, :pswitch_data_0

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->e(Landroid/os/Parcel;I)V

    goto :goto_0

    :pswitch_0
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->h(Landroid/os/Parcel;I)I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->mVersionCode:I

    goto :goto_0

    :pswitch_1
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->iss:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->aud:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->i(Landroid/os/Parcel;I)J

    move-result-wide v3

    iput-wide v3, v1, Lcom/google/android/gms/wallet/MaskedWallet;->iat:J

    goto :goto_0

    :pswitch_4
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->i(Landroid/os/Parcel;I)J

    move-result-wide v3

    iput-wide v3, v1, Lcom/google/android/gms/wallet/MaskedWallet;->exp:J

    goto :goto_0

    :pswitch_5
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->typ:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->googleTransactionId:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->merchantTransactionId:Ljava/lang/String;

    goto :goto_0

    :pswitch_8
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->l(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->email:Ljava/lang/String;

    goto :goto_0

    :pswitch_9
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/bm;->d(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v4

    add-int/2addr v0, v3

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    iput-object v4, v1, Lcom/google/android/gms/wallet/MaskedWallet;->paymentDescriptions:[Ljava/lang/String;

    goto :goto_0

    :pswitch_a
    sget-object v3, Lcom/google/android/gms/wallet/Address;->CREATOR:Lcom/google/android/gms/internal/ag;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/internal/bm;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/Address;

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->billingAddress:Lcom/google/android/gms/wallet/Address;

    goto :goto_0

    :pswitch_b
    sget-object v3, Lcom/google/android/gms/wallet/Address;->CREATOR:Lcom/google/android/gms/internal/ag;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/internal/bm;->a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/Address;

    iput-object v0, v1, Lcom/google/android/gms/wallet/MaskedWallet;->shippingAddress:Lcom/google/android/gms/wallet/Address;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    if-eq v0, v2, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/bm$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Overread allowed size end="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/internal/bm$a;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v0

    :cond_1
    return-object v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    new-array v0, p1, [Lcom/google/android/gms/wallet/MaskedWallet;

    return-object v0
.end method
