.class public Lcom/google/android/gms/internal/ad$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/ad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "a"
.end annotation


# instance fields
.field final synthetic as:Lcom/google/android/gms/internal/ad;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/internal/ad;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/internal/bk;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/ad;->cz:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/internal/ad;->cz:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    iget-object v0, v0, Lcom/google/android/gms/internal/ad;->cD:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    iput-object p2, v0, Lcom/google/android/gms/internal/ad;->cO:Lcom/google/android/gms/internal/bk;

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ad;->C()V

    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ad;->O()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/ad$a;->as:Lcom/google/android/gms/internal/ad;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ad;->R()V

    goto :goto_0
.end method
