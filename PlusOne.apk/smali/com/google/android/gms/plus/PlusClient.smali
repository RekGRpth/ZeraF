.class public final Lcom/google/android/gms/plus/PlusClient;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/plus/PlusClient$a;,
        Lcom/google/android/gms/plus/PlusClient$c;,
        Lcom/google/android/gms/plus/PlusClient$b;
    }
.end annotation


# instance fields
.field private final bL:Lcom/google/android/gms/internal/m;


# virtual methods
.method public final a(Lcom/google/android/gms/plus/PlusClient$a;Landroid/net/Uri;I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/m;->a(Lcom/google/android/gms/plus/PlusClient$a;Landroid/net/Uri;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/PlusClient$b;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/m;->a(Lcom/google/android/gms/plus/PlusClient$b;Ljava/lang/String;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/PlusClient$c;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/m;->a(Lcom/google/android/gms/plus/PlusClient$c;)V

    return-void
.end method

.method public final isConnected()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/m;->isConnected()Z

    move-result v0

    return v0
.end method

.method public final isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/m;->isConnectionCallbacksRegistered(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)Z

    move-result v0

    return v0
.end method

.method public final registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/m;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method

.method public final unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient;->bL:Lcom/google/android/gms/internal/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/m;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    return-void
.end method
