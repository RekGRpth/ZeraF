.class public final Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;
.super Landroid/view/ViewGroup;


# instance fields
.field private dl:Lcom/google/android/gms/internal/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/gms/internal/g;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected final onLayout(ZIIII)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/gms/internal/g;->layout(IIII)V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->measureChild(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/g;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/g;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->setMeasuredDimension(II)V

    return-void
.end method

.method public final setAnnotation(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/g;->setAnnotation(I)V

    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/g;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setSize(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/g;->setSize(I)V

    return-void
.end method

.method public final setType(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopup;->dl:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/g;->setType(I)V

    return-void
.end method
