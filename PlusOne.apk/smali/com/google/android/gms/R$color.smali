.class public final Lcom/google/android/gms/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final album_background_color:I = 0x7f0b0020

.field public static final album_bottom_action_bar_disabled_color:I = 0x7f0b0029

.field public static final album_bottom_action_bar_divider_color:I = 0x7f0b0027

.field public static final album_bottom_action_bar_enabled_color:I = 0x7f0b0028

.field public static final album_comment_count_color:I = 0x7f0b0023

.field public static final album_disabled_color:I = 0x7f0b0021

.field public static final album_info_background_color:I = 0x7f0b0022

.field public static final album_instructions_background_color:I = 0x7f0b0025

.field public static final album_instructions_text_color:I = 0x7f0b0026

.field public static final album_plusone_count_color:I = 0x7f0b0024

.field public static final android_notifications_bright_text_color:I = 0x7f0b00df

.field public static final avatar_background_color:I = 0x7f0b0005

.field public static final avatar_lineup_overflow_text_color:I = 0x7f0b0006

.field public static final bar_graph_bar:I = 0x7f0b008f

.field public static final bar_graph_label:I = 0x7f0b008d

.field public static final bar_graph_total:I = 0x7f0b008c

.field public static final bar_graph_value:I = 0x7f0b008e

.field public static final button_bar_background:I = 0x7f0b006e

.field public static final button_bar_divider:I = 0x7f0b006f

.field public static final card_app_invite_background:I = 0x7f0b00b2

.field public static final card_auto_text:I = 0x7f0b00a6

.field public static final card_default_text:I = 0x7f0b00a1

.field public static final card_event_divider:I = 0x7f0b00e0

.field public static final card_event_going:I = 0x7f0b00e1

.field public static final card_event_info_text:I = 0x7f0b00e5

.field public static final card_event_invited:I = 0x7f0b00e3

.field public static final card_event_maybe:I = 0x7f0b00e4

.field public static final card_event_not_going:I = 0x7f0b00e2

.field public static final card_gray_spam_background:I = 0x7f0b00a7

.field public static final card_gray_spam_text:I = 0x7f0b00a8

.field public static final card_interactive_post_button_text:I = 0x7f0b00eb

.field public static final card_link:I = 0x7f0b00a5

.field public static final card_links_background_tint:I = 0x7f0b00ae

.field public static final card_links_image_border:I = 0x7f0b00a9

.field public static final card_links_title_text:I = 0x7f0b00aa

.field public static final card_links_title_text_shadow:I = 0x7f0b00ab

.field public static final card_links_url_text:I = 0x7f0b00ac

.field public static final card_links_url_text_shadow:I = 0x7f0b00ad

.field public static final card_not_plus_oned_shadow_text:I = 0x7f0b00a3

.field public static final card_not_plus_oned_text:I = 0x7f0b00a2

.field public static final card_plus_oned_text:I = 0x7f0b00a4

.field public static final card_square_invite_invitation_text:I = 0x7f0b00af

.field public static final card_square_invite_name_text:I = 0x7f0b00b0

.field public static final card_square_invite_shadow_text:I = 0x7f0b00b1

.field public static final circle_member_count_color:I = 0x7f0b0013

.field public static final clear:I = 0x7f0b0002

.field public static final comment_link:I = 0x7f0b0018

.field public static final compose_add_text:I = 0x7f0b0050

.field public static final compose_bar_image_divider:I = 0x7f0b005c

.field public static final compose_bar_share_button_disabled:I = 0x7f0b005e

.field public static final compose_bar_share_button_enabled:I = 0x7f0b005d

.field public static final compose_empty_bg:I = 0x7f0b0059

.field public static final compose_empty_fg:I = 0x7f0b005a

.field public static final compose_item_bg:I = 0x7f0b0055

.field public static final compose_location_list_bg:I = 0x7f0b0051

.field public static final compose_location_list_item_bg:I = 0x7f0b0052

.field public static final compose_panel_bg:I = 0x7f0b0053

.field public static final compose_photo_bg:I = 0x7f0b0056

.field public static final compose_photo_count:I = 0x7f0b0058

.field public static final compose_photo_more:I = 0x7f0b0057

.field public static final compose_shadow:I = 0x7f0b0054

.field public static final compose_update_bg:I = 0x7f0b004f

.field public static final default_bg:I = 0x7f0b0003

.field public static final event_card_activity_description_color:I = 0x7f0b0041

.field public static final event_card_activity_time_color:I = 0x7f0b0040

.field public static final event_card_activity_title_color:I = 0x7f0b0042

.field public static final event_card_details_collapse_expand_color:I = 0x7f0b0044

.field public static final event_card_details_description_color:I = 0x7f0b0045

.field public static final event_card_details_going_label:I = 0x7f0b0048

.field public static final event_card_details_option_description_color:I = 0x7f0b004a

.field public static final event_card_details_option_title_color:I = 0x7f0b0049

.field public static final event_card_details_rsvp_background:I = 0x7f0b0047

.field public static final event_card_details_rsvp_count_color:I = 0x7f0b004d

.field public static final event_card_details_see_invitees_color:I = 0x7f0b0046

.field public static final event_card_details_title_color:I = 0x7f0b0043

.field public static final event_card_photo_pending_background_color:I = 0x7f0b004b

.field public static final event_card_photo_pending_text_color:I = 0x7f0b004c

.field public static final event_detail_on_air:I = 0x7f0b0039

.field public static final event_detail_private:I = 0x7f0b003a

.field public static final event_info_background_color:I = 0x7f0b0038

.field public static final event_invitee_list_footer_text_color:I = 0x7f0b003f

.field public static final event_name_text_color:I = 0x7f0b0037

.field public static final event_rsvp_action_button_color:I = 0x7f0b003e

.field public static final event_rsvp_attending_color:I = 0x7f0b003b

.field public static final event_rsvp_maybe_color:I = 0x7f0b003c

.field public static final event_rsvp_not_attending_color:I = 0x7f0b003d

.field public static final generic_selected_item:I = 0x7f0b009b

.field public static final hangout_avatar_name_color:I = 0x7f0b0080

.field public static final hangout_common_menu_background:I = 0x7f0b0073

.field public static final hangout_context_menu_divider:I = 0x7f0b0082

.field public static final hangout_drag_drop_off_target:I = 0x7f0b0083

.field public static final hangout_drag_drop_on_target:I = 0x7f0b0084

.field public static final hangout_image_button_background_color:I = 0x7f0b007f

.field public static final hangout_inset_background:I = 0x7f0b007e

.field public static final hangout_invitee_background:I = 0x7f0b0072

.field public static final hangout_list_header_text_color:I = 0x7f0b007d

.field public static final hangout_message_text_color:I = 0x7f0b0081

.field public static final hangout_participants_gallery_background:I = 0x7f0b0074

.field public static final hangout_ringing_dark_background:I = 0x7f0b0089

.field public static final hangout_ringing_darker_background:I = 0x7f0b008a

.field public static final hangout_ringing_menu_divider:I = 0x7f0b0088

.field public static final hangout_ringing_text_color_dark:I = 0x7f0b0086

.field public static final hangout_ringing_text_color_darker:I = 0x7f0b0085

.field public static final hangout_ringing_text_color_light:I = 0x7f0b0087

.field public static final hangout_ringing_yellow_background:I = 0x7f0b008b

.field public static final hangout_toast_background:I = 0x7f0b0079

.field public static final hangout_toast_border:I = 0x7f0b0078

.field public static final hangout_toast_text:I = 0x7f0b007a

.field public static final hangout_video_background:I = 0x7f0b0076

.field public static final hangout_video_background_transparent:I = 0x7f0b0077

.field public static final hangout_volume_foreground:I = 0x7f0b0075

.field public static final host_background:I = 0x7f0b0004

.field public static final image_selected_stroke:I = 0x7f0b0011

.field public static final invitation_acl_and_block_text:I = 0x7f0b006d

.field public static final invitation_text:I = 0x7f0b006c

.field public static final notification_dim_color:I = 0x7f0b00de

.field public static final notifications_header_no_messages:I = 0x7f0b00db

.field public static final notifications_text_color_read:I = 0x7f0b00dc

.field public static final notifications_text_color_unread:I = 0x7f0b00dd

.field public static final one_up_divider:I = 0x7f0b00e6

.field public static final oob_button_divider:I = 0x7f0b0104

.field public static final oob_legal_link_text_color:I = 0x7f0b0103

.field public static final oob_legal_text_color:I = 0x7f0b0102

.field public static final oob_radio_button_highlight_text:I = 0x7f0b0106

.field public static final oob_radio_button_text:I = 0x7f0b0105

.field public static final participants_gallery:I = 0x7f0b0014

.field public static final participants_gallery_active_border:I = 0x7f0b0016

.field public static final participants_gallery_loudest_speaker_border:I = 0x7f0b0017

.field public static final participants_gallery_translucent:I = 0x7f0b0015

.field public static final photo_background_color:I = 0x7f0b002a

.field public static final photo_crop_dim_color:I = 0x7f0b0035

.field public static final photo_crop_highlight_color:I = 0x7f0b0036

.field public static final photo_home_album_count_color:I = 0x7f0b001f

.field public static final photo_home_album_name_color:I = 0x7f0b001e

.field public static final photo_home_info_background_color:I = 0x7f0b001d

.field public static final photo_info_comment_count_color:I = 0x7f0b0031

.field public static final photo_info_plusone_count_color:I = 0x7f0b0032

.field public static final photo_processing_background_color:I = 0x7f0b0033

.field public static final photo_processing_text_color:I = 0x7f0b0034

.field public static final photo_tag_color:I = 0x7f0b002b

.field public static final photo_tag_scroller_name_text_color:I = 0x7f0b002f

.field public static final photo_tag_scroller_secondary_text_color:I = 0x7f0b0030

.field public static final photo_tag_shadow_color:I = 0x7f0b002c

.field public static final photo_tag_text_background_color:I = 0x7f0b002e

.field public static final photo_tag_text_color:I = 0x7f0b002d

.field public static final profile_edit_bg:I = 0x7f0b009f

.field public static final profile_edit_item_bg:I = 0x7f0b00a0

.field public static final profile_edit_item_list_bg:I = 0x7f0b009e

.field public static final profile_local_burgundy:I = 0x7f0b0092

.field public static final profile_local_editorial_label_gray:I = 0x7f0b0094

.field public static final profile_local_gray:I = 0x7f0b0093

.field public static final profile_local_user_aspect_explanation_gray:I = 0x7f0b0097

.field public static final profile_local_user_aspect_label_gray:I = 0x7f0b0096

.field public static final profile_local_zagat_explanation_background_bottom:I = 0x7f0b009a

.field public static final profile_local_zagat_explanation_background_top:I = 0x7f0b0099

.field public static final profile_local_zagat_explanation_circle:I = 0x7f0b0095

.field public static final profile_local_zagat_explanation_title:I = 0x7f0b0098

.field public static final profile_no_items:I = 0x7f0b009d

.field public static final profile_selected_item:I = 0x7f0b009c

.field public static final realtimechat_conversation_divider:I = 0x7f0b0069

.field public static final realtimechat_empty_background:I = 0x7f0b006b

.field public static final realtimechat_message_author:I = 0x7f0b005f

.field public static final realtimechat_message_author_failed:I = 0x7f0b0061

.field public static final realtimechat_message_divider:I = 0x7f0b006a

.field public static final realtimechat_message_text:I = 0x7f0b0060

.field public static final realtimechat_message_text_failed:I = 0x7f0b0062

.field public static final realtimechat_system_error_background:I = 0x7f0b0068

.field public static final realtimechat_system_error_foreground:I = 0x7f0b0066

.field public static final realtimechat_system_error_timestamp:I = 0x7f0b0067

.field public static final realtimechat_system_information_background:I = 0x7f0b0065

.field public static final realtimechat_system_information_foreground:I = 0x7f0b0063

.field public static final realtimechat_system_information_timestamp:I = 0x7f0b0064

.field public static final reshare_text:I = 0x7f0b005b

.field public static final riviera_album_background:I = 0x7f0b00ff

.field public static final riviera_body_metadata:I = 0x7f0b0100

.field public static final riviera_commenter_indicator:I = 0x7f0b0101

.field public static final riviera_media:I = 0x7f0b00fa

.field public static final riviera_media_overlay:I = 0x7f0b00f9

.field public static final riviera_reshare_background:I = 0x7f0b00fc

.field public static final riviera_separator_color:I = 0x7f0b00fd

.field public static final riviera_text_body:I = 0x7f0b00fb

.field public static final riviera_text_body_gray:I = 0x7f0b00fe

.field public static final riviera_text_btn:I = 0x7f0b00f7

.field public static final riviera_text_btn_white:I = 0x7f0b00f8

.field public static final riviera_text_h2:I = 0x7f0b00ec

.field public static final riviera_text_h3:I = 0x7f0b00ed

.field public static final riviera_text_h5:I = 0x7f0b00ee

.field public static final riviera_text_media_h1:I = 0x7f0b00ef

.field public static final riviera_text_media_metadata:I = 0x7f0b00f3

.field public static final riviera_text_media_setup_h1_blue:I = 0x7f0b00f4

.field public static final riviera_text_media_setup_h1_cyan:I = 0x7f0b00f5

.field public static final riviera_text_media_setup_h1_red:I = 0x7f0b00f6

.field public static final riviera_text_media_text:I = 0x7f0b00f1

.field public static final riviera_text_media_text_white:I = 0x7f0b00f2

.field public static final riviera_text_media_title:I = 0x7f0b00f0

.field public static final search_opaque_bg_color:I = 0x7f0b001a

.field public static final search_query_highlight_color:I = 0x7f0b001b

.field public static final search_tab_default_text_color:I = 0x7f0b0107

.field public static final search_tab_selected_text_color:I = 0x7f0b0108

.field public static final search_translucent_bg_color:I = 0x7f0b0019

.field public static final section_header_color:I = 0x7f0b001c

.field public static final section_header_opaque_bg:I = 0x7f0b004e

.field public static final signup_action_link_color:I = 0x7f0b0012

.field public static final solid_black:I = 0x7f0b0001

.field public static final solid_white:I = 0x7f0b0000

.field public static final square_blocking_explanation_background:I = 0x7f0b00ea

.field public static final square_card_description:I = 0x7f0b00e7

.field public static final square_card_divider:I = 0x7f0b00e9

.field public static final square_card_members:I = 0x7f0b00e8

.field public static final stream_circle_count_bg:I = 0x7f0b00d8

.field public static final stream_circle_disabled_text:I = 0x7f0b00da

.field public static final stream_circle_selected_item:I = 0x7f0b00d7

.field public static final stream_circle_settings_text:I = 0x7f0b00d9

.field public static final stream_content_color:I = 0x7f0b000f

.field public static final stream_link:I = 0x7f0b0009

.field public static final stream_name_color:I = 0x7f0b000e

.field public static final stream_one_up_action_bar_background:I = 0x7f0b00b6

.field public static final stream_one_up_background:I = 0x7f0b00b3

.field public static final stream_one_up_comment_body:I = 0x7f0b00c3

.field public static final stream_one_up_comment_count:I = 0x7f0b00bf

.field public static final stream_one_up_comment_count_divider:I = 0x7f0b00c0

.field public static final stream_one_up_comment_date:I = 0x7f0b00c2

.field public static final stream_one_up_comment_divider:I = 0x7f0b00c6

.field public static final stream_one_up_comment_name:I = 0x7f0b00c1

.field public static final stream_one_up_comment_plus_one:I = 0x7f0b00c4

.field public static final stream_one_up_comment_plus_one_inverse:I = 0x7f0b00c5

.field public static final stream_one_up_content:I = 0x7f0b00bb

.field public static final stream_one_up_date:I = 0x7f0b00b9

.field public static final stream_one_up_link:I = 0x7f0b00b7

.field public static final stream_one_up_linked_body:I = 0x7f0b00bd

.field public static final stream_one_up_linked_header:I = 0x7f0b00bc

.field public static final stream_one_up_list_background:I = 0x7f0b00b4

.field public static final stream_one_up_list_background_fade:I = 0x7f0b00b5

.field public static final stream_one_up_muted:I = 0x7f0b00ba

.field public static final stream_one_up_name:I = 0x7f0b00b8

.field public static final stream_one_up_reshare_body:I = 0x7f0b00be

.field public static final stream_one_up_stage_skyjam_preview:I = 0x7f0b00d6

.field public static final stream_one_up_stage_skyjam_preview_background:I = 0x7f0b00d5

.field public static final stream_one_up_stage_skyjam_tag_background:I = 0x7f0b00d3

.field public static final stream_one_up_stage_tag:I = 0x7f0b00d4

.field public static final stream_plus_one_clickable_count:I = 0x7f0b000a

.field public static final stream_tab_line_color:I = 0x7f0b0010

.field public static final styled_button_bar_background:I = 0x7f0b0070

.field public static final styled_button_text_color:I = 0x7f0b0071

.field public static final switcher_background:I = 0x7f0b0090

.field public static final switcher_item_font:I = 0x7f0b0091

.field public static final tab_background_color:I = 0x7f0b0007

.field public static final tab_text_color:I = 0x7f0b0008

.field public static final tile_photo_one_up_comment_body:I = 0x7f0b00cd

.field public static final tile_photo_one_up_comment_date:I = 0x7f0b00cc

.field public static final tile_photo_one_up_comment_divider:I = 0x7f0b00d0

.field public static final tile_photo_one_up_comment_name:I = 0x7f0b00cb

.field public static final tile_photo_one_up_comment_plus_one:I = 0x7f0b00ce

.field public static final tile_photo_one_up_comment_plus_one_inverse:I = 0x7f0b00cf

.field public static final tile_photo_one_up_content:I = 0x7f0b00c9

.field public static final tile_photo_one_up_date:I = 0x7f0b00c8

.field public static final tile_photo_one_up_link:I = 0x7f0b00d1

.field public static final tile_photo_one_up_list_background:I = 0x7f0b00ca

.field public static final tile_photo_one_up_list_background_fade:I = 0x7f0b00d2

.field public static final tile_photo_one_up_name:I = 0x7f0b00c7

.field public static final title_background:I = 0x7f0b000b

.field public static final title_text_color:I = 0x7f0b000d

.field public static final transparent_black_title_background:I = 0x7f0b000c

.field public static final widgets_kennedy_title_bar_default_color:I = 0x7f0b007b

.field public static final widgets_kennedy_title_bar_default_color_transparent:I = 0x7f0b007c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
