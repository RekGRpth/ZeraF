.class public final Lcom/google/android/gms/R$plurals;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "plurals"
.end annotation


# static fields
.field public static final accessibility_notification_count_description:I = 0x7f0f002f

.field public static final added_to_circle_notification_message:I = 0x7f0f0032

.field public static final album_photo_count:I = 0x7f0f0000

.field public static final audience_display_selection_count:I = 0x7f0f0023

.field public static final audience_hidden_user_count:I = 0x7f0f0030

.field public static final circle_button_more_circles:I = 0x7f0f0027

.field public static final circle_count_and_matched_email:I = 0x7f0f0024

.field public static final circle_entry_content_description:I = 0x7f0f001f

.field public static final comments:I = 0x7f0f001d

.field public static final common_friend_count:I = 0x7f0f0031

.field public static final delete_circles_dialog_message:I = 0x7f0f0026

.field public static final delete_circles_dialog_title:I = 0x7f0f0025

.field public static final delete_local_photo_dialog_message:I = 0x7f0f0004

.field public static final delete_photo:I = 0x7f0f001e

.field public static final delete_photo_dialog_title:I = 0x7f0f0002

.field public static final delete_photo_pending:I = 0x7f0f0005

.field public static final delete_remote_photo_dialog_message:I = 0x7f0f0003

.field public static final event_invitee_other_count:I = 0x7f0f0013

.field public static final event_invitee_with_guests:I = 0x7f0f0012

.field public static final from_your_phone_selected_count:I = 0x7f0f0001

.field public static final notification_single_post:I = 0x7f0f0021

.field public static final notification_square_multi_post_read:I = 0x7f0f0010

.field public static final notification_square_multi_post_unread:I = 0x7f0f000f

.field public static final notifications_content_text:I = 0x7f0f0022

.field public static final notifications_instant_upload_known_faces_text:I = 0x7f0f0008

.field public static final notifications_instant_upload_text:I = 0x7f0f0007

.field public static final notifications_instant_upload_title:I = 0x7f0f0006

.field public static final notifications_single_post_title_comment:I = 0x7f0f0009

.field public static final notifications_single_post_title_mention:I = 0x7f0f000c

.field public static final notifications_single_post_title_plus_one:I = 0x7f0f000a

.field public static final notifications_single_post_title_post:I = 0x7f0f000e

.field public static final notifications_single_post_title_reshare:I = 0x7f0f000b

.field public static final notifications_single_post_title_share:I = 0x7f0f000d

.field public static final notifications_ticker_text:I = 0x7f0f0020

.field public static final num_days_ago:I = 0x7f0f002a

.field public static final num_hours_ago:I = 0x7f0f0029

.field public static final num_minutes_ago:I = 0x7f0f0028

.field public static final plus_one_accessibility_description:I = 0x7f0f002e

.field public static final plus_one_people_more_plus_ones:I = 0x7f0f0016

.field public static final plusoned_by_one_known_and_unknowns:I = 0x7f0f0019

.field public static final plusoned_by_two_known_and_unknowns:I = 0x7f0f001b

.field public static final plusoned_by_unknowns:I = 0x7f0f0017

.field public static final plusoned_by_you_and_one_known_and_unknowns:I = 0x7f0f001a

.field public static final plusoned_by_you_and_two_known_and_unknowns:I = 0x7f0f001c

.field public static final plusoned_by_you_and_unknowns:I = 0x7f0f0018

.field public static final profile_added_by:I = 0x7f0f002b

.field public static final profile_local_review_count:I = 0x7f0f002c

.field public static final share_photo_count:I = 0x7f0f0015

.field public static final sign_out_message:I = 0x7f0f0014

.field public static final square_members_count:I = 0x7f0f0033

.field public static final stream_circle_settings_members_noun:I = 0x7f0f0011

.field public static final stream_one_up_comment_count:I = 0x7f0f002d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
