.class final Lcom/google/android/gms/maps/MapView$a;
.super Lcom/google/android/gms/internal/d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/maps/MapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/d",
        "<",
        "Lcom/google/android/gms/maps/MapView$b;",
        ">;"
    }
.end annotation


# instance fields
.field private final dJ:Landroid/view/ViewGroup;

.field private final dK:Lcom/google/android/gms/maps/GoogleMapOptions;

.field protected g:Lcom/google/android/gms/internal/aq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/aq",
            "<",
            "Lcom/google/android/gms/maps/MapView$b;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/d;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/maps/MapView$a;->dJ:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/gms/maps/MapView$a;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/maps/MapView$a;->dK:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/internal/aq;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/aq",
            "<",
            "Lcom/google/android/gms/maps/MapView$b;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/maps/MapView$a;->g:Lcom/google/android/gms/internal/aq;

    iget-object v0, p0, Lcom/google/android/gms/maps/MapView$a;->g:Lcom/google/android/gms/internal/aq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/MapView$a;->ab()Lcom/google/android/gms/dynamic/LifecycleDelegate;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/MapView$a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/q;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/MapView$a;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/internal/ak;->b(Ljava/lang/Object;)Lcom/google/android/gms/internal/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/MapView$a;->dK:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/am;->a(Lcom/google/android/gms/internal/f;Lcom/google/android/gms/maps/GoogleMapOptions;)Lcom/google/android/gms/maps/internal/IMapViewDelegate;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/MapView$a;->g:Lcom/google/android/gms/internal/aq;

    new-instance v2, Lcom/google/android/gms/maps/MapView$b;

    iget-object v3, p0, Lcom/google/android/gms/maps/MapView$a;->dJ:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/maps/MapView$b;-><init>(Landroid/view/ViewGroup;Lcom/google/android/gms/maps/internal/IMapViewDelegate;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/internal/aq;->b(Lcom/google/android/gms/dynamic/LifecycleDelegate;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
