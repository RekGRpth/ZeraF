.class final Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;
.super Ljava/io/InputStream;
.source "UrlDownloader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/UrlDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SharedInputStream"
.end annotation


# instance fields
.field private mOffset:J

.field private mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

.field final synthetic this$0:Lcom/google/android/picasastore/UrlDownloader;


# direct methods
.method public constructor <init>(Lcom/google/android/picasastore/UrlDownloader;Lcom/google/android/picasastore/UrlDownloader$DownloadTask;)V
    .locals 2
    .param p2    # Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    iput-object p1, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->this$0:Lcom/google/android/picasastore/UrlDownloader;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mOffset:J

    iput-object p2, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    iget-object v0, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    invoke-virtual {v0}, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->requestRead()V

    return-void
.end method


# virtual methods
.method public final close()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    if-nez v1, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->releaseReadRequest()V

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected final finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    if-eqz v0, :cond_0

    const-string v0, "UrlDownloader"

    const-string v1, "unclosed input stream"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    if-eqz v1, :cond_1

    const-string v1, "UrlDownloader"

    const-string v2, "unclosed input stream"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {p0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v0
.end method

.method public final read()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    new-array v0, v3, [B

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->read([BII)I

    move-result v1

    if-lez v1, :cond_0

    aget-byte v2, v0, v3

    and-int/lit16 v2, v2, 0xff

    :goto_0
    return v2

    :cond_0
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public final read([BII)I
    .locals 9
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    if-nez p3, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v4, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mTask:Lcom/google/android/picasastore/UrlDownloader$DownloadTask;

    iget-object v6, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->this$0:Lcom/google/android/picasastore/UrlDownloader;

    monitor-enter v6

    :try_start_0
    iget-wide v2, v4, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->downloadSize:J

    :goto_1
    iget-wide v7, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mOffset:J

    cmp-long v5, v2, v7

    if-gtz v5, :cond_2

    iget v5, v4, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->state:I

    and-int/lit8 v5, v5, 0x3

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->this$0:Lcom/google/android/picasastore/UrlDownloader;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    iget-wide v2, v4, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->downloadSize:J

    goto :goto_1

    :cond_1
    move v5, v1

    goto :goto_2

    :cond_2
    iget v5, v4, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->state:I

    const/16 v7, 0x8

    if-ne v5, v7, :cond_3

    new-instance v5, Ljava/io/IOException;

    const-string v7, "download fail!"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5

    :cond_3
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    int-to-long v5, p3

    iget-wide v7, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mOffset:J

    sub-long v7, v2, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v5

    long-to-int v0, v5

    if-nez v0, :cond_4

    const/4 v1, -0x1

    goto :goto_0

    :cond_4
    monitor-enter v4

    :try_start_2
    iget-object v5, v4, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->randomAccessFile:Ljava/io/RandomAccessFile;

    iget-wide v6, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mOffset:J

    invoke-virtual {v5, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    iget-object v5, v4, Lcom/google/android/picasastore/UrlDownloader$DownloadTask;->randomAccessFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v5, p1, p2, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v1

    iget-wide v5, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mOffset:J

    int-to-long v7, v1

    add-long/2addr v5, v7

    iput-wide v5, p0, Lcom/google/android/picasastore/UrlDownloader$SharedInputStream;->mOffset:J

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v5

    monitor-exit v4

    throw v5
.end method
