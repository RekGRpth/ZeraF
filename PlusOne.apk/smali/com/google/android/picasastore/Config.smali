.class public final Lcom/google/android/picasastore/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field public static sScreenNailSize:I

.field public static sThumbNailSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x280

    sput v0, Lcom/google/android/picasastore/Config;->sScreenNailSize:I

    const/16 v0, 0xc8

    sput v0, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 5
    .param p0    # Landroid/content/Context;

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v3, "window"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-lez v0, :cond_0

    div-int/lit8 v3, v0, 0x2

    sput v3, Lcom/google/android/picasastore/Config;->sScreenNailSize:I

    div-int/lit8 v3, v0, 0x5

    sput v3, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    :cond_0
    return-void
.end method
