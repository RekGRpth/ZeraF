.class final Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;
.super Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;
.source "PhotoPrefetch.java"

# interfaces
.implements Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PhotoPrefetch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrefetchFullImage"
.end annotation


# instance fields
.field private mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

.field final synthetic this$0:Lcom/google/android/picasasync/PhotoPrefetch;


# direct methods
.method public constructor <init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    sget-object v0, Lcom/google/android/picasasync/SyncState;->PREFETCH_FULL_IMAGE:Lcom/google/android/picasasync/SyncState;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;-><init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    return-void
.end method


# virtual methods
.method public final isBackgroundSync()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onDownloadFinish$256a6c5()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v2, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    iget v2, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    iget v3, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    sub-int v0, v2, v3

    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget v3, v1, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    invoke-virtual {v2, v0, v3}, Lcom/google/android/picasasync/PhotoPrefetch;->updateOngoingNotification(II)V

    return-void
.end method

.method public final performSync(Landroid/content/SyncResult;)V
    .locals 3
    .param p1    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, "PrefetchFullImage"

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->performSyncCommon(Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "picasa.prefetch.full_image"

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    return-void

    :catchall_0
    move-exception v1

    const-string v2, "picasa.prefetch.full_image"

    invoke-static {v0, v2}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    throw v1
.end method

.method protected final performSyncInternal$45259df4(Lcom/google/android/picasasync/UserEntry;Lcom/google/android/picasasync/PrefetchHelper;)Z
    .locals 4
    .param p1    # Lcom/google/android/picasasync/UserEntry;
    .param p2    # Lcom/google/android/picasasync/PrefetchHelper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v0, p0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->setCacheDownloadListener(Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;)V

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-static {v0, v2}, Lcom/google/android/picasasync/PhotoPrefetch;->access$100(Lcom/google/android/picasasync/PhotoPrefetch;Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;)V

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/google/android/picasasync/PrefetchHelper;->getCacheStatistics(I)Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v0, v0, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {p2, v0, p1}, Lcom/google/android/picasasync/PrefetchHelper;->syncFullImagesForUser(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Lcom/google/android/picasasync/UserEntry;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v0, v0, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->pendingCount:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mCacheStats:Lcom/google/android/picasasync/PrefetchHelper$CacheStats;

    iget v2, v2, Lcom/google/android/picasasync/PrefetchHelper$CacheStats;->totalCount:I

    invoke-virtual {v0, v2}, Lcom/google/android/picasasync/PhotoPrefetch;->showPrefetchCompleteNotification(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v0

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    const-string v3, "notification"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    throw v2

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
