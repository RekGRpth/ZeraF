.class final Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;
.super Lcom/google/android/picasasync/SyncTask;
.source "MetadataSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/MetadataSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MetadataSyncTask"
.end annotation


# instance fields
.field private mSyncCancelled:Z

.field private mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

.field final synthetic this$0:Lcom/google/android/picasasync/MetadataSync;


# direct methods
.method public constructor <init>(Lcom/google/android/picasasync/MetadataSync;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    invoke-direct {p0, p2}, Lcom/google/android/picasasync/SyncTask;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncCancelled:Z

    return-void
.end method


# virtual methods
.method public final declared-synchronized cancelSync()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncCancelled:Z

    iget-object v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->stopSync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final isBackgroundSync()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mIsManual:Z
    invoke-static {v0}, Lcom/google/android/picasasync/MetadataSync;->access$000(Lcom/google/android/picasasync/MetadataSync;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSyncOnBattery()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/MetadataSync;->access$100(Lcom/google/android/picasasync/MetadataSync;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->isSyncOnBattery(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final isSyncOnWifiOnly()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mIsManual:Z
    invoke-static {v0}, Lcom/google/android/picasasync/MetadataSync;->access$000(Lcom/google/android/picasasync/MetadataSync;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/MetadataSync;->access$100(Lcom/google/android/picasasync/MetadataSync;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->isSyncPicasaOnWifiOnly(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public final performSync(Landroid/content/SyncResult;)V
    .locals 8
    .param p1    # Landroid/content/SyncResult;

    const/4 v2, 0x0

    const-string v1, "MetadataSync"

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/picasasync/MetadataSync;->access$100(Lcom/google/android/picasasync/MetadataSync;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/picasasync/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mIsManual:Z
    invoke-static {v1}, Lcom/google/android/picasasync/MetadataSync;->access$000(Lcom/google/android/picasasync/MetadataSync;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA_MANUAL:Lcom/google/android/picasasync/SyncState;

    iget-object v5, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/picasasync/SyncState;->onSyncStart(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA:Lcom/google/android/picasasync/SyncState;

    iget-object v5, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/picasasync/SyncState;->onSyncStart(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    :goto_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncCancelled:Z

    if-eqz v1, :cond_2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :goto_1
    const-string v1, "picasa.sync.metadata"

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    return-void

    :cond_1
    :try_start_2
    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA:Lcom/google/android/picasasync/SyncState;

    iget-object v5, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/picasasync/SyncState;->onSyncStart(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v1

    or-int/lit8 v1, v1, 0x0

    sget-object v5, Lcom/google/android/picasasync/SyncState;->METADATA_MANUAL:Lcom/google/android/picasasync/SyncState;

    iget-object v6, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v5, v4, v6}, Lcom/google/android/picasasync/SyncState;->onSyncStart(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v5

    or-int/2addr v1, v5

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v3, p1, v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    iget-object v5, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->setAccount(Ljava/lang/String;)Z

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v1, "MetadataSyncProvider"

    const-string v2, "user: %s not found, sync abort"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    const-string v2, "picasa.sync.metadata"

    invoke-static {v0, v2}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    throw v1

    :catchall_1
    move-exception v1

    :try_start_5
    monitor-exit p0

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :try_start_6
    iget-object v1, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->isPicasaAccount(Ljava/lang/String;)Z
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v1

    :goto_2
    if-eqz v1, :cond_4

    :try_start_7
    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-virtual {v3, v1, v5}, Lcom/google/android/picasasync/PicasaSyncHelper;->syncAlbumsForUser(Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;Lcom/google/android/picasasync/UserEntry;)V

    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-virtual {v3, v1, v5}, Lcom/google/android/picasasync/PicasaSyncHelper;->syncPhotosForUser(Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;Lcom/google/android/picasasync/UserEntry;)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->mSyncContext:Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncHelper$SyncContext;->syncInterrupted()Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA:Lcom/google/android/picasasync/SyncState;

    iget-object v2, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/picasasync/SyncState;->onSyncFinish(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA_MANUAL:Lcom/google/android/picasasync/SyncState;

    iget-object v2, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/picasasync/SyncState;->onSyncFinish(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/picasasync/MetadataSync$MetadataSyncTask;->this$0:Lcom/google/android/picasasync/MetadataSync;

    # getter for: Lcom/google/android/picasasync/MetadataSync;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/picasasync/MetadataSync;->access$100(Lcom/google/android/picasasync/MetadataSync;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncManager;->requestPrefetchSync()V

    goto/16 :goto_1

    :catch_0
    move-exception v1

    const-string v6, "MetadataSyncProvider"

    const-string v7, "check picasa account failed"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v2

    goto :goto_2

    :cond_4
    const-string v1, "MetadataSyncProvider"

    const-string v2, "%s has not been enabled for Picasa service, just ignore"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA:Lcom/google/android/picasasync/SyncState;

    iget-object v2, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/picasasync/SyncState;->resetSyncToDirty(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/picasasync/SyncState;->METADATA_MANUAL:Lcom/google/android/picasasync/SyncState;

    iget-object v2, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, Lcom/google/android/picasasync/SyncState;->resetSyncToDirty(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1
.end method
