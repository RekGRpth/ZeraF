.class Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;
.super Lcom/google/android/picasasync/SyncTask;
.source "PhotoPrefetch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PhotoPrefetch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrefetchScreenNail"
.end annotation


# instance fields
.field protected mSyncCancelled:Z

.field protected mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

.field private mSyncState:Lcom/google/android/picasasync/SyncState;

.field final synthetic this$0:Lcom/google/android/picasasync/PhotoPrefetch;


# direct methods
.method public constructor <init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/picasasync/SyncState;->PREFETCH_SCREEN_NAIL:Lcom/google/android/picasasync/SyncState;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;-><init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/picasasync/SyncState;

    iput-object p1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    invoke-direct {p0, p2}, Lcom/google/android/picasasync/SyncTask;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncCancelled:Z

    iput-object p3, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncState:Lcom/google/android/picasasync/SyncState;

    return-void
.end method


# virtual methods
.method public final cancelSync()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncCancelled:Z

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->stopSync()V

    :cond_0
    return-void
.end method

.method public isBackgroundSync()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isSyncOnBattery()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->isSyncOnBattery(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final isSyncOnExternalStorageOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isSyncOnRoaming()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->isSyncOnRoaming(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final isSyncOnWifiOnly()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->isSyncPicasaOnWifiOnly(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public performSync(Landroid/content/SyncResult;)V
    .locals 3
    .param p1    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, "PrefetchScreenNail"

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->performSyncCommon(Landroid/content/SyncResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v1, "picasa.prefetch.screennail"

    invoke-static {v0, v1}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    return-void

    :catchall_0
    move-exception v1

    const-string v2, "picasa.prefetch.screennail"

    invoke-static {v0, v2}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    throw v1
.end method

.method protected final performSyncCommon(Landroid/content/SyncResult;)V
    .locals 8
    .param p1    # Landroid/content/SyncResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/picasasync/PrefetchHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PrefetchHelper;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncCancelled:Z

    if-eqz v4, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v1, p1, v4}, Lcom/google/android/picasasync/PrefetchHelper;->createPrefetchContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v4}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->updateCacheConfigVersion()V

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncState:Lcom/google/android/picasasync/SyncState;

    iget-object v5, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->syncAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/picasasync/SyncState;->onSyncStart(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "PhotoPrefetch"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid state: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncState:Lcom/google/android/picasasync/SyncState;

    iget-object v7, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->syncAccount:Ljava/lang/String;

    invoke-virtual {v6, v0, v7}, Lcom/google/android/picasasync/SyncState;->getState(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_1
    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->syncAccount:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/picasasync/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v4, "PhotoPrefetch"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cannot find user: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->syncAccount:Ljava/lang/String;

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v3, v1}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->performSyncInternal$45259df4(Lcom/google/android/picasasync/UserEntry;Lcom/google/android/picasasync/PrefetchHelper;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncState:Lcom/google/android/picasasync/SyncState;

    iget-object v5, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->syncAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/picasasync/SyncState;->onSyncFinish(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncState:Lcom/google/android/picasasync/SyncState;

    iget-object v5, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->syncAccount:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/picasasync/SyncState;->resetSyncToDirty(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected performSyncInternal$45259df4(Lcom/google/android/picasasync/UserEntry;Lcom/google/android/picasasync/PrefetchHelper;)Z
    .locals 3
    .param p1    # Lcom/google/android/picasasync/UserEntry;
    .param p2    # Lcom/google/android/picasasync/PrefetchHelper;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    # getter for: Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/picasasync/PhotoPrefetch;->access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, "PhotoPrefetch"

    const-string v2, "no external storage, skip screenail prefetching"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->this$0:Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-static {v1, v2}, Lcom/google/android/picasasync/PhotoPrefetch;->access$100(Lcom/google/android/picasasync/PhotoPrefetch;Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;)V

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {p2, v1, p1}, Lcom/google/android/picasasync/PrefetchHelper;->syncScreenNailsForUser(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;Lcom/google/android/picasasync/UserEntry;)V

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;->mSyncContext:Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
