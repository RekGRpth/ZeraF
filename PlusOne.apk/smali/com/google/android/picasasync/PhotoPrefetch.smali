.class Lcom/google/android/picasasync/PhotoPrefetch;
.super Ljava/lang/Object;
.source "PhotoPrefetch.java"

# interfaces
.implements Lcom/google/android/picasasync/SyncTaskProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PhotoPrefetch$PrefetchAlbumCover;,
        Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;,
        Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mImageType:I

.field private final mPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mImageType:I

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mPrefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/picasasync/PhotoPrefetch;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v0, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/PhotoPrefetch;Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;)V
    .locals 4
    .param p0    # Lcom/google/android/picasasync/PhotoPrefetch;
    .param p1    # Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x2

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mPrefs:Landroid/content/SharedPreferences;

    invoke-static {v1, v0, v3}, Lcom/google/android/picasasync/PhotoPrefetch;->compareAndSetCleanBit(Landroid/content/SharedPreferences;II)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/picasasync/PrefetchHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PrefetchHelper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/picasasync/PrefetchHelper;->cleanCache(Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;)V

    iget-object v1, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mPrefs:Landroid/content/SharedPreferences;

    invoke-virtual {p1}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->syncInterrupted()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_0
    invoke-static {v1, v3, v0}, Lcom/google/android/picasasync/PhotoPrefetch;->compareAndSetCleanBit(Landroid/content/SharedPreferences;II)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static declared-synchronized compareAndSetCleanBit(Landroid/content/SharedPreferences;II)Z
    .locals 5
    .param p0    # Landroid/content/SharedPreferences;
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/picasasync/PhotoPrefetch;

    monitor-enter v2

    :try_start_0
    const-string v3, "picasasync.prefetch.clean-cache"

    const/4 v4, 0x0

    invoke-interface {p0, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, p1, :cond_0

    :goto_0
    monitor-exit v2

    return v1

    :cond_0
    :try_start_1
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "picasasync.prefetch.clean-cache"

    invoke-interface {v1, v3, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method static onRequestSync(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/picasasync/PhotoPrefetch;->compareAndSetCleanBit(Landroid/content/SharedPreferences;II)Z

    return-void
.end method


# virtual methods
.method public final collectTasks(Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/picasasync/SyncTask;",
            ">;)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget v4, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mImageType:I

    packed-switch v4, :pswitch_data_0

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    :pswitch_0
    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getUsers()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/picasasync/UserEntry;

    sget-object v4, Lcom/google/android/picasasync/SyncState;->PREFETCH_FULL_IMAGE:Lcom/google/android/picasasync/SyncState;

    iget-object v5, v3, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/picasasync/SyncState;->isRequested(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;

    iget-object v5, v3, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-direct {v4, p0, v5}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchFullImage;-><init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;)V

    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getUsers()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/picasasync/UserEntry;

    sget-object v4, Lcom/google/android/picasasync/SyncState;->PREFETCH_SCREEN_NAIL:Lcom/google/android/picasasync/SyncState;

    iget-object v5, v3, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/picasasync/SyncState;->isRequested(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;

    iget-object v5, v3, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-direct {v4, p0, v5}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchScreenNail;-><init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;)V

    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getUsers()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/picasasync/UserEntry;

    sget-object v4, Lcom/google/android/picasasync/SyncState;->PREFETCH_ALBUM_COVER:Lcom/google/android/picasasync/SyncState;

    iget-object v5, v3, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/google/android/picasasync/SyncState;->isRequested(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchAlbumCover;

    iget-object v5, v3, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-direct {v4, p0, v5}, Lcom/google/android/picasasync/PhotoPrefetch$PrefetchAlbumCover;-><init>(Lcom/google/android/picasasync/PhotoPrefetch;Ljava/lang/String;)V

    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final resetSyncStates()V
    .locals 10

    iget-object v6, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-static {v6}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-class v7, Lcom/google/android/picasasync/PhotoPrefetch;

    monitor-enter v7

    :try_start_0
    const-string v6, "picasasync.prefetch.clean-cache"

    const/4 v8, 0x0

    invoke-interface {v3, v6, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_0

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v8, "picasasync.prefetch.clean-cache"

    const/4 v9, 0x2

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v6, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mImageType:I

    packed-switch v6, :pswitch_data_0

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :pswitch_0
    sget-object v4, Lcom/google/android/picasasync/SyncState;->PREFETCH_FULL_IMAGE:Lcom/google/android/picasasync/SyncState;

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->getUsers()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/picasasync/UserEntry;

    iget-object v6, v5, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v4, v0, v6}, Lcom/google/android/picasasync/SyncState;->resetSyncToDirty(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    sget-object v4, Lcom/google/android/picasasync/SyncState;->PREFETCH_SCREEN_NAIL:Lcom/google/android/picasasync/SyncState;

    goto :goto_0

    :pswitch_2
    sget-object v4, Lcom/google/android/picasasync/SyncState;->PREFETCH_ALBUM_COVER:Lcom/google/android/picasasync/SyncState;

    goto :goto_0

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method final showPrefetchCompleteNotification(I)V
    .locals 6
    .param p1    # I

    new-instance v2, Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/picasasync/R$layout;->ps_cache_notification:I

    invoke-direct {v2, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sget v3, Lcom/google/android/picasasync/R$id;->ps_status:I

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    sget v5, Lcom/google/android/picasasync/R$string;->ps_cache_done:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    sget v3, Lcom/google/android/picasasync/R$id;->ps_progress:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, p1, v4}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    const v3, 0x1080082

    iput v3, v1, Landroid/app/Notification;->icon:I

    iput-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v1, Landroid/app/Notification;->when:J

    iget-object v3, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    const-string v4, "notification"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method

.method final updateOngoingNotification(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/picasasync/R$string;->ps_cache_status:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/widget/RemoteViews;

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/picasasync/R$layout;->ps_cache_notification:I

    invoke-direct {v3, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sget v4, Lcom/google/android/picasasync/R$id;->ps_status:I

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    sget v4, Lcom/google/android/picasasync/R$id;->ps_progress:I

    invoke-virtual {v3, v4, p2, p1, v8}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    const v4, 0x1080081

    iput v4, v1, Landroid/app/Notification;->icon:I

    iput-object v3, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v1, Landroid/app/Notification;->when:J

    iget v4, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/app/Notification;->flags:I

    iget-object v4, p0, Lcom/google/android/picasasync/PhotoPrefetch;->mContext:Landroid/content/Context;

    const-string v5, "notification"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v9, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
