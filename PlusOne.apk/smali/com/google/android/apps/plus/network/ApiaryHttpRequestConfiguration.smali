.class public Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;
.super Ljava/lang/Object;
.source "ApiaryHttpRequestConfiguration.java"

# interfaces
.implements Lcom/google/android/apps/plus/network/HttpRequestConfiguration;


# static fields
.field private static sAuthDataFactory:Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mBackendOverrideUrl:Ljava/lang/String;

.field private final mContentType:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mScope:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->sAuthDataFactory:Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v5, "application/json"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mScope:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContentType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .locals 7
    .param p1    # Lorg/apache/http/client/methods/HttpRequestBase;

    const-string v4, "Accept-Encoding"

    const-string v5, "gzip"

    invoke-virtual {p1, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "User-Agent"

    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->getUserAgentHeader(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Content-Type"

    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContentType:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v4, :cond_0

    :try_start_0
    sget-object v4, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->sAuthDataFactory:Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;

    iget-object v4, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mScope:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;->getAuthData(Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;->getAuthToken(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;->getAuthTime(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bearer "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "X-Auth-Time"

    invoke-virtual {p1, v4, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "HttpTransaction"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "HttpTransaction"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Setting backend override url "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v4, "X-Google-Backend-Override"

    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    return-void

    :catch_0
    move-exception v3

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Cannot obtain authentication token"

    invoke-direct {v4, v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method protected getUserAgentHeader(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/network/UserAgent;->from(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (gzip)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final invalidateAuthToken()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_0

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->sAuthDataFactory:Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mScope:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory;->getAuthData(Ljava/lang/String;)Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/plus/api/ApiaryAuthDataFactory$ApiaryAuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot invalidate authentication token"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
