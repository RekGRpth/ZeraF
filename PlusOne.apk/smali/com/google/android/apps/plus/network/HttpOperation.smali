.class public Lcom/google/android/apps/plus/network/HttpOperation;
.super Ljava/lang/Object;
.source "HttpOperation.java"

# interfaces
.implements Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;,
        Lcom/google/android/apps/plus/network/HttpOperation$SimpleThreadFactory;
    }
.end annotation


# static fields
.field private static final sBufferCache:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<[B>;"
        }
    .end annotation
.end field

.field private static final sExecutorService:Ljava/util/concurrent/ExecutorService;

.field private static sSimulateException:Ljava/lang/Exception;

.field private static final sThreadFactory:Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private mAborted:Z

.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected final mContext:Landroid/content/Context;

.field private volatile mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

.field private mErrorCode:I

.field private mEx:Ljava/lang/Exception;

.field private final mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

.field private final mIntent:Landroid/content/Intent;

.field private final mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

.field private final mMethod:Ljava/lang/String;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mReasonPhrase:Ljava/lang/String;

.field private mRetriesRemaining:I

.field private mThreaded:Z

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v1, Ljava/util/Vector;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/Vector;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    const/4 v0, 0x0

    :goto_0
    if-gtz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    const/16 v2, 0x800

    new-array v2, v2, [B

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sSimulateException:Ljava/lang/Exception;

    new-instance v1, Lcom/google/android/apps/plus/network/HttpOperation$SimpleThreadFactory;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/network/HttpOperation$SimpleThreadFactory;-><init>(B)V

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sExecutorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Ljava/io/OutputStream;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    new-instance v4, Lcom/google/android/apps/plus/network/DefaultHttpRequestConfiguration;

    invoke-direct {v4, p1, p4}, Lcom/google/android/apps/plus/network/DefaultHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/network/HttpRequestConfiguration;
    .param p5    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p6    # Ljava/io/OutputStream;
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    iput-object p5, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p6, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    iput-object p7, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mIntent:Landroid/content/Intent;

    iput-object p8, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/network/HttpOperation;ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/network/HttpOperation;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->completeOperation(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected static captureResponse(Ljava/io/InputStream;ILjava/lang/StringBuilder;)Ljava/io/InputStream;
    .locals 3
    .param p0    # Ljava/io/InputStream;
    .param p1    # I
    .param p2    # Ljava/lang/StringBuilder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->readFromStream(Ljava/io/InputStream;ILjava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v2
.end method

.method private completeOperation(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;->onOperationComplete(Lcom/google/android/apps/plus/network/HttpOperation;)V

    :cond_0
    return-void
.end method

.method public static isConnected(Landroid/content/Context;)Z
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isConnectionError(Ljava/lang/Throwable;)Z
    .locals 3
    .param p0    # Ljava/lang/Throwable;

    const/4 v1, 0x0

    :goto_0
    if-nez p0, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    instance-of v2, p0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    move-object p0, v0

    goto :goto_0

    :cond_2
    instance-of v2, p0, Ljava/io/IOException;

    if-eqz v2, :cond_0

    instance-of v2, p0, Lcom/google/android/apps/plus/api/OzServerException;

    if-nez v2, :cond_0

    instance-of v2, p0, Lorg/apache/http/client/HttpResponseException;

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static readFromStream(Ljava/io/InputStream;ILjava/io/OutputStream;)V
    .locals 8
    .param p0    # Ljava/io/InputStream;
    .param p1    # I
    .param p2    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/network/NetworkException;,
            Lcom/google/android/apps/plus/network/MemoryException;
        }
    .end annotation

    const/4 v7, -0x1

    :try_start_0
    sget-object v5, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v1, 0x1

    :goto_0
    if-ne p1, v7, :cond_2

    :goto_1
    const/4 v5, 0x0

    :try_start_1
    array-length v6, v0

    invoke-virtual {p0, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-eq v2, v7, :cond_6

    const/4 v5, 0x0

    invoke-virtual {p2, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v4

    :try_start_2
    new-instance v5, Lcom/google/android/apps/plus/network/NetworkException;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v5

    if-eqz v1, :cond_0

    sget-object v6, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    invoke-virtual {v6, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz p0, :cond_1

    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    :cond_1
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_2
    throw v5

    :catch_1
    move-exception v5

    const/16 v5, 0x800

    new-array v0, v5, [B

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v3, p1

    :cond_3
    :goto_3
    if-lez v3, :cond_5

    const/4 v5, 0x0

    :try_start_4
    array-length v6, v0

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {p0, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-ne v2, v7, :cond_4

    new-instance v5, Lcom/google/android/apps/plus/network/NetworkException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid content length: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catch_2
    move-exception v4

    :try_start_5
    new-instance v5, Lcom/google/android/apps/plus/network/MemoryException;

    invoke-virtual {v4}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/network/MemoryException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_4
    if-lez v2, :cond_3

    sub-int/2addr v3, v2

    const/4 v5, 0x0

    :try_start_6
    invoke-virtual {p2, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_3

    :cond_5
    :goto_4
    const/4 v5, 0x0

    array-length v6, v0

    invoke-virtual {p0, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-eq v2, v7, :cond_6

    const/4 v5, 0x0

    invoke-virtual {p2, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    :cond_6
    if-eqz v1, :cond_7

    sget-object v5, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz p0, :cond_8

    :try_start_7
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    :cond_8
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_5
    return-void

    :catch_3
    move-exception v6

    goto :goto_2

    :catch_4
    move-exception v5

    goto :goto_5
.end method


# virtual methods
.method public final abort()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAborted:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->abort()V

    :cond_0
    return-void
.end method

.method public createPostData()Lorg/apache/http/HttpEntity;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    return v0
.end method

.method public final getException()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public final getMethod()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method public final getReasonPhrase()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public hasError()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAborted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAborted:Z

    return v0
.end method

.method protected isAuthenticationError(Ljava/lang/Exception;)Z
    .locals 2
    .param p1    # Ljava/lang/Exception;

    instance-of v1, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
    .end packed-switch
.end method

.method protected isImmediatelyRetryableError(Ljava/lang/Exception;)Z
    .locals 1
    .param p1    # Ljava/lang/Exception;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public final logAndThrowExceptionIfFailed(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->logError(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " operation failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " operation failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-direct {v0, v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " operation failed, error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method

.method public logError(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] failed due to exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-static {p1, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] failed due to error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onHttpCookie(Lorg/apache/http/cookie/Cookie;)V
    .locals 0
    .param p1    # Lorg/apache/http/cookie/Cookie;

    return-void
.end method

.method public onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    return-void
.end method

.method public onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # [Lorg/apache/http/Header;
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public onHttpReadFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # [Lorg/apache/http/Header;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-static {p1, p3, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->readFromStream(Ljava/io/InputStream;ILjava/io/OutputStream;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V

    goto :goto_0

    :cond_1
    const-string v1, "HttpTransaction"

    const-string v2, "Content type not specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->isImmediatelyRetryableError(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    if-lez v2, :cond_1

    :try_start_0
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    invoke-interface {v2}, Lcom/google/android/apps/plus/network/HttpRequestConfiguration;->invalidateAuthToken()V

    :cond_0
    const-string v2, "HttpTransaction"

    const-string v3, "====> Restarting operation..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "HttpTransaction"

    const-string v3, "====> Retry failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object p3, v0

    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    move-object v1, p3

    iget-boolean v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mThreaded:Z

    if-eqz v2, :cond_2

    new-instance v2, Lcom/google/android/apps/plus/network/HttpOperation$2;

    invoke-direct {v2, p0, p1, p2, v1}, Lcom/google/android/apps/plus/network/HttpOperation$2;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v2}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/plus/network/HttpOperation;->completeOperation(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final onStartResultProcessing()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->onStartResultProcessing()V

    :cond_0
    return-void
.end method

.method public setErrorInfo(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    iput p1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    iput-object p2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    return-void
.end method

.method public final setOutputStream(Ljava/io/OutputStream;)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;

    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    return-void
.end method

.method public final start()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v1, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    const-string v1, "HttpTransaction"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->log(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/network/HttpOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    goto :goto_0
.end method

.method public final start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p2    # Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAborted:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_1
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting op: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->createPostData()Lorg/apache/http/HttpEntity;

    move-result-object v4

    if-eqz v4, :cond_5

    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransaction;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/HttpTransaction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V

    :goto_1
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->printHeaders()V

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/network/HttpTransaction;->setHttpTransactionMetrics(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->execute()Ljava/lang/Exception;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p2, v7}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_2
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    goto :goto_0

    :cond_5
    :try_start_4
    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransaction;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/network/HttpTransaction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v8

    :try_start_5
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p2, v8}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->isAborted()Z

    move-result v1

    if-nez v1, :cond_7

    throw v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    :try_start_6
    iput-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    throw v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_1
    move-exception v6

    :try_start_7
    const-string v1, "HttpTransaction"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpOperation failed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v6}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    :try_start_8
    iput-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v1

    if-eqz p1, :cond_8

    if-eqz p2, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    :cond_8
    throw v1
.end method

.method public final startThreaded()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mThreaded:Z

    sget-object v0, Lcom/google/android/apps/plus/network/HttpOperation;->sExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/plus/network/HttpOperation$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/network/HttpOperation$1;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
