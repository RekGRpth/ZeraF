.class public final Lcom/google/android/apps/plus/network/HttpTransactionMetrics;
.super Ljava/lang/Object;
.source "HttpTransactionMetrics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;
    }
.end annotation


# instance fields
.field private mBaseReceivedBytes:J

.field private mBaseRequestCount:J

.field private mBaseSentBytes:J

.field private mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

.field private final mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mProcessingStartMillis:J

.field private mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

.field private mTransactionStartMillis:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->getDuration()J

    move-result-wide v1

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->duration:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$314(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->getProcessingDuration()J

    move-result-wide v1

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->processingDuration:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$214(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->getRequestCount()J

    move-result-wide v1

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->requestCount:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$414(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->getReceivedBytes()J

    move-result-wide v1

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->receivedBytes:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$514(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->getSentBytes()J

    move-result-wide v1

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->sentBytes:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$614(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    return-void
.end method

.method public final getDuration()J
    .locals 7

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->duration:J
    invoke-static {v5}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$300(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;)J

    move-result-wide v5

    add-long/2addr v0, v5

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method public final getProcessingDuration()J
    .locals 7

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v0, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->processingDuration:J
    invoke-static {v5}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$200(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;)J

    move-result-wide v5

    add-long/2addr v0, v5

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public final getReceivedBytes()J
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->receivedBytes:J
    invoke-static {v5}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$500(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;)J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_0

    :cond_0
    return-wide v3
.end method

.method public final getRequestCount()J
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->requestCount:J
    invoke-static {v5}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$400(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;)J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_0

    :cond_0
    return-wide v3
.end method

.method public final getSentBytes()J
    .locals 7

    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const-wide/16 v3, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->sentBytes:J
    invoke-static {v5}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$600(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;)J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_0

    :cond_0
    return-wide v3
.end method

.method public final log(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final onBeginTransaction(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # setter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->name:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$102(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mMap:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransactionStartMillis:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mProcessingStartMillis:J

    return-void
.end method

.method public final onEndResultProcessing()V
    .locals 7

    const-wide/16 v5, 0x0

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mProcessingStartMillis:J

    cmp-long v0, v0, v5

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mProcessingStartMillis:J

    sub-long/2addr v1, v3

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->processingDuration:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$214(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iput-wide v5, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mProcessingStartMillis:J

    :cond_0
    return-void
.end method

.method public final onEndTransaction()V
    .locals 7

    const-wide/16 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndResultProcessing()V

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransactionStartMillis:J

    sub-long/2addr v1, v3

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->duration:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$314(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->getRequestCount()J

    move-result-wide v1

    sub-long/2addr v1, v5

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->requestCount:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$414(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->getReceivedBytesCount()J

    move-result-wide v1

    sub-long/2addr v1, v5

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->receivedBytes:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$514(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->getSentBytesCount()J

    move-result-wide v1

    sub-long/2addr v1, v5

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->sentBytes:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$614(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    return-void
.end method

.method public final onEndTransaction(JJ)V
    .locals 5
    .param p1    # J
    .param p3    # J

    const-wide/16 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # setter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->duration:J
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$302(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    # setter for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->processingDuration:J
    invoke-static {v0, p3, p4}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$202(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->getRequestCount()J

    move-result-wide v1

    sub-long/2addr v1, v3

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->requestCount:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$414(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->getReceivedBytesCount()J

    move-result-wide v1

    sub-long/2addr v1, v3

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->receivedBytes:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$514(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    invoke-interface {v1}, Lorg/apache/http/HttpConnectionMetrics;->getSentBytesCount()J

    move-result-wide v1

    sub-long/2addr v1, v3

    # += operator for: Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->sentBytes:J
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;->access$614(Lcom/google/android/apps/plus/network/HttpTransactionMetrics$HttpTransactionMetricsHolder;J)J

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    return-void
.end method

.method public final onStartResultProcessing()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mProcessingStartMillis:J

    return-void
.end method

.method public final setConnectionMetrics(Lorg/apache/http/HttpConnectionMetrics;)V
    .locals 2
    .param p1    # Lorg/apache/http/HttpConnectionMetrics;

    const-wide/16 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mConnectionMetrics:Lorg/apache/http/HttpConnectionMetrics;

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mBaseRequestCount:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mBaseReceivedBytes:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->mBaseSentBytes:J

    return-void
.end method
