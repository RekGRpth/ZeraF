.class public final Lcom/google/android/apps/plus/network/JsonHttpEntity;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "JsonHttpEntity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Request:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">",
        "Lorg/apache/http/entity/AbstractHttpEntity;"
    }
.end annotation


# static fields
.field private static final BOUNDARY_HYPHEN_BYTES:[B

.field private static final CONTENT_TYPE_BYTES:[B

.field private static final CRLF_BYTES:[B

.field private static final DEFAULT_BOUNDARY_BYTES:[B


# instance fields
.field private final mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;"
        }
    .end annotation
.end field

.field private final mPayloadBytes:[B

.field private final mRequest:Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRequest;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "--"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->BOUNDARY_HYPHEN_BYTES:[B

    const-string v0, "onetwothreefourfivesixseven"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->DEFAULT_BOUNDARY_BYTES:[B

    const-string v0, "Content-Type: "

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CONTENT_TYPE_BYTES:[B

    const-string v0, "\r\n"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;TRequest;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;-><init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;[B)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;[B)V
    .locals 1
    .param p3    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;TRequest;[B)V"
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iput-object p2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    iput-object p3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    iget-object v0, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    if-nez v0, :cond_0

    const-string v0, "application/octet-stream"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->setContentType(Ljava/lang/String;)V

    const-string v0, "gzip"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->setContentEncoding(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "multipart/related"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->setContentType(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static writeBoundary(Ljava/io/OutputStream;Z)V
    .locals 1
    .param p0    # Ljava/io/OutputStream;
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->BOUNDARY_HYPHEN_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->DEFAULT_BOUNDARY_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->BOUNDARY_HYPHEN_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private static writeMetaData(Ljava/io/OutputStream;[B)V
    .locals 1
    .param p0    # Ljava/io/OutputStream;
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CONTENT_TYPE_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    const-string v0, "application/json; charset=UTF-8"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private static writePayload(Ljava/io/OutputStream;[B)V
    .locals 1
    .param p0    # Ljava/io/OutputStream;
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CONTENT_TYPE_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    const-string v0, "image/jpeg"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    const-string v0, "Content-Transfer-Encoding: binary"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write([B)V

    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/ByteArrayInputStream;

    new-array v2, v3, [B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    if-nez v1, :cond_1

    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeMetaData(Ljava/io/OutputStream;[B)V

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writePayload(Ljava/io/OutputStream;[B)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0
.end method

.method public final getContentLength()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final isRepeatable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .locals 5
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    if-eqz v2, :cond_0

    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeMetaData(Ljava/io/OutputStream;[B)V

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writePayload(Ljava/io/OutputStream;[B)V

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v2, :cond_1

    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/plus/json/EsJson;->writeToStream(Ljava/io/OutputStream;Ljava/lang/Object;)V

    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "A jsonGenerator was not found!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
