.class final Lcom/google/android/apps/plus/network/HttpTransaction;
.super Ljava/lang/Object;
.source "HttpTransaction.java"

# interfaces
.implements Lorg/apache/http/HttpRequestInterceptor;
.implements Lorg/apache/http/HttpResponseInterceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;,
        Lcom/google/android/apps/plus/network/HttpTransaction$MyInputStreamEntity;,
        Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    }
.end annotation


# static fields
.field private static final sHttpParams:Lorg/apache/http/params/HttpParams;

.field private static final sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;


# instance fields
.field private mAborted:Z

.field private final mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

.field private final mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

.field private mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v1, "http.socket.timeout"

    const v2, 0x15f90

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    sget-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v1, "http.connection.timeout"

    const/16 v2, 0xbb8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    sget-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v3

    const/16 v4, 0x50

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    sget-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v3

    const/16 v4, 0x1bb

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/network/HttpRequestConfiguration;
    .param p4    # Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GET"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    :goto_0
    if-nez p4, :cond_3

    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "The listener cannot be null"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    goto :goto_0

    :cond_1
    const-string v0, "DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported method: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput-object p4, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {p3, v0}, Lcom/google/android/apps/plus/network/HttpRequestConfiguration;->addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/network/HttpRequestConfiguration;
    .param p4    # Lorg/apache/http/HttpEntity;
    .param p5    # Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    :goto_0
    if-nez p5, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "The listener cannot be null"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "PUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported method: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-object p5, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {p3, v0}, Lcom/google/android/apps/plus/network/HttpRequestConfiguration;->addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V

    if-eqz p4, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    new-instance v1, Lcom/google/android/apps/plus/network/HttpTransaction$MyInputStreamEntity;

    invoke-direct {v1, p0, p4}, Lcom/google/android/apps/plus/network/HttpTransaction$MyInputStreamEntity;-><init>(Lcom/google/android/apps/plus/network/HttpTransaction;Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_3
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/network/HttpTransaction;)Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/network/HttpTransaction;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    return-object v0
.end method

.method private processCookies(Lorg/apache/http/HeaderIterator;Lorg/apache/http/cookie/CookieSpec;Lorg/apache/http/cookie/CookieOrigin;)V
    .locals 7
    .param p1    # Lorg/apache/http/HeaderIterator;
    .param p2    # Lorg/apache/http/cookie/CookieSpec;
    .param p3    # Lorg/apache/http/cookie/CookieOrigin;

    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {p1}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v3

    :try_start_0
    invoke-interface {p2, v3, p3}, Lorg/apache/http/cookie/CookieSpec;->parse(Lorg/apache/http/Header;Lorg/apache/http/cookie/CookieOrigin;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface {v5, v0}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpCookie(Lorg/apache/http/cookie/Cookie;)V
    :try_end_0
    .catch Lorg/apache/http/cookie/MalformedCookieException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v5, "HttpTransaction"

    const-string v6, "Malformed cookie"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public final abort()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_2

    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    sget-object v1, Landroid/os/StrictMode$ThreadPolicy;->LAX:Landroid/os/StrictMode$ThreadPolicy;

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    goto :goto_0
.end method

.method public final execute()Ljava/lang/Exception;
    .locals 19

    const/4 v9, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_1

    new-instance v8, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "Canceled"

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_0
    :goto_0
    return-object v8

    :cond_1
    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x0

    move-object v10, v9

    :goto_1
    const/4 v1, 0x2

    if-ge v11, v1, :cond_1e

    const/4 v7, 0x0

    :try_start_1
    new-instance v1, Lorg/apache/http/impl/conn/SingleClientConnManager;

    sget-object v2, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    sget-object v3, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v1, v2, v3}, Lorg/apache/http/impl/conn/SingleClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    new-instance v9, Lorg/apache/http/impl/client/DefaultHttpClient;

    sget-object v2, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    invoke-direct {v9, v1, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    const-class v1, Lorg/apache/http/client/protocol/RequestAddCookies;

    invoke-virtual {v9, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->removeRequestInterceptorByClass(Ljava/lang/Class;)V

    const-class v1, Lorg/apache/http/client/protocol/ResponseProcessCookies;

    invoke-virtual {v9, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->removeResponseInterceptorByClass(Ljava/lang/Class;)V

    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V
    :try_end_1
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {v9, v1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_4

    new-instance v8, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "Canceled"

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_2
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_0

    :catch_0
    move-exception v8

    move-object v7, v8

    :goto_2
    :try_start_3
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Connection failure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    add-int/lit8 v11, v11, 0x1

    move-object v10, v9

    goto/16 :goto_1

    :catch_1
    move-exception v8

    move-object v7, v8

    goto :goto_2

    :catch_2
    move-exception v8

    move-object v7, v8

    goto :goto_2

    :catch_3
    move-exception v8

    move-object v7, v8

    goto :goto_2

    :catch_4
    move-exception v8

    move-object v7, v8

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Non retryable error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_3
    if-eqz v7, :cond_6

    throw v7
    :try_end_3
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_5
    move-exception v8

    :goto_4
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_14

    new-instance v8, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "Canceled"

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_5
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    :cond_6
    :try_start_5
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_8

    new-instance v8, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "Canceled"

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_7
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    :cond_8
    const/4 v8, 0x0

    const/16 v1, 0xc8

    if-eq v6, v1, :cond_9

    :try_start_6
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v16

    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v5

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    move-object v15, v1

    :goto_5
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    const/16 v1, 0x3b

    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_a

    const/4 v2, 0x0

    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :cond_a
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v1

    long-to-int v4, v1

    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v17, "readFromHttpStream: Encoding: "

    move-object/from16 v0, v17

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v17, ", type: "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v17, ", length: "

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_b
    const/4 v2, 0x0

    :try_start_7
    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v1

    if-eqz v15, :cond_1d

    :try_start_8
    const-string v2, "gzip"

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    new-instance v2, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v2, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_e
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    const/4 v4, -0x1

    :goto_6
    const/16 v1, 0xc8

    if-ne v6, v1, :cond_f

    :try_start_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpReadFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :goto_7
    :try_start_a
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_a
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_a .. :try_end_a} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :goto_8
    const/16 v1, 0xc8

    if-eq v6, v1, :cond_c

    if-nez v8, :cond_c

    :try_start_b
    new-instance v8, Lorg/apache/http/client/HttpResponseException;

    invoke-direct {v8, v6, v14}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_11

    new-instance v8, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "Canceled"

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_b
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_8
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_d

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_d
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    :cond_e
    const/4 v1, 0x0

    move-object v15, v1

    goto/16 :goto_5

    :catch_6
    move-exception v1

    :goto_9
    :try_start_c
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_c .. :try_end_c} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :goto_a
    :try_start_d
    new-instance v2, Lcom/google/android/apps/plus/network/NetworkException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_d
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_d .. :try_end_d} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_d .. :try_end_d} :catch_5
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :catch_7
    move-exception v13

    move-object v8, v13

    goto :goto_8

    :cond_f
    :try_start_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface/range {v1 .. v6}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_7

    :catchall_0
    move-exception v1

    :try_start_f
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_f .. :try_end_f} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_f .. :try_end_f} :catch_5
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_8
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :goto_b
    :try_start_10
    throw v1
    :try_end_10
    .catch Lcom/google/android/apps/plus/api/ProtocolException; {:try_start_10 .. :try_end_10} :catch_7
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_10 .. :try_end_10} :catch_5
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_8
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :catch_8
    move-exception v8

    :goto_c
    :try_start_11
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_17

    new-instance v8, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "Canceled"

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_10
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    :cond_11
    :try_start_12
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface {v1, v6, v14, v8}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_12
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_12 .. :try_end_12} :catch_5
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_8
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_12
    if-eqz v9, :cond_13

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_13
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_14
    :try_start_13
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {v8}, Lorg/apache/http/client/HttpResponseException;->printStackTrace()V

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpResponseException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-virtual {v8}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v2

    invoke-virtual {v8}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, v8}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_16
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    :cond_17
    :try_start_14
    instance-of v1, v8, Ljava/lang/RuntimeException;

    if-eqz v1, :cond_1a

    const-string v1, "HttpTransaction"

    const-string v2, "ERROR"

    invoke-static {v1, v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_18
    :goto_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v8}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v1, :cond_19

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_19
    if-eqz v9, :cond_0

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_0

    :cond_1a
    :try_start_15
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v3}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1

    goto :goto_d

    :catchall_1
    move-exception v1

    :goto_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    :cond_1b
    if-eqz v9, :cond_1c

    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_1c
    throw v1

    :catch_9
    move-exception v2

    goto/16 :goto_a

    :catch_a
    move-exception v1

    goto/16 :goto_8

    :catch_b
    move-exception v2

    goto/16 :goto_b

    :catchall_2
    move-exception v1

    move-object v9, v10

    goto :goto_e

    :catch_c
    move-exception v8

    move-object v9, v10

    goto/16 :goto_c

    :catch_d
    move-exception v8

    move-object v9, v10

    goto/16 :goto_4

    :catch_e
    move-exception v2

    move-object/from16 v18, v2

    move-object v2, v1

    move-object/from16 v1, v18

    goto/16 :goto_9

    :cond_1d
    move-object v2, v1

    goto/16 :goto_6

    :cond_1e
    move-object v9, v10

    goto/16 :goto_3
.end method

.method public final isAborted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->isAborted()Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onStartResultProcessing()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onStartResultProcessing()V

    :cond_0
    return-void
.end method

.method final printHeaders()V
    .locals 7

    const-string v5, "HttpTransaction"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "HTTP headers:\n"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpRequestBase;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v2, v0, v3

    const-string v5, "Authorization"

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "Authorization: <removed>"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    const-string v5, "HttpTransaction"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method public final process(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .locals 13
    .param p1    # Lorg/apache/http/HttpRequest;
    .param p2    # Lorg/apache/http/protocol/HttpContext;

    const-string v11, "http.cookiespec-registry"

    invoke-interface {p2, v11}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/http/cookie/CookieSpecRegistry;

    const-string v11, "http.target_host"

    invoke-interface {p2, v11}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/http/HttpHost;

    const-string v11, "http.connection"

    invoke-interface {p2, v11}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/conn/ManagedClientConnection;

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    invoke-static {v11}, Lorg/apache/http/client/params/HttpClientParams;->getCookiePolicy(Lorg/apache/http/params/HttpParams;)Ljava/lang/String;

    move-result-object v5

    move-object v11, p1

    check-cast v11, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v11}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v8

    invoke-virtual {v10}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v6

    if-gez v6, :cond_0

    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->getRemotePort()I

    move-result v6

    :cond_0
    invoke-virtual {v8}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->isSecure()Z

    move-result v9

    new-instance v1, Lorg/apache/http/cookie/CookieOrigin;

    invoke-direct {v1, v3, v6, v4, v9}, Lorg/apache/http/cookie/CookieOrigin;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    invoke-virtual {v7, v5, v11}, Lorg/apache/http/cookie/CookieSpecRegistry;->getCookieSpec(Ljava/lang/String;Lorg/apache/http/params/HttpParams;)Lorg/apache/http/cookie/CookieSpec;

    move-result-object v2

    const-string v11, "http.cookie-spec"

    invoke-interface {p2, v11, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v11, "http.cookie-origin"

    invoke-interface {p2, v11, v1}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->getMetrics()Lorg/apache/http/HttpConnectionMetrics;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->setConnectionMetrics(Lorg/apache/http/HttpConnectionMetrics;)V

    :cond_1
    return-void
.end method

.method public final process(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .locals 4
    .param p1    # Lorg/apache/http/HttpResponse;
    .param p2    # Lorg/apache/http/protocol/HttpContext;

    const-string v3, "http.cookie-spec"

    invoke-interface {p2, v3}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/cookie/CookieSpec;

    const-string v3, "http.cookie-origin"

    invoke-interface {p2, v3}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/CookieOrigin;

    const-string v3, "Set-Cookie"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->headerIterator(Ljava/lang/String;)Lorg/apache/http/HeaderIterator;

    move-result-object v2

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->processCookies(Lorg/apache/http/HeaderIterator;Lorg/apache/http/cookie/CookieSpec;Lorg/apache/http/cookie/CookieOrigin;)V

    invoke-interface {v1}, Lorg/apache/http/cookie/CookieSpec;->getVersion()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "Set-Cookie2"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->headerIterator(Ljava/lang/String;)Lorg/apache/http/HeaderIterator;

    move-result-object v2

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->processCookies(Lorg/apache/http/HeaderIterator;Lorg/apache/http/cookie/CookieSpec;Lorg/apache/http/cookie/CookieOrigin;)V

    :cond_0
    return-void
.end method

.method public final setHttpTransactionMetrics(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    return-void
.end method
