.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;
.super Lvedroid/support/v4/content/AsyncTaskLoader;
.source "InstantUploadSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SystemSettingLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

.field private mLoaderException:Z

.field private final mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0, p1}, Lvedroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Lvedroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;)Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    return-object v0
.end method

.method private esLoadInBackground()Ljava/util/Map;
    .locals 27
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    const/4 v13, 0x0

    const/16 v25, 0x1

    const/16 v26, 0x1

    const/16 v19, 0x0

    const/4 v15, 0x1

    const/16 v23, 0x1

    const/16 v16, -0x1

    const/16 v17, -0x1

    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v8, "account"

    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->PROJECTION_PICASA_SETTINGS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1700()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    if-eqz v14, :cond_1

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const/4 v6, 0x1

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const/4 v6, 0x2

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/4 v6, 0x3

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const/4 v6, 0x4

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    const/4 v6, 0x5

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    const/4 v6, 0x6

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const/4 v6, 0x7

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v17

    :cond_0
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_1
    const-string v6, "auto_upload_enabled"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "sync_on_wifi_only"

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "sync_on_roaming"

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "sync_on_battery"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "video_upload_wifi_only"

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "upload_full_resolution"

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "quota_limit"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "quota_used"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move-object/from16 v0, v20

    invoke-interface {v0, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {v6}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Landroid/net/Uri;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v4

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    :try_start_1
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const/4 v6, 0x2

    move-object/from16 v0, v24

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/4 v6, 0x3

    move-object/from16 v0, v24

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v21

    :goto_0
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-object v6, v6, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mHandler:Landroid/os/Handler;

    new-instance v8, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader$1;

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v18

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader$1;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;III)V

    invoke-virtual {v6, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v20

    :catchall_0
    move-exception v6

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v6

    :cond_2
    const/16 v18, -0x1

    const/16 v22, -0x1

    const/16 v21, -0x1

    goto :goto_0

    :catchall_1
    move-exception v6

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    throw v6
.end method

.method private loadInBackground()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mLoaderException:Z

    if-nez v1, :cond_0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->esLoadInBackground()Ljava/util/Map;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "EsAsyncTaskLoader"

    const-string v2, "loadInBackground failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mLoaderException:Z

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mLoaderException:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lvedroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mActivity:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$2200(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->loadInBackground()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected final onAbandon()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onReset()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->onAbandon()V

    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->mObserverRegistered:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;->forceLoad()V

    return-void
.end method
