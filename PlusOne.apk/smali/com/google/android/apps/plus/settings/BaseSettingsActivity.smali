.class public Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "BaseSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;
    }
.end annotation


# static fields
.field private static final VIEW:Lcom/google/android/apps/plus/analytics/OzViews;


# instance fields
.field protected mHandler:Landroid/os/Handler;

.field protected mMasterSwitch:Landroid/widget/Switch;

.field protected mViewNavigationRecorded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    sput-object v0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordReverseViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->finish()V

    return-void
.end method

.method protected getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    instance-of v2, v0, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method protected final getAccountFromIntent()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "account"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_5

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "account"

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    move-object v0, v2

    :goto_0
    instance-of v3, v0, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    :cond_2
    :goto_1
    return-object v0

    :cond_3
    instance-of v3, v0, Landroid/accounts/Account;

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "account"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_0
.end method

.method protected final getMasterSwitch()Landroid/widget/Switch;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mMasterSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method protected final getRingtoneName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    if-nez p1, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_settings_silent_ringtone:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p0, v2}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v1, p0}, Landroid/media/Ringtone;->getTitle(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected final hookMasterSwitch(Landroid/preference/PreferenceCategory;Landroid/preference/CheckBoxPreference;)V
    .locals 7
    .param p1    # Landroid/preference/PreferenceCategory;
    .param p2    # Landroid/preference/CheckBoxPreference;

    const/16 v6, 0x10

    const/4 v5, -0x2

    const/4 v4, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onIsHidingHeaders()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onIsMultiPane()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Landroid/widget/Switch;

    invoke-direct {v2, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mMasterSwitch:Landroid/widget/Switch;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->action_bar_switch_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mMasterSwitch:Landroid/widget/Switch;

    invoke-virtual {v2, v4, v4, v1, v4}, Landroid/widget/Switch;->setPadding(IIII)V

    invoke-virtual {v0, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mMasterSwitch:Landroid/widget/Switch;

    new-instance v3, Landroid/app/ActionBar$LayoutParams;

    const/16 v4, 0x15

    invoke-direct {v3, v5, v5, v4}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v2, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mMasterSwitch:Landroid/widget/Switch;

    new-instance v3, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$1;

    invoke-direct {v3, p0, p2}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/BaseSettingsActivity;Landroid/preference/CheckBoxPreference;)V

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mMasterSwitch:Landroid/widget/Switch;

    invoke-virtual {p2}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    new-instance v2, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mHandler:Landroid/os/Handler;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    if-eqz p1, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mViewNavigationRecorded:Z

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mViewNavigationRecorded:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->mViewNavigationRecorded:Z

    :cond_0
    return-void
.end method

.method protected final putAccountExtra(Landroid/preference/PreferenceGroup;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p1    # Landroid/preference/PreferenceGroup;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_3

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v3}, Landroid/preference/Preference;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_0

    const-string v4, "account"

    invoke-virtual {v1, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    instance-of v4, v3, Landroid/preference/PreferenceGroup;

    if-eqz v4, :cond_1

    check-cast v3, Landroid/preference/PreferenceGroup;

    invoke-virtual {p0, v3, p2}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->putAccountExtra(Landroid/preference/PreferenceGroup;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final startExternalActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/high16 v0, 0x80000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
