.class public Lcom/google/android/apps/plus/settings/SettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "SettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;,
        Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsSyncPreferenceChangeListener;
    }
.end annotation


# static fields
.field private static sContactsStatsSyncKey:Ljava/lang/String;

.field private static sContactsSyncKey:Ljava/lang/String;

.field private static sDeviceLocationKey:Ljava/lang/String;

.field private static sHangoutKey:Ljava/lang/String;

.field private static sInstantUploadKey:Ljava/lang/String;

.field private static sNotificationsKey:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    return-void
.end method

.method private removePreference(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    sget v3, Lcom/google/android/apps/plus/R$string;->home_menu_settings:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsKey:Ljava/lang/String;

    if-nez v3, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$string;->communication_preference_notifications_key:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sNotificationsKey:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->communication_preference_hangout_key:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sHangoutKey:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_preference_instant_upload_key:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->contacts_sync_preference_key:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsSyncKey:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_key:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsStatsSyncKey:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->communication_preference_device_location_key:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sDeviceLocationKey:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getAccountFromIntent()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_1

    sget v3, Lcom/google/android/apps/plus/R$string;->not_signed_in:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v3

    if-eqz v3, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$xml;->main_preferences_plus_page:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->addPreferencesFromResource(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->putAccountExtra(Landroid/preference/PreferenceGroup;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_0

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$xml;->main_preferences:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->addPreferencesFromResource(I)V

    sget v3, Lcom/google/android/apps/plus/R$xml;->contacts_sync_preferences:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->addPreferencesFromResource(I)V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v3, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsSyncPreferenceChangeListener;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsSyncPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v2, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :cond_3
    sget-object v3, Lcom/google/android/apps/plus/settings/SettingsActivity;->sContactsStatsSyncKey:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->hasTelephony(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget v3, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_phone_summary:I

    :goto_2
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v3, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity$ContactsStatsSyncPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_1

    :cond_4
    sget v3, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_tablet_summary:I

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->preferences_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$id;->menu_help:I

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->url_param_help_settings:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->startExternalActivity(Landroid/content/Intent;)V

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/settings/LabelPreference;

    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/settings/LabelPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/settings/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v0, v1, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sHangoutKey:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->removePreference(Ljava/lang/String;)V

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/settings/SettingsActivity;->sDeviceLocationKey:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/SettingsActivity;->removePreference(Ljava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    if-eqz v2, :cond_5

    sget v1, Lcom/google/android/apps/plus/R$string;->es_google_iu_provider:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_sync_disabled_summary:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/settings/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_1
    new-instance v1, Lcom/google/android/apps/plus/settings/SettingsActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/settings/SettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/SettingsActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_0

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$string;->master_sync_disabled_summary:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/LabelPreference;->setSummary(I)V

    goto :goto_1
.end method
