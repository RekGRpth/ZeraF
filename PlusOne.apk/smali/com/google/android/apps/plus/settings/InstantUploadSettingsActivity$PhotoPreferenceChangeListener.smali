.class final Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;
.super Ljava/lang/Object;
.source "InstantUploadSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhotoPreferenceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 16
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    invoke-virtual/range {p1 .. p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sInstantUploadKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$700()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    move-object/from16 v2, p2

    check-cast v2, Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->updateEnabledStates(Z)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$800(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-eqz v12, :cond_1

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v13, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$1;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v1, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$1;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Boolean;)V

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_1
    const/4 v12, 0x1

    return v12

    :cond_1
    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0

    :cond_2
    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1000()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    move-object/from16 v2, p2

    check-cast v2, Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-eqz v12, :cond_3

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_2
    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v13, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$2;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$2;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Ljava/lang/Boolean;)V

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    :cond_3
    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_2

    :cond_4
    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sOnBatteryKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1100()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_5

    move-object/from16 v2, p2

    check-cast v2, Ljava/lang/Boolean;

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$3;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$3;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Ljava/lang/Boolean;)V

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    :cond_5
    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1200()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_8

    move-object/from16 v9, p2

    check-cast v9, Ljava/lang/String;

    const-string v12, "WIFI_ONLY"

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_connection_preference_summary_wifi:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    :goto_3
    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$4;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v1, v11}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$4;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Boolean;)V

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    :cond_6
    const-string v12, "WIFI_OR_MOBILE"

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_7

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_connection_preference_summary_mobile:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    goto :goto_3

    :cond_7
    const/4 v11, 0x0

    goto :goto_3

    :cond_8
    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1300()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_b

    move-object/from16 v9, p2

    check-cast v9, Ljava/lang/String;

    const-string v12, "WIFI_ONLY"

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v12, Lcom/google/android/apps/plus/R$string;->video_connection_preference_summary_wifi:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    :goto_4
    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$5;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v1, v11}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$5;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Boolean;)V

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    :cond_9
    const-string v12, "WIFI_OR_MOBILE"

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_a

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v12, Lcom/google/android/apps/plus/R$string;->video_connection_preference_summary_mobile:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    goto :goto_4

    :cond_a
    const/4 v11, 0x0

    goto :goto_4

    :cond_b
    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sUploadSizeKey:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1400()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_0

    move-object/from16 v9, p2

    check-cast v9, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I
    invoke-static {v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1500(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_c

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I
    invoke-static {v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_c

    const/4 v6, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I
    invoke-static {v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1500(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I

    move-result v13

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/phone/InstantUpload;->getSizeText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I
    invoke-static {v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1500(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I
    invoke-static {v14}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$1600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I

    move-result v14

    sub-int/2addr v13, v14

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/phone/InstantUpload;->getSizeText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v13, Lcom/google/android/apps/plus/R$string;->photo_upload_size_quota_available:I

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    const/4 v15, 0x1

    aput-object v8, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :goto_6
    const-string v12, "FULL"

    invoke-static {v9, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_FULL_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v13, Lcom/google/android/apps/plus/R$string;->photo_upload_size_preference_summary_full:I

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v5, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    :goto_7
    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$6;

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v3}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$6;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;Ljava/lang/Boolean;)V

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener$6;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    :cond_c
    const/4 v6, 0x0

    goto :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget v13, Lcom/google/android/apps/plus/R$string;->photo_upload_size_quota_unknown:I

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_6

    :cond_e
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;->this$0:Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_STANDARD_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

    # invokes: Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v12, v13}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_upload_size_preference_summary_standard:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/preference/Preference;->setSummary(I)V

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_7
.end method
