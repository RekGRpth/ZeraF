.class final Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "NotificationSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChangeNotificationsRequestComplete$6a63df5(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    sget v1, Lcom/google/android/apps/plus/R$string;->notification_settings_save_failed:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public final onGetNotificationSettings$434dcfc8(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/NotificationSettingsData;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    # getter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$002(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    # setter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$102(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Lcom/google/android/apps/plus/content/NotificationSettingsData;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    # invokes: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$200(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    const v1, 0x7f0a003f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->dismissDialog(I)V

    :cond_0
    return-void
.end method
