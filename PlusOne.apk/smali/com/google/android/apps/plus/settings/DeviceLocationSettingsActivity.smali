.class public Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "DeviceLocationSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static sBestAvailableManageAclKey:Ljava/lang/String;

.field private static sLearnMoreKey:Ljava/lang/String;

.field private static sLocationReportingSettingsKey:Ljava/lang/String;

.field private static sLocationSharingEnabledKey:Ljava/lang/String;

.field private static sNoNetworkKey:Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mCalledFromExternalApp:Z

.field private mGetSettingsRequestId:Ljava/lang/Integer;

.field private mIsLocationSharingEnabled:Z

.field private mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mLastSavedLocationSharingEnabled:Z

.field private mSaveSettingsRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSettingsLoaded:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 11
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    const/4 v5, 0x0

    const/4 v6, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v0, Lcom/google/android/apps/plus/R$string;->device_location_acl_picker_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v4, 0x5

    move-object v0, p0

    move v7, v6

    move v8, v5

    move v9, v5

    move v10, v6

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->loadSettings()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->updateLastSavedSettings()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->handleSaveComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->saveSettings()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->logAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method private handleSaveComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->save_settings_error:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->revertToLastSaved()V

    :goto_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->dismissDialog(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->updateLastSavedSettings()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->onSettingsSaved(ZLcom/google/android/apps/plus/content/AudienceData;)V

    goto :goto_1
.end method

.method private loadSettings()V
    .locals 4

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-static {}, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->getRecentlySavedSettings()Lcom/google/android/apps/plus/content/DeviceLocationSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "DeviceLocationSettings"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "DeviceLocationSettings"

    const-string v2, "loadSettings - recently saved"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->isLocationSharingEnabled:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    iget-object v1, v0, Lcom/google/android/apps/plus/content/DeviceLocationSettings;->bestAvailableLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->updateLastSavedSettings()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V

    :goto_0
    return-void

    :cond_1
    const-string v1, "DeviceLocationSettings"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "DeviceLocationSettings"

    const-string v2, "loadSettings - from server"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->showDialog(I)V

    goto :goto_0
.end method

.method private logAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    return-void
.end method

.method private revertToLastSaved()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V

    return-void
.end method

.method private saveSettings()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/EsService;->saveLocationSharingSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZILcom/google/android/apps/plus/content/AudienceData;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->showDialog(I)V

    return-void
.end method

.method private setupPreferences()V
    .locals 20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    if-eqz v10, :cond_9

    sget v10, Lcom/google/android/apps/plus/R$xml;->device_location_preferences:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->addPreferencesFromResource(I)V

    sget-object v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLearnMoreKey:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    new-instance v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;-><init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    invoke-virtual {v2, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLocationSharingEnabledKey:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v5}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->hookMasterSwitch(Landroid/preference/PreferenceCategory;Landroid/preference/CheckBoxPreference;)V

    new-instance v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$3;-><init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    invoke-virtual {v5, v10}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sBestAvailableManageAclKey:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    new-instance v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$4;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$4;-><init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    invoke-virtual {v1, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v10, :cond_7

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PeopleUtils;->isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v10

    if-nez v10, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v10, 0x104000e

    invoke-virtual {v15, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    const/4 v12, 0x0

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v10, 0x0

    move v13, v10

    :goto_0
    move/from16 v0, v18

    if-ge v13, v0, :cond_2

    aget-object v10, v17, v13

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    :goto_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v19, 0x2

    move/from16 v0, v19

    if-le v10, v0, :cond_a

    const/4 v10, 0x1

    :goto_2
    add-int/lit8 v12, v13, 0x1

    move v13, v12

    move v12, v10

    goto :goto_0

    :cond_1
    move-object v10, v11

    goto :goto_1

    :cond_2
    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v18, v0

    const/4 v10, 0x0

    move v14, v10

    :goto_3
    move/from16 v0, v18

    if-ge v14, v0, :cond_5

    aget-object v13, v17, v14

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_3

    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v14, 0x1

    move v14, v10

    goto :goto_3

    :cond_3
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    move-object v10, v13

    goto :goto_4

    :cond_4
    move-object v10, v11

    goto :goto_4

    :cond_5
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    if-eqz v12, :cond_6

    sget v10, Lcom/google/android/apps/plus/R$string;->device_location_more_than_three_no_count:I

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v15, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    :goto_5
    invoke-virtual {v1, v9}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLocationReportingSettingsKey:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GmsLocationReportingUtils;->getLocationReportingSettingsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_8

    sget v10, Lcom/google/android/apps/plus/R$string;->location_reporting_settings_summary:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    new-instance v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v4}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;-><init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Landroid/content/Intent;)V

    invoke-virtual {v3, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->updateEnabledStates()V

    return-void

    :pswitch_0
    const-string v9, ""

    goto :goto_5

    :pswitch_1
    const/4 v10, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object v9, v10

    goto :goto_5

    :pswitch_2
    sget v10, Lcom/google/android/apps/plus/R$string;->device_location_two_acl_entries:I

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v15, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    :pswitch_3
    sget v10, Lcom/google/android/apps/plus/R$string;->device_location_three_acl_entries:I

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    const/4 v13, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v15, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :cond_6
    sget v10, Lcom/google/android/apps/plus/R$string;->device_location_more_than_three_with_count:I

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v13

    add-int/lit8 v13, v13, -0x2

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v15, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :cond_7
    sget v10, Lcom/google/android/apps/plus/R$string;->device_location_acl_no_one:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_5

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto/16 :goto_6

    :cond_9
    sget v10, Lcom/google/android/apps/plus/R$xml;->device_location_preferences_no_network:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->addPreferencesFromResource(I)V

    sget-object v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sNoNetworkKey:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    new-instance v10, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$6;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$6;-><init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V

    invoke-virtual {v6, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_6

    :cond_a
    move v10, v12

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateEnabledStates()V
    .locals 3

    sget-object v2, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sBestAvailableManageAclKey:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLocationReportingSettingsKey:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_1
    return-void
.end method

.method private updateLastSavedSettings()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mCalledFromExternalApp:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->updateEnabledStates()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-eqz p3, :cond_0

    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->saveSettings()V

    const-string v0, "DeviceLocationSettings"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "DeviceLocationSettings"

    const-string v1, "Chose audience - saveSettings"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SET_CURRENT_LOCATION_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->logAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x3

    packed-switch p2, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->dismissDialog(I)V

    return-void

    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->saveSettings()V

    const-string v0, "DeviceLocationSettings"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DeviceLocationSettings"

    const-string v1, "Locating sharing enabled - saveSettings"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->logAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->revertToLastSaved()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.LOCATION_PLUS_SETTINGS"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mCalledFromExternalApp:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mCalledFromExternalApp:Z

    if-eqz v1, :cond_e

    const-string v1, "version"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v4, :cond_0

    instance-of v5, v1, Landroid/accounts/Account;

    if-nez v5, :cond_6

    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    move v1, v2

    :goto_0
    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLocationSharingEnabledKey:Ljava/lang/String;

    if-nez v1, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$string;->device_location_enable_sharing_key:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLocationSharingEnabledKey:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->device_location_best_available_manage_acl_key:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sBestAvailableManageAclKey:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->device_location_no_network_key:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sNoNetworkKey:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->device_location_learn_more_preference_key:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLearnMoreKey:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->device_location_reporting_settings_key:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->sLocationReportingSettingsKey:Ljava/lang/String;

    :cond_2
    if-eqz p1, :cond_5

    const-string v1, "get_settings_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "get_settings_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    :cond_3
    const-string v1, "save_settings_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "save_settings_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    :cond_4
    const-string v1, "settings_loaded"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    if-eqz v1, :cond_5

    const-string v1, "location_sharing_enabled"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    const-string v1, "best_location_audience"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    const-string v1, "saved_location_sharing_enabled"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    const-string v1, "saved_best_location_audience"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_5
    :goto_1
    return-void

    :cond_6
    if-le v4, v3, :cond_7

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_8

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_9

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_9
    check-cast v1, Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_a
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_b
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v1

    if-nez v1, :cond_c

    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setResult(I)V

    move v1, v2

    goto/16 :goto_0

    :cond_c
    move v1, v3

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->finish()V

    goto :goto_1

    :cond_e
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_LOCATION_SHARING:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v1

    if-nez v1, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->finish()V

    goto :goto_1

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getAccountFromIntent()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_10

    sget v1, Lcom/google/android/apps/plus/R$string;->not_signed_in:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->finish()V

    goto/16 :goto_1

    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->finish()V

    goto/16 :goto_1
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->circle_settings_saving:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->device_location_enable_sharing_dialog_title:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->device_location_legal_summary:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->handleSaveComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->dismissDialog(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->loadSettings()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "get_settings_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mGetSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "save_settings_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSaveSettingsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_2

    const-string v0, "best_location_audience"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    const-string v0, "settings_loaded"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mSettingsLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "location_sharing_enabled"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mIsLocationSharingEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_3

    const-string v0, "saved_best_location_audience"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedBestLocationAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    const-string v0, "saved_location_sharing_enabled"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->mLastSavedLocationSharingEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
