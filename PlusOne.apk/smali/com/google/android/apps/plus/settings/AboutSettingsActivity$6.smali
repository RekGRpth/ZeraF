.class final Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;
.super Ljava/lang/Object;
.source "AboutSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 7
    .param p1    # Landroid/preference/Preference;

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v0, v3, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    # getter for: Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->TERMS:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->access$100()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->startExternalActivity(Landroid/content/Intent;)V

    const/4 v3, 0x1

    return v3
.end method
