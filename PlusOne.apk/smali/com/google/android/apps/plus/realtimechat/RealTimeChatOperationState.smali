.class public final Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;
.super Ljava/lang/Object;
.source "RealTimeChatOperationState.java"


# instance fields
.field private mClientVersion:I

.field private mClientVersionChanged:Z

.field private final mCurrentConversationRowId:Ljava/lang/Long;

.field private final mRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mShouldTriggerNotification:Z


# direct methods
.method public constructor <init>(Ljava/lang/Long;)V
    .locals 2
    .param p1    # Ljava/lang/Long;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mCurrentConversationRowId:Ljava/lang/Long;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mRequests:Ljava/util/List;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mShouldTriggerNotification:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mClientVersionChanged:Z

    iput v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mClientVersion:I

    return-void
.end method


# virtual methods
.method public final addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mRequests:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final getClientVersionChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mClientVersionChanged:Z

    return v0
.end method

.method public final getCurrentConversationRowId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mCurrentConversationRowId:Ljava/lang/Long;

    return-object v0
.end method

.method public final getRequests()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mRequests:Ljava/util/List;

    return-object v0
.end method

.method public final setClientVersion(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mClientVersion:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mClientVersionChanged:Z

    return-void
.end method

.method public final setShouldTriggerNotifications()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mShouldTriggerNotification:Z

    return-void
.end method

.method public final shouldTriggerNotifications()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->mShouldTriggerNotification:Z

    return v0
.end method
