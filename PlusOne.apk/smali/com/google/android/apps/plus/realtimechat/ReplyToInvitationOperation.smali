.class public final Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "ReplyToInvitationOperation.java"


# instance fields
.field private mAccept:Z

.field private mConversationRowId:J

.field private mInviterId:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mConversationRowId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mInviterId:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mAccept:Z

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 6

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mAccept:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mInviterId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->acceptConversationLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mInviterId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->removeAllConversationsFromInviterLocally$37a126b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0
.end method
