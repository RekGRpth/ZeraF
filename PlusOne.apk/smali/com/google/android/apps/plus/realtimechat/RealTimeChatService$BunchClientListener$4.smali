.class final Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;
.super Ljava/lang/Object;
.source "RealTimeChatService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;->onResultsReceived(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;

.field final synthetic val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

.field final synthetic val$results:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;Ljava/util/List;Lcom/google/android/apps/plus/realtimechat/BunchClient;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->this$1:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;

    iput-object p2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$results:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$results:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getRequestId()I

    move-result v4

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$400()Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->removeRequest(Ljava/lang/Object;)V

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$700()Ljava/util/Map;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPingResponse()Z

    move-result v6

    if-nez v6, :cond_2

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$600()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;->onResponseReceived$1587694a(ILcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "op"

    const/16 v7, 0xdf

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "account"

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationListResponse()Z

    move-result v6

    if-eqz v6, :cond_0

    const-class v7, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    monitor-enter v7

    const/4 v6, 0x1

    :try_start_0
    # setter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z
    invoke-static {v6}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$902(Z)Z

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v6, "RealTimeChatService"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "RealTimeChatService"

    const-string v7, "conversations loaded"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v2, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v2, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "op"

    const/16 v7, 0xe3

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "account"

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$600()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;->val$client:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;->onConversationsLoaded$abe99c5()V

    goto :goto_1

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_4
    return-void
.end method
