.class public final Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "SendTypingRequestOperation.java"


# instance fields
.field final mConversationRowId:J

.field final mTypingType:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mConversationRowId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mTypingType:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mTypingType:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->sendTypingRequestLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    return-void
.end method
