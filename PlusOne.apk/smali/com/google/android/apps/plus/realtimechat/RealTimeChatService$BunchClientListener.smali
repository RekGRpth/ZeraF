.class final Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;
.super Ljava/lang/Object;
.source "RealTimeChatService.java"

# interfaces
.implements Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BunchClientListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    return-void
.end method


# virtual methods
.method public final onConnected(Lcom/google/android/apps/plus/realtimechat/BunchClient;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    const-string v1, "onConnected "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$800(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$1;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;Lcom/google/android/apps/plus/realtimechat/BunchClient;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onDisconnected(Lcom/google/android/apps/plus/realtimechat/BunchClient;I)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .param p2    # I

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "disconnected reason "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    # setter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$902(Z)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$800(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$3;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;Lcom/google/android/apps/plus/realtimechat/BunchClient;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onPingReceived(Lcom/google/android/apps/plus/realtimechat/BunchClient;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    const-string v1, "ping received"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$800(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$2;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;Lcom/google/android/apps/plus/realtimechat/BunchClient;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final onResultsReceived(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/util/List;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/realtimechat/BunchClient;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$800(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener$4;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;Ljava/util/List;Lcom/google/android/apps/plus/realtimechat/BunchClient;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
