.class public final Lcom/google/android/apps/plus/realtimechat/SetAclOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "SetAclOperation.java"


# instance fields
.field private mAcl:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput p3, p0, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;->mAcl:I

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 2

    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;->mAcl:I

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->setAcl(I)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    return-void
.end method
