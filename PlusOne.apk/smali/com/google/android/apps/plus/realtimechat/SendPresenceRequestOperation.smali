.class public final Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "SendPresenceRequestOperation.java"


# instance fields
.field final mConversationRowId:J

.field final mIsPresent:Z

.field final mReciprocate:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Z
    .param p6    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mConversationRowId:J

    iput-boolean p5, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mIsPresent:Z

    iput-boolean p6, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mReciprocate:Z

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 7

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mConversationRowId:J

    iget-boolean v4, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mIsPresent:Z

    iget-boolean v5, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mReciprocate:Z

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsConversationsData;->sendPresenceRequestLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    return-void
.end method
