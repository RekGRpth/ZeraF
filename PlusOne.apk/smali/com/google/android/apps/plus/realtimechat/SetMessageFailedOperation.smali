.class public final Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "SetMessageFailedOperation.java"


# instance fields
.field mConversationRowId:J

.field mMessageRowId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;->mConversationRowId:J

    iput-wide p5, p0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;->mMessageRowId:J

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;->mMessageRowId:J

    const/16 v4, 0x8

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->setMessageStatusLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)V

    return-void
.end method
