.class final Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
.source "BlockingC2DMClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OnC2dmReceivedListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;->this$0:Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;-><init>(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;)V

    return-void
.end method


# virtual methods
.method final onC2dmRegistration(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;->this$0:Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    # setter for: Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->access$102(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;Lcom/google/android/apps/plus/service/ServiceResult;)Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;->this$0:Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    # setter for: Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRegistrationToken:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->access$202(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;->this$0:Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    # getter for: Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mEvent:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->access$300(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method
