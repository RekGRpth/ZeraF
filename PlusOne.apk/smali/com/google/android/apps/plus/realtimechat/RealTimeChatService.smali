.class public Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
.super Landroid/app/Service;
.source "RealTimeChatService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ResultsLinkedHashMap;
    }
.end annotation


# static fields
.field private static sConversationsLoaded:Z

.field private static sCurrentConversationRowId:Ljava/lang/Long;

.field private static sLastRequestId:Ljava/lang/Integer;

.field private static final sListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;",
            ">;"
        }
    .end annotation
.end field

.field private static sWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

.field private final mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

.field private mConnectionRequestCount:I

.field private mHandler:Landroid/os/Handler;

.field private mLastConnectAttemptTime:J

.field private mLastConnectRequestTimestamp:J

.field private mLastMessageTime:J

.field private mLastResponseTime:J

.field private mLongTermConnect:Landroid/app/PendingIntent;

.field private mNeedsSync:Z

.field private final mPingRunnable:Ljava/lang/Runnable;

.field private mReconnectCount:I

.field private mReconnectDelay:J

.field private mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

.field private final mStopRunnable:Ljava/lang/Runnable;

.field private final mTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    invoke-direct {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ResultsLinkedHashMap;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ResultsLinkedHashMap;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sCurrentConversationRowId:Ljava/lang/Long;

    sput-boolean v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectAttemptTime:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$2;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$3;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Intent;)V
    .locals 10
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "op"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v0, "RealTimeChatService"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "ProcessIntent OpCode "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " requestId "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "rid"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->active()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->active()Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v3

    :goto_1
    if-eqz v4, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    :goto_2
    sparse-switch v5, :sswitch_data_0

    if-nez v3, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_4a

    if-eqz v2, :cond_4a

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_1
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    sparse-switch v5, :sswitch_data_1

    :cond_2
    :goto_4
    return-void

    :cond_3
    move v0, v8

    goto :goto_0

    :cond_4
    move v4, v8

    goto :goto_1

    :cond_5
    move v3, v8

    goto :goto_2

    :sswitch_0
    :try_start_0
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v0

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "RealTimeChatService"

    const-string v2, "Exception in processIntent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    :sswitch_1
    :try_start_1
    const-string v0, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "RealTimeChatService"

    const-string v2, "connectIfLoggedIn "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    :cond_7
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "RealTimeChatService"

    const-string v1, "Requested to connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->active()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4

    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$4;

    invoke-direct {v5, p0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$4;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    if-nez v4, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v1, :cond_b

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_d

    :goto_5
    if-eqz v9, :cond_e

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "RealTimeChatService"

    const-string v1, "marking needs sync"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4

    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v9

    goto :goto_5

    :cond_e
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "requested connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_f
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "already connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :sswitch_2
    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "RealTimeChatService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "connectAndStayConnected "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    if-nez v3, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4

    :sswitch_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "RealTimeChatService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "connectAndStayConnected "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    if-nez v3, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4

    :sswitch_4
    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-lez v1, :cond_12

    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    :cond_12
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_13

    const-string v1, "RealTimeChatService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "allowDisconnect "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    if-nez v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_14
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "RealTimeChatService"

    const-string v1, "stopping service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_4

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_17
    if-nez v4, :cond_1e

    if-eqz v2, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    if-gtz v0, :cond_18

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-lez v0, :cond_1c

    :cond_18
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "RealTimeChatService"

    const-string v1, "connecting..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_1a

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectAttemptTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->connect()V

    :cond_1a
    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    if-lez v0, :cond_1b

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    :cond_1b
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4

    :cond_1c
    iget-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleLongTermConnect(Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    :cond_1e
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "already connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :sswitch_6
    if-nez v4, :cond_2

    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v2, :cond_21

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1f
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "Requested to connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_20
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_22

    :goto_6
    if-eqz v9, :cond_23

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    const-string v0, "reconnect_delay"

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4

    :cond_22
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v9

    goto :goto_6

    :cond_23
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "requested connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :sswitch_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    sub-long/2addr v0, v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long/2addr v5, v7

    if-eqz v4, :cond_2d

    const-string v3, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_24

    const-string v3, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "OP_PING "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v7, :cond_28

    :goto_7
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v3, :cond_25

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v3

    if-nez v3, :cond_26

    :cond_25
    iget-boolean v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v3, :cond_26

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-gtz v3, :cond_26

    const-wide/32 v3, 0xea60

    cmp-long v3, v5, v3

    if-gez v3, :cond_2a

    :cond_26
    const-wide/32 v3, 0xafc8

    cmp-long v0, v0, v3

    if-ltz v0, :cond_29

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27

    const-string v0, "RealTimeChatService"

    const-string v1, "too long since last response, restarting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_27
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect(I)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4

    :cond_28
    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_7

    :cond_29
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendKeepAlive()V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    :cond_2a
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2b

    const-string v0, "RealTimeChatService"

    const-string v1, "uneeded connection found in ping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_2c
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_4

    :cond_2d
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "connection is down, can\'t ping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :sswitch_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    goto/16 :goto_4

    :sswitch_9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    goto/16 :goto_4

    :sswitch_a
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2e

    const-string v0, "RealTimeChatService"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_2f

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getOrRequestC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendUserCreateRequest$74507863(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendC2DMIdToSessionServer(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->connectionStarted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_2f
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_30

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$5;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    :sswitch_b
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_31

    const-string v0, "RealTimeChatService"

    const-string v1, "onConversationsLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    goto/16 :goto_4

    :sswitch_c
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_32

    const-string v0, "RealTimeChatService"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_32
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long v3, v0, v3

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_33

    const-string v1, "RealTimeChatService"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "OP_ON_DISCONNECTED "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v0, :cond_39

    move-object v0, v9

    :goto_8
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_33
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v0

    if-nez v0, :cond_35

    :cond_34
    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v0, :cond_35

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-gtz v0, :cond_35

    const-wide/32 v0, 0xea60

    cmp-long v0, v3, v0

    if-gez v0, :cond_3c

    :cond_35
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_36

    const-string v0, "RealTimeChatService"

    const-string v1, "scheduling reconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_36
    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    if-gtz v0, :cond_37

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-lez v0, :cond_3a

    :cond_37
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_38
    :goto_9
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$6;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$6;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    :cond_39
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_8

    :cond_3a
    iget-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleLongTermConnect(Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_3b
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_9

    :cond_3c
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3d

    const-string v0, "RealTimeChatService"

    const-string v1, "no need to stay connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3d
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_3e
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3f

    const-string v0, "RealTimeChatService"

    const-string v1, "scheduling stop runnable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3f
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_38

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_9

    :sswitch_d
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_40

    const-string v0, "RealTimeChatService"

    const-string v1, "log out"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_40
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_42

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->connected()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v0

    const-string v3, "realtimechat"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "c2dm_registration_id"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_42

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->unregisterDevice$6995facd(J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const/4 v4, -0x1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const-string v0, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_41

    const-string v0, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unregister C2DM to session server: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_41
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v0, "phone"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<iq to=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\' type=\'set\'>  <dev:device-unregister xmlns:dev=\'google:devices\' "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "device-id=\'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' app-id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' /></iq>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->write(Ljava/lang/String;)Z

    :cond_42
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_43

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_43
    const-string v0, "realtimechat"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "c2dm_registration_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0}, Lcom/google/android/apps/plus/c2dm/C2DMReceiver;->unregisterC2DM(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_4

    :sswitch_e
    const-string v0, "registration"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "realtimechat"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "c2dm_registration_id"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v3, "sticky_c2dm_registration_id"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v3, "c2dm_registration_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    const-string v4, "c2dm_registration_build_version"

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v2, :cond_45

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->connected()Z

    move-result v2

    if-eqz v2, :cond_45

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendUserCreateRequest$74507863(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendC2DMIdToSessionServer(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_44
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$7;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    :cond_45
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v3, :cond_46

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v3

    if-nez v3, :cond_44

    :cond_46
    iget-boolean v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v3, :cond_44

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-nez v3, :cond_44

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-ltz v1, :cond_44

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_a

    :sswitch_f
    const-string v0, "realtimechat"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "c2dm_registration_id"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-eqz v4, :cond_47

    invoke-static {v1}, Lcom/google/android/apps/plus/c2dm/C2DMReceiver;->requestC2DMRegistrationId(Landroid/content/Context;)V

    goto/16 :goto_4

    :cond_47
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v2, :cond_48

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v2

    if-nez v2, :cond_49

    :cond_48
    iget-boolean v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v2, :cond_49

    iget v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-nez v2, :cond_49

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-ltz v0, :cond_49

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_49
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$8;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    :cond_4a
    const-string v0, "RealTimeChatService"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "RealTimeChatService"

    const-string v3, "action requested on inactive account"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :sswitch_10
    :try_start_2
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    const-string v3, "message_text"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;

    invoke-direct {v4, v1, v2, v0, v3}, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_4

    :catch_1
    move-exception v0

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4b

    const-string v1, "RealTimeChatService"

    const-string v2, "Exception in processIntent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4b
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    const-string v1, "rid"

    invoke-virtual {p1, v1, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    invoke-direct {p0, p1, v0, v9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_4

    :sswitch_11
    :try_start_3
    const-string v0, "conversation_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "message_text"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_12
    const-string v0, "conversation_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_13
    const-string v0, "conversation_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "message_row_id"

    const-wide/16 v5, -0x1

    invoke-virtual {p1, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_14
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "message_row_id"

    const-wide/16 v5, -0x1

    invoke-virtual {p1, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_15
    const-string v0, "message_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_16
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_17
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/LeaveConversationOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/LeaveConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_18
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/MarkConversationReadOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/MarkConversationReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_19
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/MarkConversationNotificationsSeenOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/MarkConversationNotificationsSeenOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_1a
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "message_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_1b
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "conversation_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_1c
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "conversation_muted"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v5, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->setMuted(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_1d
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "inviter_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "accept"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_1e
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RequestMoreEventsOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RequestMoreEventsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_1f
    const-string v0, "acl"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-instance v3, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_20
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "is_present"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v0, "reciprocate"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_21
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "typing_status"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_22
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "tile_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "tile_event_version"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "tile_event_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "tile_event_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_4

    :sswitch_23
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantListFromAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    :cond_4c
    const-string v0, "type"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getSuggestionsRequest(Ljava/util/Collection;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const-string v3, "rid"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    const-string v2, "rid"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_4

    :sswitch_24
    const-string v0, "message_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "flags"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResultValue()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_4

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_2
        0x6f -> :sswitch_3
        0x70 -> :sswitch_4
        0x71 -> :sswitch_e
        0x72 -> :sswitch_f
        0x73 -> :sswitch_1
        0x74 -> :sswitch_d
        0x75 -> :sswitch_f
        0xdc -> :sswitch_5
        0xdd -> :sswitch_7
        0xde -> :sswitch_9
        0xdf -> :sswitch_8
        0xe0 -> :sswitch_a
        0xe1 -> :sswitch_c
        0xe3 -> :sswitch_b
        0xe4 -> :sswitch_6
        0x154 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0xe2 -> :sswitch_24
        0x14a -> :sswitch_10
        0x14b -> :sswitch_11
        0x14c -> :sswitch_16
        0x14d -> :sswitch_17
        0x14f -> :sswitch_18
        0x150 -> :sswitch_1a
        0x151 -> :sswitch_1b
        0x152 -> :sswitch_1c
        0x155 -> :sswitch_1d
        0x156 -> :sswitch_1e
        0x157 -> :sswitch_1f
        0x158 -> :sswitch_15
        0x159 -> :sswitch_12
        0x15a -> :sswitch_13
        0x15b -> :sswitch_14
        0x15c -> :sswitch_20
        0x15d -> :sswitch_21
        0x15e -> :sswitch_19
        0x15f -> :sswitch_22
        0x160 -> :sswitch_23
    .end sparse-switch
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Lcom/google/android/apps/plus/realtimechat/BunchClient;)Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$300()Landroid/os/PowerManager$WakeLock;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$902(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z

    return p0
.end method

.method public static allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeChatService"

    const-string v2, "allowDisconnect"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x70

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static checkMessageSent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0xe2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "flags"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$9;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$9;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static connectAndStayConnected(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeChatService"

    const-string v2, "connectAndStayConnected"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "RealTimeChatService"

    const-string v2, "acquiring wake lock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x6e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static connectIfLoggedIn(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeChatService"

    const-string v2, "connectIfLoggedIn"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "RealTimeChatService"

    const-string v2, "acquiring wake lock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x73

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "message_timestamp"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static createConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p3    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "message_text"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static debuggable()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    return v0
.end method

.method private executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .param p2    # Landroid/content/Intent;
    .param p3    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;

    const/4 v6, 0x0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->execute()V

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResultCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v1, v6, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResultValue()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, p2, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResponses()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sending "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " responses"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v3, "rid"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p1, v0, v3, v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommands(Ljava/util/Collection;II)Z

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v5, 0x3a98

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-object v2
.end method

.method private static getBackendSetting(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0    # Landroid/content/Context;

    sget-boolean v3, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v3, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_backend_key:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->debug_realtimechat_default_backend:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    return-object v3

    :cond_0
    sget v3, Lcom/google/android/apps/plus/R$string;->realtimechat_default_backend:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getConversationsLoaded()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z

    return v0
.end method

.method public static getCurrentConversationRowId()Ljava/lang/Long;
    .locals 2

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sCurrentConversationRowId:Ljava/lang/Long;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static getOrRequestC2dmId(Landroid/content/Context;)Ljava/lang/String;
    .locals 14
    .param p0    # Landroid/content/Context;

    const/4 v12, 0x0

    const/4 v13, 0x3

    const/4 v10, 0x0

    const-string v9, "realtimechat"

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v9, "c2dm_registration_id"

    invoke-interface {v4, v9, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "c2dm_registration_time"

    const-wide/16 v10, 0x0

    invoke-interface {v4, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v9, "c2dm_registration_build_version"

    invoke-interface {v4, v9, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v0, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-eqz v5, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    :cond_0
    const-string v9, "RealTimeChatService"

    invoke-static {v9, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "RealTimeChatService"

    const-string v10, "refreshing registration for update"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    if-eqz v6, :cond_2

    const-string v9, "RealTimeChatService"

    const-string v10, "saving registration"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "sticky_c2dm_registration_id"

    invoke-interface {v2, v9, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_2
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v6, 0x0

    :cond_3
    :goto_1
    if-nez v6, :cond_4

    invoke-static {p0}, Lcom/google/android/apps/plus/c2dm/C2DMReceiver;->requestC2DMRegistrationId(Landroid/content/Context;)V

    :cond_4
    return-object v6

    :catch_0
    move-exception v1

    const-string v9, "RealTimeChatService"

    const/4 v10, 0x6

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string v9, "RealTimeChatService"

    const-string v10, "Can\'t find package information for current package, continuing anyway"

    invoke-static {v9, v10, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_5
    move-object v0, v5

    goto :goto_0

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    const-wide/32 v11, 0x2932e00

    cmp-long v9, v9, v11

    if-lez v9, :cond_3

    const-string v9, "RealTimeChatService"

    invoke-static {v9, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_7

    const-string v9, "RealTimeChatService"

    const-string v10, "refreshing registration for expiration"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    if-eqz v6, :cond_8

    const-string v9, "RealTimeChatService"

    const-string v10, "saving registration"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "sticky_c2dm_registration_id"

    invoke-interface {v2, v9, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_8
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static getStickyC2dmId(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x0

    const-string v3, "realtimechat"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "c2dm_registration_id"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "sticky_c2dm_registration_id"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_0
    return-object v0

    :cond_0
    const-string v3, "sticky_c2dm_registration_id"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static handleC2DMRegistration(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x71

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "registration"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static handleC2DMRegistrationError(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x75

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static handleC2DMUnregistration(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x72

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private static initWakeLock(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_0

    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x1

    const-string v2, "realtimechat"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_0
    return-void
.end method

.method private initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_1
    if-nez p1, :cond_3

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "action requested on null account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    return-void

    :cond_3
    new-instance v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;

    const/4 v0, 0x0

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;B)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getBackendSetting(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getBackendSetting(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "realtime-chat-dev@bot.talk.google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v4, "realtime-chat-dev@bot.talk.google.com"

    :goto_1
    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient;-><init>(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->updateClientVersion()V

    goto :goto_0

    :cond_4
    const-string v4, "realtime-chat@bot.talk.google.com"

    goto :goto_1
.end method

.method public static initiateConnection(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeChatService"

    const-string v2, "initiateConnection"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "RealTimeChatService"

    const-string v2, "acquiring wake lock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x6f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static inviteParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x14c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static isRequestPending(I)Z
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->requestPending(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static logOut(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x74

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static markConversationNotificationsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x15e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static markConversationRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x14f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static notifyUserPresenceChanged(JLjava/lang/String;Z)V
    .locals 2
    .param p0    # J
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;-><init>(JLjava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static notifyUserTypingStatusChanged(JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p0    # J
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$11;

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$11;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static onIntentProcessed(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V
    .locals 8
    .param p0    # Landroid/content/Intent;
    .param p1    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    .param p2    # Ljava/lang/Object;

    const/4 v6, -0x1

    const-string v5, "op"

    invoke-virtual {p0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v5, "rid"

    invoke-virtual {p0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v5, "account"

    invoke-virtual {p0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    sparse-switch v2, :sswitch_data_0

    :cond_0
    return-void

    :sswitch_0
    move-object v4, p2

    check-cast v4, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;

    const-string v5, "RealTimeChatService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v6, "RealTimeChatService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v5, "conversation created "

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v4, :cond_2

    const-string v5, "-1"

    :goto_0
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-virtual {v1, v3, v4, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;->onConversationCreated$2ae26fbd(ILcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V

    goto :goto_1

    :cond_2
    iget-object v5, v4, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;->mConversationRowId:Ljava/lang/Long;

    goto :goto_0

    :sswitch_1
    sget-object v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x14a -> :sswitch_0
        0x160 -> :sswitch_1
    .end sparse-switch
.end method

.method public static registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static removeMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x150

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    return-object v0
.end method

.method public static replyToInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x155

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "inviter_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "accept"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static requestMoreEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x156

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static requestSuggestedParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p3    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x160

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "type"

    invoke-virtual {p3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->getNumber()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static resetNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x154

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method public static retrySendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x158

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v8, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectAttemptTime:J

    const-wide/16 v6, 0x3a98

    add-long v2, v4, v6

    cmp-long v4, v2, v0

    if-gez v4, :cond_1

    const-string v4, "RealTimeChatService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "RealTimeChatService"

    const-string v5, "scheduling next connect attempt immediately"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    iput-object p1, v4, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_1
    const-string v4, "RealTimeChatService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "RealTimeChatService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "scheduling next connect attempt delayed "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    iput-object p1, v4, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    sub-long v6, v2, v0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private scheduleLongTermConnect(Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    const-string v6, "RealTimeChatService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "RealTimeChatService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "scheduling long term connect "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "op"

    const/16 v7, 0xe4

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "account_id"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-wide/16 v6, 0x2

    mul-long v2, p2, v6

    const-wide/32 v6, 0x6ddd00

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    const-wide/32 v2, 0x6ddd00

    :cond_1
    const-string v6, "reconnect_delay"

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v6, "alarm"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_2
    const/4 v6, 0x0

    const/high16 v7, 0x40000000

    invoke-static {p0, v6, v1, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long v4, v6, p2

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6, v4, v5, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method

.method private sendC2DMIdToSessionServer(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "RealTimeChatService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "RealTimeChatService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sendC2DMIdToSessionServer: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p2, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object v2, p2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lcom/google/android/apps/plus/network/ClientVersion;->from(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->hangout_default_device_name:I

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    sget-object v8, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget-object v9, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "<iq type=\'set\' to=\'"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'>  <dev:device-register device-id=\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' app-id=\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, "\' locale=\'"

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' name=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' xmlns:dev=\'google:devices\'>    <dev:version version=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' build-type=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' device-os-version=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' device-hardware=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'/>    <dev:android-resource registration-id=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'>      <dev:capability>com.google.hangout.RING</dev:capability>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "    </dev:android-resource> </dev:device-register>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "</iq>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->write(Ljava/lang/String;)Z

    goto/16 :goto_0
.end method

.method public static sendLocalPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x159

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "uri"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x14b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "message_text"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "uri"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendPresenceRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Z
    .param p5    # Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x15c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "is_present"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "reciprocate"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendRemotePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJLjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # J
    .param p6    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x15a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "uri"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendTileEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x15f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "tile_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "tile_event_version"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "tile_event_type"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "tile_event_data"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendTypingRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x15d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "typing_status"

    invoke-virtual {p4}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->getNumber()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private sendUserCreateRequest$74507863(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .param p2    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v0

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->createUser(Ljava/lang/String;J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    const-string v3, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UserCreationRequest registration "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " androidId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z

    return-void
.end method

.method public static setAcl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x157

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "acl"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setConversationMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x152

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "conversation_muted"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setConversationName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x151

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "conversation_name"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setCurrentConversationRowId(Ljava/lang/Long;)V
    .locals 4
    .param p0    # Ljava/lang/Long;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    monitor-enter v1

    :try_start_0
    const-string v0, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCurrentConversationRowId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sput-object p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sCurrentConversationRowId:Ljava/lang/Long;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setMessageFailed(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # J

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "op"

    const/16 v2, 0x15b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static startCommand(Landroid/content/Context;Landroid/content/Intent;)I
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;

    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    sget-object v2, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "rid"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "RealTimeChatService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "start command request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " opCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "op"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->addRequest(Ljava/lang/Object;)V

    return v0
.end method

.method public static unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    const-string v2, "ServiceThread"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->quit()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    :cond_1
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->access$100(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;Landroid/content/Intent;)V

    const/4 v0, 0x2

    return v0
.end method
