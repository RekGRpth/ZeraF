.class public final Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "UpdateConversationOperation.java"


# instance fields
.field private final mConversationRowId:J

.field private mMuted:Ljava/lang/Boolean;

.field private mName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mMuted:Ljava/lang/Boolean;

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mConversationRowId:J

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateConversationNameLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mMuted:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mMuted:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateConversationMutedLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    :cond_1
    return-void
.end method

.method public final setMuted(Z)V
    .locals 1
    .param p1    # Z

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mMuted:Ljava/lang/Boolean;

    return-void
.end method

.method public final setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->mName:Ljava/lang/String;

    return-void
.end method
