.class final Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;
.super Ljava/lang/Thread;
.source "GoogleTalkClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GoogleTalkThread"
.end annotation


# instance fields
.field private mConnected:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->mConnected:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    const/4 v8, 0x5

    const/4 v7, 0x3

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v4, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$100(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mEsAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v5}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$200(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "webupdates"

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/network/AuthData;->getAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$002(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$000(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    const-string v3, "GoogleTalkClient"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "GoogleTalkClient"

    const-string v4, "authentication failed, null token"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    const-string v3, "GoogleTalkClient"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "GoogleTalkClient"

    const-string v4, "authentication failed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_3
    const-string v3, "GoogleTalkClient"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "GoogleTalkClient"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "token "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$000(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    new-instance v4, Ljava/net/Socket;

    const-string v5, "talk.google.com"

    const/16 v6, 0x1466

    invoke-direct {v4, v5, v6}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    # setter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$302(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/net/Socket;)Ljava/net/Socket;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # invokes: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->resetWriter()V
    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$400(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$500(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V

    :goto_2
    iget-boolean v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->mConnected:Z

    if-eqz v3, :cond_7

    new-instance v2, Lcom/google/android/apps/plus/xmpp/MessageReader;

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;
    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$300(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/net/Socket;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mDebugModeEnabled:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$600(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Z

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/xmpp/MessageReader;-><init>(Ljava/io/InputStream;Z)V

    sget-object v3, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$1;->$SwitchMap$com$google$android$apps$plus$xmpp$MessageReader$Event:[I

    invoke-virtual {v2}, Lcom/google/android/apps/plus/xmpp/MessageReader;->read()Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "GoogleTalkClient"

    const-string v4, "end of stream"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    const-string v3, "GoogleTalkClient"

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "GoogleTalkClient"

    const-string v4, "Exception reading data"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    :cond_7
    const-string v3, "GoogleTalkClient"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "GoogleTalkClient"

    const-string v4, "thread finished"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :catch_2
    move-exception v3

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    goto/16 :goto_1

    :pswitch_1
    :try_start_3
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "GoogleTalkClient"

    const-string v4, "unexpected features"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    goto/16 :goto_2

    :pswitch_2
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "GoogleTalkClient"

    const-string v4, "Authentication failed"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$100(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mEsAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$200(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "webupdates"

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const/4 v4, 0x0

    # setter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$002(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    goto/16 :goto_2

    :pswitch_3
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "GoogleTalkClient"

    const-string v4, "TLS required"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const-string v4, "<starttls xmlns=\'urn:ietf:params:xml:ns:xmpp-tls\'/>"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->write(Ljava/lang/String;)Z

    goto/16 :goto_2

    :pswitch_4
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v3, "GoogleTalkClient"

    const-string v4, "Proceed with TLS"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$700(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$500(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V

    goto/16 :goto_2

    :pswitch_5
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "GoogleTalkClient"

    const-string v4, "Authenticated required"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v4, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$000(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/xmpp/Commands;->authenticate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->write(Ljava/lang/String;)Z

    goto/16 :goto_2

    :pswitch_6
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "GoogleTalkClient"

    const-string v4, "Authenticated successfully"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-static {v3}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$500(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V

    goto/16 :goto_2

    :pswitch_7
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "GoogleTalkClient"

    const-string v4, "stream reaqy"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v4, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    # getter for: Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mResource:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$800(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "<iq type=\'set\'><bind xmlns=\'urn:ietf:params:xml:ns:xmpp-bind\'><resource>"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "</resource></bind></iq>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->write(Ljava/lang/String;)Z

    goto/16 :goto_2

    :pswitch_8
    const-string v3, "GoogleTalkClient"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v3, "GoogleTalkClient"

    const-string v4, "jid available"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/xmpp/MessageReader;->getEventData()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->access$900(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_9
    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->this$0:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/xmpp/MessageReader;->getEventData()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->onMessageReceived([B)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public final setDisconnected()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->mConnected:Z

    return-void
.end method
