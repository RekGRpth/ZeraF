.class public Lcom/google/android/apps/plus/views/ColumnGridView;
.super Landroid/view/ViewGroup;
.source "ColumnGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;,
        Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;,
        Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;,
        Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;,
        Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;,
        Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;,
        Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;,
        Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;,
        Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;,
        Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;
    }
.end annotation


# instance fields
.field private mActivePointerId:I

.field private mAdapter:Landroid/widget/ListAdapter;

.field private mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mColCount:I

.field private mColCountSetting:I

.field private final mCurrentTouchPoint:Landroid/graphics/Point;

.field private mDataChanged:Z

.field private final mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

.field private mFirstPosition:I

.field private final mFlingVelocity:I

.field private mHasStableIds:Z

.field private mHorizontalOrientation:Z

.field private mInLayout:Z

.field private mItemCount:I

.field private mItemEnd:[I

.field private mItemMargin:I

.field private mItemStart:[I

.field private mLastScrollState:I

.field private mLastTouch:F

.field private final mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mLocation:[I

.field private final mMaximumVelocity:I

.field private mMinColWidth:I

.field private final mObserver:Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;

.field private mOnScrollListener:Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

.field private mPopulating:Z

.field private mPressed:Z

.field private mRatio:F

.field private final mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

.field private mRestoreOffset:I

.field private mScrollState:I

.field private final mScroller:Landroid/widget/Scroller;

.field private final mSelectedPositions:Landroid/util/SparseBooleanArray;

.field private mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

.field private mSelectionMode:Z

.field private final mSelectionStartPoint:Landroid/graphics/Point;

.field private mSelector:Landroid/graphics/drawable/Drawable;

.field private mSetPressedRunnable:Ljava/lang/Runnable;

.field private final mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

.field private mTouchRemainder:F

.field private final mTouchSlop:I

.field private final mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVisibleOffset:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, -0x1

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCountSetting:I

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMinColWidth:I

    new-instance v1, Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v1}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;-><init>(B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;-><init>(Lcom/google/android/apps/plus/views/ColumnGridView;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mObserver:Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/high16 v1, 0x3f800000

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;-><init>(B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionStartPoint:Landroid/graphics/Point;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v4, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/ColumnGridView$1;-><init>(Lcom/google/android/apps/plus/views/ColumnGridView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSetPressedRunnable:Ljava/lang/Runnable;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMaximumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFlingVelocity:I

    new-instance v1, Landroid/widget/Scroller;

    invoke-direct {v1, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    new-instance v1, Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, p1}, Lvedroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    new-instance v1, Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, p1}, Lvedroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setWillNotDraw(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setClipToPadding(Z)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/views/ColumnGridView;)Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/views/ColumnGridView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearAllState()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/views/ColumnGridView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/views/ColumnGridView;)Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/views/ColumnGridView;)Lvedroid/support/v4/util/SparseArrayCompat;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/views/ColumnGridView;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->addScrap(Landroid/view/View;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeAllViewsInLayout()V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeAllViews()V

    goto :goto_1
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/views/ColumnGridView;)[I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/views/ColumnGridView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/views/ColumnGridView;)[I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/views/ColumnGridView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPressed:Z

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/views/ColumnGridView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/ColumnGridView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/views/ColumnGridView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    return p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/ColumnGridView;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/views/ColumnGridView;)Landroid/util/SparseBooleanArray;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    return-object v0
.end method

.method private checkForSelection(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionStartPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    sub-int v6, v8, p1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionStartPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    sub-int v7, v8, p2

    mul-int v8, v6, v6

    mul-int v9, v7, v7

    add-int v4, v8, v9

    iget v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    iget v9, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    mul-int/2addr v8, v9

    if-ge v4, v8, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v8

    add-int/lit8 v1, v8, -0x1

    :goto_1
    if-ltz v1, :cond_4

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    invoke-virtual {v5, v8}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    aget v8, v8, v10

    if-lt p1, v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    aget v8, v8, v10

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v9

    add-int/2addr v8, v9

    if-gt p1, v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    aget v8, v8, v11

    if-lt p2, v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    aget v8, v8, v11

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    if-gt p2, v8, :cond_2

    add-int v2, v1, v0

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->isSelected(I)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->deselect(I)V

    :goto_2
    const/4 v3, 0x1

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->select(I)V

    goto :goto_2

    :cond_4
    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidate()V

    goto :goto_0
.end method

.method private clearAllState()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v1, "clearallstate - clear"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Lvedroid/support/v4/util/SparseArrayCompat;->clear()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeAllViews()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->resetStateForGridTop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->clear()V

    return-void
.end method

.method private clearPressedState()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPressed:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidate()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPressed:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSetPressedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private fillDown(II)I
    .locals 37
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v34, v0

    if-eqz v34, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v27

    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnSize()I

    move-result v11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v34, v0

    if-eqz v34, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v34

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingRight()I

    move-result v35

    sub-int v14, v34, v35

    :goto_1
    add-int v13, v14, p2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextColumnDown([I)I

    move-result v26

    move/from16 v28, p1

    :goto_2
    if-ltz v26, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    aget v34, v34, v26

    move/from16 v0, v34

    if-ge v0, v13, :cond_15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    move/from16 v34, v0

    move/from16 v0, v28

    move/from16 v1, v34

    if-ge v0, v1, :cond_15

    const/16 v34, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    move/from16 v34, v0

    if-eqz v34, :cond_7

    const/16 v34, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v34

    move-object/from16 v2, v23

    invoke-virtual {v0, v5, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    :cond_0
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v34, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    move/from16 v35, v0

    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->min(II)I

    move-result v30

    mul-int v34, v11, v30

    add-int/lit8 v35, v30, -0x1

    mul-int v35, v35, v20

    add-int v31, v34, v35

    const/16 v34, 0x1

    move/from16 v0, v30

    move/from16 v1, v34

    if-le v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v30

    move-object/from16 v3, v34

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextRecordDown(II[I)Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    move-result-object v29

    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v26, v0

    :goto_4
    const/16 v19, 0x0

    if-nez v29, :cond_9

    new-instance v29, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    const/16 v34, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v34, v0

    const-string v35, "filldown - put"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v36

    invoke-virtual/range {v34 .. v36}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    move/from16 v0, v26

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    :goto_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z

    move/from16 v34, v0

    if-eqz v34, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v17

    move-wide/from16 v0, v17

    move-object/from16 v2, v29

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->id:J

    move-wide/from16 v0, v17

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    :cond_1
    move/from16 v0, v26

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v34, v0

    if-eqz v34, :cond_d

    const/high16 v34, 0x40000000

    move/from16 v0, v31

    move/from16 v1, v34

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v34, v0

    const/16 v35, -0x2

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_b

    const/16 v34, 0x0

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v33

    :goto_6
    move/from16 v0, v33

    invoke-virtual {v5, v0, v15}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v34, v0

    if-eqz v34, :cond_10

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    :goto_7
    if-nez v19, :cond_2

    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    move/from16 v34, v0

    move/from16 v0, v34

    if-eq v9, v0, :cond_3

    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    move/from16 v34, v0

    if-lez v34, :cond_3

    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidateLayoutRecordsAfterPosition(I)V

    :cond_3
    move-object/from16 v0, v29

    iput v9, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    const/16 v34, 0x1

    move/from16 v0, v30

    move/from16 v1, v34

    if-le v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    aget v21, v34, v26

    add-int/lit8 v16, v26, 0x1

    :goto_8
    add-int v34, v26, v30

    move/from16 v0, v16

    move/from16 v1, v34

    if-ge v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    aget v4, v34, v16

    move/from16 v0, v21

    if-le v4, v0, :cond_4

    move/from16 v21, v4

    :cond_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v27

    goto/16 :goto_0

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v34

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingBottom()I

    move-result v35

    sub-int v14, v34, v35

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->addView(Landroid/view/View;)V

    goto/16 :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v34, v0

    const-string v35, "filldown - get"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v36

    invoke-virtual/range {v34 .. v36}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v34, v0

    move-object/from16 v0, v34

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    goto/16 :goto_4

    :cond_9
    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v34, v0

    move/from16 v0, v30

    move/from16 v1, v34

    if-eq v0, v1, :cond_a

    move/from16 v0, v30

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v0, v26

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    const/16 v19, 0x1

    goto/16 :goto_5

    :cond_a
    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v26, v0

    goto/16 :goto_5

    :cond_b
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v34, v0

    const/16 v35, -0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_c

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v34, v0

    add-int/lit8 v34, v34, -0x1

    mul-int v25, v20, v34

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v34, v0

    mul-int v34, v34, v11

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v35, v0

    mul-float v34, v34, v35

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    add-int v24, v34, v25

    const/high16 v34, 0x40000000

    move/from16 v0, v24

    move/from16 v1, v34

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v33

    goto/16 :goto_6

    :cond_c
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v34, v0

    const/high16 v35, 0x40000000

    invoke-static/range {v34 .. v35}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v33

    goto/16 :goto_6

    :cond_d
    const/high16 v34, 0x40000000

    move/from16 v0, v31

    move/from16 v1, v34

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v33

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v34, v0

    const/16 v35, -0x2

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_e

    const/16 v34, 0x0

    const/16 v35, 0x0

    invoke-static/range {v34 .. v35}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    goto/16 :goto_6

    :cond_e
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v34, v0

    const/16 v35, -0x1

    move/from16 v0, v34

    move/from16 v1, v35

    if-ne v0, v1, :cond_f

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v34, v0

    add-int/lit8 v34, v34, -0x1

    mul-int v25, v20, v34

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v34, v0

    mul-int v34, v34, v11

    move/from16 v0, v34

    int-to-float v0, v0

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v35, v0

    mul-float v34, v34, v35

    move/from16 v0, v34

    float-to-int v0, v0

    move/from16 v34, v0

    add-int v24, v34, v25

    const/high16 v34, 0x40000000

    move/from16 v0, v24

    move/from16 v1, v34

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    goto/16 :goto_6

    :cond_f
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v34, v0

    const/high16 v35, 0x40000000

    invoke-static/range {v34 .. v35}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    goto/16 :goto_6

    :cond_10
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    goto/16 :goto_7

    :cond_11
    move/from16 v32, v21

    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v34, v0

    if-eqz v34, :cond_13

    add-int v7, v32, v20

    add-int v8, v7, v9

    add-int v34, v11, v20

    mul-int v34, v34, v26

    add-int v10, v27, v34

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v34

    add-int v6, v10, v34

    move v12, v8

    :goto_a
    invoke-virtual {v5, v7, v10, v8, v6}, Landroid/view/View;->layout(IIII)V

    move/from16 v16, v26

    :goto_b
    add-int v34, v26, v30

    move/from16 v0, v16

    move/from16 v1, v34

    if-ge v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    sub-int v35, v16, v26

    move-object/from16 v0, v29

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->getMarginAfter(I)I

    move-result v35

    add-int v35, v35, v12

    aput v35, v34, v16

    add-int/lit8 v16, v16, 0x1

    goto :goto_b

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    aget v32, v34, v26

    goto :goto_9

    :cond_13
    add-int v34, v11, v20

    mul-int v34, v34, v26

    add-int v7, v27, v34

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v34

    add-int v8, v7, v34

    add-int v10, v32, v20

    add-int v6, v10, v9

    move v12, v6

    goto :goto_a

    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextColumnDown([I)I

    move-result v26

    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_2

    :cond_15
    const/16 v22, 0x0

    const/16 v16, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v34, v0

    move/from16 v0, v16

    move/from16 v1, v34

    if-ge v0, v1, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    aget v34, v34, v16

    move/from16 v0, v34

    move/from16 v1, v22

    if-le v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v34, v0

    aget v22, v34, v16

    :cond_16
    add-int/lit8 v16, v16, 0x1

    goto :goto_c

    :cond_17
    sub-int v34, v22, v14

    return v34
.end method

.method private fillUp(II)I
    .locals 42
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v36, v0

    if-eqz v36, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v26

    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnSize()I

    move-result v9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v36, v0

    if-eqz v36, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v12

    :goto_1
    sub-int v11, v12, p2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextColumnUp()I

    move-result v25

    move/from16 v27, p1

    const/16 v30, 0x1

    move/from16 v28, v27

    :goto_2
    if-ltz v25, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v36, v36, v25

    move/from16 v0, v36

    if-gt v0, v11, :cond_0

    if-nez v30, :cond_1c

    :cond_0
    if-ltz v28, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    move/from16 v36, v0

    move/from16 v0, v28

    move/from16 v1, v36

    if-ge v0, v1, :cond_1c

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-object/from16 v2, v36

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v22

    check-cast v22, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    move/from16 v36, v0

    if-eqz v36, :cond_4

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v36

    move-object/from16 v2, v22

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v36, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    move/from16 v37, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->min(II)I

    move-result v31

    mul-int v36, v9, v31

    add-int/lit8 v37, v31, -0x1

    mul-int v37, v37, v20

    add-int v32, v36, v37

    const/16 v36, 0x1

    move/from16 v0, v31

    move/from16 v1, v36

    if-le v0, v1, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v36, v0

    const-string v37, "getnextrecordup - get"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v38

    invoke-virtual/range {v36 .. v38}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    if-nez v36, :cond_5

    new-instance v29, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    const/16 v36, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;-><init>(B)V

    move/from16 v0, v31

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v36, v0

    const-string v37, "getnextrecordup - put"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v38

    invoke-virtual/range {v36 .. v38}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :goto_4
    const/16 v40, -0x1

    const/high16 v38, -0x80000000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v36, v0

    sub-int v39, v36, v31

    :goto_5
    if-ltz v39, :cond_7

    const v37, 0x7fffffff

    move/from16 v41, v39

    :goto_6
    add-int v36, v39, v31

    move/from16 v0, v41

    move/from16 v1, v36

    if-ge v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v36, v36, v41

    move/from16 v0, v36

    move/from16 v1, v37

    if-ge v0, v1, :cond_20

    :goto_7
    add-int/lit8 v37, v41, 0x1

    move/from16 v41, v37

    move/from16 v37, v36

    goto :goto_6

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v26

    goto/16 :goto_0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v12

    goto/16 :goto_1

    :cond_4
    const/16 v36, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->addView(Landroid/view/View;I)V

    goto/16 :goto_3

    :cond_5
    move-object/from16 v0, v36

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v37, v0

    move/from16 v0, v37

    move/from16 v1, v31

    if-eq v0, v1, :cond_21

    new-instance v37, Ljava/lang/IllegalStateException;

    new-instance v38, Ljava/lang/StringBuilder;

    const-string v39, "Invalid LayoutRecord! Record had span="

    invoke-direct/range {v38 .. v39}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v36

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v36, v0

    move-object/from16 v0, v38

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v38, " but caller requested span="

    move-object/from16 v0, v36

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    const-string v38, " for position="

    move-object/from16 v0, v36

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v36

    move-object/from16 v0, v36

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v37

    :cond_6
    move/from16 v0, v37

    move/from16 v1, v38

    if-le v0, v1, :cond_1f

    move/from16 v36, v39

    :goto_8
    add-int/lit8 v39, v39, -0x1

    move/from16 v38, v37

    move/from16 v40, v36

    goto/16 :goto_5

    :cond_7
    move/from16 v0, v40

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    const/16 v36, 0x0

    :goto_9
    move/from16 v0, v36

    move/from16 v1, v31

    if-ge v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v37, v0

    add-int v39, v36, v40

    aget v37, v37, v39

    sub-int v37, v37, v38

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->setMarginAfter(II)V

    add-int/lit8 v36, v36, 0x1

    goto :goto_9

    :cond_8
    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v25, v0

    :goto_a
    const/16 v19, 0x0

    if-nez v29, :cond_e

    new-instance v29, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    const/16 v36, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v36

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v36, v0

    const-string v37, "fillup - put"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v38

    invoke-virtual/range {v36 .. v38}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    move/from16 v0, v25

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v0, v31

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z

    move/from16 v36, v0

    if-eqz v36, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v28

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v17

    move-wide/from16 v0, v17

    move-object/from16 v2, v29

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->id:J

    move-wide/from16 v0, v17

    move-object/from16 v2, v22

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    :cond_9
    move/from16 v0, v25

    move-object/from16 v1, v22

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v36, v0

    if-eqz v36, :cond_12

    const/high16 v36, 0x40000000

    move/from16 v0, v32

    move/from16 v1, v36

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v36, v0

    const/16 v37, -0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_10

    const/16 v36, 0x0

    const/16 v37, 0x0

    invoke-static/range {v36 .. v37}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v35

    :goto_c
    move/from16 v0, v35

    invoke-virtual {v3, v0, v13}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v36, v0

    if-eqz v36, :cond_15

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    :goto_d
    if-nez v19, :cond_a

    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    move/from16 v36, v0

    move/from16 v0, v36

    if-eq v7, v0, :cond_b

    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    move/from16 v36, v0

    if-lez v36, :cond_b

    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidateLayoutRecordsBeforePosition(I)V

    :cond_b
    move-object/from16 v0, v29

    iput v7, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    const/16 v36, 0x1

    move/from16 v0, v31

    move/from16 v1, v36

    if-le v0, v1, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v14, v36, v25

    add-int/lit8 v16, v25, 0x1

    :goto_e
    add-int v36, v25, v31

    move/from16 v0, v16

    move/from16 v1, v36

    if-ge v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v34, v36, v16

    move/from16 v0, v34

    if-ge v0, v14, :cond_c

    move/from16 v14, v34

    :cond_c
    add-int/lit8 v16, v16, 0x1

    goto :goto_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v36, v0

    const-string v37, "fillup - get"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v38

    invoke-virtual/range {v36 .. v38}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    goto/16 :goto_a

    :cond_e
    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v36, v0

    move/from16 v0, v31

    move/from16 v1, v36

    if-eq v0, v1, :cond_f

    move/from16 v0, v31

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v0, v25

    move-object/from16 v1, v29

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    const/16 v19, 0x1

    goto/16 :goto_b

    :cond_f
    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v25, v0

    goto/16 :goto_b

    :cond_10
    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v36, v0

    const/16 v37, -0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_11

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v36, v0

    add-int/lit8 v36, v36, -0x1

    mul-int v24, v20, v36

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v36, v0

    mul-int v36, v36, v9

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v37, v0

    mul-float v36, v36, v37

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v36, v0

    add-int v23, v36, v24

    const/high16 v36, 0x40000000

    move/from16 v0, v23

    move/from16 v1, v36

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v35

    goto/16 :goto_c

    :cond_11
    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v36, v0

    const/high16 v37, 0x40000000

    invoke-static/range {v36 .. v37}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v35

    goto/16 :goto_c

    :cond_12
    const/high16 v36, 0x40000000

    move/from16 v0, v32

    move/from16 v1, v36

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v35

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v36, v0

    const/16 v37, -0x2

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_13

    const/16 v36, 0x0

    const/16 v37, 0x0

    invoke-static/range {v36 .. v37}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    goto/16 :goto_c

    :cond_13
    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v36, v0

    const/16 v37, -0x1

    move/from16 v0, v36

    move/from16 v1, v37

    if-ne v0, v1, :cond_14

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v36, v0

    add-int/lit8 v36, v36, -0x1

    mul-int v24, v20, v36

    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v36, v0

    mul-int v36, v36, v9

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v37, v0

    mul-float v36, v36, v37

    move/from16 v0, v36

    float-to-int v0, v0

    move/from16 v36, v0

    add-int v23, v36, v24

    const/high16 v36, 0x40000000

    move/from16 v0, v23

    move/from16 v1, v36

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    goto/16 :goto_c

    :cond_14
    move-object/from16 v0, v22

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v36, v0

    const/high16 v37, 0x40000000

    invoke-static/range {v36 .. v37}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    goto/16 :goto_c

    :cond_15
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    goto/16 :goto_d

    :cond_16
    move/from16 v33, v14

    :goto_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v36, v0

    if-eqz v36, :cond_18

    move/from16 v6, v33

    sub-int v5, v33, v7

    add-int v36, v9, v20

    mul-int v36, v36, v25

    add-int v8, v26, v36

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v36

    add-int v4, v8, v36

    move v10, v5

    :goto_10
    invoke-virtual {v3, v5, v8, v6, v4}, Landroid/view/View;->layout(IIII)V

    move/from16 v16, v25

    :goto_11
    add-int v36, v25, v31

    move/from16 v0, v16

    move/from16 v1, v36

    if-ge v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    sub-int v37, v16, v25

    move-object/from16 v0, v29

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->getMarginBefore(I)I

    move-result v37

    sub-int v37, v10, v37

    sub-int v37, v37, v20

    aput v37, v36, v16

    add-int/lit8 v16, v16, 0x1

    goto :goto_11

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v33, v36, v25

    goto :goto_f

    :cond_18
    move/from16 v4, v33

    sub-int v8, v33, v7

    add-int v36, v9, v20

    mul-int v36, v36, v25

    add-int v5, v26, v36

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v36

    add-int v6, v5, v36

    move v10, v8

    goto :goto_10

    :cond_19
    move-object/from16 v0, v22

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    const/16 v37, 0x0

    aget v21, v36, v37

    const/16 v16, 0x1

    :goto_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v36, v0

    move/from16 v0, v16

    move/from16 v1, v36

    if-ge v0, v1, :cond_1b

    if-eqz v30, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v36, v36, v16

    move/from16 v0, v36

    move/from16 v1, v21

    if-eq v0, v1, :cond_1a

    const/16 v30, 0x0

    :cond_1a
    add-int/lit8 v16, v16, 0x1

    goto :goto_12

    :cond_1b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextColumnUp()I

    move-result v25

    add-int/lit8 v27, v28, -0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v28, v27

    goto/16 :goto_2

    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v15

    const/16 v16, 0x0

    :goto_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v36, v0

    move/from16 v0, v16

    move/from16 v1, v36

    if-ge v0, v1, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v36, v36, v16

    move/from16 v0, v36

    if-ge v0, v15, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v36, v0

    aget v15, v36, v16

    :cond_1d
    add-int/lit8 v16, v16, 0x1

    goto :goto_13

    :cond_1e
    sub-int v36, v12, v15

    return v36

    :cond_1f
    move/from16 v37, v38

    move/from16 v36, v40

    goto/16 :goto_8

    :cond_20
    move/from16 v36, v37

    goto/16 :goto_7

    :cond_21
    move-object/from16 v29, v36

    goto/16 :goto_4
.end method

.method private generateDefaultLayoutParams()Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    .locals 4

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const/4 v3, -0x2

    invoke-direct {v2, v0, v3, v1, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    return-object v2

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    .locals 2
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    return-object v0

    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method private getNextColumnDown([I)I
    .locals 5
    .param p1    # [I

    const/4 v3, -0x1

    const v4, 0x7fffffff

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget v0, p1, v2

    if-ge v0, v4, :cond_0

    move v4, v0

    move v3, v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method private getNextColumnUp()I
    .locals 6

    const/4 v3, -0x1

    const/high16 v0, -0x80000000

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-ltz v2, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    aget v4, v5, v2

    if-le v4, v0, :cond_0

    move v0, v4

    move v3, v2

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method private getNextRecordDown(II[I)Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v9, "getnextrecorddown - get"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, p1}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    const/4 v8, 0x0

    invoke-direct {v4, v8}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;-><init>(B)V

    iput p2, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v9, "getnextrecorddown - put"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v8, p1, v4}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :cond_0
    const/4 v6, -0x1

    const v7, 0x7fffffff

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    const/4 v2, 0x0

    :goto_0
    sub-int v8, v1, p2

    if-gt v2, v8, :cond_5

    const/high16 v0, -0x80000000

    move v3, v2

    :goto_1
    add-int v8, v2, p2

    if-ge v3, v8, :cond_3

    aget v5, p3, v3

    if-le v5, v0, :cond_1

    move v0, v5

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iget v8, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    if-eq v8, p2, :cond_0

    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Invalid LayoutRecord! Record had span="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " but caller requested span="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for position="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_3
    if-ge v0, v7, :cond_4

    move v7, v0

    move v6, v2

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    iput v6, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    const/4 v2, 0x0

    :goto_2
    if-ge v2, p2, :cond_6

    add-int v8, v2, v6

    aget v8, p3, v8

    sub-int v8, v7, v8

    invoke-virtual {v4, v2, v8}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->setMarginBefore(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    return-object v4
.end method

.method private invalidateLayoutRecordsAfterPosition(I)V
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, v0}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v1

    if-le v1, p1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v2, "invalidateafter - removeatrange"

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-virtual {v1, v2, v3}, Lvedroid/support/v4/util/SparseArrayCompat;->removeAtRange(II)V

    return-void
.end method

.method private invalidateLayoutRecordsBeforePosition(I)V
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, v0}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v1

    if-ge v1, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v2, "invalidatebefore - removeatrange"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lvedroid/support/v4/util/SparseArrayCompat;->removeAtRange(II)V

    return-void
.end method

.method private invokeOnItemScrollListener(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    iget v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;->onScroll$1ca47ba7(Lcom/google/android/apps/plus/views/ColumnGridView;IIII)V

    :cond_0
    invoke-virtual {p0, v6, v6, v6, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->onScrollChanged(IIII)V

    return-void
.end method

.method private isSelected(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v0

    return v0
.end method

.method private obtainView(ILandroid/view/View;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-virtual {v7, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->getTransientStateView(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object v6, v5

    :goto_0
    return-object v6

    :cond_0
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iget v1, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->viewType:I

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    if-ne v1, v2, :cond_5

    move-object v3, p2

    :goto_2
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7, p1, v3, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    if-eq v5, v3, :cond_1

    if-eqz v3, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v8

    invoke-virtual {v7, v3, v8}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->addScrap(Landroid/view/View;I)V

    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    if-eq v7, p0, :cond_3

    if-nez v0, :cond_6

    const-string v7, "ColumnGridView"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "view at position "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " doesn\'t have layout parameters;using default layout paramters"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->generateDefaultLayoutParams()Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-result-object v0

    :cond_2
    :goto_3
    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    move-object v4, v0

    check-cast v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iput p1, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->position:I

    iput v2, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->viewType:I

    move-object v6, v5

    goto :goto_0

    :cond_4
    const/4 v1, -0x1

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-virtual {v7, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v3

    goto :goto_2

    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "ColumnGridView"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "view at position "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " doesn\'t have layout parameters of type ColumnGridView.LayoutParams; wrapping parameters"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-result-object v0

    goto :goto_3
.end method

.method private populate()V
    .locals 24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMinColWidth:I

    div-int v2, v5, v6

    :goto_1
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    if-eq v2, v5, :cond_2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    array-length v5, v5

    if-eq v5, v2, :cond_4

    :cond_3
    new-array v5, v2, [I

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    new-array v5, v2, [I

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v4

    :goto_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    add-int v3, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    invoke-static {v5, v3}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    invoke-static {v5, v3}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v6, "populate - clear"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5}, Lvedroid/support/v4/util/SparseArrayCompat;->clear()V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    if-eqz v5, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeAllViewsInLayout()V

    :goto_3
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    :cond_4
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPopulating:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mDataChanged:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v5

    move v6, v5

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnSize()I

    move-result v20

    const/4 v15, -0x1

    const/4 v14, -0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    const/high16 v7, -0x80000000

    invoke-static {v5, v7}, Ljava/util/Arrays;->fill([II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v21

    const/4 v5, 0x0

    move/from16 v17, v5

    :goto_5
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_18

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iget v0, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    add-int v13, v7, v17

    if-nez v18, :cond_5

    invoke-virtual {v8}, Landroid/view/View;->isLayoutRequested()Z

    move-result v7

    if-eqz v7, :cond_c

    :cond_5
    const/4 v7, 0x1

    move v9, v7

    :goto_6
    if-eqz v18, :cond_37

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v8}, Lcom/google/android/apps/plus/views/ColumnGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v7

    if-eq v7, v8, :cond_36

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeViewAt(I)V

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->addView(Landroid/view/View;I)V

    :goto_7
    iget v8, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iget v10, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    if-eq v10, v8, :cond_6

    const-string v8, "ColumnGridView"

    const-string v10, "Span changed!"

    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move/from16 v0, v16

    iput v0, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    :goto_8
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    iget v10, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    invoke-static {v8, v10}, Ljava/lang/Math;->min(II)I

    move-result v22

    mul-int v8, v20, v22

    add-int/lit8 v10, v22, -0x1

    mul-int v10, v10, v19

    add-int/2addr v8, v10

    if-eqz v9, :cond_7

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v9, :cond_f

    const/high16 v9, 0x40000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_d

    const/4 v5, 0x0

    const/4 v9, 0x0

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    move/from16 v23, v8

    move v8, v5

    move/from16 v5, v23

    :goto_9
    invoke-virtual {v7, v8, v5}, Landroid/view/View;->measure(II)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    aget v5, v5, v16

    const/high16 v8, -0x80000000

    if-le v5, v8, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    aget v5, v5, v16

    add-int v8, v5, v19

    :goto_a
    const/4 v5, 0x1

    move/from16 v0, v22

    if-le v0, v5, :cond_14

    add-int/lit8 v5, v16, 0x1

    move v9, v5

    :goto_b
    add-int v5, v16, v22

    if-ge v9, v5, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    aget v5, v5, v9

    add-int v5, v5, v19

    if-le v5, v8, :cond_35

    :goto_c
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v5

    goto :goto_b

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMinColWidth:I

    div-int v2, v5, v6

    goto/16 :goto_1

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v4

    goto/16 :goto_2

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeAllViews()V

    goto/16 :goto_3

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v5

    move v6, v5

    goto/16 :goto_4

    :cond_c
    const/4 v7, 0x0

    move v9, v7

    goto/16 :goto_6

    :cond_d
    iget v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_e

    iget v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    add-int/lit8 v9, v9, -0x1

    mul-int v9, v9, v19

    iget v5, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    mul-int v5, v5, v20

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    mul-float/2addr v5, v10

    float-to-int v5, v5

    add-int/2addr v5, v9

    const/high16 v9, 0x40000000

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    move/from16 v23, v8

    move v8, v5

    move/from16 v5, v23

    goto :goto_9

    :cond_e
    iget v5, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    const/high16 v9, 0x40000000

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    move/from16 v23, v8

    move v8, v5

    move/from16 v5, v23

    goto/16 :goto_9

    :cond_f
    const/high16 v9, 0x40000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_10

    const/4 v5, 0x0

    const/4 v9, 0x0

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto/16 :goto_9

    :cond_10
    iget v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_11

    iget v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    add-int/lit8 v9, v9, -0x1

    mul-int v9, v9, v19

    iget v5, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    mul-int v5, v5, v20

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    mul-float/2addr v5, v10

    float-to-int v5, v5

    add-int/2addr v5, v9

    const/high16 v9, 0x40000000

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto/16 :goto_9

    :cond_11
    iget v5, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    const/high16 v9, 0x40000000

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    goto/16 :goto_9

    :cond_12
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_13

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v8

    goto/16 :goto_a

    :cond_13
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v8

    goto/16 :goto_a

    :cond_14
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_15

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    move v9, v5

    :goto_d
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_16

    add-int v5, v8, v9

    add-int v10, v20, v19

    mul-int v10, v10, v16

    add-int/2addr v10, v6

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v10

    move v12, v5

    :goto_e
    invoke-virtual {v7, v8, v10, v5, v11}, Landroid/view/View;->layout(IIII)V

    move/from16 v5, v16

    :goto_f
    add-int v7, v16, v22

    if-ge v5, v7, :cond_17

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    aput v12, v7, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_f

    :cond_15
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    move v9, v5

    goto :goto_d

    :cond_16
    add-int v5, v20, v19

    mul-int v5, v5, v16

    add-int v10, v6, v5

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v10

    add-int v11, v8, v9

    move v12, v11

    move/from16 v23, v8

    move v8, v10

    move/from16 v10, v23

    goto :goto_e

    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v7, "layoutchildren - get"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5, v13}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    if-eqz v5, :cond_34

    iget v7, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    if-eq v7, v9, :cond_34

    iput v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    move v7, v13

    :goto_10
    if-eqz v5, :cond_33

    iget v8, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v0, v22

    if-eq v8, v0, :cond_33

    move/from16 v0, v22

    iput v0, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move v5, v13

    :goto_11
    add-int/lit8 v8, v17, 0x1

    move/from16 v17, v8

    move v14, v5

    move v15, v7

    goto/16 :goto_5

    :cond_18
    const/4 v5, 0x0

    :goto_12
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    if-ge v5, v6, :cond_1a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    aget v6, v6, v5

    const/high16 v7, -0x80000000

    if-ne v6, v7, :cond_19

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    aget v7, v7, v5

    aput v7, v6, v5

    :cond_19
    add-int/lit8 v5, v5, 0x1

    goto :goto_12

    :cond_1a
    if-gez v15, :cond_1b

    if-ltz v14, :cond_20

    :cond_1b
    if-ltz v15, :cond_1c

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidateLayoutRecordsBeforePosition(I)V

    :cond_1c
    if-ltz v14, :cond_1d

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidateLayoutRecordsAfterPosition(I)V

    :cond_1d
    const/4 v5, 0x0

    move v8, v5

    :goto_13
    move/from16 v0, v21

    if-ge v8, v0, :cond_20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    add-int v7, v5, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v10, "layoutchildren - get2"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v6, v10, v11}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v7}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    if-nez v6, :cond_1e

    new-instance v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    const/4 v10, 0x0

    invoke-direct {v6, v10}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v11, "layoutchildren - put2"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v7, v6}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :cond_1e
    iget v7, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    iput v7, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v7, :cond_1f

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v7

    :goto_14
    iput v7, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    iget-wide v9, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    iput-wide v9, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->id:J

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    iget v5, v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    invoke-static {v7, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    iput v5, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    add-int/lit8 v5, v8, 0x1

    move v8, v5

    goto :goto_13

    :cond_1f
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v7

    goto :goto_14

    :cond_20
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    if-eqz v5, :cond_30

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    new-array v13, v5, [I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnSize()I

    move-result v15

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextColumnDown([I)I

    move-result v6

    const/4 v5, 0x0

    move v10, v5

    move v7, v6

    :goto_15
    if-ge v10, v12, :cond_30

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    if-ge v10, v5, :cond_30

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    iget v8, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v16

    mul-int v5, v15, v16

    add-int/lit8 v8, v16, -0x1

    mul-int/2addr v8, v14

    add-int v9, v5, v8

    const/4 v5, 0x1

    move/from16 v0, v16

    if-le v0, v5, :cond_24

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v10, v1, v13}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextRecordDown(II[I)Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    move-result-object v7

    iget v8, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    :goto_16
    const/4 v5, 0x0

    if-nez v7, :cond_25

    new-instance v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-direct {v7, v0}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v17, v0

    const-string v18, "prefilldown - put"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10, v7}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    iput v8, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    move/from16 v0, v16

    iput v0, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    :goto_17
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z

    move/from16 v17, v0

    if-eqz v17, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v10}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v17

    move-wide/from16 v0, v17

    iput-wide v0, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->id:J

    move-wide/from16 v0, v17

    iput-wide v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    :cond_21
    iput v8, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v17, v0

    if-eqz v17, :cond_29

    const/high16 v17, 0x40000000

    move/from16 v0, v17

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    iget v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v17, v0

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_27

    const/4 v6, 0x0

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move/from16 v23, v9

    move v9, v6

    move/from16 v6, v23

    :goto_18
    invoke-virtual {v11, v9, v6}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v6, :cond_2c

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    move v11, v6

    :goto_19
    if-nez v5, :cond_22

    iget v5, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    if-eq v11, v5, :cond_23

    iget v5, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    if-lez v5, :cond_23

    :cond_22
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidateLayoutRecordsAfterPosition(I)V

    :cond_23
    iput v11, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->size:I

    const/4 v5, 0x1

    move/from16 v0, v16

    if-le v0, v5, :cond_2d

    aget v6, v13, v8

    add-int/lit8 v5, v8, 0x1

    move v9, v5

    :goto_1a
    add-int v5, v8, v16

    if-ge v9, v5, :cond_2e

    aget v5, v13, v9

    if-le v5, v6, :cond_32

    :goto_1b
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    move v6, v5

    goto :goto_1a

    :cond_24
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    const-string v8, "prefilldown - get"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v5, v8, v0}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5, v10}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    move v8, v7

    move-object v7, v5

    goto/16 :goto_16

    :cond_25
    iget v0, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_26

    move/from16 v0, v16

    iput v0, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->span:I

    iput v8, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    const/4 v5, 0x1

    goto/16 :goto_17

    :cond_26
    iget v8, v7, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->column:I

    goto/16 :goto_17

    :cond_27
    iget v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_28

    iget v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    mul-int v17, v17, v14

    iget v6, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    mul-int/2addr v6, v15

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v18, v0

    mul-float v6, v6, v18

    float-to-int v6, v6

    add-int v6, v6, v17

    const/high16 v17, 0x40000000

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move/from16 v23, v9

    move v9, v6

    move/from16 v6, v23

    goto/16 :goto_18

    :cond_28
    iget v6, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    const/high16 v17, 0x40000000

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move/from16 v23, v9

    move v9, v6

    move/from16 v6, v23

    goto/16 :goto_18

    :cond_29
    const/high16 v17, 0x40000000

    move/from16 v0, v17

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    iget v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v17, v0

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2a

    const/4 v6, 0x0

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    goto/16 :goto_18

    :cond_2a
    iget v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2b

    iget v0, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    mul-int v17, v17, v14

    iget v6, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    mul-int/2addr v6, v15

    int-to-float v6, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v18, v0

    mul-float v6, v6, v18

    float-to-int v6, v6

    add-int v6, v6, v17

    const/high16 v17, 0x40000000

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    goto/16 :goto_18

    :cond_2b
    iget v6, v6, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    const/high16 v17, 0x40000000

    move/from16 v0, v17

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    goto/16 :goto_18

    :cond_2c
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    move v11, v6

    goto/16 :goto_19

    :cond_2d
    aget v6, v13, v8

    :cond_2e
    add-int v5, v6, v11

    add-int v6, v5, v14

    move v5, v8

    :goto_1c
    add-int v9, v8, v16

    if-ge v5, v9, :cond_2f

    sub-int v9, v5, v8

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->getMarginAfter(I)I

    move-result v9

    add-int/2addr v9, v6

    aput v9, v13, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1c

    :cond_2f
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/android/apps/plus/views/ColumnGridView;->getNextColumnDown([I)I

    move-result v6

    add-int/lit8 v5, v10, 0x1

    move v10, v5

    move v7, v6

    goto/16 :goto_15

    :cond_30
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v6

    add-int/2addr v5, v6

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->fillDown(II)I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    add-int/lit8 v6, v5, -0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    if-lez v5, :cond_31

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    :goto_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->fillUp(II)I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibleOffset()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPopulating:Z

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mDataChanged:Z

    goto/16 :goto_0

    :cond_31
    const/4 v5, 0x0

    goto :goto_1d

    :cond_32
    move v5, v6

    goto/16 :goto_1b

    :cond_33
    move v5, v14

    goto/16 :goto_11

    :cond_34
    move v7, v15

    goto/16 :goto_10

    :cond_35
    move v5, v8

    goto/16 :goto_c

    :cond_36
    move-object v7, v8

    goto/16 :goto_7

    :cond_37
    move-object v7, v8

    goto/16 :goto_8
.end method

.method private reportScrollStateChange(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastScrollState:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastScrollState:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;->onScrollStateChanged$6f02efe7(I)V

    :cond_0
    return-void
.end method

.method private resetStateForGridTop()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    array-length v2, v2

    if-eq v2, v0, :cond_1

    :cond_0
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->fill([II)V

    :cond_2
    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    return-void
.end method

.method private setVisibleOffset()V
    .locals 6

    iget v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    neg-int v4, v5

    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v3, v1, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v2

    :goto_1
    if-ge v2, v4, :cond_1

    iget v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v2

    goto :goto_1

    :cond_1
    return-void
.end method

.method private trackMotionScroll(IZ)Z
    .locals 30
    .param p1    # I
    .param p2    # Z

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v19, v0

    if-nez v19, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v9, 0x0

    :goto_0
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-nez v9, :cond_2f

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView;->mPopulating:Z

    if-lez p1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->fillUp(II)I

    move-result v17

    const/16 v18, 0x1

    :goto_1
    move/from16 v0, v17

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v14

    if-eqz v18, :cond_8

    move/from16 v19, v14

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v20, v0

    if-eqz v20, :cond_9

    move/from16 v20, v19

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v21, v0

    if-eqz v21, :cond_a

    const/16 v21, 0x0

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v23

    const/16 v22, 0x0

    :goto_5
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_b

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getLeft()I

    move-result v25

    add-int v25, v25, v20

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getTop()I

    move-result v26

    add-int v26, v26, v21

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getRight()I

    move-result v27

    add-int v27, v27, v20

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getBottom()I

    move-result v28

    add-int v28, v28, v21

    invoke-virtual/range {v24 .. v28}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v22, v22, 0x1

    goto :goto_5

    :cond_1
    const v21, 0x7fffffff

    const/high16 v20, -0x80000000

    const/16 v19, 0x0

    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v22, v0

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v22, v0

    aget v22, v22, v19

    move/from16 v0, v22

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v21, v0

    aget v21, v21, v19

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v22, v0

    aget v22, v22, v19

    move/from16 v0, v22

    move/from16 v1, v20

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v20, v0

    aget v20, v20, v19

    :cond_3
    add-int/lit8 v19, v19, 0x1

    goto :goto_6

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingRight()I

    move-result v22

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v19

    :goto_7
    move/from16 v0, v21

    move/from16 v1, v23

    if-lt v0, v1, :cond_6

    sub-int v19, v19, v22

    move/from16 v0, v20

    move/from16 v1, v19

    if-gt v0, v1, :cond_6

    const/4 v9, 0x1

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingBottom()I

    move-result v22

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v19

    goto :goto_7

    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v20

    add-int v19, v19, v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->fillDown(II)I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    move/from16 v20, v0

    add-int v17, v19, v20

    const/16 v18, 0x0

    goto/16 :goto_1

    :cond_8
    neg-int v0, v14

    move/from16 v19, v0

    goto/16 :goto_2

    :cond_9
    const/16 v20, 0x0

    goto/16 :goto_3

    :cond_a
    move/from16 v21, v19

    goto/16 :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v21, v0

    const/16 v20, 0x0

    :goto_8
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v22, v0

    aget v23, v22, v20

    add-int v23, v23, v19

    aput v23, v22, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v22, v0

    aget v23, v22, v20

    add-int v23, v23, v19

    aput v23, v22, v20

    add-int/lit8 v20, v20, 0x1

    goto :goto_8

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_e

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v19

    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnSize()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    move/from16 v22, v0

    mul-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v22, v0

    mul-int v22, v22, v20

    add-int v24, v21, v22

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v21, v0

    add-int v25, v19, v20

    move/from16 v0, v24

    neg-int v0, v0

    move/from16 v20, v0

    add-int v26, v19, v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v23, v19

    :goto_a
    if-ltz v23, :cond_13

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v27

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_f

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getRight()I

    move-result v19

    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v22, v0

    if-eqz v22, :cond_10

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLeft()I

    move-result v22

    :goto_c
    move/from16 v0, v22

    move/from16 v1, v21

    if-le v0, v1, :cond_d

    move/from16 v0, v19

    move/from16 v1, v25

    if-lt v0, v1, :cond_38

    :cond_d
    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_38

    move/from16 v0, v22

    move/from16 v1, v26

    if-gt v0, v1, :cond_38

    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_38

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_11

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getWidth()I

    move-result v19

    :goto_d
    move/from16 v0, v19

    move/from16 v1, v24

    if-le v0, v1, :cond_12

    move/from16 v19, v21

    :goto_e
    add-int/lit8 v20, v23, -0x1

    move/from16 v23, v20

    move/from16 v20, v19

    goto :goto_a

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v19

    goto/16 :goto_9

    :cond_f
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getBottom()I

    move-result v19

    goto :goto_b

    :cond_10
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getTop()I

    move-result v22

    goto :goto_c

    :cond_11
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getHeight()I

    move-result v19

    goto :goto_d

    :cond_12
    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    move/from16 v19, v0

    if-nez v19, :cond_38

    move/from16 v19, v21

    goto :goto_e

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move/from16 v21, v19

    :goto_f
    if-ltz v21, :cond_16

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_14

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getLeft()I

    move-result v19

    :goto_10
    move/from16 v0, v19

    move/from16 v1, v26

    if-le v0, v1, :cond_16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    move/from16 v19, v0

    if-eqz v19, :cond_15

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeViewsInLayout(II)V

    :goto_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v23

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->addScrap(Landroid/view/View;I)V

    add-int/lit8 v19, v21, -0x1

    move/from16 v21, v19

    goto :goto_f

    :cond_14
    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getTop()I

    move-result v19

    goto :goto_10

    :cond_15
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeViewAt(I)V

    goto :goto_11

    :cond_16
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    :goto_12
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v19

    if-lez v19, :cond_17

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1b

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getRight()I

    move-result v19

    :goto_13
    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_1c

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibleOffset()V

    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v24

    if-lez v24, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v19, v0

    const v20, 0x7fffffff

    invoke-static/range {v19 .. v20}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v19, v0

    const/high16 v20, -0x80000000

    invoke-static/range {v19 .. v20}, Ljava/util/Arrays;->fill([II)V

    const/16 v19, 0x0

    move/from16 v23, v19

    :goto_14
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_21

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1e

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getLeft()I

    move-result v20

    :goto_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    move/from16 v22, v0

    sub-int v25, v20, v22

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1f

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getRight()I

    move-result v20

    move/from16 v21, v20

    :goto_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v20, v0

    const-string v22, "recycleoffscreenveiws - get"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v26, v0

    add-int v26, v26, v23

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLayoutRecords:Lvedroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v22, v0

    add-int v22, v22, v23

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;

    if-nez v20, :cond_18

    const-string v22, "ColumnGridView"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mBug6713624LinkedHashMap:Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/views/ColumnGridView$Bug6713624LinkedHashMap;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_18
    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v26, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    move/from16 v27, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->min(II)I

    move-result v26

    add-int v26, v26, v22

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move/from16 v22, v0

    :goto_17
    move/from16 v0, v22

    move/from16 v1, v26

    if-ge v0, v1, :cond_20

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move/from16 v27, v0

    sub-int v27, v22, v27

    move-object/from16 v0, v20

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->getMarginBefore(I)I

    move-result v27

    sub-int v27, v25, v27

    move-object/from16 v0, v19

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    move/from16 v28, v0

    sub-int v28, v22, v28

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutRecord;->getMarginAfter(I)I

    move-result v28

    add-int v28, v28, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v29, v0

    aget v29, v29, v22

    move/from16 v0, v27

    move/from16 v1, v29

    if-ge v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v29, v0

    aput v27, v29, v22

    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v27, v0

    aget v27, v27, v22

    move/from16 v0, v28

    move/from16 v1, v27

    if-le v0, v1, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v27, v0

    aput v28, v27, v22

    :cond_1a
    add-int/lit8 v22, v22, 0x1

    goto :goto_17

    :cond_1b
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getBottom()I

    move-result v19

    goto/16 :goto_13

    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1d

    const/16 v19, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeViewsInLayout(II)V

    :goto_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    move-object/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v22

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->addScrap(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    goto/16 :goto_12

    :cond_1d
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeViewAt(I)V

    goto :goto_18

    :cond_1e
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getTop()I

    move-result v20

    goto/16 :goto_15

    :cond_1f
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getBottom()I

    move-result v20

    move/from16 v21, v20

    goto/16 :goto_16

    :cond_20
    add-int/lit8 v19, v23, 0x1

    move/from16 v23, v19

    goto/16 :goto_14

    :cond_21
    const v20, 0x7fffffff

    const/16 v19, 0x0

    :goto_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v21, v0

    aget v21, v21, v19

    move/from16 v0, v21

    move/from16 v1, v20

    if-ge v0, v1, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v20, v0

    aget v20, v20, v19

    :cond_22
    add-int/lit8 v19, v19, 0x1

    goto :goto_19

    :cond_23
    const v19, 0x7fffffff

    move/from16 v0, v20

    move/from16 v1, v19

    if-ne v0, v1, :cond_24

    const/16 v20, 0x0

    :cond_24
    const/16 v19, 0x0

    :goto_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v21, v0

    aget v21, v21, v19

    const v22, 0x7fffffff

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemStart:[I

    move-object/from16 v21, v0

    aput v20, v21, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemEnd:[I

    move-object/from16 v21, v0

    aput v20, v21, v19

    :cond_25
    add-int/lit8 v19, v19, 0x1

    goto :goto_1a

    :cond_26
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/ColumnGridView;->mPopulating:Z

    sub-int v16, v3, v17

    :goto_1b
    if-eqz p2, :cond_28

    invoke-static/range {p0 .. p0}, Lvedroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v15

    if-eqz v15, :cond_27

    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v15, v0, :cond_28

    if-nez v9, :cond_28

    :cond_27
    if-lez v16, :cond_28

    if-lez p1, :cond_30

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    :goto_1c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_31

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v10

    :goto_1d
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v19

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v10

    move/from16 v20, v0

    div-float v19, v19, v20

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Lvedroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    invoke-static/range {p0 .. p0}, Lvedroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_28
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v6

    if-lez v6, :cond_32

    const/4 v4, 0x1

    :goto_1e
    if-eqz v4, :cond_2a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v19, v0

    if-nez v19, :cond_2a

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_33

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v13

    :goto_1f
    if-lt v8, v13, :cond_29

    if-gez p1, :cond_34

    :cond_29
    const/4 v4, 0x1

    :cond_2a
    :goto_20
    if-eqz v4, :cond_2c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    move/from16 v19, v0

    add-int v19, v19, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_2c

    add-int/lit8 v19, v6, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    move/from16 v19, v0

    if-eqz v19, :cond_35

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingRight()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v11

    :goto_21
    sub-int v19, v11, v13

    move/from16 v0, v19

    if-gt v7, v0, :cond_2b

    if-lez p1, :cond_36

    :cond_2b
    const/4 v4, 0x1

    :cond_2c
    :goto_22
    if-eqz v4, :cond_2d

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/views/ColumnGridView;->invokeOnItemScrollListener(I)V

    :cond_2d
    if-eqz p1, :cond_2e

    if-eqz v14, :cond_37

    :cond_2e
    const/16 v19, 0x1

    :goto_23
    return v19

    :cond_2f
    move/from16 v16, v3

    const/4 v14, 0x0

    goto/16 :goto_1b

    :cond_30
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    goto/16 :goto_1c

    :cond_31
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v10

    goto/16 :goto_1d

    :cond_32
    const/4 v4, 0x0

    goto/16 :goto_1e

    :cond_33
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v13

    goto :goto_1f

    :cond_34
    const/4 v4, 0x0

    goto :goto_20

    :cond_35
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingBottom()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v11

    goto :goto_21

    :cond_36
    const/4 v4, 0x0

    goto :goto_22

    :cond_37
    const/16 v19, 0x0

    goto :goto_23

    :cond_38
    move/from16 v19, v20

    goto/16 :goto_e
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    return v0
.end method

.method public computeScroll()V
    .locals 9

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    :goto_0
    int-to-float v7, v0

    iget v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    sub-float/2addr v7, v8

    float-to-int v3, v7

    int-to-float v7, v0

    iput v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    invoke-direct {p0, v3, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->trackMotionScroll(IZ)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v5, 0x1

    :goto_1
    if-nez v5, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->isFinished()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {p0}, Lvedroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    :goto_2
    iget v6, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->reportScrollStateChange(I)V

    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    goto :goto_0

    :cond_2
    move v5, v6

    goto :goto_1

    :cond_3
    if-eqz v5, :cond_6

    invoke-static {p0}, Lvedroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v4

    const/4 v7, 0x2

    if-eq v4, v7, :cond_5

    if-lez v3, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    :goto_3
    const/4 v1, 0x0

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xe

    if-lt v7, v8, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v7

    float-to-int v1, v7

    :cond_4
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v7

    invoke-virtual {v2, v7}, Lvedroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    invoke-static {p0}, Lvedroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_6
    iput v6, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    goto :goto_2

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_3
.end method

.method public final deselect(I)V
    .locals 4
    .param p1    # I

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    sub-int v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    invoke-interface {v2, v1, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;->onItemDeselected(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v10, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getRight()I

    move-result v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingRight()I

    move-result v11

    sub-int v3, v10, v11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getBottom()I

    move-result v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingBottom()I

    move-result v11

    sub-int v0, v10, v11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v10

    add-int/lit8 v1, v10, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    iget v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    add-int/2addr v10, v1

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/views/ColumnGridView;->isSelected(I)Z

    move-result v10

    if-nez v10, :cond_3

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPressed:Z

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    if-ltz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    if-ltz v10, :cond_4

    instance-of v10, v5, Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;

    if-eqz v10, :cond_2

    move-object v10, v5

    check-cast v10, Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;

    invoke-interface {v10}, Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;->shouldHighlightOnPress()Z

    move-result v10

    if-eqz v10, :cond_4

    :cond_2
    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    invoke-virtual {v5, v10}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    const/4 v12, 0x0

    aget v11, v11, v12

    if-lt v10, v11, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    const/4 v12, 0x0

    aget v11, v11, v12

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v12

    add-int/2addr v11, v12

    if-gt v10, v11, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    const/4 v12, 0x1

    aget v11, v11, v12

    if-lt v10, v11, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLocation:[I

    const/4 v12, 0x1

    aget v11, v11, v12

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v12

    add-int/2addr v11, v12

    if-gt v10, v11, :cond_4

    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v7

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v9

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v6

    if-gt v7, v3, :cond_4

    if-lt v8, v2, :cond_4

    if-gt v9, v0, :cond_4

    if-lt v6, v4, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, v7, v9, v8, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v10, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    add-int/lit8 v1, v1, -0x1

    goto/16 :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Lvedroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    const/high16 v3, 0x43870000

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Lvedroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_0
    const/4 v0, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Lvedroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    const/high16 v3, 0x42b40000

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->rotate(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v5, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Lvedroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :goto_1
    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {p0}, Lvedroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_2
    return-void

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Lvedroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v2

    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v3, 0x43340000

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Lvedroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_1
.end method

.method public final endSelectionMode()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not in selection mode!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidate()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->generateDefaultLayoutParams()Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ColumnGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final getColumnCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    return v0
.end method

.method public final getColumnSize()I
    .locals 6

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v2

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingBottom()I

    move-result v1

    :goto_1
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getHeight()I

    move-result v0

    :goto_2
    sub-int v3, v0, v2

    sub-int/2addr v3, v1

    iget v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    iget v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v4, v5

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    div-int/2addr v3, v4

    return v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingRight()I

    move-result v1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getWidth()I

    move-result v0

    goto :goto_2
.end method

.method public final getFirstVisiblePosition()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    return v0
.end method

.method public final isInSelectionMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSetPressedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v10, -0x1

    const/4 v12, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v7, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    and-int/lit16 v0, v7, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v7, v9

    :goto_1
    return v7

    :pswitch_0
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v10

    float-to-int v10, v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v11

    float-to-int v11, v11

    invoke-virtual {v7, v10, v11}, Landroid/graphics/Point;->set(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSetPressedRunnable:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {p0, v7, v10, v11}, Lcom/google/android/apps/plus/views/ColumnGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v7}, Landroid/view/VelocityTracker;->clear()V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->abortAnimation()V

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v7, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    :goto_2
    invoke-static {p1, v9}, Lvedroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    iput v12, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchRemainder:F

    iget v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    const/4 v10, 0x2

    if-ne v7, v10, :cond_2

    iput v8, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    move v7, v8

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    goto :goto_2

    :cond_2
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-eqz v7, :cond_0

    move v7, v8

    goto :goto_1

    :pswitch_1
    iget v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    invoke-static {p1, v7}, Lvedroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v3

    if-gez v3, :cond_3

    const-string v7, "ColumnGridView"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v10, "onInterceptTouchEvent could not find pointer with id "

    invoke-direct {v8, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " - did we receive an inconsistent event stream?"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v9

    goto :goto_1

    :cond_3
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v7, :cond_6

    invoke-static {p1, v3}, Lvedroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v6

    :goto_3
    iget v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    sub-float v7, v6, v7

    iget v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchRemainder:F

    add-float v2, v7, v10

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v7

    iget v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    int-to-float v10, v10

    cmpl-float v7, v7, v10

    if-lez v7, :cond_7

    move v1, v8

    :goto_4
    if-eqz v1, :cond_4

    cmpl-float v7, v2, v12

    if-lez v7, :cond_8

    iget v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    neg-int v7, v7

    int-to-float v7, v7

    :goto_5
    add-float/2addr v2, v7

    :cond_4
    float-to-int v4, v2

    int-to-float v7, v4

    sub-float v7, v2, v7

    iput v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchRemainder:F

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-interface {v5, v8}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_5
    move v7, v8

    goto/16 :goto_1

    :cond_6
    invoke-static {p1, v3}, Lvedroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v6

    goto :goto_3

    :cond_7
    move v1, v9

    goto :goto_4

    :cond_8
    iget v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    int-to-float v7, v7

    goto :goto_5

    :pswitch_2
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    invoke-virtual {v7, v10, v10}, Landroid/graphics/Point;->set(II)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearPressedState()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->populate()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mInLayout:Z

    sub-int v1, p4, p2

    sub-int v0, p5, p3

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v0, v1}, Lvedroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v0, v1}, Lvedroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    :goto_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->invokeOnItemScrollListener(I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mStartEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v1, v0}, Lvedroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mEndEdge:Lvedroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v1, v0}, Lvedroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/high16 v7, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-eq v2, v7, :cond_0

    const-string v4, "ColumnGridView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onMeasure: must have an exact width or match_parent! Using fallback spec of EXACTLY "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eq v0, v7, :cond_1

    const-string v4, "ColumnGridView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onMeasure: must have an exact height or match_parent! Using fallback spec of EXACTLY "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setMeasuredDimension(II)V

    iget v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCountSetting:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    if-lez v1, :cond_2

    if-lez v3, :cond_2

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v4, :cond_3

    iget v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMinColWidth:I

    div-int v4, v1, v4

    :goto_0
    iput v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    :cond_2
    return-void

    :cond_3
    iget v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMinColWidth:I

    div-int v4, v3, v4

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5
    .param p1    # Landroid/os/Parcelable;

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-super {p0, v2}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mDataChanged:Z

    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    if-nez v2, :cond_0

    iget v2, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    :goto_0
    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    iget v2, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->visibleOffset:I

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    iget v2, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->topOffset:I

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->clear()V

    iget-object v2, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->selectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_1
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    iget-object v3, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->selectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    iget-object v4, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->selectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_0
    iget v2, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    iget v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    div-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    mul-int/2addr v2, v3

    goto :goto_0

    :cond_1
    iget-boolean v2, v1, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->selectionMode:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->requestLayout()V

    return-void
.end method

.method public final onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearPressedState()V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 14

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v8

    new-instance v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;

    invoke-direct {v7, v8}, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v6, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    iget v9, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    iput v6, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    iput v9, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->visibleOffset:I

    if-ltz v6, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v10}, Landroid/widget/ListAdapter;->getCount()I

    move-result v10

    if-ge v6, v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v10, v6}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v10

    iput-wide v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->firstId:J

    :cond_0
    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v10}, Landroid/util/SparseBooleanArray;->size()I

    move-result v10

    new-instance v11, Landroid/util/SparseBooleanArray;

    invoke-direct {v11, v10}, Landroid/util/SparseBooleanArray;-><init>(I)V

    add-int/lit8 v10, v10, -0x1

    :goto_0
    if-ltz v10, :cond_1

    iget-object v12, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v12, v10}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v12

    iget-object v13, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v13, v10}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v10, v10, -0x1

    goto :goto_0

    :cond_1
    iput-object v11, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->selectedPositions:Landroid/util/SparseBooleanArray;

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    iput-boolean v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->selectionMode:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    iget-boolean v10, v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    if-eqz v10, :cond_4

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v10, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    :goto_2
    iget v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    sub-int v10, v2, v10

    sub-int/2addr v10, v5

    iput v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->topOffset:I

    if-eqz v3, :cond_2

    add-int v10, v6, v3

    iput v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    iget v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    if-ltz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v10, :cond_2

    iget v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v11}, Landroid/widget/ListAdapter;->getCount()I

    move-result v11

    if-ge v10, v11, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    iget v11, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->position:I

    invoke-interface {v10, v11}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v10

    iput-wide v10, v7, Lcom/google/android/apps/plus/views/ColumnGridView$SavedState;->firstId:J

    :cond_2
    return-object v7

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getPaddingTop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    goto :goto_2

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 17
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v10, v1, 0xff

    packed-switch v10, :pswitch_data_0

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->reportScrollStateChange(I)V

    const/4 v1, 0x1

    :goto_1
    return v1

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionStartPoint:Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSetPressedRunnable:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->postDelayed(Ljava/lang/Runnable;J)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    if-eqz v14, :cond_1

    const/4 v1, 0x1

    invoke-interface {v14, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    :goto_2
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lvedroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchRemainder:F

    goto :goto_0

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    goto :goto_2

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearPressedState()V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lvedroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v12

    if-gez v12, :cond_3

    const-string v1, "ColumnGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onInterceptTouchEvent could not find pointer with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - did we receive an inconsistent event stream?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v1, :cond_6

    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lvedroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v15

    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    sub-float v1, v15, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchRemainder:F

    add-float v11, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    if-nez v1, :cond_5

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    if-eqz v14, :cond_4

    const/4 v1, 0x1

    invoke-interface {v14, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_4
    const/4 v1, 0x0

    cmpl-float v1, v11, v1

    if-lez v1, :cond_7

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    neg-int v1, v1

    int-to-float v1, v1

    :goto_4
    add-float/2addr v11, v1

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    :cond_5
    float-to-int v13, v11

    int-to-float v1, v13

    sub-float v1, v11, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchRemainder:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->trackMotionScroll(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lvedroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v15

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mTouchSlop:I

    int-to-float v1, v1

    goto :goto_4

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearPressedState()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    goto/16 :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    const/4 v2, -0x1

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearPressedState()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMaximumVelocity:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    invoke-static {v1, v2}, Lvedroid/support/v4/view/VelocityTrackerCompat;->getXVelocity(Landroid/view/VelocityTracker;I)F

    move-result v16

    :goto_5
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(F)F

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFlingVelocity:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_b

    const/4 v1, 0x2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v1, :cond_9

    move/from16 v0, v16

    float-to-int v4, v0

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    if-eqz v1, :cond_a

    const/4 v5, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v6, -0x80000000

    const v7, 0x7fffffff

    const/high16 v8, -0x80000000

    const v9, 0x7fffffff

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mLastTouch:F

    invoke-static/range {p0 .. p0}, Lvedroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :goto_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->checkForSelection(II)V

    goto/16 :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mActivePointerId:I

    invoke-static {v1, v2}, Lvedroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v16

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    goto :goto_6

    :cond_a
    move/from16 v0, v16

    float-to-int v5, v0

    goto :goto_7

    :cond_b
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ColumnGridView;->mScrollState:I

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final registerSelectionListener(Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    return-void
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, -0x1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mCurrentTouchPoint:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearPressedState()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mPopulating:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method public final select(I)V
    .locals 4
    .param p1    # I

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectedPositions:Landroid/util/SparseBooleanArray;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    sub-int v2, p1, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    invoke-interface {v2, v1, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;->onItemSelected(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 3
    .param p1    # Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mObserver:Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->clearAllState()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mDataChanged:Z

    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemCount:I

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mObserver:Lcom/google/android/apps/plus/views/ColumnGridView$AdapterDataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->setViewTypeCount(I)V

    invoke-interface {p1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->endSelectionMode()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->populate()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHasStableIds:Z

    goto :goto_1
.end method

.method public setColumnCount(I)V
    .locals 4
    .param p1    # I

    if-gtz p1, :cond_0

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "colCount must be at least 1 - received "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    if-eq p1, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCountSetting:I

    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mColCount:I

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->populate()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setItemMargin(I)V
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    if-eq p1, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mItemMargin:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->populate()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMinColumnWidth(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mMinColWidth:I

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    return-void
.end method

.method public setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->invokeOnItemScrollListener(I)V

    return-void
.end method

.method public setOrientation(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mHorizontalOrientation:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRatio(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRatio:F

    return-void
.end method

.method public setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRecycler:Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;

    # setter for: Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;->access$402(Lcom/google/android/apps/plus/views/ColumnGridView$RecycleBin;Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;

    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionFromTop(II)V

    return-void
.end method

.method public setSelectionFromTop(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mFirstPosition:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mVisibleOffset:I

    iput p2, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mRestoreOffset:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->requestLayout()V

    goto :goto_0
.end method

.method public setSelectionToTop()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->removeAllViews()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->resetStateForGridTop()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->populate()V

    return-void
.end method

.method public setSelector(I)V
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public final startSelectionMode()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already in selection mode!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionMode:Z

    return-void
.end method

.method public final unregisterSelectionListener()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView;->mSelectionListener:Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;

    return-void
.end method
