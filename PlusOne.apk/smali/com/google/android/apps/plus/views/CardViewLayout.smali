.class public Lcom/google/android/apps/plus/views/CardViewLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "CardViewLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sBackground:Landroid/graphics/drawable/Drawable;

.field private static sInitialized:Z

.field private static sPaddingBottom:I

.field private static sPaddingLeft:I

.field private static sPaddingRight:I

.field private static sPaddingTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/CardViewLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CardViewLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private static adjustPadding(IIZ)I
    .locals 2
    .param p0    # I
    .param p1    # I
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    mul-int/2addr v0, p1

    add-int/2addr v0, p0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method protected init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-boolean v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sInitialized:Z

    if-nez v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingLeft:I

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_border_top_padding:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingTop:I

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_border_right_padding:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingRight:I

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_border_bottom_padding:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    sput v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingBottom:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sBackground:Landroid/graphics/drawable/Drawable;

    sput-boolean v2, Lcom/google/android/apps/plus/views/CardViewLayout;->sInitialized:Z

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/CardViewLayout;->toggleCardBorderStyle(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v3, :cond_2

    move v0, v2

    :goto_0
    new-instance v4, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    if-eqz v0, :cond_1

    move v2, v3

    :cond_1
    const/4 v3, -0x3

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/CardViewLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toggleCardBorderStyle(Z)V
    .locals 6
    .param p1    # Z

    if-eqz p1, :cond_0

    sget-object v4, Lcom/google/android/apps/plus/views/CardViewLayout;->sBackground:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/CardViewLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->getPaddingLeft()I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingLeft:I

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;->adjustPadding(IIZ)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->getPaddingTop()I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingTop:I

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;->adjustPadding(IIZ)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->getPaddingRight()I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingRight:I

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;->adjustPadding(IIZ)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->getPaddingBottom()I

    move-result v4

    sget v5, Lcom/google/android/apps/plus/views/CardViewLayout;->sPaddingBottom:I

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;->adjustPadding(IIZ)I

    move-result v0

    invoke-virtual {p0, v1, v3, v2, v0}, Lcom/google/android/apps/plus/views/CardViewLayout;->setPadding(IIII)V

    return-void

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
