.class public Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;
.super Landroid/view/ViewGroup;
.source "CirclesWithTopAvatarsGridLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;
    }
.end annotation


# instance fields
.field private mCircleHeight:I

.field private mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

.field private mCircleWidth:I

.field private mColumnSeparatorWidth:I

.field private mContext:Landroid/content/Context;

.field private mNumCirclesPerRow:I

.field private mOnClickListener:Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    iget v1, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mColumnSeparatorWidth:I

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;[ILcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # [I
    .param p3    # Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->removeAllViews()V

    iput-object p3, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mOnClickListener:Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;

    array-length v2, p2

    iput v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    new-array v2, v2, [Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    if-ge v0, v2, :cond_1

    aget v1, p2, v0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    new-instance v3, Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->bind(Landroid/database/Cursor;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->addView(Landroid/view/View;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mOnClickListener:Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;

    if-eqz v3, :cond_0

    move-object v2, p1

    check-cast v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->getCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->getCircleName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mOnClickListener:Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;

    invoke-interface {v3, v0, v1}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;->onCircleClicked(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->getPaddingTop()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleHeight:I

    add-int v2, v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->getPaddingLeft()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    iget v4, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v4, v4, v0

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v4, v4, v0

    iget v5, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleWidth:I

    add-int/2addr v5, v1

    invoke-virtual {v4, v1, v3, v5, v2}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->layout(IIII)V

    iget v4, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleWidth:I

    iget v5, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mColumnSeparatorWidth:I

    add-int/2addr v4, v5

    add-int/2addr v1, v4

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    array-length v3, v7

    :goto_0
    if-nez v3, :cond_1

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    :goto_1
    return-void

    :cond_0
    move v3, v6

    goto :goto_0

    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->getPaddingLeft()I

    move-result v7

    sub-int v7, v5, v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->getPaddingRight()I

    move-result v8

    sub-int v0, v7, v8

    iget v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mColumnSeparatorWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    add-int/lit8 v8, v8, -0x1

    mul-int v2, v7, v8

    sub-int v7, v0, v2

    iget v8, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    div-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleWidth:I

    const/4 v1, 0x0

    :goto_2
    iget v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    if-ge v1, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v7, v7, v1

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v7, v7, v1

    iget v8, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleWidth:I

    const/high16 v9, 0x40000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->measure(II)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v6, v7, v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->getMeasuredHeight()I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleHeight:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->getPaddingBottom()I

    move-result v7

    add-int v4, v6, v7

    invoke-virtual {p0, v5, v4}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->setMeasuredDimension(II)V

    goto :goto_1
.end method

.method public onRecycle()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mNumCirclesPerRow:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->mCircleViews:[Lcom/google/android/apps/plus/views/CircleAvatarGridView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->onRecycle()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public requestLayout()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->forceLayout()V

    return-void
.end method
