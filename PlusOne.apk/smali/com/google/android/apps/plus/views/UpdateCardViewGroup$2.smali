.class final Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;
.super Ljava/lang/Object;
.source "UpdateCardViewGroup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->startDelayedShakeAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

.field final synthetic val$rotX:F

.field final synthetic val$rotY:F


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;FF)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->this$0:Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    iput p2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->val$rotX:F

    iput p3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->val$rotY:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    const v3, 0x3f733333

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->this$0:Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->this$0:Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->val$rotX:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->val$rotY:F

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sDampingInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->access$000()Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;->this$0:Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->access$102(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    return-void
.end method
