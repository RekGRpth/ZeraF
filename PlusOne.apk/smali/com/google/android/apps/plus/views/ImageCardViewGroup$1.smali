.class final Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;
.super Ljava/lang/Object;
.source "ImageCardViewGroup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ImageCardViewGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/ImageCardViewGroup;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/ImageCardViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/ImageCardViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    const-wide/16 v6, 0xff

    const/16 v5, 0xff

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/ImageCardViewGroup;

    iget-wide v1, v1, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeEndTime:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v1, v3

    mul-long/2addr v1, v6

    const-wide/16 v3, 0xfa

    div-long/2addr v1, v3

    sub-long v1, v6, v1

    long-to-int v0, v1

    if-le v0, v5, :cond_0

    const/16 v0, 0xff

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/ImageCardViewGroup;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mResizePaintWithAlpha:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;->this$0:Lcom/google/android/apps/plus/views/ImageCardViewGroup;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->invalidate()V

    if-eq v0, v5, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x10

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method
