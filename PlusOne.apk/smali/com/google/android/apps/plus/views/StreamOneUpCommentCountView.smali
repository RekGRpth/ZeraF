.class public Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "StreamOneUpCommentCountView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sCountMarginLeft:I

.field private static sCountPaint:Landroid/text/TextPaint;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sDividerWidth:I

.field private static sMarginLeft:I

.field private static sMarginRight:I


# instance fields
.field private mContentDescriptionDirty:Z

.field private mCount:Ljava/lang/String;

.field private mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mDivider:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_divider_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_count:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_count_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_divider_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_count:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_count_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_divider_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_count:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_comment_count_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_comment_count_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setCount(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->requestLayout()V

    return-void
.end method

.method public invalidate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->invalidate()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getHeight()I

    move-result v6

    int-to-float v3, v7

    int-to-float v4, v6

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v3, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->bottom:F

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginLeft:I

    add-int v10, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getPaddingTop()I

    move-result v11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getMeasuredWidth()I

    move-result v9

    sub-int v0, v9, v10

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sMarginRight:I

    sub-int v8, v0, v1

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v3, v0

    new-instance v0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0, v10, v11}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    add-int/2addr v1, v3

    add-int/2addr v1, v10

    iget v2, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v0, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int v0, v2, v0

    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sDividerWidth:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v11

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mDivider:Landroid/graphics/RectF;

    int-to-float v1, v1

    int-to-float v3, v0

    add-int v4, v10, v8

    sget v5, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->sCountMarginLeft:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    int-to-float v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v11, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getPaddingBottom()I

    move-result v0

    add-int/2addr v0, v11

    invoke-virtual {p0, v9, v0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setMeasuredDimension(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public onRecycle()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    return-void
.end method

.method public setCount(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$plurals;->stream_one_up_comment_count:I

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCount:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mCountLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->mContentDescriptionDirty:Z

    return-void
.end method
