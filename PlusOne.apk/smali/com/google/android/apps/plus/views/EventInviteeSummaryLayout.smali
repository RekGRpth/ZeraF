.class public Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventInviteeSummaryLayout.java"


# static fields
.field private static sFontColor:I

.field private static sFontSize:F

.field private static sGuestsFormat:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sRsvpInvitedFormat:Ljava/lang/String;

.field private static sRsvpInvitedPastFormat:Ljava/lang/String;

.field private static sRsvpMaybeFormat:Ljava/lang/String;

.field private static sRsvpYesFormat:Ljava/lang/String;

.field private static sRsvpYesPastFormat:Ljava/lang/String;


# instance fields
.field private mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

.field private mSize:I

.field private mStatus:Landroid/widget/TextView;

.field private mVisibleSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private static getPeople(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 8
    .param p0    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/PlusEvent;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsEventData;->getInviteeSummary(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Lcom/google/api/services/plusi/model/InviteeSummary;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v7, v4, Lcom/google/api/services/plusi/model/InviteeSummary;->invitee:Ljava/util/List;

    if-eqz v7, :cond_1

    iget-object v7, v4, Lcom/google/api/services/plusi/model/InviteeSummary;->invitee:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Invitee;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v7, :cond_0

    iget-object v7, v2, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v5, v7, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v6, v7, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    if-eqz v5, :cond_0

    new-instance v3, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    invoke-direct {v3}, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;-><init>()V

    iput-object v5, v3, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    iput-object v6, v3, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v7, v4, Lcom/google/api/services/plusi/model/InviteeSummary;->count:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    return v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-boolean v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sInitialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v0, Lcom/google/android/apps/plus/R$string;->event_detail_rsvp_yes_count:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpYesFormat:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_detail_rsvp_yes_count_past:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpYesPastFormat:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_details_rsvp_guests_count:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sGuestsFormat:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_detail_rsvp_maybe_count:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpMaybeFormat:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_detail_rsvp_invited_count:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpInvitedFormat:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_detail_rsvp_invited_count_past:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sRsvpInvitedPastFormat:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_details_rsvp_count_size:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontSize:F

    sget v0, Lcom/google/android/apps/plus/R$color;->event_card_details_rsvp_count_color:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontColor:I

    sput-boolean v6, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sInitialized:Z

    :cond_0
    sget v3, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sFontColor:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->addView(Landroid/view/View;)V

    iput v5, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    iput v5, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;Z)V
    .locals 7
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/views/EventActionListener;
    .param p3    # Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "ATTENDING"

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getPeople(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    add-int/lit8 v1, v2, 0x0

    const-string v2, "MAYBE"

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getPeople(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    add-int/2addr v1, v2

    const-string v2, "NOT_RESPONDED"

    invoke-static {p1, v2, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getPeople(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    iget v3, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    invoke-virtual {v2, v0, p2, v3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->bind(Ljava/util/ArrayList;Lcom/google/android/apps/plus/views/EventActionListener;I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    sget-object v2, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->sGuestsFormat:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->requestLayout()V

    return-void
.end method

.method public final clear()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    iput v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    return-void
.end method

.method protected measureChildren(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, -0x80000000

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-static {v2, v1, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->measure(Landroid/view/View;IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-static {v2, v3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setCorner(Landroid/view/View;II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mStatus:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mVisibleSize:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-static {v2, v1, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->measure(Landroid/view/View;IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setCorner(Landroid/view/View;II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mLineupLayout:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getMeasuredHeight()I

    :cond_0
    return-void
.end method

.method public final size()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->mSize:I

    return v0
.end method
