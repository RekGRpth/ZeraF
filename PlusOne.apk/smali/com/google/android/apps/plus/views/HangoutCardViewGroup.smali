.class public Lcom/google/android/apps/plus/views/HangoutCardViewGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "HangoutCardViewGroup.java"


# instance fields
.field private mAvatarsToDisplay:I

.field private mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

.field protected mDisplayAuthorGaiaId:Ljava/lang/String;

.field protected mDisplayAuthorName:Ljava/lang/String;

.field private final mHangoutAvatars:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableAvatar;",
            ">;"
        }
    .end annotation
.end field

.field private mHangoutBackground:Landroid/graphics/Rect;

.field private mHangoutTitleLayout:Landroid/text/StaticLayout;

.field private mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mTitleCorner:Landroid/graphics/Point;

.field private mUnsupportedLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutBackground:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mTitleCorner:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->bindResources()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final createHero(III)I
    .locals 25
    .param p1    # I
    .param p2    # I
    .param p3    # I

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v13, p1, v3

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v6, p3, v3

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v24, p2, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v23

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-object/from16 v0, v23

    if-ne v0, v3, :cond_1

    const/16 v20, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isInProgress()Z

    move-result v19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v19, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$string;->card_hangout_state_active:I

    :goto_1
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorName:Ljava/lang/String;

    aput-object v8, v5, v7

    invoke-virtual {v4, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Landroid/text/StaticLayout;

    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct/range {v3 .. v10}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int v24, v24, v3

    if-nez v20, :cond_0

    if-nez v19, :cond_4

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getNumAttendees()I

    move-result v22

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->maxHangoutAvatarsToDisplay:I

    move/from16 v0, v22

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarMargin:I

    sget-object v5, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int/2addr v4, v5

    div-int v4, v6, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAvatarsToDisplay:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAvatarsToDisplay:I

    if-lez v3, :cond_4

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v24, v24, v3

    move/from16 v17, v13

    const/16 v21, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAvatarsToDisplay:I

    move/from16 v0, v21

    if-ge v0, v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v3, v3, v17

    sget-object v4, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v4, v4, v24

    move-object/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    sget-object v4, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarMargin:I

    add-int/2addr v3, v4

    add-int v17, v17, v3

    add-int/lit8 v21, v21, 0x1

    goto :goto_2

    :cond_1
    const/16 v20, 0x0

    goto/16 :goto_0

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$string;->card_hangout_state_inactive:I

    goto/16 :goto_1

    :cond_3
    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v24, v24, v3

    :cond_4
    if-eqz v19, :cond_5

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->yPadding:I

    add-int v24, v24, v3

    if-nez v20, :cond_6

    const/4 v3, 0x6

    move-object/from16 v0, v18

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v9

    new-instance v7, Landroid/text/StaticLayout;

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->getErrorMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    sget-object v11, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v12, 0x3f800000

    const/4 v13, 0x0

    const/4 v14, 0x0

    move/from16 v10, p3

    invoke-direct/range {v7 .. v14}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mUnsupportedLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mUnsupportedLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int v24, v24, v3

    :cond_5
    :goto_3
    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v24, v24, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutBackground:Landroid/graphics/Rect;

    add-int v4, p1, p3

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, v24

    invoke-virtual {v3, v0, v1, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    return v24

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isJoinable()Z

    move-result v3

    if-eqz v3, :cond_7

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_enter_greenroom:I

    :goto_4
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    const/4 v12, 0x1

    move-object/from16 v10, v18

    move/from16 v14, v24

    move-object/from16 v15, p0

    invoke-static/range {v10 .. v15}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButton(Landroid/content/Context;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int v24, v24, v3

    goto :goto_3

    :cond_7
    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_broadcast_view:I

    goto :goto_4
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v5, p3, v7

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v3, p2, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutBackground:Landroid/graphics/Rect;

    sget-object v8, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->hangoutBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    int-to-float v7, v3

    int-to-float v8, v5

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v7, v3

    int-to-float v7, v7

    neg-int v8, v5

    int-to-float v8, v8

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    add-int/2addr v5, v7

    const/4 v2, 0x0

    :goto_0
    iget v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAvatarsToDisplay:I

    if-ge v2, v7, :cond_3

    if-nez v2, :cond_0

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v5, v7

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->authorBitmap:Landroid/graphics/Bitmap;

    :cond_1
    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v7, v4, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAvatarsToDisplay:I

    add-int/lit8 v7, v7, -0x1

    if-ne v2, v7, :cond_2

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int/2addr v5, v7

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v7, :cond_4

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->yPadding:I

    add-int/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v7, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/2addr v5, v7

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mUnsupportedLayout:Landroid/text/StaticLayout;

    if-eqz v7, :cond_5

    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->yPadding:I

    add-int/2addr v5, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mUnsupportedLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getWidth()I

    move-result v7

    sub-int v7, p4, v7

    div-int/lit8 v7, v7, 0x2

    add-int v6, p2, v7

    int-to-float v7, v6

    int-to-float v8, v5

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mUnsupportedLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v7, v6

    int-to-float v7, v7

    neg-int v8, v5

    int-to-float v8, v8

    invoke-virtual {p1, v7, v8}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mUnsupportedLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    add-int/2addr v5, v7

    :cond_5
    sget-object v7, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v7, v5

    return v7
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 18
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v3, 0x21

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v12

    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorGaiaId:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorName:Ljava/lang/String;

    invoke-static {v12}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedHangout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getNumAttendees()I

    move-result v15

    sget-object v3, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->maxHangoutAvatarsToDisplay:I

    invoke-static {v3, v15}, Ljava/lang/Math;->min(II)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeGaiaIds()Ljava/util/ArrayList;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeNames()Ljava/util/ArrayList;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeAvatarUrls()Ljava/util/ArrayList;

    move-result-object v9

    const/4 v13, 0x0

    :goto_2
    if-ge v13, v14, :cond_2

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v10, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v9, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x2

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAuthorGaiaId:Ljava/lang/String;

    move-object/from16 v16, v0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mAuthorName:Ljava/lang/String;

    move-object/from16 v17, v0

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isJoinable()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https://www.youtube.com/watch?v="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getYoutubeLiveId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "com.google.android.youtube"

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/hangout/Utils;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.google.android.youtube"

    const-string v3, "com.google.android.youtube.WatchActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDbEmbedHangout:Lcom/google/android/apps/plus/content/DbEmbedHangout;

    invoke-static {v2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/service/Hangout;->enterGreenRoom(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutBackground:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mTitleCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Point;->set(II)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mJoinButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mDisplayAuthorName:Ljava/lang/String;

    return-void
.end method

.method public unbindResources()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->unbindResources()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;->mHangoutAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
