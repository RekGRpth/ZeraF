.class public Lcom/google/android/apps/plus/views/CircleListItemView;
.super Lcom/google/android/apps/plus/views/CheckableListItemView;
.source "CircleListItemView.java"


# static fields
.field private static final sCircleTypeIcons:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private static sDefaultCircleDrawable:Landroid/graphics/drawable/Drawable;


# instance fields
.field private final mCircleIconBounds:Landroid/graphics/Rect;

.field private mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

.field private final mCircleIconSize:I

.field private mCircleId:Ljava/lang/String;

.field private mCircleName:Ljava/lang/String;

.field private mCircleType:I

.field private final mCountTextView:Landroid/widget/TextView;

.field private final mGapBetweenCountAndCheckBox:I

.field private final mGapBetweenIconAndText:I

.field private final mGapBetweenNameAndCount:I

.field private mHighlightedText:Ljava/lang/String;

.field private mMemberCount:I

.field private mMemberCountShown:Z

.field private mMemberCountVisible:Z

.field private final mNameTextBuilder:Landroid/text/SpannableStringBuilder;

.field private final mNameTextView:Landroid/widget/TextView;

.field private final mPaddingBottom:I

.field private final mPaddingLeft:I

.field private final mPaddingRight:I

.field private final mPaddingTop:I

.field private final mPreferredHeight:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/CircleListItemView;->sCircleTypeIcons:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CircleListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const v11, 0x1030044

    const/16 v10, 0x10

    const/4 v9, -0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CheckableListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountVisible:Z

    sget-object v5, Lcom/google/android/apps/plus/R$styleable;->CircleListItemView:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v7, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPreferredHeight:I

    invoke-virtual {v0, v8, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingTop:I

    const/4 v5, 0x2

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingBottom:I

    const/4 v5, 0x3

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingLeft:I

    const/4 v5, 0x4

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingRight:I

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    const/4 v5, 0x7

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    const/16 v5, 0x9

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenNameAndCount:I

    const/4 v5, 0x5

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenIconAndText:I

    const/16 v5, 0x8

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    const/16 v5, 0xb

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    const/16 v5, 0xa

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenCountAndCheckBox:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v5, p1, v11}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextSize(F)V

    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/CircleListItemView;->addView(Landroid/view/View;)V

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v5, p1, v11}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    new-instance v6, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v6, v9, v9}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/CircleListItemView;->addView(Landroid/view/View;)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconBounds:Landroid/graphics/Rect;

    sget-object v5, Lcom/google/android/apps/plus/views/CircleListItemView;->sDefaultCircleDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$drawable;->list_public:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    sput-object v5, Lcom/google/android/apps/plus/views/CircleListItemView;->sDefaultCircleDrawable:Landroid/graphics/drawable/Drawable;

    :cond_1
    sget-object v5, Lcom/google/android/apps/plus/views/CircleListItemView;->sDefaultCircleDrawable:Landroid/graphics/drawable/Drawable;

    iput-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method


# virtual methods
.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;

    iget v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleType:I

    const/4 v1, -0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconBounds:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/CheckableListItemView;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final getCircleId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCircleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    return-object v0
.end method

.method public final getCircleType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleType:I

    return v0
.end method

.method public final getMemberCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCount:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 24
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingLeft:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v16

    move/from16 v19, v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountShown:Z

    move/from16 v21, v0

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    move/from16 v0, v16

    invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I

    move-result v19

    :cond_0
    sub-int v21, p5, p3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    div-int/lit8 v13, v21, 0x2

    sub-int v21, p5, p3

    sub-int v21, v21, v19

    div-int/lit8 v20, v21, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    move/from16 v21, v0

    sub-int v21, v21, v12

    div-int/lit8 v21, v21, 0x2

    add-int v5, v13, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    move/from16 v21, v0

    sub-int v21, v21, v14

    div-int/lit8 v21, v21, 0x2

    add-int v4, v15, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconBounds:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    add-int v22, v4, v14

    add-int v23, v5, v12

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenIconAndText:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int v15, v15, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingRight:I

    move/from16 v21, v0

    sub-int v18, p4, v21

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBoxVisible:Z

    move/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v6

    sub-int v21, p5, p3

    sub-int v21, v21, v6

    div-int/lit8 v7, v21, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v21, v0

    sub-int v22, v18, v8

    add-int v23, v7, v6

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v18

    move/from16 v3, v23

    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/widget/CheckBox;->layout(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenCountAndCheckBox:I

    move/from16 v21, v0

    add-int v21, v21, v8

    sub-int v18, v18, v21

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountShown:Z

    move/from16 v21, v0

    if-eqz v21, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v11

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v21

    sub-int v22, v18, v15

    sub-int v22, v22, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenNameAndCount:I

    move/from16 v23, v0

    sub-int v22, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->min(II)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    add-int v22, v15, v17

    add-int v23, v20, v19

    move-object/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v15, v1, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountShown:Z

    move/from16 v21, v0

    if-eqz v21, :cond_2

    add-int v21, v15, v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenNameAndCount:I

    move/from16 v22, v0

    add-int v10, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    move-object/from16 v21, v0

    add-int v22, v20, v19

    move-object/from16 v0, v21

    move/from16 v1, v20

    move/from16 v2, v18

    move/from16 v3, v22

    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    :cond_2
    return-void

    :cond_3
    const/4 v11, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/high16 v7, 0x40000000

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    const/4 v0, 0x0

    iget v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingLeft:I

    sub-int v4, v3, v4

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingRight:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenIconAndText:I

    sub-int v1, v4, v5

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBoxVisible:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6, p2}, Landroid/widget/CheckBox;->measure(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v4

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenCountAndCheckBox:I

    add-int/2addr v4, v5

    sub-int/2addr v1, v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v6, v6}, Landroid/widget/TextView;->measure(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountShown:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v6, v6}, Landroid/widget/TextView;->measure(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mGapBetweenNameAndCount:I

    add-int/2addr v4, v5

    sub-int/2addr v1, v4

    :cond_1
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconSize:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    iget v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingTop:I

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPaddingBottom:I

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    iget v4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mPreferredHeight:I

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/CircleListItemView;->setMeasuredDimension(II)V

    return-void
.end method

.method public setCircle(Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z

    const/4 v0, 0x1

    const/4 v8, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleId:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleType:I

    iput p4, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCount:I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountVisible:Z

    if-eqz v1, :cond_3

    if-eq p2, v0, :cond_0

    const/4 v1, 0x5

    if-eq p2, v1, :cond_0

    const/16 v1, 0xa

    if-ne p2, v1, :cond_3

    :cond_0
    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountShown:Z

    move v6, p2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    const-string v0, "v.whatshot"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v6, -0x2

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/CircleListItemView;->sCircleTypeIcons:Landroid/util/SparseArray;

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    sparse-switch v6, :sswitch_data_0

    iput-object p3, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/CircleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v5, Lcom/google/android/apps/plus/views/CircleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    sparse-switch v6, :sswitch_data_1

    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_circle:I

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    sget-object v0, Lcom/google/android/apps/plus/views/CircleListItemView;->sCircleTypeIcons:Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountShown:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    return-void

    :cond_3
    move v0, v8

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->acl_public:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    goto :goto_1

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->acl_extended_network:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->acl_your_circles:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->stream_whats_hot:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    goto/16 :goto_1

    :sswitch_4
    if-eqz p5, :cond_4

    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_public_red:I

    :goto_4
    goto :goto_2

    :cond_4
    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_public:I

    goto :goto_4

    :sswitch_5
    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_domain:I

    goto :goto_2

    :sswitch_6
    if-eqz p5, :cond_5

    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_extended_red:I

    :goto_5
    goto/16 :goto_2

    :cond_5
    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_extended:I

    goto :goto_5

    :sswitch_7
    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_circle_blocked:I

    goto/16 :goto_2

    :sswitch_8
    sget v7, Lcom/google/android/apps/plus/R$drawable;->ic_circles_active:I

    goto/16 :goto_2

    :sswitch_9
    sget v7, Lcom/google/android/apps/plus/R$drawable;->ic_private:I

    goto/16 :goto_2

    :sswitch_a
    sget v7, Lcom/google/android/apps/plus/R$drawable;->list_whats_hot:I

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCountTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2 -> :sswitch_3
        0x5 -> :sswitch_2
        0x7 -> :sswitch_1
        0x9 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x2 -> :sswitch_a
        0x5 -> :sswitch_8
        0x7 -> :sswitch_6
        0x8 -> :sswitch_5
        0x9 -> :sswitch_4
        0xa -> :sswitch_7
        0x65 -> :sswitch_9
    .end sparse-switch
.end method

.method public setHighlightedText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mHighlightedText:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mHighlightedText:Ljava/lang/String;

    goto :goto_0
.end method

.method public setMemberCountVisible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCountVisible:Z

    return-void
.end method

.method public final updateContentDescription()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$plurals;->circle_entry_content_description:I

    iget v2, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCount:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mCircleName:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleListItemView;->mMemberCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
