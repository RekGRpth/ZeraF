.class final Lcom/google/android/apps/plus/views/TypeableAudienceView$2;
.super Ljava/lang/Object;
.source "TypeableAudienceView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/TypeableAudienceView;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/TypeableAudienceView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$2;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$2;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "input_method"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v1

    packed-switch p2, :pswitch_data_0

    :cond_0
    move v2, v3

    :goto_0
    return v2

    :pswitch_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$2;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/TypeableAudienceView;->mEditText:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getSelectionStart()I

    move-result v4

    if-gtz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$2;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/TypeableAudienceView;->mEditText:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->getSelectionEnd()I

    move-result v4

    if-gtz v4, :cond_0

    if-nez v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$2;->this$0:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->removeLastChip()V

    goto :goto_0

    :pswitch_1
    if-eqz v1, :cond_0

    invoke-virtual {v0, v3, v3}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x42
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
