.class public Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;
.super Landroid/view/ViewGroup;
.source "ProfileAboutButtonsLayout.java"


# static fields
.field private static sInitialized:Z

.field private static sMargin:I

.field private static sPlusButtonPadding:I


# instance fields
.field private mCirclesButton:Landroid/view/View;

.field private mExpandButton:Landroid/view/View;

.field private mPlusOneButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->init(Landroid/content/Context;)V

    return-void
.end method

.method private static init(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-boolean v0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sInitialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_content_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sMargin:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->clickable_button_horizontal_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sPlusButtonPadding:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sInitialized:Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    sget v0, Lcom/google/android/apps/plus/R$id;->circles:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->plus_one:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->expand:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v5, 0x0

    sub-int v2, p4, p2

    sub-int v0, p5, p3

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v3, v5, v5, v4, v0}, Landroid/view/View;->layout(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/lit8 v1, v3, 0x0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    sget v3, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sMargin:I

    add-int/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v1, v5, v4, v0}, Landroid/view/View;->layout(IIII)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v2, v4

    invoke-virtual {v3, v4, v5, v2, v0}, Landroid/view/View;->layout(IIII)V

    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/4 v10, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    sget v6, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sPlusButtonPadding:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sPlusButtonPadding:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/view/View;->setPadding(IIII)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5, v10, p2}, Landroid/view/View;->measure(II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v5, v10, p2}, Landroid/view/View;->measure(II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    invoke-virtual {v5, v10, p2}, Landroid/view/View;->measure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/4 v1, 0x0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->sMargin:I

    add-int/2addr v5, v6

    add-int/lit8 v1, v5, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v1, v5

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mExpandButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_1
    if-nez v2, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v1, v5

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_2
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->setMeasuredDimension(II)V

    return-void

    :cond_3
    sub-int v3, v4, v1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    if-le v5, v3, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    const/high16 v6, -0x80000000

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, p2}, Landroid/view/View;->measure(II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutButtonsLayout;->mCirclesButton:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_4
    move v1, v4

    goto :goto_0
.end method
