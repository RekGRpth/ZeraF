.class public abstract Lcom/google/android/apps/plus/views/CardView;
.super Landroid/view/View;
.source "CardView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field protected static sBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field protected static sBottomBorderPadding:I

.field private static sCardViewInitialized:Z

.field protected static sDefaultTextPaint:Landroid/text/TextPaint;

.field protected static sDrawRect:Landroid/graphics/Rect;

.field protected static sLeftBorderPadding:I

.field protected static sPressedStateBackground:Landroid/graphics/drawable/Drawable;

.field protected static final sResizePaint:Landroid/graphics/Paint;

.field protected static sRightBorderPadding:I

.field protected static sTopBorderPadding:I

.field protected static sXDoublePadding:I

.field protected static sXPadding:I

.field protected static sYDoublePadding:I

.field protected static sYPadding:I


# instance fields
.field protected mBackgroundRect:Landroid/graphics/Rect;

.field private final mClickableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field protected mDisplaySizeType:I

.field protected mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field protected mPaddingEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/CardView;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, -0x2

    iput v1, p0, Lcom/google/android/apps/plus/views/CardView;->mDisplaySizeType:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    sget-boolean v1, Lcom/google/android/apps/plus/views/CardView;->sCardViewInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/CardView;->sCardViewInitialized:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_default_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/CardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_right_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sRightBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_top_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_border_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sBottomBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sXPadding:I

    mul-int/lit8 v1, v1, 0x2

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sXDoublePadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_y_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sYPadding:I

    mul-int/lit8 v1, v1, 0x2

    sput v1, Lcom/google/android/apps/plus/views/CardView;->sYDoublePadding:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/CardView;->sDrawRect:Landroid/graphics/Rect;

    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CardView;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CardView;->setFocusable(Z)V

    return-void
.end method


# virtual methods
.method public final addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final bindResources()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->onBindResources()V

    :cond_0
    return-void
.end method

.method protected abstract draw(Landroid/graphics/Canvas;IIII)I
.end method

.method protected drawableStateChanged()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    return-void
.end method

.method public final init$51b687e2(ILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/view/View$OnClickListener;
    .param p3    # Lcom/google/android/apps/plus/views/ItemClickListener;

    iput p1, p0, Lcom/google/android/apps/plus/views/CardView;->mDisplaySizeType:I

    iput-object p2, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/CardView;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    return-void
.end method

.method protected abstract layoutElements(IIII)I
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->bindResources()V

    return-void
.end method

.method protected onBindResources()V
    .locals 0

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->onUnbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    const/4 v12, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->getHeight()I

    move-result v6

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    if-eqz v0, :cond_2

    sget v9, Lcom/google/android/apps/plus/views/CardView;->sXPadding:I

    sget v11, Lcom/google/android/apps/plus/views/CardView;->sYPadding:I

    sget v8, Lcom/google/android/apps/plus/views/CardView;->sXDoublePadding:I

    sget v10, Lcom/google/android/apps/plus/views/CardView;->sYDoublePadding:I

    :goto_0
    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int v2, v0, v9

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int v3, v0, v11

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int/2addr v0, v8

    sget v1, Lcom/google/android/apps/plus/views/CardView;->sRightBorderPadding:I

    add-int/2addr v0, v1

    sub-int v4, v7, v0

    sget v0, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int/2addr v0, v10

    sget v1, Lcom/google/android/apps/plus/views/CardView;->sBottomBorderPadding:I

    add-int/2addr v0, v1

    sub-int v5, v6, v0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/CardView;->draw(Landroid/graphics/Canvas;IIII)I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v12, v12, v7, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/CardView;->sPressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void

    :cond_2
    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/4 v10, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-gtz v1, :cond_0

    move v0, v2

    :goto_0
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    if-eqz v7, :cond_1

    sget v4, Lcom/google/android/apps/plus/views/CardView;->sXPadding:I

    sget v6, Lcom/google/android/apps/plus/views/CardView;->sYPadding:I

    sget v3, Lcom/google/android/apps/plus/views/CardView;->sXDoublePadding:I

    sget v5, Lcom/google/android/apps/plus/views/CardView;->sYDoublePadding:I

    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/CardView;->setMeasuredDimension(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->getMeasuredHeight()I

    move-result v9

    invoke-virtual {v7, v10, v10, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    sget v7, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int/2addr v7, v4

    sget v8, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int/2addr v8, v6

    sget v9, Lcom/google/android/apps/plus/views/CardView;->sLeftBorderPadding:I

    add-int/2addr v9, v3

    sget v10, Lcom/google/android/apps/plus/views/CardView;->sRightBorderPadding:I

    add-int/2addr v9, v10

    sub-int v9, v2, v9

    sget v10, Lcom/google/android/apps/plus/views/CardView;->sTopBorderPadding:I

    add-int/2addr v10, v5

    sget v11, Lcom/google/android/apps/plus/views/CardView;->sBottomBorderPadding:I

    add-int/2addr v10, v11

    sub-int v10, v0, v10

    invoke-virtual {p0, v7, v8, v9, v10}, Lcom/google/android/apps/plus/views/CardView;->layoutElements(IIII)I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    goto :goto_1
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->onUnbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardView;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->clearAnimation()V

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v4, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    return v5

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    :goto_1
    if-ltz v1, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v2, v3, v4, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :pswitch_2
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v5, :cond_3

    iput-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/views/CardView;->setPressed(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    :cond_3
    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v1, v5, -0x1

    :goto_2
    if-ltz v1, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    or-int/2addr v0, v5

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_4
    if-nez v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v5, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :pswitch_3
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v3, v4, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/CardView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->invalidate()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onUnbindResources()V
    .locals 0

    return-void
.end method

.method public final removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardView;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setPaddingEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CardView;->mPaddingEnabled:Z

    return-void
.end method

.method public final unbindResources()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CardView;->onUnbindResources()V

    return-void
.end method
