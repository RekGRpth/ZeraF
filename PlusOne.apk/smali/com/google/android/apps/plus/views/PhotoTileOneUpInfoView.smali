.class public Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;
.super Landroid/view/View;
.source "PhotoTileOneUpInfoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sAvatarMarginLeft:I

.field private static sAvatarMarginRight:I

.field private static sAvatarMarginTop:I

.field private static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sCaptionMarginTop:I

.field private static sCaptionPaint:Landroid/text/TextPaint;

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sFontSpacing:F

.field private static sMarginBottom:I

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sNameMarginTop:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mBackgroundOffset:I

.field private mCaption:Landroid/text/Spannable;

.field private mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mOwnerId:Ljava/lang/String;

.field private mOwnerName:Ljava/lang/String;

.field private mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_info_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNameMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_caption_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionMarginTop:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_content:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_info_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNameMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_caption_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionMarginTop:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_content:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_info_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNameMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_caption_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionMarginTop:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_content:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->tile_photo_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v4, v5

    :cond_0
    :goto_0
    return v4

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->invalidate()V

    goto :goto_1

    :pswitch_2
    iput-object v7, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->invalidate()V

    move v4, v5

    goto :goto_0

    :pswitch_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->invalidate()V

    goto :goto_0

    :cond_3
    move v4, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mBackgroundOffset:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2
    return-void

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 14
    .param p1    # I
    .param p2    # I

    invoke-super/range {p0 .. p2}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginLeft:I

    add-int v11, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getPaddingTop()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getMeasuredWidth()I

    move-result v10

    sub-int v0, v10, v11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginRight:I

    sub-int v9, v0, v1

    iput v12, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mBackgroundOffset:I

    sget v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginLeft:I

    add-int/2addr v0, v11

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginTop:I

    add-int/2addr v1, v12

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget v3, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    add-int/2addr v3, v0

    sget v4, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    sget v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginLeft:I

    add-int/2addr v0, v11

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    add-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginRight:I

    add-int v13, v0, v1

    sget v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNameMarginTop:I

    add-int/2addr v0, v12

    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginTop:I

    add-int v8, v0, v1

    sub-int v0, v9, v13

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mOwnerName:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v3, v0

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v2, v3, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    const/4 v3, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    float-to-int v2, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v3

    new-instance v0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0, v13, v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v8, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDate:Ljava/lang/String;

    if-eqz v0, :cond_1

    sub-int v3, v9, v13

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v0, " "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0, v13, v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v8

    :goto_0
    sget v1, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sAvatarMarginTop:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v12

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v12

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    sget v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sMarginBottom:I

    add-int/2addr v0, v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v10, v0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionMarginTop:I

    add-int v13, v12, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaption:Landroid/text/Spannable;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sCaptionPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    move v3, v9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, v11, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v12

    goto :goto_1

    :cond_1
    move v0, v8

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaption:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    return-void
.end method

.method public setCaption(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mCaption:Landroid/text/Spannable;

    :cond_0
    return-void
.end method

.method public setDate(J)V
    .locals 2
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mDate:Ljava/lang/String;

    return-void
.end method

.method public setOwner(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mOwnerId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mOwnerId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mOwnerName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mOwnerName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    const/4 v6, 0x2

    move-object v1, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setTileOneUpListener(Lcom/google/android/apps/plus/views/TileOneUpListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/TileOneUpListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTileOneUpInfoView;->mTileOneUpListener:Lcom/google/android/apps/plus/views/TileOneUpListener;

    return-void
.end method
