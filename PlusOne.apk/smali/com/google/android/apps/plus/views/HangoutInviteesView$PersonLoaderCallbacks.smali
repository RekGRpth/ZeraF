.class final Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;
.super Ljava/lang/Object;
.source "HangoutInviteesView.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/HangoutInviteesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PersonLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    # getter for: Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$200(Lcom/google/android/apps/plus/views/HangoutInviteesView;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    # getter for: Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInviteeId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$300(Lcom/google/android/apps/plus/views/HangoutInviteesView;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v7

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks$1;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    # getter for: Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v3}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$200(Lcom/google/android/apps/plus/views/HangoutInviteesView;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    # getter for: Lcom/google/android/apps/plus/views/HangoutInviteesView;->INVITEE_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$400()[Ljava/lang/String;

    move-result-object v4

    const-string v5, "gaia_id=?"

    const/4 v1, 0x1

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    # getter for: Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInviteeId:Ljava/lang/String;
    invoke-static {v8}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$300(Lcom/google/android/apps/plus/views/HangoutInviteesView;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v1

    move-object v1, p0

    move-object v8, v2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks$1;-><init>(Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    const/4 v1, 0x0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPackedCircleIds:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$502(Lcom/google/android/apps/plus/views/HangoutInviteesView;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->access$100(Lcom/google/android/apps/plus/views/HangoutInviteesView;)V

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
