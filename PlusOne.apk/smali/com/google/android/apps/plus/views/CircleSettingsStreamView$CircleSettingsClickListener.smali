.class public interface abstract Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;
.super Ljava/lang/Object;
.source "CircleSettingsStreamView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/CircleSettingsStreamView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CircleSettingsClickListener"
.end annotation


# virtual methods
.method public abstract onAvatarClicked(Ljava/lang/String;)V
.end method

.method public abstract onCircleCountClicked()V
.end method

.method public abstract onSettingsClicked()V
.end method
