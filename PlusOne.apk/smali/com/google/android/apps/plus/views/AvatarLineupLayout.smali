.class public Lcom/google/android/apps/plus/views/AvatarLineupLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "AvatarLineupLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sAvatarLineupItemPadding:I

.field private static sAvatarLineupItemSize:I

.field private static sDescriptionFontColor:I

.field private static sDescriptionFontSize:F

.field private static sInitialized:Z


# instance fields
.field private mAvatars:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/AvatarView;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mOverflowText:Landroid/widget/TextView;

.field private mPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalPeopleCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v6, 0x1

    sget-boolean v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sInitialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_avatar_lineup_item_padding:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemPadding:I

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_avatar_lineup_item_size:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_avatar_lineup_overflow_text_size:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontSize:F

    sget v0, Lcom/google/android/apps/plus/R$color;->avatar_lineup_overflow_text_color:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontColor:I

    sput-boolean v6, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sInitialized:Z

    :cond_0
    sget v3, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontSize:F

    sget v4, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontColor:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->addView(Landroid/view/View;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final bind(Ljava/util/ArrayList;Lcom/google/android/apps/plus/views/EventActionListener;I)V
    .locals 3
    .param p2    # Lcom/google/android/apps/plus/views/EventActionListener;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;",
            "Lcom/google/android/apps/plus/views/EventActionListener;",
            "I)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iput p3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->requestLayout()V

    return-void
.end method

.method public final clear()V
    .locals 5

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/AvatarView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->removeView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    return-void
.end method

.method protected measureChildren(II)V
    .locals 25
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v19

    sget v20, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    sget v21, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemPadding:I

    add-int v16, v20, v21

    div-int v8, v19, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v7, 0x0

    if-lt v8, v13, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    move/from16 v20, v0

    move/from16 v0, v20

    if-le v0, v13, :cond_4

    :cond_0
    const/4 v9, 0x0

    const/4 v5, 0x0

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    move/from16 v20, v0

    sub-int v11, v20, v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    sget v21, Lcom/google/android/apps/plus/R$plurals;->event_invitee_other_count:I

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v11, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    sget v22, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v23, -0x80000000

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->measure(II)V

    mul-int v20, v13, v16

    sub-int v9, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v20

    move/from16 v0, v20

    if-ge v9, v0, :cond_3

    const/4 v5, 0x1

    :goto_0
    if-eqz v5, :cond_2

    add-int/lit8 v13, v13, -0x1

    :cond_2
    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/high16 v21, -0x80000000

    move/from16 v0, v21

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    sget v22, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v23, -0x80000000

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/widget/TextView;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    mul-int v21, v16, v13

    const/16 v22, 0x0

    sget v23, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v24

    sub-int v23, v23, v24

    div-int/lit8 v23, v23, 0x2

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->max(II)I

    move-result v22

    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->setCorner(Landroid/view/View;II)V

    :goto_1
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    sub-int v21, v13, v21

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v4

    move v3, v4

    :goto_2
    if-lez v3, :cond_5

    new-instance v10, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->addView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const/16 v21, 0x8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/4 v6, 0x0

    :goto_3
    move/from16 v0, v18

    if-ge v6, v0, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/views/AvatarView;

    if-ge v6, v13, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    iget-object v0, v14, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v15, v14, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_6

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_6
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    sget v20, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v21, 0x40000000

    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    sget v21, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v22, 0x40000000

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    if-lez v6, :cond_7

    sget v20, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemPadding:I

    :goto_4
    add-int v7, v7, v20

    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-static {v0, v7, v1}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->setCorner(Landroid/view/View;II)V

    sget v20, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    add-int v7, v7, v20

    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_7
    const/16 v20, 0x0

    goto :goto_4

    :cond_8
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    const/16 v20, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_5

    :cond_9
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    check-cast p1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onAvatarClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
