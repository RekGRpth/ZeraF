.class public Lcom/google/android/apps/plus/views/PhotoView;
.super Landroid/view/View;
.source "PhotoView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;,
        Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;,
        Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;,
        Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;,
        Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;
    }
.end annotation


# static fields
.field private static sBackgroundColor:I

.field private static sCommentBitmap:Landroid/graphics/Bitmap;

.field private static sCommentCountLeftMargin:I

.field private static sCommentCountPaint:Landroid/text/TextPaint;

.field private static sCommentCountTextWidth:I

.field private static sCropDimPaint:Landroid/graphics/Paint;

.field private static sCropPaint:Landroid/graphics/Paint;

.field private static sCropSizeCoverWidth:I

.field private static sCropSizeProfile:I

.field private static sHasMultitouchDistinct:Z

.field private static sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sInitialized:Z

.field private static sMediaNotFoundTextPaint:Landroid/text/TextPaint;

.field private static sMediaNotFoundTitle:Ljava/lang/String;

.field private static sPanoramaImage:Landroid/graphics/Bitmap;

.field private static sPhotoOverlayBottomPadding:I

.field private static sPhotoOverlayRightPadding:I

.field private static sPlusOneBitmap:Landroid/graphics/Bitmap;

.field private static sPlusOneBottomMargin:I

.field private static sPlusOneCountLeftMargin:I

.field private static sPlusOneCountPaint:Landroid/text/TextPaint;

.field private static sPlusOneCountTextWidth:I

.field private static sProcessingMediaBackgroundPaint:Landroid/graphics/Paint;

.field private static sProcessingMediaSubTitle:Ljava/lang/String;

.field private static sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

.field private static sProcessingMediaSubTitleVerticalPosition:I

.field private static sProcessingMediaTitle:Ljava/lang/String;

.field private static sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

.field private static sProcessingMediaTitleVerticalPosition:I

.field private static sTagPaint:Landroid/graphics/Paint;

.field private static sTagTextBackgroundPaint:Landroid/graphics/Paint;

.field private static sTagTextPadding:I

.field private static sTagTextPaint:Landroid/text/TextPaint;

.field private static sVideoImage:Landroid/graphics/Bitmap;

.field private static sVideoNotReadyImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAllowCrop:Z

.field private mBackgroundColor:I

.field private mCommentText:Ljava/lang/String;

.field private mCoverPhotoOffset:Ljava/lang/Integer;

.field private mCropMode:I

.field private mCropRect:Landroid/graphics/Rect;

.field private mCropSizeHeight:I

.field private mCropSizeWidth:I

.field private mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

.field private mDoubleTapDebounce:Z

.field private mDoubleTapToZoomEnabled:Z

.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mExternalClickListener:Landroid/view/View$OnClickListener;

.field private mFixedHeight:I

.field private mFlingDebounce:Z

.field private mFullScreen:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHaveLayout:Z

.field private mImageIsInvalid:Z

.field private mImageListener:Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

.field mInitialTranslationY:F

.field private mIsDoubleTouch:Z

.field private mIsPlaceHolder:Z

.field private mLastTwoFingerUp:J

.field private mLoadAnimatedImage:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMaxScale:F

.field mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mMinScale:F

.field mOriginalAspectRatio:F

.field private mOriginalMatrix:Landroid/graphics/Matrix;

.field private mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

.field private mPlusOneText:Ljava/lang/String;

.field private mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;

.field private mRotation:F

.field private mScaleFactor:F

.field private mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

.field private mShouldTriggerViewLoaded:Z

.field private mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

.field private mTagNameBackground:Landroid/graphics/RectF;

.field private mTempDst:Landroid/graphics/RectF;

.field private mTempSrc:Landroid/graphics/RectF;

.field private mTransformNoScaling:Z

.field private mTransformVerticalOnly:Z

.field private mTransformsEnabled:Z

.field private mTranslateRect:Landroid/graphics/RectF;

.field private mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

.field private mValues:[F

.field private mVideoBlob:[B

.field private mVideoReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mShouldTriggerViewLoaded:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTagNameBackground:Landroid/graphics/RectF;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mShouldTriggerViewLoaded:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTagNameBackground:Landroid/graphics/RectF;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mShouldTriggerViewLoaded:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTagNameBackground:Landroid/graphics/RectF;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->initialize()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/PhotoView;FFF)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/PhotoView;
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PhotoView;->scale(FFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/PhotoView;FF)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/PhotoView;
    .param p1    # F
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/PhotoView;->translate(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/PhotoView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->snap()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/PhotoView;FZ)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/views/PhotoView;
    .param p1    # F
    .param p2    # Z

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotation:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotation:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    return-void
.end method

.method private clearDrawable()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private configureBounds(Z)V
    .locals 13
    .param p1    # Z

    const/4 v3, 0x0

    const/high16 v12, 0x446b0000

    const/high16 v11, 0x41000000

    const/high16 v10, 0x40000000

    const/4 v9, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mHaveLayout:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v3, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    if-nez p1, :cond_2

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    cmpl-float v2, v2, v9

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mHaveLayout:Z

    if-eqz v2, :cond_4

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    int-to-float v7, v2

    int-to-float v8, v3

    invoke-virtual {v6, v9, v9, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v6, :cond_6

    int-to-float v3, v3

    int-to-float v2, v2

    div-float v2, v3, v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalAspectRatio:F

    int-to-float v2, v5

    int-to-float v3, v4

    div-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalAspectRatio:F

    cmpl-float v2, v3, v2

    if-lez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v3, v4

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalAspectRatio:F

    mul-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    sub-int v7, v2, v3

    int-to-float v7, v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    int-to-float v8, v8

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v5, v6, v7, v8, v2}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v3, v5, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropMode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mInitialTranslationY:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCoverPhotoOffset:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCoverPhotoOffset:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropMode:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_7

    const/high16 v2, -0x40800000

    :goto_2
    sub-float v2, v3, v2

    int-to-float v3, v4

    div-float/2addr v3, v12

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v9, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getInternalScale()F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    mul-float/2addr v2, v10

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    mul-float/2addr v3, v11

    invoke-static {v3, v11}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMaxScale:F

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v3, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalAspectRatio:F

    div-float/2addr v3, v5

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    sub-int v6, v2, v3

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-virtual {v5, v6, v7, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempDst:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v2, v9, v9, v3, v5}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    invoke-virtual {v2, v5}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    const/4 v5, 0x5

    aget v2, v2, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mInitialTranslationY:F

    sub-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    const/4 v6, 0x4

    aget v6, v2, v6

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v8

    int-to-float v2, v2

    div-float/2addr v2, v10

    int-to-float v7, v7

    div-float/2addr v7, v10

    sub-float/2addr v2, v7

    int-to-float v5, v5

    sub-float/2addr v2, v5

    div-float v5, v2, v6

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v12, v2

    mul-float/2addr v2, v5

    neg-float v2, v2

    goto/16 :goto_2
.end method

.method private getInternalScale()F
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private initialize()V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-boolean v3, Lcom/google/android/apps/plus/views/PhotoView;->sInitialized:Z

    if-nez v3, :cond_0

    sput-boolean v2, Lcom/google/android/apps/plus/views/PhotoView;->sInitialized:Z

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_photodetail_comment:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_photodetail_plus:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->video_overlay:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sVideoImage:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_loading_video:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->overlay_lightcycle:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sPanoramaImage:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$color;->photo_background_color:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sBackgroundColor:I

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_info_plusone_count_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_text_size:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_info_comment_count_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_text_size:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_tag_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_stroke_width:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_shadow_radius:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->photo_tag_shadow_color:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v6, v6, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_crop_profile_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropSizeProfile:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_crop_cover_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropSizeCoverWidth:I

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_crop_dim_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_crop_highlight_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_crop_stroke_width:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_tag_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPaint:Landroid/text/TextPaint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v6, v6, v6, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_text_size:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_title_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_subtitle_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaBackgroundPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_background_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget v3, Lcom/google/android/apps/plus/R$string;->media_processing_message:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitle:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->media_processing_message_subtitle:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitle:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_title_vertical_position:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleVerticalPosition:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_subtitle_vertical_position:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleVerticalPosition:I

    sget v3, Lcom/google/android/apps/plus/R$string;->media_not_found_message:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTitle:Ljava/lang/String;

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->media_not_found_message_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_tag_text_background_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_overlay_right_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sPhotoOverlayRightPadding:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_overlay_bottom_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sPhotoOverlayBottomPadding:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_count_left_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountLeftMargin:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_count_text_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountTextWidth:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_count_left_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountLeftMargin:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_count_text_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountTextWidth:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_bottom_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneBottomMargin:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_tag_text_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoView;->sTagTextPadding:I

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    sput-boolean v3, Lcom/google/android/apps/plus/views/PhotoView;->sHasMultitouchDistinct:Z

    :cond_0
    new-instance v3, Landroid/view/GestureDetector;

    const/4 v4, 0x0

    sget-boolean v5, Lcom/google/android/apps/plus/views/PhotoView;->sHasMultitouchDistinct:Z

    if-nez v5, :cond_1

    :goto_0
    invoke-direct {v3, v0, p0, v4, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Landroid/view/ScaleGestureDetector;

    invoke-direct {v2, v0, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static onStart()V
    .locals 0

    return-void
.end method

.method public static onStop()V
    .locals 0

    return-void
.end method

.method private resetTransformations()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mOriginalMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    return-void
.end method

.method private scale(FFF)V
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotation:F

    neg-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    invoke-static {p1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMaxScale:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getInternalScale()F

    move-result v0

    div-float v1, p1, v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->snap()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotation:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getInternalScale()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    div-float/2addr v2, v3

    :cond_0
    return-void
.end method

.method private snap()V
    .locals 15

    const/high16 v14, 0x41a00000

    const/high16 v13, 0x40000000

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v3, v11

    :goto_0
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v4, v11

    :goto_1
    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v11, Landroid/graphics/RectF;->right:F

    sub-float v11, v6, v1

    sub-float v12, v4, v3

    cmpg-float v11, v11, v12

    if-gez v11, :cond_3

    sub-float v11, v4, v3

    add-float v12, v6, v1

    sub-float/2addr v11, v12

    div-float/2addr v11, v13

    add-float v8, v3, v11

    :goto_2
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v11, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    :goto_3
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    :goto_4
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_8

    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    div-float/2addr v10, v13

    add-float v9, v5, v10

    :goto_5
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-gtz v10, :cond_0

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-lez v10, :cond_b

    :cond_0
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

    invoke-virtual {v10, v8, v9}, Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;->start(FF)Z

    :goto_6
    return-void

    :cond_1
    move v3, v10

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v11

    int-to-float v4, v11

    goto :goto_1

    :cond_3
    cmpl-float v11, v1, v3

    if-lez v11, :cond_4

    sub-float v8, v3, v1

    goto :goto_2

    :cond_4
    cmpg-float v11, v6, v4

    if-gez v11, :cond_5

    sub-float v8, v4, v6

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    :cond_6
    move v5, v10

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4

    :cond_8
    cmpl-float v10, v7, v5

    if-lez v10, :cond_9

    sub-float v9, v5, v7

    goto :goto_5

    :cond_9
    cmpg-float v10, v0, v2

    if-gez v10, :cond_a

    sub-float v9, v2, v0

    goto :goto_5

    :cond_a
    const/4 v9, 0x0

    goto :goto_5

    :cond_b
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto :goto_6
.end method

.method private translate(FF)Z
    .locals 12
    .param p1    # F
    .param p2    # F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v3, v10

    :goto_0
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v10

    :goto_1
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v10, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v10, Landroid/graphics/RectF;->right:F

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float v10, v3, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    sub-float v11, v4, v11

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    :goto_2
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    :goto_3
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    :goto_4
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v5, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    sub-float v11, v2, v11

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    :goto_5
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    cmpl-float v10, v8, p1

    if-nez v10, :cond_8

    cmpl-float v10, v9, p2

    if-nez v10, :cond_8

    const/4 v10, 0x1

    :goto_6
    return v10

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v10

    int-to-float v4, v10

    goto :goto_1

    :cond_2
    sub-float v10, v6, v1

    sub-float v11, v4, v3

    cmpg-float v10, v10, v11

    if-gez v10, :cond_3

    sub-float v10, v4, v3

    add-float v11, v6, v1

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    add-float v8, v3, v10

    goto :goto_2

    :cond_3
    sub-float v10, v4, v6

    sub-float v11, v3, v1

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4

    :cond_6
    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_7

    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    add-float v9, v5, v10

    goto :goto_5

    :cond_7
    sub-float v10, v2, v0

    sub-float v11, v5, v7

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    goto :goto_6
.end method

.method private unbindCurrentResource()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/MediaResource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_0
    return-void
.end method

.method private unbindPendingResource()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/MediaResource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_0
    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 6

    const/16 v4, 0x10

    const/4 v2, 0x3

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLoadAnimatedImage:Z

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropMode:I

    if-ne v0, v2, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/16 v2, 0x3ac

    const/4 v3, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v1, v2, v4, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    goto :goto_0
.end method

.method public final destroy()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->clearDrawable()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindPendingResource()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindCurrentResource()V

    return-void
.end method

.method public final enableImageTransforms(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->resetTransformations()V

    :cond_0
    return-void
.end method

.method public final init(Lcom/google/android/apps/plus/api/MediaRef;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mIsPlaceHolder:Z

    sget v0, Lcom/google/android/apps/plus/views/PhotoView;->sBackgroundColor:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-ne v1, p1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindPendingResource()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mBackgroundColor:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->bindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto :goto_0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->bindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindPendingResource()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindCurrentResource()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapDebounce:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getInternalScale()F

    move-result v0

    const/high16 v2, 0x3fc00000

    mul-float v1, v0, v2

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMaxScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->start(FFFF)Z

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapDebounce:Z

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$SnapRunnable;->stop()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 18
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mBackgroundColor:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mIsPlaceHolder:Z

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v1

    int-to-float v5, v1

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitle:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleVerticalPosition:I

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitle:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget v3, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleVerticalPosition:I

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->sPhotoOverlayBottomPadding:I

    sub-int v17, v1, v2

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mFullScreen:Z

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v8, v1

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->sPhotoOverlayRightPadding:I

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountTextWidth:I

    sub-int v16, v1, v2

    sub-int v17, v17, v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    move/from16 v0, v16

    int-to-float v2, v0

    move/from16 v0, v17

    int-to-float v3, v0

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget v1, Lcom/google/android/apps/plus/views/PhotoView;->sCommentCountLeftMargin:I

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v16, v16, v1

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v16

    int-to-float v2, v0

    move/from16 v0, v17

    int-to-float v3, v0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget v1, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneBottomMargin:I

    sub-int v17, v17, v1

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mFullScreen:Z

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v12, v1

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v1, v12}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->sPhotoOverlayRightPadding:I

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountTextWidth:I

    sub-int v16, v1, v2

    sub-int v17, v17, v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    move/from16 v0, v16

    int-to-float v2, v0

    move/from16 v0, v17

    int-to-float v3, v0

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget v1, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneCountLeftMargin:I

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v16, v16, v1

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v16

    int-to-float v2, v0

    move/from16 v0, v17

    int-to-float v3, v0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_c

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v14

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/graphics/Canvas;->restoreToCount(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mVideoBlob:[B

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mVideoReady:Z

    if-eqz v1, :cond_8

    sget-object v15, Lcom/google/android/apps/plus/views/PhotoView;->sVideoImage:Landroid/graphics/Bitmap;

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v9, v1, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v1

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v10, v1, 0x2

    int-to-float v1, v9

    int-to-float v2, v10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v1, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v13

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v1

    int-to-float v5, v1

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoView;->sCropDimPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/graphics/Canvas;->restoreToCount(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sCropPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_8
    sget-object v15, Lcom/google/android/apps/plus/views/PhotoView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_a

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v2

    if-ne v1, v2, :cond_a

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sPanoramaImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v9, v1, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoView;->sPanoramaImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v10, v1, 0x2

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sPanoramaImage:Landroid/graphics/Bitmap;

    int-to-float v2, v9

    int-to-float v3, v10

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/MediaResource;->getResourceType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    const/4 v1, 0x1

    goto :goto_3

    :cond_b
    const/4 v1, 0x0

    goto :goto_3

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/PhotoView;->mImageIsInvalid:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTitle:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformVerticalOnly:Z

    if-eqz v0, :cond_0

    const/4 p3, 0x0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFlingDebounce:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;->start(FF)Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFlingDebounce:Z

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mHaveLayout:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getHeight()I

    move-result v6

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v10, :cond_0

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropMode:I

    packed-switch v10, :pswitch_data_0

    invoke-static {v8, v6}, Ljava/lang/Math;->min(II)I

    move-result v7

    sget v5, Lcom/google/android/apps/plus/views/PhotoView;->sCropSizeProfile:I

    const/high16 v0, 0x3f800000

    :goto_0
    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeWidth:I

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeWidth:I

    int-to-float v10, v10

    div-float/2addr v10, v0

    float-to-int v10, v10

    iput v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeHeight:I

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeWidth:I

    sub-int v10, v8, v10

    div-int/lit8 v2, v10, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeHeight:I

    sub-int v10, v6, v10

    div-int/lit8 v4, v10, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeWidth:I

    add-int v3, v2, v10

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropSizeHeight:I

    add-int v1, v4, v10

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v2, v4, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PhotoView;->configureBounds(Z)V

    return-void

    :pswitch_0
    int-to-float v10, v8

    const v11, 0x3dcccccd

    mul-float/2addr v10, v11

    float-to-int v9, v10

    sub-int v7, v8, v9

    sget v5, Lcom/google/android/apps/plus/views/PhotoView;->sCropSizeCoverWidth:I

    const v0, 0x40a71c72

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-eqz v1, :cond_0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mShouldTriggerViewLoaded:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;->onImageLoadFinished$4344180e()V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mShouldTriggerViewLoaded:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->clearDrawable()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_8

    check-cast v2, Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v3, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eq v2, v3, :cond_4

    :cond_1
    if-eqz v2, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-ne v3, v6, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-eq v3, v6, :cond_7

    :cond_2
    move v3, v4

    :goto_1
    const/4 v4, 0x0

    iput v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMinScale:F

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->clearDrawable()V

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_3

    if-eqz v2, :cond_3

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v4, v6, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_3
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/PhotoView;->configureBounds(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    :cond_4
    :goto_3
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLoadAnimatedImage:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLoadAnimatedImage:Z

    if-eqz v3, :cond_5

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLoadAnimatedImage:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindPendingResource()V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v6, 0x3

    const/16 v7, 0x14

    invoke-virtual {v3, v4, v6, v7, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_5
    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLoadAnimatedImage:Z

    :cond_6
    const/4 v1, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageIsInvalid:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindCurrentResource()V

    check-cast p1, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    goto/16 :goto_0

    :cond_7
    move v3, v5

    goto :goto_1

    :cond_8
    instance-of v3, v2, Lcom/google/android/apps/plus/util/GifImage;

    if-eqz v3, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/util/GifDrawable;

    check-cast v2, Lcom/google/android/apps/plus/util/GifImage;

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/util/GifDrawable;-><init>(Lcom/google/android/apps/plus/util/GifImage;)V

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/util/GifDrawable;->setAnimationEnabled(Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/views/PhotoView;->configureBounds(Z)V

    goto :goto_3

    :pswitch_2
    const/4 v1, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageIsInvalid:Z

    goto/16 :goto_0

    :cond_9
    move v3, v5

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformNoScaling:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    const/high16 v4, 0x3f800000

    sub-float v2, v3, v4

    cmpg-float v3, v2, v5

    if-gez v3, :cond_2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_3

    :cond_2
    cmpl-float v3, v2, v5

    if-lez v3, :cond_4

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    cmpg-float v3, v3, v5

    if-gez v3, :cond_4

    :cond_3
    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    :cond_4
    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    add-float/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3d23d70a

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mIsDoubleTouch:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getInternalScale()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    mul-float v1, v0, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    invoke-direct {p0, v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoView;->scale(FFF)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->stop()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mIsDoubleTouch:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleFactor:F

    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mIsDoubleTouch:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapDebounce:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->resetTransformations()V

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFlingDebounce:Z

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLastTwoFingerUp:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformVerticalOnly:Z

    if-eqz v2, :cond_0

    const/4 p3, 0x0

    :cond_0
    const-wide/16 v2, 0x190

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformsEnabled:Z

    if-eqz v2, :cond_1

    neg-float v2, p3

    neg-float v3, p4

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->translate(FF)Z

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mIsDoubleTouch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mIsDoubleTouch:Z

    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    # getter for: Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;->mRunning:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;->access$000(Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->snap()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLastTwoFingerUp:J

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v2, v4, :cond_2

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mLastTwoFingerUp:J

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setCommentCount(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto :goto_0

    :cond_2
    const/16 v1, 0x63

    if-le p1, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCommentText:Ljava/lang/String;

    goto :goto_1
.end method

.method public setCropMode(I)V
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mHaveLayout:Z

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot set crop after view has been laid out"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    if-eqz v3, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot unset crop mode"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mAllowCrop:Z

    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropMode:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCropMode:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformVerticalOnly:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTransformNoScaling:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDoubleTapToZoomEnabled:Z

    :cond_3
    return-void
.end method

.method public setCropModeCoverPhotoOffset(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mCoverPhotoOffset:Ljava/lang/Integer;

    return-void
.end method

.method public setFixedHeight(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    if-eq p1, v2, :cond_1

    move v0, v1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFixedHeight:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoView;->setMeasuredDimension(II)V

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoView;->configureBounds(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFullScreen(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFullScreen:Z

    if-eq p1, v0, :cond_1

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFullScreen:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mFullScreen:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$TranslateRunnable;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView$RotateRunnable;->stop()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    :cond_1
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnImageListener(Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;

    return-void
.end method

.method public setPlusOneCount(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->invalidate()V

    goto :goto_0

    :cond_2
    const/16 v1, 0x63

    if-le p1, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mPlusOneText:Ljava/lang/String;

    goto :goto_1
.end method

.method public setVideoBlob([B)V
    .locals 3
    .param p1    # [B

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mVideoBlob:[B

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    const-string v1, "FINAL"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "READY"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView;->mVideoReady:Z

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public unbindResources()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindPendingResource()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoView;->unbindCurrentResource()V

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
