.class public Lcom/google/android/apps/plus/views/NotificationBarLayout;
.super Lcom/google/android/apps/plus/views/SlidingPanelLayout;
.source "NotificationBarLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;
    }
.end annotation


# instance fields
.field private mDimBackground:Landroid/graphics/drawable/ColorDrawable;

.field private mDimColor:I

.field private mListener:Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

.field private mMinNotificationBarMarginLeft:I

.field private mMinNotificationBarWidth:I

.field private mNotificationBarWidth:I

.field private mOffsetClosed:I

.field private mOffsetOpen:I

.field private mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method private brighten(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mDimBackground:Landroid/graphics/drawable/ColorDrawable;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    return-void
.end method

.method private isScrolling()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->getScroll()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    if-eq v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0xc8

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->brighten(I)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setScrollEnabled(Z)V

    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->smoothScrollTo(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_min_notification_bar_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mMinNotificationBarWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_min_notification_bar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mMinNotificationBarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$color;->notification_dim_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mDimColor:I

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mDimColor:I

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mDimBackground:Landroid/graphics/drawable/ColorDrawable;

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mPanel:Landroid/view/View;

    iget v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mNotificationBarWidth:I

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->getScroll()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isScrolling()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    :goto_0
    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->scrollTo(II)V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onMeasure(II)V

    iget v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mMinNotificationBarWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mMinNotificationBarMarginLeft:I

    sub-int v3, v1, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mNotificationBarWidth:I

    neg-int v2, v1

    iget v3, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mNotificationBarWidth:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    neg-int v2, v1

    iput v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mPanel:Landroid/view/View;

    iget v3, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mNotificationBarWidth:I

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    iget v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    iget v3, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setScrollLimits(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mPanel:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onScrollChanged(IIII)V

    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    if-ne p1, v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->brighten(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;->onNotificationBarClosed()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setOpen(Z)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

    goto :goto_1
.end method

.method protected final onScrollFinished(I)V
    .locals 1
    .param p1    # I

    if-lez p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->smoothScrollTo(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetClosed:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->smoothScrollTo(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    const/16 v0, 0xc8

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->brighten(I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isScrolling()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->close()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final open()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mDimBackground:Landroid/graphics/drawable/ColorDrawable;

    aput-object v2, v1, v3

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOpen:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mPanel:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setScrollEnabled(Z)V

    iget v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mOffsetOpen:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->smoothScrollTo(I)V

    goto :goto_0
.end method

.method public performClick()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->performClick()Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->close()V

    const/4 v0, 0x1

    return v0
.end method

.method public setOnNotificationBarStateChange(Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;

    return-void
.end method

.method public setOpen(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setOpen(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mDimBackground:Landroid/graphics/drawable/ColorDrawable;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/NotificationBarLayout;->mTransparentBackground:Landroid/graphics/drawable/ColorDrawable;

    goto :goto_0
.end method
