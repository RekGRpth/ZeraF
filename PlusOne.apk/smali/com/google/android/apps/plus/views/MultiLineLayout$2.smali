.class final Lcom/google/android/apps/plus/views/MultiLineLayout$2;
.super Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;
.source "MultiLineLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/MultiLineLayout;->onMeasure(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mRequestedHeight:I

.field private mRequestedWidth:I

.field final synthetic this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

.field final synthetic val$heightConstraint:I

.field final synthetic val$widthConstraint:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/MultiLineLayout;II)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    iput p2, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->val$widthConstraint:I

    iput p3, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->val$heightConstraint:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;-><init>(Lcom/google/android/apps/plus/views/MultiLineLayout;B)V

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedWidth:I

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedHeight:I

    return-void
.end method


# virtual methods
.method public final apply(I)V
    .locals 4
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->apply(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedWidth:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedWidth:I

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedHeight:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedHeight:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    iget v1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedWidth:I

    iget v2, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->val$widthConstraint:I

    invoke-static {v1, v2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedHeight:I

    iget v3, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->val$heightConstraint:I

    invoke-static {v2, v3}, Landroid/view/View;->resolveSize(II)I

    move-result v2

    # invokes: Lcom/google/android/apps/plus/views/MultiLineLayout;->setMeasuredDimension(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$200(Lcom/google/android/apps/plus/views/MultiLineLayout;II)V

    return-void
.end method

.method protected final layout(Landroid/view/View;IIII)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedWidth:I

    add-int v1, p2, p4

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedWidth:I

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedHeight:I

    add-int v1, p3, p5

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->mRequestedHeight:I

    return-void
.end method

.method protected final measure(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    iget v1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->val$widthConstraint:I

    iget v2, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->val$heightConstraint:I

    # invokes: Lcom/google/android/apps/plus/views/MultiLineLayout;->measureChild(Landroid/view/View;II)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$100(Lcom/google/android/apps/plus/views/MultiLineLayout;Landroid/view/View;II)V

    return-void
.end method
