.class public Lcom/google/android/apps/plus/views/EsListView;
.super Landroid/widget/ListView;
.source "EsListView.java"


# instance fields
.field private final mObserver:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/plus/views/EsListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/EsListView$1;-><init>(Lcom/google/android/apps/plus/views/EsListView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EsListView;->mObserver:Landroid/database/DataSetObserver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/EsListView;->wrapContextIfNeeded(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/plus/views/EsListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/EsListView$1;-><init>(Lcom/google/android/apps/plus/views/EsListView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EsListView;->mObserver:Landroid/database/DataSetObserver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/EsListView;->wrapContextIfNeeded(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/views/EsListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/EsListView$1;-><init>(Lcom/google/android/apps/plus/views/EsListView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EsListView;->mObserver:Landroid/database/DataSetObserver;

    return-void
.end method

.method private static wrapContextIfNeeded(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/util/AttributeSet;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const/4 v1, 0x0

    const-string v2, "theme"

    const/4 v3, 0x0

    invoke-interface {p1, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-direct {v1, p0, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object p0, v1

    goto :goto_0
.end method


# virtual methods
.method protected final adjustFastScroll()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsListView;->isFastScrollEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EsListView;->setFastScrollEnabled(Z)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EsListView;->setFastScrollEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsListView;->getHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0, v1, v0}, Lcom/google/android/apps/plus/views/EsListView;->onSizeChanged(IIII)V

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1    # Landroid/widget/Adapter;

    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/EsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 3
    .param p1    # Landroid/widget/ListAdapter;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EsListView;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EsListView;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method
