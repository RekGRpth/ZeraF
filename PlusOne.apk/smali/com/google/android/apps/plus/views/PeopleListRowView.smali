.class public Lcom/google/android/apps/plus/views/PeopleListRowView;
.super Landroid/widget/RelativeLayout;
.source "PeopleListRowView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;
    }
.end annotation


# instance fields
.field private mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

.field private mAllowAvatarClickWithoutGaiaId:Z

.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mContext:Landroid/content/Context;

.field private mForSharing:Z

.field private mHasProfile:Z

.field private mHideCirclesButtons:Z

.field private mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

.field private mNameView:Lcom/google/android/apps/plus/views/PeopleListRowNameView;

.field private mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

.field private mPersonId:Ljava/lang/String;

.field private mPersonName:Ljava/lang/String;

.field private mSuggestionId:Ljava/lang/String;

.field private mUserIsBlocked:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAllowAvatarClickWithoutGaiaId:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mContext:Landroid/content/Context;

    return-void
.end method

.method private bind(Lcom/google/api/services/plusi/model/DataCirclePerson;Ljava/lang/String;Ljava/lang/String;)V
    .locals 14
    .param p1    # Lcom/google/api/services/plusi/model/DataCirclePerson;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->getGaiaId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    if-nez v0, :cond_1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    iget-object v13, p1, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    const/4 v7, 0x0

    if-eqz v13, :cond_3

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v10, :cond_2

    invoke-interface {v13, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/DataMembership;

    iget-object v0, v12, Lcom/google/api/services/plusi/model/DataMembership;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    if-eqz v0, :cond_0

    iget-object v0, v12, Lcom/google/api/services/plusi/model/DataMembership;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCircleId;->obfuscatedGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, v12, Lcom/google/api/services/plusi/model/DataMembership;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCircleId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x7c

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->photoUrl:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_3
    const/4 v6, 0x0

    move-object v0, p0

    move-object/from16 v5, p2

    move-object/from16 v8, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static generateDescription(Landroid/content/Context;Ljava/lang/Integer;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/Integer;
    .param p2    # Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$plurals;->common_friend_count:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v0, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    sget v2, Lcom/google/android/apps/plus/R$string;->people_search_job:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    aput-object v4, v3, v5

    iget-object v4, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v0, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v0, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    goto :goto_0

    :cond_6
    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/DataSuggestedPerson;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->explanation:Lcom/google/api/services/plusi/model/DataSugggestionExplanation;

    iget-object v3, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_2

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataSugggestionExplanation;->numberOfCommonFriends:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v5, v4, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->generateDescription(Landroid/content/Context;Ljava/lang/Integer;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->suggestionId:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Lcom/google/api/services/plusi/model/DataCirclePerson;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final bind(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mContext:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->numberOfCommonFriends:Ljava/lang/Integer;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->generateDescription(Landroid/content/Context;Ljava/lang/Integer;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->suggestionId:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Lcom/google/api/services/plusi/model/DataCirclePerson;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Lcom/google/api/services/plusi/model/DataSuggestedPerson;)V

    goto :goto_0
.end method

.method public final bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonName:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mSuggestionId:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->ensureSchemeIsPresent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v4, p2, p3}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p2, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAllowAvatarClickWithoutGaiaId:Z

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonName:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonName:Ljava/lang/String;

    const/4 v4, 0x1

    move-object v3, p0

    :goto_1
    iput-boolean v4, v3, Lcom/google/android/apps/plus/views/PeopleListRowView;->mHasProfile:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mNameView:Lcom/google/android/apps/plus/views/PeopleListRowNameView;

    invoke-virtual {v4, v1, p5, p6}, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->bind(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mHideCirclesButtons:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    :goto_2
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setClickable(Z)V

    goto :goto_0

    :cond_2
    const-string v4, "e:"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "p:"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v3, p0

    :goto_3
    const/4 v4, 0x0

    goto :goto_1

    :cond_4
    move-object v1, p1

    if-eqz p2, :cond_a

    const/4 v4, 0x1

    move-object v3, p0

    goto :goto_1

    :cond_5
    invoke-static {p7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mForSharing:Z

    if-eqz v4, :cond_6

    sget v4, Lcom/google/android/apps/plus/R$string;->add_to_circles:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_4
    invoke-virtual {v5, v4}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    if-eqz p1, :cond_7

    invoke-static {p1}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->isCircleMembershipRequestPending(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowProgressIndicator(Z)V

    goto :goto_2

    :cond_6
    sget v4, Lcom/google/android/apps/plus/R$string;->follow:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowProgressIndicator(Z)V

    goto :goto_2

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string v4, "15"

    invoke-static {p7, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setBlockedMode(Z)V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mUserIsBlocked:Z

    goto/16 :goto_2

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setBlockedMode(Z)V

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mUserIsBlocked:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v4, p7}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNameListForPackedIds(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setCircles(Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_a
    move-object v3, p0

    goto/16 :goto_3
.end method

.method public final getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPersonName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuggestionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mSuggestionId:Ljava/lang/String;

    return-object v0
.end method

.method public final init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;
    .param p2    # Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .param p3    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mForSharing:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->avatar:I

    if-eq v0, v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->name_view:I

    if-ne v0, v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mHasProfile:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mSuggestionId:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;->onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$id;->in_circles_button:I

    if-ne v0, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mUserIsBlocked:Z

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonId:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mForSharing:Z

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v2, v3, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;->onUnblockButtonClicked(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mSuggestionId:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;->onInACircleButtonClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$id;->add_to_circles_button:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mPersonName:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mForSharing:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mSuggestionId:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;->onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->name_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mNameView:Lcom/google/android/apps/plus/views/PeopleListRowNameView;

    sget v0, Lcom/google/android/apps/plus/R$id;->in_circles_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/CirclesButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mInCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    sget v0, Lcom/google/android/apps/plus/R$id;->add_to_circles_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/CirclesButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAddToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowIcon(Z)V

    return-void
.end method

.method public setAllowAvatarClickWithoutGaiaId(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mAllowAvatarClickWithoutGaiaId:Z

    return-void
.end method

.method public setHideCirclesButtons(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListRowView;->mHideCirclesButtons:Z

    return-void
.end method
