.class final Lcom/google/android/apps/plus/views/SquareLandingView$1;
.super Landroid/text/style/ClickableSpan;
.source "SquareLandingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/SquareLandingView;->showBlockingExplanation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/SquareLandingView;

.field final synthetic val$urlSpan:Landroid/text/style/URLSpan;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/SquareLandingView;Landroid/text/style/URLSpan;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SquareLandingView$1;->this$0:Lcom/google/android/apps/plus/views/SquareLandingView;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/SquareLandingView$1;->val$urlSpan:Landroid/text/style/URLSpan;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$1;->this$0:Lcom/google/android/apps/plus/views/SquareLandingView;

    # getter for: Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->access$000(Lcom/google/android/apps/plus/views/SquareLandingView;)Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$1;->this$0:Lcom/google/android/apps/plus/views/SquareLandingView;

    # getter for: Lcom/google/android/apps/plus/views/SquareLandingView;->mOnClickListener:Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/SquareLandingView;->access$000(Lcom/google/android/apps/plus/views/SquareLandingView;)Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareLandingView$1;->val$urlSpan:Landroid/text/style/URLSpan;

    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;->onBlockingHelpLinkClicked(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1
    .param p1    # Landroid/text/TextPaint;

    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    return-void
.end method
