.class public Lcom/google/android/apps/plus/views/OneUpSquareView;
.super Landroid/view/View;
.source "OneUpSquareView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# static fields
.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private static sMaxWidth:I

.field private static sMinExposureLand:I

.field private static sMinExposurePort:I

.field private static sOneUpSquareViewInitialized:Z

.field protected static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field protected mAvailableContentHeight:I

.field protected mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

.field protected mDestRect:Landroid/graphics/Rect;

.field protected mHasSeenImage:Z

.field protected mImageResource:Lcom/google/android/apps/plus/service/Resource;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field protected mSquareInvitationLayout:Landroid/text/StaticLayout;

.field protected mSquareNameLayout:Landroid/text/StaticLayout;

.field protected mSrcRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/OneUpSquareView;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OneUpSquareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OneUpSquareView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/OneUpSquareView;->sOneUpSquareViewInitialized:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/OneUpSquareView;->sOneUpSquareViewInitialized:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpSquareView;->sMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_land:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpSquareView;->sMinExposureLand:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_port:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpSquareView;->sMinExposurePort:I

    invoke-static {p1}, Lcom/google/android/apps/plus/util/SquareRenderUtils;->init(Landroid/content/Context;)V

    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSrcRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDestRect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method protected getMinExposureLand()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/OneUpSquareView;->sMinExposureLand:I

    return v0
.end method

.method protected getMinExposurePort()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/OneUpSquareView;->sMinExposurePort:I

    return v0
.end method

.method public final init(Lcom/google/android/apps/plus/content/DbEmbedSquare;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getImageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x3a83126f

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/OneUpSquareView;->setAlpha(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->bindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->invalidate()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->unbindResources()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getWidth()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-nez v0, :cond_0

    const/4 v6, 0x0

    :goto_0
    if-nez v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getBackgroundPattern(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v7, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDestRect:Landroid/graphics/Rect;

    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getTransparentOverlayPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const/4 v1, 0x0

    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mAvailableContentHeight:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareInvitationLayout:Landroid/text/StaticLayout;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareNameLayout:Landroid/text/StaticLayout;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/SquareRenderUtils;->drawIconAndText$65511810(Landroid/graphics/Canvas;IIILandroid/text/StaticLayout;Landroid/text/StaticLayout;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v6, v0

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mHasSeenImage:Z

    if-nez v0, :cond_4

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/views/OneUpSquareView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    if-nez v0, :cond_2

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/OneUpSquareView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v3, 0x1f4

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpSquareView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mHasSeenImage:Z

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDestRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSrcRect:Landroid/graphics/Rect;

    invoke-static {v6, v0, v1}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createBackgroundSourceRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSrcRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDestRect:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/views/OneUpSquareView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v8, 0x0

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    invoke-static {}, Lcom/google/android/apps/plus/util/SquareRenderUtils;->getSquareIconBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/plus/util/SquareRenderUtils;->getContentYPadding()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getMeasuredHeight()I

    move-result v2

    sget v6, Lcom/google/android/apps/plus/views/OneUpSquareView;->sMaxWidth:I

    if-gt v4, v6, :cond_2

    iput v2, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mAvailableContentHeight:I

    :goto_0
    iget v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mAvailableContentHeight:I

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v6, v7

    mul-int/lit8 v7, v1, 0x3

    sub-int v0, v6, v7

    move v5, p3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-static {v6, p3, v4, v0}, Lcom/google/android/apps/plus/util/SquareRenderUtils;->createSquareName$3db85e19(Lcom/google/android/apps/plus/content/DbEmbedSquare;III)Landroid/text/StaticLayout;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareNameLayout:Landroid/text/StaticLayout;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareNameLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v6

    add-int/2addr v6, v1

    add-int v5, p3, v6

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->isInvitation()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v5, v4, v0}, Lcom/google/android/apps/plus/util/SquareRenderUtils;->createInvitation$5f0039f6(Landroid/content/Context;III)Landroid/text/StaticLayout;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareInvitationLayout:Landroid/text/StaticLayout;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareInvitationLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSquareInvitationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getHeight()I

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v6, v8, v8, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getMinExposureLand()I

    move-result v6

    sub-int v6, v2, v6

    iput v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mAvailableContentHeight:I

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->getMinExposurePort()I

    move-result v6

    sub-int v6, v2, v6

    iput v6, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mAvailableContentHeight:I

    goto :goto_0
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpSquareView;->invalidate()V

    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpSquareView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
