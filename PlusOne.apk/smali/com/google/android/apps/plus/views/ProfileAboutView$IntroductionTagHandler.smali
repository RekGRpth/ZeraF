.class public final Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;
.super Ljava/lang/Object;
.source "ProfileAboutView.java"

# interfaces
.implements Landroid/text/Html$TagHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "IntroductionTagHandler"
.end annotation


# instance fields
.field private mListStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/views/ProfileAboutView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/ProfileAboutView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->this$0:Lcom/google/android/apps/plus/views/ProfileAboutView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private handleListTag(ZLandroid/text/Editable;Z)V
    .locals 2
    .param p1    # Z
    .param p2    # Landroid/text/Editable;
    .param p3    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    :cond_0
    if-eqz p1, :cond_5

    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p2, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    :cond_1
    const-string v0, "\n"

    invoke-interface {p2, v0}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    if-eqz p3, :cond_4

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_1
    return-void

    :cond_4
    const/4 v0, -0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method public final handleTag(ZLjava/lang/String;Landroid/text/Editable;Lorg/xml/sax/XMLReader;)V
    .locals 6
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/text/Editable;
    .param p4    # Lorg/xml/sax/XMLReader;

    const/4 v3, 0x1

    const-string v4, "ul"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->handleListTag(ZLandroid/text/Editable;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "ol"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, p1, p3, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->handleListTag(ZLandroid/text/Editable;Z)V

    goto :goto_0

    :cond_2
    const-string v4, "li"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p1, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    if-nez v4, :cond_3

    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_4

    const-string v4, "  "

    invoke-interface {p3, v4}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->size()I

    move-result v3

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_6

    :cond_5
    const-string v1, "\u2022 "

    :goto_3
    invoke-interface {p3, v1}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;->mListStack:Ljava/util/Stack;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_7
    const-string v4, "\n"

    invoke-interface {p3, v4}, Landroid/text/Editable;->append(Ljava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0
.end method
