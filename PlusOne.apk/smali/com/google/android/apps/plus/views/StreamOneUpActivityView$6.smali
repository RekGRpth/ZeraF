.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;
.super Ljava/lang/Object;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Landroid/text/Spannable;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Landroid/text/Spannable;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mLinkedHeader:Landroid/text/Spannable;
    invoke-static {v3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Landroid/text/Spannable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Spannable;->length()I

    move-result v3

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v2, v5, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/style/URLSpan;

    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    aget-object v0, v1, v5

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$6;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/apps/plus/views/OneUpListener;->onSpanClick(Landroid/text/style/URLSpan;)V

    :cond_0
    return-void
.end method
