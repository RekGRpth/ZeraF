.class public Lcom/google/android/apps/plus/views/PositionedStaticLayout;
.super Landroid/text/StaticLayout;
.source "PositionedStaticLayout.java"


# instance fields
.field protected mContentArea:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .locals 8
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/text/TextPaint;
    .param p3    # I
    .param p4    # Landroid/text/Layout$Alignment;
    .param p5    # F
    .param p6    # F
    .param p7    # Z

    const/4 v0, 0x0

    invoke-static {v0, p3}, Ljava/lang/Math;->max(II)I

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->mContentArea:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final getBottom()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public final getLeft()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public final getRight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public final getTop()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public final setPosition(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getWidth()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method
