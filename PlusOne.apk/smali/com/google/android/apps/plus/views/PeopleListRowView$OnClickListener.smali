.class public interface abstract Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;
.super Ljava/lang/Object;
.source "PeopleListRowView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/PeopleListRowView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnClickListener"
.end annotation


# virtual methods
.method public abstract onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method public abstract onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onInACircleButtonClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onUnblockButtonClicked(Ljava/lang/String;Z)V
.end method
