.class public final Lcom/google/android/apps/plus/views/ClickableRect;
.super Ljava/lang/Object;
.source "ClickableRect.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;
    }
.end annotation


# instance fields
.field private mClicked:Z

.field private mContentDescription:Ljava/lang/CharSequence;

.field private mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;
    .param p6    # Ljava/lang/CharSequence;

    new-instance v0, Landroid/graphics/Rect;

    add-int v1, p1, p3

    add-int v2, p2, p4

    invoke-direct {v0, p1, p2, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-direct {p0, v0, p5, p6}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(Landroid/graphics/Rect;Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    return-void
.end method

.method private constructor <init>(Landroid/graphics/Rect;Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;
    .param p3    # Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mContentDescription:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    if-ne p3, v2, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_2

    if-ne p3, v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mListener:Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;->onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V

    :cond_3
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final isClicked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mClicked:Z

    return v0
.end method

.method public final setBounds(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableRect;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    return-void
.end method
