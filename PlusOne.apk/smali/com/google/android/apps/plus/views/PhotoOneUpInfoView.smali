.class public Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;
.super Lcom/google/android/apps/plus/views/OneUpBaseView;
.source "PhotoOneUpInfoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;
    }
.end annotation


# static fields
.field private static sActionBarBackgroundPaint:Landroid/graphics/Paint;

.field private static sAvatarMarginLeft:I

.field private static sAvatarMarginRight:I

.field private static sAvatarMarginTop:I

.field private static sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBackgroundPaint:Landroid/graphics/Paint;

.field private static sCaptionMarginTop:I

.field private static sContentPaint:Landroid/text/TextPaint;

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sFontSpacing:F

.field private static sMarginBottom:I

.field private static sMarginLeft:I

.field private static sMarginRight:I

.field private static sNameMarginTop:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlusOneButtonMarginLeft:I

.field private static sPlusOneButtonMarginRight:I

.field private static sResizePaint:Landroid/graphics/Paint;


# instance fields
.field private mAlbumId:Ljava/lang/String;

.field private mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mBackgroundOffset:I

.field private mCaption:Landroid/text/Spannable;

.field private mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContentDescriptionDirty:Z

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/String;

.field private mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mOwnerId:Ljava/lang/String;

.field private mOwnerName:Ljava/lang/String;

.field protected mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field private mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_one_up_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_one_up_caption_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_plus_one_button_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_content:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_action_bar_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setupAccessibility(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_one_up_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_one_up_caption_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_plus_one_button_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_content:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_action_bar_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setupAccessibility(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/OneUpBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_font_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_one_up_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_avatar_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_one_up_caption_margin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_plus_one_button_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sput v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_taco_avatar:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_name:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_date:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_date_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_date_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_content:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_content_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_content_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_action_bar_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sActionBarBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setupAccessibility(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    return-object v0
.end method

.method private setupAccessibility(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;-><init>(Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->install(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private updateAccessibility()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->invalidateParent()V

    :cond_0
    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v4, v5

    :cond_0
    :goto_0
    return v4

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    goto :goto_1

    :pswitch_2
    iput-object v7, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    move v4, v5

    goto :goto_0

    :pswitch_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    goto :goto_0

    :cond_3
    move v4, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public invalidate()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->invalidate()V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    if-eqz v4, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v2

    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setFocusable(Z)V

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    :cond_1
    return-void

    :cond_2
    move v0, v3

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->bindResources()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->updateAccessibility()V

    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAlbumId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/plus/views/OneUpListener;->onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$string;->plus_one_removed_confirmation:I

    :goto_1
    const/16 v3, 0x4000

    invoke-static {v3}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-interface {v3, p0, v0}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$string;->plus_one_added_confirmation:I

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->uninstall()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onDraw(Landroid/graphics/Canvas;)V

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mBackgroundOffset:I

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarOverlayBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->updateAccessibility()V

    return-void

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1    # I
    .param p2    # I

    invoke-super/range {p0 .. p2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingLeft()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginLeft:I

    add-int v11, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingTop()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    sub-int v12, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getMeasuredWidth()I

    move-result v10

    sub-int v0, v10, v11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginRight:I

    sub-int v9, v0, v1

    iput v12, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mBackgroundOffset:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    add-int/2addr v0, v11

    sget v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginTop:I

    add-int/2addr v2, v12

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget v4, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    add-int/2addr v4, v0

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    add-int/2addr v5, v2

    invoke-virtual {v3, v0, v2, v4, v5}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v5, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    add-int v0, v11, v9

    sget v3, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginRight:I

    sub-int v13, v0, v3

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    add-int v14, v12, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v5, :cond_4

    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_2
    if-eqz v5, :cond_5

    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_3
    if-eqz v5, :cond_6

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_4
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v0, v13, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1, v0, v14}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginLeft:I

    add-int/2addr v0, v11

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    add-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarMarginRight:I

    add-int v14, v0, v1

    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNameMarginTop:I

    add-int v8, v12, v0

    sub-int v0, v9, v14

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sPlusOneButtonMarginLeft:I

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    int-to-float v2, v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, v2, v4}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0, v14, v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v0

    add-int v13, v8, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v0, " "

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sDatePaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, v14, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v13

    :goto_5
    sget v1, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sAvatarSize:I

    sub-int/2addr v0, v12

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v12

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_6
    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sMarginBottom:I

    add-int/2addr v0, v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v10, v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setMeasuredDimension(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mTouchExplorer:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView$OneUpActivityTouchExplorer;->invalidateItemCache()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    move v5, v0

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v0

    goto/16 :goto_1

    :cond_4
    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto/16 :goto_2

    :cond_5
    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_3

    :cond_6
    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_4

    :cond_7
    sget v0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sCaptionMarginTop:I

    add-int v13, v12, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sContentPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sget v5, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->sFontSpacing:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    move v3, v9

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, v11, v13}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getBottom()I

    move-result v12

    goto :goto_6

    :cond_8
    move v0, v13

    goto :goto_5
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->unbindResources()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDateLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaptionLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    return-void
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAlbumId:Ljava/lang/String;

    return-void
.end method

.method public setCaption(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mCaption:Landroid/text/Spannable;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    return-void
.end method

.method public setDate(J)V
    .locals 2
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/util/Dates;->getAbbreviatedRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mDate:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    return-void
.end method

.method public setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/OneUpListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    return-void
.end method

.method public setOwner(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    const-string v0, "PhotoOneUp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===> Author name was null for gaia id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOwnerName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    const/4 v6, 0x2

    move-object v1, p0

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mClickableItems:Ljava/util/Set;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->bindResources()V

    goto :goto_0
.end method

.method public setPlusOne([B)V
    .locals 5
    .param p1    # [B

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/content/DbPlusOneData;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {v1, v4, v2, v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Ljava/lang/String;IZ)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mContentDescriptionDirty:Z

    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    :cond_0
    return-void
.end method
