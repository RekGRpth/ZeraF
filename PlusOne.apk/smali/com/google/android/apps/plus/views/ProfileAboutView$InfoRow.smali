.class final Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;
.super Ljava/lang/Object;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InfoRow"
.end annotation


# instance fields
.field container:Landroid/view/View;

.field public icon:Landroid/widget/ImageView;

.field public text:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    return-void
.end method
