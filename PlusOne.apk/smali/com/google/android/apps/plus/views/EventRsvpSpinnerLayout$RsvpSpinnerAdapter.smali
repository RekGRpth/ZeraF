.class final Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;
.super Landroid/widget/BaseAdapter;
.source "EventRsvpSpinnerLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "RsvpSpinnerAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPast:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;Landroid/content/Context;Z)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->this$0:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-boolean p3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;
    .param p1    # Ljava/lang/String;

    const/4 v0, -0x1

    const-string v1, "ATTENDING"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CHECKIN"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return v0

    :cond_2
    const-string v1, "MAYBE"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    if-eqz v1, :cond_4

    const-string v1, "NOT_ATTENDING"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const-string v1, "NOT_ATTENDING"

    invoke-static {p1, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-eqz v1, :cond_1

    sget v3, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->this$0:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "MAYBE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    sget v4, Lcom/google/android/apps/plus/R$string;->event_rsvp_maybe:I

    :cond_0
    :goto_1
    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object v1

    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$layout;->event_rsvp_attending:I

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const-string v0, "ATTENDING"

    goto :goto_0

    :pswitch_1
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mPast:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$layout;->event_rsvp_maybe:I

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const-string v0, "MAYBE"

    goto :goto_0

    :cond_2
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$layout;->event_rsvp_not_attending:I

    invoke-virtual {v3, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const-string v0, "NOT_ATTENDING"

    goto :goto_0

    :cond_3
    const-string v6, "NOT_ATTENDING"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    sget v4, Lcom/google/android/apps/plus/R$string;->event_rsvp_not_attending:I

    goto :goto_1

    :cond_4
    const-string v6, "ATTENDING"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->event_rsvp_attending:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
