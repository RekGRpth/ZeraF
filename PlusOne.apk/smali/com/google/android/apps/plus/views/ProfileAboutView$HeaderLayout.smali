.class final Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;
.super Ljava/lang/Object;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HeaderLayout"
.end annotation


# instance fields
.field public addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

.field public addedByCount:Landroid/widget/TextView;

.field public avatarChoosePhotoIcon:Landroid/widget/ImageView;

.field public avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

.field public blockedText:Landroid/widget/TextView;

.field public buttons:Landroid/view/View;

.field public circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

.field public container:Landroid/view/View;

.field public coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

.field public coverPhotoChoosePhotoIcon:Landroid/widget/ImageView;

.field public deviceLocationContainer:Landroid/view/View;

.field public deviceLocationHelp:Landroid/view/View;

.field public deviceLocationText:Landroid/widget/TextView;

.field public education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

.field public employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

.field public expandArea:Landroid/widget/TextView;

.field public familyName:Landroid/widget/TextView;

.field public fullName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

.field public givenName:Landroid/widget/TextView;

.field public location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

.field public plusOneButton:Landroid/widget/Button;

.field public progressBar:Landroid/widget/ProgressBar;

.field public scrapbookAlbum:Landroid/view/View;

.field public scrapbookPhoto:[Lcom/google/android/apps/plus/views/ImageResourceView;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/plus/views/ImageResourceView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto:[Lcom/google/android/apps/plus/views/ImageResourceView;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;-><init>()V

    return-void
.end method
