.class public Lcom/google/android/apps/plus/views/EventDetailsHeaderView;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsHeaderView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sAvatarOverlap:I

.field private static sAvatarSize:I

.field private static sCollapseText:Ljava/lang/String;

.field private static sExpandText:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sOnAirColor:I

.field private static sOnAirDrawable:Landroid/graphics/drawable/Drawable;

.field private static sOnAirText:Ljava/lang/String;

.field private static sPadding:I

.field private static sPrivatePublicColor:I

.field private static sPrivateText:Ljava/lang/String;

.field private static sPublicText:Ljava/lang/String;

.field private static sSecondaryPadding:I

.field private static sTypeSize:F


# instance fields
.field private mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

.field private mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

.field private mCanPlus:Z

.field private mChevronResId:I

.field private mExpandCollapseChevronView:Landroid/widget/ImageView;

.field private mExpandCollapseTextView:Landroid/widget/TextView;

.field private mExpandCollapseView:Landroid/view/View;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field private mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

.field private mTitleView:Landroid/widget/TextView;

.field private mTypeView:Landroid/widget/TextView;

.field private mVideoThemeUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sInitialized:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v4, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_details_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    int-to-float v1, v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_avatar_percent_overlap:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarOverlap:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_details_secondary_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sSecondaryPadding:I

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_on_air:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$color;->event_detail_on_air:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirColor:I

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_private:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivateText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_public:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPublicText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$color;->event_detail_private:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivatePublicColor:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_details_on_air_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sTypeSize:F

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_events_on_air:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_show_more:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_show_less:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sCollapseText:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventThemeView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/EventThemeView;->setFadeIn(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_details_title_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_title_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_down:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_down:I

    iput v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mChevronResId:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_subtitle_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_title_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_details_collapse_expand_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/content/DbPlusOneData;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 3
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/fragments/EventActiveState;
    .param p3    # Lcom/google/android/apps/plus/content/DbPlusOneData;
    .param p4    # Landroid/view/View$OnClickListener;
    .param p5    # Z
    .param p6    # Lcom/google/android/apps/plus/views/EventActionListener;

    const/4 v2, 0x0

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creator:Lcom/google/api/services/plusi/model/EmbedsPerson;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creator:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creator:Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeView(Landroid/view/View;)V

    if-eqz p4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    if-eqz p5, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    :cond_2
    iput-object p4, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput-object p6, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sTypeSize:F

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnImageListener(Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->plusable:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mCanPlus:Z

    return-void

    :cond_3
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPublicText:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivatePublicColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivateText:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected measureChildren(II)V
    .locals 14
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v7, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-nez v6, :cond_0

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    :cond_0
    int-to-float v7, v6

    const v8, 0x40570a3d

    div-float/2addr v7, v8

    float-to-int v4, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/high16 v8, 0x40000000

    const/high16 v9, 0x40000000

    invoke-static {v7, v6, v8, v4, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredWidth()I

    move-result v8

    const/high16 v9, 0x40000000

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredHeight()I

    move-result v10

    const/high16 v11, 0x40000000

    invoke-static {v7, v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    const/high16 v9, 0x40000000

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    const/high16 v11, 0x40000000

    invoke-static {v7, v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarOverlap:I

    sub-int/2addr v9, v10

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int v1, v7, v8

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sub-int v2, v7, v8

    const/4 v7, 0x0

    sub-int v8, v2, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-eqz v0, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v7, v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sSecondaryPadding:I

    add-int/2addr v8, v9

    sub-int v8, v3, v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    sub-int v8, v2, v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int/2addr v9, v10

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    :cond_2
    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    const/high16 v8, -0x80000000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v3, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v9, 0x2

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int/2addr v9, v10

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/high16 v8, -0x80000000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v3, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    :cond_3
    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int v5, v7, v8

    sub-int v7, v3, v5

    const/4 v8, 0x0

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    if-eqz v0, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    const/high16 v8, -0x80000000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v3, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sSecondaryPadding:I

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    const/4 v10, 0x1

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    const/4 v7, 0x2

    new-array v7, v7, [Landroid/view/View;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    aput-object v9, v7, v8

    invoke-static {v7}, Lcom/google/android/apps/plus/views/ExactLayout;->getMaxHeight([Landroid/view/View;)I

    move-result v8

    invoke-static {v8, v7}, Lcom/google/android/apps/plus/views/ExactLayout;->verticallyCenter(I[Landroid/view/View;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v8, 0x2

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v10, 0x2

    invoke-static {v8, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    sub-int v10, v7, v8

    const/high16 v11, 0x40000000

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    :goto_1
    if-ltz v7, :cond_5

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    const/4 v13, 0x3

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/views/ExactLayout;->getBound(Landroid/view/View;I)I

    move-result v12

    invoke-static {v8, v12}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int/lit8 v7, v7, -0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v12, 0x3

    invoke-static {v7, v12}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v7

    sub-int v7, v8, v7

    const/high16 v8, 0x40000000

    invoke-static {v9, v10, v11, v7, v8}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->measure(Landroid/view/View;IIII)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v9, 0x2

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getBound(Landroid/view/View;I)I

    move-result v9

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setCorner(Landroid/view/View;II)V

    :cond_6
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    check-cast p1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onAvatarClicked(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setAlpha(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->invalidate()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onEventPlusOneClicked()V

    :cond_1
    return-void
.end method

.method protected final onDrawCustom(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mCanPlus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    return v0
.end method

.method public final onImageLoadFinished$6ee69d29()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mVideoThemeUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mVideoThemeUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mVideoThemeUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->setVideoUrl(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    :cond_0
    return-void
.end method

.method protected final onMeasureCustom$7347e0dc(I)Landroid/graphics/Point;
    .locals 11
    .param p1    # I

    const/4 v0, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredWidth()I

    move-result v1

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sub-int/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredHeight()I

    move-result v1

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sub-int/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v3

    if-eqz v3, :cond_0

    move v5, v0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v3, :cond_1

    move v3, v0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    new-array v10, v0, [Ljava/lang/Object;

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v10, v2

    invoke-virtual {v4, v6, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v5, :cond_2

    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_2
    if-eqz v5, :cond_3

    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_3
    if-eqz v5, :cond_4

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_4
    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, v7, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int v2, v8, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, v9}, Landroid/graphics/Point;-><init>(II)V

    return-object v0

    :cond_0
    move v5, v2

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v3

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto :goto_2

    :cond_3
    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_3

    :cond_4
    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_4
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x7d0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, v3}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p1, v3}, Landroid/media/MediaPlayer;->setLooping(Z)V

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    return-void
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mCanPlus:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mVideoThemeUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnImageListener(Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->stopPlayback()V

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public final pausePlayback()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->stopPlayback()V

    :cond_0
    return-void
.end method

.method public final resumePlayback()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->startPlayback()V

    :cond_0
    return-void
.end method

.method public setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/Theme;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Theme;->image:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Theme;->image:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/ThemeImage;

    const-string v2, "MOV"

    iget-object v3, v1, Lcom/google/api/services/plusi/model/ThemeImage;->format:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "LARGE"

    iget-object v3, v1, Lcom/google/api/services/plusi/model/ThemeImage;->aspectRatio:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    const-string v3, "mp4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mVideoThemeUrl:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/EventThemeView;->setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V

    return-void
.end method

.method public setExpandState(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_up:I

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mChevronResId:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sCollapseText:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mChevronResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sCollapseText:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    if-eqz p1, :cond_4

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sCollapseText:Ljava/lang/String;

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/views/EventActionListener;->onExpansionToggled(Z)V

    :cond_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_down:I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    goto :goto_3
.end method

.method public setLayoutType(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final stop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAnimatedThemeView:Lcom/google/android/apps/plus/views/AnimatedThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->clearAnimation()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setAlpha(F)V

    :cond_0
    return-void
.end method
