.class public final Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;
.super Landroid/text/style/URLSpan;
.source "ClickableStaticLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ClickableStaticLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StateURLSpan"
.end annotation


# instance fields
.field private mBgColor:I

.field private mClicked:Z

.field private mFirstTime:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mFirstTime:Z

    return-void
.end method


# virtual methods
.method public final setClicked(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mClicked:Z

    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 3
    .param p1    # Landroid/text/TextPaint;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mFirstTime:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mFirstTime:Z

    iget v0, p1, Landroid/text/TextPaint;->bgColor:I

    iput v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mBgColor:I

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mClicked:Z

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_1

    const v0, -0xcc4a1b

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    :goto_0
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {p1, v2}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    return-void

    :cond_1
    const/16 v0, -0x8000

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->mBgColor:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    goto :goto_0
.end method
