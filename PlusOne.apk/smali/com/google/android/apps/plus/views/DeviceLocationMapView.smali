.class public Lcom/google/android/apps/plus/views/DeviceLocationMapView;
.super Lcom/google/android/apps/plus/views/ImageResourceView;
.source "DeviceLocationMapView.java"


# instance fields
.field private final mAspectRatio:I

.field private final mMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/DeviceLocationMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->setSizeCategory(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->setScaleMode(I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->places_map_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->mMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->device_location_map_aspect_ratio:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->mAspectRatio:I

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->mMaxWidth:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit8 v2, v1, 0x64

    iget v3, p0, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->mAspectRatio:I

    div-int v0, v2, v3

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/DeviceLocationMapView;->setMeasuredDimension(II)V

    return-void
.end method
