.class public Lcom/google/android/apps/plus/views/PeopleListItemView;
.super Lcom/google/android/apps/plus/views/CheckableListItemView;
.source "PeopleListItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;
    }
.end annotation


# static fields
.field private static sAddButtonIcon:Landroid/graphics/drawable/Drawable;

.field private static sDefaultUserImage:Landroid/graphics/Bitmap;

.field private static sMediumAvatarSize:I

.field private static sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

.field private static sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sSmallAvatarSize:I

.field private static sTinyAvatarSize:I

.field private static sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

.field private static sVerticalDivider:Landroid/graphics/drawable/Drawable;

.field private static sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

.field private static sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mActionButton:Landroid/widget/TextView;

.field private final mActionButtonResourceId:I

.field private mActionButtonVisible:Z

.field private final mActionButtonWidth:I

.field private mAddButton:Landroid/widget/ImageView;

.field private mAddButtonVisible:Z

.field private final mAvatarBounds:Landroid/graphics/Rect;

.field private final mAvatarOriginalBounds:Landroid/graphics/Rect;

.field private final mAvatarPaint:Landroid/graphics/Paint;

.field private mAvatarRequestSize:I

.field private mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

.field private final mAvatarSize:I

.field private mAvatarUrl:Ljava/lang/String;

.field private mAvatarVisible:Z

.field private final mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

.field private final mCircleIconSize:I

.field private mCircleIconVisible:Z

.field private mCircleLineHeight:I

.field private mCircleListVisible:Z

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private final mCirclesTextColor:I

.field private final mCirclesTextSize:F

.field private final mCirclesTextView:Landroid/widget/TextView;

.field private mContactLookupKey:Ljava/lang/String;

.field private mDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private mDisplayName:Ljava/lang/String;

.field private final mEmailIconPaddingLeft:I

.field private final mEmailIconPaddingTop:I

.field private final mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

.field private mFirstRow:Z

.field private mGaiaId:Ljava/lang/String;

.field private final mGapBetweenIconAndCircles:I

.field private final mGapBetweenImageAndText:I

.field private final mGapBetweenNameAndCircles:I

.field private final mGapBetweenTextAndButton:I

.field private mHighlightedText:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

.field private final mNameTextBuilder:Landroid/text/SpannableStringBuilder;

.field private final mNameTextView:Landroid/widget/TextView;

.field private final mPaddingBottom:I

.field private final mPaddingLeft:I

.field private final mPaddingRight:I

.field private final mPaddingTop:I

.field private mPersonId:Ljava/lang/String;

.field private mPlusPage:Z

.field private final mPreferredHeight:I

.field private mRemoveButton:Landroid/widget/ImageView;

.field private mRemoveButtonVisible:Z

.field protected mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

.field protected mSectionHeaderHeight:I

.field protected mSectionHeaderVisible:Z

.field private mTypeTextView:Landroid/widget/TextView;

.field private mTypeTextViewVisible:Z

.field private mUnblockButton:Landroid/widget/ImageView;

.field private mUnblockButtonVisible:Z

.field private mVerticalDividerLeft:I

.field private final mVerticalDividerPadding:I

.field private final mVerticalDividerWidth:I

.field private mWellFormedEmail:Ljava/lang/String;

.field private mWellFormedEmailMode:Z

.field private mWellFormedSms:Ljava/lang/String;

.field private mWellFormedSmsMode:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v6, -0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CheckableListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v2, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->ContactListItemView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingTop:I

    invoke-virtual {v0, v7, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingBottom:I

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingLeft:I

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingRight:I

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextSize:F

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenImageAndText:I

    const/4 v2, 0x7

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenNameAndCircles:I

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenTextAndButton:I

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    const/16 v2, 0xf

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-virtual {v0, v8, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    const/16 v2, 0x11

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextColor:I

    const/16 v2, 0x12

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingTop:I

    const/16 v2, 0x13

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingLeft:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    const v3, 0x1030044

    invoke-virtual {v2, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    const v3, 0x1030044

    invoke-virtual {v2, p1, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextSize:F

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->divider:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->profile_email:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->profile_sms:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    :cond_4
    sget-object v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    if-nez v2, :cond_5

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sSmallAvatarSize:I

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/PeopleListItemView;->sTinyAvatarSize:I

    :cond_5
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    if-le v2, v3, :cond_6

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    :goto_0
    return-void

    :cond_6
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sMediumAvatarSize:I

    if-ne v2, v3, :cond_7

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sSmallAvatarSize:I

    if-le v2, v3, :cond_8

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_0

    :cond_8
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sSmallAvatarSize:I

    if-ne v2, v3, :cond_9

    iput v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sTinyAvatarSize:I

    if-le v2, v3, :cond_a

    iput v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/PeopleListItemView;->sTinyAvatarSize:I

    if-ne v2, v3, :cond_b

    iput v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    goto :goto_0

    :cond_b
    iput v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    goto :goto_0
.end method

.method public static createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;
    .locals 5
    .param p0    # Landroid/content/Context;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    :try_start_0
    const-string v1, "com.google.android.apps.plus.views.PeopleListItemViewV11"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListItemView;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "PeopleListItemView"

    const-string v2, "Cannot instantiate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private updateDisplayName()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/PeopleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v5, Lcom/google/android/apps/plus/views/PeopleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarUrl:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarRequestSize:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    if-eqz v6, :cond_3

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v5

    div-int/lit8 v7, v7, 0x2

    add-int v2, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v1

    div-int/lit8 v7, v7, 0x2

    add-int v3, v6, v7

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v3, v1

    invoke-virtual {v6, v2, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    :goto_0
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-nez v6, :cond_1

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-eqz v6, :cond_2

    :cond_1
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v6, :cond_8

    iget v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    add-int v4, v6, v7

    :goto_1
    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    iget v9, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getHeight()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    sub-int/2addr v9, v10

    invoke-virtual {v6, v7, v4, v8, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sVerticalDivider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/CheckableListItemView;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void

    :cond_3
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    if-eqz v6, :cond_4

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v5

    div-int/lit8 v7, v7, 0x2

    add-int v2, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    sub-int/2addr v7, v1

    div-int/lit8 v7, v7, 0x2

    add-int v3, v6, v7

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v3, v1

    invoke-virtual {v6, v2, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedSmsIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_4
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_5
    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v6, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :cond_6
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-virtual {v6, v9, v9, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarOriginalBounds:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_7
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v6, :cond_0

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLeft()I

    move-result v6

    sub-int/2addr v6, v5

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingLeft:I

    sub-int v2, v6, v7

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingTop:I

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    add-int v7, v2, v5

    add-int v8, v3, v1

    invoke-virtual {v6, v2, v3, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_0

    :cond_8
    iget v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    goto/16 :goto_1
.end method

.method protected final drawBackground(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v2, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getHeight()I

    move-result v3

    invoke-virtual {p2, v1, v0, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getGaiaId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public getWellFormedEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getWellFormedSms()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSms:Ljava/lang/String;

    return-object v0
.end method

.method public isPlusPage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPlusPage:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->bindResources()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x2

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    const/4 v1, 0x3

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;->onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CheckableListItemView;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->unbindResources()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 32
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v14, p5, p3

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    sub-int v30, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    move/from16 v31, v0

    invoke-virtual/range {v27 .. v31}, Lcom/google/android/apps/plus/views/SectionHeaderView;->layout(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    move/from16 v27, v0

    add-int/lit8 v23, v27, 0x0

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingLeft:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move/from16 v0, v16

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    sub-int v28, v14, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v29, v0

    sub-int v28, v28, v29

    div-int/lit8 v28, v28, 0x2

    add-int v28, v28, v23

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarBounds:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move/from16 v0, v28

    move-object/from16 v1, v27

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenImageAndText:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v16, v16, v27

    :cond_1
    sub-int v27, p4, p2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingRight:I

    move/from16 v28, v0

    sub-int v20, v27, v28

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    sub-int v5, v20, v6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v27, v0

    sub-int v27, v5, v27

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v5, v6

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v0, v5, v1, v2, v14}, Landroid/widget/TextView;->layout(IIII)V

    sub-int v20, v20, v6

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v26

    sub-int v27, v20, v26

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v23

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v14}, Landroid/widget/ImageView;->layout(IIII)V

    sub-int v20, v20, v26

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v19

    sub-int v27, v20, v19

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v29, v0

    add-int v28, v28, v29

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v23

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v14}, Landroid/widget/ImageView;->layout(IIII)V

    sub-int v20, v20, v19

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_a

    sub-int v27, v20, v8

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    move/from16 v28, v0

    add-int v7, v27, v28

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    move-object/from16 v27, v0

    add-int v28, v7, v8

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-virtual {v0, v7, v1, v2, v14}, Landroid/widget/ImageView;->layout(IIII)V

    sub-int v20, v20, v8

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v9

    sub-int v27, v14, v23

    sub-int v27, v27, v9

    div-int/lit8 v27, v27, 0x2

    add-int v10, v23, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v27, v0

    sub-int v28, v20, v11

    add-int v29, v10, v9

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v20

    move/from16 v3, v29

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/widget/CheckBox;->layout(IIII)V

    sub-int v20, v20, v11

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    move/from16 v27, v0

    if-nez v27, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_8

    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenTextAndButton:I

    move/from16 v27, v0

    sub-int v20, v20, v27

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v25

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    move/from16 v27, v0

    if-nez v27, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    sub-int v27, v14, v23

    sub-int v27, v27, v17

    div-int/lit8 v27, v27, 0x2

    add-int v22, v23, v27

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    sub-int v28, v20, v25

    add-int v29, v22, v17

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v22

    move/from16 v3, v20

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    move/from16 v27, v0

    add-int v27, v27, v25

    sub-int v20, v20, v27

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v22, v17

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v22

    move/from16 v3, v20

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    :goto_2
    return-void

    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerLeft:I

    move/from16 v27, v0

    sub-int v7, v27, v8

    goto/16 :goto_0

    :cond_b
    const/16 v25, 0x0

    goto :goto_1

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v12

    move/from16 v24, v12

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v27, v0

    move/from16 v0, v27

    move/from16 v1, v24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v24

    :cond_d
    add-int v24, v24, v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    move/from16 v27, v0

    sub-int v27, v27, v24

    div-int/lit8 v27, v27, 0x2

    add-int v23, v23, v27

    move/from16 v18, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    move-object/from16 v27, v0

    if-eqz v27, :cond_e

    sget-object v27, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailIconPaddingLeft:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v18, v18, v27

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v23, v17

    move-object/from16 v0, v27

    move/from16 v1, v18

    move/from16 v2, v23

    move/from16 v3, v20

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenNameAndCircles:I

    move/from16 v27, v0

    add-int v27, v27, v17

    add-int v23, v23, v27

    move/from16 v21, v16

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    div-int/lit8 v27, v27, 0x2

    add-int v15, v23, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v28, v0

    add-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v29, v0

    add-int v29, v29, v15

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v21, v21, v27

    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    move/from16 v27, v0

    sub-int v27, v27, v12

    div-int/lit8 v27, v27, 0x2

    add-int v13, v23, v27

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    move/from16 v27, v0

    if-eqz v27, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    sub-int v28, v20, v25

    add-int v29, v13, v12

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v20

    move/from16 v3, v29

    invoke-virtual {v0, v1, v13, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    move/from16 v27, v0

    add-int v27, v27, v25

    sub-int v20, v20, v27

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v13, v12

    move-object/from16 v0, v27

    move/from16 v1, v21

    move/from16 v2, v20

    move/from16 v3, v28

    invoke-virtual {v0, v1, v13, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_2

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v17

    sub-int v27, v14, v23

    sub-int v27, v27, v17

    div-int/lit8 v27, v27, 0x2

    add-int v22, v23, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    move-object/from16 v27, v0

    add-int v28, v22, v17

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v22

    move/from16 v3, v20

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    goto/16 :goto_2
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/high16 v11, 0x40000000

    const/4 v10, 0x0

    invoke-static {v10, p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->resolveSize(II)I

    move-result v6

    const/4 v3, 0x0

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingLeft:I

    sub-int v7, v6, v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingRight:I

    sub-int v5, v7, v8

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenImageAndText:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    :cond_0
    const/4 v1, 0x0

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v7, v10, p2}, Landroid/widget/TextView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v1

    sub-int/2addr v5, v7

    :cond_1
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/ImageView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    :cond_2
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/ImageView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    :cond_3
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/ImageView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerWidth:I

    add-int/2addr v7, v8

    sub-int/2addr v5, v7

    :cond_4
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v10, p2}, Landroid/widget/CheckBox;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getMeasuredHeight()I

    move-result v7

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v5, v7

    :cond_5
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-nez v7, :cond_6

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-nez v7, :cond_6

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-nez v7, :cond_6

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-nez v7, :cond_6

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCheckBoxVisible:Z

    if-eqz v7, :cond_7

    :cond_6
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenTextAndButton:I

    sub-int/2addr v5, v7

    :cond_7
    move v4, v5

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    if-eqz v7, :cond_8

    sget-object v7, Lcom/google/android/apps/plus/views/PeopleListItemView;->sWellFormedEmailIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    :cond_8
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-static {v4, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, p2}, Landroid/widget/TextView;->measure(II)V

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarSize:I

    move v2, v5

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    if-eqz v7, :cond_9

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    add-int/2addr v7, v8

    sub-int/2addr v2, v7

    :cond_9
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v10, v10}, Landroid/widget/TextView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenIconAndCircles:I

    add-int/2addr v7, v8

    sub-int/2addr v2, v7

    :cond_a
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v10, v10}, Landroid/widget/TextView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconSize:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v9, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    invoke-static {v9, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGapBetweenNameAndCircles:I

    add-int/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleLineHeight:I

    add-int/2addr v7, v8

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    :cond_b
    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingTop:I

    iget v8, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPaddingBottom:I

    add-int/2addr v7, v8

    add-int/2addr v3, v7

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPreferredHeight:I

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-static {v3, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/TextView;->measure(II)V

    :cond_c
    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v7, :cond_d

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {v7, p1, v10}, Lcom/google/android/apps/plus/views/SectionHeaderView;->measure(II)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/SectionHeaderView;->getMeasuredHeight()I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    iget v7, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderHeight:I

    add-int/2addr v3, v7

    :cond_d
    invoke-virtual {p0, v6, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->unbindResources()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPersonId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPlusPage:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmailMode:Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSms:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->invalidate()V

    return-void
.end method

.method public setActionButtonLabel(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-nez v1, :cond_0

    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mVerticalDividerPadding:I

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setActionButtonVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonVisible:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-nez v0, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->add:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setActionButtonLabel(I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButton:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAddButtonVisible(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButtonVisible:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sAddButtonIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_btn_add_member:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sAddButtonIcon:Landroid/graphics/drawable/Drawable;

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sAddButtonIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->add_to_circles:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAddButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAvatarVisible(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    return-void
.end method

.method public setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    return-void
.end method

.method public setContactIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mContactLookupKey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->bindResources()V

    :cond_1
    return-void
.end method

.method public setContactName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateDisplayName()V

    return-void
.end method

.method public setCustomText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDefaultAvatar(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    return-void

    :cond_0
    sget-object p1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public setFirstRow(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mFirstRow:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarVisible:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mGaiaId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarUrl:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->bindResources()V

    :cond_0
    return-void
.end method

.method public setHighlightedText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    goto :goto_0
.end method

.method public setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    return-void
.end method

.method public setPackedCircleIds(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public setPackedCircleIdsAndEmailAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIdsEmailAddressAndPhoneNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setPackedCircleIdsEmailAddressAndPhoneNumber(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIdsEmailAddressPhoneNumberAndSnippet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setPackedCircleIdsEmailAddressPhoneNumberAndSnippet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-static/range {p4 .. p4}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->getStringForPhoneType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    const v4, 0x1030044

    invoke-virtual {v3, v1, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextSize:F

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextColor:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    const/16 v3, 0x10

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextView:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mTypeTextViewVisible:Z

    if-eqz v1, :cond_b

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v9, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v9, 0x1

    const/4 v11, 0x0

    :goto_3
    const/16 v1, 0x7c

    invoke-virtual {p1, v1, v11}, Ljava/lang/String;->indexOf(II)I

    move-result v12

    const/4 v1, -0x1

    if-eq v12, v1, :cond_4

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v11, v12, 0x1

    goto :goto_3

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "|"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$plurals;->circle_count_and_matched_email:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v10, v4, v5

    invoke-virtual {v1, v3, v9, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$plurals;->circle_count_and_matched_email:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p3, v4, v5

    invoke-virtual {v1, v3, v9, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/views/PeopleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v6, Lcom/google/android/apps/plus/views/PeopleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mEmailTextBuilder:Landroid/text/SpannableStringBuilder;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mHighlightedText:Ljava/lang/String;

    sget-object v7, Lcom/google/android/apps/plus/views/PeopleListItemView;->sBoldSpan:Landroid/text/style/StyleSpan;

    sget-object v8, Lcom/google/android/apps/plus/views/PeopleListItemView;->sColorSpan:Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v4, p3

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/plus/util/SpannableUtils;->setTextWithHighlight$5cdafd0b(Landroid/widget/TextView;Ljava/lang/String;Landroid/text/SpannableStringBuilder;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    goto/16 :goto_0

    :cond_6
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_7
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-static/range {p6 .. p6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleIconVisible:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_a
    const/16 v1, 0x8

    goto/16 :goto_1

    :cond_b
    const/16 v1, 0x8

    goto/16 :goto_2
.end method

.method public setPersonId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPersonId:Ljava/lang/String;

    return-void
.end method

.method public setPlusPage(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mPlusPage:Z

    return-void
.end method

.method public setRemoveButtonVisible(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButtonVisible:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_btn_dismiss_person:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sRemoveButtonIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->remove_from_circles:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mRemoveButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSectionHeader(C)V
    .locals 2
    .param p1    # C

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setSectionHeaderBackgroundColor()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$color;->section_header_opaque_bg:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setBackgroundColor(I)V

    return-void
.end method

.method public setSectionHeaderVisible(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeaderVisible:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->section_header:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SectionHeaderView;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderBackgroundColor()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mSectionHeader:Lcom/google/android/apps/plus/views/SectionHeaderView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setUnblockButtonVisible(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButtonVisible:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mActionButtonResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->list_unblock:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListItemView;->sUnblockButtonIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_item_unblock_person:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->addView(Landroid/view/View;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mUnblockButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setWellFormedEmail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateDisplayName()V

    return-void
.end method

.method public setWellFormedSms(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSmsMode:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedSms:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method public updateContentDescription()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCirclesTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mCircleListVisible:Z

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-lez v2, :cond_1

    sget v2, Lcom/google/android/apps/plus/R$string;->person_with_subtitle_entry_content_description:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    if-eqz v2, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$string;->person_entry_content_description:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mDisplayName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$string;->person_entry_email_content_description:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListItemView;->mWellFormedEmail:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
