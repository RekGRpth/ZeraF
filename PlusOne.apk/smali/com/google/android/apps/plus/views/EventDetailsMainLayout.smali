.class public Lcom/google/android/apps/plus/views/EventDetailsMainLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsMainLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ItemClickListener;


# static fields
.field private static sDescriptionTextPaint:Landroid/text/TextPaint;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sGoingLabelColor:I

.field private static sGoingLabelSize:I

.field private static sGoingLabelText:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sPadding:I

.field private static sWentLabelText:Ljava/lang/String;


# instance fields
.field private mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

.field private mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

.field private mRsvpLabel:Landroid/widget/TextView;

.field private mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

.field private mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v9, -0x1

    const/4 v8, -0x2

    const/4 v6, 0x1

    sget-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sInitialized:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_padding:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/apps/plus/R$color;->card_event_divider:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_divider_stroke_width:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v1, Lcom/google/android/apps/plus/R$color;->event_card_details_description_color:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_details_description_size:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v1, Lcom/google/android/apps/plus/R$color;->comment_link:I

    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Landroid/text/TextPaint;->linkColor:I

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_details_description_size:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v0, Lcom/google/android/apps/plus/R$color;->event_card_details_going_label:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelColor:I

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_details_going_label_text_size:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelSize:I

    sget v0, Lcom/google/android/apps/plus/R$string;->event_rsvp_are_you_going:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelText:Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_rsvp_are_you_going_past:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sWentLabelText:Ljava/lang/String;

    sput-boolean v6, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sInitialized:Z

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ConstrainedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sDescriptionTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setTextPaint(Landroid/text/TextPaint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    sget v1, Lcom/google/android/apps/plus/R$id;->event_instant_share_selection:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setId(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    new-instance v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v1, v9, v8}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    sget v1, Lcom/google/android/apps/plus/R$id;->event_rsvp_section:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setId(I)V

    sget v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelSize:I

    int-to-float v3, v0

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelColor:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v1, v9, v8}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/fragments/EventActiveState;
    .param p3    # Lcom/google/android/apps/plus/views/EventActionListener;

    const/16 v5, 0x8

    const/4 v4, 0x0

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionHtml:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionHtml:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setHtmlText(Ljava/lang/String;Z)V

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sWentLabelText:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->bind$3ba8bae(Lcom/google/api/services/plusi/model/PlusEvent;)V

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v2, :cond_5

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v2, p1, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->setVisibility(I)V

    :goto_2
    iget-boolean v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    if-nez v2, :cond_1

    iget-boolean v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    if-eqz v2, :cond_6

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setListener(Lcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->bind(Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setVisibility(I)V

    :goto_3
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->canRsvp(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->eventSource:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setVisibility(I)V

    :goto_4
    return-void

    :cond_2
    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sGoingLabelText:Ljava/lang/String;

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->setVisibility(I)V

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setVisibility(I)V

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setVisibility(I)V

    goto :goto_4
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->clear()V

    return-void
.end method

.method protected measureChildren(II)V
    .locals 18
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    const/4 v1, 0x0

    sget v7, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/lit8 v4, v7, 0x0

    sget v7, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    mul-int/lit8 v7, v7, 0x2

    sub-int v5, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->getLength()I

    move-result v7

    if-lez v7, :cond_3

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setVisibility(I)V

    add-int/lit8 v2, v3, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    sub-int v8, v5, v8

    const/high16 v9, -0x80000000

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v2, v10}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/2addr v8, v4

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->getMeasuredHeight()I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/2addr v7, v8

    add-int/lit8 v1, v7, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    const/high16 v8, 0x40000000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v5, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-static {v7, v4, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLabel:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sget v8, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->sPadding:I

    add-int/2addr v7, v8

    add-int/2addr v1, v7

    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    const/high16 v8, 0x40000000

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v7, v6, v8, v9, v10}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    const/4 v8, 0x0

    invoke-static {v7, v8, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mRsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v1, v7

    :cond_1
    const/4 v7, 0x3

    new-array v11, v7, [Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mInstantShareRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    aput-object v8, v11, v7

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mTimeRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;

    aput-object v8, v11, v7

    const/4 v7, 0x2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mLocationRow:Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;

    aput-object v8, v11, v7

    const/4 v9, 0x0

    array-length v12, v11

    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v7, 0x0

    move/from16 v17, v7

    move v7, v10

    move/from16 v10, v17

    :goto_1
    if-ge v10, v12, :cond_5

    if-nez v7, :cond_2

    aget-object v7, v11, v10

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getVisibility()I

    move-result v7

    if-nez v7, :cond_4

    :cond_2
    const/4 v7, 0x1

    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_4
    const/4 v7, 0x0

    goto :goto_2

    :cond_5
    const/4 v7, 0x0

    move v10, v9

    move v9, v8

    move v8, v7

    :goto_3
    if-ge v8, v12, :cond_8

    aget-object v7, v11, v8

    const/4 v13, 0x0

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getVisibility()I

    move-result v14

    const/16 v15, 0x8

    if-eq v14, v15, :cond_6

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->setFirst(Z)V

    const/high16 v14, -0x80000000

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v7, v6, v14, v15, v0}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->measure(Landroid/view/View;IIII)V

    invoke-static {v7, v13, v1}, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->setCorner(Landroid/view/View;II)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->getMeasuredHeight()I

    move-result v7

    :goto_4
    add-int/2addr v10, v7

    add-int/2addr v1, v7

    if-eqz v9, :cond_7

    if-nez v7, :cond_7

    const/4 v7, 0x1

    :goto_5
    add-int/lit8 v8, v8, 0x1

    move v9, v7

    goto :goto_3

    :cond_6
    const/4 v7, 0x0

    goto :goto_4

    :cond_7
    const/4 v7, 0x0

    goto :goto_5

    :cond_8
    return-void
.end method

.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 2
    .param p1    # Landroid/text/style/URLSpan;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsMainLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onLinkClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
