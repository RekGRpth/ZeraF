.class public Lcom/google/android/apps/plus/views/LinkPreviewView;
.super Lcom/google/android/apps/plus/views/OneUpLinkView;
.source "LinkPreviewView.java"


# static fields
.field private static sLinkPreviewViewInitialized:Z

.field private static sMinExposureLand:I

.field private static sMinExposurePort:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/OneUpLinkView;-><init>(Landroid/content/Context;)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/LinkPreviewView;->sLinkPreviewViewInitialized:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/LinkPreviewView;->sLinkPreviewViewInitialized:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->share_preview_margin_top_landscape:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/LinkPreviewView;->sMinExposureLand:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->share_preview_margin_top_portrait:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/LinkPreviewView;->sMinExposurePort:I

    :cond_0
    return-void
.end method


# virtual methods
.method protected final getMinExposureLand()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/LinkPreviewView;->sMinExposureLand:I

    return v0
.end method

.method protected final getMinExposurePort()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/LinkPreviewView;->sMinExposurePort:I

    return v0
.end method
