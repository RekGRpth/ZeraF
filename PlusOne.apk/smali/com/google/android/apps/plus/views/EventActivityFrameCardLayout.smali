.class public Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;
.super Lcom/google/android/apps/plus/views/CardViewLayout;
.source "EventActivityFrameCardLayout.java"


# static fields
.field private static sAvatarLineupMarginBottom:I

.field private static sAvatarLineupMarginLeft:I

.field private static sAvatarLineupMarginRight:I

.field private static sCheckinIconDrawable:Landroid/graphics/drawable/Drawable;

.field private static sDateTextColor:I

.field private static sDateTextSize:I

.field private static sInitialized:Z

.field private static sInvitedIconDrawable:Landroid/graphics/drawable/Drawable;

.field private static sPaddingBottom:I

.field private static sPaddingLeft:I

.field private static sPaddingRight:I

.field private static sPaddingTop:I


# instance fields
.field private mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

.field private mDate:Landroid/widget/TextView;

.field private mDescription:Landroid/widget/TextView;

.field private mIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getText(ILjava/util/ArrayList;)Ljava/lang/CharSequence;
    .locals 13
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/4 v5, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    array-length v8, v0

    if-lt v7, v8, :cond_1

    array-length v7, v0

    add-int/lit8 v7, v7, -0x1

    aget-object v1, v0, v7

    new-array v7, v12, [Ljava/lang/Object;

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    :goto_1
    return-object v5

    :pswitch_0
    sget v7, Lcom/google/android/apps/plus/R$array;->event_activity_invite_strings:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    sget v7, Lcom/google/android/apps/plus/R$array;->event_activity_rsvp_no_strings:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    sget v7, Lcom/google/android/apps/plus/R$array;->event_activity_rsvp_yes_strings:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget v7, Lcom/google/android/apps/plus/R$array;->event_activity_checked_in_strings:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    iget v7, v4, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->numAdditionalGuests:I

    if-nez v7, :cond_2

    iget-object v7, v4, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    :goto_3
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    sget v7, Lcom/google/android/apps/plus/R$plurals;->event_invitee_with_guests:I

    iget v8, v4, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->numAdditionalGuests:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, v4, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    aput-object v10, v9, v11

    iget v10, v4, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->numAdditionalGuests:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v6, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    aget-object v1, v0, v7

    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v7

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final bind(IJLjava/util/List;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 6
    .param p1    # I
    .param p2    # J
    .param p5    # Lcom/google/android/apps/plus/views/EventActionListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;",
            "Lcom/google/android/apps/plus/views/EventActionListener;",
            ")V"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    iget-object v4, v3, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    if-eqz v4, :cond_1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_1
    sget-object v4, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sCheckinIconDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_2
    sget-object v4, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sInvitedIconDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {v4, v0, p5, v2}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->bind(Ljava/util/ArrayList;Lcom/google/android/apps/plus/views/EventActionListener;I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p2, p3}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->getText(ILjava/util/ArrayList;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, -0x2

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_activity_invited:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sInvitedIconDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_activity_checkin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sCheckinIconDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$color;->event_card_activity_time_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sDateTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_time_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sDateTextSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_padding_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_padding_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_padding_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_padding_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_lineup_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_lineup_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_lineup_margin_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginBottom:I

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sInitialized:Z

    :cond_0
    sget v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingLeft:I

    sget v2, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingTop:I

    sget v3, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingRight:I

    sget v4, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sPaddingBottom:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->addPadding(IIII)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sDateTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sDateTextSize:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method protected measureChildren(II)V
    .locals 15
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    add-int/lit8 v9, v10, 0x0

    add-int/lit8 v1, v5, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    const/high16 v12, -0x80000000

    invoke-static {v10, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/high16 v13, -0x80000000

    invoke-static {v5, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/ImageView;->measure(II)V

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->setCorner(Landroid/view/View;II)V

    sub-int v0, v10, v6

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    const/high16 v12, -0x80000000

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/high16 v13, -0x80000000

    invoke-static {v5, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->measure(II)V

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    sub-int v2, v9, v3

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-static {v11, v2, v12}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->setCorner(Landroid/view/View;II)V

    sub-int/2addr v0, v3

    sget v11, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginLeft:I

    sget v12, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginRight:I

    add-int/2addr v11, v12

    sub-int/2addr v0, v11

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    const/high16 v12, -0x80000000

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/high16 v13, -0x80000000

    invoke-static {v5, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->measure(II)V

    if-lez v6, :cond_0

    sget v11, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginLeft:I

    add-int/2addr v11, v6

    :goto_0
    add-int/lit8 v7, v11, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    const/4 v12, 0x0

    invoke-static {v11, v7, v12}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->setCorner(Landroid/view/View;II)V

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/view/View;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    aput-object v13, v11, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    aput-object v13, v11, v12

    invoke-static {v11}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->getMaxHeight([Landroid/view/View;)I

    move-result v8

    const/4 v11, 0x3

    new-array v11, v11, [Landroid/view/View;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    aput-object v13, v11, v12

    const/4 v12, 0x2

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    aput-object v13, v11, v12

    invoke-static {v8, v11}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->verticallyCenter(I[Landroid/view/View;)V

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getMeasuredHeight()I

    move-result v11

    add-int/lit8 v11, v11, 0x0

    sget v12, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->sAvatarLineupMarginBottom:I

    add-int v4, v11, v12

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-static {v11, v12, v4}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->setCorner(Landroid/view/View;II)V

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    const/high16 v12, -0x80000000

    invoke-static {v10, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    sub-int v13, v1, v4

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/widget/TextView;->measure(II)V

    return-void

    :cond_0
    const/4 v11, 0x0

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDate:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->mAvatarLineup:Lcom/google/android/apps/plus/views/AvatarLineupLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->clear()V

    return-void
.end method
