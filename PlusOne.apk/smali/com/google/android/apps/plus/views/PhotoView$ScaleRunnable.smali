.class final Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;
.super Ljava/lang/Object;
.source "PhotoView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/PhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScaleRunnable"
.end annotation


# instance fields
.field private mCenterX:F

.field private mCenterY:F

.field private final mPhoto:Lcom/google/android/apps/plus/views/PhotoView;

.field private mRunning:Z

.field private mStartScale:F

.field private mStartTime:J

.field private mStop:Z

.field private mTargetScale:F

.field private mVelocity:F

.field private mZoomingIn:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoView;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mPhoto:Lcom/google/android/apps/plus/views/PhotoView;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStop:Z

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStartTime:J

    sub-long v0, v3, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStartScale:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mVelocity:F

    long-to-float v7, v0

    mul-float/2addr v6, v7

    add-float v2, v5, v6

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mPhoto:Lcom/google/android/apps/plus/views/PhotoView;

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mCenterX:F

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mCenterY:F

    # invokes: Lcom/google/android/apps/plus/views/PhotoView;->scale(FFF)V
    invoke-static {v5, v2, v6, v7}, Lcom/google/android/apps/plus/views/PhotoView;->access$100(Lcom/google/android/apps/plus/views/PhotoView;FFF)V

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mTargetScale:F

    cmpl-float v5, v2, v5

    if-eqz v5, :cond_2

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mZoomingIn:Z

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mTargetScale:F

    cmpl-float v5, v2, v5

    if-lez v5, :cond_4

    const/4 v5, 0x1

    :goto_1
    if-ne v6, v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mPhoto:Lcom/google/android/apps/plus/views/PhotoView;

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mTargetScale:F

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mCenterX:F

    iget v8, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mCenterY:F

    # invokes: Lcom/google/android/apps/plus/views/PhotoView;->scale(FFF)V
    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/views/PhotoView;->access$100(Lcom/google/android/apps/plus/views/PhotoView;FFF)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->stop()V

    :cond_3
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStop:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mPhoto:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final start(FFFF)Z
    .locals 5
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mRunning:Z

    if-eqz v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iput p3, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mCenterX:F

    iput p4, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mCenterY:F

    iput p2, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mTargetScale:F

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStartTime:J

    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStartScale:F

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mTargetScale:F

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStartScale:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mZoomingIn:Z

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mTargetScale:F

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStartScale:F

    sub-float/2addr v0, v3

    const/high16 v3, 0x43960000

    div-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mVelocity:F

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mRunning:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStop:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mPhoto:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoView;->post(Ljava/lang/Runnable;)Z

    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public final stop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mRunning:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoView$ScaleRunnable;->mStop:Z

    return-void
.end method
