.class public abstract Lcom/google/android/apps/plus/views/SlidingPanelLayout;
.super Lcom/google/android/apps/plus/views/ScrollableViewGroup;
.source "SlidingPanelLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;
    }
.end annotation


# instance fields
.field protected mOpen:Z

.field protected mPanel:Landroid/view/View;

.field private mShadow:Landroid/graphics/drawable/Drawable;

.field private mShadowWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setVertical(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setVertical(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setVertical(Z)V

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->getScrollX()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mShadow:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mShadowWidth:I

    neg-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->getHeight()I

    move-result v3

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mShadow:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public final isOpen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mOpen:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " should have exactly one child"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mPanel:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_shadow_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mShadowWidth:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->navigation_shadow:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mShadow:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;->open:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setOpen(Z)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mOpen:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->setScrollEnabled(Z)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mOpen:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/SlidingPanelLayout$SavedState;->open:Z

    return-object v0
.end method

.method public setOpen(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->mOpen:Z

    return-void
.end method
