.class public Lcom/google/android/apps/plus/views/ImageResourceView;
.super Landroid/view/View;
.source "ImageResourceView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;,
        Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;
    }
.end annotation


# static fields
.field private static sAlbumCoverIcon:Landroid/graphics/Bitmap;

.field private static sBoundsRect:Landroid/graphics/RectF;

.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private static sDefaultIcon:Landroid/graphics/Bitmap;

.field private static sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sPanoramaIcon:Landroid/graphics/Bitmap;

.field private static final sScalePaint:Landroid/graphics/Paint;

.field private static sVideoIcon:Landroid/graphics/Bitmap;


# instance fields
.field private mAlbumCoverIconX:I

.field private mAlbumCoverIconY:I

.field private mAnimatedResource:Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mClearCurrentContent:Z

.field private mCustomImageHeight:I

.field private mCustomImageWidth:I

.field private mDefaultIcon:Landroid/graphics/Bitmap;

.field private mDefaultIconEnabled:Z

.field private mDefaultIconX:I

.field private mDefaultIconY:I

.field private mDestinationRect:Landroid/graphics/Rect;

.field private mDestinationRectF:Landroid/graphics/RectF;

.field private mDimmed:Z

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mFadeIn:Z

.field private mHighlightOnPress:Z

.field private mImageListener:Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;

.field private mImageResourceFlags:I

.field private mIsAlbumCover:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMatrixInverse:Landroid/graphics/Matrix;

.field private mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mOverlayDrawable:Landroid/graphics/drawable/Drawable;

.field private mOverrideMatrix:Landroid/graphics/Matrix;

.field private mOverrideMatrixInverse:Landroid/graphics/Matrix;

.field private mPanoramaIconX:I

.field private mPanoramaIconY:I

.field private mPaused:Z

.field private mReleaseImageWhenPaused:Z

.field private mRequestTime:J

.field private mResource:Lcom/google/android/apps/plus/service/MediaResource;

.field private mResourceBrokenDrawable:Landroid/graphics/drawable/Drawable;

.field private mResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

.field private mResourceLoadingDrawablePath:Ljava/lang/String;

.field private mResourceMissing:Z

.field private mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

.field private mScaleMode:I

.field private mSelectorDrawable:Landroid/graphics/drawable/Drawable;

.field private mSizeCategory:I

.field private mSourceRect:Landroid/graphics/Rect;

.field private mSourceRectF:Landroid/graphics/RectF;

.field private mVideoIconX:I

.field private mVideoIconY:I

.field private mZoomBias:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sScalePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sBoundsRect:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrixInverse:Landroid/graphics/Matrix;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mHighlightOnPress:Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrixInverse:Landroid/graphics/Matrix;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mHighlightOnPress:Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrixInverse:Landroid/graphics/Matrix;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mHighlightOnPress:Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/ImageResourceView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ImageResourceView;

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageResourceFlags:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/ImageResourceView;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ImageResourceView;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;->getMediaResource(ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/ImageResourceView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->clearDrawable()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/ImageResourceView;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/views/ImageResourceView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/ImageResourceView;
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method private clearDrawable()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private computeRects(II)V
    .locals 17
    .param p1    # I
    .param p2    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingTop()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingRight()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingBottom()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingLeft()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v13

    sub-int/2addr v13, v7

    sub-int v12, v13, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v13

    sub-int/2addr v13, v9

    sub-int v4, v13, v6

    move/from16 v0, p1

    int-to-float v13, v0

    move/from16 v0, p2

    int-to-float v14, v0

    div-float v3, v13, v14

    int-to-float v13, v12

    int-to-float v14, v4

    div-float v11, v13, v14

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    packed-switch v13, :pswitch_data_0

    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v13, v14}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    invoke-virtual {v13, v14}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRectF:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRectF:Landroid/graphics/RectF;

    sget-object v16, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual/range {v13 .. v16}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {v13, v14}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {v13}, Landroid/graphics/Matrix;->reset()V

    :cond_0
    return-void

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v13, v14, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    cmpl-float v13, v3, v11

    if-lez v13, :cond_1

    int-to-float v13, v12

    div-float/2addr v13, v3

    float-to-int v13, v13

    sub-int v13, v4, v13

    div-int/lit8 v5, v13, 0x2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    add-int v14, v9, v5

    add-int v15, v7, v12

    add-int v16, v9, v4

    sub-int v16, v16, v5

    move/from16 v0, v16

    invoke-virtual {v13, v7, v14, v15, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :cond_1
    int-to-float v13, v4

    mul-float/2addr v13, v3

    float-to-int v13, v13

    sub-int v13, v12, v13

    div-int/lit8 v5, v13, 0x2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    add-int v14, v7, v5

    add-int v15, v7, v12

    sub-int/2addr v15, v5

    add-int v16, v9, v4

    move/from16 v0, v16

    invoke-virtual {v13, v14, v9, v15, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    :pswitch_1
    cmpl-float v13, v3, v11

    if-lez v13, :cond_2

    move/from16 v0, p2

    int-to-float v13, v0

    mul-float/2addr v13, v11

    float-to-int v13, v13

    sub-int v13, p1, v13

    div-int/lit8 v5, v13, 0x2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    const/4 v14, 0x0

    sub-int v15, p1, v5

    move/from16 v0, p2

    invoke-virtual {v13, v5, v14, v15, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    add-int v14, v7, v12

    add-int v15, v9, v4

    invoke-virtual {v13, v7, v9, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    :cond_2
    move/from16 v0, p1

    int-to-float v13, v0

    div-float/2addr v13, v11

    float-to-int v10, v13

    move/from16 v0, p2

    int-to-float v13, v0

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mZoomBias:F

    mul-float/2addr v13, v14

    float-to-int v2, v13

    div-int/lit8 v13, v10, 0x2

    sub-int v13, v2, v13

    const/4 v14, 0x0

    invoke-static {v13, v14}, Ljava/lang/Math;->max(II)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    const/4 v14, 0x0

    add-int v15, v5, v10

    move/from16 v0, p1

    invoke-virtual {v13, v14, v5, v0, v15}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v13, v14, v15, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDestinationRect:Landroid/graphics/Rect;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15, v12, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->computeRects(II)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrix:Landroid/graphics/Matrix;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrix:Landroid/graphics/Matrix;

    :goto_1
    sget-object v1, Lcom/google/android/apps/plus/views/ImageResourceView;->sScalePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    goto :goto_1
.end method

.method private drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-static {p2}, Lcom/google/android/apps/plus/views/ImageResourceView;->isDrawableReady(Landroid/graphics/drawable/Drawable;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconEnabled:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIcon:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconY:I

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceBrokenDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawResourceStatus(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->computeRects(II)V

    :cond_2
    invoke-virtual {p2, v3, v3, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method private drawResourceStatus(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Landroid/graphics/drawable/Drawable;

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method private getDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getMediaResource(ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;
    .locals 6
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mCustomImageWidth:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mCustomImageHeight:I

    if-eqz v0, :cond_2

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mCustomImageWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mCustomImageHeight:I

    :goto_0
    if-nez v2, :cond_1

    if-eqz v3, :cond_3

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    move v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v4, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    invoke-virtual {v0, v1, v4, p1, p2}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    goto :goto_1
.end method

.method private hasBitmap()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->getResource()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v2, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ov_play_video_48:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sVideoIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->overlay_lightcycle:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sPanoramaIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_overlay_album:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sAlbumCoverIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_missing_photo:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sDefaultIcon:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sDefaultIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIcon:Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->stream_list_selector:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    const v2, 0x3ecccccd

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mZoomBias:F

    if-eqz p2, :cond_2

    const-string v2, "size"

    invoke-interface {p2, v6, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v2, "custom"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput v4, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    :cond_1
    :goto_0
    const-string v2, "scale"

    invoke-interface {p2, v6, v2}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v2, "zoom"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    iput v5, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    :cond_2
    :goto_1
    return-void

    :cond_3
    const-string v2, "thumbnail"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    goto :goto_0

    :cond_4
    const-string v2, "large"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x3

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    goto :goto_0

    :cond_5
    const-string v2, "portrait"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x4

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    goto :goto_0

    :cond_6
    const-string v2, "landscape"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x5

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    goto :goto_0

    :cond_7
    const-string v2, "full"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iput v5, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    goto :goto_0

    :cond_8
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid size category: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9
    const-string v2, "fit"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    iput v4, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    goto :goto_1

    :cond_a
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid scale mode: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private static isDrawableReady(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p0    # Landroid/graphics/drawable/Drawable;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    check-cast p0, Lcom/google/android/apps/plus/util/GifDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->isValid()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public bindResources()V
    .locals 3

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPaused:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Size category is not set: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->hierarchyToString(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mRequestTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageResourceFlags:I

    and-int/lit8 v0, v0, -0x5

    invoke-direct {p0, v0, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getMediaResource(ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->clearDrawable()V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    return-void
.end method

.method protected final getIntrinsicWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->hasBitmap()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final hasImage()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/MediaResource;->getStatus()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPaused:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->unbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1    # Landroid/graphics/Canvas;

    const/4 v6, 0x0

    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDimmed:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sBoundsRect:Landroid/graphics/RectF;

    const/16 v3, 0x69

    const/16 v4, 0x1f

    invoke-virtual {p1, v2, v3, v4}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->isDrawableReady(Landroid/graphics/drawable/Drawable;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->hasBitmap()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissing:Z

    if-eqz v2, :cond_b

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onDrawMedia(Landroid/graphics/Canvas;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissing:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v3

    if-ne v2, v3, :cond_5

    move v2, v0

    :goto_0
    if-eqz v2, :cond_6

    sget-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sVideoIcon:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mVideoIconX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mVideoIconY:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDimmed:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getHeight()I

    move-result v3

    invoke-virtual {v0, v1, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    return-void

    :cond_5
    move v2, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_8

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v3

    if-ne v2, v3, :cond_8

    :cond_7
    :goto_2
    if-eqz v0, :cond_a

    sget-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sPanoramaIcon:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPanoramaIconX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPanoramaIconY:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/MediaResource;->getResourceType()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_7

    :cond_9
    move v0, v1

    goto :goto_2

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mIsAlbumCover:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/views/ImageResourceView;->sAlbumCoverIcon:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAlbumCoverIconX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAlbumCoverIconY:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_b
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconEnabled:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIcon:Landroid/graphics/Bitmap;

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconY:I

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawResourceStatus(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawResourceStatus(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceBrokenDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawResourceStatus(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawResourceStatus(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected onDrawMedia(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->isDrawableReady(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->hasBitmap()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->drawDrawable(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v1, p4, p2

    sub-int v0, p5, p3

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sVideoIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mVideoIconX:I

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sVideoIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mVideoIconY:I

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sPanoramaIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPanoramaIconX:I

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sPanoramaIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPanoramaIconY:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconX:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconY:I

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sAlbumCoverIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v2, v1, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAlbumCoverIconX:I

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sAlbumCoverIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAlbumCoverIconY:I

    if-eqz p1, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->unbindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->bindResources()V

    :cond_0
    return-void
.end method

.method public onRecycle()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->unbindResources()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageListener:Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageListener:Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;->onImageLoadFinished$6ee69d29()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    return-void

    :pswitch_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mRequestTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x64

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mFadeIn:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    if-nez v2, :cond_2

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/ImageResourceView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    :cond_2
    const v2, 0x3c23d70a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setAlpha(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/ImageResourceView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    :cond_3
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageResourceFlags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/MediaResource;->isGif()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAnimatedResource:Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;-><init>(Lcom/google/android/apps/plus/views/ImageResourceView;B)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAnimatedResource:Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAnimatedResource:Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->bindResources()V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawablePath:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawablePath:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onResume()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mReleaseImageWhenPaused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPaused:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->bindResources()V

    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mReleaseImageWhenPaused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mPaused:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->unbindResources()V

    :cond_0
    return-void
.end method

.method public setCustomImageSize(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mCustomImageWidth:I

    iput p2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mCustomImageHeight:I

    return-void
.end method

.method public setDefaultIcon(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setDefaultIcon(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public setDefaultIcon(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    if-nez p1, :cond_0

    sget-object p1, Lcom/google/android/apps/plus/views/ImageResourceView;->sDefaultIcon:Landroid/graphics/Bitmap;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIcon:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setDefaultIconEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconEnabled:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDefaultIconEnabled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setDimmed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDimmed:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    return-void
.end method

.method public setFadeIn(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mFadeIn:Z

    return-void
.end method

.method public setHighlightOnPress(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mHighlightOnPress:Z

    return-void
.end method

.method public setImageResourceFlags(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageResourceFlags:I

    return-void
.end method

.method public setIsAlbumCover(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mIsAlbumCover:Z

    return-void
.end method

.method public setLocalUri(Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    return-void
.end method

.method public setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    return-void
.end method

.method public setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mClearCurrentContent:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissing:Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->bindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    goto :goto_0
.end method

.method public setOnImageListener(Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mImageListener:Lcom/google/android/apps/plus/views/ImageResourceView$OnImageLoadListener;

    return-void
.end method

.method public setOverlay(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setOverrideMatrix(Landroid/graphics/Matrix;)V
    .locals 2
    .param p1    # Landroid/graphics/Matrix;

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrixInverse:Landroid/graphics/Matrix;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverrideMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    return-void
.end method

.method public setReleaseImageWhenPaused(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mReleaseImageWhenPaused:Z

    return-void
.end method

.method public setResourceBrokenDrawable(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceBrokenDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setResourceBrokenDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceBrokenDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setResourceDownloadingDrawablePath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawablePath:Ljava/lang/String;

    return-void
.end method

.method public setResourceLoadingDrawable(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setResourceLoadingDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setResourceMissing(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissing:Z

    return-void
.end method

.method public setResourceMissingDrawable(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setResourceMissingDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResourceMissingDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method public setScaleMode(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mScaleMode:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->isSelected()Z

    move-result v0

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    return-void
.end method

.method public setSizeCategory(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSizeCategory:I

    return-void
.end method

.method public setZoomBias(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mZoomBias:F

    return-void
.end method

.method public shouldHighlightOnPress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mHighlightOnPress:Z

    return v0
.end method

.method public unbindResources()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mClearCurrentContent:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/MediaResource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/MediaResource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAnimatedResource:Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mAnimatedResource:Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->unbindResources()V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->clearDrawable()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mSelectorDrawable:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
