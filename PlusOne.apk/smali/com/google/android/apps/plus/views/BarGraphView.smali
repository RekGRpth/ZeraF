.class public Lcom/google/android/apps/plus/views/BarGraphView;
.super Landroid/view/View;
.source "BarGraphView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;,
        Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;
    }
.end annotation


# static fields
.field protected static BAR_GRAPH_HEIGHT:I

.field protected static BAR_SPACING:I

.field protected static LABEL_BAR_SPACING:I

.field protected static TOTAL_GRAPH_SPACING:I

.field protected static sBarGraphPaint:Landroid/graphics/Paint;

.field protected static sLabelTextPaint:Landroid/text/TextPaint;

.field protected static sTotalTextPaint:Landroid/text/TextPaint;

.field protected static sValueTextPaint:Landroid/text/TextPaint;


# instance fields
.field protected mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

.field protected mMaxValue:J

.field protected mTotalLayout:Landroid/text/StaticLayout;

.field protected mTotalValue:J

.field protected mUnits:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/BarGraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/BarGraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->bar_graph_total:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->bar_graph_total_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->bar_graph_total_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sLabelTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sLabelTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->bar_graph_label:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sLabelTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->bar_graph_label_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sLabelTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sLabelTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->bar_graph_label_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sValueTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sValueTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->bar_graph_value:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sValueTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->bar_graph_value_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sValueTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sValueTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->bar_graph_value_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sBarGraphPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->bar_graph_bar:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/BarGraphView;->sBarGraphPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v1, Lcom/google/android/apps/plus/R$dimen;->bar_graph_total_graph_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/BarGraphView;->TOTAL_GRAPH_SPACING:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->bar_graph_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_GRAPH_HEIGHT:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->bar_graph_label_text_bar_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/BarGraphView;->LABEL_BAR_SPACING:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->bar_graph_bar_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_SPACING:I

    :cond_0
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingLeft()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingRight()I

    move-result v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getWidth()I

    move-result v0

    sub-int/2addr v0, v8

    sub-int v12, v0, v10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    array-length v11, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingTop()I

    move-result v13

    if-lez v11, :cond_2

    int-to-float v0, v8

    int-to-float v1, v13

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v8

    int-to-float v0, v0

    neg-int v1, v13

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->TOTAL_GRAPH_SPACING:I

    add-int/2addr v0, v1

    add-int/2addr v13, v0

    :cond_2
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v11, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    aget-object v7, v0, v6

    iget-object v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabelLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    iget-object v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValueLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    iget-wide v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValue:J

    long-to-float v0, v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mMaxValue:J

    long-to-float v1, v1

    div-float v9, v0, v1

    int-to-float v0, v8

    int-to-float v1, v13

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabelLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v8

    int-to-float v0, v0

    neg-int v1, v13

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabelLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->LABEL_BAR_SPACING:I

    add-int/2addr v0, v1

    add-int/2addr v13, v0

    int-to-float v0, v8

    int-to-float v1, v13

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValueLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v8

    int-to-float v0, v0

    neg-int v1, v13

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, v7, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValueLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->LABEL_BAR_SPACING:I

    add-int/2addr v0, v1

    add-int/2addr v13, v0

    int-to-float v1, v8

    int-to-float v2, v13

    int-to-float v0, v8

    int-to-float v3, v12

    mul-float/2addr v3, v9

    add-float/2addr v3, v0

    sget v0, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_GRAPH_HEIGHT:I

    add-int/2addr v0, v13

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/BarGraphView;->sBarGraphPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    sget v0, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_GRAPH_HEIGHT:I

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_SPACING:I

    add-int/2addr v0, v1

    add-int/2addr v13, v0

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 14
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    const/high16 v0, 0x40000000

    if-ne v1, v0, :cond_1

    :goto_0
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int v3, v8, v1

    const/high16 v1, 0x40000000

    if-ne v0, v1, :cond_2

    :cond_0
    :goto_1
    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/views/BarGraphView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    const/16 v0, 0x1e0

    const/high16 v2, -0x80000000

    if-ne v1, v2, :cond_5

    const/16 v0, 0x1e0

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getPaddingBottom()I

    move-result v1

    add-int v9, v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    array-length v12, v0

    if-lez v12, :cond_4

    sget v0, Lcom/google/android/apps/plus/R$string;->network_statistics_total:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalValue:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mUnits:Ljava/lang/String;

    aput-object v4, v1, v2

    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/BarGraphView;->sTotalTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->TOTAL_GRAPH_SPACING:I

    add-int/2addr v0, v1

    add-int/2addr v0, v9

    :goto_2
    const/4 v1, 0x0

    move v9, v1

    move v10, v0

    :goto_3
    if-ge v9, v12, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    aget-object v13, v0, v9

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabel:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/BarGraphView;->sLabelTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabelLayout:Landroid/text/StaticLayout;

    iget-object v0, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabelLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->LABEL_BAR_SPACING:I

    add-int/2addr v0, v1

    add-int/2addr v10, v0

    sget v0, Lcom/google/android/apps/plus/R$string;->network_statistics_value:I

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValue:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mUnits:Ljava/lang/String;

    aput-object v4, v1, v2

    const/4 v2, 0x2

    const-wide/16 v4, 0x64

    iget-wide v6, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValue:J

    mul-long/2addr v4, v6

    iget-wide v6, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalValue:J

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Landroid/text/StaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/BarGraphView;->sValueTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValueLayout:Landroid/text/StaticLayout;

    iget-object v0, v13, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValueLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->LABEL_BAR_SPACING:I

    add-int/2addr v0, v1

    add-int v1, v10, v0

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    move v10, v1

    goto :goto_3

    :cond_3
    sget v0, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_GRAPH_HEIGHT:I

    sget v1, Lcom/google/android/apps/plus/views/BarGraphView;->BAR_SPACING:I

    add-int/2addr v0, v1

    mul-int/2addr v0, v12

    add-int v9, v10, v0

    goto/16 :goto_1

    :cond_4
    move v0, v9

    goto :goto_2

    :cond_5
    move v8, v0

    goto/16 :goto_0
.end method

.method public final update([Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;Ljava/lang/String;)V
    .locals 6
    .param p1    # [Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;
    .param p2    # Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mUnits:Ljava/lang/String;

    array-length v0, p1

    new-array v2, v0, [Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalValue:J

    const-wide/32 v2, -0x80000000

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mMaxValue:J

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    new-instance v3, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    invoke-direct {v3}, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;-><init>()V

    aput-object v3, v2, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    aget-object v2, v2, v1

    aget-object v3, p1, v1

    iget-wide v3, v3, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;->mValue:J

    iput-wide v3, v2, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValue:J

    iget-object v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    aget-object v2, v2, v1

    aget-object v3, p1, v1

    iget-object v3, v3, Lcom/google/android/apps/plus/views/BarGraphView$RowInfo;->mLabel:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mLabel:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalValue:J

    iget-object v4, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    aget-object v4, v4, v1

    iget-wide v4, v4, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValue:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mTotalValue:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mMaxValue:J

    iget-object v4, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mInternalRowInfos:[Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;

    aget-object v4, v4, v1

    iget-wide v4, v4, Lcom/google/android/apps/plus/views/BarGraphView$InternalRowInfo;->mValue:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/BarGraphView;->mMaxValue:J

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BarGraphView;->requestLayout()V

    return-void
.end method
