.class public Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;
.super Landroid/view/ViewGroup;
.source "TileOneUpTouchHandler.java"


# instance fields
.field private mBackground:Landroid/view/View;

.field private mLocation:[I

.field private mScrollView:Landroid/view/View;

.field private mTargetView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v6, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mScrollView:Landroid/view/View;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mScrollView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mScrollView:Landroid/view/View;

    const v5, 0x102000a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    int-to-float v4, v4

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    aget v4, v4, v6

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    int-to-float v4, v4

    cmpl-float v4, v2, v4

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mLocation:[I

    aget v4, v4, v9

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, v2, v4

    if-gez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mScrollView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mBackground:Landroid/view/View;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mBackground:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-ne v5, v10, :cond_4

    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-virtual {v4, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->setAction(I)V

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-eq v0, v10, :cond_2

    if-ne v0, v9, :cond_3

    :cond_2
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mTargetView:Landroid/view/View;

    :cond_3
    return v9

    :cond_4
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->getScrollX()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->getScrollY()I

    move-result v7

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    invoke-virtual {v4, v5}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v5}, Landroid/view/MotionEvent;->recycle()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public setBackground(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mBackground:Landroid/view/View;

    return-void
.end method

.method public setScrollView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->mScrollView:Landroid/view/View;

    return-void
.end method
