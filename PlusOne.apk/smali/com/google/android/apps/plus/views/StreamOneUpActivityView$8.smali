.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;
.super Landroid/widget/BaseAdapter;
.source "StreamOneUpActivityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->bind(Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaCount:I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    array-length v1, v1

    if-le v1, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    array-length v1, v1

    if-gt v1, p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v1

    if-le v1, p1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1400(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    aget-object v4, v1, p1

    if-eqz v0, :cond_4

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setClickable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    aget-object v1, v1, p1

    if-eqz v0, :cond_5

    :goto_3
    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setHighlightOnPress(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    aget-object v1, v1, p1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)[Lcom/google/android/apps/plus/views/ImageResourceView;

    move-result-object v1

    aget-object v1, v1, p1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1500(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1500(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$8;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_3
.end method
