.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;
.super Ljava/lang/Object;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 3
    .param p1    # Landroid/text/style/URLSpan;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "square"

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$300(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$2;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/plus/views/OneUpListener;->onSquareClick(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
