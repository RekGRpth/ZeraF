.class public Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;
.super Lcom/google/android/apps/plus/views/ImageCardViewGroup;
.source "SkyjamCardViewGroup.java"


# instance fields
.field private mAlbum:Ljava/lang/String;

.field private mArtist:Ljava/lang/String;

.field private mIsAlbum:Z

.field private mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mMediaTitle:Ljava/lang/String;

.field private mThumbnailUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected final createCustomLayout(Landroid/content/Context;III)I
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v3, p2, v0

    sget-object v0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->googlePlayIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    add-int/2addr v0, v1

    add-int v4, p3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mIsAlbum:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->skyjam_listen_buy:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    move-object v0, p1

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButton(Landroid/content/Context;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v4, v0

    return v4

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->skyjam_button_listen:I

    goto :goto_0
.end method

.method protected final drawCustomLayout(Landroid/graphics/Canvas;III)I
    .locals 6
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    move v1, p3

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v0, p2, v2

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->googlePlayIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v3, v0

    sget-object v4, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->googlePlayIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->googlePlayIcon:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v5, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v2, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    add-int/2addr v2, v3

    add-int v1, p3, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method

.method protected final getCenterBitmap()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mArtist:Ljava/lang/String;

    return-object v0
.end method

.method protected final getSubtitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mAlbum:Ljava/lang/String;

    return-object v0
.end method

.method protected final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mMediaTitle:Ljava/lang/String;

    return-object v0
.end method

.method protected final hasCustomLayout()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v2, 0x1f

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->isAlbum()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mIsAlbum:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mIsAlbum:Z

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mMediaTitle:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getArtist()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mArtist:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getImageUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mThumbnailUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mThumbnailUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mThumbnailUrl:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mSpan:I

    invoke-virtual {p0, p2, v2}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->getAvailableWidth(Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)I

    move-result v2

    :goto_1
    iput v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mDesiredWidth:I

    iput v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mDesiredHeight:I

    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getSong()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mMediaTitle:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mAlbum:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mListenButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getStreamOneUpActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "auto_play_music"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->onRecycle()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mMediaTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mArtist:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mAlbum:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mThumbnailUrl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;->mIsAlbum:Z

    return-void
.end method
