.class public Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;
.super Lcom/google/android/apps/plus/views/PromoCardViewGroup;
.source "PeoplePromoCardViewGroup.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;


# instance fields
.field private final mAbsoluteParentRect:Landroid/graphics/Rect;

.field private final mAbsoluteRowRect:Landroid/graphics/Rect;

.field private mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mBodyLayout:Landroid/text/StaticLayout;

.field protected mBodyText:Ljava/lang/String;

.field protected mButtonText:Ljava/lang/String;

.field private final mCardViewGroupRect:Landroid/graphics/Rect;

.field private mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

.field private mNotYetSeenSuggestedPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;",
            ">;"
        }
    .end annotation
.end field

.field private final mRelativeRowRect:Landroid/graphics/Rect;

.field private mSuggestedPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;",
            ">;"
        }
    .end annotation
.end field

.field protected mSuggestedPeopleFullBleedStartX:I

.field protected mSuggestedPeopleFullBleedWidth:I

.field protected mSuggestedPeopleHeight:I

.field protected mSuggestedPeopleStartX:I

.field protected mSuggestedPeopleWidth:I

.field private mTitleLayout:Landroid/text/StaticLayout;

.field protected mTitleText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mCardViewGroupRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mRelativeRowRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteRowRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteParentRect:Landroid/graphics/Rect;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->setClickable(Z)V

    return-void
.end method

.method private updatePeopleRows(Ljava/lang/StringBuilder;)V
    .locals 14
    .param p1    # Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v8, v2, -0x1

    :goto_0
    if-ltz v8, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->onRecycle()V

    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getSuggestedPeople()Ljava/util/ArrayList;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v5, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleHeight:I

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v12, :cond_2

    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    new-instance v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->setCardActionListener(Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    iget v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoType:I

    invoke-virtual {v0, p0, p0, v13, v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->init(Lcom/google/android/apps/plus/views/PromoCardViewGroup;Lcom/google/android/apps/plus/views/ClickableHost;Lcom/google/android/apps/plus/content/DbSuggestedPerson;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->bindResources()V

    iget v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleStartX:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleFullBleedStartX:I

    iget v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleFullBleedWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleWidth:I

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->create(Landroid/content/Context;IIIII)I

    move-result v5

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v9

    if-eqz v9, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getDescription()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v10}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    invoke-static {p1, v7}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mNotYetSeenSuggestedPeople:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final applyPromoCircleDeltas(Ljava/util/List;Z)V
    .locals 7
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;",
            ">;Z)V"
        }
    .end annotation

    if-eqz p1, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->getPersonId()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getSuggestedPeople()Ljava/util/ArrayList;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_1
    if-ltz v3, :cond_0

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->getCircleData()Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v4

    const/16 v5, 0x12d

    if-ne v4, v5, :cond_1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    :cond_0
    :goto_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v4

    const/16 v5, 0x12e

    if-ne v4, v5, :cond_0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->removeCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->updatePeopleRows(Ljava/lang/StringBuilder;)V

    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->invalidate()V

    :cond_5
    return-void
.end method

.method public bindResources()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->bindResources()V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->bindResources()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected final createCard$3fd9ad2e(IIIII)I
    .locals 13
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    move v11, p1

    sget-object v3, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr p1, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoType:I

    const/4 v5, 0x1

    if-ne v3, v5, :cond_4

    const/16 v3, 0x10

    :goto_0
    invoke-static {v4, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleText:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v5, p5

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr p1, v3

    :cond_0
    if-eq v11, p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    add-int/2addr p1, v3

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const/16 v4, 0xb

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v5, p5

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyLayout:Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr p1, v3

    :cond_2
    if-eq v11, p1, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mButtonText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    sget-object v3, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr p1, v3

    :cond_3
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleStartX:I

    iput p2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleFullBleedStartX:I

    move/from16 v0, p3

    iput v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleFullBleedWidth:I

    iput p1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleHeight:I

    move/from16 v0, p5

    iput v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeopleWidth:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v12

    const/4 v10, 0x0

    :goto_1
    if-ge v10, v12, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    move/from16 v4, p4

    move v5, p2

    move/from16 v6, p3

    move v7, p1

    move/from16 v8, p5

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->create(Landroid/content/Context;IIIII)I

    move-result p1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->setCardActionListener(Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_4
    const/16 v3, 0x11

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mButtonText:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mButtonText:Ljava/lang/String;

    const/4 v5, 0x3

    move/from16 v6, p4

    move v7, p1

    move-object v8, p0

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButton(Landroid/content/Context;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move/from16 v0, p5

    move/from16 v1, p4

    invoke-virtual {v3, v0, v1, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->setWidthAndPosition(III)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ClickableButton;->setHeight(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr p1, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_7
    sget-object v3, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p1, v3

    return p1
.end method

.method protected final drawCard(Landroid/graphics/Canvas;IIIII)I
    .locals 12
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    move/from16 v9, p6

    sget-object v1, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v1, v1, 0x2

    add-int p6, p6, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_0

    move/from16 v0, p4

    int-to-float v1, v0

    move/from16 v0, p6

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move/from16 v0, p4

    neg-int v1, v0

    int-to-float v1, v1

    move/from16 v0, p6

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int p6, p6, v1

    :cond_0
    move/from16 v0, p6

    if-eq v9, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    add-int p6, p6, v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_2

    move/from16 v0, p4

    int-to-float v1, v0

    move/from16 v0, p6

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    move/from16 v0, p4

    neg-int v1, v0

    int-to-float v1, v1

    move/from16 v0, p6

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int p6, p6, v1

    :cond_2
    move/from16 v0, p6

    if-eq v9, v0, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mButtonText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v1, v1, 0x2

    add-int p6, p6, v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v11, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    move/from16 v0, p4

    int-to-float v2, v0

    move/from16 v0, p6

    int-to-float v3, v0

    add-int v1, p4, p5

    int-to-float v4, v1

    move/from16 v0, p6

    int-to-float v5, v0

    sget-object v1, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v6, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object v1, v10

    move-object v2, p1

    move/from16 v3, p4

    move v4, p2

    move v5, p3

    move/from16 v6, p6

    move/from16 v7, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->draw(Landroid/graphics/Canvas;IIIII)I

    move-result p6

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int p6, p6, v1

    :cond_5
    sget-object v1, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p6, p6, v1

    return p6
.end method

.method public final getAppearedOnScreenViews(Landroid/view/View;)Ljava/util/ArrayList;
    .locals 7
    .param p1    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mNotYetSeenSuggestedPeople:Ljava/util/List;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mNotYetSeenSuggestedPeople:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mNotYetSeenSuggestedPeople:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mNotYetSeenSuggestedPeople:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mRelativeRowRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getRect(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mCardViewGroupRect:Landroid/graphics/Rect;

    invoke-static {v4, p0}, Lcom/google/android/apps/plus/util/ViewUtils;->getAbsoluteRect(Landroid/graphics/Rect;Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteRowRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getRect(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteRowRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mCardViewGroupRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mCardViewGroupRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteParentRect:Landroid/graphics/Rect;

    invoke-static {v4, p1}, Lcom/google/android/apps/plus/util/ViewUtils;->getAbsoluteRect(Landroid/graphics/Rect;Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mRelativeRowRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteRowRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mAbsoluteParentRect:Landroid/graphics/Rect;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/util/ViewUtils;->hasAppearedOnScreen(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mNotYetSeenSuggestedPeople:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 3
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V

    const/16 v2, 0x26

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->deserialize([B)Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getTitleText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleText:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getBodyText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mMessage:Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPromoPeopleMessage;->getButtonText()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mButtonText:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->updatePeopleRows(Ljava/lang/StringBuilder;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mPromoType:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;->onLaunchCircles(I)V

    :cond_0
    return-void
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->onRecycle()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleText:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyText:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mButtonText:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mBodyLayout:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->onRecycle()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public unbindResources()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->unbindResources()V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->unbindResources()V

    return-void
.end method
