.class public Lcom/google/android/apps/plus/views/AvatarView;
.super Landroid/view/View;
.source "AvatarView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# static fields
.field private static sBoundsRect:Landroid/graphics/RectF;

.field private static sImageSelectedPaint:Landroid/graphics/Paint;

.field private static sInitialized:Z

.field private static sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;


# instance fields
.field private mAllowNonSquare:Z

.field private mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

.field private mAvatarSize:I

.field private mAvatarUrl:Ljava/lang/String;

.field private mByGaiaId:Z

.field private mDimmed:Z

.field private mGaiaId:Ljava/lang/String;

.field private mResizePaint:Landroid/graphics/Paint;

.field private mResizeRectDest:Landroid/graphics/Rect;

.field private mResizeRectSrc:Landroid/graphics/Rect;

.field private mResizeRequired:Z

.field private mRound:Z

.field private mScale:Z

.field private mSelector:Landroid/graphics/drawable/Drawable;

.field private mSizeInPixels:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/AvatarView;->sBoundsRect:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v9, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-boolean v7, Lcom/google/android/apps/plus/views/AvatarView;->sInitialized:Z

    if-nez v7, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v7

    sput-object v7, Lcom/google/android/apps/plus/views/AvatarView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    sput-object v7, Lcom/google/android/apps/plus/views/AvatarView;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v7, Lcom/google/android/apps/plus/views/AvatarView;->sImageSelectedPaint:Landroid/graphics/Paint;

    const/high16 v8, 0x40800000

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v7, Lcom/google/android/apps/plus/views/AvatarView;->sImageSelectedPaint:Landroid/graphics/Paint;

    sget v8, Lcom/google/android/apps/plus/R$color;->image_selected_stroke:I

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v7, Lcom/google/android/apps/plus/views/AvatarView;->sImageSelectedPaint:Landroid/graphics/Paint;

    sget-object v8, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v8}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sput-boolean v4, Lcom/google/android/apps/plus/views/AvatarView;->sInitialized:Z

    :cond_0
    sget v7, Lcom/google/android/apps/plus/R$drawable;->stream_list_selector:I

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSelector:Landroid/graphics/drawable/Drawable;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    if-eqz p2, :cond_7

    const-string v7, "size"

    invoke-interface {p2, v9, v7}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Missing \'size\' attribute"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    const-string v7, "round"

    invoke-interface {p2, v9, v7}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    :cond_2
    const-string v7, "scale"

    invoke-interface {p2, v9, v7}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/plus/views/AvatarView;->mScale:Z

    :cond_3
    const-string v7, "tiny"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v4, v5

    :cond_4
    :goto_0
    iput v4, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    const-string v4, "allowNonSquare"

    invoke-interface {p2, v9, v4, v5}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAllowNonSquare:Z

    :goto_1
    iget v4, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    return-void

    :cond_5
    const-string v7, "small"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v4, "medium"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v6

    goto :goto_0

    :cond_6
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid avatar size: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_7
    iput v6, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    goto :goto_1
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mByGaiaId:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarByGaiaId(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->invalidate()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarUrl:Ljava/lang/String;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/views/AvatarView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarUrl:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->invalidate()V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->invalidate()V

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    return-void
.end method

.method public final enableScale(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mScale:Z

    return-void
.end method

.method public final getGaiaId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->unbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :cond_0
    if-nez v0, :cond_1

    iget v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mDimmed:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/views/AvatarView;->sBoundsRect:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    sget-object v2, Lcom/google/android/apps/plus/views/AvatarView;->sBoundsRect:Landroid/graphics/RectF;

    const/16 v3, 0x69

    const/16 v4, 0x1f

    invoke-virtual {p1, v2, v3, v4}, Landroid/graphics/Canvas;->saveLayerAlpha(Landroid/graphics/RectF;II)I

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRequired:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectSrc:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectDest:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_1
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mDimmed:Z

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->isPressed()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mDimmed:Z

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getWidth()I

    move-result v2

    div-int/lit8 v1, v2, 0x2

    int-to-float v2, v1

    int-to-float v3, v1

    add-int/lit8 v4, v1, -0x2

    int-to-float v4, v4

    sget-object v5, Lcom/google/android/apps/plus/views/AvatarView;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_5
    :goto_2
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :cond_6
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v5, v5, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSelector:Landroid/graphics/drawable/Drawable;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    const/high16 v11, -0x80000000

    const/high16 v10, 0x40000000

    const/4 v9, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-ne v3, v10, :cond_6

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    :cond_0
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAllowNonSquare:Z

    if-eqz v8, :cond_7

    iget v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    if-eq v1, v10, :cond_1

    if-ne v1, v11, :cond_2

    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_2
    :goto_1
    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    if-eq v2, v8, :cond_9

    const/4 v8, 0x1

    :goto_2
    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRequired:Z

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRequired:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizePaint:Landroid/graphics/Paint;

    if-nez v8, :cond_3

    new-instance v8, Landroid/graphics/Paint;

    const/4 v10, 0x2

    invoke-direct {v8, v10}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizePaint:Landroid/graphics/Paint;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectDest:Landroid/graphics/Rect;

    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectDest:Landroid/graphics/Rect;

    invoke-virtual {v8, v9, v9, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    if-le v8, v2, :cond_b

    iget-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectSrc:Landroid/graphics/Rect;

    if-nez v8, :cond_4

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectSrc:Landroid/graphics/Rect;

    :cond_4
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mScale:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectSrc:Landroid/graphics/Rect;

    iget v10, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    iget v11, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    invoke-virtual {v8, v9, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    :cond_5
    :goto_3
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setMeasuredDimension(II)V

    return-void

    :cond_6
    if-ne v3, v11, :cond_0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_0

    :cond_7
    if-ne v1, v10, :cond_8

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_4
    move v2, v0

    goto :goto_1

    :cond_8
    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    invoke-static {v2, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_4

    :cond_9
    move v8, v9

    goto :goto_2

    :cond_a
    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    sub-int/2addr v8, v2

    div-int/lit8 v4, v8, 0x2

    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    add-int/2addr v8, v2

    div-int/lit8 v5, v8, 0x2

    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    sub-int/2addr v8, v0

    div-int/lit8 v7, v8, 0x2

    iget v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    add-int/2addr v8, v0

    div-int/lit8 v6, v8, 0x2

    iget-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v8, v4, v7, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_3

    :cond_b
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/google/android/apps/plus/views/AvatarView;->mResizeRectSrc:Landroid/graphics/Rect;

    goto :goto_3
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->invalidate()V

    return-void
.end method

.method public setAvatarSize(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    iget v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarSize:I

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSizeInPixels:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDimmed(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mDimmed:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->invalidate()V

    return-void
.end method

.method public setGaiaId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarUrl:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mByGaiaId:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->bindResources()V

    :cond_0
    return-void
.end method

.method public setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarUrl:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mGaiaId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarUrl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mByGaiaId:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarView;->bindResources()V

    :cond_1
    return-void
.end method

.method public setRounded(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/AvatarView;->mRound:Z

    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
