.class public Lcom/google/android/apps/plus/views/SquareListItemView;
.super Landroid/widget/FrameLayout;
.source "SquareListItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;
    }
.end annotation


# instance fields
.field private mMemberCountView:Landroid/widget/TextView;

.field private mNameView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

.field protected mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

.field private mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

.field protected mSquareId:Ljava/lang/String;

.field private mUnreadCountView:Landroid/widget/TextView;

.field private mVisibilityView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public init(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;ZZ)V
    .locals 7
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;
    .param p3    # Z
    .param p4    # Z

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    iput-object p2, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mSquareId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mNameView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v4, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    :goto_0
    if-eqz p3, :cond_1

    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SquareListItemView;->setSquareVisibility(I)V

    if-eqz p4, :cond_2

    const/4 v1, 0x7

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_2
    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mUnreadCountView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mMemberCountView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_4
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/SquareListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissing(Z)V

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mUnreadCountView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v3, 0x63

    if-le v1, v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mUnreadCountView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mMemberCountView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$plurals;->square_members_count:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mMemberCountView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method public initChildViews()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mNameView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->square_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mNameView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->square_photo:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_community_avatar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceLoadingDrawable(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_community_avatar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissingDrawable(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->square_visibility:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mVisibilityView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->member_count:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mMemberCountView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->unread_count:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mUnreadCountView:Landroid/widget/TextView;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mSquareId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/SquareListItemView$OnItemClickListener;->onClick(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SquareListItemView;->initChildViews()V

    return-void
.end method

.method public onRecycle()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mPhotoView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setSquareVisibility(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mVisibilityView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mVisibilityView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mVisibilityView:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_1
    return-void

    :pswitch_0
    const/4 v2, 0x1

    sget v1, Lcom/google/android/apps/plus/R$string;->square_public:I

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_public_small:I

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x1

    sget v1, Lcom/google/android/apps/plus/R$string;->square_private:I

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_private_small:I

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareListItemView;->mVisibilityView:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
