.class public Lcom/google/android/apps/plus/views/PhotoTagSpanner;
.super Landroid/view/View;
.source "PhotoTagSpanner.java"


# instance fields
.field private mTagParent:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoTagSpanner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->setFixedWidth()V

    return-void
.end method

.method public setFixedWidth()V
    .locals 7

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->mTagParent:Landroid/view/View;

    if-nez v3, :cond_3

    sget v5, Lcom/google/android/apps/plus/R$id;->tag_approval:I

    move-object v3, p0

    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v6, v3, Landroid/view/View;

    if-eqz v6, :cond_1

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v6

    if-ne v6, v5, :cond_2

    :goto_0
    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->mTagParent:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->mTagParent:Landroid/view/View;

    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v5, Lcom/google/android/apps/plus/views/PhotoTagSpanner;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " must have a parent with and ID of \'tag_approval\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    const/4 v3, 0x0

    :cond_2
    if-nez v3, :cond_0

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->mTagParent:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->mTagParent:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->tag_buttons:I

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->mTagParent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    :goto_1
    sub-int/2addr v2, v3

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    :goto_2
    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoTagSpanner;->setMeasuredDimension(II)V

    return-void

    :cond_4
    move v3, v4

    goto :goto_1

    :cond_5
    move v3, v4

    goto :goto_2
.end method
