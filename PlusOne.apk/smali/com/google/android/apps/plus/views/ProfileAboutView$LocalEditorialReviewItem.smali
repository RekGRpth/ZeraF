.class final Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;
.super Lcom/google/android/apps/plus/views/ProfileAboutView$Item;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocalEditorialReviewItem"
.end annotation


# instance fields
.field final editorialText:Ljava/lang/String;

.field final priceLabel:Ljava/lang/String;

.field final priceValue:Ljava/lang/String;

.field final reviewCount:I

.field final scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;


# direct methods
.method public constructor <init>(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView$Item;-><init>(B)V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->editorialText:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    return-void
.end method
