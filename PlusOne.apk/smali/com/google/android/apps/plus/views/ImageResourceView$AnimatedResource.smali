.class final Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;
.super Ljava/lang/Object;
.source "ImageResourceView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ImageResourceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimatedResource"
.end annotation


# instance fields
.field private mResourceWithAnimation:Lcom/google/android/apps/plus/service/MediaResource;

.field final synthetic this$0:Lcom/google/android/apps/plus/views/ImageResourceView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/ImageResourceView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/ImageResourceView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;-><init>(Lcom/google/android/apps/plus/views/ImageResourceView;)V

    return-void
.end method


# virtual methods
.method public final bindResources()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    # getter for: Lcom/google/android/apps/plus/views/ImageResourceView;->mImageResourceFlags:I
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$000(Lcom/google/android/apps/plus/views/ImageResourceView;)I

    move-result v1

    # invokes: Lcom/google/android/apps/plus/views/ImageResourceView;->getMediaResource(ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;
    invoke-static {v0, v1, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$100(Lcom/google/android/apps/plus/views/ImageResourceView;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->mResourceWithAnimation:Lcom/google/android/apps/plus/service/MediaResource;

    return-void
.end method

.method public final onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v1

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    # invokes: Lcom/google/android/apps/plus/views/ImageResourceView;->clearDrawable()V
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$200(Lcom/google/android/apps/plus/views/ImageResourceView;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/plus/util/GifImage;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v2, Lcom/google/android/apps/plus/util/GifDrawable;

    check-cast v0, Lcom/google/android/apps/plus/util/GifImage;

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/util/GifDrawable;-><init>(Lcom/google/android/apps/plus/util/GifImage;)V

    # setter for: Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$302(Lcom/google/android/apps/plus/views/ImageResourceView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    # getter for: Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$300(Lcom/google/android/apps/plus/views/ImageResourceView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/GifDrawable;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/util/GifDrawable;->setAnimationEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    # getter for: Lcom/google/android/apps/plus/views/ImageResourceView;->mDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$300(Lcom/google/android/apps/plus/views/ImageResourceView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->invalidate()V

    return-void
.end method

.method public final unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->mResourceWithAnimation:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->mResourceWithAnimation:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/MediaResource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->mResourceWithAnimation:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageResourceView$AnimatedResource;->this$0:Lcom/google/android/apps/plus/views/ImageResourceView;

    # invokes: Lcom/google/android/apps/plus/views/ImageResourceView;->clearDrawable()V
    invoke-static {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->access$200(Lcom/google/android/apps/plus/views/ImageResourceView;)V

    return-void
.end method
