.class public Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
.super Landroid/widget/LinearLayout;
.source "LinearLayoutWithLayoutNotifications.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;
    }
.end annotation


# instance fields
.field private mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

.field private mMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    return-void
.end method


# virtual methods
.method public onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    iget v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;->onMeasured(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    :cond_0
    return-void
.end method

.method public setLayoutListener(Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mLayoutListener:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;

    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->mMaxWidth:I

    return-void
.end method
