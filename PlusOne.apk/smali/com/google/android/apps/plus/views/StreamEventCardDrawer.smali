.class public final Lcom/google/android/apps/plus/views/StreamEventCardDrawer;
.super Ljava/lang/Object;
.source "StreamEventCardDrawer.java"


# static fields
.field private static sEventCardPadding:I

.field private static sEventInfoBackgroundPaint:Landroid/graphics/Paint;

.field private static sEventInfoTextPaint:Landroid/text/TextPaint;

.field private static sEventNameTextPaint:Landroid/text/TextPaint;

.field private static sEventTextLineSpacing:I

.field private static sHangoutBitmap:Landroid/graphics/Bitmap;

.field private static sHangoutTitle:Ljava/lang/String;

.field private static sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sInitialized:Z

.field private static sLocationBitmap:Landroid/graphics/Bitmap;

.field private static sMediaHeight:I

.field private static sMediaWidth:I

.field private static sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sOnAirPaint:Landroid/text/TextPaint;

.field private static sOnAirTitle:Ljava/lang/String;

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sTimeBitmap:Landroid/graphics/Bitmap;


# instance fields
.field private mBackgroundRect:Landroid/graphics/Rect;

.field private mBound:Z

.field private mDateLayout:Landroid/text/StaticLayout;

.field private mDateLayoutCorner:Landroid/graphics/Point;

.field private mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mLocationIcon:Landroid/graphics/Bitmap;

.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayout:Landroid/text/StaticLayout;

.field private mLocationLayoutCorner:Landroid/graphics/Point;

.field private mNameLayout:Landroid/text/StaticLayout;

.field private mNameLayoutCorner:Landroid/graphics/Point;

.field private mThemeImageRect:Landroid/graphics/Rect;

.field private mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

.field private mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

.field private mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mTimeIconRect:Landroid/graphics/Rect;

.field private mTimeZoneCorner:Landroid/graphics/Point;

.field private mTimeZoneLayout:Landroid/text/StaticLayout;

.field private mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->initialize(Landroid/content/Context;)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventCardPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_text_line_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventTextLineSpacing:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-static {p1, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    invoke-static {p1, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    new-instance v1, Landroid/text/TextPaint;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-direct {v1, v2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_detail_on_air:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_on_air_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_on_air_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_location_grey_12:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sLocationBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_hangouts_grey_12:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sHangoutBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_time_grey_12:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sTimeBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_hangout_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sHangoutTitle:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_on_air:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirTitle:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_events_on_air:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaWidth(Landroid/content/Context;)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sMediaWidth:I

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sMediaHeight:I

    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sInitialized:Z

    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeIconRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBackgroundRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    return-void
.end method

.method private static drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V
    .locals 2
    .param p0    # Landroid/text/StaticLayout;
    .param p1    # Landroid/graphics/Point;
    .param p2    # Landroid/graphics/Canvas;

    iget v0, p1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p0, p2}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget v0, p1, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method private static layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .locals 11
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/graphics/Point;
    .param p5    # Landroid/text/TextPaint;
    .param p6    # Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v10, 0x1

    move v0, p0

    move v1, p1

    move v2, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v9, p5

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bind(Lcom/google/android/apps/plus/service/ResourceConsumer;Lcom/google/api/services/plusi/model/PlusEvent;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->unbindResources(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->clear()V

    iput-object p2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBound:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->bindResources(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bindResources(Lcom/google/android/apps/plus/service/ResourceConsumer;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    sget v2, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sMediaWidth:I

    sget v3, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sMediaHeight:I

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method public final clear()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBound:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBound:Z

    goto :goto_0
.end method

.method public final draw$680d9d43(ILandroid/graphics/Canvas;)I
    .locals 10
    .param p1    # I
    .param p2    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBound:Z

    if-nez v6, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBackgroundRect:Landroid/graphics/Rect;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v6, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    sget-object v7, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v9, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    invoke-static {v6, v7, p2}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    add-int v1, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    invoke-static {v6, v7, p2}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    sget-object v6, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sTimeBitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeIconRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v6, v9, v7, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move v2, v1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    add-int v4, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    invoke-static {v6, v7, p2}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    move v2, v4

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v6

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v6, p2}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    move v2, v5

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    add-int/2addr v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    invoke-static {v6, v7, p2}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v6, v9, v7, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move v2, v3

    :cond_4
    move p1, v2

    goto/16 :goto_0
.end method

.method public final layout(Landroid/content/Context;III)I
    .locals 21
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget v3, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventCardPadding:I

    mul-int/lit8 v16, v3, 0x2

    sget v19, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventTextLineSpacing:I

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    add-int v6, p2, p4

    add-int v7, p3, v20

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    add-int v18, p3, v20

    add-int v4, p2, p4

    sub-int/2addr v4, v3

    sub-int v5, v4, v16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBackgroundRect:Landroid/graphics/Rect;

    add-int v6, p2, p4

    move/from16 v0, p2

    move/from16 v1, v18

    move/from16 v2, v18

    invoke-virtual {v4, v0, v1, v6, v2}, Landroid/graphics/Rect;->set(IIII)V

    add-int v4, v18, v3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    sget-object v8, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    const/4 v9, 0x1

    invoke-static/range {v3 .. v9}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    add-int v4, v4, v19

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    if-eqz v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v11

    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const/4 v12, 0x1

    invoke-static/range {v6 .. v12}, Lcom/google/android/apps/plus/util/EventDateUtils;->shouldShowDay(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v7, v8, v9, v6}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v10

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sTimeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeIconRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    sget-object v12, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v13, 0x1

    move v9, v3

    invoke-static/range {v3 .. v13}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsEventData;->isEventHangout(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v13

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v7, v6, v13}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getDisplayString(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_2

    add-int v7, v4, v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    iget v6, v4, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v3

    sub-int v8, v5, v4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneCorner:Landroid/graphics/Point;

    sget-object v11, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v12, 0x1

    invoke-static/range {v6 .. v12}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTimeZoneLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v4, v7

    :cond_2
    if-eqz v13, :cond_3

    add-int v15, v4, v19

    new-instance v6, Lcom/google/android/apps/plus/views/ClickableButton;

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirTitle:Ljava/lang/String;

    sget-object v10, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget-object v11, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v12, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v13, 0x0

    move-object/from16 v7, p1

    move v14, v3

    invoke-direct/range {v6 .. v15}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v15

    :cond_3
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v6, :cond_7

    iget-object v7, v6, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-eqz v7, :cond_6

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    :goto_0
    sget-object v7, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sLocationBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    move-object v10, v6

    :cond_4
    :goto_1
    if-eqz v10, :cond_5

    add-int v4, v4, v19

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    sget-object v12, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v13, 0x1

    move v9, v3

    invoke-static/range {v3 .. v13}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v6

    add-int/2addr v4, v6

    :cond_5
    add-int/2addr v4, v3

    sub-int v17, v4, v18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBackgroundRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mBackgroundRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int v6, v6, v17

    iput v6, v4, Landroid/graphics/Rect;->bottom:I

    add-int v4, v18, v17

    sub-int v4, v4, p3

    return v4

    :cond_6
    iget-object v6, v6, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto :goto_0

    :cond_7
    if-eqz v7, :cond_4

    sget-object v10, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sHangoutTitle:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->sHangoutBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method public final unbindResources(Lcom/google/android/apps/plus/service/ResourceConsumer;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->mThemeImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
