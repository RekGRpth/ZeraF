.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;
.super Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
.source "StreamOneUpActivityView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OneUpActivityTouchExplorer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/TouchExplorationHelper",
        "<",
        "Lcom/google/android/apps/plus/views/ClickableItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mIsItemCacheStale:Z

.field private mItemCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;Landroid/content/Context;)V
    .locals 2
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0, p2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    new-instance v0, Ljava/util/ArrayList;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;
    invoke-static {p1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1600(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    return-void
.end method

.method private refreshItemCache()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mClickableItems:Ljava/util/Set;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$1600(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected final bridge synthetic getIdForItem(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->refreshItemCache()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected final bridge synthetic getItemAt(FF)Ljava/lang/Object;
    .locals 6
    .param p1    # F
    .param p2    # F

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->refreshItemCache()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    float-to-int v4, p1

    float-to-int v5, p2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final bridge synthetic getItemForId(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->refreshDrawableState()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ClickableItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final getVisibleItems(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->refreshItemCache()V

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mItemCache:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final invalidateItemCache()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$OneUpActivityTouchExplorer;->mIsItemCacheStale:Z

    return-void
.end method

.method protected final bridge synthetic performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    const/4 v0, 0x1

    const/4 v1, 0x0

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    const/16 v2, 0x10

    if-ne p2, v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    invoke-interface {p1, v2, v3, v1}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    invoke-interface {p1, v1, v2, v0}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected final bridge synthetic populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final bridge synthetic populateNodeForItem(Ljava/lang/Object;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p2, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
