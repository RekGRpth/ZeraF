.class public final Lcom/google/android/apps/plus/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_switch_padding:I = 0x7f0e01b6

.field public static final album_bottom_action_bar_divider_height:I = 0x7f0e0048

.field public static final album_bottom_action_bar_divider_width:I = 0x7f0e0049

.field public static final album_bottom_action_bar_height:I = 0x7f0e0047

.field public static final album_comment_count_text_size:I = 0x7f0e003d

.field public static final album_grid_horizontal_spacing:I = 0x7f0e0039

.field public static final album_grid_vertical_spacing:I = 0x7f0e0038

.field public static final album_grid_width:I = 0x7f0e0037

.field public static final album_info_height:I = 0x7f0e003f

.field public static final album_info_inner_padding:I = 0x7f0e0042

.field public static final album_info_left_margin:I = 0x7f0e0041

.field public static final album_info_right_margin:I = 0x7f0e0040

.field public static final album_instructions_height:I = 0x7f0e0045

.field public static final album_instructions_left_padding:I = 0x7f0e0046

.field public static final album_notification_right_margin:I = 0x7f0e0043

.field public static final album_notification_top_margin:I = 0x7f0e0044

.field public static final album_photo_grid_spacing:I = 0x7f0e003a

.field public static final album_photo_height:I = 0x7f0e003c

.field public static final album_photo_width:I = 0x7f0e003b

.field public static final album_plusone_count_text_size:I = 0x7f0e003e

.field public static final android_stream_item_thumbnail_size:I = 0x7f0e00a5

.field public static final audience_autocomplete_dropdown_width:I = 0x7f0e00a6

.field public static final avatar_dimension:I = 0x7f0e0026

.field public static final bar_graph_bar_height:I = 0x7f0e00e8

.field public static final bar_graph_bar_spacing:I = 0x7f0e00e9

.field public static final bar_graph_label_text_bar_spacing:I = 0x7f0e00e7

.field public static final bar_graph_label_text_size:I = 0x7f0e00e4

.field public static final bar_graph_total_graph_spacing:I = 0x7f0e00e6

.field public static final bar_graph_total_text_size:I = 0x7f0e00e3

.field public static final bar_graph_value_text_size:I = 0x7f0e00e5

.field public static final card_avatar_size:I = 0x7f0e00ff

.field public static final card_border_bottom_padding:I = 0x7f0e00f6

.field public static final card_border_left_padding:I = 0x7f0e00f3

.field public static final card_border_right_padding:I = 0x7f0e00f4

.field public static final card_border_top_padding:I = 0x7f0e00f5

.field public static final card_content_y_padding:I = 0x7f0e0100

.field public static final card_default_text_size:I = 0x7f0e00f9

.field public static final card_gray_spam_min_height:I = 0x7f0e0109

.field public static final card_gray_spam_x_padding:I = 0x7f0e0107

.field public static final card_gray_spam_y_padding:I = 0x7f0e0108

.field public static final card_interactive_post_button_text_size:I = 0x7f0e00fb

.field public static final card_links_icon_x_padding:I = 0x7f0e010f

.field public static final card_links_image_dimension:I = 0x7f0e010a

.field public static final card_links_image_max_width_percent:I = 0x7f0e010d

.field public static final card_links_image_stroke_dimension:I = 0x7f0e010b

.field public static final card_links_image_x_padding:I = 0x7f0e010e

.field public static final card_links_title_text_shadow_radius:I = 0x7f0e0111

.field public static final card_links_title_text_shadow_x:I = 0x7f0e0112

.field public static final card_links_title_text_shadow_y:I = 0x7f0e0113

.field public static final card_links_title_text_size:I = 0x7f0e0110

.field public static final card_links_url_text_shadow_radius:I = 0x7f0e0115

.field public static final card_links_url_text_shadow_x:I = 0x7f0e0116

.field public static final card_links_url_text_shadow_y:I = 0x7f0e0117

.field public static final card_links_url_text_size:I = 0x7f0e0114

.field public static final card_links_x_padding:I = 0x7f0e010c

.field public static final card_margin_percentage:I = 0x7f0e0169

.field public static final card_place_review_location_icon_padding:I = 0x7f0e011d

.field public static final card_plus_bar_x_padding:I = 0x7f0e0106

.field public static final card_plus_oned_text_shadow_radius:I = 0x7f0e00fc

.field public static final card_plus_oned_text_shadow_x:I = 0x7f0e00fd

.field public static final card_plus_oned_text_shadow_y:I = 0x7f0e00fe

.field public static final card_plus_oned_text_size:I = 0x7f0e00fa

.field public static final card_square_invite_invitation_text_size:I = 0x7f0e0118

.field public static final card_square_invite_name_text_size:I = 0x7f0e0119

.field public static final card_square_invite_shadow_radius:I = 0x7f0e011a

.field public static final card_square_invite_shadow_x:I = 0x7f0e011b

.field public static final card_square_invite_shadow_y:I = 0x7f0e011c

.field public static final card_tag_icon_x_padding:I = 0x7f0e0103

.field public static final card_tag_icon_y_padding_checkin:I = 0x7f0e0104

.field public static final card_tag_icon_y_padding_location:I = 0x7f0e0105

.field public static final card_tag_text_x_padding:I = 0x7f0e0102

.field public static final card_tag_y_offset:I = 0x7f0e0101

.field public static final card_x_padding:I = 0x7f0e00f7

.field public static final card_y_padding:I = 0x7f0e00f8

.field public static final circle_intra_avatar_padding:I = 0x7f0e019f

.field public static final circle_name_area_height:I = 0x7f0e01a0

.field public static final circle_name_size:I = 0x7f0e01a1

.field public static final circle_name_tweak:I = 0x7f0e01a2

.field public static final circle_outside_padding:I = 0x7f0e019e

.field public static final clickable_button_bitmap_text_x_spacing:I = 0x7f0e001c

.field public static final clickable_button_extra_horizontal_spacing:I = 0x7f0e001b

.field public static final clickable_button_horizontal_spacing:I = 0x7f0e001a

.field public static final compose_bar_recent_images_default_padding:I = 0x7f0e0172

.field public static final compose_bar_recent_images_dimension:I = 0x7f0e0170

.field public static final compose_bar_recent_images_padding:I = 0x7f0e0171

.field public static final compose_bar_text:I = 0x7f0e0173

.field public static final compose_bar_text_drawable_padding:I = 0x7f0e0175

.field public static final compose_bar_text_y_padding:I = 0x7f0e0174

.field public static final dropdownListPreferredItemHeight:I = 0x7f0e016e

.field public static final emotishare_item_margin:I = 0x7f0e00e1

.field public static final emotishare_preview_height:I = 0x7f0e00d1

.field public static final event_card_Details_rsvp_action_button_internal_spacing:I = 0x7f0e0095

.field public static final event_card_activity_avatar_lineup_margin_bottom:I = 0x7f0e008f

.field public static final event_card_activity_avatar_lineup_margin_left:I = 0x7f0e008d

.field public static final event_card_activity_avatar_lineup_margin_right:I = 0x7f0e008e

.field public static final event_card_activity_avatar_magin_top:I = 0x7f0e009b

.field public static final event_card_activity_avatar_margin_left:I = 0x7f0e009a

.field public static final event_card_activity_avatar_size:I = 0x7f0e0099

.field public static final event_card_activity_description_size:I = 0x7f0e0098

.field public static final event_card_activity_padding_bottom:I = 0x7f0e00a0

.field public static final event_card_activity_padding_left:I = 0x7f0e009e

.field public static final event_card_activity_padding_right:I = 0x7f0e009d

.field public static final event_card_activity_padding_top:I = 0x7f0e009f

.field public static final event_card_activity_photo_avatar_margin_bottom:I = 0x7f0e00a4

.field public static final event_card_activity_photo_margin_bottom:I = 0x7f0e00a3

.field public static final event_card_activity_text_margin_left:I = 0x7f0e009c

.field public static final event_card_activity_text_margin_right:I = 0x7f0e00a1

.field public static final event_card_activity_text_top_avatar_percentage:I = 0x7f0e00a2

.field public static final event_card_activity_time_size:I = 0x7f0e0097

.field public static final event_card_activity_title_size:I = 0x7f0e0096

.field public static final event_card_avatar_lineup_item_padding:I = 0x7f0e0090

.field public static final event_card_avatar_lineup_item_size:I = 0x7f0e0091

.field public static final event_card_avatar_lineup_overflow_text_size:I = 0x7f0e0092

.field public static final event_card_details_avatar_percent_overlap:I = 0x7f0e007c

.field public static final event_card_details_avatar_size:I = 0x7f0e007d

.field public static final event_card_details_description_size:I = 0x7f0e0081

.field public static final event_card_details_going_label_text_size:I = 0x7f0e0093

.field public static final event_card_details_on_air_size:I = 0x7f0e0085

.field public static final event_card_details_option_description_size:I = 0x7f0e0089

.field public static final event_card_details_option_min_height:I = 0x7f0e008b

.field public static final event_card_details_option_min_side_width:I = 0x7f0e008a

.field public static final event_card_details_option_title_size:I = 0x7f0e0088

.field public static final event_card_details_padding:I = 0x7f0e007e

.field public static final event_card_details_rsvp_action_button_text_size:I = 0x7f0e0094

.field public static final event_card_details_rsvp_count_size:I = 0x7f0e008c

.field public static final event_card_details_rsvp_height:I = 0x7f0e007b

.field public static final event_card_details_secondary_padding:I = 0x7f0e0083

.field public static final event_card_details_secondary_scroll_padding:I = 0x7f0e0082

.field public static final event_card_details_see_invitees_height:I = 0x7f0e0087

.field public static final event_card_details_see_invitees_size:I = 0x7f0e0086

.field public static final event_card_details_subtitle_size:I = 0x7f0e0080

.field public static final event_card_details_title_size:I = 0x7f0e007f

.field public static final event_card_devails_percent_divider:I = 0x7f0e0084

.field public static final event_card_divider_stroke_width:I = 0x7f0e0075

.field public static final event_card_info_text_size:I = 0x7f0e0078

.field public static final event_card_name_text_size:I = 0x7f0e0079

.field public static final event_card_padding:I = 0x7f0e0073

.field public static final event_card_ribbon_percent_height_overlap:I = 0x7f0e0076

.field public static final event_card_status_text_size:I = 0x7f0e0077

.field public static final event_card_text_line_spacing:I = 0x7f0e0074

.field public static final event_create_event_text_size:I = 0x7f0e007a

.field public static final flat_button_internal_separation:I = 0x7f0e00e2

.field public static final hangout_avatar_margin:I = 0x7f0e00b0

.field public static final hangout_dropshadow_height:I = 0x7f0e00ca

.field public static final hangout_filmstrip_border_bottom:I = 0x7f0e00c2

.field public static final hangout_filmstrip_border_left:I = 0x7f0e00c3

.field public static final hangout_filmstrip_border_right:I = 0x7f0e00c4

.field public static final hangout_filmstrip_border_top:I = 0x7f0e00c1

.field public static final hangout_filmstrip_gradient_width:I = 0x7f0e00c5

.field public static final hangout_filmstrip_margin:I = 0x7f0e00c0

.field public static final hangout_filmstrip_remote_video_border_bottom:I = 0x7f0e00c8

.field public static final hangout_filmstrip_remote_video_border_left:I = 0x7f0e00c7

.field public static final hangout_filmstrip_remote_video_border_right:I = 0x7f0e00c9

.field public static final hangout_filmstrip_remote_video_border_top:I = 0x7f0e00c6

.field public static final hangout_inset_border_plus_dropshadow_size:I = 0x7f0e00cc

.field public static final hangout_inset_border_size:I = 0x7f0e00cb

.field public static final hangout_invitees_logo_size:I = 0x7f0e00b7

.field public static final hangout_invitees_message_height:I = 0x7f0e00b6

.field public static final hangout_invitees_view_height:I = 0x7f0e00b5

.field public static final hangout_list_header_height:I = 0x7f0e00bf

.field public static final hangout_message_logo_size:I = 0x7f0e00b8

.field public static final hangout_overlay_menu_height:I = 0x7f0e00b4

.field public static final hangout_ring_toggle_height:I = 0x7f0e00be

.field public static final hangout_ringing_inviter_avatar_size:I = 0x7f0e00b2

.field public static final hangout_ringing_notification_height:I = 0x7f0e00b3

.field public static final hangout_ringing_title_height:I = 0x7f0e00b1

.field public static final hangout_self_video_container_height:I = 0x7f0e00ba

.field public static final hangout_self_video_container_margin:I = 0x7f0e00bb

.field public static final hangout_self_video_container_width:I = 0x7f0e00b9

.field public static final hangout_toast_avatar_size:I = 0x7f0e00bd

.field public static final host_action_bar_height:I = 0x7f0e00f2

.field public static final host_max_navigation_bar_width:I = 0x7f0e00ee

.field public static final host_min_navigation_bar_width:I = 0x7f0e00ed

.field public static final host_min_notification_bar_margin_left:I = 0x7f0e00f0

.field public static final host_min_notification_bar_width:I = 0x7f0e00ef

.field public static final host_shadow_width:I = 0x7f0e00ec

.field public static final host_spinner_margin_left:I = 0x7f0e00f1

.field public static final label_preference_margin_left:I = 0x7f0e00ea

.field public static final large_avatar_dimension:I = 0x7f0e002b

.field public static final location_picker_map_size:I = 0x7f0e00e0

.field public static final max_list_dialog_height:I = 0x7f0e016c

.field public static final max_post_dialog_height:I = 0x7f0e016d

.field public static final media_max_portrait_aspect_ratio:I = 0x7f0e016b

.field public static final media_min_landscape_aspect_ratio:I = 0x7f0e016a

.field public static final media_not_found_message_text_size:I = 0x7f0e0058

.field public static final medium_avatar_dimension:I = 0x7f0e0027

.field public static final medium_avatar_name_height:I = 0x7f0e0028

.field public static final medium_avatar_selected_dimension:I = 0x7f0e002a

.field public static final medium_avatar_selected_padding:I = 0x7f0e0029

.field public static final micro_kind_max_dimension:I = 0x7f0e0013

.field public static final mini_kind_max_dimension:I = 0x7f0e0014

.field public static final notification_bar_text:I = 0x7f0e0176

.field public static final notification_bigpicture_height:I = 0x7f0e0023

.field public static final notification_bigpicture_width:I = 0x7f0e0022

.field public static final notification_large_icon_size:I = 0x7f0e0024

.field public static final one_up_photo_min_exposure_land:I = 0x7f0e0122

.field public static final one_up_photo_min_exposure_port:I = 0x7f0e0121

.field public static final oob_action_bar_padding:I = 0x7f0e000a

.field public static final oob_action_bar_padding_bottom:I = 0x7f0e000b

.field public static final oob_button_padding_x:I = 0x7f0e0009

.field public static final oob_button_padding_y:I = 0x7f0e0008

.field public static final oob_button_text_size:I = 0x7f0e000c

.field public static final oob_items_padding:I = 0x7f0e0007

.field public static final oob_legal_text_size:I = 0x7f0e0010

.field public static final oob_page_max_width:I = 0x7f0e0006

.field public static final oob_page_padding_bottom:I = 0x7f0e0005

.field public static final oob_page_padding_top:I = 0x7f0e0004

.field public static final oob_page_padding_x:I = 0x7f0e0003

.field public static final oob_radio_button_text_size:I = 0x7f0e000d

.field public static final oob_small_button_height:I = 0x7f0e000f

.field public static final oob_small_button_text_size:I = 0x7f0e000e

.field public static final oob_small_padding:I = 0x7f0e0012

.field public static final oob_text_drawable_padding:I = 0x7f0e0011

.field public static final participant_tray_avatar_and_padding_dimension:I = 0x7f0e00a8

.field public static final participant_tray_avatar_height:I = 0x7f0e00a9

.field public static final participant_tray_avatar_padding_dimension:I = 0x7f0e00a7

.field public static final people_add_circle_height:I = 0x7f0e01ab

.field public static final people_add_circle_text_size:I = 0x7f0e01a9

.field public static final people_add_circle_top_padding:I = 0x7f0e01aa

.field public static final people_list_row_detail_size:I = 0x7f0e01a5

.field public static final people_list_row_name_padding:I = 0x7f0e01a6

.field public static final people_list_row_name_size:I = 0x7f0e01a4

.field public static final people_row_spacing:I = 0x7f0e01a3

.field public static final people_suggestions_max_card_width:I = 0x7f0e01ad

.field public static final people_your_circles_max_card_width:I = 0x7f0e01ac

.field public static final people_your_circles_text_size:I = 0x7f0e01a7

.field public static final people_your_circles_top_padding:I = 0x7f0e01a8

.field public static final photo_crop_cover_width:I = 0x7f0e004b

.field public static final photo_crop_profile_width:I = 0x7f0e004a

.field public static final photo_crop_stroke_width:I = 0x7f0e004c

.field public static final photo_home_album_count_left_padding:I = 0x7f0e0035

.field public static final photo_home_album_count_right_padding:I = 0x7f0e0036

.field public static final photo_home_album_count_text_size:I = 0x7f0e0031

.field public static final photo_home_album_name_left_padding:I = 0x7f0e0033

.field public static final photo_home_album_name_right_padding:I = 0x7f0e0034

.field public static final photo_home_album_name_text_size:I = 0x7f0e0030

.field public static final photo_home_info_height:I = 0x7f0e0032

.field public static final photo_home_item_divider_height:I = 0x7f0e002f

.field public static final photo_home_item_height:I = 0x7f0e002d

.field public static final photo_home_item_inner_padding:I = 0x7f0e002e

.field public static final photo_info_comment_count_left_margin:I = 0x7f0e004f

.field public static final photo_info_comment_count_text_width:I = 0x7f0e0050

.field public static final photo_info_comment_text_size:I = 0x7f0e0054

.field public static final photo_info_plusone_bottom_margin:I = 0x7f0e0051

.field public static final photo_info_plusone_count_left_margin:I = 0x7f0e0052

.field public static final photo_info_plusone_count_text_width:I = 0x7f0e0053

.field public static final photo_info_plusone_text_size:I = 0x7f0e0055

.field public static final photo_one_up_avatar_size:I = 0x7f0e0166

.field public static final photo_one_up_caption_margin_top:I = 0x7f0e0165

.field public static final photo_overlay_bottom_padding:I = 0x7f0e004e

.field public static final photo_overlay_right_padding:I = 0x7f0e004d

.field public static final photo_processing_message_subtitle_size:I = 0x7f0e0057

.field public static final photo_processing_message_subtitle_vertical_position:I = 0x7f0e005a

.field public static final photo_processing_message_title_size:I = 0x7f0e0056

.field public static final photo_processing_message_title_vertical_position:I = 0x7f0e0059

.field public static final photo_tag_scroller_avatar_height:I = 0x7f0e0062

.field public static final photo_tag_scroller_avatar_margin_left:I = 0x7f0e0068

.field public static final photo_tag_scroller_avatar_margin_right:I = 0x7f0e0069

.field public static final photo_tag_scroller_avatar_padding_bottom:I = 0x7f0e006d

.field public static final photo_tag_scroller_avatar_padding_left:I = 0x7f0e006c

.field public static final photo_tag_scroller_avatar_padding_right:I = 0x7f0e006b

.field public static final photo_tag_scroller_avatar_padding_top:I = 0x7f0e006a

.field public static final photo_tag_scroller_avatar_width:I = 0x7f0e0063

.field public static final photo_tag_scroller_button_height:I = 0x7f0e0072

.field public static final photo_tag_scroller_button_margin:I = 0x7f0e0070

.field public static final photo_tag_scroller_button_width:I = 0x7f0e0071

.field public static final photo_tag_scroller_height:I = 0x7f0e0061

.field public static final photo_tag_scroller_name_text_size:I = 0x7f0e005c

.field public static final photo_tag_scroller_padding_bottom:I = 0x7f0e0066

.field public static final photo_tag_scroller_padding_left:I = 0x7f0e0067

.field public static final photo_tag_scroller_padding_right:I = 0x7f0e0065

.field public static final photo_tag_scroller_padding_top:I = 0x7f0e0064

.field public static final photo_tag_scroller_secondary_text_size:I = 0x7f0e005d

.field public static final photo_tag_scroller_text_padding_left:I = 0x7f0e006f

.field public static final photo_tag_scroller_text_padding_right:I = 0x7f0e006e

.field public static final photo_tag_shadow_radius:I = 0x7f0e005f

.field public static final photo_tag_stroke_width:I = 0x7f0e0060

.field public static final photo_tag_text_padding:I = 0x7f0e005e

.field public static final photo_tag_text_size:I = 0x7f0e005b

.field public static final places_map_max_width:I = 0x7f0e002c

.field public static final plus_mention_suggestion_min_space:I = 0x7f0e0168

.field public static final plus_mention_suggestion_popup_offset:I = 0x7f0e0167

.field public static final profile_card_bottom_padding:I = 0x7f0e017a

.field public static final profile_cover_photo_full_bleed_height:I = 0x7f0e017c

.field public static final profile_cover_photo_negative_space:I = 0x7f0e017b

.field public static final quick_actions_item_height:I = 0x7f0e00af

.field public static final realtimechat_image_height:I = 0x7f0e00ae

.field public static final riviera_album_image_padding:I = 0x7f0e019d

.field public static final riviera_avatar_margin:I = 0x7f0e0197

.field public static final riviera_avatar_size:I = 0x7f0e0196

.field public static final riviera_button_bitmap_text_spacing:I = 0x7f0e01b5

.field public static final riviera_card_border_bottom_padding:I = 0x7f0e01b1

.field public static final riviera_card_border_left_padding:I = 0x7f0e01ae

.field public static final riviera_card_border_right_padding:I = 0x7f0e01af

.field public static final riviera_card_border_top_padding:I = 0x7f0e01b0

.field public static final riviera_card_link_default_height:I = 0x7f0e01b2

.field public static final riviera_commenter_avatar_spacing:I = 0x7f0e01b4

.field public static final riviera_commenter_indicator_height:I = 0x7f0e01b3

.field public static final riviera_content_x_padding:I = 0x7f0e0188

.field public static final riviera_content_y_padding:I = 0x7f0e0189

.field public static final riviera_corner_icon_padding:I = 0x7f0e019c

.field public static final riviera_default_padding:I = 0x7f0e0198

.field public static final riviera_desired_card_width:I = 0x7f0e0186

.field public static final riviera_desired_separator_width:I = 0x7f0e0187

.field public static final riviera_icon_text_padding:I = 0x7f0e019b

.field public static final riviera_metadata_padding:I = 0x7f0e0199

.field public static final riviera_min_overlay_media_height:I = 0x7f0e018a

.field public static final riviera_padding_percent_tablet:I = 0x7f0e0185

.field public static final riviera_padding_phone:I = 0x7f0e0184

.field public static final riviera_separator_width:I = 0x7f0e019a

.field public static final riviera_text_size_body:I = 0x7f0e018b

.field public static final riviera_text_size_body_metadata:I = 0x7f0e0193

.field public static final riviera_text_size_btn:I = 0x7f0e0194

.field public static final riviera_text_size_h2:I = 0x7f0e018c

.field public static final riviera_text_size_h3:I = 0x7f0e018d

.field public static final riviera_text_size_h5:I = 0x7f0e018e

.field public static final riviera_text_size_media_h1:I = 0x7f0e018f

.field public static final riviera_text_size_media_metadata:I = 0x7f0e0192

.field public static final riviera_text_size_media_setup_h1:I = 0x7f0e0195

.field public static final riviera_text_size_media_text:I = 0x7f0e0191

.field public static final riviera_text_size_media_title:I = 0x7f0e0190

.field public static final sdk_activity_padding:I = 0x7f0e00ce

.field public static final sdk_activity_size:I = 0x7f0e00cd

.field public static final share_acl_margin:I = 0x7f0e00d8

.field public static final share_audience_margin:I = 0x7f0e00d7

.field public static final share_footer_margin:I = 0x7f0e00db

.field public static final share_footer_text_size:I = 0x7f0e00da

.field public static final share_gallery_margin:I = 0x7f0e00dd

.field public static final share_gallery_photo_height:I = 0x7f0e00dc

.field public static final share_gallery_remove_margin:I = 0x7f0e00de

.field public static final share_media_height:I = 0x7f0e00d0

.field public static final share_media_margin:I = 0x7f0e00d9

.field public static final share_media_separator_height:I = 0x7f0e00d2

.field public static final share_post_margin:I = 0x7f0e00d6

.field public static final share_preview_margin_top_landscape:I = 0x7f0e00d4

.field public static final share_preview_margin_top_portrait:I = 0x7f0e00d3

.field public static final share_preview_remove_margin:I = 0x7f0e00df

.field public static final share_title_padding_left:I = 0x7f0e00cf

.field public static final share_ui_margin:I = 0x7f0e00d5

.field public static final spinner_padding_left:I = 0x7f0e016f

.field public static final square_card_min_height:I = 0x7f0e0179

.field public static final square_card_min_width:I = 0x7f0e0178

.field public static final square_card_padding:I = 0x7f0e0177

.field public static final stream_circle_count_number_size:I = 0x7f0e0180

.field public static final stream_circle_count_unit_size:I = 0x7f0e0181

.field public static final stream_circle_line_height_reduction:I = 0x7f0e017f

.field public static final stream_circle_min_avatar_size:I = 0x7f0e017d

.field public static final stream_circle_settings_max_size:I = 0x7f0e017e

.field public static final stream_circle_settings_padding:I = 0x7f0e0183

.field public static final stream_circle_settings_size:I = 0x7f0e0182

.field public static final stream_footer_plus_one_count_font_size:I = 0x7f0e0017

.field public static final stream_footer_plus_one_icon_y_offset:I = 0x7f0e0015

.field public static final stream_footer_plus_one_selection_padding:I = 0x7f0e0016

.field public static final stream_one_up_avatar_margin_left:I = 0x7f0e0146

.field public static final stream_one_up_avatar_margin_right:I = 0x7f0e0147

.field public static final stream_one_up_avatar_margin_top:I = 0x7f0e0145

.field public static final stream_one_up_avatar_size:I = 0x7f0e014a

.field public static final stream_one_up_comment_avatar_margin_right:I = 0x7f0e0152

.field public static final stream_one_up_comment_content_text_size:I = 0x7f0e0160

.field public static final stream_one_up_comment_count_divider_width:I = 0x7f0e014d

.field public static final stream_one_up_comment_count_margin_left:I = 0x7f0e014c

.field public static final stream_one_up_comment_count_text_size:I = 0x7f0e015d

.field public static final stream_one_up_comment_date_text_size:I = 0x7f0e015f

.field public static final stream_one_up_comment_divider_thickness:I = 0x7f0e0154

.field public static final stream_one_up_comment_margin_bottom:I = 0x7f0e0151

.field public static final stream_one_up_comment_margin_left:I = 0x7f0e014f

.field public static final stream_one_up_comment_margin_right:I = 0x7f0e0150

.field public static final stream_one_up_comment_margin_top:I = 0x7f0e014e

.field public static final stream_one_up_comment_name_margin_right:I = 0x7f0e0153

.field public static final stream_one_up_comment_name_text_size:I = 0x7f0e015e

.field public static final stream_one_up_comment_plus_one_text_size:I = 0x7f0e0161

.field public static final stream_one_up_content_text_size:I = 0x7f0e0159

.field public static final stream_one_up_date_text_size:I = 0x7f0e0158

.field public static final stream_one_up_font_spacing:I = 0x7f0e011e

.field public static final stream_one_up_linked_body_text_size:I = 0x7f0e015b

.field public static final stream_one_up_linked_header_text_size:I = 0x7f0e015a

.field public static final stream_one_up_list_max_width:I = 0x7f0e0141

.field public static final stream_one_up_list_min_height_land:I = 0x7f0e0120

.field public static final stream_one_up_list_min_height_port:I = 0x7f0e011f

.field public static final stream_one_up_margin_bottom:I = 0x7f0e0144

.field public static final stream_one_up_margin_left:I = 0x7f0e0142

.field public static final stream_one_up_margin_right:I = 0x7f0e0143

.field public static final stream_one_up_name_margin_top:I = 0x7f0e0148

.field public static final stream_one_up_name_text_size:I = 0x7f0e0157

.field public static final stream_one_up_place_review_aspects_margin_bottom:I = 0x7f0e0164

.field public static final stream_one_up_place_review_aspects_margin_top:I = 0x7f0e0163

.field public static final stream_one_up_plus_one_button_margin_right:I = 0x7f0e0149

.field public static final stream_one_up_reshare_body_text_size:I = 0x7f0e015c

.field public static final stream_one_up_side_image:I = 0x7f0e014b

.field public static final stream_one_up_stage_default_text_size:I = 0x7f0e0162

.field public static final stream_one_up_stage_hangout_name_margin:I = 0x7f0e0155

.field public static final stream_one_up_stage_skyjam_play_stop_padding:I = 0x7f0e0156

.field public static final stream_selected_tab_line_height:I = 0x7f0e0020

.field public static final stream_tab_line_height:I = 0x7f0e001f

.field public static final stream_tab_strip_height:I = 0x7f0e001d

.field public static final stream_tab_strip_spacing:I = 0x7f0e001e

.field public static final stream_tab_text_size:I = 0x7f0e0021

.field public static final switcher_item_font_size:I = 0x7f0e00eb

.field public static final tile_one_up_photo_min_exposure_land:I = 0x7f0e0124

.field public static final tile_one_up_photo_min_exposure_port:I = 0x7f0e0123

.field public static final tile_photo_one_up_avatar_margin_left:I = 0x7f0e012d

.field public static final tile_photo_one_up_avatar_margin_right:I = 0x7f0e012e

.field public static final tile_photo_one_up_avatar_margin_top:I = 0x7f0e012c

.field public static final tile_photo_one_up_avatar_size:I = 0x7f0e0131

.field public static final tile_photo_one_up_caption_margin_top:I = 0x7f0e0130

.field public static final tile_photo_one_up_comment_avatar_margin_right:I = 0x7f0e0136

.field public static final tile_photo_one_up_comment_content_text_size:I = 0x7f0e013f

.field public static final tile_photo_one_up_comment_date_margin_right:I = 0x7f0e0138

.field public static final tile_photo_one_up_comment_date_text_size:I = 0x7f0e013e

.field public static final tile_photo_one_up_comment_divider_thickness:I = 0x7f0e0139

.field public static final tile_photo_one_up_comment_margin_bottom:I = 0x7f0e0135

.field public static final tile_photo_one_up_comment_margin_left:I = 0x7f0e0133

.field public static final tile_photo_one_up_comment_margin_right:I = 0x7f0e0134

.field public static final tile_photo_one_up_comment_margin_top:I = 0x7f0e0132

.field public static final tile_photo_one_up_comment_name_margin_right:I = 0x7f0e0137

.field public static final tile_photo_one_up_comment_name_text_size:I = 0x7f0e013d

.field public static final tile_photo_one_up_comment_plus_one_text_size:I = 0x7f0e0140

.field public static final tile_photo_one_up_content_text_size:I = 0x7f0e013c

.field public static final tile_photo_one_up_date_text_size:I = 0x7f0e013b

.field public static final tile_photo_one_up_detail_list_opaque_scroll_position:I = 0x7f0e0125

.field public static final tile_photo_one_up_font_spacing:I = 0x7f0e0127

.field public static final tile_photo_one_up_info_avatar_size:I = 0x7f0e0128

.field public static final tile_photo_one_up_margin_bottom:I = 0x7f0e0129

.field public static final tile_photo_one_up_margin_left:I = 0x7f0e012a

.field public static final tile_photo_one_up_margin_right:I = 0x7f0e012b

.field public static final tile_photo_one_up_name_margin_top:I = 0x7f0e012f

.field public static final tile_photo_one_up_name_text_size:I = 0x7f0e013a

.field public static final tile_photo_one_up_translucent_layer_scroll_position:I = 0x7f0e0126

.field public static final tiny_avatar_dimension:I = 0x7f0e0025

.field public static final titlebar_height:I = 0x7f0e0000

.field public static final titlebar_icon_size:I = 0x7f0e0001

.field public static final titlebar_text_size:I = 0x7f0e0002

.field public static final tray_avatar_height:I = 0x7f0e00bc

.field public static final widget_content_text_size:I = 0x7f0e0019

.field public static final widget_margin_bottom:I = 0x7f0e00ad

.field public static final widget_margin_left:I = 0x7f0e00aa

.field public static final widget_margin_right:I = 0x7f0e00ac

.field public static final widget_margin_top:I = 0x7f0e00ab

.field public static final widget_stream_name_text_size:I = 0x7f0e0018


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
