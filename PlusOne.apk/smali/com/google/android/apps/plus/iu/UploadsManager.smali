.class final Lcom/google/android/apps/plus/iu/UploadsManager;
.super Ljava/lang/Object;
.source "UploadsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/UploadsManager$MessageHandler;,
        Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;,
        Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;
    }
.end annotation


# static fields
.field private static final EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static final PROJECTION_DATA:[Ljava/lang/String;

.field private static final PROJECTION_FINGERPRINT:[Ljava/lang/String;

.field private static final PROJECTION_QUOTA:[Ljava/lang/String;

.field private static sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

.field private mExternalStorageFsId:I

.field private final mHandler:Landroid/os/Handler;

.field private volatile mIsExternalStorageFsIdReady:Z

.field private final mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

.field private final mPreferences:Landroid/content/SharedPreferences;

.field private mResetDelay:J

.field private final mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

.field private final mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

.field private mSyncedAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

.field private mUploadUrl:Ljava/lang/String;

.field private final mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "fingerprint"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_DATA:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "quota_limit"

    aput-object v1, v0, v2

    const-string v1, "quota_used"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_QUOTA:[Ljava/lang/String;

    const-string v0, "content://media/external/fs_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z

    const-wide/16 v4, 0x3a98

    iput-wide v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mResetDelay:J

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/UploadSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "picasa-uploads-manager"

    const/16 v4, 0xa

    invoke-direct {v1, v2, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lcom/google/android/apps/plus/iu/UploadsManager$MessageHandler;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/android/apps/plus/iu/UploadsManager$MessageHandler;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSyncAccount()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    const/16 v4, 0x28

    invoke-virtual {v2, v0, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v4, "external_storage_fsid"

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v4, "external_storage_fsid"

    const/4 v5, -0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mExternalStorageFsId:I

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    const-string v4, "system_release"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "system_release"

    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v4, "iu.UploadsManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "System upgrade from "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " to "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->reset()V

    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/plus/iu/UploadsManager;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    new-instance v5, Lcom/google/android/apps/plus/iu/UploadsManager$1;

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    invoke-direct {v5, p0, v6}, Lcom/google/android/apps/plus/iu/UploadsManager$1;-><init>(Lcom/google/android/apps/plus/iu/UploadsManager;Landroid/os/Handler;)V

    invoke-virtual {v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x7

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    return-void

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/iu/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->onFsIdChangedInternal()V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/apps/plus/iu/UploadsManager;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/iu/UploadsManager;J)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/UploadsManager;->removeTaskFromDb(J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/iu/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/iu/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .param p2    # J

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Landroid/content/SyncResult;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;
    .param p3    # Landroid/content/SyncResult;
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/iu/UploadsManager;->doUpload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Landroid/content/SyncResult;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->updateTaskStateAndProgressInDb(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/MediaRecordEntry;Landroid/content/SyncResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .param p3    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/UploadsManager;->writeToPhotoTable$595d6497(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/MediaRecordEntry;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_QUOTA:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus.iu.manual_upload_report"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v0, "manual_upload_upload_id"

    iget-wide v2, p1, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->id:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v0, "manual_upload_content_uri"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "manual_upload_state"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "manual_upload_uploader_state"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "manual_upload_progress"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getPercentageUploaded()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "manual_upload_media_record_id"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/iu/UploadsManager;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$2000(Landroid/content/Context;)Z
    .locals 1
    .param p0    # Landroid/content/Context;

    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$2200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/iu/UploadsManager;J)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelTaskInternal(J)V

    return-void
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/iu/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->sendUploadAllProgressBroadcast()V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/iu/UploadsManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->uploadExistingPhotosInternal(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/iu/UploadsManager;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelUploadExistingPhotosInternal(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/iu/UploadsManager;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->reset()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/NewMediaTracker;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    return-object v0
.end method

.method static synthetic access$500()Z
    .locals 1

    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->isExternalStorageMounted()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/iu/UploadsManager;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/iu/UploadsManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;
    .param p1    # Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/UploadsManager;->setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/iu/UploadsManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/UploadsManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private declared-synchronized cancelTaskInternal(J)V
    .locals 4
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->cancelIfCurrentTaskMatches(J)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->fromDb(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/UploadsManager;->removeTaskFromDb(J)Z

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V

    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "--- CANCEL task: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized cancelUploadExistingPhotosInternal(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x28

    monitor-enter p0

    :try_start_0
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.UploadsManager"

    const-string v1, "--- CANCEL upload all"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    const/16 v1, 0x28

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->cancelUpload(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    iget v0, v0, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->mUploadType:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;->stopCurrentTask(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private doUpload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Landroid/content/SyncResult;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 13
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;
    .param p3    # Landroid/content/SyncResult;
    .param p4    # Z

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isReadyForUpload()Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    invoke-virtual {p0, p1, v8}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    const-wide/16 v1, 0x3e8

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadUrl:Ljava/lang/String;

    :goto_0
    new-instance v7, Lcom/google/android/apps/plus/iu/GDataUploader;

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/google/android/apps/plus/iu/GDataUploader;-><init>(Landroid/content/Context;)V

    :try_start_1
    move/from16 v0, p4

    invoke-virtual {v7, p1, p2, v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->upload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_1
    .catch Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/apps/plus/iu/Uploader$RestartException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lcom/google/android/apps/plus/iu/Uploader$LocalIoException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lcom/google/android/apps/plus/iu/Uploader$UploadException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v8

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_0
    :goto_1
    return-object v8

    :cond_1
    const/4 v8, 0x0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    :catch_0
    move-exception v3

    :try_start_3
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; media changed: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    const/4 v8, 0x6

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_3
    const/4 v8, 0x0

    goto :goto_1

    :catch_1
    move-exception v3

    :try_start_4
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; media unavailable: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    const/4 v8, 0x6

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    :catch_2
    move-exception v3

    :try_start_5
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_6

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; transient error: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :cond_6
    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_7
    monitor-enter p0

    :try_start_6
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isUploading()Z

    move-result v8

    if-eqz v8, :cond_1b

    add-int/lit8 v5, v5, 0x1

    const/4 v8, 0x5

    if-le v5, v8, :cond_18

    const/4 v8, 0x5

    new-instance v9, Ljava/io/IOException;

    const-string v10, "max retries reached"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1, v8, v9}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V

    move-object/from16 v0, p3

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x4

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_8

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "--- SKIP task; too many retries: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v8, 0x0

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    :catchall_1
    move-exception v8

    monitor-exit p0

    throw v8

    :catch_3
    move-exception v3

    :try_start_7
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_9

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; unauthorized: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_9
    move-object/from16 v0, p3

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v8, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, Landroid/content/SyncStats;->numAuthExceptions:J

    const/16 v8, 0x9

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_a

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_a
    const/4 v8, 0x0

    goto/16 :goto_1

    :catch_4
    move-exception v3

    :try_start_8
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_b

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; quota exceeded: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_b
    const/16 v8, 0xa

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_c

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_c
    const/4 v8, 0x0

    goto/16 :goto_1

    :catch_5
    move-exception v3

    :try_start_9
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->isExternalStorageMounted()Z

    move-result v8

    if-eqz v8, :cond_f

    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_d

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FAIL task: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_d
    const/4 v8, 0x5

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V

    move-object/from16 v0, p3

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :goto_2
    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_e

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_f
    :try_start_a
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_10

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; media unmounted: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_10
    const/4 v8, 0x6

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto :goto_2

    :catchall_2
    move-exception v8

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_11

    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    :cond_11
    throw v8

    :catch_6
    move-exception v3

    :try_start_b
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_12

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PAUSE task; retryable exception: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_12
    move-object/from16 v0, p3

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v8, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, Landroid/content/SyncStats;->numIoExceptions:J

    const/4 v8, 0x6

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_13

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_13
    const/4 v8, 0x0

    goto/16 :goto_1

    :catch_7
    move-exception v3

    :try_start_c
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_14

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FAIL task: permanent failure: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_14
    move-object/from16 v0, p3

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const/4 v8, 0x5

    invoke-virtual {p0, p1, v8, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_15

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_15
    const/4 v8, 0x0

    goto/16 :goto_1

    :catch_8
    move-exception v6

    :try_start_d
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x6

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_16

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "FAIL task: permanent failure: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_16
    move-object/from16 v0, p3

    iget-object v8, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    iput-wide v9, v8, Landroid/content/SyncStats;->numSkippedEntries:J

    const/4 v8, 0x5

    invoke-virtual {p0, p1, v8, v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getResizedFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_17

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setResizedFilename(Ljava/lang/String;)V

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->delete()Z

    :cond_17
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_18
    :try_start_e
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_19

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "   back off "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_19
    const/4 v8, 0x2

    invoke-virtual {p0, p1, v8}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :try_start_f
    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_9
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    :cond_1a
    :goto_3
    const-wide/16 v8, 0x2

    mul-long/2addr v1, v8

    :cond_1b
    :try_start_10
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->shouldRetry()Z

    move-result v8

    if-eqz v8, :cond_1c

    const/4 v8, 0x1

    invoke-virtual {p0, p1, v8}, Lcom/google/android/apps/plus/iu/UploadsManager;->setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    monitor-exit p0

    goto/16 :goto_0

    :catch_9
    move-exception v8

    :try_start_11
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1a

    const-string v8, "iu.UploadsManager"

    const-string v9, "waiting being interrupted!"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_1c
    const/4 v8, 0x0

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto/16 :goto_1
.end method

.method private static getFilePath(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 8
    .param p0    # Landroid/net/Uri;
    .param p1    # Landroid/content/ContentResolver;

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const/4 v6, 0x0

    :try_start_0
    sget-object v2, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_DATA:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_1
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    move-object v0, v7

    goto :goto_1

    :catch_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    move-object v0, v7

    goto :goto_0

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private static getFsId(Landroid/content/Context;)I
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/UploadsManager;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return v1

    :cond_0
    :try_start_1
    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "iu.UploadsManager"

    const-string v2, "No FSID on this device!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    const/4 v1, -0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/UploadsManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/UploadsManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/UploadsManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static isExternalStorageMounted()Z
    .locals 2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private notifyUploadChanges()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void
.end method

.method private declared-synchronized onFsIdChangedInternal()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->isExternalStorageMounted()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "iu.UploadsManager"

    const-string v2, "external storage not mounted"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->getFsId(Landroid/content/Context;)I

    move-result v0

    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "storage changed; old: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mExternalStorageFsId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", new: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z

    if-nez v1, :cond_4

    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "set fsid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mIsExternalStorageFsIdReady:Z

    iput v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mExternalStorageFsId:I

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "external_storage_fsid"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_4
    :try_start_2
    iget v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mExternalStorageFsId:I

    if-ne v1, v0, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    goto :goto_1

    :cond_5
    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fsid changed from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mExternalStorageFsId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iput v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mExternalStorageFsId:I

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "external_storage_fsid"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->reset()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private removeTaskFromDb(J)Z
    .locals 2
    .param p1    # J

    sget-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/android/gallery3d/common/EntrySchema;->deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z

    move-result v0

    return v0
.end method

.method private requestUploadAllProgressBroadcast()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method private declared-synchronized reset()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->requestUploadAllProgressBroadcast()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->reset()V

    const-wide/16 v0, 0x3a98

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mResetDelay:J
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSyncedAccounts:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings(Landroid/database/Cursor;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "iu.UploadsManager"

    const-string v2, "DB not ready for reset?"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mResetDelay:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mResetDelay:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mResetDelay:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private sendUploadAllProgressBroadcast()V
    .locals 13

    const/16 v12, 0x28

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSyncAccount()Ljava/lang/String;

    move-result-object v9

    const/4 v6, 0x0

    const/4 v11, -0x1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "upload_all_state"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    :cond_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadTotal$505cff29(I)I

    move-result v10

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    invoke-virtual {v0, v9, v12}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v8

    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.google.android.apps.plus.iu.upload_all_progress"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "upload_all_account"

    invoke-virtual {v7, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "upload_all_progress"

    sub-int v1, v10, v8

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "upload_all_count"

    invoke-virtual {v7, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "upload_all_state"

    invoke-virtual {v7, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private declared-synchronized setCurrentUploadTask(Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mCurrent:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setMediaRecordStateAndProgress(JILjava/lang/Throwable;ZJJLjava/lang/String;)V
    .locals 9
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/Throwable;
    .param p5    # Z
    .param p6    # J
    .param p8    # J
    .param p10    # Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1, p2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p3, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/16 v3, 0x64

    const/4 v4, 0x1

    :goto_1
    if-eqz p5, :cond_1

    move-wide v0, p6

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setBytesTotal(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-wide/from16 v0, p8

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setBytesUploaded(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    if-eqz p10, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadUrl:Ljava/lang/String;

    move-object/from16 v0, p10

    invoke-static {v0, v7}, Lcom/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    move-object/from16 v0, p10

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    :cond_1
    invoke-virtual {v2, v3, v4, p4}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(IILjava/lang/Throwable;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v7, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-virtual {v7, v8, v2}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto :goto_0

    :pswitch_2
    const/16 v3, 0x190

    const/4 v4, 0x0

    goto :goto_1

    :pswitch_3
    const/16 v3, 0x190

    const/16 v4, 0x22

    goto :goto_1

    :pswitch_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    move-result-wide v7

    cmp-long v7, v7, v5

    if-gez v7, :cond_2

    const/16 v3, 0x12c

    const/16 v4, 0x28

    goto :goto_1

    :cond_2
    const/16 v3, 0xc8

    const/4 v4, 0x0

    goto :goto_1

    :pswitch_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    move-result-wide v7

    cmp-long v7, v7, v5

    if-gez v7, :cond_3

    const/16 v3, 0x12c

    const/16 v4, 0x28

    goto :goto_1

    :cond_3
    const/16 v3, 0x64

    const/4 v4, 0x2

    goto :goto_1

    :pswitch_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    move-result-wide v7

    cmp-long v7, v7, v5

    if-gez v7, :cond_4

    const/16 v3, 0x12c

    :goto_2
    const/16 v4, 0x1f

    goto :goto_1

    :cond_4
    const/16 v3, 0x64

    goto :goto_2

    :pswitch_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J

    move-result-wide v7

    cmp-long v7, v7, v5

    if-gez v7, :cond_5

    const/16 v3, 0x12c

    :goto_3
    const/16 v4, 0x1e

    goto/16 :goto_1

    :cond_5
    const/16 v3, 0x64

    goto :goto_3

    :pswitch_8
    const/16 v3, 0x12c

    const/4 v4, 0x0

    goto/16 :goto_1

    :pswitch_9
    const/16 v3, 0x12c

    const/16 v4, 0x27

    goto/16 :goto_1

    :pswitch_a
    const/16 v3, 0x12c

    const/16 v4, 0x21

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_8
        :pswitch_5
        :pswitch_0
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_a
        :pswitch_3
    .end packed-switch
.end method

.method private static setRetryEndTime(Lcom/google/android/apps/plus/iu/MediaRecordEntry;J)J
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .param p1    # J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getRetryEndTime()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const-wide/32 v2, 0x66ff300

    add-long v0, p1, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setRetryEndTime(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    :cond_0
    return-wide v0
.end method

.method private declared-synchronized updateTaskStateAndProgressInDb(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isReadyForUpload()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "state"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "bytes_uploaded"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadUrl:Ljava/lang/String;

    invoke-static {v10, v0}, Lcom/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "upload_url"

    invoke-virtual {v11, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getState()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v8

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/iu/UploadsManager;->setMediaRecordStateAndProgress(JILjava/lang/Throwable;ZJJLjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized uploadExistingPhotosInternal(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "iu.UploadsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "enable existing photos upload for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    const/16 v3, 0x28

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->startUpload(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    const/16 v3, 0x28

    invoke-virtual {v2, p1, v3}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "upload_all_state"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->sendUploadAllProgressBroadcast()V

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "iu.UploadsManager"

    const-string v3, "--- DONE upload all; no more photos"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->isExternalStorageMounted()Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v0, Landroid/content/ContentValues;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    const-string v2, "upload_all_state"

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->sendUploadAllProgressBroadcast()V

    const-string v2, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "iu.UploadsManager"

    const-string v3, "--- DONE upload all; no storage"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v2

    const-wide/16 v3, 0x1f4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private writeToPhotoTable$595d6497(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/MediaRecordEntry;)Z
    .locals 13
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getUserId(Ljava/lang/String;)J

    move-result-wide v7

    const-wide/16 v9, -0x1

    cmp-long v9, v7, v9

    if-nez v9, :cond_1

    const-string v9, "iu.UploadsManager"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "iu.UploadsManager"

    const-string v10, "no user owns the photo"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/iu/UploadsManager;->getFilePath(Landroid/net/Uri;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/plus/iu/PhotoEntry;

    invoke-direct {v5}, Lcom/google/android/apps/plus/iu/PhotoEntry;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getUploadId()J

    move-result-wide v9

    iput-wide v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->id:J

    iput-wide v7, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->userId:J

    iput-object v0, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->albumId:Ljava/lang/String;

    if-nez v2, :cond_5

    const-string v2, "No title"

    :cond_2
    :goto_1
    iput-object v2, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->title:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getBytesTotal()J

    move-result-wide v9

    long-to-int v9, v9

    iput v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->size:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->contentType:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getFingerprintBytes()[B

    move-result-object v9

    iput-object v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->fingerprint:[B

    invoke-virtual {p2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v9

    iput v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->fingerprintHash:I

    invoke-virtual {p2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getMediaTime()J

    move-result-wide v9

    iput-wide v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->dateTaken:J

    :try_start_0
    new-instance v9, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v9}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v10, 0x1

    iput-boolean v10, v9, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-virtual {v6, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11, v9}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v10, v9, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v10, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->width:I

    iget v9, v9, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v9, v5, Lcom/google/android/apps/plus/iu/PhotoEntry;->height:I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_2
    sget-object v9, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v10, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    invoke-virtual {v9, v10, v5}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    move-result-wide v3

    const-string v9, "iu.UploadsManager"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "iu.UploadsManager"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "write to photo table: ID="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", album="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v9, 0x1

    goto/16 :goto_0

    :cond_5
    const-string v9, "/"

    invoke-virtual {v2, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    if-lez v9, :cond_2

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :catch_0
    move-exception v9

    const-string v10, "iu.UploadsManager"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "iu.UploadsManager"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "setPhotoSize: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ": "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v10, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public final addUpload(Landroid/content/ContentValues;)J
    .locals 9
    .param p1    # Landroid/content/ContentValues;

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v6, "_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    const-string v6, "_id"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const/4 v1, 0x0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v0, v6, v7}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v1

    :goto_0
    const-string v6, "upload_reason"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "upload_reason"

    invoke-virtual {p1, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_1
    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadReason(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    const/16 v6, 0x64

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v6, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v6, v0, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    move-result-wide v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V

    const-string v6, "iu.UploadsManager"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "iu.UploadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+++ ADD record; manual upload: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v6

    const-wide/16 v7, 0x1f4

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V

    return-wide v2

    :cond_2
    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->update(Landroid/content/ContentValues;)V

    goto :goto_0

    :cond_3
    const/16 v5, 0xa

    goto :goto_1
.end method

.method public final cancelTask(J)V
    .locals 3
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final cancelUploadExistingPhotos(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public final getInstantUploadStatus()Landroid/database/Cursor;
    .locals 6

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSyncAccount()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "iu_pending_count"

    aput-object v5, v3, v4

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    const/16 v4, 0x1e

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "get iu pending count for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v1
.end method

.method public final getUploadAllStatus(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1    # Ljava/lang/String;

    const/16 v11, 0x28

    const/4 v10, 0x3

    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const-string v7, "upload_all_account"

    aput-object v7, v6, v5

    const-string v7, "upload_all_progress"

    aput-object v7, v6, v4

    const/4 v7, 0x2

    const-string v8, "upload_all_count"

    aput-object v8, v6, v7

    const-string v7, "upload_all_state"

    aput-object v7, v6, v10

    invoke-direct {v0, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz p1, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    invoke-virtual {v6, v11}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadTotal$505cff29(I)I

    move-result v3

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mToddsMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    invoke-virtual {v6, p1, v11}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getUploadProgress(Ljava/lang/String;I)I

    move-result v1

    sub-int v2, v3, v1

    const-string v6, "iu.UploadsManager"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "iu.UploadsManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "get upload-all status for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " allDone? "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-ne v3, v2, :cond_1

    :goto_0
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " current:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " total:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " state=0"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    :goto_1
    return-object v0

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_1
.end method

.method public final reloadSystemSettings()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSystemSettingsCursor()Landroid/database/Cursor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method final retrieveAllFingerprints(Ljava/lang/String;)Ljava/util/HashSet;
    .locals 14
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/gallery3d/common/Fingerprint;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x3

    const/4 v13, 0x0

    if-nez p1, :cond_0

    move-object v10, v13

    :goto_0
    return-object v10

    :cond_0
    const-string v1, "iu.UploadsManager"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "retrieveAllFingerprints for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mPicasaDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getUserId(Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v11

    const-wide/16 v1, -0x1

    cmp-long v1, v11, v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v10, v13

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v1, Lcom/google/android/apps/plus/iu/UploadsManager;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/UploadsManager;->PROJECTION_FINGERPRINT:[Ljava/lang/String;

    const-string v3, "user_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v8

    if-nez v8, :cond_3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v10, v13

    goto :goto_0

    :cond_3
    :goto_1
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    if-eqz v9, :cond_3

    new-instance v1, Lcom/android/gallery3d/common/Fingerprint;

    invoke-direct {v1, v9}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V

    invoke-virtual {v10, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    :cond_4
    :try_start_4
    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "iu.UploadsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "create fingerprint set:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/util/HashSet;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_5
    :try_start_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0
.end method

.method final declared-synchronized setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;I)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # I

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setState(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move v3, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/iu/UploadsManager;->setMediaRecordStateAndProgress(JILjava/lang/Throwable;ZJJLjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized setState(Lcom/google/android/apps/plus/iu/UploadTaskEntry;ILjava/lang/Throwable;)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # I
    .param p3    # Ljava/lang/Throwable;

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1, p2, p3}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setState(ILjava/lang/Throwable;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v1

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/iu/UploadsManager;->setMediaRecordStateAndProgress(JILjava/lang/Throwable;ZJJLjava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/UploadsManager;->notifyUploadChanges()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final uploadExistingPhotos(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/UploadsManager;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
