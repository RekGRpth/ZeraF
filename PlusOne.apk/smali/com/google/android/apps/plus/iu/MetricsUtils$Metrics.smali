.class final Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;
.super Ljava/lang/Object;
.source "MetricsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/MetricsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Metrics"
.end annotation


# instance fields
.field public endTimestamp:J

.field public inBytes:J

.field public name:Ljava/lang/String;

.field public networkOpCount:I

.field public networkOpDuration:J

.field public nextFree:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

.field public outBytes:J

.field public queryResultCount:I

.field public startTimestamp:J

.field public updateCount:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized obtain(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;
    .locals 5
    .param p0    # Ljava/lang/String;

    const-class v2, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/MetricsUtils;->sFreeMetrics:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    invoke-direct {v0}, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;-><init>()V

    :goto_0
    iput-object p0, v0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->name:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, v0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->startTimestamp:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    return-object v0

    :cond_0
    :try_start_1
    iget-object v1, v0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->nextFree:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    sput-object v1, Lcom/google/android/apps/plus/iu/MetricsUtils;->sFreeMetrics:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized recycle(Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    const-class v1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/MetricsUtils;->sFreeMetrics:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->nextFree:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    sput-object p0, Lcom/google/android/apps/plus/iu/MetricsUtils;->sFreeMetrics:Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final merge(Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;

    iget v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->queryResultCount:I

    iget v1, p1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->queryResultCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->queryResultCount:I

    iget v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->updateCount:I

    iget v1, p1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->updateCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->updateCount:I

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->inBytes:J

    iget-wide v2, p1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->inBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->inBytes:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->outBytes:J

    iget-wide v2, p1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->outBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->outBytes:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpDuration:J

    iget-wide v2, p1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpDuration:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpDuration:J

    iget v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpCount:I

    iget v1, p1, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpCount:I

    return-void
.end method

.method public final recycle()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->name:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->queryResultCount:I

    iput v1, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->updateCount:I

    iput-wide v2, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->inBytes:J

    iput-wide v2, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->outBytes:J

    iput-wide v2, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpDuration:J

    iput v1, p0, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->networkOpCount:I

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;->recycle(Lcom/google/android/apps/plus/iu/MetricsUtils$Metrics;)V

    return-void
.end method
