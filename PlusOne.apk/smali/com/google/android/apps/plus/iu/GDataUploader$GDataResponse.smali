.class final Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "GDataUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/GDataUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GDataResponse"
.end annotation


# instance fields
.field errorCode:Ljava/lang/String;

.field fingerprint:Lcom/android/gallery3d/common/Fingerprint;

.field iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

.field private mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamIdList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mText:Ljava/lang/StringBuilder;

.field photoId:J

.field photoUrl:Ljava/lang/String;

.field timestamp:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;-><init>()V

    return-void
.end method

.method private static getIUStatsAttrs(Lorg/xml/sax/Attributes;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;
    .locals 6
    .param p0    # Lorg/xml/sax/Attributes;

    new-instance v3, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    invoke-direct {v3}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;-><init>()V

    invoke-interface {p0}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "quotaLimitMB"

    invoke-virtual {v4, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    :try_start_0
    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaLimit:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const-string v4, "quotaUsedMB"

    invoke-virtual {v4, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    :try_start_1
    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->quotaUsed:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    goto :goto_1

    :cond_2
    const-string v4, "disableFullRes"

    invoke-virtual {v4, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v3, Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;->disableFullRes:Z

    goto :goto_1

    :cond_3
    return-object v3

    :catch_1
    move-exception v4

    goto :goto_1
.end method


# virtual methods
.method public final characters([CII)V
    .locals 1
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v0, "gphoto:streamId"

    invoke-virtual {v0, p3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    return-void
.end method

.method public final startDocument()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "code"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:id"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:size"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:streamId"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:timestamp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    if-nez v0, :cond_4

    const-string v0, "media:content"

    invoke-virtual {v0, p3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "url"

    invoke-virtual {v3, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    :cond_0
    :goto_2
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    const-string v0, "gphoto:iuStats"

    invoke-virtual {v0, p3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p4}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->getIUStatsAttrs(Lorg/xml/sax/Attributes;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mText:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_2
.end method

.method public final validateResult()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "code"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:id"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoId:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v1, "gphoto:timestamp"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->timestamp:J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v1, "photo URL missing"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error parsing photo ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v3, "gphoto:id"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error parsing timestamp: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mMap:Ljava/util/HashMap;

    const-string v3, "gphoto:timestamp"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/android/gallery3d/common/Fingerprint;->extractFingerprint(Ljava/util/List;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->fingerprint:Lcom/android/gallery3d/common/Fingerprint;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->fingerprint:Lcom/android/gallery3d/common/Fingerprint;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fingerprint missing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->mStreamIdList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method
