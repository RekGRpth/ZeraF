.class public Lcom/google/android/apps/plus/iu/MediaRecordEntry;
.super Lcom/android/gallery3d/common/Entry;
.source "MediaRecordEntry.java"


# annotations
.annotation runtime Lcom/android/gallery3d/common/Entry$Table;
    value = "media_record"
.end annotation


# static fields
.field public static final SCHEMA:Lcom/android/gallery3d/common/EntrySchema;


# instance fields
.field private mAlbumId:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "album_id"
    .end annotation
.end field

.field private mBytesTotal:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "bytes_total"
    .end annotation
.end field

.field private mBytesUploaded:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "bytes_uploaded"
    .end annotation
.end field

.field private mError:Ljava/lang/Throwable;

.field private mEventId:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "event_id"
    .end annotation
.end field

.field private mFingerprint:[B
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "fingerprint"
    .end annotation
.end field

.field private mFromCamera:Z
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        defaultValue = "0"
        value = "from_camera"
    .end annotation
.end field

.field private mIsImage:Z
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        defaultValue = "1"
        value = "is_image"
    .end annotation
.end field

.field private mMediaHash:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        value = "media_hash"
    .end annotation
.end field

.field private mMediaId:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        indexed = true
        value = "media_id"
    .end annotation
.end field

.field private mMediaTime:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        value = "media_time"
    .end annotation
.end field

.field private mMediaUrl:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        value = "media_url"
    .end annotation
.end field

.field private mRetryEndTime:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        defaultValue = "0"
        value = "retry_end_time"
    .end annotation
.end field

.field private mUploadAccount:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "upload_account"
    .end annotation
.end field

.field private mUploadError:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "upload_error"
    .end annotation
.end field

.field private mUploadId:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "upload_id"
    .end annotation
.end field

.field private mUploadReason:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        defaultValue = "0"
        value = "upload_reason"
    .end annotation
.end field

.field private mUploadState:I
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        allowNull = false
        defaultValue = "500"
        value = "upload_state"
    .end annotation
.end field

.field private mUploadTime:J
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "upload_time"
    .end annotation
.end field

.field private mUploadUrl:Ljava/lang/String;
    .annotation runtime Lcom/android/gallery3d/common/Entry$Column;
        value = "upload_url"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/android/gallery3d/common/EntrySchema;

    const-class v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/gallery3d/common/Entry;-><init>()V

    return-void
.end method

.method static createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 2
    .param p0    # Landroid/content/ContentValues;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lcom/android/gallery3d/common/EntrySchema;->valuesToObject(Landroid/content/ContentValues;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    return-object v0
.end method

.method public static fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 2
    .param p0    # Landroid/database/Cursor;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, p0, v1}, Lcom/android/gallery3d/common/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    return-object v0
.end method

.method public static fromId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 2
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # J

    new-instance v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    invoke-direct {v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1, p0, p1, p2, v0}, Lcom/android/gallery3d/common/EntrySchema;->queryWithId(Landroid/database/sqlite/SQLiteDatabase;JLcom/android/gallery3d/common/Entry;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static fromMediaId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 9
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # J

    const/4 v5, 0x0

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "media_id = ? AND upload_account IS NULL"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v0

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;-><init>()V

    invoke-virtual {v0, v8, v1}, Lcom/android/gallery3d/common/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move-object v0, v5

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final getAlbumId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final getBytesTotal()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesTotal:J

    return-wide v0
.end method

.method public final getEventId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method public final getFingerprint()Lcom/android/gallery3d/common/Fingerprint;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mFingerprint:[B

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/gallery3d/common/Fingerprint;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mFingerprint:[B

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getFingerprintBytes()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mFingerprint:[B

    return-object v0
.end method

.method public final getMediaTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mMediaTime:J

    return-wide v0
.end method

.method public final getMediaUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mMediaUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getRetryEndTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mRetryEndTime:J

    return-wide v0
.end method

.method public final getUploadAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadAccount:Ljava/lang/String;

    return-object v0
.end method

.method public final getUploadId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadId:J

    return-wide v0
.end method

.method public final getUploadReason()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadReason:I

    return v0
.end method

.method public final getUploadTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadTime:J

    return-wide v0
.end method

.method public final isImage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mIsImage:Z

    return v0
.end method

.method public final setBytesTotal(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesTotal:J

    return-object p0
.end method

.method public final setBytesUploaded(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesUploaded:J

    return-object p0
.end method

.method public final setRetryEndTime(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mRetryEndTime:J

    return-object p0
.end method

.method public final setState(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadState:I

    return-object p0
.end method

.method public final setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 1
    .param p1    # I
    .param p2    # I

    add-int v0, p1, p2

    iput v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadState:I

    return-object p0
.end method

.method public final setState(IILjava/lang/Throwable;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/Throwable;

    add-int v0, p1, p2

    iput v0, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadState:I

    iput-object p3, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mError:Ljava/lang/Throwable;

    return-object p0
.end method

.method public final setUploadAccount(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadAccount:Ljava/lang/String;

    return-object p0
.end method

.method public final setUploadId(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadId:J

    return-object p0
.end method

.method public final setUploadReason(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadReason:I

    return-object p0
.end method

.method public final setUploadTime(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadTime:J

    return-object p0
.end method

.method public final setUploadUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mUploadUrl:Ljava/lang/String;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const/16 v3, 0x9

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "media_url"

    aput-object v4, v3, v0

    const/4 v4, 0x1

    const-string v5, "media_id"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "album_id"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "event_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "upload_reason"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "upload_state"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "upload_account"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "upload_url"

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const-string v5, "bytes_total"

    aput-object v5, v3, v4

    invoke-virtual {v2, p0, v3}, Lcom/android/gallery3d/common/EntrySchema;->toDebugString(Lcom/android/gallery3d/common/Entry;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesTotal:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesUploaded:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesUploaded:J

    long-to-float v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->mBytesTotal:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    float-to-double v2, v0

    const-wide/high16 v4, 0x4059000000000000L

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    const/16 v2, 0x64

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public final update(Landroid/content/ContentValues;)V
    .locals 1
    .param p1    # Landroid/content/ContentValues;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0, p1, p0}, Lcom/android/gallery3d/common/EntrySchema;->valuesToObject(Landroid/content/ContentValues;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    return-void
.end method
