.class final Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;
.super Landroid/os/AsyncTask;
.source "InstantUploadProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/InstantUploadProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "QuotaTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mAccount:Ljava/lang/String;

    return-void
.end method

.method private varargs doInBackground$10299ca()Ljava/lang/Void;
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Lcom/google/android/apps/plus/iu/GDataUploader;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/iu/GDataUploader;-><init>(Landroid/content/Context;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->getQuota(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mAccount:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateQuotaSettings(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    # getter for: Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->access$000()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_1
    # getter for: Lcom/google/android/apps/plus/iu/InstantUploadProvider;->sQuotaTask:Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;
    invoke-static {}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->access$100()Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    move-result-object v2

    if-ne v2, p0, :cond_1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->access$102(Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;)Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;

    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-object v4

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->close()V

    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->doInBackground$10299ca()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method final getAccount()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider$QuotaTask;->mAccount:Ljava/lang/String;

    return-object v0
.end method
