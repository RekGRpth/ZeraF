.class final Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;
.super Ljava/lang/Object;
.source "EsEventData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsEventData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EventActivityKey"
.end annotation


# instance fields
.field public ownerGaiaId:Ljava/lang/String;

.field public timestamp:J

.field public type:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;-><init>()V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;

    iget v1, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    iget v2, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v1, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    iget-wide v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    iget v1, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->type:I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->timestamp:J

    add-long/2addr v0, v2

    long-to-int v0, v0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsEventData$EventActivityKey;->ownerGaiaId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method
