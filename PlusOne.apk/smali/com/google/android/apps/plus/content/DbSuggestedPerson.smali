.class public final Lcom/google/android/apps/plus/content/DbSuggestedPerson;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbSuggestedPerson.java"


# instance fields
.field private mCircles:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation
.end field

.field private mCommonFriends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;"
        }
    .end annotation
.end field

.field private mDescription:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;

.field private mPersonInfo:Lcom/google/android/apps/plus/content/PersonData;

.field private mSuggestionId:Ljava/lang/String;

.field private mTotalCommonFriendsCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/api/services/plusi/model/PeopleViewPerson;)V
    .locals 15
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/api/services/plusi/model/PeopleViewPerson;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->numberOfCommonFriends:Ljava/lang/Integer;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v5

    const/4 v12, 0x0

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->suggestionId:Ljava/lang/String;

    const/4 v7, 0x0

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v1, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v1, :cond_0

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v12, v1, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    :cond_0
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v1, :cond_3

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    :goto_0
    invoke-static {v12, v1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->generatePersonData(Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v4

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v1, :cond_1

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v7

    :cond_1
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->numberOfCommonFriends:Ljava/lang/Integer;

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v1, :cond_4

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    :goto_1
    move-object/from16 v0, p1

    invoke-static {v0, v14, v1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->generateDescription(Landroid/content/Context;Ljava/lang/Integer;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->commonFriend:Ljava/util/List;

    if-eqz v1, :cond_5

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;->commonFriend:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/api/services/plusi/model/DataCirclePerson;

    const/4 v11, 0x0

    iget-object v1, v9, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v1, :cond_2

    iget-object v1, v9, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v11, v1, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    :cond_2
    iget-object v1, v9, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    invoke-static {v11, v1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->generatePersonData(Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v10

    invoke-interface {v2, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    move-object v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->init(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/PersonData;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/PersonData;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p3    # Lcom/google/android/apps/plus/content/PersonData;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;",
            "Lcom/google/android/apps/plus/content/PersonData;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->init(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/PersonData;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static generatePersonData(Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCircleMemberProperties;)Lcom/google/android/apps/plus/content/PersonData;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->photoUrl:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    if-eqz v5, :cond_1

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataEmail;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataEmail;->primary:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    :cond_1
    new-instance v5, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v5, p0, v1, v2, v4}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v5
.end method

.method private static getPromoCircle(Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/content/CircleData;
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/content/CircleData;

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    const/16 v1, 0x12d

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/CircleData;->setType(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/CircleData;->setSize$13462e()V

    return-object v0
.end method

.method protected static getSuggestedPerson(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/DbSuggestedPerson;
    .locals 8
    .param p0    # Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v7

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson(Ljava/nio/ByteBuffer;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPeople(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getCircles(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;-><init>(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/PersonData;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private init(Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/plus/content/PersonData;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/plus/content/PersonData;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;",
            "Lcom/google/android/apps/plus/content/PersonData;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCommonFriends:Ljava/util/List;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mPersonInfo:Lcom/google/android/apps/plus/content/PersonData;

    iput-object p7, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mSuggestionId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mTotalCommonFriendsCount:I

    iput-object p5, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mDescription:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mSuggestionId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mPersonId:Ljava/lang/String;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    invoke-virtual {v0, p2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method protected static putSuggestedPerson(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/DbSuggestedPerson;)V
    .locals 1
    .param p0    # Ljava/io/DataOutputStream;
    .param p1    # Lcom/google/android/apps/plus/content/DbSuggestedPerson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mTotalCommonFriendsCount:I

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mPersonId:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mSuggestionId:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mDescription:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mPersonInfo:Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putPerson(Ljava/io/DataOutputStream;Lcom/google/android/apps/plus/content/PersonData;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCommonFriends:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putPeople(Ljava/io/DataOutputStream;Ljava/util/List;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getCircles()Ljava/util/List;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->putCircles(Ljava/io/DataOutputStream;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final addCircle(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPromoCircle(Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final addCircles(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getCircles()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getPerson()Lcom/google/android/apps/plus/content/PersonData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mPersonInfo:Lcom/google/android/apps/plus/content/PersonData;

    return-object v0
.end method

.method public final getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSuggestionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mSuggestionId:Ljava/lang/String;

    return-object v0
.end method

.method public final removeCircle(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPromoCircle(Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final removeCircleList([Ljava/lang/String;)V
    .locals 6
    .param p1    # [Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v5, p1

    add-int/lit8 v2, v5, -0x1

    :goto_0
    if-ltz v2, :cond_2

    aget-object v1, p1, v2

    iget-object v5, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->mCircles:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    return-void
.end method
