.class public Lcom/google/android/apps/plus/content/SquareTargetData;
.super Ljava/lang/Object;
.source "SquareTargetData.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/SquareTargetData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSquareId:Ljava/lang/String;

.field private mSquareName:Ljava/lang/String;

.field private mSquareStreamId:Ljava/lang/String;

.field private mSquareStreamName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/SquareTargetData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/SquareTargetData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/SquareTargetData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/SquareTargetData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Ljava/lang/Object;

    instance-of v1, p1, Lcom/google/android/apps/plus/content/SquareTargetData;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getSquareId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareStreamId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSquareStreamName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    if-eqz v1, :cond_1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    if-eqz v1, :cond_2

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    if-eqz v1, :cond_3

    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    :cond_3
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{SquareStreamData name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " stream="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/SquareTargetData;->mSquareStreamName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
