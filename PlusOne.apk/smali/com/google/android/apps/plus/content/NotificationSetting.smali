.class public Lcom/google/android/apps/plus/content/NotificationSetting;
.super Ljava/lang/Object;
.source "NotificationSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/NotificationSetting$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/NotificationSetting$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/NotificationSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/NotificationSetting;->readBoolean(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/NotificationSetting;->readBoolean(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/NotificationSetting;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/NotificationSetting;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/NotificationSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    return-void
.end method

.method private static readBoolean(Landroid/os/Parcel;)Ljava/lang/Boolean;
    .locals 2
    .param p0    # Landroid/os/Parcel;

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    return-object v1

    :cond_0
    if-nez v0, :cond_1

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static writeBoolean(Landroid/os/Parcel;Ljava/lang/Boolean;)V
    .locals 1
    .param p0    # Landroid/os/Parcel;
    .param p1    # Ljava/lang/Boolean;

    if-nez p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getDeliveryOption()Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setEnabled(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{Setting "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " offNetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/NotificationSetting;->writeBoolean(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/NotificationSetting;->writeBoolean(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    return-void
.end method
