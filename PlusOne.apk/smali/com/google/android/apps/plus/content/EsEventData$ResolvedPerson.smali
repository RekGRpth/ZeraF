.class public final Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;
.super Ljava/lang/Object;
.source "EsEventData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsEventData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResolvedPerson"
.end annotation


# instance fields
.field public avatarUrl:Ljava/lang/String;

.field public gaiaId:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;->gaiaId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;->avatarUrl:Ljava/lang/String;

    return-void
.end method
