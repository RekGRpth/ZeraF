.class final Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;
.super Ljava/lang/Object;
.source "EsSquaresData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsSquaresData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SquareData"
.end annotation


# instance fields
.field public final inviter:Lcom/google/api/services/plusi/model/SquareMember;

.field public final sortIndex:I

.field public final suggested:Z

.field public final suggestionSortIndex:I

.field public final viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;


# direct methods
.method public constructor <init>(Lcom/google/api/services/plusi/model/ViewerSquare;ZLcom/google/api/services/plusi/model/SquareMember;II)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/ViewerSquare;
    .param p2    # Z
    .param p3    # Lcom/google/api/services/plusi/model/SquareMember;
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->suggested:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->inviter:Lcom/google/api/services/plusi/model/SquareMember;

    iput p4, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->sortIndex:I

    iput p5, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->suggestionSortIndex:I

    return-void
.end method


# virtual methods
.method public final getInviterGaiaId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->inviter:Lcom/google/api/services/plusi/model/SquareMember;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->inviter:Lcom/google/api/services/plusi/model/SquareMember;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
