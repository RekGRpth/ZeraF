.class public Lcom/google/android/apps/plus/content/AccountSettingsData;
.super Ljava/lang/Object;
.source "AccountSettingsData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/AccountSettingsData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBestLocationSharingAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mIsChild:Z

.field private mIsLocationSharingEnabled:Z

.field private mNotGooglePlus:Z

.field private mPlusPageIds:[Ljava/lang/String;

.field private mPlusPageNames:[Ljava/lang/String;

.field private mPlusPagePhotoUrls:[Ljava/lang/String;

.field private mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

.field private mUserDisplayName:Ljava/lang/String;

.field private mUserGaiaId:Ljava/lang/String;

.field private mWarmWelcomeTimestamp:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/AccountSettingsData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/AccountSettingsData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;Z)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;
    .param p4    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    iget-object v1, p3, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    if-eqz v1, :cond_4

    iget-object v0, p3, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->notGooglePlus:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->notGooglePlus:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ENABLE_SKINNY_PLUS_PAGE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_1
    :goto_1
    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->displayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->deviceLocationAcl:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->deviceLocationAcl:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->deviceLocationAcl:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->sharingEnabled:Ljava/lang/Boolean;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    :cond_2
    iput-boolean v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsLocationSharingEnabled:Z

    if-nez p4, :cond_3

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/plus/content/AccountSettingsData;->populateLocationSharingAcls(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;)V

    :cond_3
    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->plusPageInfo:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/content/AccountSettingsData;->setPlusPages(Ljava/util/List;)V

    :cond_4
    iget-object v1, p3, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    if-eqz v1, :cond_5

    iget-object v1, p3, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobilePreference;->wwMainFlowAckTimestampMsec:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    :cond_5
    iget-object v1, p3, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    return-void

    :cond_6
    move v1, v3

    goto :goto_0

    :cond_7
    move v2, v3

    goto :goto_1
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsLocationSharingEnabled:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->getInstance()Lcom/google/api/services/plusi/model/ShareboxSettingsJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/ShareboxSettings;

    :goto_2
    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_4

    :goto_3
    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    return-void

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    move v2, v3

    goto :goto_3
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/AccountSettingsData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private populateLocationSharingAcls(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v2, p3, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->deviceLocationAcl:Ljava/util/List;

    if-eqz v2, :cond_1

    iget-object v2, p3, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->deviceLocationAcl:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DeviceLocationAcl;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->renderedSharingRoster:Lcom/google/api/services/plusi/model/RenderedSharingRosters;

    if-eqz v2, :cond_0

    const-string v2, "BEST_AVAILABLE"

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->renderedSharingRoster:Lcom/google/api/services/plusi/model/RenderedSharingRosters;

    invoke-static {p1, p2, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertSharingRosterToAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/RenderedSharingRosters;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mBestLocationSharingAudience:Lcom/google/android/apps/plus/content/AudienceData;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private setPlusPages(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    :goto_0
    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->displayName:Ljava/lang/String;

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoUrl:Ljava/lang/String;

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getBestAvailableLocationSharingAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mBestLocationSharingAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-object v0
.end method

.method public final getNumPlusPages()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public final getPlusPageId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getPlusPageName(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getPlusPagePhotoUrl(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getShareboxSettings()Lcom/google/api/services/plusi/model/ShareboxSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    return-object v0
.end method

.method public final getUserDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserGaiaId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserPhotoUrl()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getWarmWelcomeTimestamp()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    return-object v0
.end method

.method public final isChild()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    return v0
.end method

.method public final isGooglePlus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLocationSharingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsLocationSharingEnabled:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsLocationSharingEnabled:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->getInstance()Lcom/google/api/services/plusi/model/ShareboxSettingsJson;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    invoke-virtual {v1, v4}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mNotGooglePlus:Z

    if-eqz v1, :cond_4

    :goto_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_4
.end method
