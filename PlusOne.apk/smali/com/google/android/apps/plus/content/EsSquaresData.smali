.class public final Lcom/google/android/apps/plus/content/EsSquaresData;
.super Ljava/lang/Object;
.source "EsSquaresData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;
    }
.end annotation


# static fields
.field public static final MEMBER_STATUS_PROJECTION:[Ljava/lang/String;

.field public static final SQUARES_PROJECTION:[Ljava/lang/String;

.field private static final SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

.field private static final UPDATE_SQUARE_MEMBERSHIP_PROJECTION:[Ljava/lang/String;

.field private static sMembershipActionToStatusMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sMembershipStatusMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "square_id"

    aput-object v1, v0, v3

    const-string v1, "square_name"

    aput-object v1, v0, v4

    const-string v1, "tagline"

    aput-object v1, v0, v5

    const-string v1, "photo_url"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "about_text"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "joinability"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "member_count"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_member"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "suggested"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "post_visibility"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "can_see_members"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "can_see_posts"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "can_join"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "can_request_to_join"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "can_share"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "can_invite"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "notifications_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "square_streams"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "sort_index"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "inviter_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "last_sync"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "last_members_sync"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "auto_subscribe"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "disable_subscription"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "volume"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsSquaresData;->SQUARES_PROJECTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "token"

    aput-object v1, v0, v3

    const-string v1, "member_count"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/content/EsSquaresData;->MEMBER_STATUS_PROJECTION:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "link_person_id"

    aput-object v1, v0, v3

    const-string v1, "membership_status"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/content/EsSquaresData;->SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "post_visibility"

    aput-object v1, v0, v3

    const-string v1, "joinability"

    aput-object v1, v0, v4

    const-string v1, "square_streams"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/content/EsSquaresData;->UPDATE_SQUARE_MEMBERSHIP_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public static addSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MemberList;",
            ">;)I"
        }
    .end annotation

    if-nez p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-static {v1, p3}, Lcom/google/android/apps/plus/content/EsSquaresData;->replaceSquareContactsInTransaction$40100863(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    invoke-static {v1, p2, p3}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateSquareMemberCountsAndTokens$400325ad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/MemberList;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/MemberList;->member:Ljava/util/List;

    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/google/api/services/plusi/model/MemberList;->member:Ljava/util/List;

    invoke-static {v1, p2, v4}, Lcom/google/android/apps/plus/content/EsSquaresData;->replaceSquareMembersInTransaction$285f16a0(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_SQUARE_ID_URI:Landroid/net/Uri;

    invoke-static {v5, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, 0x0

    const-string v1, "squares"

    const-string v2, "is_member=0 AND membership_status NOT IN (4,5)"

    invoke-virtual {p0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v1, "square_member_status"

    invoke-virtual {p0, v1, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const-string v1, "square_contact"

    invoke-virtual {p0, v1, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v0, Landroid/content/ContentValues;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/content/ContentValues;-><init>(I)V

    const-string v1, "last_members_sync"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "squares"

    invoke-virtual {p0, v1, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private static deleteSquareStreams(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/content/DbSquareStream;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # [Lcom/google/android/apps/plus/content/DbSquareStream;

    const/4 v6, 0x0

    if-eqz p3, :cond_0

    const/4 v0, 0x0

    array-length v1, p3

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3, v6}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x0

    invoke-static {p2, v5, v6}, Lcom/google/android/apps/plus/content/EsPostsData;->buildSquareStreamKey(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, p1, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method

.method public static dismissSquareInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "invitation_dismissed"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "squares"

    const-string v4, "square_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v1, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method public static getAllSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [Ljava/lang/String;

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v5, "sort_index,suggestion_sort_index"

    move-object v2, p2

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6
.end method

.method private static getJoinability(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "ANYONE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "REQUIRES_APPROVAL"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "REQUIRES_INVITE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getJoinedSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "is_member!=0"

    const/4 v4, 0x0

    move-object v2, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6
.end method

.method public static getMembershipStatus(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "NONE"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "OWNER"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "MODERATOR"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "MEMBER"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "PENDING"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "INVITED"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "BANNED"

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    const-string v2, "IGNORED"

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipStatusMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public static getSquareMemberCountAndToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v5, 0x0

    packed-switch p3, :pswitch_data_0

    const/4 v8, 0x3

    :goto_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "square_member_status"

    sget-object v2, Lcom/google/android/apps/plus/content/EsSquaresData;->MEMBER_STATUS_PROJECTION:[Ljava/lang/String;

    const-string v3, "square_id=? AND membership_status=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    const/4 v6, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :pswitch_0
    const/4 v8, 0x6

    goto :goto_0

    :pswitch_1
    const/4 v8, 0x5

    goto :goto_0

    :pswitch_2
    const/4 v8, 0x4

    goto :goto_0

    :pswitch_3
    const/4 v8, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_SQUARE_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    packed-switch p3, :pswitch_data_0

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x2

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    :goto_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "membership_status IN ("

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    :goto_1
    array-length v0, v4

    if-ge v8, v0, :cond_0

    const-string v0, "?,"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    const/4 v0, 0x1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v0, ")"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    move-object v2, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    return-object v7

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getVisibility(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "PUBLIC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "MEMBERS_ONLY"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static hasSquareChanged(Landroid/database/Cursor;Lcom/google/api/services/plusi/model/ViewerSquare;)Z
    .locals 35
    .param p0    # Landroid/database/Cursor;
    .param p1    # Lcom/google/api/services/plusi/model/ViewerSquare;

    const/16 v26, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/16 v26, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v26, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v26, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v26, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/16 v26, 0x6

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const/16 v26, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    const/16 v26, 0xa

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    const/16 v26, 0x19

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const/16 v26, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_1

    const/4 v13, 0x1

    :goto_0
    const/16 v26, 0xb

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_2

    const/4 v9, 0x1

    :goto_1
    const/16 v26, 0xc

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_3

    const/4 v10, 0x1

    :goto_2
    const/16 v26, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_4

    const/4 v7, 0x1

    :goto_3
    const/16 v26, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_5

    const/4 v8, 0x1

    :goto_4
    const/16 v26, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_6

    const/4 v11, 0x1

    :goto_5
    const/16 v26, 0x10

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_7

    const/4 v6, 0x1

    :goto_6
    const/16 v26, 0x11

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_8

    const/16 v19, 0x1

    :goto_7
    const/16 v26, 0x17

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_9

    const/4 v4, 0x1

    :goto_8
    const/16 v26, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    if-eqz v26, :cond_a

    const/4 v12, 0x1

    :goto_9
    const/16 v26, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v26

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/DbSquareStream;->deserialize([B)[Lcom/google/android/apps/plus/content/DbSquareStream;

    move-result-object v23

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Square;->profile:Lcom/google/api/services/plusi/model/SquareProfile;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareProfile;->name:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareProfile;->tagline:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareProfile;->photoUrl:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareProfile;->aboutText:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-eqz v26, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerMembershipStatus:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/EsSquaresData;->getMembershipStatus(Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v16

    move/from16 v1, v26

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Square;->joinability:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/EsSquaresData;->getJoinability(Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v26

    if-eq v14, v0, :cond_b

    :cond_0
    const/16 v26, 0x1

    :goto_a
    return v26

    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v10, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_4

    :cond_6
    const/4 v11, 0x0

    goto/16 :goto_5

    :cond_7
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_8
    const/16 v19, 0x0

    goto/16 :goto_7

    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_8

    :cond_a
    const/4 v12, 0x0

    goto/16 :goto_9

    :cond_b
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->calculatedMembershipProperties:Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;

    if-eqz v5, :cond_d

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->isMember:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v13, v0, :cond_c

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canSeeMemberList:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v9, v0, :cond_c

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canSeePosts:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v10, v0, :cond_c

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canJoin:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v7, v0, :cond_c

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canRequestToJoin:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v8, v0, :cond_c

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canShareSquare:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v11, v0, :cond_c

    iget-object v0, v5, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canInviteToSquare:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-eq v6, v0, :cond_d

    :cond_c
    const/16 v26, 0x1

    goto :goto_a

    :cond_d
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Square;->visibility:Lcom/google/api/services/plusi/model/SquareVisibility;

    move-object/from16 v26, v0

    if-eqz v26, :cond_e

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Square;->visibility:Lcom/google/api/services/plusi/model/SquareVisibility;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareVisibility;->posts:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/EsSquaresData;->getVisibility(Ljava/lang/String;)I

    move-result v26

    move/from16 v0, v21

    move/from16 v1, v26

    if-eq v0, v1, :cond_e

    const/16 v26, 0x1

    goto/16 :goto_a

    :cond_e
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerNotificationSettings:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_f

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerNotificationSettings:Ljava/lang/String;

    move-object/from16 v26, v0

    const-string v27, "ENABLED"

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    move/from16 v0, v19

    move/from16 v1, v26

    if-eq v0, v1, :cond_f

    const/16 v26, 0x1

    goto/16 :goto_a

    :cond_f
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareMemberStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;

    move-object/from16 v26, v0

    if-eqz v26, :cond_10

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareMemberStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;->memberCount:Ljava/lang/Integer;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v26

    move/from16 v0, v26

    if-eq v15, v0, :cond_10

    const/16 v26, 0x1

    goto/16 :goto_a

    :cond_10
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

    move-object/from16 v26, v0

    if-eqz v26, :cond_1a

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

    move-object/from16 v26, v0

    if-eqz v26, :cond_11

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquareStreamList;->squareStream:Ljava/util/List;

    move-object/from16 v27, v0

    if-eqz v27, :cond_11

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquareStreamList;->squareStream:Ljava/util/List;

    move-object/from16 v27, v0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v27

    if-nez v27, :cond_14

    :cond_11
    if-eqz v23, :cond_12

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v26, v0

    if-nez v26, :cond_13

    :cond_12
    const/16 v26, 0x1

    :goto_b
    if-nez v26, :cond_1a

    const/16 v26, 0x1

    goto/16 :goto_a

    :cond_13
    const/16 v26, 0x0

    goto :goto_b

    :cond_14
    if-nez v23, :cond_15

    const/16 v26, 0x0

    goto :goto_b

    :cond_15
    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquareStreamList;->squareStream:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v26

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v27, v0

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_16

    const/16 v26, 0x0

    goto :goto_b

    :cond_16
    const/16 v27, 0x0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v30, v0

    const/16 v26, 0x0

    move/from16 v34, v26

    move/from16 v26, v27

    move/from16 v27, v34

    :goto_c
    move/from16 v0, v27

    move/from16 v1, v30

    if-ge v0, v1, :cond_19

    aget-object v31, v23, v27

    add-int/lit8 v28, v26, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/api/services/plusi/model/SquareStream;

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareStream;->id:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v32

    if-eqz v32, :cond_17

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/content/DbSquareStream;->getName()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareStream;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v32

    if-eqz v32, :cond_17

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/content/DbSquareStream;->getDescription()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareStream;->description:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/StringUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_18

    :cond_17
    const/16 v26, 0x0

    goto :goto_b

    :cond_18
    add-int/lit8 v26, v27, 0x1

    move/from16 v27, v26

    move/from16 v26, v28

    goto :goto_c

    :cond_19
    const/16 v26, 0x1

    goto/16 :goto_b

    :cond_1a
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareNotificationOptions:Lcom/google/api/services/plusi/model/SquareNotificationOptions;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1c

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareNotificationOptions;->autoSubscribeOnJoin:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-ne v4, v0, :cond_1b

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SquareNotificationOptions;->disableSubscription:Ljava/lang/Boolean;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v26

    move/from16 v0, v26

    if-eq v12, v0, :cond_1c

    :cond_1b
    const/16 v26, 0x1

    goto/16 :goto_a

    :cond_1c
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareActivityStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

    if-eqz v3, :cond_1d

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->unreadPostCount:Ljava/lang/Integer;

    move-object/from16 v26, v0

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_1d

    const/16 v26, 0x1

    goto/16 :goto_a

    :cond_1d
    const/16 v26, 0x0

    goto/16 :goto_a
.end method

.method public static insertSquare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ViewerSquare;)Z
    .locals 16
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/ViewerSquare;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsSquaresData;->validateSquare(Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v9, 0x0

    :goto_0
    return v9

    :cond_0
    const/4 v9, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v13, v2, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v14, 0x0

    :try_start_0
    const-string v2, "squares"

    sget-object v3, Lcom/google/android/apps/plus/content/EsSquaresData;->SQUARES_PROJECTION:[Ljava/lang/String;

    const-string v4, "square_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v13, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p2

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/content/EsSquaresData;->hasSquareChanged(Landroid/database/Cursor;Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsSquaresData;->toContentValues(Lcom/google/api/services/plusi/model/ViewerSquare;)Landroid/content/ContentValues;

    move-result-object v14

    const/4 v9, 0x1

    const-string v2, "EsSquaresData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "EsSquaresData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Update square: id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Square;->profile:Lcom/google/api/services/plusi/model/SquareProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SquareProfile;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    const-string v2, "last_sync"

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "unread_count"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    if-eqz v14, :cond_5

    const-string v2, "squares"

    const-string v3, "square_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v13, v4, v5

    invoke-virtual {v1, v2, v14, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    :goto_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    :cond_4
    :try_start_3
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v14, v15

    goto :goto_1

    :catchall_0
    move-exception v2

    :try_start_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    :cond_5
    :try_start_5
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsSquaresData;->toContentValues(Lcom/google/api/services/plusi/model/ViewerSquare;)Landroid/content/ContentValues;

    move-result-object v14

    const-string v2, "last_sync"

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "squares"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v14}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const/4 v9, 0x1

    const-string v2, "EsSquaresData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "EsSquaresData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Insert square: id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Square;->profile:Lcom/google/api/services/plusi/model/SquareProfile;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SquareProfile;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method public static insertSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/List;Ljava/util/List;)I
    .locals 38
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/InvitedSquare;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/JoinedSquare;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SuggestedSquare;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v33, Ljava/util/HashMap;

    invoke-direct/range {v33 .. v33}, Ljava/util/HashMap;-><init>()V

    const/4 v7, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v25

    :goto_0
    move/from16 v0, v25

    if-ge v7, v0, :cond_2

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/api/services/plusi/model/InvitedSquare;

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->inviter:Ljava/util/List;

    if-eqz v3, :cond_1

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->inviter:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->inviter:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/SquareMember;

    move-object v6, v3

    :goto_1
    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->validateSquare(Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v10, v3, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    move-object/from16 v0, v30

    iget-object v4, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    const/4 v5, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;-><init>(Lcom/google/api/services/plusi/model/ViewerSquare;ZLcom/google/api/services/plusi/model/SquareMember;II)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v10, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v25

    :goto_2
    move/from16 v0, v25

    if-ge v7, v0, :cond_5

    move-object/from16 v0, p4

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/api/services/plusi/model/SuggestedSquare;

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SuggestedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->validateSquare(Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    move-result v3

    if-eqz v3, :cond_3

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SuggestedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SuggestedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    new-instance v8, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    move-object/from16 v0, v23

    iget-object v9, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    const/4 v10, 0x1

    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->inviter:Lcom/google/api/services/plusi/model/SquareMember;

    move-object/from16 v0, v23

    iget v12, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->sortIndex:I

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;-><init>(Lcom/google/api/services/plusi/model/ViewerSquare;ZLcom/google/api/services/plusi/model/SquareMember;II)V

    :goto_3
    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SuggestedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v3, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_4
    new-instance v8, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    move-object/from16 v0, v30

    iget-object v9, v0, Lcom/google/api/services/plusi/model/SuggestedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;-><init>(Lcom/google/api/services/plusi/model/ViewerSquare;ZLcom/google/api/services/plusi/model/SquareMember;II)V

    goto :goto_3

    :cond_5
    const/4 v7, 0x0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v25

    :goto_4
    move/from16 v0, v25

    if-ge v7, v0, :cond_7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/api/services/plusi/model/JoinedSquare;

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/JoinedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->validateSquare(Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, v30

    iget-object v3, v0, Lcom/google/api/services/plusi/model/JoinedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    new-instance v9, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    move-object/from16 v0, v30

    iget-object v10, v0, Lcom/google/api/services/plusi/model/JoinedSquare;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    move v13, v7

    invoke-direct/range {v9 .. v14}, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;-><init>(Lcom/google/api/services/plusi/model/ViewerSquare;ZLcom/google/api/services/plusi/model/SquareMember;II)V

    move-object/from16 v0, v33

    invoke-virtual {v0, v3, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_7
    const/16 v18, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v35, Ljava/util/HashMap;

    invoke-direct/range {v35 .. v35}, Ljava/util/HashMap;-><init>()V

    new-instance v34, Ljava/util/ArrayList;

    invoke-direct/range {v34 .. v34}, Ljava/util/ArrayList;-><init>()V

    const-string v12, "is_member!=0 OR suggested!=0 OR membership_status IN (5,4)"

    const-string v10, "squares"

    sget-object v11, Lcom/google/android/apps/plus/content/EsSquaresData;->SQUARES_PROJECTION:[Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v9 .. v16}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v19

    :goto_5
    :try_start_1
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_f

    const/4 v3, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    if-nez v31, :cond_8

    move-object/from16 v0, v34

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v3

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    :cond_8
    :try_start_3
    move-object/from16 v0, v31

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    move-object/from16 v0, v19

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->hasSquareChanged(Landroid/database/Cursor;Lcom/google/api/services/plusi/model/ViewerSquare;)Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x1

    :goto_6
    if-eqz v3, :cond_e

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/content/EsSquaresData;->toContentValues(Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;)Landroid/content/ContentValues;

    move-result-object v37

    const-string v3, "EsSquaresData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "EsSquaresData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Update square: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "square_name"

    move-object/from16 v0, v37

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_7
    move-object/from16 v0, v35

    move-object/from16 v1, v32

    move-object/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_a
    const/16 v3, 0x9

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_c

    const/4 v3, 0x1

    :goto_8
    const/16 v4, 0x14

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v31

    iget-boolean v5, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->suggested:Z

    if-ne v3, v5, :cond_b

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->getInviterGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    :cond_b
    const/4 v3, 0x1

    goto :goto_6

    :cond_c
    const/4 v3, 0x0

    goto :goto_8

    :cond_d
    const/4 v3, 0x0

    goto :goto_6

    :cond_e
    new-instance v37, Landroid/content/ContentValues;

    const/4 v3, 0x2

    move-object/from16 v0, v37

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "sort_index"

    move-object/from16 v0, v31

    iget v4, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->sortIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "suggestion_sort_index"

    move-object/from16 v0, v31

    iget v4, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->suggestionSortIndex:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_7

    :cond_f
    :try_start_4
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_11

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v3, "square_id IN ("

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v7, 0x0

    :goto_9
    move/from16 v0, v28

    if-ge v7, v0, :cond_10

    const-string v3, "?,"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v34

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v17, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    :cond_10
    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v3, ")"

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "squares"

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v9, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v18, v28, 0x0

    const-string v3, "EsSquaresData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_11

    const-string v3, "EsSquaresData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Delete "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " squares"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    invoke-virtual/range {v35 .. v35}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_a
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/util/Map$Entry;

    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/String;

    const-string v4, "squares"

    invoke-interface/range {v21 .. v21}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    const-string v5, "square_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object v32, v10, v11

    invoke-virtual {v9, v4, v3, v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v18, v18, 0x1

    goto :goto_a

    :cond_12
    invoke-virtual/range {v33 .. v33}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_13
    :goto_b
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;

    const-string v3, "squares"

    const/4 v4, 0x0

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/content/EsSquaresData;->toContentValues(Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;)Landroid/content/ContentValues;

    move-result-object v5

    const/4 v10, 0x5

    invoke-virtual {v9, v3, v4, v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    add-int/lit8 v18, v18, 0x1

    const-string v3, "EsSquaresData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_13

    const-string v3, "EsSquaresData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Insert square: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " name="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v31

    iget-object v5, v0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Square;->profile:Lcom/google/api/services/plusi/model/SquareProfile;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SquareProfile;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    :cond_14
    new-instance v36, Ljava/util/ArrayList;

    invoke-direct/range {v36 .. v36}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_15
    :goto_c
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/api/services/plusi/model/InvitedSquare;

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->inviter:Ljava/util/List;

    if-eqz v3, :cond_15

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->inviter:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_15

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/api/services/plusi/model/InvitedSquare;->inviter:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/SquareMember;

    new-instance v20, Lcom/google/api/services/plusi/model/DataUser;

    invoke-direct/range {v20 .. v20}, Lcom/google/api/services/plusi/model/DataUser;-><init>()V

    iget-object v3, v6, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/api/services/plusi/model/SquareMember;->photoUrl:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->profilePhotoUrl:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/api/services/plusi/model/SquareMember;->displayName:Ljava/lang/String;

    move-object/from16 v0, v20

    iput-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    move-object/from16 v0, v36

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    :cond_16
    move-object/from16 v0, v36

    invoke-static {v9, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-virtual {v9}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "last_squares_sync_time"

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v4, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v5, "account_status"

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v3, v5, v4, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    if-eqz v18, :cond_17

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_17
    return v18
.end method

.method public static queryLastSquareMemberSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)J
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    :try_start_0
    const-string v1, "SELECT last_members_sync  FROM squares WHERE square_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method public static queryLastSquaresSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    :try_start_0
    const-string v1, "SELECT last_squares_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v1

    :goto_0
    return-wide v1

    :catch_0
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private static replaceSquareContactsInTransaction$40100863(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MemberList;",
            ">;)V"
        }
    .end annotation

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/MemberList;

    iget-object v4, v3, Lcom/google/api/services/plusi/model/MemberList;->member:Ljava/util/List;

    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/SquareMember;

    new-instance v0, Lcom/google/api/services/plusi/model/DataUser;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataUser;-><init>()V

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SquareMember;->photoUrl:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/api/services/plusi/model/DataUser;->profilePhotoUrl:Ljava/lang/String;

    iget-object v7, v5, Lcom/google/api/services/plusi/model/SquareMember;->displayName:Ljava/lang/String;

    iput-object v7, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    return-void
.end method

.method public static replaceSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I
    .locals 29
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MemberList;",
            ">;)I"
        }
    .end annotation

    if-nez p3, :cond_0

    const/4 v12, 0x0

    :goto_0
    return v12

    :cond_0
    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/MemberList;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MemberList;->member:Ljava/util/List;

    move-object/from16 v24, v0

    if-eqz v24, :cond_1

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/google/api/services/plusi/model/SquareMember;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    iget-object v4, v0, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    const/4 v12, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "square_contact"

    sget-object v4, Lcom/google/android/apps/plus/content/EsSquaresData;->SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

    const-string v5, "link_square_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xc9

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/16 v4, 0xc8

    if-le v3, v4, :cond_4

    const-string v3, "square_contact"

    const-string v4, "link_square_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v12, v3, 0x0

    :cond_3
    :goto_2
    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsSquaresData;->replaceSquareContactsInTransaction$40100863(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateSquareMemberCountsAndTokens$400325ad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual/range {v26 .. v26}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->replaceSquareMembersInTransaction$285f16a0(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)I

    move-result v3

    add-int/2addr v12, v3

    new-instance v28, Landroid/content/ContentValues;

    const/4 v3, 0x1

    move-object/from16 v0, v28

    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    const-string v3, "last_members_sync"

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "squares"

    const-string v4, "square_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v5, v6

    move-object/from16 v0, v28

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_SQUARE_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    :cond_4
    :goto_3
    :try_start_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/google/api/services/plusi/model/SquareMember;

    if-nez v25, :cond_5

    move-object/from16 v0, v27

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v3

    :try_start_2
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    :cond_5
    const/4 v3, 0x1

    :try_start_3
    invoke-interface {v13, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SquareMember;->membershipStatus:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->getMembershipStatus(Ljava/lang/String;)I

    move-result v3

    move/from16 v0, v18

    if-ne v0, v3, :cond_4

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :cond_6
    :try_start_4
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v21

    add-int/lit8 v3, v21, 0x1

    new-array v11, v3, [Ljava/lang/String;

    const-string v3, "link_square_id=? AND "

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "link_person_id IN ("

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    aput-object p2, v11, v3

    const/4 v14, 0x0

    :goto_4
    move/from16 v0, v21

    if-ge v14, v0, :cond_7

    const-string v3, "?,"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v14, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v11, v4

    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v3, ")"

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "square_contact"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v11}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v12, v3, 0x0

    const-string v3, "EsSquaresData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "EsSquaresData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Delete "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " square members"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_2
.end method

.method private static replaceSquareMembersInTransaction$285f16a0(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/Collection;)I
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/api/services/plusi/model/SquareMember;",
            ">;)I"
        }
    .end annotation

    const/4 v7, 0x3

    const/4 v0, 0x0

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v7}, Landroid/content/ContentValues;-><init>(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/SquareMember;

    const-string v4, "link_square_id"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "link_person_id"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "g:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "membership_status"

    iget-object v5, v2, Lcom/google/api/services/plusi/model/SquareMember;->membershipStatus:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsSquaresData;->getMembershipStatus(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "square_contact"

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-virtual {p0, v4, v5, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    add-int/lit8 v0, v0, 0x1

    const-string v4, "EsSquaresData"

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsSquaresData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Insert user: id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " name="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/google/api/services/plusi/model/SquareMember;->displayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static setSquareVolume(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/VolumeSettings;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "notifications_enabled"

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/VolumeSettings;->getNotificationsEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "volume"

    invoke-virtual {p3}, Lcom/google/android/apps/plus/content/VolumeSettings;->getVolume()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "squares"

    const-string v5, "square_id=?"

    new-array v3, v3, [Ljava/lang/String;

    aput-object p2, v3, v4

    invoke-virtual {v0, v2, v1, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void

    :cond_0
    move v2, v4

    goto :goto_0
.end method

.method private static toContentValues(Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;)Landroid/content/ContentValues;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsSquaresData;->toContentValues(Lcom/google/api/services/plusi/model/ViewerSquare;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "inviter_gaia_id"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->getInviterGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "suggested"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->suggested:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "sort_index"

    iget v2, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->sortIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "suggestion_sort_index"

    iget v2, p0, Lcom/google/android/apps/plus/content/EsSquaresData$SquareData;->suggestionSortIndex:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static toContentValues(Lcom/google/api/services/plusi/model/ViewerSquare;)Landroid/content/ContentValues;
    .locals 14
    .param p0    # Lcom/google/api/services/plusi/model/ViewerSquare;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v5, v10, Lcom/google/api/services/plusi/model/Square;->profile:Lcom/google/api/services/plusi/model/SquareProfile;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->calculatedMembershipProperties:Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;

    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerMembershipStatus:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsSquaresData;->getMembershipStatus(Ljava/lang/String;)I

    move-result v3

    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    const-string v10, "square_id"

    iget-object v11, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "square_name"

    iget-object v11, v5, Lcom/google/api/services/plusi/model/SquareProfile;->name:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "tagline"

    iget-object v11, v5, Lcom/google/api/services/plusi/model/SquareProfile;->tagline:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "photo_url"

    iget-object v11, v5, Lcom/google/api/services/plusi/model/SquareProfile;->photoUrl:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "about_text"

    iget-object v11, v5, Lcom/google/api/services/plusi/model/SquareProfile;->aboutText:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "joinability"

    iget-object v11, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/Square;->joinability:Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsSquaresData;->getJoinability(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "membership_status"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/Square;->visibility:Lcom/google/api/services/plusi/model/SquareVisibility;

    if-eqz v10, :cond_0

    const-string v10, "post_visibility"

    iget-object v11, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/Square;->visibility:Lcom/google/api/services/plusi/model/SquareVisibility;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/SquareVisibility;->posts:Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsSquaresData;->getVisibility(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    if-eqz v1, :cond_a

    const-string v11, "is_member"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->isMember:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v10, 0x1

    :goto_0
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "can_see_members"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canSeeMemberList:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v10, 0x1

    :goto_1
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "can_see_posts"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canSeePosts:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v10, 0x1

    :goto_2
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "can_join"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canJoin:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v10, 0x1

    :goto_3
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "can_request_to_join"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canRequestToJoin:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v10, 0x1

    :goto_4
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "can_share"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canShareSquare:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_8

    const/4 v10, 0x1

    :goto_5
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "can_invite"

    iget-object v10, v1, Lcom/google/api/services/plusi/model/ViewerSquareCalculatedMembershipProperties;->canInviteToSquare:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_9

    const/4 v10, 0x1

    :goto_6
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_7
    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareMemberStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;

    if-eqz v10, :cond_1

    const-string v10, "member_count"

    iget-object v11, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareMemberStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/ViewerSquareSquareMemberStats;->memberCount:Ljava/lang/Integer;

    invoke-static {v11}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_1
    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerNotificationSettings:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v11, "notifications_enabled"

    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->viewerNotificationSettings:Ljava/lang/String;

    const-string v12, "ENABLED"

    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    const/4 v10, 0x1

    :goto_8
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/ViewerSquareStreamList;->squareStream:Ljava/util/List;

    if-eqz v10, :cond_f

    iget-object v10, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->streams:Lcom/google/api/services/plusi/model/ViewerSquareStreamList;

    iget-object v8, v10, Lcom/google/api/services/plusi/model/ViewerSquareStreamList;->squareStream:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v10

    new-array v6, v10, [Lcom/google/android/apps/plus/content/DbSquareStream;

    const/4 v2, 0x0

    :goto_9
    array-length v10, v6

    if-ge v2, v10, :cond_e

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/plusi/model/SquareStream;

    new-instance v10, Lcom/google/android/apps/plus/content/DbSquareStream;

    iget-object v11, v7, Lcom/google/api/services/plusi/model/SquareStream;->id:Ljava/lang/String;

    iget-object v12, v7, Lcom/google/api/services/plusi/model/SquareStream;->name:Ljava/lang/String;

    iget-object v13, v7, Lcom/google/api/services/plusi/model/SquareStream;->description:Ljava/lang/String;

    invoke-direct {v10, v11, v12, v13}, Lcom/google/android/apps/plus/content/DbSquareStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v10, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_3
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_3

    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_4

    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_5

    :cond_9
    const/4 v10, 0x0

    goto :goto_6

    :cond_a
    const-string v11, "is_member"

    const/4 v10, 0x3

    if-eq v3, v10, :cond_b

    const/4 v10, 0x2

    if-eq v3, v10, :cond_b

    const/4 v10, 0x1

    if-ne v3, v10, :cond_c

    :cond_b
    const/4 v10, 0x1

    :goto_a
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_7

    :cond_c
    const/4 v10, 0x0

    goto :goto_a

    :cond_d
    const/4 v10, 0x0

    goto :goto_8

    :cond_e
    const-string v10, "square_streams"

    invoke-static {v6}, Lcom/google/android/apps/plus/content/DbSquareStream;->serialize([Lcom/google/android/apps/plus/content/DbSquareStream;)[B

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_f
    iget-object v4, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareNotificationOptions:Lcom/google/api/services/plusi/model/SquareNotificationOptions;

    if-eqz v4, :cond_10

    const-string v11, "auto_subscribe"

    iget-object v10, v4, Lcom/google/api/services/plusi/model/SquareNotificationOptions;->autoSubscribeOnJoin:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_12

    const/4 v10, 0x1

    :goto_b
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v11, "disable_subscription"

    iget-object v10, v4, Lcom/google/api/services/plusi/model/SquareNotificationOptions;->disableSubscription:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_13

    const/4 v10, 0x1

    :goto_c
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v11, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_10
    iget-object v0, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->squareActivityStats:Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;

    if-eqz v0, :cond_11

    const-string v10, "unread_count"

    iget-object v11, v0, Lcom/google/api/services/plusi/model/ViewerSquareSquareActivityStats;->unreadPostCount:Ljava/lang/Integer;

    invoke-static {v11}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_11
    return-object v9

    :cond_12
    const/4 v10, 0x0

    goto :goto_b

    :cond_13
    const/4 v10, 0x0

    goto :goto_c
.end method

.method private static updateMemberCountInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I

    const/4 v0, 0x1

    if-ne p4, v0, :cond_1

    const/4 v4, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateMemberCountInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    packed-switch p4, :pswitch_data_0

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p4, v0, :cond_0

    const/4 v4, 0x3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateMemberCountInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    goto :goto_0

    :pswitch_0
    const-string v0, "UPDATE square_member_status SET member_count = member_count + ? WHERE square_id=? AND membership_status=?"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    int-to-long v3, p5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    int-to-long v3, p4

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private static updateSquareMemberCountsAndTokens$400325ad(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;)V
    .locals 10
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MemberList;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/MemberList;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MemberList;->member:Ljava/util/List;

    const/4 v0, 0x0

    const/4 v5, 0x0

    if-eqz v4, :cond_0

    iget-object v7, v2, Lcom/google/api/services/plusi/model/MemberList;->totalMembers:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    iget-object v8, v2, Lcom/google/api/services/plusi/model/MemberList;->totalMembers:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-ge v7, v8, :cond_0

    iget-object v0, v2, Lcom/google/api/services/plusi/model/MemberList;->continuationToken:Ljava/lang/String;

    :cond_0
    iget-object v7, v2, Lcom/google/api/services/plusi/model/MemberList;->membershipStatus:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsSquaresData;->getMembershipStatus(Ljava/lang/String;)I

    move-result v3

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "square_id"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "membership_status"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "token"

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "member_count"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "square_member_status"

    const/4 v8, 0x0

    const/4 v9, 0x5

    invoke-virtual {p0, v7, v8, v6, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static updateSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 19
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "JOIN"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "JOIN_WITH_SUBSCRIPTION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ACCEPT_INVITATION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "ACCEPT_INVITATION_WITH_SUBSCRIPTION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_0
    const-string v4, "JOIN_WITH_SUBSCRIPTION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "ACCEPT_INVITATION_WITH_SUBSCRIPTION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_1
    const/16 v17, 0x1

    :goto_0
    const-string v4, "membership_status"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "is_member"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_see_members"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_see_posts"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_join"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_request_to_join"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "notifications_enabled"

    if-eqz v17, :cond_6

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v11, 0x1

    :cond_2
    :goto_2
    if-eqz v11, :cond_3

    const-string v4, "squares"

    const-string v5, "square_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p2, v6, v7

    move-object/from16 v0, v18

    invoke-virtual {v3, v4, v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    if-eqz v11, :cond_4

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_4
    return-void

    :cond_5
    const/16 v17, 0x0

    goto :goto_0

    :cond_6
    const/4 v4, 0x0

    goto :goto_1

    :cond_7
    :try_start_1
    const-string v4, "APPLY_TO_JOIN"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "APPLY_TO_JOIN_WITH_SUBSCRIPTION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_8
    const-string v4, "membership_status"

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_request_to_join"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v11, 0x1

    goto :goto_2

    :cond_9
    const-string v4, "CANCEL_JOIN_REQUEST"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "membership_status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_request_to_join"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v11, 0x1

    goto :goto_2

    :cond_a
    const-string v4, "SUBSCRIBE"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "notifications_enabled"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v11, 0x1

    goto/16 :goto_2

    :cond_b
    const-string v4, "UNSUBSCRIBE"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "notifications_enabled"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v11, 0x1

    goto/16 :goto_2

    :cond_c
    const-string v4, "LEAVE"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    const-string v4, "DECLINE_INVITATION"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_d
    const/4 v14, -0x1

    const/4 v13, 0x0

    const/4 v15, 0x0

    const-string v4, "squares"

    sget-object v5, Lcom/google/android/apps/plus/content/EsSquaresData;->UPDATE_SQUARE_MEMBERSHIP_PROJECTION:[Ljava/lang/String;

    const-string v6, "square_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v12

    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_e

    const/4 v4, 0x0

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-nez v4, :cond_10

    const/4 v13, 0x1

    :goto_3
    const/4 v4, 0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/4 v4, 0x2

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbSquareStream;->deserialize([B)[Lcom/google/android/apps/plus/content/DbSquareStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v15

    :cond_e
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    const-string v4, "membership_status"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "is_member"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "can_see_members"

    if-eqz v13, :cond_11

    const/4 v4, 0x1

    :goto_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "can_see_posts"

    if-eqz v13, :cond_12

    const/4 v4, 0x1

    :goto_5
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "can_join"

    if-nez v14, :cond_13

    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "can_request_to_join"

    const/4 v4, 0x1

    if-ne v14, v4, :cond_14

    const/4 v4, 0x1

    :goto_7
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_share"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "can_invite"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    if-nez v13, :cond_f

    const-string v4, "LEAVE"

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2, v15}, Lcom/google/android/apps/plus/content/EsSquaresData;->deleteSquareStreams(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/content/DbSquareStream;)V

    const-string v4, "square_streams"

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_f
    const/4 v11, 0x1

    goto/16 :goto_2

    :cond_10
    const/4 v13, 0x0

    goto/16 :goto_3

    :catchall_0
    move-exception v4

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v4

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    :cond_11
    const/4 v4, 0x0

    goto :goto_4

    :cond_12
    const/4 v4, 0x0

    goto :goto_5

    :cond_13
    const/4 v4, 0x0

    goto :goto_6

    :cond_14
    const/4 v4, 0x0

    goto :goto_7
.end method

.method public static updateTargetMembershipStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 23
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipActionToStatusMap:Ljava/util/HashMap;

    if-nez v3, :cond_0

    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    const-string v3, "APPROVE_JOIN_REQUEST"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "IGNORE"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "REMOVE_MEMBER_FROM_SQUARE"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "BAN"

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "REMOVE_BAN"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "PROMOTE_MEMBER_TO_MODERATOR"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "PROMOTE_MODERATOR_TO_OWNER"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "DEMOTE_OWNER_TO_MODERATOR"

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "DEMOTE_MODERATOR_TO_MEMBER"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "DEMOTE_OWNER_TO_MEMBER"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "CANCEL_INVITATION"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sput-object v19, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipActionToStatusMap:Ljava/util/HashMap;

    :cond_0
    sget-object v3, Lcom/google/android/apps/plus/content/EsSquaresData;->sMembershipActionToStatusMap:Ljava/util/HashMap;

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v16

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    const/16 v20, 0x0

    const/4 v11, -0x1

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v5, "link_square_id=? AND link_person_id=?"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v6, v3

    const/4 v3, 0x1

    aput-object v21, v6, v3

    const-string v3, "square_contact"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "membership_status"

    aput-object v8, v4, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    if-eqz v18, :cond_1

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    :cond_1
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "membership_status"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "square_contact"

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    const-string v3, "membership_status"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "squares"

    const-string v4, "square_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v4, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    const/16 v20, 0x1

    :cond_2
    const/4 v12, -0x1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object v9, v2

    move-object/from16 v10, p2

    invoke-static/range {v7 .. v12}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateMemberCountInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    const/16 v17, 0x1

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object v14, v2

    move-object/from16 v15, p2

    invoke-static/range {v12 .. v17}, Lcom/google/android/apps/plus/content/EsSquaresData;->updateMemberCountInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;II)V

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_SQUARE_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    if-eqz v20, :cond_3

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_3
    return-void

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private static validateSquare(Lcom/google/api/services/plusi/model/ViewerSquare;)Z
    .locals 3
    .param p0    # Lcom/google/api/services/plusi/model/ViewerSquare;

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Square;->profile:Lcom/google/api/services/plusi/model/SquareProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/api/services/plusi/model/ViewerSquare;->square:Lcom/google/api/services/plusi/model/Square;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Square;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "EsSquaresData"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EsSquaresData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid ViewerSquare:\n"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/api/services/plusi/model/ViewerSquareJson;->getInstance()Lcom/google/api/services/plusi/model/ViewerSquareJson;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/api/services/plusi/model/ViewerSquareJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
