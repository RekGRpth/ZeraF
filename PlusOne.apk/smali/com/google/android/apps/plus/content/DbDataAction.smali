.class public final Lcom/google/android/apps/plus/content/DbDataAction;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbDataAction.java"


# direct methods
.method public static deserializeDataActionList([B)Ljava/util/List;
    .locals 5
    .param p0    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAction;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 v3, 0x0

    :cond_0
    return-object v3

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataAction;->getDataAction(Ljava/nio/ByteBuffer;)Lcom/google/api/services/plusi/model/DataAction;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static deserializeDataActorList([B)Ljava/util/List;
    .locals 5
    .param p0    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 v3, 0x0

    :cond_0
    return-object v3

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataAction;->getDataActor(Ljava/nio/ByteBuffer;)Lcom/google/api/services/plusi/model/DataActor;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static getDataAction(Ljava/nio/ByteBuffer;)Lcom/google/api/services/plusi/model/DataAction;
    .locals 6
    .param p0    # Ljava/nio/ByteBuffer;

    new-instance v0, Lcom/google/api/services/plusi/model/DataAction;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataAction;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAction;->type:Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    new-instance v4, Lcom/google/api/services/plusi/model/DataItem;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/DataItem;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataItem;->id:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataItem;->notificationType:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getDataActor(Ljava/nio/ByteBuffer;)Lcom/google/api/services/plusi/model/DataActor;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    return-object v0
.end method

.method private static getDataActor(Ljava/nio/ByteBuffer;)Lcom/google/api/services/plusi/model/DataActor;
    .locals 2
    .param p0    # Ljava/nio/ByteBuffer;

    new-instance v0, Lcom/google/api/services/plusi/model/DataActor;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataActor;-><init>()V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataActor;->gender:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataActor;->profileType:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/DbDataAction;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataActor;->profileUrl:Ljava/lang/String;

    return-object v0
.end method

.method public static getDataActorList(Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAction;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    return-object v3

    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataAction;

    if-eqz v0, :cond_2

    iget-object v8, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    if-eqz v8, :cond_2

    iget-object v8, v0, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataItem;

    if-eqz v4, :cond_3

    iget-object v8, v4, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v8, :cond_3

    iget-object v1, v4, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static putDataAction(Ljava/io/DataOutputStream;Lcom/google/api/services/plusi/model/DataAction;)V
    .locals 3
    .param p0    # Ljava/io/DataOutputStream;
    .param p1    # Lcom/google/api/services/plusi/model/DataAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataAction;->type:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataItem;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataItem;->id:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataItem;->notificationType:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putDataActor(Ljava/io/DataOutputStream;Lcom/google/api/services/plusi/model/DataActor;)V

    goto :goto_0
.end method

.method private static putDataActor(Ljava/io/DataOutputStream;Lcom/google/api/services/plusi/model/DataActor;)V
    .locals 1
    .param p0    # Ljava/io/DataOutputStream;
    .param p1    # Lcom/google/api/services/plusi/model/DataActor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataActor;->gender:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataActor;->profileType:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataActor;->profileUrl:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    return-void
.end method

.method public static serializeDataActionList(Ljava/util/List;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAction;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    if-nez p0, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataAction;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putDataAction(Ljava/io/DataOutputStream;Lcom/google/api/services/plusi/model/DataAction;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0
.end method

.method public static serializeDataActorList(Ljava/util/List;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    if-nez p0, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataActor;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/DbDataAction;->putDataActor(Ljava/io/DataOutputStream;Lcom/google/api/services/plusi/model/DataActor;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0
.end method
