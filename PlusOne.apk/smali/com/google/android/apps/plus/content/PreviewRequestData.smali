.class public final Lcom/google/android/apps/plus/content/PreviewRequestData;
.super Ljava/lang/Object;
.source "PreviewRequestData.java"


# instance fields
.field public final callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

.field public final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/api/CallToActionData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    return-void
.end method

.method public static fromSelectionArg(Ljava/lang/String;)Lcom/google/android/apps/plus/content/PreviewRequestData;
    .locals 7
    .param p0    # Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_0

    move-object v1, v3

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-le v4, v5, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/api/CallToActionData;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v6, v3

    :goto_1
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v5, v3

    :goto_2
    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v4, v3

    :goto_3
    invoke-direct {v0, v6, v5, v4}, Lcom/google/android/apps/plus/api/CallToActionData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/content/PreviewRequestData;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v4, v3

    :goto_4
    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/plus/content/PreviewRequestData;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;)V

    goto :goto_0

    :cond_2
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v6, v4

    goto :goto_1

    :cond_3
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v4

    goto :goto_2

    :cond_4
    const/4 v4, 0x3

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_4

    :catch_0
    move-exception v4

    const-string v4, "PreviewRequestData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "PreviewRequestData"

    const-string v5, "Failed to deserialize PreviewRequestData JSON."

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move-object v1, v3

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/apps/plus/content/PreviewRequestData;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/content/PreviewRequestData;

    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v4, v0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v4, v0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/api/CallToActionData;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-nez v3, :cond_1

    :goto_1
    add-int v0, v1, v2

    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/CallToActionData;->hashCode()I

    move-result v2

    goto :goto_1
.end method

.method public final toSelectionArg()Ljava/lang/String;
    .locals 2

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/CallToActionData;->mLabel:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/CallToActionData;->mLabel:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/CallToActionData;->mUrl:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/CallToActionData;->mUrl:Ljava/lang/String;

    :goto_1
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/CallToActionData;->mDeepLinkId:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/content/PreviewRequestData;->callToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/CallToActionData;->mDeepLinkId:Ljava/lang/String;

    :goto_2
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    sget-object v1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object v1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    sget-object v1, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    goto :goto_2
.end method
