.class public final Lcom/google/android/apps/plus/content/EsMediaCache;
.super Ljava/lang/Object;
.source "EsMediaCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;
    }
.end annotation


# static fields
.field private static sMaxCacheSize:J

.field private static sMediaCacheDir:Ljava/io/File;


# direct methods
.method public static buildShortFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;

    const-wide v0, 0x3ffffffffffe5L

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    const-wide/16 v4, 0x1f

    mul-long/2addr v4, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-long v6, v6

    add-long v0, v4, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x4

    shr-long v4, v0, v4

    const-wide v6, 0xfffffffffffffffL

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static cleanup()V
    .locals 16

    sget-object v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    if-nez v12, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-object v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v12, v2

    if-eqz v12, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v10, 0x0

    const/4 v5, 0x0

    :goto_0
    array-length v12, v2

    if-ge v5, v12, :cond_3

    aget-object v12, v2, v5

    invoke-virtual {v12}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    const/4 v6, 0x0

    :goto_1
    array-length v12, v4

    if-ge v6, v12, :cond_2

    new-instance v3, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;

    aget-object v12, v4, v6

    invoke-direct {v3, v12, v8, v9}, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;-><init>(Ljava/io/File;J)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v12, v3, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->size:J

    add-long/2addr v10, v12

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    sget-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    const-wide/16 v14, 0x0

    cmp-long v12, v12, v14

    if-nez v12, :cond_5

    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v13, 0x9

    if-lt v12, v13, :cond_6

    sget-object v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getUsableSpace()J

    move-result-wide v12

    add-long/2addr v12, v10

    long-to-double v12, v12

    const-wide/high16 v14, 0x3fd0000000000000L

    mul-double/2addr v12, v14

    double-to-long v12, v12

    sput-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    const-wide/32 v14, 0x500000

    cmp-long v12, v12, v14

    if-gez v12, :cond_4

    const-wide/32 v12, 0x500000

    sput-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    :cond_4
    sget-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    const-wide/32 v14, 0x6400000

    cmp-long v12, v12, v14

    if-lez v12, :cond_5

    const-wide/32 v12, 0x6400000

    sput-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    :cond_5
    :goto_2
    sget-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    cmp-long v12, v10, v12

    if-lez v12, :cond_0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const-string v12, "ResourceCache"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_8

    const-string v12, "ResourceCache"

    const-string v13, "Media cache"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v1, :cond_8

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;

    const-string v13, "ResourceCache"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v12, v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->recent:Z

    if-eqz v12, :cond_7

    const-string v12, "R "

    :goto_4
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->timestamp:J

    sub-long v14, v8, v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " ms, "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-wide v14, v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->size:J

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " bytes"

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v13, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    const-wide/32 v12, 0xf00000

    sput-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    goto :goto_2

    :cond_7
    const-string v12, "  "

    goto :goto_4

    :cond_8
    const/4 v5, 0x0

    :goto_5
    if-ge v5, v1, :cond_0

    sget-wide v12, Lcom/google/android/apps/plus/content/EsMediaCache;->sMaxCacheSize:J

    cmp-long v12, v10, v12

    if-lez v12, :cond_0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;

    iget-object v3, v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v12

    if-eqz v12, :cond_9

    iget-wide v12, v7, Lcom/google/android/apps/plus/content/EsMediaCache$MediaCacheFile;->size:J

    sub-long/2addr v10, v12

    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_5
.end method

.method public static cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;
    .locals 13
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # F
    .param p2    # F

    const/high16 v12, 0x40000000

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    int-to-float v6, v11

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    int-to-float v5, v11

    div-float v7, v6, v5

    div-float v0, p1, p2

    cmpl-float v11, v7, v0

    if-lez v11, :cond_0

    mul-float v11, v6, p2

    div-float/2addr v11, v5

    float-to-int v2, v11

    float-to-int v1, p2

    :goto_0
    const/4 v11, 0x1

    invoke-static {p0, v2, v1, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    int-to-float v11, v11

    sub-float v4, v11, p1

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    int-to-float v11, v11

    sub-float v3, v11, p2

    div-float v11, v4, v12

    float-to-int v9, v11

    div-float v11, v3, v12

    float-to-int v10, v11

    float-to-int v11, p1

    float-to-int v12, p2

    invoke-static {v8, v9, v10, v11, v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v11

    :goto_1
    return-object v11

    :cond_0
    mul-float v11, v5, p1

    div-float/2addr v11, v6

    float-to-int v1, v11

    float-to-int v2, p1

    goto :goto_0

    :cond_1
    const/4 v11, 0x0

    goto :goto_1
.end method

.method public static exists(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private static getMediaCacheDir(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheRootDir(Landroid/content/Context;)Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getMediaCacheFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheDir(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getMediaCacheRootDir(Landroid/content/Context;)Ljava/io/File;
    .locals 4
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    if-nez v1, :cond_0

    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "media"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "ResourceCache"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot create cache media directory: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/apps/plus/content/EsMediaCache;->sMediaCacheDir:Ljava/io/File;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static obtainAvatarForMultipleUsers(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    if-eqz p0, :cond_0

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v12

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v16

    invoke-static {v0, v10, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v11

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v11}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    const/high16 v2, -0x1000000

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v2, 0x40000000

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v2, 0x1

    if-ne v12, v2, :cond_2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne v12, v2, :cond_4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeight(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v15

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeight(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v15, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v9, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    move/from16 v0, v16

    int-to-float v4, v0

    div-int/lit8 v5, v10, 0x2

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_3
    :goto_1
    move-object v2, v11

    goto :goto_0

    :cond_4
    const/4 v2, 0x3

    if-ne v12, v2, :cond_5

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeight(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v15

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v15, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v7, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v2, v16, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v8, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v2, v16, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, v16, 0x2

    int-to-float v4, v4

    int-to-float v5, v10

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    move/from16 v0, v16

    int-to-float v4, v0

    div-int/lit8 v5, v10, 0x2

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_5
    const/4 v2, 0x4

    if-lt v12, v2, :cond_3

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v13

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v14

    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v7

    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v13, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v2, v16, 0x2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {v1, v14, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v7, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v2, v16, 0x2

    int-to-float v2, v2

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    invoke-virtual {v1, v8, v2, v3, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v2, v16, 0x2

    int-to-float v2, v2

    const/4 v3, 0x0

    div-int/lit8 v4, v16, 0x2

    int-to-float v4, v4

    int-to-float v5, v10

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const/4 v2, 0x0

    div-int/lit8 v3, v10, 0x2

    int-to-float v3, v3

    move/from16 v0, v16

    int-to-float v4, v0

    div-int/lit8 v5, v10, 0x2

    int-to-float v5, v5

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method private static obtainAvatarWithHalfHeight(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0    # Landroid/graphics/Bitmap;

    if-nez p0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    const/4 v2, 0x0

    div-int/lit8 v3, v0, 0x4

    div-int/lit8 v4, v0, 0x2

    invoke-static {p0, v2, v3, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method private static obtainAvatarWithHalfHeightAndHalfWidth(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0    # Landroid/graphics/Bitmap;

    if-nez p0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    div-int/lit8 v2, v1, 0x2

    div-int/lit8 v3, v0, 0x2

    const/4 v4, 0x0

    invoke-static {p0, v2, v3, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method

.method public static read(Landroid/content/Context;Ljava/lang/String;)[B
    .locals 15
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    const/4 v9, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v11

    sub-long v11, v5, v11

    const-wide/16 v13, 0x2710

    cmp-long v11, v11, v13

    if-lez v11, :cond_0

    invoke-virtual {v4, v5, v6}, Ljava/io/File;->setLastModified(J)Z

    :cond_0
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v11

    long-to-int v8, v11

    new-array v1, v8, [B

    const/4 v7, 0x0

    move v0, v8

    :goto_0
    if-lez v0, :cond_3

    invoke-virtual {v10, v1, v7, v0}, Ljava/io/FileInputStream;->read([BII)I

    move-result v2

    if-gez v2, :cond_2

    new-instance v11, Ljava/io/IOException;

    invoke-direct {v11}, Ljava/io/IOException;-><init>()V

    throw v11
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catch_0
    move-exception v3

    move-object v9, v10

    :goto_1
    if-eqz v9, :cond_1

    :try_start_2
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_1
    :goto_2
    const/4 v1, 0x0

    :goto_3
    return-object v1

    :cond_2
    add-int/2addr v7, v2

    sub-int/2addr v0, v2

    goto :goto_0

    :cond_3
    :try_start_3
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    move-object v9, v10

    goto :goto_3

    :catch_1
    move-exception v3

    :goto_5
    :try_start_4
    const-string v11, "ResourceCache"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Cannot read file from cache: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v9, :cond_4

    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_4
    :goto_6
    const/4 v1, 0x0

    goto :goto_3

    :catchall_0
    move-exception v11

    :goto_7
    if-eqz v9, :cond_5

    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    :cond_5
    :goto_8
    throw v11

    :catch_2
    move-exception v11

    goto :goto_4

    :catch_3
    move-exception v11

    goto :goto_2

    :catch_4
    move-exception v11

    goto :goto_6

    :catch_5
    move-exception v12

    goto :goto_8

    :catchall_1
    move-exception v11

    move-object v9, v10

    goto :goto_7

    :catch_6
    move-exception v3

    move-object v9, v10

    goto :goto_5

    :catch_7
    move-exception v3

    goto :goto_1
.end method

.method public static write(Landroid/content/Context;Ljava/lang/String;[B)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheDir(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz p2, :cond_1

    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v3, p2}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v4, "ResourceCache"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot create cache directory: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v4, "ResourceCache"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot write file to cache: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method
