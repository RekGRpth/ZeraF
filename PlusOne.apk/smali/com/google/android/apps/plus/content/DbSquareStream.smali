.class public Lcom/google/android/apps/plus/content/DbSquareStream;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbSquareStream.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbSquareStream;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDescription:Ljava/lang/String;

.field private final mId:Ljava/lang/String;

.field private final mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/DbSquareStream$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbSquareStream$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbSquareStream;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mDescription:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbSquareStream;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mDescription:Ljava/lang/String;

    return-void
.end method

.method public static deserialize([B)[Lcom/google/android/apps/plus/content/DbSquareStream;
    .locals 8
    .param p0    # [B

    if-nez p0, :cond_1

    const/4 v5, 0x0

    :cond_0
    return-object v5

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v3

    new-array v5, v3, [Lcom/google/android/apps/plus/content/DbSquareStream;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v1

    new-instance v7, Lcom/google/android/apps/plus/content/DbSquareStream;

    invoke-direct {v7, v6, v4, v1}, Lcom/google/android/apps/plus/content/DbSquareStream;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v7, v5, v2

    add-int/lit8 v7, v2, 0x1

    int-to-short v2, v7

    goto :goto_0
.end method

.method public static serialize([Lcom/google/android/apps/plus/content/DbSquareStream;)[B
    .locals 7
    .param p0    # [Lcom/google/android/apps/plus/content/DbSquareStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    array-length v6, p0

    if-nez v6, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x20

    invoke-direct {v1, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    array-length v6, p0

    invoke-virtual {v2, v6}, Ljava/io/DataOutputStream;->writeShort(I)V

    move-object v0, p0

    array-length v4, p0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    iget-object v6, v5, Lcom/google/android/apps/plus/content/DbSquareStream;->mId:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/content/DbSquareStream;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v6, v5, Lcom/google/android/apps/plus/content/DbSquareStream;->mName:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/content/DbSquareStream;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v6, v5, Lcom/google/android/apps/plus/content/DbSquareStream;->mDescription:Ljava/lang/String;

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/content/DbSquareStream;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_0

    :catchall_0
    move-exception v6

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    throw v6
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getStreamId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{SquareStream id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbSquareStream;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
