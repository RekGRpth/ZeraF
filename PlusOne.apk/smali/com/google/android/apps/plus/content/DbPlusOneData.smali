.class public final Lcom/google/android/apps/plus/content/DbPlusOneData;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbPlusOneData.java"


# instance fields
.field private mCount:I

.field private mId:Ljava/lang/String;

.field private mPlusOnedByMe:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    iput-boolean p3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .locals 5
    .param p0    # [B

    const/4 v3, 0x1

    if-nez p0, :cond_0

    const/4 v4, 0x0

    :goto_0
    return-object v4

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    if-ne v4, v3, :cond_1

    :goto_1
    new-instance v4, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v4, v2, v1, v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbPlusOneData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->write(I)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/DataPlusOne;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Lcom/google/api/services/plusi/model/DataPlusOne;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    return v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final isPlusOnedByMe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    return v0
.end method

.method public final setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    return-void
.end method

.method public final updatePlusOnedByMe(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    iget v1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    :cond_0
    return-void

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method
