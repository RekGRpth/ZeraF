.class public final Lcom/google/android/apps/plus/content/DbEmbedAlbum;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbEmbedAlbum.java"


# instance fields
.field protected mAlbumName:Ljava/lang/String;

.field protected mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

.field protected mMediaCount:I

.field protected mReportedMediaCount:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/PlusPhotoAlbum;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/PlusPhotoAlbum;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    iget v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->photoCount:Ljava/lang/Integer;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mReportedMediaCount:I

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mAlbumName:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    new-array v1, v1, [Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    new-instance v3, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusPhoto;

    invoke-direct {v3, v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/PlusPhoto;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusPhotoAlbum;->associatedMedia:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedAlbum;
    .locals 5
    .param p0    # [B

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;-><init>()V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v3

    iput v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mReportedMediaCount:I

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mAlbumName:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    new-array v3, v3, [Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iput-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    const/4 v2, 0x0

    :goto_0
    iget v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    if-ge v2, v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    new-instance v4, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-direct {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>()V

    aput-object v4, v3, v2

    iget-object v3, v0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    aget-object v3, v3, v2

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserializeFromStream(Ljava/nio/ByteBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbEmbedAlbum;)[B
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/content/DbEmbedAlbum;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget v4, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v4, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mReportedMediaCount:I

    invoke-virtual {v1, v4}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mAlbumName:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    iget v4, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    if-ge v0, v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    aget-object v4, v4, v0

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->serializeToStream(Ljava/io/DataOutputStream;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    return-object v2
.end method


# virtual methods
.method public final getAlbumName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMediaCount:I

    return v0
.end method

.method public final getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mMedia:[Lcom/google/android/apps/plus/content/DbEmbedMedia;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getReportedMediaCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->mReportedMediaCount:I

    return v0
.end method
