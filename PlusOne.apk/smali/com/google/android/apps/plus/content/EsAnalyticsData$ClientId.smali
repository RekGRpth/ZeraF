.class final enum Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
.super Ljava/lang/Enum;
.source "EsAnalyticsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsAnalyticsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "ClientId"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

.field public static final enum ANDROID_OS:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

.field public static final enum ANDROID_TABLET:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    const-string v1, "ANDROID_OS"

    const-string v2, "4"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_OS:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    new-instance v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    const-string v1, "ANDROID_TABLET"

    const-string v2, "10"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_TABLET:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    sget-object v1, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_OS:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_TABLET:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->$VALUES:[Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->mValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->$VALUES:[Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    return-object v0
.end method


# virtual methods
.method public final value()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->mValue:Ljava/lang/String;

    return-object v0
.end method
