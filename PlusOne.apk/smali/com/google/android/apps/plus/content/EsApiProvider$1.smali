.class final Lcom/google/android/apps/plus/content/EsApiProvider$1;
.super Ljava/lang/Object;
.source "EsApiProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsApiProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/content/EsApiProvider;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$info:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

.field final synthetic val$skip:Z

.field final synthetic val$urlList:Ljava/util/List;

.field final synthetic val$urls:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/content/EsApiProvider;[Ljava/lang/String;ZLjava/util/List;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->this$0:Lcom/google/android/apps/plus/content/EsApiProvider;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$urls:[Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$skip:Z

    iput-object p4, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$urlList:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p6, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$info:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$urls:[Ljava/lang/String;

    array-length v3, v3

    new-array v1, v3, [Lcom/google/android/apps/plus/content/PreviewRequestData;

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$urls:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    new-instance v3, Lcom/google/android/apps/plus/content/PreviewRequestData;

    iget-object v4, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$urls:[Ljava/lang/String;

    aget-object v4, v4, v0

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/plus/content/PreviewRequestData;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;)V

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->this$0:Lcom/google/android/apps/plus/content/EsApiProvider;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$skip:Z

    # getter for: Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Lvedroid/support/v4/util/LruCache;
    invoke-static {}, Lcom/google/android/apps/plus/content/EsApiProvider;->access$000()Lvedroid/support/v4/util/LruCache;

    move-result-object v5

    # invokes: Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedKeys(ZLvedroid/support/v4/util/LruCache;[Lcom/google/android/apps/plus/content/PreviewRequestData;)Ljava/util/List;
    invoke-static {v3, v4, v5, v1}, Lcom/google/android/apps/plus/content/EsApiProvider;->access$100(Lcom/google/android/apps/plus/content/EsApiProvider;ZLvedroid/support/v4/util/LruCache;[Lcom/google/android/apps/plus/content/PreviewRequestData;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$urlList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->this$0:Lcom/google/android/apps/plus/content/EsApiProvider;

    iget-object v4, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/content/EsApiProvider$1;->val$info:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    # invokes: Lcom/google/android/apps/plus/content/EsApiProvider;->updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    invoke-static {v3, v4, v5, v2}, Lcom/google/android/apps/plus/content/EsApiProvider;->access$200(Lcom/google/android/apps/plus/content/EsApiProvider;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    :cond_1
    return-void
.end method
