.class public Lcom/google/android/apps/plus/content/DbLocation;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbLocation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBestAddress:Ljava/lang/String;

.field private final mClusterId:Ljava/lang/String;

.field private final mHasCoordinates:Z

.field private final mLatitudeE7:I

.field private final mLongitudeE7:I

.field private final mName:Ljava/lang/String;

.field private final mPrecisionMeters:D

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbLocation$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/location/Location;)V
    .locals 4
    .param p1    # I
    .param p2    # Landroid/location/Location;

    const-wide v2, 0x416312d000000000L

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    :goto_0
    iput-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    return-void

    :cond_1
    const-wide/high16 v0, -0x4010000000000000L

    goto :goto_0
.end method

.method public constructor <init>(ILcom/google/api/services/plusi/model/Location;)V
    .locals 8
    .param p1    # I
    .param p2    # Lcom/google/api/services/plusi/model/Location;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide v6, 0x416312d000000000L

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-ltz p1, :cond_0

    const/4 v4, 0x3

    if-gt p1, v4, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_1
    iput p1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->clusterId:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    iget-object v0, p2, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    :goto_0
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    if-eqz v4, :cond_4

    iget-object v1, p2, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    :goto_1
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v2, :cond_7

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    :goto_3
    iget-object v2, p2, Lcom/google/api/services/plusi/model/Location;->precisionMeters:Ljava/lang/Double;

    if-nez v2, :cond_8

    const-wide/high16 v2, -0x4010000000000000L

    :goto_4
    iput-wide v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    return-void

    :cond_2
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->latitude:Ljava/lang/Float;

    if-eqz v4, :cond_3

    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->latitude:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_0

    :cond_4
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->longitude:Ljava/lang/Float;

    if-eqz v4, :cond_5

    iget-object v2, p2, Lcom/google/api/services/plusi/model/Location;->longitude:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v4, v2

    mul-double/2addr v4, v6

    double-to-int v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_2

    :cond_7
    iput v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    goto :goto_3

    :cond_8
    iget-object v2, p2, Lcom/google/api/services/plusi/model/Location;->precisionMeters:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto :goto_4
.end method

.method public constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # D

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iput p1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    :goto_1
    iput-wide p7, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    goto :goto_1
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/Checkin;)V
    .locals 5
    .param p1    # Lcom/google/api/services/plusi/model/Checkin;

    const-wide v3, 0x416312d000000000L

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->name:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->latitude:Ljava/lang/Double;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->longitude:Ljava/lang/Double;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->latitude:Ljava/lang/Double;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v0

    mul-double/2addr v0, v3

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->longitude:Ljava/lang/Double;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v0

    mul-double/2addr v0, v3

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    :goto_3
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->clusterId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    :goto_4
    const-wide/high16 v0, -0x4010000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    goto :goto_3

    :cond_5
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Checkin;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    goto :goto_4
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/Place;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/Place;

    const/4 v1, 0x0

    const-wide v4, 0x416312d000000000L

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->latitude:Ljava/lang/Double;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->longitude:Ljava/lang/Double;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->latitude:Ljava/lang/Double;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GeoCoordinates;->longitude:Ljava/lang/Double;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v2

    mul-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    :goto_2
    const-wide/high16 v2, -0x4010000000000000L

    iput-wide v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    iput-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    goto :goto_2
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 13
    .param p0    # [B

    const/4 v3, 0x0

    if-nez p0, :cond_0

    :goto_0
    return-object v3

    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbLocation;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbLocation;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v10, 0x1

    :goto_1
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v7

    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbLocation;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v10, :cond_3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_2
    if-eqz v10, :cond_1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_1
    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    move-object v3, v0

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    :cond_3
    move-object v2, v3

    goto :goto_2
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/content/DbLocation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbLocation;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbLocation;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-wide v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {v0, v3, v4}, Ljava/io/DataOutputStream;->writeDouble(D)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbLocation;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    return-object v1

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/Checkin;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/Checkin;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(Lcom/google/api/services/plusi/model/Checkin;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B

    move-result-object v0

    return-object v0
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/Place;)[B
    .locals 1
    .param p0    # Lcom/google/api/services/plusi/model/Place;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(Lcom/google/api/services/plusi/model/Place;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getAndroidLocation()Landroid/location/Location;
    .locals 5

    const-wide v3, 0x416312d000000000L

    new-instance v0, Landroid/location/Location;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    :cond_0
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_1

    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    :cond_1
    return-object v0
.end method

.method public final getBestAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getClusterId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    return-object v0
.end method

.method public final getLatitudeE7()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    return v0
.end method

.method public final getLocationName(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .param p1    # Landroid/content/Context;

    const-wide v6, 0x416312d000000000L

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->location_lat_long_format:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    int-to-double v4, v4

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    int-to-double v4, v4

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method public final getLongitudeE7()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrecisionMeters()D
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    return-wide v0
.end method

.method public final hasCoordinates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    return v0
.end method

.method public final isCoarse()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPrecise()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSamePlace(Lcom/google/android/apps/plus/content/DbLocation;)Z
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v3, 0x3

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbLocation;->isPrecise()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/DbLocation;->isPrecise()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbLocation;->isCoarse()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/DbLocation;->isCoarse()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    iget v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    if-ne v2, v3, :cond_5

    iget v2, p1, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    iget-boolean v3, p1, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    iget v3, p1, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iget v3, p1, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    if-eq v2, v3, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final toProtocolObject()Lcom/google/api/services/plusi/model/Location;
    .locals 5

    new-instance v0, Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Location;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->clusterId:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    :cond_0
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_1

    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->precisionMeters:Ljava/lang/Double;

    :cond_1
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "LocationValue type: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unknown("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", addr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasCoord: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latE7: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lngE7: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cluster: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", precision: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-string v0, "precise"

    goto :goto_0

    :pswitch_1
    const-string v0, "coarse"

    goto :goto_0

    :pswitch_2
    const-string v0, "place"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
