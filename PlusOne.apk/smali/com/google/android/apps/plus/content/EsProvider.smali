.class public Lcom/google/android/apps/plus/content/EsProvider;
.super Landroid/content/ContentProvider;
.source "EsProvider.java"


# static fields
.field private static final ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ACCOUNT_STATUS_URI:Landroid/net/Uri;

.field private static final ACTIVITIES_BY_CIRCLE_ID_URI:Landroid/net/Uri;

.field private static final ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

.field public static final ACTIVITIES_URI:Landroid/net/Uri;

.field private static final ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ACTIVITY_SUMMARY_URI:Landroid/net/Uri;

.field public static final ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

.field public static final ACTIVITY_VIEW_URI:Landroid/net/Uri;

.field public static final ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final ALBUM_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CIRCLES_URI:Landroid/net/Uri;

.field public static final COMMENTS_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

.field private static final COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final COMMENTS_VIEW_URI:Landroid/net/Uri;

.field public static final CONTACTS_BY_CIRCLE_ID_URI:Landroid/net/Uri;

.field public static final CONTACTS_BY_SQUARE_ID_URI:Landroid/net/Uri;

.field private static final CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONTACTS_QUERY_URI:Landroid/net/Uri;

.field private static final CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONTACTS_URI:Landroid/net/Uri;

.field public static final CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

.field private static final CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONVERSATIONS_URI:Landroid/net/Uri;

.field private static final EMOTISHARE_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final EMOTISHARE_URI:Landroid/net/Uri;

.field public static final EVENTS_ALL_URI:Landroid/net/Uri;

.field private static final EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

.field private static final LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOCATION_QUERIES_VIEW_URI:Landroid/net/Uri;

.field private static final MESSAGES_BY_CONVERSATION_URI:Landroid/net/Uri;

.field private static final MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MESSAGE_NOTIFICATIONS_URI:Landroid/net/Uri;

.field private static final MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

.field private static final NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_DATA_STATS_URI:Landroid/net/Uri;

.field private static final NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_DATA_TRANSACTIONS_URI:Landroid/net/Uri;

.field private static final NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NOTIFICATIONS_URI:Landroid/net/Uri;

.field public static final PANORAMA_IMAGE_URI:Landroid/net/Uri;

.field private static final PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PARTICIPANTS_URI:Landroid/net/Uri;

.field private static final PHOTOS_BY_ALBUM_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTOS_BY_ALBUM_VIEW_SQL:Ljava/lang/String;

.field private static final PHOTOS_BY_EVENT_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTOS_BY_EVENT_VIEW_SQL:Ljava/lang/String;

.field private static final PHOTOS_BY_STREAM_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final PHOTOS_BY_STREAM_VIEW_SQL:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final PHOTOS_BY_USER_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final PHOTOS_BY_USER_VIEW_SQL:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

.field public static final PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

.field public static final PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

.field public static final PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

.field private static final PHOTO_COMMENTS_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTO_HOME_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PHOTO_HOME_URI:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PHOTO_NOTIFICATION_COUNT_URI:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final PHOTO_NOTIFICATION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PHOTO_OF_USER_ID_URI:Landroid/net/Uri;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

.field private static final PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PHOTO_URI:Landroid/net/Uri;

.field private static final PHOTO_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final PHOTO_VIEW_SQL:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PLATFORM_AUDIENCE_URI:Landroid/net/Uri;

.field private static final PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PLUS_PAGES_URI:Landroid/net/Uri;

.field private static final SQUARES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SQUARES_URI:Landroid/net/Uri;

.field private static final SQUARE_CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;

.field private static sActivitiesFirstPageSize:I

.field private static sActivitiesPageSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v7, 0x4a

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.is_panorama as is_panorama, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, contacts.avatar as owner_avatar_url, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photo_view"

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_SQL:Ljava/lang/String;

    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.is_panorama as is_panorama, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, contacts.avatar as owner_avatar_url, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_album_view"

    aput-object v2, v1, v4

    const-string v2, "photos_in_album.sort_index as sort_index, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_in_album ON photo.photo_id=photos_in_album.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_SQL:Ljava/lang/String;

    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.is_panorama as is_panorama, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, contacts.avatar as owner_avatar_url, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_event_view"

    aput-object v2, v1, v4

    const-string v2, "photos_in_event.collection_id as event_id, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_in_event ON photo.photo_id=photos_in_event.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_SQL:Ljava/lang/String;

    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.is_panorama as is_panorama, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, contacts.avatar as owner_avatar_url, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_stream_view"

    aput-object v2, v1, v4

    const-string v2, "photos_in_stream.collection_id as stream_id, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_in_stream ON photo.photo_id=photos_in_stream.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_SQL:Ljava/lang/String;

    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.is_panorama as is_panorama, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, contacts.avatar as owner_avatar_url, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_user_view"

    aput-object v2, v1, v4

    const-string v2, "photos_of_user.collection_id as photo_of_user_id, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_of_user ON photo.photo_id=photos_of_user.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_SQL:Ljava/lang/String;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/account_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activities"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activities_stream_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_by_circle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_BY_CIRCLE_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activities/summary"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activity_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/comments_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/location_queries_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/notifications"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/message_notifications_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/messages/conversation"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_BY_CONVERSATION_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/participants"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/messenger_suggestions"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/hangout_suggestions"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/circles"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/contacts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/contacts/id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/circle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_CIRCLE_ID_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/square"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_SQUARE_ID_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/query"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_QUERY_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/contacts/suggested"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_home"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/album_view_by_user"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/album_view_by_album_and_owner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_album"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_event"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_user"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_stream_and_owner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_notification_count"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_NOTIFICATION_COUNT_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_comment_by_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_shape_by_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/network_data_transactions"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/network_data_stats"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/platform_audience"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/plus_pages"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/events"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/panorama_image"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PANORAMA_IMAGE_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/squares"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/emotishare_data"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_URI:Landroid/net/Uri;

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "account_status"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activity_view/activity/*"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities_stream_view/stream/*"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities_stream_view_by_circle/*"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities/summary"

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "comments_view/activity/*"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "location_queries_view/query/*"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "notifications"

    const/16 v3, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "circles"

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/circle/*"

    const/16 v3, 0x47

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/square/*"

    const/16 v3, 0x4b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/query/*"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/query"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/id/*"

    const/16 v3, 0x48

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/suggested"

    const/16 v3, 0x49

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "circle_contact"

    const/16 v3, 0x3e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "conversations"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "participants/conversation/*"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "message_notifications_view"

    const/16 v3, 0xa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "messages/conversation/*"

    const/16 v3, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "messenger_suggestions"

    const/16 v3, 0x73

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "hangout_suggestions"

    const/16 v3, 0x74

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_home"

    const/16 v3, 0x82

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view/*"

    const/16 v3, 0x83

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view_by_user/*"

    const/16 v3, 0x84

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view_by_album_and_owner/*/*"

    const/16 v3, 0x90

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view_by_stream/*"

    const/16 v3, 0x85

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_photo/*"

    const/16 v3, 0x86

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_album/*"

    const/16 v3, 0x87

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_event/*"

    const/16 v3, 0x91

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_user/*"

    const/16 v3, 0x8b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_stream_and_owner/*/*"

    const/16 v3, 0x8a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_comment_by_photo/*"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_shape_by_photo/*"

    const/16 v3, 0x8f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_notification_count"

    const/16 v3, 0x8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "network_data_transactions"

    const/16 v3, 0xb4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "network_data_stats"

    const/16 v3, 0xb5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "platform_audience/*"

    const/16 v3, 0xb6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "plus_pages"

    const/16 v3, 0xbe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "panorama_image"

    const/16 v3, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "squares"

    const/16 v3, 0xd2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "squares/*"

    const/16 v3, 0xd3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "emotishare_data"

    const/16 v3, 0xd4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_sync_time"

    const-string v2, "circle_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_sync_time"

    const-string v2, "last_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_stats_sync_time"

    const-string v2, "last_stats_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_contacted_time"

    const-string v2, "last_contacted_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "wipeout_stats"

    const-string v2, "wipeout_stats"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "people_sync_time"

    const-string v2, "people_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "people_last_update_token"

    const-string v2, "people_last_update_token"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "avatars_downloaded"

    const-string v2, "avatars_downloaded"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "audience_data"

    const-string v2, "audience_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "audience_history"

    const-string v2, "audience_history"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "user_id"

    const-string v2, "user_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contacts_sync_version"

    const-string v2, "contacts_sync_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "push_notifications"

    const-string v2, "push_notifications"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_analytics_sync_time"

    const-string v2, "last_analytics_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_settings_sync_time"

    const-string v2, "last_settings_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_squares_sync_time"

    const-string v2, "last_squares_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_emotishare_sync_time"

    const-string v2, "last_emotishare_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "data_state"

    const-string v2, "data_state"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sort_index"

    const-string v2, "sort_index"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_activity"

    const-string v2, "last_activity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "token"

    const-string v2, "token"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "avatar"

    const-string v2, "avatar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "source_id"

    const-string v2, "source_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "source_name"

    const-string v2, "source_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "total_comment_count"

    const-string v2, "total_comment_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "plus_one_data"

    const-string v2, "plus_one_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "public"

    const-string v2, "public"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "spam"

    const-string v2, "spam"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "acl_display"

    const-string v2, "acl_display"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_comment"

    const-string v2, "can_comment"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_reshare"

    const-string v2, "can_reshare"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_plusoneable"

    const-string v2, "is_plusoneable"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "has_muted"

    const-string v2, "has_muted"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "has_read"

    const-string v2, "has_read"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "loc"

    const-string v2, "loc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_edited"

    const-string v2, "is_edited"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "photo_collection"

    const-string v2, "photo_collection"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "whats_hot"

    const-string v2, "whats_hot"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "content_flags"

    const-string v2, "content_flags"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "annotation"

    const-string v2, "annotation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "annotation_plaintext"

    const-string v2, "annotation_plaintext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "title_plaintext"

    const-string v2, "title_plaintext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "original_author_id"

    const-string v2, "original_author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "original_author_name"

    const-string v2, "original_author_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "original_author_avatar_url"

    const-string v2, "original_author_avatar_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "comment"

    const-string v2, "comment"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "permalink"

    const-string v2, "permalink"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "event_data"

    const-string v2, "event_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_update"

    const-string v2, "square_update"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_reshare_update"

    const-string v2, "square_reshare_update"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_deep_link"

    const-string v2, "embed_deep_link"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_media"

    const-string v2, "embed_media"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_photo_album"

    const-string v2, "embed_photo_album"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_checkin"

    const-string v2, "embed_checkin"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_place"

    const-string v2, "embed_place"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_place_review"

    const-string v2, "embed_place_review"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_skyjam"

    const-string v2, "embed_skyjam"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_appinvite"

    const-string v2, "embed_appinvite"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_hangout"

    const-string v2, "embed_hangout"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_square"

    const-string v2, "embed_square"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_emotishare"

    const-string v2, "embed_emotishare"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "promo"

    const-string v2, "promo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_edited"

    const-string v2, "is_edited"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "comment_id"

    const-string v2, "comment_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "content"

    const-string v2, "content"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "avatar"

    const-string v2, "avatar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "plus_one_data"

    const-string v2, "plus_one_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "location"

    const-string v2, "location"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notif_id"

    const-string v2, "notif_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "category"

    const-string v2, "category"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "message"

    const-string v2, "message"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "enabled"

    const-string v2, "enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "read"

    const-string v2, "read"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_data"

    const-string v2, "circle_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_gaia_id"

    const-string v2, "pd_gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_album_id"

    const-string v2, "pd_album_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_album_name"

    const-string v2, "pd_album_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_photo_id"

    const-string v2, "pd_photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "ed_event"

    const-string v2, "ed_event"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "ed_event_id"

    const-string v2, "ed_event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "ed_creator_id"

    const-string v2, "ed_creator_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notification_type"

    const-string v2, "notification_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "coalescing_code"

    const-string v2, "coalescing_code"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "entity_type"

    const-string v2, "entity_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "entity_snippet"

    const-string v2, "entity_snippet"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "entity_photos_data"

    const-string v2, "entity_photos_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "entity_squares_data"

    const-string v2, "entity_squares_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_id"

    const-string v2, "square_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_name"

    const-string v2, "square_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_photo_url"

    const-string v2, "square_photo_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "taggee_photo_ids"

    const-string v2, "taggee_photo_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "taggee_data_actors"

    const-string v2, "taggee_data_actors"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "circles.rowid AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_id"

    const-string v2, "circles.circle_id AS circle_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_name"

    const-string v2, "circle_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "semantic_hints"

    const-string v2, "semantic_hints"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contact_count"

    const-string v2, "contact_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "member_ids"

    const-string v2, "group_concat(link_person_id, \'|\') AS member_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "show_order"

    const-string v2, "show_order"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "volume"

    const-string v2, "volume"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notifications_enabled"

    const-string v2, "notifications_enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "contacts.rowid AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "person_id"

    const-string v2, "contacts.person_id AS person_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "avatar"

    const-string v2, "avatar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_updated_time"

    const-string v2, "last_updated_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "profile_type"

    const-string v2, "profile_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "in_my_circles"

    const-string v2, "in_my_circles"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "for_sharing"

    const-string v2, "(CASE WHEN person_id IN (SELECT link_person_id FROM circle_contact WHERE link_circle_id IN (SELECT circle_id FROM circles WHERE semantic_hints & 64 != 0)) THEN 1 ELSE 0 END) AS for_sharing"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "blocked"

    const-string v2, "blocked"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "interaction_sort_key"

    const-string v2, "interaction_sort_key"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "packed_circle_ids"

    const-string v2, "group_concat(link_circle_id, \'|\') AS packed_circle_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contact_update_time"

    const-string v2, "contact_update_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contact_proto"

    const-string v2, "contact_proto"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "profile_update_time"

    const-string v2, "profile_update_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "profile_proto"

    const-string v2, "profile_proto"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "email"

    const-string v2, "email"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "person_id"

    const-string v2, "person_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "packed_circle_ids"

    const-string v2, "packed_circle_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "phone"

    const-string v2, "phone"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "phone_type"

    const-string v2, "phone_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "category"

    const-string v2, "category"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "category_label"

    const-string v2, "category_label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "explanation"

    const-string v2, "explanation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "properties"

    const-string v2, "properties"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "suggestion_id"

    const-string v2, "suggestion_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_muted"

    const-string v2, "is_muted"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_visible"

    const-string v2, "is_visible"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_event_timestamp"

    const-string v2, "latest_event_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_timestamp"

    const-string v2, "latest_message_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "earliest_event_timestamp"

    const-string v2, "earliest_event_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "has_older_events"

    const-string v2, "has_older_events"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "generated_name"

    const-string v2, "generated_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_text"

    const-string v2, "latest_message_text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_image_url"

    const-string v2, "latest_message_image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_id"

    const-string v2, "latest_message_author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_type"

    const-string v2, "latest_message_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_full_name"

    const-string v2, "latest_message_author_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_first_name"

    const-string v2, "latest_message_author_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_type"

    const-string v2, "latest_message_author_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_group"

    const-string v2, "is_group"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_pending_accept"

    const-string v2, "is_pending_accept"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_id"

    const-string v2, "inviter_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_full_name"

    const-string v2, "inviter_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_first_name"

    const-string v2, "inviter_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_type"

    const-string v2, "inviter_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "fatal_error_type"

    const-string v2, "fatal_error_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_pending_leave"

    const-string v2, "is_pending_leave"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_awaiting_event_stream"

    const-string v2, "is_awaiting_event_stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_awaiting_older_events"

    const-string v2, "is_awaiting_older_events"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_event_timestamp"

    const-string v2, "first_event_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "packed_participants"

    const-string v2, "packed_participants"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "participant_id"

    const-string v2, "participant_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "active"

    const-string v2, "active"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sequence"

    const-string v2, "sequence"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "participant_id"

    const-string v2, "participant_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "participant_id"

    const-string v2, "participant_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "message_id"

    const-string v2, "message_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "text"

    const-string v2, "text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_first_name"

    const-string v2, "author_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_full_name"

    const-string v2, "author_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_type"

    const-string v2, "author_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "image_url"

    const-string v2, "image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "message_id"

    const-string v2, "message_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "text"

    const-string v2, "text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "image_url"

    const-string v2, "image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notification_seen"

    const-string v2, "notification_seen"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_full_name"

    const-string v2, "author_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_first_name"

    const-string v2, "author_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_type"

    const-string v2, "author_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_muted"

    const-string v2, "conversation_muted"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_group"

    const-string v2, "conversation_group"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_name"

    const-string v2, "conversation_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "generated_name"

    const-string v2, "generated_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_pending_accept"

    const-string v2, "conversation_pending_accept"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_pending_leave"

    const-string v2, "conversation_pending_leave"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_id"

    const-string v2, "inviter_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_full_name"

    const-string v2, "inviter_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_first_name"

    const-string v2, "inviter_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_type"

    const-string v2, "inviter_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "photo_count"

    const-string v2, "photo_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "height"

    const-string v2, "height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "image"

    const-string v2, "image"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "notification_count"

    const-string v2, "notification_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "photo_home_key"

    const-string v2, "photo_home_key"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "size"

    const-string v2, "size"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "sort_order"

    const-string v2, "sort_order"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "width"

    const-string v2, "width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_NOTIFICATION_MAP:Ljava/util/HashMap;

    const-string v1, "_count"

    const-string v2, "notification_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_type"

    const-string v2, "album_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_key"

    const-string v2, "album_key"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "cover_photo_id"

    const-string v2, "cover_photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "entity_version"

    const-string v2, "entity_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "height"

    const-string v2, "height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "is_activity"

    const-string v2, "is_activity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_id"

    const-string v2, "owner_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_count"

    const-string v2, "photo_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "size"

    const-string v2, "size"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "sort_order"

    const-string v2, "sort_order"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "stream_id"

    const-string v2, "stream_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "width"

    const-string v2, "width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "audience"

    const-string v2, "audience"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "person_id"

    const-string v2, "person_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "sort_key"

    const-string v2, "sort_key"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "avatar"

    const-string v2, "avatar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "last_updated_time"

    const-string v2, "last_updated_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "profile_type"

    const-string v2, "profile_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "profile_state"

    const-string v2, "profile_state"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "in_my_circles"

    const-string v2, "in_my_circles"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENT_PEOPLE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "blocked"

    const-string v2, "blocked"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_count"

    const-string v2, "COUNT(*) AS _count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "action_state"

    const-string v2, "action_state"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_name"

    const-string v2, "album_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_stream"

    const-string v2, "album_stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "comment_count"

    const-string v2, "comment_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "downloadable"

    const-string v2, "downloadable"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "entity_version"

    const-string v2, "entity_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "height"

    const-string v2, "height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_id"

    const-string v2, "owner_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_name"

    const-string v2, "owner_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_avatar_url"

    const-string v2, "owner_avatar_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "pending_status"

    const-string v2, "pending_status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_by_me"

    const-string v2, "plusone_by_me"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_count"

    const-string v2, "plusone_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_data"

    const-string v2, "plusone_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_id"

    const-string v2, "plusone_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "fingerprint"

    const-string v2, "fingerprint"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "upload_status"

    const-string v2, "upload_status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "video_data"

    const-string v2, "video_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "is_panorama"

    const-string v2, "is_panorama"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "width"

    const-string v2, "width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_count"

    const-string v2, "count(*)  as _count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_of_user_id"

    const-string v2, "NULL AS photo_of_user_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_MAP:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "event_id"

    const-string v2, "photos_in_event.collection_id as event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_MAP:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_of_user_id"

    const-string v2, "photos_of_user.collection_id as photo_of_user_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "bounds"

    const-string v2, "bounds"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "creator_id"

    const-string v2, "creator_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "shape_id"

    const-string v2, "shape_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "subject_id"

    const-string v2, "subject_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "creator_name"

    const-string v2, "creator_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "subject_name"

    const-string v2, "subject_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "comment_id"

    const-string v2, "comment_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "content"

    const-string v2, "content"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "create_time"

    const-string v2, "create_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_data"

    const-string v2, "plusone_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "truncated"

    const-string v2, "truncated"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "update_time"

    const-string v2, "update_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "owner_name"

    const-string v2, "contacts.name as owner_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "avatar"

    const-string v2, "contacts.avatar as avatar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "time"

    const-string v2, "time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "network_duration"

    const-string v2, "network_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "process_duration"

    const-string v2, "process_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sent"

    const-string v2, "sent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "recv"

    const-string v2, "recv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "req_count"

    const-string v2, "req_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "exception"

    const-string v2, "exception"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first"

    const-string v2, "first"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last"

    const-string v2, "last"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "network_duration"

    const-string v2, "network_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "process_duration"

    const-string v2, "process_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sent"

    const-string v2, "sent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "recv"

    const-string v2, "recv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "req_count"

    const-string v2, "req_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "package_name"

    const-string v2, "package_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "audience_data"

    const-string v2, "audience_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_id"

    const-string v2, "square_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_name"

    const-string v2, "square_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "tagline"

    const-string v2, "tagline"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "photo_url"

    const-string v2, "photo_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "about_text"

    const-string v2, "about_text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "joinability"

    const-string v2, "joinability"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "member_count"

    const-string v2, "member_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "membership_status"

    const-string v2, "membership_status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_member"

    const-string v2, "is_member"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "suggested"

    const-string v2, "suggested"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "post_visibility"

    const-string v2, "post_visibility"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_see_members"

    const-string v2, "can_see_members"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_see_posts"

    const-string v2, "can_see_posts"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_join"

    const-string v2, "can_join"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_request_to_join"

    const-string v2, "can_request_to_join"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_share"

    const-string v2, "can_share"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_invite"

    const-string v2, "can_invite"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notifications_enabled"

    const-string v2, "notifications_enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "square_streams"

    const-string v2, "square_streams"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_gaia_id"

    const-string v2, "inviter_gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_name"

    const-string v2, "contacts.name as inviter_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_photo_url"

    const-string v2, "contacts.avatar as inviter_photo_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sort_index"

    const-string v2, "sort_index"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_sync"

    const-string v2, "last_sync"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_members_sync"

    const-string v2, "last_members_sync"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "invitation_dismissed"

    const-string v2, "invitation_dismissed"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "suggestion_sort_index"

    const-string v2, "suggestion_sort_index"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "auto_subscribe"

    const-string v2, "auto_subscribe"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "disable_subscription"

    const-string v2, "disable_subscription"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "volume"

    const-string v2, "volume"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARE_CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "membership_status"

    const-string v2, "square_contact.membership_status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "data"

    const-string v2, "data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "generation"

    const-string v2, "generation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method public static analyzeDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "ANALYZE"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v1, "ANALYZE sqlite_master"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public static appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0    # Landroid/net/Uri$Builder;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v0, "account"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 1
    .param p0    # Landroid/net/Uri;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildActivityViewUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # J

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_BY_CONVERSATION_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildPanoramaUri(Ljava/io/File;)Landroid/net/Uri;
    .locals 3
    .param p0    # Ljava/io/File;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PANORAMA_IMAGE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {p0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # J

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "conversation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildPeopleQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZZLjava/lang/String;I)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # I

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_QUERY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "limit"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v1, "self_gaia_id"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v2, "plus_pages"

    if-eqz p2, :cond_2

    const-string v1, "true"

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    const-string v2, "in_circles"

    if-eqz p3, :cond_3

    const-string v1, "true"

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz p4, :cond_1

    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1

    :cond_2
    const-string v1, "false"

    goto :goto_0

    :cond_3
    const-string v1, "false"

    goto :goto_1
.end method

.method public static buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "stream"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static declared-synchronized cleanupData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V
    .locals 22
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Z

    const-class v17, Lcom/google/android/apps/plus/content/EsProvider;

    monitor-enter v17

    if-nez p2, :cond_1

    :try_start_0
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getLastDatabaseCleanupTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v18

    sub-long v18, v18, v14

    const-wide/32 v20, 0x25c3f80

    cmp-long v16, v18, v20

    if-gez v16, :cond_1

    :cond_0
    :goto_0
    monitor-exit v17

    return-void

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/apps/plus/content/EsMediaCache;->cleanup()V

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    new-instance v16, Ljava/io/File;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    const-wide/32 v18, 0xf4240

    cmp-long v16, v10, v18

    if-gez v16, :cond_2

    if-nez p2, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastDatabaseCleanupTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    const-string v16, "EsProvider"

    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v16

    if-eqz v16, :cond_0

    new-instance v16, Ljava/io/File;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v7

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v5, v18, v12

    const-wide/16 v18, 0x3e8

    div-long v18, v5, v18

    move-wide/from16 v0, v18

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v16

    const-string v18, "."

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    const-wide/16 v18, 0x3e8

    rem-long v18, v5, v18

    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v16

    const-string v18, " seconds"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v16, "EsProvider"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, ">>>>> cleanup db took "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " old size: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", new size: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v16

    monitor-exit v17

    throw v16

    :cond_2
    :try_start_2
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPostsData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsNotificationData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsSquaresData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/plus/content/EsPhotosData;->cleanupData(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    invoke-static {}, Lcom/google/android/apps/plus/content/EsNetworkData;->cleanupData$3105fef4()V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->cleanupData(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastDatabaseCleanupTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    const-string v16, "EsProvider"

    const/16 v18, 0x4

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v16

    if-eqz v16, :cond_0

    new-instance v16, Ljava/io/File;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->length()J

    move-result-wide v7

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v5, v18, v12

    const-wide/16 v18, 0x3e8

    div-long v18, v5, v18

    move-wide/from16 v0, v18

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v16

    const-string v18, "."

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v16

    const-wide/16 v18, 0x3e8

    rem-long v18, v5, v18

    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v16

    const-string v18, " seconds"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v16, "EsProvider"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, ">>>>> cleanup db took "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " old size: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", new size: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :catchall_1
    move-exception v16

    :try_start_6
    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v16
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catchall_2
    move-exception v16

    :try_start_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v18

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastDatabaseCleanupTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    const-string v18, "EsProvider"

    const/16 v19, 0x4

    invoke-static/range {v18 .. v19}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v18

    if-eqz v18, :cond_3

    new-instance v18, Ljava/io/File;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v7

    new-instance v9, Ljava/lang/StringBuffer;

    invoke-direct {v9}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v5, v18, v12

    const-wide/16 v18, 0x3e8

    div-long v18, v5, v18

    move-wide/from16 v0, v18

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, "."

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v18

    const-wide/16 v19, 0x3e8

    rem-long v19, v5, v19

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v18

    const-string v19, " seconds"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v18, "EsProvider"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, ">>>>> cleanup db took "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " old size: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", new size: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    throw v16
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public static deleteDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->deleteDatabase()V

    return-void
.end method

.method private static ensureActivitiesPageSizes(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v1, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesPageSize:I

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    iget v1, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v1, :cond_1

    const/16 v1, 0xf

    sput v1, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesPageSize:I

    const/16 v1, 0xa

    sput v1, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesFirstPageSize:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x18

    sput v1, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesPageSize:I

    const/16 v1, 0x14

    sput v1, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesFirstPageSize:I

    goto :goto_0
.end method

.method public static getActivitiesPageSize(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsProvider;->ensureActivitiesPageSizes(Landroid/content/Context;)V

    sget v0, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesPageSize:I

    return v0
.end method

.method static getIndexSQLs()[Ljava/lang/String;
    .locals 3

    const/16 v1, 0x15

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE INDEX contacts_in_my_circles ON contacts(in_my_circles,person_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE INDEX contacts_name ON contacts(name)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE INDEX contacts_sort_key ON contacts(sort_key)"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE INDEX contacts_gaia_id ON contacts(gaia_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE UNIQUE INDEX circle_contact_forward ON circle_contact(link_circle_id,link_person_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE UNIQUE INDEX circle_contact_backward ON circle_contact(link_person_id,link_circle_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE INDEX contact_search_key ON contact_search(search_key)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE INDEX album_album_id ON album(album_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE INDEX photo_photo_id ON photo(photo_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CREATE INDEX photo_comment_comment_id ON photo_comment(comment_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CREATE INDEX photo_shape_photo_id ON photo_shape(photo_id,shape_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CREATE INDEX photos_in_stream_stream_id ON photos_in_stream(collection_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CREATE INDEX photos_in_album_album_id ON photos_in_album(collection_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CREATE INDEX photos_in_event_event_id ON photos_in_event(collection_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE INDEX photos_of_user_user_id ON photos_of_user(collection_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CREATE INDEX photo_comment_photo_id ON photo_comment(photo_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CREATE INDEX activity_streams_activity_id ON activity_streams(activity_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "CREATE INDEX photo_timestamp ON photo(timestamp)"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "CREATE INDEX tile_idx ON all_tiles( view_id, view_order, type, hidden, mine, collection_id, tile_id, title, subtitle, image_url, image_width, image_height, comment_count, plusone_count )"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CREATE INDEX scroll_idx ON scroll_sections( view_id, landscape, view_order, row, tile_id, title )"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "CREATE INDEX tile_request_idx ON tile_requests( view_id )"

    aput-object v2, v0, v1

    return-object v0
.end method

.method protected static getTableSQLs()[Ljava/lang/String;
    .locals 3

    const/16 v1, 0x35

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE TABLE account_status (user_id TEXT,last_sync_time INT DEFAULT(-1),last_stats_sync_time INT DEFAULT(-1),last_contacted_time INT DEFAULT(-1),wipeout_stats INT DEFAULT(0),circle_sync_time INT DEFAULT(-1),people_sync_time INT DEFAULT(-1),people_last_update_token TEXT,suggested_people_sync_time INT DEFAULT(-1),blocked_people_sync_time INT DEFAULT(-1),event_list_sync_time INT DEFAULT(-1),event_themes_sync_time INT DEFAULT(-1),avatars_downloaded INT DEFAULT(0),audience_data BLOB,audience_history BLOB,contacts_sync_version INT DEFAULT(0),push_notifications INT DEFAULT(0),last_analytics_sync_time INT DEFAULT(-1),last_settings_sync_time INT DEFAULT(-1),last_squares_sync_time INT DEFAULT(-1),last_emotishare_sync_time INT DEFAULT(-1));"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "INSERT INTO account_status DEFAULT VALUES;"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE TABLE activity_streams (stream_key TEXT NOT NULL,activity_id TEXT NOT NULL,sort_index INT NOT NULL,last_activity INT,token TEXT,PRIMARY KEY (stream_key,activity_id));"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE TABLE activities (_id INTEGER PRIMARY KEY, activity_id TEXT UNIQUE NOT NULL, data_state INT NOT NULL DEFAULT (0), author_id TEXT NOT NULL, source_id TEXT, source_name TEXT, total_comment_count INT NOT NULL, plus_one_data BLOB, public INT NOT NULL, spam INT NOT NULL, acl_display TEXT, can_comment INT NOT NULL, can_reshare INT NOT NULL, is_plusoneable INT NOT NULL DEFAULT(1), has_muted INT NOT NULL, has_read INT NOT NULL, loc BLOB, created INT NOT NULL, is_edited INT NOT NULL DEFAULT(0), modified INT NOT NULL, whats_hot BLOB, content_flags INT NOT NULL DEFAULT(0), annotation TEXT, annotation_plaintext TEXT, title TEXT, title_plaintext TEXT, original_author_id TEXT, original_author_name TEXT, original_author_avatar_url TEXT, comment BLOB, permalink TEXT, event_id TEXT, photo_collection BLOB, square_update BLOB, square_reshare_update BLOB, embed_deep_link BLOB, album_id TEXT, embed_media BLOB, embed_photo_album BLOB, embed_checkin BLOB, embed_place BLOB, embed_place_review BLOB, embed_skyjam BLOB, embed_appinvite BLOB, embed_hangout BLOB, embed_square BLOB, embed_emotishare BLOB, promo BLOB);"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE TABLE activity_comments (_id INTEGER PRIMARY KEY,activity_id TEXT NOT NULL,comment_id TEXT UNIQUE NOT NULL,author_id TEXT NOT NULL,content TEXT,created INT NOT NULL,plus_one_data BLOB,FOREIGN KEY (activity_id) REFERENCES activities(activity_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE TABLE locations (_id INTEGER PRIMARY KEY,qrid INT NOT NULL,name TEXT,location BLOB,FOREIGN KEY (qrid) REFERENCES location_queries(_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE TABLE location_queries (_id INTEGER PRIMARY KEY,key TEXT UNIQUE NOT NULL);"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE TABLE notifications (_id INTEGER, notif_id TEXT UNIQUE NOT NULL, coalescing_code TEXT PRIMARY KEY, category INT NOT NULL DEFAULT(0), message TEXT, enabled INT, read INT NOT NULL, seen INT NOT NULL, timestamp INT NOT NULL, circle_data BLOB, pd_gaia_id TEXT, pd_album_id TEXT, pd_album_name TEXT, pd_photo_id INT, activity_id TEXT, ed_event INT DEFAULT(0),ed_event_id TEXT, ed_creator_id TEXT, notification_type INT NOT NULL DEFAULT(0),entity_type INT NOT NULL DEFAULT(0),entity_snippet TEXT,entity_photos_data BLOB,entity_squares_data BLOB,square_id TEXT,square_name TEXT,square_photo_url TEXT,taggee_photo_ids TEXT,taggee_data_actors BLOB);"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE TABLE contacts (person_id TEXT PRIMARY KEY,gaia_id TEXT,avatar TEXT,name TEXT,sort_key TEXT,interaction_sort_key TEXT,last_updated_time INT,profile_type INT DEFAULT(0),profile_state INT DEFAULT(0),in_my_circles INT DEFAULT(0),blocked INT DEFAULT(0) );"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CREATE TABLE circles (circle_id TEXT PRIMARY KEY,circle_name TEXT,sort_key TEXT,type INT, contact_count INT,semantic_hints INT NOT NULL DEFAULT(0),show_order INT,volume INT,notifications_enabled INT NOT NULL DEFAULT(0));"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CREATE TABLE circle_contact (link_circle_id TEXT NOT NULL,link_person_id TEXT NOT NULL,UNIQUE (link_circle_id, link_person_id), FOREIGN KEY (link_circle_id) REFERENCES circles(circle_id) ON DELETE CASCADE,FOREIGN KEY (link_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CREATE TABLE suggested_people (_id INTEGER PRIMARY KEY, suggested_person_id TEXT NOT NULL,dismissed INT DEFAULT(0),sort_order INT DEFAULT(0),category TEXT NOT NULL,category_label TEXT,category_sort_key TEXT,explanation TEXT,properties TEXT,suggestion_id TEXT );"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CREATE TABLE circle_action (gaia_id TEXT NOT NULL,notification_id INT NOT NULL,UNIQUE (gaia_id, notification_id), FOREIGN KEY (notification_id) REFERENCES notifications(notif_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CREATE TABLE photo_home (_id INTEGER PRIMARY KEY AUTOINCREMENT,type TEXT NOT NULL,photo_count INT,sort_order INT NOT NULL DEFAULT( 100 ),timestamp INT,notification_count INT);"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE TABLE photo_home_cover (photo_home_key INT NOT NULL,photo_id INT,url TEXT NOT NULL,width INT,height INT,size INT,image BLOB, FOREIGN KEY (photo_home_key) REFERENCES photo_home(_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CREATE TABLE profiles (profile_person_id TEXT PRIMARY KEY,contact_update_time INT,contact_proto BLOB,profile_update_time INT,profile_proto BLOB,FOREIGN KEY (profile_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CREATE TABLE album ( _id INTEGER PRIMARY KEY AUTOINCREMENT, album_id TEXT UNIQUE NOT NULL, title TEXT, photo_count INT, sort_order INT NOT NULL DEFAULT( 100 ), owner_id TEXT, timestamp INT, entity_version INT, album_type TEXT NOT NULL DEFAULT(\'ALL_OTHERS\'), cover_photo_id INT, stream_id TEXT, is_activity BOOLEAN DEFAULT \'0\', audience INT NOT NULL DEFAULT( -1 ));"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "CREATE TABLE album_cover (album_key INT NOT NULL,photo_id INT,url TEXT,width INT,height INT,size INT, FOREIGN KEY (album_key) REFERENCES album(_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "CREATE TABLE photo (_id INTEGER PRIMARY KEY AUTOINCREMENT, photo_id INT NOT NULL, url TEXT, title TEXT, description TEXT, action_state INT, comment_count INT, owner_id TEXT, plus_one_key INT NOT NULL, width INT, height INT, album_id TEXT NOT NULL, timestamp INT, entity_version INT, fingerprint BLOB, video_data BLOB, is_panorama INT DEFAULT(0), upload_status TEXT, downloadable BOOLEAN, UNIQUE (photo_id) FOREIGN KEY (album_id) REFERENCES album(album_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CREATE TABLE photo_comment (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, comment_id TEXT UNIQUE NOT NULL, author_id TEXT NOT NULL, content TEXT, create_time INT, truncated INT, update_time INT, plusone_data BLOB, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "CREATE TABLE photo_plusone (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, plusone_id TEXT, plusone_by_me BOOLEAN DEFAULT \'0\' NOT NULL, plusone_count INTEGER, plusone_data BLOB, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE );"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "CREATE TABLE photos_in_album (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, sort_index INT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "CREATE TABLE photos_of_user (photo_id INT NOT NULL, collection_id TEXT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CREATE TABLE photos_in_event (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, UNIQUE (photo_id, collection_id) FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "CREATE TABLE photos_in_stream (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, collection_id TEXT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "CREATE TABLE photo_shape (shape_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, subject_id TEXT, creator_id TEXT NOT NULL, status TEXT, bounds BLOB, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "CREATE TABLE conversations (_id INTEGER PRIMARY KEY, conversation_id TEXT, is_muted INT, is_visible INT, latest_event_timestamp INT, latest_message_timestamp INT, earliest_event_timestamp INT, has_older_events INT, unread_count INT, name TEXT, generated_name TEXT, latest_message_text TEXT, latest_message_image_url TEXT, latest_message_author_id TEXT, latest_message_type INT, is_group INT, is_pending_accept INT, inviter_id TEXT, fatal_error_type INT, is_pending_leave INT, is_awaiting_event_stream INT, is_awaiting_older_events INT, first_event_timestamp INT, packed_participants TEXT, UNIQUE (conversation_id ));"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "CREATE TABLE conversation_participants (conversation_id INT, participant_id TEXT, active INT, sequence INT, UNIQUE (conversation_id,participant_id) ON CONFLICT REPLACE, FOREIGN KEY (conversation_id) REFERENCES conversations(_id) ON DELETE CASCADE, FOREIGN KEY (participant_id) REFERENCES participants(participant_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CREATE TABLE participants (_id INTEGER PRIMARY KEY, participant_id TEXT UNIQUE ON CONFLICT REPLACE, full_name TEXT, first_name TEXT,type INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CREATE TABLE messages (_id INTEGER PRIMARY KEY, message_id TEXT, conversation_id INT, author_id TEXT, text TEXT, timestamp INT, status INT, type INT, notification_seen INT, image_url TEXT, FOREIGN KEY (conversation_id) REFERENCES conversations(_id) ON DELETE CASCADE,FOREIGN KEY (author_id) REFERENCES participants(participant_id) ON DELETE CASCADE, UNIQUE (conversation_id,timestamp) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CREATE TABLE messenger_suggestions (_id INTEGER PRIMARY KEY, participant_id TEXT UNIQUE ON CONFLICT REPLACE, full_name TEXT, first_name TEXT,sequence INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CREATE TABLE hangout_suggestions (_id INTEGER PRIMARY KEY, participant_id TEXT UNIQUE ON CONFLICT REPLACE, full_name TEXT, first_name TEXT,sequence INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "CREATE TABLE realtimechat_metadata (key TEXT UNIQUE, value TEXT)"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "CREATE TABLE analytics_events (event_data BLOB NOT NULL)"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "CREATE TABLE people_suggestion_events (action_type TEXT, person_id BLOB, suggestion_id BLOB, suggestion_ui TEXT, timestamp INT)"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "CREATE TABLE search (search_key TEXT NOT NULL,continuation_token TEXT,PRIMARY KEY (search_key));"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "CREATE TABLE contact_search(search_person_id TEXT NOT NULL,search_key_type TEXT NOT NULL DEFAULT(0),search_key TEXT,FOREIGN KEY (search_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "CREATE TABLE network_data_transactions(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL,time INT,sent INT,recv INT,network_duration INT,process_duration INT,req_count INT,exception TEXT);"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "CREATE TABLE network_data_stats(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL,first INT,last INT,sent INT,recv INT,network_duration INT,process_duration INT,req_count INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "CREATE TABLE platform_audience(package_name TEXT PRIMARY KEY, audience_data BLOB);"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "CREATE TABLE events(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT UNIQUE NOT NULL, activity_id TEXT UNIQUE, name TEXT, source INT, update_timestamp INT, refresh_timestamp INT, activity_refresh_timestamp INT, invitee_roster_timestamp INT, fingerprint INT NOT NULL DEFAULT(0), start_time INT NOT NULL, end_time INT NOT NULL, instant_share_end_time INT, can_invite_people INT DEFAULT (0), can_post_photos INT DEFAULT (0), can_comment INT DEFAULT(0) NOT NULL, mine INT DEFAULT (0) NOT NULL, polling_token TEXT,resume_token TEXT,display_time INT DEFAULT (0),event_data BLOB, plus_one_data BLOB, invitee_roster BLOB);"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "CREATE TABLE event_people(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT NOT NULL, gaia_id TEXT NOT NULL, CONSTRAINT uc_eventID UNIQUE (event_id, gaia_id) FOREIGN KEY (event_id) REFERENCES events(event_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CREATE TABLE plus_pages(gaia_id TEXT PRIMARY KEY, name TEXT);"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "CREATE TABLE event_activities(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT NOT NULL, type INT, owner_gaia_id TEXT, owner_name TEXT, timestamp INT, fingerprint INT NOT NULL DEFAULT(0), url TEXT,comment TEXT,data BLOB, photo_id INT,FOREIGN KEY (event_id) REFERENCES events(event_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "CREATE TABLE event_themes(_id INTEGER PRIMARY KEY AUTOINCREMENT, theme_id INTEGER UNIQUE NOT NULL, is_default INT DEFAULT(0), is_featured INT DEFAULT(0), image_url TEXT NOT NULL, placeholder_path TEXT, sort_order INT DEFAULT(0));"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "CREATE TABLE deep_link_installs(_id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INT DEFAULT(0), package_name TEXT UNIQUE NOT NULL, launch_source TEXT NOT NULL, activity_id TEXT NOT NULL, author_id TEXT NOT NULL);"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "CREATE TABLE squares (_id INTEGER PRIMARY KEY,square_id TEXT UNIQUE NOT NULL,square_name TEXT,tagline TEXT,photo_url TEXT,about_text TEXT,joinability INT NOT NULL DEFAULT(0),member_count INT NOT NULL DEFAULT(0),membership_status INT NOT NULL DEFAULT(0),is_member INT NOT NULL DEFAULT(0),suggested INT NOT NULL DEFAULT(0),post_visibility INT NOT NULL DEFAULT(-1),can_see_members INT NOT NULL DEFAULT(0),can_see_posts INT NOT NULL DEFAULT(0),can_join INT NOT NULL DEFAULT(0),can_request_to_join INT NOT NULL DEFAULT(0),can_share INT NOT NULL DEFAULT(0),can_invite INT NOT NULL DEFAULT(0),notifications_enabled INT NOT NULL DEFAULT(0),square_streams BLOB,inviter_gaia_id TEXT,sort_index INT NOT NULL DEFAULT(0),last_sync INT DEFAULT(-1),last_members_sync INT DEFAULT(-1),invitation_dismissed INT NOT NULL DEFAULT(0),suggestion_sort_index INT NOT NULL DEFAULT(0),auto_subscribe INT NOT NULL DEFAULT(0),disable_subscription INT NOT NULL DEFAULT(0),unread_count INT NOT NULL DEFAULT(0),volume INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "CREATE TABLE square_contact (link_square_id TEXT NOT NULL,link_person_id TEXT NOT NULL,membership_status INT NOT NULL DEFAULT(0),UNIQUE (link_square_id, link_person_id), FOREIGN KEY (link_square_id) REFERENCES squares(square_id) ON DELETE CASCADE,FOREIGN KEY (link_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "CREATE TABLE emotishare_data (_id INTEGER PRIMARY KEY AUTOINCREMENT,type TEXT UNIQUE ON CONFLICT REPLACE,data BLOB,generation INT DEFAULT(-1));"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "CREATE TABLE square_member_status (square_id TEXT NOT NULL,membership_status INT NOT NULL,member_count INT NOT NULL DEFAULT(0),token TEXT,UNIQUE (square_id, membership_status), FOREIGN KEY (square_id) REFERENCES squares(square_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "CREATE TABLE all_tiles (_id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, collection_id TEXT, tile_id TEXT NOT NULL, type TEXT NOT NULL, title TEXT, subtitle TEXT, image_url TEXT, image_width INTEGER, image_height INTEGER, color INTEGER, comment_count INTEGER, plusone_count INTEGER, parent_key INTEGER, parent_title TEXT, data BLOB, view_order INTEGER NOT NULL, hidden BOOLEAN NOT NULL DEFAULT \'0\', mine BOOLEAN NOT NULL DEFAULT \'0\' );"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "CREATE TABLE scroll_sections (_id INTEGER PRIMARY KEY AUTOINCREMENT, view_id TEXT NOT NULL, row INTEGER NOT NULL, tile_id TEXT NOT NULL, title TEXT, view_order INTEGER NOT NULL, landscape BOOLEAN NOT NULL DEFAULT \'0\' );"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "CREATE TABLE tile_requests (view_id TEXT NOT NULL, resume_token TEXT, last_refresh_time INTEGER NOT NULL DEFAULT \'0\', last_refresh_token TEXT );"

    aput-object v2, v0, v1

    return-object v0
.end method

.method static getViewNames()[Ljava/lang/String;
    .locals 3

    const/16 v1, 0x12

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "activities_stream_view"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "activity_view"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "comments_view"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "location_queries_view"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "conversations_view"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "participants_view"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "messages_view"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "photo_home_view"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album_view"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "photo_view"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "photos_by_album_view"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "photos_by_event_view"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "photos_by_stream_view"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "photos_by_user_view"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "photo_shape_view"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_notifications_view"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "deep_link_installs_view"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "event_people_view"

    aput-object v2, v0, v1

    return-object v0
.end method

.method static getViewSQLs()[Ljava/lang/String;
    .locals 3

    const/16 v1, 0x12

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE VIEW activities_stream_view AS SELECT activity_streams.stream_key as stream_key,activity_streams.sort_index as sort_index,activity_streams.last_activity as last_activity,activity_streams.token as token,activities._id as _id,activities.activity_id as activity_id,activities.author_id as author_id,activities.source_id as source_id,activities.source_name as source_name,activities.total_comment_count as total_comment_count,activities.plus_one_data as plus_one_data,activities.public as public,activities.spam as spam,activities.acl_display as acl_display,activities.can_comment as can_comment,activities.can_reshare as can_reshare,activities.is_plusoneable as is_plusoneable,activities.has_muted as has_muted,activities.has_read as has_read,activities.loc as loc,activities.created as created,activities.is_edited as is_edited,activities.modified as modified,activities.data_state as data_state,activities.event_id as event_id,activities.photo_collection as photo_collection,activities.whats_hot as whats_hot,activities.content_flags as content_flags,activities.annotation as annotation,activities.annotation_plaintext as annotation_plaintext,activities.title as title,activities.title_plaintext as title_plaintext,activities.original_author_id as original_author_id,activities.original_author_name as original_author_name,activities.original_author_avatar_url as original_author_avatar_url,activities.comment as comment,activities.permalink as permalink,activities.square_update as square_update,activities.square_reshare_update as square_reshare_update,activities.embed_deep_link as embed_deep_link,activities.embed_media as embed_media,activities.embed_photo_album as embed_photo_album,activities.embed_checkin as embed_checkin,activities.embed_place as embed_place,activities.embed_place_review as embed_place_review,activities.embed_skyjam as embed_skyjam,activities.embed_appinvite as embed_appinvite,activities.embed_hangout as embed_hangout,activities.embed_square as embed_square,activities.embed_emotishare as embed_emotishare,activities.promo as promo,events.event_data as event_data,contacts.name as name,contacts.avatar as avatar FROM activity_streams INNER JOIN activities ON activity_streams.activity_id=activities.activity_id LEFT OUTER JOIN contacts ON activities.author_id=contacts.gaia_id LEFT OUTER JOIN events ON activities.event_id=events.event_id WHERE data_state    IN (1, 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE VIEW activity_view AS SELECT activities._id as _id,activities.activity_id as activity_id,activities.author_id as author_id,activities.source_id as source_id,activities.source_name as source_name,activities.total_comment_count as total_comment_count,activities.plus_one_data as plus_one_data,activities.public as public,activities.spam as spam,activities.acl_display as acl_display,activities.can_comment as can_comment,activities.can_reshare as can_reshare,activities.is_plusoneable as is_plusoneable,activities.has_muted as has_muted,activities.has_read as has_read,activities.loc as loc,activities.created as created,activities.is_edited as is_edited,activities.modified as modified,activities.data_state as data_state,contacts.name as name,contacts.avatar as avatar,activities.photo_collection as photo_collection,activities.whats_hot as whats_hot,activities.content_flags as content_flags,activities.annotation as annotation,activities.annotation_plaintext as annotation_plaintext,activities.title as title,activities.title_plaintext as title_plaintext,activities.original_author_id as original_author_id,activities.original_author_name as original_author_name,activities.original_author_avatar_url as original_author_avatar_url,activities.comment as comment,activities.permalink as permalink,activities.square_update as square_update,activities.square_reshare_update as square_reshare_update,activities.embed_deep_link as embed_deep_link,activities.embed_media as embed_media,activities.embed_photo_album as embed_photo_album,activities.embed_checkin as embed_checkin,activities.embed_place as embed_place,activities.embed_place_review as embed_place_review,activities.embed_skyjam as embed_skyjam,activities.embed_appinvite as embed_appinvite,activities.embed_hangout as embed_hangout,activities.embed_square as embed_square,activities.embed_emotishare as embed_emotishare,activities.promo as promo,events.event_data as event_data FROM activities JOIN contacts ON activities.author_id=contacts.gaia_id LEFT OUTER JOIN events ON activities.event_id=events.event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE VIEW comments_view AS SELECT activity_comments._id as _id,activity_comments.activity_id as activity_id,activity_comments.comment_id as comment_id,activity_comments.author_id as author_id,activity_comments.content as content,activity_comments.created as created,activity_comments.plus_one_data as plus_one_data,contacts.name as name,contacts.avatar as avatar FROM activity_comments JOIN contacts ON activity_comments.author_id=contacts.gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE VIEW location_queries_view AS SELECT location_queries.key as key,locations._id as _id,locations.name as name,locations.location as location FROM location_queries LEFT JOIN locations ON location_queries._id=locations.qrid"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE VIEW conversations_view AS SELECT conversations._id as _id, conversations.conversation_id as conversation_id, conversations.is_muted as is_muted, conversations.is_visible as is_visible, conversations.latest_event_timestamp as latest_event_timestamp, conversations.latest_message_timestamp as latest_message_timestamp, conversations.earliest_event_timestamp as earliest_event_timestamp, conversations.has_older_events as has_older_events, conversations.unread_count as unread_count, conversations.name as name, conversations.generated_name as generated_name, conversations.latest_message_text as latest_message_text, conversations.latest_message_image_url as latest_message_image_url, conversations.latest_message_author_id as latest_message_author_id, conversations.latest_message_type as latest_message_type, conversations.is_group as is_group, conversations.is_pending_accept as is_pending_accept, conversations.inviter_id as inviter_id, conversations.fatal_error_type as fatal_error_type, conversations.is_pending_leave as is_pending_leave, conversations.is_awaiting_event_stream as is_awaiting_event_stream, conversations.is_awaiting_older_events as is_awaiting_older_events, conversations.first_event_timestamp as first_event_timestamp, conversations.packed_participants as packed_participants, participants.full_name as latest_message_author_full_name, participants.first_name as latest_message_author_first_name, participants.type as latest_message_author_type, inviter_alias.full_name as inviter_full_name, inviter_alias.first_name as inviter_first_name, inviter_alias.type as inviter_type  FROM conversations LEFT JOIN participants ON conversations.latest_message_author_id=participants.participant_id LEFT JOIN participants inviter_alias ON conversations.inviter_id=inviter_alias.participant_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE VIEW participants_view AS SELECT participants._id as _id, participants.participant_id as participant_id, participants.full_name as full_name, participants.first_name as first_name, participants.type as type, conversation_participants.conversation_id as conversation_id, conversation_participants.active as active, conversation_participants.sequence as sequence FROM participants JOIN conversation_participants ON participants.participant_id=conversation_participants.participant_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE VIEW messages_view AS SELECT messages._id as _id, messages.message_id as message_id, messages.conversation_id as conversation_id, messages.author_id as author_id, messages.text as text, messages.timestamp as timestamp, messages.status as status, messages.type as type, messages.image_url as image_url, participants.full_name as author_full_name, participants.first_name as author_first_name, participants.type as author_type FROM messages LEFT JOIN participants ON messages.author_id=participants.participant_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE VIEW photo_home_view AS SELECT photo_home._id as _id, photo_home.photo_count as photo_count, photo_home.notification_count as notification_count, photo_home.sort_order as sort_order, photo_home.timestamp as timestamp, photo_home.type as type, photo_home_cover.height as height, photo_home_cover.image as image, photo_home_cover.photo_id as photo_id, photo_home_cover.photo_home_key as photo_home_key, photo_home_cover.size as size, photo_home_cover.url as url, photo_home_cover.width as width FROM photo_home LEFT JOIN photo_home_cover ON photo_home._id=photo_home_cover.photo_home_key"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE VIEW album_view AS SELECT album._id as _id, album.album_id as album_id, album.entity_version as entity_version, album.is_activity as is_activity, album.owner_id as owner_id, album.photo_count as photo_count, album.sort_order as sort_order, album.stream_id as stream_id, album.timestamp as timestamp, album.title as title, album.cover_photo_id as cover_photo_id, album.album_type as album_type, album.audience as audience, album_cover.album_key as album_key, album_cover.height as height, album_cover.photo_id as photo_id, album_cover.size as size, album_cover.url as url, album_cover.width as width FROM album LEFT JOIN album_cover ON album._id=album_cover.album_key"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE VIEW photo_shape_view AS SELECT photo_shape.photo_id as photo_id, photo_shape.bounds as bounds, photo_shape.creator_id as creator_id, photo_shape.shape_id as shape_id, photo_shape.status as status, photo_shape.subject_id as subject_id, (SELECT a.name FROM contacts as a WHERE a.gaia_id=photo_shape.creator_id ) AS creator_name, (SELECT b.name FROM contacts as b WHERE b.gaia_id=photo_shape.subject_id ) AS subject_name FROM photo_shape"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CREATE VIEW message_notifications_view AS SELECT messages._id as _id, messages.message_id as message_id, messages.conversation_id as conversation_id, messages.author_id as author_id, messages.text as text, messages.image_url as image_url, messages.timestamp as timestamp, messages.status as status, messages.type as type, messages.notification_seen as notification_seen, author_alias.full_name as author_full_name, author_alias.first_name as author_first_name, author_alias.type as author_type, conversations.is_muted as conversation_muted, conversations.is_visible as conversation_visible, conversations.is_group as conversation_group, conversations.is_pending_accept as conversation_pending_accept, conversations.is_pending_leave as conversation_pending_leave, conversations.name as conversation_name, conversations.generated_name as generated_name, inviter_alias.participant_id as inviter_id, inviter_alias.full_name as inviter_full_name, inviter_alias.first_name as inviter_first_name, inviter_alias.type as inviter_type FROM messages LEFT JOIN participants author_alias ON messages.author_id=author_alias.participant_id LEFT JOIN conversations ON messages.conversation_id=conversations._id LEFT JOIN participants inviter_alias ON conversations.inviter_id=inviter_alias.participant_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CREATE VIEW deep_link_installs_view AS SELECT deep_link_installs._id as _id,deep_link_installs.timestamp as timestamp,deep_link_installs.package_name as package_name,deep_link_installs.launch_source as launch_source,contacts.name as name,activities.source_name as source_name,activities.embed_deep_link as embed_deep_link,activities.embed_appinvite as embed_appinvite FROM deep_link_installs INNER JOIN activities ON deep_link_installs.activity_id=activities.activity_id INNER JOIN contacts ON deep_link_installs.author_id=contacts.gaia_id;"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "CREATE VIEW event_people_view AS SELECT event_people._id as _id,event_people.event_id as event_id,event_people.gaia_id as gaia_id,contacts.person_id as person_id,contacts.name as name,contacts.sort_key as sort_key,contacts.avatar as avatar,contacts.last_updated_time as last_updated_time,contacts.profile_type as profile_type,contacts.profile_state as profile_state,contacts.in_my_circles as in_my_circles,contacts.blocked as blocked FROM event_people INNER JOIN contacts ON event_people.gaia_id=contacts.gaia_id;"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static getsActivitiesFirstPageSize(Landroid/content/Context;)I
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsProvider;->ensureActivitiesPageSizes(Landroid/content/Context;)V

    sget v0, Lcom/google/android/apps/plus/content/EsProvider;->sActivitiesFirstPageSize:I

    return v0
.end method

.method private static insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .param p0    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "type"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "contact_count"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "semantic_hints"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "show_order"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "volume"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "circles"

    const-string v2, "circle_id"

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    return-void
.end method

.method public static insertVirtualCircles(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    const/4 v3, -0x1

    const-string v0, "v.nearby"

    sget v1, Lcom/google/android/apps/plus/R$string;->stream_nearby:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2710

    invoke-static {p1, v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V

    const-string v0, "v.all.circles"

    sget v1, Lcom/google/android/apps/plus/R$string;->stream_circles:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V

    const-string v0, "v.whatshot"

    sget v1, Lcom/google/android/apps/plus/R$string;->stream_whats_hot:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {p1, v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method private static varargs isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 11
    .param p0    # [Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v8, 0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v8

    :cond_1
    array-length v10, p1

    if-ne v10, v8, :cond_2

    aget-object v2, p1, v9

    move-object v0, p0

    array-length v5, p0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_4

    aget-object v7, v0, v3

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move-object v0, p0

    array-length v5, p0

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v5, :cond_4

    aget-object v7, v0, v4

    move-object v1, p1

    array-length v6, p1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v6, :cond_3

    aget-object v2, v1, v3

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    :cond_4
    move v8, v9

    goto :goto_0
.end method

.method public static localeChanged(Landroid/content/Context;)V
    .locals 6
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    const-string v4, "com.google.android.apps.plus.content.EsProvider"

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {v3, v4, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private static preparePeopleSearchQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8
    .param p0    # Landroid/database/sqlite/SQLiteQueryBuilder;
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Ljava/lang/String;

    const/16 v7, 0x25

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const/4 v2, 0x0

    :goto_0
    array-length v4, v0

    if-ge v2, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS filtered_person_id, MIN((CASE WHEN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_key_type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " THEN search_key"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ELSE NULL END)) AS email"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN contact_search"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON (contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=search_person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_key GLOB "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v0, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2a

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND in_my_circles"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!=0 GROUP BY filtered_person_id, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_key_type INTERSECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0xb

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UNION SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "contacts."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "person_id AS filtered_person_id, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " NULL AS email"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE gaia_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") AND ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "name LIKE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR name"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIKE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "% "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "contacts JOIN ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") ON (contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=filtered_person_id) LEFT OUTER JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "circle_contact ON ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "contacts."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "person_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "circle_contact."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "link_person_id)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v4, "("

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "gaia_id"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, " != \'"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "\' OR "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "gaia_id"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, " IS NULL"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, ")"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    if-nez p2, :cond_2

    const-string v4, " AND "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "profile_type"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, " != "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, " AND "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "0"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_3
    if-nez p5, :cond_4

    const-string v4, " AND "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, "in_my_circles"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v4, " = 0"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    :cond_4
    return-void
.end method

.method private static varargs prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p0    # [Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    array-length v4, p1

    if-nez v4, :cond_2

    :cond_0
    move-object v1, p0

    :cond_1
    :goto_0
    return-object v1

    :cond_2
    if-nez p0, :cond_3

    move v2, v3

    :goto_1
    array-length v0, p1

    add-int v4, v2, v0

    new-array v1, v4, [Ljava/lang/String;

    invoke-static {p1, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    if-lez v2, :cond_1

    invoke-static {p0, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_3
    array-length v2, p0

    goto :goto_1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Delete not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.accounts"

    :goto_0
    return-object v0

    :sswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.activities"

    goto :goto_0

    :sswitch_2
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.comments"

    goto :goto_0

    :sswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.locations"

    goto :goto_0

    :sswitch_4
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.notifications"

    goto :goto_0

    :sswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.contacts"

    goto :goto_0

    :sswitch_6
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.conversations"

    goto :goto_0

    :sswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.participants"

    goto :goto_0

    :sswitch_8
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.messages"

    goto :goto_0

    :sswitch_9
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.message_notifications"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x1e -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x46 -> :sswitch_5
        0x48 -> :sswitch_5
        0x64 -> :sswitch_6
        0x6e -> :sswitch_7
        0x78 -> :sswitch_8
        0xa0 -> :sswitch_9
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Insert not supported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public declared-synchronized openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    const-string v4, "EsProvider"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Open asset file uri: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " -> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_1

    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported URI: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_1
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    :try_start_2
    const-string v4, "file"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "EsProvider"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "EsProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Opening panorama file: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x10000000

    invoke-static {v4, v5}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    :try_start_3
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v4

    :catchall_1
    move-exception v4

    :try_start_4
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 33
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const-string v9, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    if-nez v24, :cond_0

    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Every URI must have the \'account\' parameter specified.\nURI: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_0
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    const/4 v13, 0x0

    const-string v9, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v12, p4

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v29

    const-string v9, "EsProvider"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "EsProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "QUERY URI: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " -> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    sparse-switch v29, :sswitch_data_0

    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unknown URI "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    :sswitch_0
    const-string v9, "account_status"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    :cond_2
    :goto_0
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    move-object/from16 v21, p5

    :cond_3
    const-string v9, "EsProvider"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v17, "EsProvider"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v9, "QUERY: "

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v14, 0x0

    move-object v9, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v15, p5

    move-object/from16 v16, v5

    invoke-virtual/range {v9 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-static {v0, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    move/from16 v0, v23

    invoke-static {v9, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;I)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    const/16 v20, 0x0

    move-object v14, v2

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, v12

    move-object/from16 v19, v13

    move-object/from16 v22, v5

    invoke-virtual/range {v14 .. v22}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    const-string v9, "EsProvider"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_5

    const-string v9, "EsProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "QUERY results: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v9, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-object v26

    :sswitch_1
    const-string v9, "activities"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_2
    const-string v9, "activity_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    :sswitch_3
    const-string v9, "activity_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_4
    const-string v9, "activities_stream_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "stream_key"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_5
    const-string v9, "activities_stream_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "(\'g:\'||author_id) IN ( SELECT link_person_id FROM circle_contact WHERE link_circle_id=?)"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    if-eqz v5, :cond_6

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v27

    const-wide/16 v9, 0x14

    cmp-long v9, v27, v9

    if-lez v9, :cond_2

    const-wide/16 v9, 0x14

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :cond_6
    const-wide/16 v9, 0x14

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :sswitch_6
    const-string v9, "comments_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "activity_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_7
    const-string v9, "location_queries_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "key"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_8
    const-string v9, "notifications"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    if-eqz v5, :cond_8

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v27

    const-wide/16 v9, 0xc8

    cmp-long v9, v27, v9

    if-lez v9, :cond_7

    const-wide/16 v9, 0xc8

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    :cond_7
    :goto_1
    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_8
    const-wide/16 v9, 0xc8

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :sswitch_9
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "member_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    const-string v30, "SELECT link_circle_id,link_person_id FROM circle_contact JOIN contacts AS c  ON (c.person_id=link_person_id) ORDER BY c.sort_key, UPPER(c.name)"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "circles LEFT OUTER JOIN ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") AS "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circle_contact ON ( "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circle_contact."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "link_circle_id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circles."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circle_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v13, "circle_id"

    :goto_2
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :cond_9
    const-string v9, "circles"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_2

    :sswitch_a
    const-string v9, "person_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    :sswitch_b
    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "contact_update_time"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "contact_proto"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string v11, "profile_update_time"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const-string v11, "profile_proto"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    const-string v32, "contacts LEFT OUTER JOIN profiles ON (contacts.person_id=profiles.profile_person_id)"

    :goto_3
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "packed_circle_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LEFT OUTER JOIN circle_contact ON ( circle_contact.link_person_id = contacts.person_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const-string v13, "person_id"

    :cond_a
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const-string v21, "sort_key, UPPER(name)"

    goto/16 :goto_0

    :cond_b
    const-string v32, "contacts"

    goto :goto_3

    :sswitch_c
    const-string v9, "contacts JOIN circle_contact ON (contacts.person_id=circle_contact.link_person_id) JOIN circles ON (circle_contact.link_circle_id = circles.circle_id)"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "person_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " IN ("

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "SELECT "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "link_person_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " FROM "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "circle_contact"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " WHERE "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "link_circle_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const-string v9, ")"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    if-nez p5, :cond_c

    const-string v21, "UPPER(name)"

    :goto_4
    const-string v13, "person_id"

    goto/16 :goto_0

    :cond_c
    move-object/from16 v21, p5

    goto :goto_4

    :sswitch_d
    const-string v32, "contacts INNER JOIN square_contact ON (contacts.person_id=square_contact.link_person_id)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "packed_circle_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_d

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LEFT OUTER JOIN circle_contact ON (circle_contact.link_person_id = contacts.person_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const-string v13, "person_id"

    :cond_d
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "link_square_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->SQUARE_CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const-string v21, "square_contact.membership_status, name"

    goto/16 :goto_0

    :sswitch_e
    const-string v32, "contacts JOIN suggested_people ON (contacts.person_id=suggested_people.suggested_person_id)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "packed_circle_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LEFT OUTER JOIN circle_contact ON ( circle_contact.link_person_id = contacts.person_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    const-string v13, "suggested_people._id"

    :cond_e
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const-string p3, "dismissed=0 AND blocked=0"

    const-string v21, "CAST (category_sort_key AS INTEGER),sort_order"

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v31

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_12

    const-string v3, ""

    :goto_5
    const-string v9, "self_gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "true"

    const-string v10, "plus_pages"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v9, "true"

    const-string v10, "in_circles"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    const-string v9, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    const/4 v8, 0x0

    if-eqz v25, :cond_f

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "SELECT author_id FROM activities WHERE activity_id =  "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v25 .. v25}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " UNION "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "SELECT author_id FROM activity_comments WHERE activity_id = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v25 .. v25}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :cond_f
    const-string v9, "+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_10

    const-string v9, "@"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_11

    :cond_10
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_11
    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/plus/content/EsProvider;->preparePeopleSearchQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    const-string v13, "person_id"

    const/4 v5, 0x0

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const-string v21, "UPPER(name)"

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "gaia_id IN ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") DESC,"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_0

    :cond_12
    const/4 v9, 0x2

    move-object/from16 v0, v31

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :sswitch_10
    const-string v9, "conversations_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_11
    const-string v9, "messenger_suggestions"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_12
    const-string v9, "hangout_suggestions"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_13
    const-string v9, "participants_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "conversation_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_14
    const-string v9, "message_notifications_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_15
    const-string v9, "messages_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "conversation_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_16
    const-string v9, "photo_home_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_17
    const-string v9, "album_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "owner_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "title"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " IS NOT NULL"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "url"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " IS NOT NULL"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "is_activity"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " = 0"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_18
    const-string v9, "album_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "album_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "owner_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "title"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " IS NOT NULL"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "is_activity"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " = 0"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x2

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    const/4 v11, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_19
    const-string v9, "photo_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "photo_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    const-string v5, "1"

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_1a
    const-string v9, "photos_by_album_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "album_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_1b
    const-string v9, "photos_by_event_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "event_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_1c
    const-string v9, "photos_by_user_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "photo_of_user_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_1d
    const-string v9, "photos_by_stream_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "stream_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "owner_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    const/4 v9, 0x2

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    const/4 v11, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    :sswitch_1e
    const-string v9, "photo_home_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "type"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=\'"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "photos_of_me"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "\'"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_NOTIFICATION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_1f
    const-string v9, "photo_comment JOIN contacts ON photo_comment.author_id=contacts.gaia_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "photo_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_20
    const-string v9, "photo_shape_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "photo_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    move-object/from16 v21, p5

    goto/16 :goto_0

    :sswitch_21
    const-string v9, "network_data_transactions"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_22
    const-string v9, "network_data_stats"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_23
    const-string v9, "platform_audience"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    const-string v9, "package_name"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "="

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_24
    const-string v9, "plus_pages"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    :sswitch_25
    const-string v9, "square_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    :sswitch_26
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "inviter_name"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "inviter_photo_url"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_13

    const-string v32, "squares LEFT OUTER JOIN contacts ON (squares.inviter_gaia_id=contacts.gaia_id)"

    :goto_6
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_14

    move-object/from16 v21, p5

    goto/16 :goto_0

    :cond_13
    const-string v32, "squares"

    goto :goto_6

    :cond_14
    const-string v21, "sort_index"

    goto/16 :goto_0

    :sswitch_27
    const-string v9, "emotishare_data"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->EMOTISHARE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    const/16 v21, 0x0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x14 -> :sswitch_3
        0x15 -> :sswitch_4
        0x16 -> :sswitch_2
        0x17 -> :sswitch_5
        0x18 -> :sswitch_1
        0x1e -> :sswitch_6
        0x28 -> :sswitch_7
        0x32 -> :sswitch_8
        0x3c -> :sswitch_9
        0x46 -> :sswitch_b
        0x47 -> :sswitch_c
        0x48 -> :sswitch_a
        0x49 -> :sswitch_e
        0x4a -> :sswitch_f
        0x4b -> :sswitch_d
        0x64 -> :sswitch_10
        0x6e -> :sswitch_13
        0x73 -> :sswitch_11
        0x74 -> :sswitch_12
        0x78 -> :sswitch_15
        0x82 -> :sswitch_16
        0x84 -> :sswitch_17
        0x86 -> :sswitch_19
        0x87 -> :sswitch_1a
        0x8a -> :sswitch_1d
        0x8b -> :sswitch_1c
        0x8c -> :sswitch_1e
        0x8d -> :sswitch_1f
        0x8f -> :sswitch_20
        0x90 -> :sswitch_18
        0x91 -> :sswitch_1b
        0xa0 -> :sswitch_14
        0xb4 -> :sswitch_21
        0xb5 -> :sswitch_22
        0xb6 -> :sswitch_23
        0xbe -> :sswitch_24
        0xd2 -> :sswitch_26
        0xd3 -> :sswitch_25
        0xd4 -> :sswitch_27
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Update not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
