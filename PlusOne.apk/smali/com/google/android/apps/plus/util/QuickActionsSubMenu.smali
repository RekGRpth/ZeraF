.class final Lcom/google/android/apps/plus/util/QuickActionsSubMenu;
.super Ljava/lang/Object;
.source "QuickActionsSubMenu.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/SubMenu;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mHeaderTitle:Ljava/lang/CharSequence;

.field private final mItem:Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/util/QuickActionsMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/util/QuickActionsMenuItem;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/util/QuickActionsMenuItem;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p4    # Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItem:Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    iput-object p3, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    iput-object p4, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    return-void
.end method

.method private static visible(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/view/MenuItem;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v2, v4, :cond_2

    :goto_1
    return-object p0

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move-object p0, v3

    goto :goto_1
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    new-instance v0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    iget-object v7, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/content/ComponentName;
    .param p5    # [Landroid/content/Intent;
    .param p6    # Landroid/content/Intent;
    .param p7    # I
    .param p8    # [Landroid/view/MenuItem;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final clearHeader()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mHeaderTitle:Ljava/lang/CharSequence;

    return-void
.end method

.method public final close()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 3
    .param p1    # I

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    if-ne p1, v2, :cond_0

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic getItem()Landroid/view/MenuItem;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItem:Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    return-object v0
.end method

.method public final bridge synthetic getItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    check-cast p1, Landroid/app/AlertDialog;

    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->invoke()Z

    return-void
.end method

.method public final performIdentifierAction(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final removeItem(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setGroupEnabled(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setGroupVisible(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mHeaderTitle:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # Landroid/view/View;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setQwertyMode(Z)V
    .locals 1
    .param p1    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final show()V
    .locals 8

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->visible(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mContext:Landroid/content/Context;

    const v6, 0x1090003

    const v7, 0x1020014

    invoke-direct {v0, v5, v6, v7, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    move-object v3, p0

    invoke-virtual {v1, v0, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x106000d

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mHeaderTitle:Ljava/lang/CharSequence;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mHeaderTitle:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
