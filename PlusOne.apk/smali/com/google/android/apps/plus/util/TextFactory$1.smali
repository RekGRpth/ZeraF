.class final Lcom/google/android/apps/plus/util/TextFactory$1;
.super Landroid/database/ContentObserver;
.source "TextFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/util/TextFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$res:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/content/res/Resources;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/apps/plus/util/TextFactory$1;->val$res:Landroid/content/res/Resources;

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 6
    .param p1    # Z

    const/4 v0, 0x0

    # getter for: Lcom/google/android/apps/plus/util/TextFactory;->sTextPaints:Landroid/util/SparseArray;
    invoke-static {}, Lcom/google/android/apps/plus/util/TextFactory;->access$000()Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    # getter for: Lcom/google/android/apps/plus/util/TextFactory;->sTextPaints:Landroid/util/SparseArray;
    invoke-static {}, Lcom/google/android/apps/plus/util/TextFactory;->access$000()Landroid/util/SparseArray;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;

    iget-object v3, v1, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;->paint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/TextFactory$1;->val$res:Landroid/content/res/Resources;

    iget-object v5, v1, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;->style:Lcom/google/android/apps/plus/util/TextFactory$TextStyle;

    iget v5, v5, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->sizeRes:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
