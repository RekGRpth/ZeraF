.class public final Lcom/google/android/apps/plus/util/StreamLayoutInfo;
.super Ljava/lang/Object;
.source "StreamLayoutInfo.java"


# instance fields
.field public columnWidth:I

.field public paddingWidth:I

.field public separatorWidth:I

.field public streamHeight:I

.field public streamWidth:I

.field public totalColumns:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v7, "window"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v7, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    iget v7, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamHeight:I

    iget v7, v5, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v7, :cond_0

    sget v7, Lcom/google/android/apps/plus/R$dimen;->riviera_padding_phone:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    :goto_0
    iget v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    mul-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    sget v7, Lcom/google/android/apps/plus/R$dimen;->riviera_desired_separator_width:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    sget v7, Lcom/google/android/apps/plus/R$dimen;->riviera_desired_card_width:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v0, v7

    iget v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    sub-int/2addr v7, v0

    iget v8, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    add-int/2addr v8, v0

    div-int/2addr v7, v8

    add-int/lit8 v1, v7, 0x1

    const/4 v7, 0x1

    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v7

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    iget v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    if-ge v7, v0, :cond_1

    iget v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    :goto_1
    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->columnWidth:I

    return-void

    :cond_0
    sget v7, Lcom/google/android/apps/plus/R$dimen;->riviera_padding_percent_tablet:I

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    iget v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    int-to-float v7, v7

    mul-float/2addr v7, v3

    float-to-int v7, v7

    iput v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    goto :goto_0

    :cond_1
    iget v7, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    add-int/lit8 v8, v8, -0x1

    iget v9, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    mul-int/2addr v8, v9

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    div-int/2addr v7, v8

    goto :goto_1
.end method


# virtual methods
.method public final getNumberOfColumnsForWidth(I)I
    .locals 4
    .param p1    # I

    iget v2, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->columnWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    sub-int v0, v2, v3

    iget v1, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    :goto_0
    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    if-le p1, v0, :cond_0

    :goto_1
    return v1

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->columnWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public final getWidthForColumns(I)I
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->columnWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    add-int/2addr v0, v1

    mul-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    sub-int/2addr v0, v1

    return v0
.end method
