.class final Lcom/google/android/apps/plus/util/QuickActionsContextMenu;
.super Landroid/app/Dialog;
.source "QuickActionsContextMenu.java"

# interfaces
.implements Landroid/view/ContextMenu;


# instance fields
.field private final mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/util/QuickActionsMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private mLeftAligned:Z

.field private final mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private mShowAbove:Z

.field private mVertical:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;ZZZ)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p3    # Landroid/view/MenuItem$OnMenuItemClickListener;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    sget v2, Lcom/google/android/apps/plus/R$style;->QuickActions:I

    invoke-direct {p0, p1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-boolean p4, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mLeftAligned:Z

    iput-boolean p6, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mVertical:Z

    iput-boolean p5, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mShowAbove:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    iput-object p3, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-virtual {v1, v2, v3}, Landroid/view/Window;->setLayout(II)V

    if-eqz p4, :cond_1

    if-eqz p5, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->tooltip_top_left_background:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->setCanceledOnTouchOutside(Z)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->tooltip_bottom_left_background:I

    goto :goto_0

    :cond_1
    if-eqz p5, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$drawable;->tooltip_top_right_background:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$drawable;->tooltip_bottom_right_background:I

    goto :goto_0
.end method

.method private add(IIILjava/lang/CharSequence;)Lcom/google/android/apps/plus/util/QuickActionsMenuItem;
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    iget-object v7, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    new-instance v0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public final add(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(IIII)Landroid/view/MenuItem;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->add(IIILjava/lang/CharSequence;)Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    move-result-object v1

    return-object v1
.end method

.method public final bridge synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->add(IIILjava/lang/CharSequence;)Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, p1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->add(IIILjava/lang/CharSequence;)Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/content/ComponentName;
    .param p5    # [Landroid/content/Intent;
    .param p6    # Landroid/content/Intent;
    .param p7    # I
    .param p8    # [Landroid/view/MenuItem;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->addSubMenu(IIII)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v1

    return-object v1
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->add(IIILjava/lang/CharSequence;)Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

    iget-object v3, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/util/QuickActionsMenuItem;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->setSubMenu(Lcom/google/android/apps/plus/util/QuickActionsSubMenu;)V

    return-object v2
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0, v0, p1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    return-object v0
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final clearHeader()V
    .locals 0

    return-void
.end method

.method public final close()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->dismiss()V

    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 5
    .param p1    # I

    iget-object v4, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    if-ne p1, v4, :cond_1

    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v1}, Landroid/view/MenuItem;->hasSubMenu()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/view/SubMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MenuItem;

    return-object v0
.end method

.method public final hasVisibleItems()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    return v0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v13, 0x0

    const/4 v12, 0x0

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    iget-boolean v10, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mVertical:Z

    if-eqz v10, :cond_2

    sget v8, Lcom/google/android/apps/plus/R$layout;->quick_actions_dialog_vertical:I

    :goto_0
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->setContentView(I)V

    iget-boolean v10, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mVertical:Z

    if-eqz v10, :cond_3

    sget v7, Lcom/google/android/apps/plus/R$layout;->quick_actions_item_vertical:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v10, Lcom/google/android/apps/plus/R$id;->quick_actions_buttons:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    iget-object v10, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->isVisible()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v10

    if-eqz v10, :cond_1

    iget-boolean v10, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mVertical:Z

    if-eqz v10, :cond_4

    sget v10, Lcom/google/android/apps/plus/R$layout;->quick_actions_divider_horizontal:I

    :goto_3
    const/4 v11, 0x1

    invoke-virtual {v5, v10, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_1
    invoke-virtual {v5, v7, v1, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    invoke-virtual {v4, v12, v12, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, " "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    invoke-virtual {v0, v4, v13, v13, v13}, Landroid/widget/Button;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->isEnabled()Z

    move-result v10

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v10, Lcom/google/android/apps/plus/util/QuickActionsContextMenu$1;

    invoke-direct {v10, p0, v6}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu$1;-><init>(Lcom/google/android/apps/plus/util/QuickActionsContextMenu;Lcom/google/android/apps/plus/util/QuickActionsMenuItem;)V

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_2

    :cond_2
    sget v8, Lcom/google/android/apps/plus/R$layout;->quick_actions_dialog:I

    goto/16 :goto_0

    :cond_3
    sget v7, Lcom/google/android/apps/plus/R$layout;->quick_actions_item:I

    goto/16 :goto_1

    :cond_4
    sget v10, Lcom/google/android/apps/plus/R$layout;->quick_actions_divider_vertical:I

    goto :goto_3

    :cond_5
    invoke-virtual {v0, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_6
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/app/Dialog;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->dismiss()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final performIdentifierAction(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final removeItem(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setGroupEnabled(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setGroupVisible(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setHeaderIcon(I)Landroid/view/ContextMenu;
    .locals 0
    .param p1    # I

    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/ContextMenu;
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/ContextMenu;
    .locals 0
    .param p1    # I

    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/ContextMenu;
    .locals 0
    .param p1    # Landroid/view/View;

    return-object p0
.end method

.method public final setQwertyMode(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method final showAnchoredAt(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mShowAbove:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x50

    :goto_0
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-boolean v2, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mLeftAligned:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    :goto_1
    or-int/2addr v2, v3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->show()V

    return-void

    :cond_0
    const/16 v2, 0x30

    goto :goto_0

    :cond_1
    const/4 v2, 0x5

    goto :goto_1
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
