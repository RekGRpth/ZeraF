.class public final Lcom/google/android/apps/plus/util/DeepLinkHelper;
.super Ljava/lang/Object;
.source "DeepLinkHelper.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mContext:Landroid/content/Context;

.field private mCreationSourceId:Ljava/lang/String;

.field private mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

.field private mRequestSpaceOffset:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/HostedFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/HostedFragment;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mRequestSpaceOffset:I

    iput-object p1, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    iput-object p3, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final handleActivityResult$6eb84b56(II)Z
    .locals 8
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mRequestSpaceOffset:I

    add-int/2addr p1, v4

    if-eq p1, v3, :cond_0

    if-ne p1, v6, :cond_3

    :cond_0
    const-string v4, "extra_creation_source_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mCreationSourceId:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-ne p1, v6, :cond_1

    move v2, v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v4, -0x1

    if-ne p2, v4, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_4

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v7

    invoke-static {v5, v6, v4, v7, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_2
    :goto_1
    move v2, v3

    :cond_3
    return v2

    :cond_4
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_6

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_2
    iget-object v7, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v7

    invoke-static {v5, v6, v4, v7, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_6
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_2
.end method

.method public final launchDeepLink$5724b368(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mCreationSourceId:Ljava/lang/String;

    const-string v1, "extra_creation_source_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mCreationSourceId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    const/4 v10, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPackageInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v10, 0x1

    if-eqz p8, :cond_2

    const-string v1, "stream_interactive_post"

    :goto_1
    move-object/from16 v0, p5

    invoke-static {v11, v12, v0, v1}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getContinueIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    if-eqz v9, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/phone/HostedFragment;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p8, :cond_3

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v6

    invoke-static {v2, v4, v1, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_3
    return-void

    :cond_2
    const-string v1, "stream"

    goto :goto_1

    :cond_3
    :try_start_1
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to start deep linked Activity with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    if-nez v10, :cond_8

    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v5, p7

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getInstallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    iget v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mRequestSpaceOffset:I

    if-eqz p8, :cond_5

    const/4 v1, 0x2

    :goto_4
    add-int/2addr v1, v4

    invoke-virtual {v2, v9, v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p8, :cond_6

    const-string v6, "stream_install_interactive_post"

    :goto_5
    move-object v4, p2

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p8, :cond_7

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_6
    iget-object v6, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v6

    invoke-static {v2, v4, v1, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_3

    :cond_5
    const/4 v1, 0x1

    goto :goto_4

    :cond_6
    const-string v6, "stream_install"

    goto :goto_5

    :cond_7
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_6

    :cond_8
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mCreationSourceId:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-static {v1, v2, v0, v4, p2}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v10, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p8, :cond_9

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_7
    iget-object v6, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v6

    invoke-static {v2, v4, v1, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_9
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_7

    :cond_a
    invoke-static {v11}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPlayStoreInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p8, :cond_b

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_8
    iget-object v6, p0, Lcom/google/android/apps/plus/util/DeepLinkHelper;->mHostFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v6

    invoke-static {v2, v4, v1, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_b
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_8
.end method
