.class final Lcom/google/android/apps/plus/util/QuickActionsMenuItem;
.super Ljava/lang/Object;
.source "QuickActionsMenuItem.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private final mGroupId:I

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIntent:Landroid/content/Intent;

.field private final mItemId:I

.field private final mMenuClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final mOrder:I

.field private mSubMenu:Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleCondensed:Ljava/lang/CharSequence;

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILjava/lang/CharSequence;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/CharSequence;
    .param p6    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p7    # Landroid/view/MenuItem$OnMenuItemClickListener;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mGroupId:I

    iput p3, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mItemId:I

    iput p4, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mOrder:I

    iput-object p5, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitle:Ljava/lang/CharSequence;

    iput-object p6, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mEnabled:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mVisible:Z

    iput-object p7, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-void
.end method


# virtual methods
.method public final collapseActionView()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final expandActionView()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getActionView()Landroid/view/View;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getAlphabeticShortcut()C
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getGroupId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mGroupId:I

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public final getItemId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mItemId:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public final getNumericShortcut()C
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getOrder()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mOrder:I

    return v0
.end method

.method public final bridge synthetic getSubMenu()Landroid/view/SubMenu;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mSubMenu:Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitle:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final hasSubMenu()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mSubMenu:Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final invoke()Z
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v2, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v2, p0}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIntent:Landroid/content/Intent;

    if-eqz v2, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mSubMenu:Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/QuickActionsSubMenu;->show()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final isActionViewExpanded()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isCheckable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isChecked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mEnabled:Z

    return v0
.end method

.method public final isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mVisible:Z

    return v0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Landroid/view/ActionProvider;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final setActionView(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Landroid/view/View;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .param p1    # C

    return-object p0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    return-object p0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    return-object p0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mEnabled:Z

    return-object p0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .locals 2
    .param p1    # I

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object p0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mIntent:Landroid/content/Intent;

    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .param p1    # C

    return-object p0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 1
    .param p1    # Landroid/view/MenuItem$OnActionExpandListener;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .locals 0
    .param p1    # C
    .param p2    # C

    return-object p0
.end method

.method public final setShowAsAction(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method final setSubMenu(Lcom/google/android/apps/plus/util/QuickActionsSubMenu;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mSubMenu:Lcom/google/android/apps/plus/util/QuickActionsSubMenu;

    return-void
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mVisible:Z

    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/QuickActionsMenuItem;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
