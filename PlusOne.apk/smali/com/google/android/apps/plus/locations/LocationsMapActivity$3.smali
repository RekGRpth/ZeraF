.class final Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;
.super Ljava/lang/Object;
.source "LocationsMapActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/locations/LocationsMapActivity;->prepareActionBar(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iput-object p2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onUpButtonClick()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->val$activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v3}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$400(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    invoke-static {v1, v2}, Lvedroid/support/v4/app/NavUtils;->shouldUpRecreateTask(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->val$activity:Landroid/app/Activity;

    invoke-static {v1}, Lvedroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$400(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    invoke-virtual {v0}, Lvedroid/support/v4/app/TaskStackBuilder;->startActivities()V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->onBackPressed()V

    goto :goto_0
.end method
