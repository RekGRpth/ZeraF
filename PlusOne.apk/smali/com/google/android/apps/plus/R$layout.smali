.class public final Lcom/google/android/apps/plus/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final account_name_list_item_single_choice:I = 0x7f040000

.field public static final account_selection_activity:I = 0x7f040001

.field public static final acl_dropdown:I = 0x7f040002

.field public static final acl_row_view:I = 0x7f040003

.field public static final action_bar_progress_spinner_layout:I = 0x7f040004

.field public static final action_bar_search_bar:I = 0x7f040005

.field public static final action_bar_spinner:I = 0x7f040006

.field public static final added_to_circle_activity:I = 0x7f040007

.field public static final album_column_grid_view_item:I = 0x7f040008

.field public static final album_spinner_item:I = 0x7f040009

.field public static final audience_fragment:I = 0x7f04000a

.field public static final audience_view:I = 0x7f04000b

.field public static final audience_view_text_only:I = 0x7f04000c

.field public static final ban_user_dialog:I = 0x7f04000d

.field public static final block_profile_confirm_dialog:I = 0x7f04000e

.field public static final bubble_background_layout:I = 0x7f04000f

.field public static final button_bar:I = 0x7f040010

.field public static final button_bar_dark:I = 0x7f040011

.field public static final checkin_list:I = 0x7f040012

.field public static final circle_properties_dialog:I = 0x7f040013

.field public static final circle_selection_activity:I = 0x7f040014

.field public static final circle_settings:I = 0x7f040015

.field public static final circle_spinner_dropdown_item:I = 0x7f040016

.field public static final circles_item_new_circle:I = 0x7f040017

.field public static final circles_multiple_select_fragment:I = 0x7f040018

.field public static final client_oob_action_bar:I = 0x7f040019

.field public static final comment_edit_dialog:I = 0x7f04001a

.field public static final compose_bar:I = 0x7f04001b

.field public static final compose_gallery_image_container:I = 0x7f04001c

.field public static final compose_message:I = 0x7f04001d

.field public static final container_tile_view:I = 0x7f04001e

.field public static final conversation_activity:I = 0x7f04001f

.field public static final conversation_list_fragment:I = 0x7f040020

.field public static final conversation_list_item_view:I = 0x7f040021

.field public static final conversation_rename:I = 0x7f040022

.field public static final conversation_tile:I = 0x7f040023

.field public static final edit_audience_activity:I = 0x7f040024

.field public static final edit_audience_fragment:I = 0x7f040025

.field public static final edit_comment_activity:I = 0x7f040026

.field public static final edit_comment_fragment:I = 0x7f040027

.field public static final edit_event_fragment:I = 0x7f040028

.field public static final edit_post_activity:I = 0x7f040029

.field public static final emotishare_view:I = 0x7f04002a

.field public static final empty_layout:I = 0x7f04002b

.field public static final event_instant_share_dialog_view:I = 0x7f04002c

.field public static final event_invitee_list_section_footer:I = 0x7f04002d

.field public static final event_location_activity:I = 0x7f04002e

.field public static final event_location_fragment:I = 0x7f04002f

.field public static final event_rsvp_attending:I = 0x7f040030

.field public static final event_rsvp_button_layout:I = 0x7f040031

.field public static final event_rsvp_maybe:I = 0x7f040032

.field public static final event_rsvp_not_attending:I = 0x7f040033

.field public static final event_theme_list_fragment:I = 0x7f040034

.field public static final event_theme_list_item:I = 0x7f040035

.field public static final footer_bar:I = 0x7f040036

.field public static final get_more_suggestions:I = 0x7f040037

.field public static final hangout_abuse_dialog:I = 0x7f040038

.field public static final hangout_common_menu:I = 0x7f040039

.field public static final hangout_incoming_video_view:I = 0x7f04003a

.field public static final hangout_invitees_view:I = 0x7f04003b

.field public static final hangout_minor_dialog:I = 0x7f04003c

.field public static final hangout_onair_dialog:I = 0x7f04003d

.field public static final hangout_ringing_activity:I = 0x7f04003e

.field public static final hangout_self_video_view:I = 0x7f04003f

.field public static final hangout_tablet_tile:I = 0x7f040040

.field public static final hangout_tile:I = 0x7f040041

.field public static final hangout_tile_event_message_list_item_view:I = 0x7f040042

.field public static final hangout_tile_overlay_menu_top:I = 0x7f040043

.field public static final hangout_toasts_view:I = 0x7f040044

.field public static final hangout_video_view:I = 0x7f040045

.field public static final home_activity:I = 0x7f040046

.field public static final host_action_bar:I = 0x7f040047

.field public static final host_activity:I = 0x7f040048

.field public static final host_dialog_activity:I = 0x7f040049

.field public static final host_emotishare_chooser_activity:I = 0x7f04004a

.field public static final host_navigation_item:I = 0x7f04004b

.field public static final hosted_album_view:I = 0x7f04004c

.field public static final hosted_all_photos_tile_fragment:I = 0x7f04004d

.field public static final hosted_emotishare_chooser_view:I = 0x7f04004e

.field public static final hosted_event_fragment:I = 0x7f04004f

.field public static final hosted_events_fragment:I = 0x7f040050

.field public static final hosted_hangout_fragment:I = 0x7f040051

.field public static final hosted_people_fragment:I = 0x7f040052

.field public static final hosted_post_link_fragment:I = 0x7f040053

.field public static final hosted_square_search_fragment:I = 0x7f040054

.field public static final hosted_squares_fragment:I = 0x7f040055

.field public static final image_text_button:I = 0x7f040056

.field public static final invitation_activity:I = 0x7f040057

.field public static final invitation_fragment:I = 0x7f040058

.field public static final label_preference:I = 0x7f040059

.field public static final license_activity:I = 0x7f04005a

.field public static final list_layout:I = 0x7f04005b

.field public static final list_layout_acl:I = 0x7f04005c

.field public static final loading_message:I = 0x7f04005d

.field public static final loading_notifications:I = 0x7f04005e

.field public static final loading_tile_view:I = 0x7f04005f

.field public static final local_review_fragment:I = 0x7f040060

.field public static final location_picker_activity:I = 0x7f040061

.field public static final location_row_layout:I = 0x7f040062

.field public static final locations_map_activity:I = 0x7f040063

.field public static final message_list_fragment:I = 0x7f040064

.field public static final message_list_item_loading_older:I = 0x7f040065

.field public static final message_list_item_view:I = 0x7f040066

.field public static final message_list_item_view_image:I = 0x7f040067

.field public static final network_statistics_activity:I = 0x7f040068

.field public static final network_statistics_fragment:I = 0x7f040069

.field public static final network_transaction_row_view:I = 0x7f04006a

.field public static final network_transactions:I = 0x7f04006b

.field public static final new_conversation_activity:I = 0x7f04006c

.field public static final new_event_activity:I = 0x7f04006d

.field public static final new_hangout_activity:I = 0x7f04006e

.field public static final no_notifications:I = 0x7f04006f

.field public static final notification_row_view:I = 0x7f040070

.field public static final oob_contacts_sync_activity:I = 0x7f040071

.field public static final oob_contacts_sync_fragment:I = 0x7f040072

.field public static final oob_dialog:I = 0x7f040073

.field public static final oob_dialog_button:I = 0x7f040074

.field public static final oob_instant_upload_activity:I = 0x7f040075

.field public static final oob_instant_upload_fragment:I = 0x7f040076

.field public static final oob_profile_photo_activity:I = 0x7f040077

.field public static final oob_profile_photo_fragment:I = 0x7f040078

.field public static final oob_select_plus_page_activity:I = 0x7f040079

.field public static final oob_select_plus_page_fragment:I = 0x7f04007a

.field public static final out_of_box_activity:I = 0x7f04007b

.field public static final out_of_box_fragment:I = 0x7f04007c

.field public static final participant_list_activity:I = 0x7f04007d

.field public static final participant_list_fragment:I = 0x7f04007e

.field public static final participant_list_item_view:I = 0x7f04007f

.field public static final participant_tray_avatar_view:I = 0x7f040080

.field public static final participants_gallery_view:I = 0x7f040081

.field public static final people_audience_view_chip:I = 0x7f040082

.field public static final people_list:I = 0x7f040083

.field public static final people_list_category_as_card:I = 0x7f040084

.field public static final people_list_circle_as_card:I = 0x7f040085

.field public static final people_list_fragment:I = 0x7f040086

.field public static final people_list_row_as_card:I = 0x7f040087

.field public static final people_search_activity:I = 0x7f040088

.field public static final people_search_fragment:I = 0x7f040089

.field public static final people_search_item_public_profiles:I = 0x7f04008a

.field public static final people_suggestions:I = 0x7f04008b

.field public static final people_suggestions_category:I = 0x7f04008c

.field public static final people_suggestions_row:I = 0x7f04008d

.field public static final people_suggestions_status:I = 0x7f04008e

.field public static final people_suggestions_view_all:I = 0x7f04008f

.field public static final photo_compose_activity:I = 0x7f040090

.field public static final photo_compose_fragment:I = 0x7f040091

.field public static final photo_home_view:I = 0x7f040092

.field public static final photo_home_view_item:I = 0x7f040093

.field public static final photo_one_up_activity:I = 0x7f040094

.field public static final photo_one_up_fragment:I = 0x7f040095

.field public static final photo_one_up_info_view:I = 0x7f040096

.field public static final photo_one_up_stage_media:I = 0x7f040097

.field public static final photo_picker_activity:I = 0x7f040098

.field public static final photo_picker_fragment:I = 0x7f040099

.field public static final photo_tag_approval_view:I = 0x7f04009a

.field public static final photo_tag_view:I = 0x7f04009b

.field public static final photo_tile_one_up_activity:I = 0x7f04009c

.field public static final photo_tile_one_up_comment_view:I = 0x7f04009d

.field public static final photo_tile_one_up_fragment:I = 0x7f04009e

.field public static final photo_tile_one_up_info_view:I = 0x7f04009f

.field public static final photo_tile_one_up_view:I = 0x7f0400a0

.field public static final photo_tile_view:I = 0x7f0400a1

.field public static final photos_selection_activity:I = 0x7f0400a2

.field public static final photos_selection_fragment:I = 0x7f0400a3

.field public static final post_acl_button:I = 0x7f0400a4

.field public static final post_activity:I = 0x7f0400a5

.field public static final post_fragment:I = 0x7f0400a6

.field public static final post_link_activity:I = 0x7f0400a7

.field public static final post_search_activity:I = 0x7f0400a8

.field public static final profile_about_fragment:I = 0x7f0400a9

.field public static final profile_edit_item_education:I = 0x7f0400aa

.field public static final profile_edit_item_employment:I = 0x7f0400ab

.field public static final profile_edit_item_location:I = 0x7f0400ac

.field public static final profile_edit_items:I = 0x7f0400ad

.field public static final profile_item_details:I = 0x7f0400ae

.field public static final profile_item_device_location:I = 0x7f0400af

.field public static final profile_item_header:I = 0x7f0400b0

.field public static final profile_item_introduction:I = 0x7f0400b1

.field public static final profile_item_link:I = 0x7f0400b2

.field public static final profile_item_local_actions:I = 0x7f0400b3

.field public static final profile_item_local_details:I = 0x7f0400b4

.field public static final profile_item_local_user_review:I = 0x7f0400b5

.field public static final profile_item_local_zagat_editorial:I = 0x7f0400b6

.field public static final profile_item_location:I = 0x7f0400b7

.field public static final profile_item_multi_line:I = 0x7f0400b8

.field public static final profile_item_multi_line_with_icon:I = 0x7f0400b9

.field public static final profile_item_phone:I = 0x7f0400ba

.field public static final profile_item_two_line:I = 0x7f0400bb

.field public static final profile_item_two_line_with_icon:I = 0x7f0400bc

.field public static final profile_section_header:I = 0x7f0400bd

.field public static final ps_cache_notification:I = 0x7f0400be

.field public static final quick_actions_dialog:I = 0x7f0400bf

.field public static final quick_actions_dialog_vertical:I = 0x7f0400c0

.field public static final quick_actions_divider_horizontal:I = 0x7f0400c1

.field public static final quick_actions_divider_vertical:I = 0x7f0400c2

.field public static final quick_actions_item:I = 0x7f0400c3

.field public static final quick_actions_item_vertical:I = 0x7f0400c4

.field public static final reshare_activity:I = 0x7f0400c5

.field public static final reshare_fragment:I = 0x7f0400c6

.field public static final search_bar:I = 0x7f0400c7

.field public static final section_header:I = 0x7f0400c8

.field public static final section_header_view:I = 0x7f0400c9

.field public static final server_oob_action_bar:I = 0x7f0400ca

.field public static final share_activity:I = 0x7f0400cb

.field public static final signup_birthday_input:I = 0x7f0400cc

.field public static final signup_button_field:I = 0x7f0400cd

.field public static final signup_checkbox_input:I = 0x7f0400ce

.field public static final signup_error_field:I = 0x7f0400cf

.field public static final signup_hidden_input:I = 0x7f0400d0

.field public static final signup_image_field:I = 0x7f0400d1

.field public static final signup_spinner_input:I = 0x7f0400d2

.field public static final signup_text_field:I = 0x7f0400d3

.field public static final signup_text_input:I = 0x7f0400d4

.field public static final simple_audience_picker_dialog:I = 0x7f0400d5

.field public static final simple_list_item_2_single_choice:I = 0x7f0400d6

.field public static final simple_spinner_item:I = 0x7f0400d7

.field public static final square_cant_see_posts:I = 0x7f0400d8

.field public static final square_item_details:I = 0x7f0400d9

.field public static final square_item_header:I = 0x7f0400da

.field public static final square_landing_view:I = 0x7f0400db

.field public static final square_list_invitation_view:I = 0x7f0400dc

.field public static final square_list_item_view:I = 0x7f0400dd

.field public static final square_member_list_fragment:I = 0x7f0400de

.field public static final square_member_list_item_view:I = 0x7f0400df

.field public static final square_member_list_message_view:I = 0x7f0400e0

.field public static final square_search_status_card:I = 0x7f0400e1

.field public static final stream:I = 0x7f0400e2

.field public static final stream_one_up_activity_view:I = 0x7f0400e3

.field public static final stream_one_up_comment_count_view:I = 0x7f0400e4

.field public static final stream_one_up_comment_view:I = 0x7f0400e5

.field public static final stream_one_up_fragment:I = 0x7f0400e6

.field public static final stream_one_up_leftover_view:I = 0x7f0400e7

.field public static final stream_one_up_loading_view:I = 0x7f0400e8

.field public static final suggested_participant_view:I = 0x7f0400e9

.field public static final suggested_people_list_item_view:I = 0x7f0400ea

.field public static final system_message_list_item_view:I = 0x7f0400eb

.field public static final text_input_dialog:I = 0x7f0400ec

.field public static final timezone_spinner_dropdown_item:I = 0x7f0400ed

.field public static final title_layout:I = 0x7f0400ee

.field public static final title_layout_dark:I = 0x7f0400ef

.field public static final title_layout_light:I = 0x7f0400f0

.field public static final typeable_audience_view:I = 0x7f0400f1

.field public static final upload_statistics_fragment:I = 0x7f0400f2

.field public static final upload_statistics_media_row:I = 0x7f0400f3

.field public static final url_gateway_loader_activity:I = 0x7f0400f4

.field public static final video_view_activity:I = 0x7f0400f5

.field public static final video_view_fragment:I = 0x7f0400f6

.field public static final whats_new_dialog:I = 0x7f0400f7

.field public static final widget_configuration_activity:I = 0x7f0400f8

.field public static final widget_configuration_entry:I = 0x7f0400f9

.field public static final widget_empty_layout:I = 0x7f0400fa

.field public static final widget_froyo_layout:I = 0x7f0400fb

.field public static final widget_header_overlay:I = 0x7f0400fc

.field public static final widget_image_layout:I = 0x7f0400fd

.field public static final widget_layout:I = 0x7f0400fe

.field public static final widget_text_layout:I = 0x7f0400ff

.field public static final zagat_explanation_dialog:I = 0x7f040100


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
