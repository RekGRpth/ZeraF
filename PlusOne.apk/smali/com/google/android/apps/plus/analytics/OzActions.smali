.class public final enum Lcom/google/android/apps/plus/analytics/OzActions;
.super Ljava/lang/Enum;
.source "OzActions.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/analytics/OzActions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ADD_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum C2DM_MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CALL_TO_ACTION_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_WIFI_ONLY_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_ADD_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_ADD_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_ADD_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_REMOVE_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_REMOVE_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_REMOVE_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_CHANGE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_CHOOSE_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_CHOOSE_OWN_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_REPOSITION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_UPGRADE_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_UPGRADE_OK:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COVER_PHOTO_UPGRADE_START:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CURRENT_LOCATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DISABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DISABLE_FIND_BY_PHONE_NUMBER:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DISABLE_IMPROVE_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EMOTISHARE_INSERT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EMOTISHARE_REMOVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EMOTISHARE_SELECTED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EMOTISHARE_TEXT_MODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EMOTISHARE_TEXT_UNMODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ENABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ENABLE_FIND_BY_PHONE_NUMBER:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ENABLE_FULL_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ENABLE_IMPROVE_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ENABLE_STANDARD_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENTS_REPORT_ABUSE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENTS_VIEW_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_COMMENT_BOX_CLOSED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_COMMENT_BOX_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_DELETED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_PAGE_ADD_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_PAGE_DELETE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_PAGE_EDIT_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_PAGE_VIEWED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_WITH_DESCRIPTION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_WITH_ENDTIME_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_WITH_HANGOUT_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_WITH_IMAGE_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENT_WITH_LOCATION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GREY_SPAM_REMOVE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GREY_SPAM_RESTORE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_RENAME:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum HIDE_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum INSTANT_UPLOAD_SHARE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum INVITE_WIDGET_ADD_PEOPLE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum INVITE_WIDGET_CANCEL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum INVITE_WIDGET_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LANDING_CREATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LIST_INVITED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LIST_WIDGET_PAST_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LIST_WIDGET_UPCOMING_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MIXIN_FIND_FRIENDS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MIXIN_ITEM_READ:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MIXIN_PEOPLE_VIEW_ALL_SUGGESTIONS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MIXIN_SEE_MORE_SUL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MIXIN_SEE_MORE_YMK_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_CANCEL_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_OPEN_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum OPEN_RESHARE_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum OPEN_URL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PHOTOS_SURPRISE_CAID:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CLICKED_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_OPEN_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_OPEN_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_OPEN_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_POST_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_POST_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_UNDO_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PROFILE_EDIT_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PROFILE_EDIT_SAVE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PROFILE_EDIT_START:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum REMOVE_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum RSVP_JOIN_HANGOUT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum RSVP_MAYBE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum RSVP_NO_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum RSVP_YES_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SEARCHBOX_SELECT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_HELP:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SET_CURRENT_LOCATION_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SHOW_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_ACCEPT_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_ACCEPT_INVITATION_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_APPLY_TO_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_APPLY_TO_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_APPROVE_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_BAN_MEMBER_FROM_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_BAN_MEMBER_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_CANCEL_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_CANCEL_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_DECLINE_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_DEMOTE_MODERATOR_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_DEMOTE_OWNER_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_DEMOTE_OWNER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_IGNORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_PROMOTE_MEMBER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_PROMOTE_MODERATOR_TO_OWNER:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_REMOVE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_REMOVE_MEMBER_FROM_SQUARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_REMOVE_POST_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_SUBSCRIBE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SQUARE_UNSUBSCRIBE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_DISMISS_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_SHARE_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum UPDATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum VOLUME_CHANGED_VIA_COMMON_CONTROL:Lcom/google/android/apps/plus/analytics/OzActions;


# instance fields
.field private final mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/16 v9, 0xd

    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, 0x4

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EXIT"

    const/4 v2, 0x0

    const-string v3, "natapp"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LAUNCH"

    const/4 v2, 0x1

    const-string v3, "natapp"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SHOW_LEFT_NAV"

    const-string v2, "natapp"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SHOW_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "HIDE_LEFT_NAV"

    const/4 v2, 0x3

    const-string v3, "natapp"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->HIDE_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_HELP"

    const-string v2, "Settings"

    const/16 v3, 0x12

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_HELP:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_FEEDBACK"

    const-string v2, "Settings"

    const/16 v3, 0x13

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_TOS"

    const-string v2, "Settings"

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_SIGNOUT"

    const/4 v2, 0x7

    const-string v3, "Settings"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LOOP_CHECKIN"

    const/16 v2, 0x8

    const-string v3, "ttn"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_SELECT_ACTIVITY"

    const/16 v2, 0x9

    const-string v3, "str"

    const/16 v4, 0x16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_SELECT_AUTHOR"

    const/16 v2, 0xa

    const-string v3, "pr"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_POST"

    const/16 v2, 0xb

    const-string v3, "ttn"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_CHANGE_ACL"

    const/16 v2, 0xc

    const-string v3, "ttn"

    const/16 v4, 0x5e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_ADD_VISIBILITY_ACL"

    const-string v2, "ttn"

    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_REMOVE_VISIBILITY_ACL"

    const/16 v2, 0xe

    const-string v3, "ttn"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_ADD_CIRCLE_ACL"

    const/16 v2, 0xf

    const-string v3, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_REMOVE_CIRCLE_ACL"

    const/16 v2, 0x10

    const-string v3, "ttn"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_ADD_INDIVIDUAL_ACL"

    const/16 v2, 0x11

    const-string v3, "ttn"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_REMOVE_INDIVIDUAL_ACL"

    const/16 v2, 0x12

    const-string v3, "ttn"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_CHANGE_LOCATION"

    const/16 v2, 0x13

    const-string v3, "ttn"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_TAKE_PHOTO"

    const/16 v2, 0x14

    const-string v3, "ttn"

    const/16 v4, 0x5f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_CHOOSE_PHOTO"

    const/16 v2, 0x15

    const-string v3, "ttn"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EMOTISHARE_INSERT_CLICKED"

    const/16 v2, 0x16

    const-string v3, "ttn"

    const/16 v4, 0x6a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_INSERT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EMOTISHARE_SELECTED"

    const/16 v2, 0x17

    const-string v3, "ttn"

    const/16 v4, 0x6b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_SELECTED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EMOTISHARE_REMOVED"

    const/16 v2, 0x18

    const-string v3, "ttn"

    const/16 v4, 0x6c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_REMOVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EMOTISHARE_TEXT_MODIFIED"

    const/16 v2, 0x19

    const-string v3, "ttn"

    const/16 v4, 0x6d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_TEXT_MODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EMOTISHARE_TEXT_UNMODIFIED"

    const/16 v2, 0x1a

    const-string v3, "ttn"

    const/16 v4, 0x6e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_TEXT_UNMODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PROFILE_EDIT_START"

    const/16 v2, 0x1b

    const-string v3, "pr"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_START:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PROFILE_EDIT_SAVE"

    const/16 v2, 0x1c

    const-string v3, "pr"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_SAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PROFILE_EDIT_CANCEL"

    const/16 v2, 0x1d

    const-string v3, "pr"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_UPGRADE_START"

    const/16 v2, 0x1e

    const-string v3, "pr"

    const/16 v4, 0x27

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_UPGRADE_START:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_UPGRADE_OK"

    const/16 v2, 0x1f

    const-string v3, "pr"

    const/16 v4, 0x28

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_UPGRADE_OK:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_UPGRADE_CANCEL"

    const/16 v2, 0x20

    const-string v3, "pr"

    const/16 v4, 0x29

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_UPGRADE_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_CHANGE"

    const/16 v2, 0x21

    const-string v3, "pr"

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHANGE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_CHOOSE_OWN_PHOTO"

    const/16 v2, 0x22

    const-string v3, "pr"

    const/16 v4, 0x2a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_OWN_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_CHOOSE_GALLERY"

    const/16 v2, 0x23

    const-string v3, "pr"

    const/16 v4, 0x2b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COVER_PHOTO_REPOSITION"

    const/16 v2, 0x24

    const-string v3, "pr"

    const/16 v4, 0x24

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_REPOSITION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ENABLE_CURRENT_LOCATION"

    const/16 v2, 0x25

    const-string v3, "pr"

    const/16 v4, 0x89

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DISABLE_CURRENT_LOCATION"

    const/16 v2, 0x26

    const-string v3, "pr"

    const/16 v4, 0x8a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SET_CURRENT_LOCATION_ACL"

    const/16 v2, 0x27

    const-string v3, "pr"

    const/16 v4, 0x8b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SET_CURRENT_LOCATION_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CURRENT_LOCATION_CLICKED"

    const/16 v2, 0x28

    const-string v3, "pr"

    const/16 v4, 0x8c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CURRENT_LOCATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_SELECT_AUTHOR"

    const/16 v2, 0x29

    const-string v3, "pr"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_SELECT_PERSON"

    const/16 v2, 0x2a

    const-string v3, "pr"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_SELECT_PHOTO"

    const/16 v2, 0x2b

    const-string v3, "str"

    const/16 v4, 0x34

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_POST_COMMENT"

    const/16 v2, 0x2c

    const-string v3, "str"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "OPEN_RESHARE_SHAREBOX"

    const/16 v2, 0x2d

    const-string v3, "str"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->OPEN_RESHARE_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "RESHARE"

    const/16 v2, 0x2e

    const-string v3, "str"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_REMOVE_ACTIVITY"

    const/16 v2, 0x2f

    const-string v3, "str"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_MUTE_ACTIVITY"

    const/16 v2, 0x30

    const-string v3, "str"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_REPORT_ABUSE_ACTIVITY"

    const/16 v2, 0x31

    const-string v3, "str"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_MARK_ACTIVITY_AS_READ"

    const/16 v2, 0x32

    const-string v3, "str"

    const/16 v4, 0x39

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_MARK_ACTIVITY_AS_READ"

    const/16 v2, 0x33

    const-string v3, "str"

    const/16 v4, 0x39

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_SHARE_INSTANT_UPLOAD_PHOTOS"

    const/16 v2, 0x34

    const-string v3, "str"

    const/16 v4, 0x5e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SHARE_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_DISMISS_INSTANT_UPLOAD_PHOTOS"

    const/16 v2, 0x35

    const-string v3, "str"

    const/16 v4, 0x5f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_DISMISS_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_REPORT_ABUSE_COMMENT"

    const/16 v2, 0x36

    const-string v3, "str"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_OPEN_REPORT_ABUSE_COMMENT"

    const/16 v2, 0x37

    const-string v3, "str"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_OPEN_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_CANCEL_REPORT_ABUSE_COMMENT"

    const/16 v2, 0x38

    const-string v3, "str"

    const/16 v4, 0x1a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_CANCEL_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_DELETE_COMMENT"

    const/16 v2, 0x39

    const-string v3, "str"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_EDIT_COMMENT"

    const/16 v2, 0x3a

    const-string v3, "str"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "OPEN_URL"

    const/16 v2, 0x3b

    const-string v3, "str"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->OPEN_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GREY_SPAM_REMOVE_POST"

    const/16 v2, 0x3c

    const-string v3, "str"

    const/16 v4, 0x60

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_REMOVE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GREY_SPAM_RESTORE_POST"

    const/16 v2, 0x3d

    const-string v3, "str"

    const/16 v4, 0x61

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_RESTORE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "VOLUME_CHANGED_VIA_COMMON_CONTROL"

    const/16 v2, 0x3e

    const-string v3, "str"

    const/16 v4, 0x68

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->VOLUME_CHANGED_VIA_COMMON_CONTROL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MIXIN_ITEM_READ"

    const/16 v2, 0x3f

    const-string v3, "str"

    const/16 v4, 0x6b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_ITEM_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MIXIN_FIND_FRIENDS_CLICKED"

    const/16 v2, 0x40

    const-string v3, "str"

    const/16 v4, 0x6c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_FIND_FRIENDS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MIXIN_SEE_MORE_YMK_CLICKED"

    const/16 v2, 0x41

    const-string v3, "str"

    const/16 v4, 0x6d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_SEE_MORE_YMK_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MIXIN_SEE_MORE_SUL_CLICKED"

    const/16 v2, 0x42

    const-string v3, "str"

    const/16 v4, 0x6e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_SEE_MORE_SUL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MIXIN_PEOPLE_VIEW_ALL_SUGGESTIONS_CLICKED"

    const/16 v2, 0x43

    const-string v3, "str"

    const/16 v4, 0x6f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_PEOPLE_VIEW_ALL_SUGGESTIONS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PHOTOS_SURPRISE_CAID"

    const/16 v2, 0x44

    const-string v3, "phst"

    const/16 v4, 0x3b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PHOTOS_SURPRISE_CAID:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATIONS_START_NEW"

    const/16 v2, 0x45

    const-string v3, "messenger"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_ABORT_NEW"

    const/16 v2, 0x46

    const-string v3, "messenger"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_RETRY_SEND"

    const/16 v2, 0x47

    const-string v3, "messenger"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_POST"

    const/16 v2, 0x48

    const-string v3, "messenger"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_TAKE_PHOTO"

    const/16 v2, 0x49

    const-string v3, "messenger"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_CHOOSE_PHOTO"

    const/16 v2, 0x4a

    const-string v3, "messenger"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_MUTE"

    const/16 v2, 0x4b

    const-string v3, "messenger"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_UNMUTE"

    const/16 v2, 0x4c

    const-string v3, "messenger"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_ADD_PEOPLE"

    const/16 v2, 0x4d

    const-string v3, "messenger"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_MAKE_CIRCLE"

    const/16 v2, 0x4e

    const-string v3, "messenger"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_LEAVE"

    const/16 v2, 0x4f

    const-string v3, "messenger"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_RENAME"

    const/16 v2, 0x50

    const-string v3, "messenger"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_RENAME:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_POST"

    const/16 v2, 0x51

    const-string v3, "messenger"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_TAKE_PHOTO"

    const/16 v2, 0x52

    const-string v3, "messenger"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO"

    const/16 v2, 0x53

    const-string v3, "messenger"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_MUTE"

    const/16 v2, 0x54

    const-string v3, "messenger"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_UNMUTE"

    const/16 v2, 0x55

    const-string v3, "messenger"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_ADD_PEOPLE"

    const/16 v2, 0x56

    const-string v3, "messenger"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_OPTED_IN"

    const/16 v2, 0x57

    const-string v3, "Settings"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_OPTED_OUT"

    const/16 v2, 0x58

    const-string v3, "Settings"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_SYNC_ALL"

    const/16 v2, 0x59

    const-string v3, "Settings"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE"

    const/16 v2, 0x5a

    const-string v3, "Settings"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY"

    const/16 v2, 0x5b

    const-string v3, "Settings"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE"

    const/16 v2, 0x5c

    const-string v3, "Settings"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY"

    const/16 v2, 0x5d

    const-string v3, "Settings"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_ROAMING_ENABLED"

    const/16 v2, 0x5e

    const-string v3, "Settings"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_ROAMING_DISABLED"

    const/16 v2, 0x5f

    const-string v3, "Settings"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ENABLE_STANDARD_SIZE_PHOTO_UPLOAD"

    const/16 v2, 0x60

    const-string v3, "Settings"

    const/16 v4, 0x1a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_STANDARD_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ENABLE_FULL_SIZE_PHOTO_UPLOAD"

    const/16 v2, 0x61

    const-string v3, "Settings"

    const/16 v4, 0x1b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_FULL_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CLICKED_PLUSONE"

    const/16 v2, 0x62

    const-string v3, "plusone"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PLUSONE_CONFIRMED"

    const/16 v2, 0x63

    const-string v3, "plusone"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PLUSONE_CANCELED"

    const/16 v2, 0x64

    const-string v3, "plusone"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_UNDO_PLUSONE_CONFIRMED"

    const/16 v2, 0x65

    const-string v3, "plusone"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_UNDO_PLUSONE_CANCELED"

    const/16 v2, 0x66

    const-string v3, "plusone"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_READ_PLUSONES"

    const/16 v2, 0x67

    const-string v3, "plusone"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_READ_PLUSONES_ERROR"

    const/16 v2, 0x68

    const-string v3, "plusone"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_WRITE_PLUSONE"

    const/16 v2, 0x69

    const-string v3, "plusone"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_WRITE_PLUSONE_ERROR"

    const/16 v2, 0x6a

    const-string v3, "plusone"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_ERROR_PLUSONE"

    const/16 v2, 0x6b

    const-string v3, "plusone"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PLUSONE_PREVIEW_SHOWN"

    const/16 v2, 0x6c

    const-string v3, "plusone"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_OPEN_SHAREBOX"

    const/16 v2, 0x6d

    const-string v3, "ttn"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONFIRM_SHARE"

    const/16 v2, 0x6e

    const-string v3, "ttn"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CANCEL_SHARE"

    const/16 v2, 0x6f

    const-string v3, "ttn"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_OPEN_SHAREBOX_CALL_TO_ACTION"

    const/16 v2, 0x70

    const-string v3, "ttn"

    const/16 v4, 0x70

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_OPEN_SHAREBOX_DEEP_LINK"

    const/16 v2, 0x71

    const-string v3, "ttn"

    const/16 v4, 0x71

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_POST_SHAREBOX_CALL_TO_ACTION"

    const/16 v2, 0x72

    const-string v3, "ttn"

    const/16 v4, 0x72

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_POST_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_POST_SHAREBOX_DEEP_LINK"

    const/16 v2, 0x73

    const-string v3, "ttn"

    const/16 v4, 0x73

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_POST_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONFIRM_SHARE_FROM_PLUSONE"

    const/16 v2, 0x74

    const-string v3, "ttn"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CANCEL_SHARE_FROM_PLUSONE"

    const/16 v2, 0x75

    const-string v3, "ttn"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CLICKED_SHARE_FROM_PLUSONE"

    const/16 v2, 0x76

    const-string v3, "plusone"

    const/16 v4, 0x16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_ERROR_SHARE"

    const/16 v2, 0x77

    const-string v3, "ttn"

    const/16 v4, 0x2f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_PREVIEW_SHOWN"

    const/16 v2, 0x78

    const-string v3, "ttn"

    const/16 v4, 0x59

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_PREVIEW_ERROR"

    const/16 v2, 0x79

    const-string v3, "ttn"

    const/16 v4, 0x5a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_COMMENT_ADDED"

    const/16 v2, 0x7a

    const-string v3, "ttn"

    const/16 v4, 0x5b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_ERROR"

    const/16 v2, 0x7b

    const-string v3, "ttn"

    const/16 v4, 0x2f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_ATTACHMENT"

    const/16 v2, 0x7c

    const-string v3, "ttn"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_COMMENT"

    const/16 v2, 0x7d

    const-string v3, "ttn"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_LOCATION"

    const/16 v2, 0x7e

    const-string v3, "ttn"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_URL"

    const/16 v2, 0x7f

    const-string v3, "ttn"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CIRCLES_SHARE_ACL_ADDED"

    const/16 v2, 0x80

    const-string v3, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PEOPLE_SHARE_ACL_ADDED"

    const/16 v2, 0x81

    const-string v3, "ttn"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_GET_ACCOUNT"

    const/16 v2, 0x82

    const-string v3, "signin"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_LOCATION"

    const/16 v2, 0x83

    const-string v3, "ttn"

    const/16 v4, 0x5d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_ACL"

    const/16 v2, 0x84

    const-string v3, "ttn"

    const/16 v4, 0x5e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_CAMERA"

    const/16 v2, 0x85

    const-string v3, "ttn"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_GALLERY"

    const/16 v2, 0x86

    const-string v3, "ttn"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONNECT_SHOW_OOB"

    const/16 v2, 0x87

    const-string v3, "oob"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONNECT_SELECT_ACCOUNT"

    const/16 v2, 0x88

    const-string v3, "oob"

    const/16 v4, 0x29

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_AUDIENCE_VIEW_CLICKED"

    const/16 v2, 0x89

    const-string v3, "ttn"

    const/16 v4, 0x3f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED"

    const/16 v2, 0x8a

    const-string v3, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_AUDIENCE_VIEW_PERSON_ADDED"

    const/16 v2, 0x8b

    const-string v3, "pr"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_ENABLED"

    const/16 v2, 0x8c

    const-string v3, "Settings"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_DISABLED"

    const/16 v2, 0x8d

    const-string v3, "Settings"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_OPTED_IN"

    const/16 v2, 0x8e

    const-string v3, "oob"

    const/16 v4, 0x22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_WIFI_ONLY_OPTED_IN"

    const/16 v2, 0x8f

    const-string v3, "oob"

    const/16 v4, 0x23

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_WIFI_ONLY_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_OPTED_OUT"

    const/16 v2, 0x90

    const-string v3, "oob"

    const/16 v4, 0x24

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATIONS_EMPTY"

    const/16 v2, 0x91

    const-string v3, "messenger"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_AUTO_RETRY_SUCCESS"

    const/16 v2, 0x92

    const-string v3, "messenger"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_AUTO_RETRY_FAIL"

    const/16 v2, 0x93

    const-string v3, "messenger"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_MANUAL_RETRY_FAIL"

    const/16 v2, 0x94

    const-string v3, "messenger"

    const/16 v4, 0x16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_MANUAL_RETRY_SUCCESS"

    const/16 v2, 0x95

    const-string v3, "messenger"

    const/16 v4, 0x17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "C2DM_MESSAGE_RECEIVED"

    const/16 v2, 0x96

    const-string v3, "messenger"

    const/16 v4, 0x18

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->C2DM_MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MESSAGE_RECEIVED"

    const/16 v2, 0x97

    const-string v3, "messenger"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LANDING_CREATE_EVENT_CLICKED"

    const/16 v2, 0x98

    const-string v3, "oevt"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LANDING_CREATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "RSVP_YES_CLICKED"

    const/16 v2, 0x99

    const-string v3, "oevt"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_YES_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "RSVP_NO_CLICKED"

    const/16 v2, 0x9a

    const-string v3, "oevt"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_NO_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "RSVP_MAYBE_CLICKED"

    const/16 v2, 0x9b

    const-string v3, "oevt"

    const/16 v4, 0x37

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_MAYBE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "RSVP_JOIN_HANGOUT_CLICKED"

    const/16 v2, 0x9c

    const-string v3, "oevt"

    const/16 v4, 0x34

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_JOIN_HANGOUT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "INVITE_WIDGET_OPENED"

    const/16 v2, 0x9d

    const-string v3, "oevt"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LIST_INVITED"

    const/16 v2, 0x9e

    const-string v3, "oevt"

    const/16 v4, 0x39

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_INVITED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "INVITE_WIDGET_ADD_PEOPLE_CLICKED"

    const/16 v2, 0x9f

    const-string v3, "oevt"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_ADD_PEOPLE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "INVITE_WIDGET_CANCEL_CLICKED"

    const/16 v2, 0xa0

    const-string v3, "oevt"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_CANCEL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LIST_WIDGET_UPCOMING_CLICKED"

    const/16 v2, 0xa1

    const-string v3, "oevt"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_WIDGET_UPCOMING_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LIST_WIDGET_PAST_CLICKED"

    const/16 v2, 0xa2

    const-string v3, "oevt"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_WIDGET_PAST_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_PAGE_ADD_PHOTOS_CLICKED"

    const/16 v2, 0xa3

    const-string v3, "oevt"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_ADD_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_PAGE_VIEWED"

    const/16 v2, 0xa4

    const-string v3, "oevt"

    const/16 v4, 0x3f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_VIEWED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_PAGE_DELETE_EVENT_CLICKED"

    const/16 v2, 0xa5

    const-string v3, "oevt"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_DELETE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_PAGE_EDIT_EVENT_CLICKED"

    const/16 v2, 0xa6

    const-string v3, "oevt"

    const/16 v4, 0x30

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_EDIT_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENTS_REPORT_ABUSE_CLICKED"

    const/16 v2, 0xa7

    const-string v3, "oevt"

    const/16 v4, 0x31

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_REPORT_ABUSE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENTS_VIEW_PHOTOS_CLICKED"

    const/16 v2, 0xa8

    const-string v3, "oevt"

    const/16 v4, 0x36

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_VIEW_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_COMMENT_BOX_OPENED"

    const/16 v2, 0xa9

    const-string v3, "oevt"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_COMMENT_BOX_CLOSED"

    const/16 v2, 0xaa

    const-string v3, "oevt"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_CLOSED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_COMMENT_ADDED"

    const/16 v2, 0xab

    const-string v3, "oevt"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_DELETED"

    const/16 v2, 0xac

    const-string v3, "oevt"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_DELETED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "UPDATE_EVENT_CLICKED"

    const/16 v2, 0xad

    const-string v3, "oevt"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->UPDATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_WITH_ENDTIME_CREATED"

    const/16 v2, 0xae

    const-string v3, "oevt"

    const/16 v4, 0x21

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_ENDTIME_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_WITH_LOCATION_CREATED"

    const/16 v2, 0xaf

    const-string v3, "oevt"

    const/16 v4, 0x22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_LOCATION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_WITH_IMAGE_CREATED"

    const/16 v2, 0xb0

    const-string v3, "oevt"

    const/16 v4, 0x23

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_IMAGE_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_WITH_DESCRIPTION_CREATED"

    const/16 v2, 0xb1

    const-string v3, "oevt"

    const/16 v4, 0x24

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_DESCRIPTION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENT_WITH_HANGOUT_CREATED"

    const/16 v2, 0xb2

    const-string v3, "oevt"

    const/16 v4, 0x2c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_HANGOUT_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENTS_PARTY_MODE_ON"

    const/16 v2, 0xb3

    const-string v3, "oevt"

    const/16 v4, 0x3a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENTS_PARTY_MODE_OFF"

    const/16 v2, 0xb4

    const-string v3, "oevt"

    const/16 v4, 0x3b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_OFFERED"

    const/16 v2, 0xb5

    const-string v3, "dl"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_DECLINED"

    const/16 v2, 0xb6

    const-string v3, "dl"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_ACCEPTED"

    const/16 v2, 0xb7

    const-string v3, "dl"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED"

    const/16 v2, 0xb8

    const-string v3, "dl"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_CONSUMED"

    const/16 v2, 0xb9

    const-string v3, "dl"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_UNRESOLVED"

    const/16 v2, 0xba

    const-string v3, "dl"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE"

    const/16 v2, 0xbb

    const-string v3, "dl"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE"

    const/16 v2, 0xbc

    const-string v3, "dl"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION"

    const/16 v2, 0xbd

    const-string v3, "dl"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_INSTALL_ACCEPTED"

    const/16 v2, 0xbe

    const-string v3, "dl"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED"

    const/16 v2, 0xbf

    const-string v3, "dl"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_CONSUMED"

    const/16 v2, 0xc0

    const-string v3, "dl"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_UNRESOLVED"

    const/16 v2, 0xc1

    const-string v3, "dl"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_INSTALL_STARTED_ON_PLAY_STORE"

    const/16 v2, 0xc2

    const-string v3, "dl"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_INSTALL_NOT_STARTED_ON_PLAY_STORE"

    const/16 v2, 0xc3

    const-string v3, "dl"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CALL_TO_ACTION_INSTALL_COMPLETED_NOTIFICATION"

    const/16 v2, 0xc4

    const-string v3, "dl"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "TICKLE_NOTIFICATION_RECEIVED"

    const/16 v2, 0xc5

    const-string v3, "nots"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "TICKLE_EVENT_RECEIVED"

    const/16 v2, 0xc6

    const-string v3, "nots"

    const/16 v4, 0x1a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "NOTIFICATION_FETCHED_FROM_TICKLE"

    const/16 v2, 0xc7

    const-string v3, "nots"

    const/16 v4, 0x17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "NOTIFICATION_FETCHED_FROM_USER_REFRESH"

    const/16 v2, 0xc8

    const-string v3, "nots"

    const/16 v4, 0x18

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "AGGREGATED_NOTIFICATION_CLICKED"

    const/16 v2, 0xc9

    const-string v3, "nots"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SINGLE_NOTIFICATION_CLICKED"

    const/16 v2, 0xca

    const-string v3, "nots"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "INSTANT_UPLOAD_SHARE_CLICKED"

    const/16 v2, 0xcb

    const-string v3, "phst"

    const/16 v4, 0x3f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->INSTANT_UPLOAD_SHARE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ADD_CIRCLE_MEMBERS"

    const/16 v2, 0xcc

    const-string v3, "sg"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ADD_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "REMOVE_CIRCLE_MEMBERS"

    const/16 v2, 0xcd

    const-string v3, "sg"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->REMOVE_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_APPLY_TO_JOIN"

    const/16 v2, 0xce

    const-string v3, "sq"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPLY_TO_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_APPROVE_JOIN_REQUEST"

    const/16 v2, 0xcf

    const-string v3, "sq"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPROVE_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_ACCEPT_INVITATION"

    const/16 v2, 0xd0

    const-string v3, "sq"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_ACCEPT_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_JOIN"

    const/16 v2, 0xd1

    const-string v3, "sq"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_IGNORE"

    const/16 v2, 0xd2

    const-string v3, "sq"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_IGNORE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_LEAVE"

    const/16 v2, 0xd3

    const-string v3, "sq"

    const/16 v4, 0x16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_REMOVE_MEMBER_FROM_SQUARE"

    const/16 v2, 0xd4

    const-string v3, "sq"

    const/16 v4, 0x17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_MEMBER_FROM_SQUARE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_BAN"

    const/16 v2, 0xd5

    const-string v3, "sq"

    const/16 v4, 0x18

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_REMOVE_BAN"

    const/16 v2, 0xd6

    const-string v3, "sq"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_PROMOTE_MEMBER_TO_MODERATOR"

    const/16 v2, 0xd7

    const-string v3, "sq"

    const/16 v4, 0x1a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_PROMOTE_MEMBER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_PROMOTE_MODERATOR_TO_OWNER"

    const/16 v2, 0xd8

    const-string v3, "sq"

    const/16 v4, 0x1b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_PROMOTE_MODERATOR_TO_OWNER:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_DEMOTE_OWNER_TO_MODERATOR"

    const/16 v2, 0xd9

    const-string v3, "sq"

    const/16 v4, 0x1d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_OWNER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_DEMOTE_MODERATOR_TO_MEMBER"

    const/16 v2, 0xda

    const-string v3, "sq"

    const/16 v4, 0x1e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_MODERATOR_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_DEMOTE_OWNER_TO_MEMBER"

    const/16 v2, 0xdb

    const-string v3, "sq"

    const/16 v4, 0x1f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_OWNER_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_SUBSCRIBE"

    const/16 v2, 0xdc

    const-string v3, "sq"

    const/16 v4, 0x20

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_SUBSCRIBE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_UNSUBSCRIBE"

    const/16 v2, 0xdd

    const-string v3, "sq"

    const/16 v4, 0x21

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_UNSUBSCRIBE:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_APPLY_TO_JOIN_WITH_SUBSCRIPTION"

    const/16 v2, 0xde

    const-string v3, "sq"

    const/16 v4, 0x22

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPLY_TO_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_ACCEPT_INVITATION_WITH_SUBSCRIPTION"

    const/16 v2, 0xdf

    const-string v3, "sq"

    const/16 v4, 0x23

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_ACCEPT_INVITATION_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_JOIN_WITH_SUBSCRIPTION"

    const/16 v2, 0xe0

    const-string v3, "sq"

    const/16 v4, 0x24

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_CANCEL_JOIN_REQUEST"

    const/16 v2, 0xe1

    const-string v3, "sq"

    const/16 v4, 0x2c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_CANCEL_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_DECLINE_INVITATION"

    const/16 v2, 0xe2

    const-string v3, "sq"

    const/16 v4, 0x2d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DECLINE_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_CANCEL_INVITATION"

    const/16 v2, 0xe3

    const-string v3, "sq"

    const/16 v4, 0x2e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_CANCEL_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_BAN_MEMBER_VIA_POST_OPTIONS_MENU"

    const/16 v2, 0xe4

    const-string v3, "sq"

    const/16 v4, 0x34

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN_MEMBER_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_REMOVE_POST_VIA_POST_OPTIONS_MENU"

    const/16 v2, 0xe5

    const-string v3, "sq"

    const/16 v4, 0x35

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_POST_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SQUARE_BAN_MEMBER_FROM_COMMENT"

    const/16 v2, 0xe6

    const-string v3, "sq"

    const/16 v4, 0x49

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN_MEMBER_FROM_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ENABLE_IMPROVE_SUGGESTIONS"

    const/16 v2, 0xe7

    const-string v3, "Settings"

    const/16 v4, 0x16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_IMPROVE_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DISABLE_IMPROVE_SUGGESTIONS"

    const/16 v2, 0xe8

    const-string v3, "Settings"

    const/16 v4, 0x17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_IMPROVE_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ENABLE_FIND_BY_PHONE_NUMBER"

    const/16 v2, 0xe9

    const-string v3, "Settings"

    const/16 v4, 0x18

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_FIND_BY_PHONE_NUMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DISABLE_FIND_BY_PHONE_NUMBER"

    const/16 v2, 0xea

    const-string v3, "Settings"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_FIND_BY_PHONE_NUMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SEARCHBOX_SELECT"

    const/16 v2, 0xeb

    const-string v3, "se"

    const/16 v4, 0x20

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SEARCHBOX_SELECT:Lcom/google/android/apps/plus/analytics/OzActions;

    const/16 v0, 0xec

    new-array v0, v0, [Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SHOW_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->HIDE_LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_HELP:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v9

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_INSERT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_SELECTED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_REMOVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_TEXT_MODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_TEXT_UNMODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_START:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_SAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PROFILE_EDIT_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_UPGRADE_START:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_UPGRADE_OK:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_UPGRADE_CANCEL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHANGE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_OWN_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_CHOOSE_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COVER_PHOTO_REPOSITION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_CURRENT_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SET_CURRENT_LOCATION_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CURRENT_LOCATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->OPEN_RESHARE_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SHARE_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_DISMISS_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_OPEN_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_CANCEL_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->OPEN_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_REMOVE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_RESTORE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->VOLUME_CHANGED_VIA_COMMON_CONTROL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_ITEM_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_FIND_FRIENDS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_SEE_MORE_YMK_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_SEE_MORE_SUL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_PEOPLE_VIEW_ALL_SUGGESTIONS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PHOTOS_SURPRISE_CAID:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_RENAME:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_STANDARD_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_FULL_SIZE_PHOTO_UPLOAD:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_POST_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_POST_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_WIFI_ONLY_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->C2DM_MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LANDING_CREATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_YES_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_NO_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_MAYBE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->RSVP_JOIN_HANGOUT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_INVITED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_ADD_PEOPLE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_CANCEL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_WIDGET_UPCOMING_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LIST_WIDGET_PAST_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_ADD_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_VIEWED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_DELETE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_PAGE_EDIT_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_REPORT_ABUSE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_VIEW_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_BOX_CLOSED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_DELETED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->UPDATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_ENDTIME_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_LOCATION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_IMAGE_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_DESCRIPTION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_HANGOUT_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xca

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->INSTANT_UPLOAD_SHARE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ADD_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->REMOVE_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xce

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPLY_TO_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPROVE_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_ACCEPT_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_JOIN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_IGNORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_MEMBER_FROM_SQUARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_PROMOTE_MEMBER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_PROMOTE_MODERATOR_TO_OWNER:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_OWNER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xda

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_MODERATOR_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_OWNER_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_SUBSCRIBE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_UNSUBSCRIBE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xde

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPLY_TO_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_ACCEPT_INVITATION_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_JOIN_WITH_SUBSCRIPTION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_CANCEL_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DECLINE_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_CANCEL_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN_MEMBER_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_POST_VIA_POST_OPTIONS_MENU:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN_MEMBER_FROM_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_IMPROVE_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_IMPROVE_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ENABLE_FIND_BY_PHONE_NUMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xea

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DISABLE_FIND_BY_PHONE_NUMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SEARCHBOX_SELECT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzActions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V
    .locals 6
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 2
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p4, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeStr:Ljava/lang/String;

    return-void
.end method

.method public static getName(Lcom/google/android/apps/plus/analytics/OzActions;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/analytics/OzActions;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/OzActions;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzActions;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/OzActions;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/analytics/OzActions;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/analytics/OzActions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/analytics/OzActions;

    return-object v0
.end method


# virtual methods
.method public final getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    return-object v0
.end method
