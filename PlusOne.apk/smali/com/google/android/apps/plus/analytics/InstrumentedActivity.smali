.class public abstract Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "InstrumentedActivity.java"


# static fields
.field private static sPlayServicesKnownAvailable:Z


# instance fields
.field private mExited:Z

.field private mRecorded:Z

.field private mStartingActivity:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private static getStartTime(Landroid/content/Intent;)J
    .locals 3
    .param p0    # Landroid/content/Intent;

    const-string v0, "com.google.plus.analytics.intent.extra.START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 3
    .param p0    # Landroid/content/Intent;

    const-string v1, "com.google.plus.analytics.intent.extra.START_VIEW"

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->valueOf(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    return-object v1
.end method

.method private instrument(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 7
    .param p1    # Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    if-eqz v3, :cond_3

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v4, "com.google.plus.analytics.intent.extra.START_VIEW"

    invoke-virtual {v3}, Lcom/google/android/apps/plus/analytics/OzViews;->ordinal()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "com.google.plus.analytics.intent.extra.START_TIME"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.plus.analytics.intent.extra.FROM_THIRD_PARTY_APP"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const-string v4, "com.google.plus.analytics.intent.extra.FROM_THIRD_PARTY_APP"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/os/BadParcelableException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_2
    move-object p1, v2

    :cond_3
    move-object v2, p1

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    :goto_1
    const-string v4, "InstrumentedActivity"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "InstrumentedActivity"

    const-string v5, "Unable to instrument Intent"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    move-object v2, p1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object p1, v2

    goto :goto_1
.end method

.method public static isFromThirdPartyApp(Landroid/content/Intent;)Z
    .locals 2
    .param p0    # Landroid/content/Intent;

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string v1, "com.google.plus.analytics.intent.extra.FROM_THIRD_PARTY_APP"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    return v0
.end method

.method private recordNotificationActionEvent(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v8, 0x0

    if-nez p1, :cond_2

    move v7, v8

    :goto_0
    if-eqz v7, :cond_1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v7, "notif_id"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v7, "coalescing_id"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v7, "extra_notification_id"

    invoke-virtual {v3, v7, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "extra_notification_read"

    const-string v9, "com.google.plus.analytics.intent.extra.NOTIFICATION_READ"

    invoke-virtual {p1, v9, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    invoke-virtual {v3, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_1
    const-string v7, "notif_types"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "notif_types"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getIntegerArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    :goto_2
    const-string v7, "coalescing_codes"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    const-string v7, "coalescing_codes"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    :goto_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v7, v8, :cond_0

    const-string v7, "extra_notification_types"

    invoke-virtual {v3, v7, v5}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v7, "extra_coalescing_codes"

    invoke-virtual {v3, v7, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    const-string v7, "com.google.plus.analytics.intent.extra.START_VIEW"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v8

    invoke-static {v7, v8, v0, v6, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    sget-object v7, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    if-ne v0, v7, :cond_1

    const/16 v7, 0x12

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/analytics/OzActions;->INSTANT_UPLOAD_SHARE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v7, v8, v9, v6, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_1
    return-void

    :cond_2
    const-string v7, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    goto/16 :goto_0

    :cond_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_1

    :cond_5
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_2

    :cond_6
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_3

    :cond_7
    sget-object v6, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_4
.end method

.method public static recordReverseViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V
    .locals 10
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p3    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    if-eqz v3, :cond_0

    move-object v2, p2

    move-object v6, p3

    const-string v0, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    if-eqz v9, :cond_1

    const-string v0, "com.google.plus.analytics.intent.extra.FROM_THIRD_PARTY_APP"

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    const-string v0, "extra_platform_event"

    const/4 v1, 0x1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private recordViewNavigation()V
    .locals 3

    iget-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    goto :goto_0
.end method

.method public static recordViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;)V
    .locals 9
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    const-string v1, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartTime(Landroid/content/Intent;)J

    move-result-wide v3

    if-eqz v0, :cond_0

    const-string v1, "com.google.plus.analytics.intent.extra.FROM_THIRD_PARTY_APP"

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    const-string v0, "extra_platform_event"

    const/4 v1, 0x1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v7, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void

    :cond_0
    move-object v8, v5

    goto :goto_0
.end method


# virtual methods
.method protected createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public finish()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_0
    :goto_0
    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {p0, v1, v0, v2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordReverseViewNavigation(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected abstract getAccount()Lcom/google/android/apps/plus/content/EsAccount;
.end method

.method public final getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/apps/plus/analytics/AnalyticsInfo;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartTime(Landroid/content/Intent;)J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;J)V

    return-object v0
.end method

.method protected getDefaultFragmentContainerViewId()I
    .locals 1

    const v0, 0x1020002

    return v0
.end method

.method protected getExtrasForLogging()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
.end method

.method public final isGooglePlayServicesAvailable()Z
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected needsAsyncData()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/16 v0, 0x5dbd

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isGooglePlayServicesAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lvedroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onAsyncData()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v2, "analytics:recorded"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    const-string v2, "analytics:exited"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordNotificationActionEvent(Landroid/content/Intent;)V

    sget-boolean v2, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->sPlayServicesKnownAvailable:Z

    if-nez v2, :cond_0

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_3

    sput-boolean v3, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->sPlayServicesKnownAvailable:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->createDefaultFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->replaceFragment(Lvedroid/support/v4/app/Fragment;)V

    :cond_2
    iput-boolean v3, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    goto :goto_0

    :cond_3
    if-nez p1, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isUserRecoverableError(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->getGooglePlayServicesUpgradeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    const/16 v3, 0x5dbd

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordNotificationActionEvent(Landroid/content/Intent;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getStartView(Landroid/content/Intent;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    const-string v0, "com.google.plus.analytics.intent.extra.EXTRA_START_VIEW_EXTRAS"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v7

    if-eqz p1, :cond_1

    const-string v0, "com.google.plus.analytics.intent.extra.FROM_THIRD_PARTY_APP"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    const-string v0, "extra_platform_event"

    const/4 v5, 0x1

    invoke-virtual {v8, v0, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    if-eq v2, v3, :cond_0

    move-object v0, p0

    move-object v5, v4

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    const-string v1, "power"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v3

    :goto_0
    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->needsAsyncData()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordViewNavigation()V

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "analytics:recorded"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mRecorded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "analytics:exited"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onUserLeaveHint()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mExited:Z

    :cond_0
    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzActions;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, v0, p1, p2, p3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)J

    :cond_0
    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-static {p0, v0, p1, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method protected replaceFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 4
    .param p1    # Lvedroid/support/v4/app/Fragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getDefaultFragmentContainerViewId()I

    move-result v0

    const-string v1, "default"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p1, v2}, Lvedroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3, v0, p1, v1}, Lvedroid/support/v4/app/FragmentTransaction;->replace(ILvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lvedroid/support/v4/app/FragmentTransaction;->setTransition(I)Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->instrument(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->instrument(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, v0, p2}, Lvedroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    return-void
.end method

.method public final startActivityFromFragment(Lvedroid/support/v4/app/Fragment;Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/Fragment;
    .param p2    # Landroid/content/Intent;
    .param p3    # I

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->instrument(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Lvedroid/support/v4/app/FragmentActivity;->startActivityFromFragment(Lvedroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->mStartingActivity:Z

    return-void
.end method

.method public final startExternalActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/high16 v0, 0x80000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
