.class public final Lcom/google/android/apps/plus/service/AndroidNotification;
.super Ljava/lang/Object;
.source "AndroidNotification.java"


# static fields
.field private static final COMMENT_NOTIFICATION_TYPES:[I

.field private static final NOTIFICATION_IDS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidNotification;->NOTIFICATION_IDS:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidNotification;->COMMENT_NOTIFICATION_TYPES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
    .end array-data

    :array_1
    .array-data 4
        0x2
        0x3
        0xe
        0x19
        0x1a
    .end array-data
.end method

.method private static buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":notifications:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I

    const-class v3, Lcom/google/android/apps/plus/service/AndroidNotification;

    monitor-enter v3

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "notification"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-virtual {v0, v1, p2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-void

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized cancelAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-class v7, Lcom/google/android/apps/plus/service/AndroidNotification;

    monitor-enter v7

    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "notification"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidNotification;->NOTIFICATION_IDS:[I

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, v0, v1

    invoke-virtual {v4, v5, v3}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v7

    return-void

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6
.end method

.method public static cancelFirstTimeFullSizeNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method public static cancelQuotaNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method private static countActorsForComment(Lvedroid/support/v4/util/SparseArrayCompat;)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/util/SparseArrayCompat",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)I"
        }
    .end annotation

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidNotification;->COMMENT_NOTIFICATION_TYPES:[I

    array-length v6, v3

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_1

    aget v7, v3, v5

    invoke-virtual {p0, v7}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v8

    return v8
.end method

.method private static createAudienceData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;",
            ">;)",
            "Lcom/google/android/apps/plus/content/AudienceData;"
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-static {p0, p1, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v4

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    if-nez v4, :cond_1

    :goto_0
    return-object v5

    :cond_1
    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v5, v4}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_0

    :cond_2
    if-eqz v4, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;

    new-instance v6, Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8, v5}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v5, v3, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method

.method private static createAudienceDataForYourCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x5

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_0
.end method

.method private static createDigestNotification$78923c81(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;
    .locals 21
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/Cursor;

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasOnlyHangoutNotifications(Landroid/database/Cursor;)Z

    move-result v19

    if-eqz v19, :cond_0

    const/16 v19, 0x0

    :goto_0
    return-object v19

    :cond_0
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v19

    if-nez v19, :cond_1

    const/16 v19, 0x0

    goto :goto_0

    :cond_1
    const-wide v17, 0x7fffffffffffffffL

    const/4 v10, 0x0

    new-instance v8, Landroid/app/Notification$InboxStyle;

    invoke-direct {v8}, Landroid/app/Notification$InboxStyle;-><init>()V

    const/4 v6, 0x0

    :cond_2
    const/16 v19, 0x3

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v19, 0x8

    move/from16 v0, v19

    if-eq v4, v0, :cond_4

    const/16 v19, 0x4

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    if-nez v6, :cond_3

    move-object v6, v12

    :cond_3
    const/16 v19, 0x5

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const-wide/16 v19, 0x3e8

    div-long v19, v14, v19

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v17

    add-int/lit8 v10, v10, 0x1

    :cond_4
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v19

    if-nez v19, :cond_2

    move/from16 v16, v10

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    sget v19, Lcom/google/android/apps/plus/R$plurals;->notifications_ticker_text:I

    move/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v13

    move-object v7, v13

    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/phone/Intents;->getNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v9

    const/high16 v19, 0x14000000

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v19, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v3, Landroid/app/Notification$Builder;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    move-wide/from16 v0, v19

    long-to-int v0, v0

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-static {v0, v1, v9, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v8, v7}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    invoke-virtual {v3, v13}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v19

    sget v20, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    invoke-virtual/range {v19 .. v20}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v19

    sget v20, Lcom/google/android/apps/plus/R$drawable;->stat_notify_multiple_gplus:I

    move/from16 v0, v20

    invoke-static {v11, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v19

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Landroid/app/PendingIntent;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_5

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    :cond_5
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_6

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    :goto_1
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v19

    goto/16 :goto_0

    :cond_6
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    goto :goto_1
.end method

.method private static createMediaRefList(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 10
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v7, :cond_0

    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    if-eqz v1, :cond_0

    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v1, :cond_1

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_1
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v1, "AndroidNotification"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot convert "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v7, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " into Long."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->isPanorama:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v1, v7, Lcom/google/api/services/plusi/model/DataPhoto;->isPanorama:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_1

    :cond_2
    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_3
    return-object v9
.end method

.method private static createNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/Notification;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x0

    const/4 v4, 0x1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsNotificationData;->getNotificationsToDisplay(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_a

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    if-ne v2, v4, :cond_5

    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidNotification;->isRunningJellybeanOrLater()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->createRichNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    const/4 v3, 0x3

    :try_start_1
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v3, 0x8

    if-ne v4, v3, :cond_2

    :goto_2
    goto :goto_1

    :cond_2
    const/16 v3, 0x10

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    :goto_3
    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1, v3, v5, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    const v3, 0xffff

    if-eq v4, v3, :cond_3

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    :cond_3
    :goto_4
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v4, v6

    const/high16 v6, 0x8000000

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v6, v7}, Lvedroid/support/v4/app/TaskStackBuilder;->getPendingIntent(IILandroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, p0, v5, v4, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/4 v3, 0x1

    invoke-static {p0, p1, v3}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v3

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v3

    :sswitch_0
    :try_start_2
    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_stat_circle:I

    goto :goto_3

    :sswitch_1
    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_stat_instant_upload:I

    goto :goto_3

    :cond_4
    iget v3, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/app/Notification;->defaults:I

    goto :goto_4

    :cond_5
    if-le v2, v4, :cond_9

    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidNotification;->isRunningJellybeanOrLater()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->createDigestNotification$78923c81(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;

    move-result-object v1

    goto :goto_1

    :cond_6
    invoke-static {v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasOnlyHangoutNotifications(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_7

    :goto_5
    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_ticker_text:I

    invoke-virtual {v3, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    new-instance v1, Landroid/app/Notification;

    sget v7, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    invoke-direct {v1, v7, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    sget v5, Lcom/google/android/apps/plus/R$plurals;->notifications_content_text:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x14000000

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v6, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v6

    iput-object v6, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x0

    invoke-static {p0, v6, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v1, p0, v4, v3, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/4 v3, 0x1

    invoke-static {p0, p1, v3}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Landroid/app/PendingIntent;

    move-result-object v3

    iput-object v3, v1, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    goto :goto_5

    :cond_8
    iget v6, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v1, Landroid/app/Notification;->defaults:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_6

    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_a
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
        0x27 -> :sswitch_0
    .end sparse-switch
.end method

.method private static createRichNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/app/Notification;
    .locals 98
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/Cursor;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v77

    const/4 v4, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/16 v4, 0x10

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v61

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    const/4 v4, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v92

    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_ticker_text:I

    const/4 v5, 0x1

    move-object/from16 v0, v77

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v86

    const/4 v4, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v9, 0x3e8

    div-long v94, v4, v9

    const/16 v4, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v33, v86

    move-object/from16 v32, v92

    move-object/from16 v24, v92

    const/16 v74, 0x0

    const/16 v76, 0x0

    const/16 v75, 0x0

    const/16 v78, 0x0

    const/16 v80, 0x0

    const/16 v79, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    sget v81, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    const/16 v4, 0x16

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v82

    const/4 v4, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/google/android/apps/plus/content/DbDataAction;->deserializeDataActionList([B)Ljava/util/List;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbDataAction;->getDataActorList(Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidNotification;->getActorAvatars$d8296a3(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/EsMediaCache;->obtainAvatarForMultipleUsers(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v51

    const/16 v4, 0x12

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static/range {v40 .. v40}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v40

    :cond_0
    new-instance v20, Lvedroid/support/v4/util/SparseArrayCompat;

    invoke-direct/range {v20 .. v20}, Lvedroid/support/v4/util/SparseArrayCompat;-><init>()V

    if-eqz v16, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataAction;

    if-eqz v4, :cond_1

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_2
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataItem;

    iget-object v5, v4, Lcom/google/api/services/plusi/model/DataItem;->notificationType:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsNotificationData;->getNotificationType(Ljava/lang/String;)I

    move-result v11

    iget-object v5, v4, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v5, :cond_2

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-nez v5, :cond_3

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v11, v5}, Lvedroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :cond_3
    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    const/16 v59, 0x0

    const/4 v4, 0x1

    if-ne v8, v4, :cond_6

    if-eqz v18, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_e

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    move-object/from16 v24, v33

    move-object/from16 v23, v32

    :cond_5
    :goto_1
    sget v78, Lcom/google/android/apps/plus/R$drawable;->stat_notify_comment:I

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_action_comment:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v80

    const/4 v9, 0x1

    const/4 v10, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v10}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZZ)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_13

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    if-eqz v5, :cond_13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    long-to-int v5, v9

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v79

    :cond_6
    :goto_2
    const/16 v4, 0xb

    if-ne v8, v4, :cond_7

    sget v81, Lcom/google/android/apps/plus/R$drawable;->ic_community_notify:I

    const/16 v4, 0x30

    move/from16 v0, v61

    if-eq v0, v4, :cond_7

    const/16 v4, 0x17

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v84

    const/16 v83, 0x0

    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v52

    invoke-static/range {v84 .. v84}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v9, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v0, v4, v5, v9}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatar(Ljava/lang/String;IZ)Ljava/lang/Object;

    move-result-object v83

    check-cast v83, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v83, :cond_7

    move-object/from16 v51, v83

    :cond_7
    :goto_3
    sparse-switch v61, :sswitch_data_0

    const-string v4, "AndroidNotification"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "AndroidNotification"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Unknown notification type: "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v61

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_4
    :sswitch_0
    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v91

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v4, v4

    const/high16 v5, 0x8000000

    const/4 v9, 0x0

    move-object/from16 v0, v91

    invoke-virtual {v0, v4, v5, v9}, Lvedroid/support/v4/app/TaskStackBuilder;->getPendingIntent(IILandroid/os/Bundle;)Landroid/app/PendingIntent;

    move-result-object v73

    new-instance v27, Landroid/app/Notification$Builder;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v92

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v4

    move-wide/from16 v0, v94

    invoke-virtual {v4, v0, v1}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v4

    move/from16 v0, v81

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    move-object/from16 v0, v73

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/service/EsService;->getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Notification$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    if-eqz v22, :cond_48

    new-instance v4, Landroid/app/Notification$BigPictureStyle;

    invoke-direct {v4}, Landroid/app/Notification$BigPictureStyle;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigPictureStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigPictureStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigPictureStyle;->bigPicture(Landroid/graphics/Bitmap;)Landroid/app/Notification$BigPictureStyle;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    :cond_9
    :goto_5
    if-eqz v51, :cond_a

    move-object/from16 v0, v27

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    :cond_a
    if-eqz v74, :cond_b

    move-object/from16 v0, v27

    move/from16 v1, v74

    move-object/from16 v2, v76

    move-object/from16 v3, v75

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_b
    if-eqz v78, :cond_c

    move-object/from16 v0, v27

    move/from16 v1, v78

    move-object/from16 v2, v80

    move-object/from16 v3, v79

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    :cond_c
    const v4, 0xffff

    if-eq v8, v4, :cond_d

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->hasRingtone(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4b

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->getRingtone(Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    :cond_d
    :goto_6
    invoke-virtual/range {v27 .. v27}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    :goto_7
    return-object v4

    :cond_e
    const/16 v63, 0x0

    invoke-virtual/range {v20 .. v20}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v62

    add-int/lit8 v44, v62, -0x1

    :goto_8
    if-ltz v44, :cond_f

    move-object/from16 v0, v20

    move/from16 v1, v44

    invoke-virtual {v0, v1}, Lvedroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Ljava/util/List;

    invoke-interface/range {v35 .. v35}, Ljava/util/List;->size()I

    move-result v4

    add-int v63, v63, v4

    add-int/lit8 v44, v44, -0x1

    goto :goto_8

    :cond_f
    const-string v4, "AndroidNotification"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "# actors "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v63

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " # of actions "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v62

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/service/AndroidNotification;->countActorsForComment(Lvedroid/support/v4/util/SparseArrayCompat;)I

    move-result v5

    const/4 v4, 0x1

    move/from16 v0, v62

    if-ne v0, v4, :cond_10

    packed-switch v61, :pswitch_data_0

    :pswitch_0
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_post:I

    :goto_9
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {v63 .. v63}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v9

    move-object/from16 v0, v77

    move/from16 v1, v63

    invoke-virtual {v0, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    move-object/from16 v24, v33

    move-object/from16 v32, v40

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidNotification;->getNamesForDisplay(Landroid/content/Context;Lvedroid/support/v4/util/SparseArrayCompat;)Ljava/util/List;

    move-result-object v58

    move-object/from16 v23, v40

    if-eqz v58, :cond_5

    invoke-interface/range {v58 .. v58}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    move/from16 v0, v62

    if-ne v0, v4, :cond_12

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, v58

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    goto/16 :goto_1

    :pswitch_1
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_comment:I

    goto :goto_9

    :pswitch_2
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_plus_one:I

    goto :goto_9

    :pswitch_3
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_reshare:I

    goto :goto_9

    :pswitch_4
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_mention:I

    goto :goto_9

    :pswitch_5
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_share:I

    goto :goto_9

    :cond_10
    if-lez v5, :cond_11

    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_single_post_title_comment:I

    move/from16 v63, v5

    goto :goto_9

    :cond_11
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notification_single_post:I

    goto :goto_9

    :cond_12
    const/4 v4, 0x1

    move/from16 v0, v62

    if-le v0, v4, :cond_5

    move-object/from16 v59, v58

    goto/16 :goto_1

    :cond_13
    const-string v4, "AndroidNotification"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_14

    const-string v4, "AndroidNotification"

    const-string v5, "newPostCommentsActivityPendingIntent returning null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    const/16 v79, 0x0

    goto/16 :goto_2

    :catch_0
    move-exception v37

    const-string v4, "AndroidNotification"

    const-string v5, "Cannot download square avatar"

    move-object/from16 v0, v37

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    :sswitch_1
    const/4 v4, 0x0

    goto/16 :goto_7

    :sswitch_2
    sget v4, Lcom/google/android/apps/plus/R$string;->friends_circle_name:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v41 .. v41}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_19

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v15

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v45

    :cond_15
    :goto_a
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v34

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-static/range {v42 .. v42}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_15

    move-object/from16 v0, v42

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_15

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPackedCircleIds(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v71

    if-nez v71, :cond_15

    move-object/from16 v0, v19

    move-object/from16 v1, v34

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_16
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_19

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v64

    if-lez v64, :cond_1b

    sget v74, Lcom/google/android/apps/plus/R$drawable;->stat_notify_adduser:I

    const/4 v4, 0x1

    move/from16 v0, v64

    if-ne v0, v4, :cond_17

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_action_add_to_friends:I

    :goto_b
    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v76

    const/4 v4, 0x1

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v30, v0

    const/4 v4, 0x0

    aput-object v41, v30, v4

    move/from16 v0, v64

    new-array v0, v0, [Lcom/google/android/apps/plus/util/ParticipantParcelable;

    move-object/from16 v72, v0

    const/16 v44, 0x0

    :goto_c
    move/from16 v0, v44

    move/from16 v1, v64

    if-ge v0, v1, :cond_18

    move-object/from16 v0, v19

    move/from16 v1, v44

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/api/services/plusi/model/DataActor;

    new-instance v4, Lcom/google/android/apps/plus/util/ParticipantParcelable;

    move-object/from16 v0, v34

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "g:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v34

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v5, v9}, Lcom/google/android/apps/plus/util/ParticipantParcelable;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v72, v44

    add-int/lit8 v44, v44, 0x1

    goto :goto_c

    :cond_17
    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_action_add_all_to_friends:I

    goto :goto_b

    :cond_18
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v72

    move-object/from16 v3, v30

    invoke-static {v0, v1, v2, v3, v7}, Lcom/google/android/apps/plus/service/EsService;->createAddPeopleToCirclePendingIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/util/ParticipantParcelable;[Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v75

    :cond_19
    :goto_d
    :sswitch_3
    sget v81, Lcom/google/android/apps/plus/R$drawable;->stat_notify_circle:I

    if-eqz v18, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    const/16 v4, 0x27

    move/from16 v0, v61

    if-ne v0, v4, :cond_1c

    const/16 v50, 0x1

    :goto_e
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_21

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    if-eqz v50, :cond_1d

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_content_added_back_to_circle:I

    :goto_f
    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    if-eqz v50, :cond_1a

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v34, :cond_1e

    const/4 v4, 0x1

    :goto_10
    move-object/from16 v0, v34

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    if-eqz v5, :cond_1f

    const/4 v5, 0x1

    :goto_11
    and-int/2addr v4, v5

    if-eqz v4, :cond_1a

    sget v74, Lcom/google/android/apps/plus/R$drawable;->stat_notify_comment:I

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_action_say_hello:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v76

    if-nez v17, :cond_20

    const/4 v4, 0x0

    :goto_12
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v4}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    long-to-int v5, v9

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v75

    :cond_1a
    :goto_13
    move-object/from16 v24, v33

    move-object/from16 v23, v32

    goto/16 :goto_4

    :cond_1b
    sget v74, Lcom/google/android/apps/plus/R$drawable;->stat_notify_addeduser:I

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_action_added_to_friends:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v76

    const/16 v75, 0x0

    goto :goto_d

    :cond_1c
    const/16 v50, 0x0

    goto :goto_e

    :cond_1d
    sget v4, Lcom/google/android/apps/plus/R$string;->notification_content_added_to_circle:I

    goto :goto_f

    :cond_1e
    const/4 v4, 0x0

    goto :goto_10

    :cond_1f
    const/4 v5, 0x0

    goto :goto_11

    :cond_20
    new-instance v4, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v5, Lcom/google/android/apps/plus/content/PersonData;

    move-object/from16 v0, v17

    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct {v5, v9, v10, v11}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_12

    :cond_21
    if-eqz v50, :cond_22

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_title_added_back_to_circle:I

    :goto_14
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v9

    move-object/from16 v0, v77

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/plus/service/AndroidNotification;->getActorNamesForDisplay(Ljava/util/List;)Ljava/lang/String;

    move-result-object v32

    goto :goto_13

    :cond_22
    sget v4, Lcom/google/android/apps/plus/R$string;->notification_title_added_to_circle:I

    goto :goto_14

    :sswitch_4
    sget v81, Lcom/google/android/apps/plus/R$drawable;->stat_notify_instant_upload:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->stat_notify_instant_upload:I

    move-object/from16 v0, v77

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v51

    const/4 v4, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v70

    const/16 v4, 0x13

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v39

    invoke-static {}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    move-result-object v4

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Lcom/google/api/services/plusi/model/EntityPhotosData;

    const/16 v4, 0x18

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v90

    const/16 v4, 0x19

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v88

    invoke-static/range {v88 .. v88}, Lcom/google/android/apps/plus/content/DbDataAction;->deserializeDataActorList([B)Ljava/util/List;

    move-result-object v87

    if-nez v38, :cond_2a

    const/16 v65, 0x0

    const/16 v69, 0x0

    const/16 v55, 0x0

    :goto_15
    move-object/from16 v0, v55

    move-object/from16 v1, v87

    move-object/from16 v2, v90

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/PhotoTaggeeData;->createMediaRefUserMap(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v54

    sget v4, Lcom/google/android/apps/plus/R$dimen;->notification_bigpicture_width:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    move/from16 v96, v0

    sget v4, Lcom/google/android/apps/plus/R$dimen;->notification_bigpicture_height:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    move/from16 v43, v0

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v55 .. v55}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v45

    :cond_23
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_25

    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v53

    check-cast v53, Lcom/google/android/apps/plus/api/MediaRef;

    :try_start_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v52

    const/4 v4, 0x0

    move-object/from16 v0, v52

    move-object/from16 v1, v53

    move/from16 v2, v96

    move/from16 v3, v43

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/graphics/Bitmap;

    if-eqz v25, :cond_24

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_24
    :goto_16
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x3

    if-lt v4, v5, :cond_23

    :cond_25
    if-eqz v26, :cond_26

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2e

    :cond_26
    const/16 v22, 0x0

    :cond_27
    :goto_17
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_28

    sget v4, Lcom/google/android/apps/plus/R$dimen;->notification_large_icon_size:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    move/from16 v46, v0

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    move/from16 v0, v46

    int-to-float v5, v0

    move/from16 v0, v46

    int-to-float v9, v0

    invoke-static {v4, v5, v9}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v51

    :cond_28
    add-int v67, v65, v69

    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_instant_upload_title:I

    move-object/from16 v0, v77

    move/from16 v1, v67

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v33

    if-nez v87, :cond_31

    const/4 v4, 0x0

    move v5, v4

    :goto_18
    packed-switch v5, :pswitch_data_1

    sget v9, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_more_than_two_taggee_names:I

    const/4 v4, 0x2

    new-array v10, v4, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v87

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    aput-object v4, v10, v11

    const/4 v4, 0x1

    add-int/lit8 v5, v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v10, v4

    move-object/from16 v0, v77

    invoke-virtual {v0, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v89

    :goto_19
    invoke-static/range {v89 .. v89}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_33

    const/4 v4, 0x1

    move/from16 v0, v67

    if-ne v0, v4, :cond_32

    const/16 v32, 0x0

    :goto_1a
    move-object/from16 v23, v32

    move-object/from16 v24, v33

    sget v74, Lcom/google/android/apps/plus/R$drawable;->stat_notify_reshare:I

    sget v4, Lcom/google/android/apps/plus/R$string;->post_share_button_text:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v76

    if-eqz v55, :cond_29

    invoke-virtual/range {v55 .. v55}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3a

    :cond_29
    const/16 v75, 0x0

    :goto_1b
    goto/16 :goto_4

    :cond_2a
    move-object/from16 v0, v38

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->numPhotos:Ljava/lang/Integer;

    if-nez v4, :cond_2b

    const/16 v65, 0x0

    :goto_1c
    move-object/from16 v0, v38

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->numVideos:Ljava/lang/Integer;

    if-nez v4, :cond_2c

    const/16 v69, 0x0

    :goto_1d
    move-object/from16 v0, v38

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    move-object/from16 v36, v0

    if-nez v36, :cond_2d

    const/16 v55, 0x0

    goto/16 :goto_15

    :cond_2b
    move-object/from16 v0, v38

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->numPhotos:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v65

    goto :goto_1c

    :cond_2c
    move-object/from16 v0, v38

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->numVideos:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v69

    goto :goto_1d

    :cond_2d
    move-object/from16 v0, v70

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidNotification;->createMediaRefList(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v55

    goto/16 :goto_15

    :catch_1
    move-exception v37

    const-string v4, "AndroidNotification"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Could not download image "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v53

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v37

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_16

    :cond_2e
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v96

    move/from16 v1, v43

    invoke-static {v0, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v22

    new-instance v9, Landroid/graphics/Canvas;

    move-object/from16 v0, v22

    invoke-direct {v9, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    const/high16 v5, -0x1000000

    invoke-virtual {v14, v5}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v5, 0x40800000

    invoke-virtual {v14, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2f

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    move/from16 v0, v96

    int-to-float v5, v0

    move/from16 v0, v43

    int-to-float v9, v0

    invoke-static {v4, v5, v9}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v22

    goto/16 :goto_17

    :cond_2f
    const/4 v5, 0x2

    if-ne v4, v5, :cond_30

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    div-int/lit8 v5, v96, 0x2

    int-to-float v5, v5

    move/from16 v0, v43

    int-to-float v10, v0

    invoke-static {v4, v5, v10}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v5

    const/4 v4, 0x1

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    div-int/lit8 v10, v96, 0x2

    int-to-float v10, v10

    move/from16 v0, v43

    int-to-float v11, v0

    invoke-static {v4, v10, v11}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v9, v5, v10, v11, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v5, v96, 0x2

    int-to-float v5, v5

    const/4 v10, 0x0

    invoke-virtual {v9, v4, v5, v10, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    div-int/lit8 v4, v96, 0x2

    int-to-float v10, v4

    const/4 v11, 0x0

    div-int/lit8 v4, v96, 0x2

    int-to-float v12, v4

    move/from16 v0, v43

    int-to-float v13, v0

    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_17

    :cond_30
    const/4 v5, 0x3

    if-lt v4, v5, :cond_27

    div-int/lit8 v5, v96, 0x3

    div-int/lit8 v97, v43, 0x2

    const/4 v4, 0x0

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    sub-int v10, v96, v5

    int-to-float v10, v10

    move/from16 v0, v43

    int-to-float v11, v0

    invoke-static {v4, v10, v11}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v10

    const/4 v4, 0x1

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    int-to-float v11, v5

    move/from16 v0, v97

    int-to-float v12, v0

    invoke-static {v4, v11, v12}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v11

    const/4 v4, 0x2

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    int-to-float v12, v5

    move/from16 v0, v97

    int-to-float v13, v0

    invoke-static {v4, v12, v13}, Lcom/google/android/apps/plus/content/EsMediaCache;->cropPhoto$5d96f1cd(Landroid/graphics/Bitmap;FF)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v9, v10, v12, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sub-int v10, v96, v5

    int-to-float v10, v10

    const/4 v12, 0x0

    invoke-virtual {v9, v11, v10, v12, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sub-int v10, v96, v5

    int-to-float v10, v10

    move/from16 v0, v97

    int-to-float v11, v0

    invoke-virtual {v9, v4, v10, v11, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sub-int v4, v96, v5

    int-to-float v10, v4

    const/4 v11, 0x0

    sub-int v4, v96, v5

    int-to-float v12, v4

    move/from16 v0, v43

    int-to-float v13, v0

    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sub-int v4, v96, v5

    int-to-float v10, v4

    move/from16 v0, v97

    int-to-float v11, v0

    move/from16 v0, v96

    int-to-float v12, v0

    move/from16 v0, v97

    int-to-float v13, v0

    invoke-virtual/range {v9 .. v14}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_17

    :cond_31
    invoke-interface/range {v87 .. v87}, Ljava/util/List;->size()I

    move-result v4

    move v5, v4

    goto/16 :goto_18

    :pswitch_6
    const/16 v89, 0x0

    goto/16 :goto_19

    :pswitch_7
    const/4 v4, 0x0

    move-object/from16 v0, v87

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    move-object/from16 v89, v0

    goto/16 :goto_19

    :pswitch_8
    sget v5, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_two_taggee_names:I

    const/4 v4, 0x2

    new-array v9, v4, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v87

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    aput-object v4, v9, v10

    const/4 v10, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, v87

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    aput-object v4, v9, v10

    move-object/from16 v0, v77

    invoke-virtual {v0, v5, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v89

    goto/16 :goto_19

    :cond_32
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notifications_instant_upload_text:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {v67 .. v67}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v9

    move-object/from16 v0, v77

    move/from16 v1, v67

    invoke-virtual {v0, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_1a

    :cond_33
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget v5, Lcom/google/android/apps/plus/R$plurals;->notifications_instant_upload_known_faces_text:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v89, v9, v10

    move-object/from16 v0, v77

    move/from16 v1, v67

    invoke-virtual {v0, v5, v1, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v87, :cond_35

    const/4 v4, 0x0

    :goto_1e
    const/4 v9, 0x1

    if-ne v4, v9, :cond_39

    const/4 v4, 0x0

    move-object/from16 v0, v87

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v4, :cond_34

    iget-object v9, v4, Lcom/google/api/services/plusi/model/DataActor;->gender:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_36

    :cond_34
    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_share_with_them:I

    :goto_1f
    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_1a

    :cond_35
    invoke-interface/range {v87 .. v87}, Ljava/util/List;->size()I

    move-result v4

    goto :goto_1e

    :cond_36
    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataActor;->gender:Ljava/lang/String;

    const-string v9, "male"

    invoke-static {v9, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_37

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_share_with_him:I

    goto :goto_1f

    :cond_37
    const-string v9, "female"

    invoke-static {v9, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_38

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_share_with_her:I

    goto :goto_1f

    :cond_38
    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_share_with_them:I

    goto :goto_1f

    :cond_39
    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_instant_upload_share_with_them:I

    goto :goto_1f

    :cond_3a
    invoke-virtual/range {v55 .. v55}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3b

    const/4 v4, 0x0

    move-object/from16 v0, v55

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v54

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/AndroidNotification;->createAudienceData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v55

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    :goto_20
    if-eqz v4, :cond_3c

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    if-eqz v5, :cond_3c

    const-string v5, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v9, 0x1

    invoke-virtual {v4, v5, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v5, "notif_id"

    invoke-virtual {v4, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "coalescing_id"

    move-object/from16 v0, v31

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v5, Ljava/util/ArrayList;

    const/4 v9, 0x1

    invoke-direct {v5, v9}, Ljava/util/ArrayList;-><init>(I)V

    const/16 v9, 0x12

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v9, "notif_types"

    invoke-virtual {v4, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    long-to-int v5, v9

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v75

    goto/16 :goto_1b

    :cond_3b
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->createAudienceDataForYourCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v14

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v7

    move-object/from16 v12, v55

    move-object/from16 v13, v54

    invoke-static/range {v9 .. v14}, Lcom/google/android/apps/plus/service/AndroidNotification;->getPhotosSelectionActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/Map;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_20

    :cond_3c
    const-string v4, "AndroidNotification"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3d

    const-string v4, "AndroidNotification"

    const-string v5, "newInstantUploadAlbumIntent returning null"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3d
    const/16 v75, 0x0

    goto/16 :goto_1b

    :sswitch_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/plus/content/EsPostsData;->isActivityPlusOnedByViewer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z

    move-result v49

    if-nez v49, :cond_3e

    sget v74, Lcom/google/android/apps/plus/R$drawable;->stat_notify_plusone:I

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_action_plusone_post:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v76

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->getCreatePostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v75

    :cond_3e
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityImageData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v22

    if-nez v22, :cond_3f

    const-string v4, "AndroidNotification"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3f

    const-string v4, "AndroidNotification"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "failed to decode media object for "

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3f
    if-nez v23, :cond_8

    move-object/from16 v23, v92

    goto/16 :goto_4

    :sswitch_6
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_8

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v48

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    move-object/from16 v0, v48

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v82 .. v82}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_invite:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v82, v5, v9

    move-object/from16 v0, v77

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_4

    :sswitch_7
    invoke-static/range {v82 .. v82}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    move-object/from16 v33, v82

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_membership_approved:I

    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_4

    :sswitch_8
    invoke-static/range {v82 .. v82}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    const/16 v4, 0x14

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v28

    invoke-static {}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v85

    check-cast v85, Lcom/google/api/services/plusi/model/EntitySquaresData;

    if-eqz v85, :cond_8

    invoke-static/range {v85 .. v85}, Lcom/google/android/apps/plus/content/EsNotificationData;->getNumSquarePosts(Lcom/google/api/services/plusi/model/EntitySquaresData;)I

    move-result v66

    invoke-static/range {v85 .. v85}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadSquarePosts(Lcom/google/api/services/plusi/model/EntitySquaresData;)I

    move-result v68

    const-string v4, "AndroidNotification"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_40

    const-string v4, "AndroidNotification"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v9, "Square subscription numPosts="

    invoke-direct {v5, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v66

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " numUnread="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v68

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_40
    const/4 v4, 0x1

    move/from16 v0, v66

    if-le v0, v4, :cond_42

    move-object/from16 v33, v82

    if-lez v68, :cond_41

    sget v4, Lcom/google/android/apps/plus/R$plurals;->notification_square_multi_post_unread:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {v68 .. v68}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v5, v9

    move-object/from16 v0, v77

    move/from16 v1, v68

    invoke-virtual {v0, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_4

    :cond_41
    sget v4, Lcom/google/android/apps/plus/R$plurals;->notification_square_multi_post_read:I

    move-object/from16 v0, v77

    move/from16 v1, v66

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_4

    :cond_42
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_8

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    move-object/from16 v33, v82

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_single_post_share:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    aput-object v10, v5, v9

    move-object/from16 v0, v77

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_4

    :sswitch_9
    const/16 v93, 0x0

    const/16 v56, 0x0

    invoke-static/range {v82 .. v82}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_43

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_43

    const/16 v4, 0x14

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v28

    invoke-static {}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v85

    check-cast v85, Lcom/google/api/services/plusi/model/EntitySquaresData;

    if-eqz v85, :cond_43

    move-object/from16 v0, v85

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    if-eqz v4, :cond_43

    move-object/from16 v0, v85

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_43

    move-object/from16 v0, v85

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v60

    check-cast v60, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;

    move-object/from16 v0, v60

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;->newModeratorOid:Ljava/util/List;

    if-eqz v4, :cond_43

    move-object/from16 v0, v60

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;->newModeratorOid:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_43

    move-object/from16 v0, v60

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;->newModeratorOid:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v56

    check-cast v56, Ljava/lang/String;

    move-object/from16 v0, v60

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModerator;->toOwner:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v93

    :cond_43
    invoke-static/range {v56 .. v56}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    move-object/from16 v0, p1

    move-object/from16 v1, v56

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_45

    move-object/from16 v33, v82

    if-eqz v93, :cond_44

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_promoted_to_owner:I

    :goto_21
    move-object/from16 v0, v77

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v32

    goto/16 :goto_4

    :cond_44
    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_promoted_to_mod:I

    goto :goto_21

    :cond_45
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v45

    :cond_46
    :goto_22
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v56

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_46

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_46

    move-object/from16 v33, v82

    if-eqz v93, :cond_47

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_new_owner:I

    :goto_23
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, v17

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    aput-object v10, v5, v9

    move-object/from16 v0, v77

    invoke-virtual {v0, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    goto :goto_22

    :cond_47
    sget v4, Lcom/google/android/apps/plus/R$string;->notification_square_new_moderator:I

    goto :goto_23

    :cond_48
    if-eqz v23, :cond_9

    if-nez v59, :cond_49

    new-instance v4, Landroid/app/Notification$BigTextStyle;

    invoke-direct {v4}, Landroid/app/Notification$BigTextStyle;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigTextStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/app/Notification$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/app/Notification$BigTextStyle;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setStyle(Landroid/app/Notification$Style;)Landroid/app/Notification$Builder;

    goto/16 :goto_5

    :cond_49
    new-instance v4, Landroid/app/Notification$InboxStyle;

    move-object/from16 v0, v27

    invoke-direct {v4, v0}, Landroid/app/Notification$InboxStyle;-><init>(Landroid/app/Notification$Builder;)V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Landroid/app/Notification$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    move-result-object v47

    invoke-interface/range {v59 .. v59}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v45

    :goto_24
    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4a

    invoke-interface/range {v45 .. v45}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v57

    check-cast v57, Ljava/lang/CharSequence;

    move-object/from16 v0, v47

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto :goto_24

    :cond_4a
    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, " "

    move-object/from16 v0, v47

    invoke-virtual {v0, v4}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    move-object/from16 v0, v47

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/app/Notification$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/app/Notification$InboxStyle;

    goto/16 :goto_5

    :cond_4b
    const/4 v4, 0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    goto/16 :goto_6

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
        0xf -> :sswitch_0
        0x10 -> :sswitch_5
        0x12 -> :sswitch_4
        0x18 -> :sswitch_5
        0x21 -> :sswitch_1
        0x27 -> :sswitch_3
        0x30 -> :sswitch_6
        0x31 -> :sswitch_8
        0x33 -> :sswitch_7
        0x41 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static getActorAvatars$d8296a3(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v1, :cond_0

    iget-object v7, v1, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    if-eqz v7, :cond_0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v6

    iget-object v7, v1, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatar(Ljava/lang/String;IZ)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v2, v0

    if-eqz v2, :cond_0

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v7, "AndroidNotification"

    const-string v8, "Cannot download square avatar"

    invoke-static {v7, v8, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method private static getActorNamesForDisplay(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p0, :cond_0

    const-string v3, ""

    :goto_0
    return-object v3

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataActor;

    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static getFormattedActors(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v3, "#%06X"

    new-array v4, v9, [Ljava/lang/Object;

    const v5, 0xffffff

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$color;->android_notifications_bright_text_color:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    and-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$string;->notification_actors_for_action_description:I

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v8

    aput-object p1, v4, v9

    const/4 v5, 0x2

    aput-object p2, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/SpannableString;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    return-object v2
.end method

.method private static getNamesForDisplay(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    if-nez p0, :cond_0

    const-string v3, ""

    :goto_0
    return-object v3

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static getNamesForDisplay(Landroid/content/Context;Lvedroid/support/v4/util/SparseArrayCompat;)Ljava/util/List;
    .locals 11
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lvedroid/support/v4/util/SparseArrayCompat",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    const/4 v10, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    invoke-virtual {p1, v10}, Lvedroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-static {v8}, Lcom/google/android/apps/plus/service/AndroidNotification;->getNamesForDisplay(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Lvedroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    :goto_1
    if-ltz v0, :cond_2

    invoke-virtual {p1, v0}, Lvedroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v6

    invoke-virtual {p1, v6}, Lvedroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-static {v8}, Lcom/google/android/apps/plus/service/AndroidNotification;->getNamesForDisplay(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    sget v7, Lcom/google/android/apps/plus/R$string;->notifications_single_post_action_post:I

    :goto_2
    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v3}, Lcom/google/android/apps/plus/service/AndroidNotification;->getFormattedActors(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v8

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :pswitch_1
    sget v7, Lcom/google/android/apps/plus/R$string;->notifications_single_post_action_comment:I

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v3}, Lcom/google/android/apps/plus/service/AndroidNotification;->getFormattedActors(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v2

    goto :goto_3

    :pswitch_2
    sget v7, Lcom/google/android/apps/plus/R$string;->notifications_single_post_action_reshare:I

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v3}, Lcom/google/android/apps/plus/service/AndroidNotification;->getFormattedActors(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v5

    goto :goto_3

    :pswitch_3
    sget v7, Lcom/google/android/apps/plus/R$string;->notifications_single_post_action_plus_one:I

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8, v3}, Lcom/google/android/apps/plus/service/AndroidNotification;->getFormattedActors(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v4

    goto :goto_3

    :pswitch_4
    sget v7, Lcom/google/android/apps/plus/R$string;->notifications_single_post_action_mention:I

    goto :goto_2

    :pswitch_5
    sget v7, Lcom/google/android/apps/plus/R$string;->notifications_single_post_action_share:I

    goto :goto_2

    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-interface {v1, v10, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_3
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-interface {v1, v10, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_4
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-interface {v1, v10, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static getPhotosSelectionActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/Map;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/content/AudienceData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PhotoTaggeeData$PhotoTaggee;",
            ">;>;",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v1, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosSelectionActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setMediaRefUserMap(Ljava/util/Map;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method public static getRingtone(Landroid/content/Context;)Landroid/net/Uri;
    .locals 5
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_key:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_default_value:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    return-object v4
.end method

.method private static hasOnlyHangoutNotifications(Landroid/database/Cursor;)Z
    .locals 3
    .param p0    # Landroid/database/Cursor;

    const/4 v1, 0x1

    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    const/4 v1, 0x0

    :cond_1
    return v1
.end method

.method public static hasRingtone(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_key:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_default_value:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static isRunningJellybeanOrLater()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static newViewNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Lvedroid/support/v4/app/TaskStackBuilder;)Landroid/content/Intent;
    .locals 65
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Lvedroid/support/v4/app/TaskStackBuilder;

    const/4 v4, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    const v4, 0xffff

    if-ne v15, v4, :cond_1

    const/16 v50, 0x0

    :cond_0
    :goto_0
    return-object v50

    :cond_1
    const/16 v4, 0x10

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v4, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v45

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_photo_deleted:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v59

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_event_deleted:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v48

    sget v4, Lcom/google/android/apps/plus/R$string;->notification_post_deleted:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v60

    invoke-static/range {v45 .. v45}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, v45

    move-object/from16 v1, v59

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, v45

    move-object/from16 v1, v60

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/16 v50, 0x0

    goto :goto_0

    :cond_3
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v50, 0x0

    const/16 v4, 0xe

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v4, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v4, 0xd

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    const/16 v51, 0x1

    :goto_1
    if-eqz v51, :cond_4

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v50

    :cond_4
    if-nez v50, :cond_5

    packed-switch v15, :pswitch_data_0

    :cond_5
    :goto_2
    :pswitch_0
    if-eqz v50, :cond_0

    const-string v4, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v5, 0x1

    move-object/from16 v0, v50

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v4, "notif_id"

    move-object/from16 v0, v50

    invoke-virtual {v0, v4, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/16 v4, 0xc

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_13

    const/16 v52, 0x1

    :goto_3
    const-string v4, "com.google.plus.analytics.intent.extra.NOTIFICATION_READ"

    move-object/from16 v0, v50

    move/from16 v1, v52

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v55, Ljava/util/ArrayList;

    const/4 v4, 0x1

    move-object/from16 v0, v55

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v55

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "notif_types"

    move-object/from16 v0, v50

    move-object/from16 v1, v55

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v4, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v41

    new-instance v42, Ljava/util/ArrayList;

    const/4 v4, 0x1

    move-object/from16 v0, v42

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "coalescing_codes"

    move-object/from16 v0, v50

    move-object/from16 v1, v42

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v50

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Lvedroid/support/v4/app/TaskStackBuilder;->getIntentCount()I

    move-result v4

    if-nez v4, :cond_6

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    :cond_6
    move-object/from16 v0, p3

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    goto/16 :goto_0

    :cond_7
    const/16 v51, 0x0

    goto/16 :goto_1

    :pswitch_1
    const/16 v4, 0xb

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_5

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object v14, v10

    invoke-static/range {v11 .. v17}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZZ)Landroid/content/Intent;

    move-result-object v50

    goto/16 :goto_2

    :pswitch_2
    const/4 v4, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v40

    if-eqz v40, :cond_5

    invoke-static/range {v40 .. v40}, Lcom/google/android/apps/plus/content/DbDataAction;->deserializeDataActionList([B)Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/DbDataAction;->getDataActorList(Ljava/util/List;)Ljava/util/List;

    move-result-object v36

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v34

    new-instance v54, Ljava/util/ArrayList;

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v4

    move-object/from16 v0, v54

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v49

    :cond_8
    :goto_4
    invoke-interface/range {v49 .. v49}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface/range {v49 .. v49}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/api/services/plusi/model/DataActor;

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    move-object/from16 v0, v54

    move-object/from16 v1, v35

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    :try_start_0
    invoke-static/range {v54 .. v54}, Lcom/google/android/apps/plus/content/DbDataAction;->serializeDataActorList(Ljava/util/List;)[B

    move-result-object v53

    invoke-interface/range {v54 .. v54}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-interface/range {v54 .. v54}, Ljava/util/List;->size()I

    move-result v43

    const/4 v4, 0x1

    move/from16 v0, v43

    if-ne v0, v4, :cond_a

    const/4 v4, 0x0

    move-object/from16 v0, v54

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataActor;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v10, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v50

    goto/16 :goto_2

    :cond_a
    const/4 v4, 0x1

    move/from16 v0, v43

    if-le v0, v4, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v53

    invoke-static {v0, v1, v2, v10}, Lcom/google/android/apps/plus/phone/Intents;->getAddedToCircleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[BLjava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v50

    goto/16 :goto_2

    :pswitch_3
    const/16 v4, 0x12

    if-ne v7, v4, :cond_e

    const/4 v4, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v58

    const/16 v4, 0x13

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v47

    invoke-static {}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntityPhotosDataJson;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/EntityPhotosDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v46

    check-cast v46, Lcom/google/api/services/plusi/model/EntityPhotosData;

    const/16 v4, 0x18

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v64

    const/16 v4, 0x19

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v63

    invoke-static/range {v63 .. v63}, Lcom/google/android/apps/plus/content/DbDataAction;->deserializeDataActorList([B)Ljava/util/List;

    move-result-object v62

    if-eqz v46, :cond_c

    move-object/from16 v0, v46

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    if-eqz v4, :cond_c

    move-object/from16 v0, v46

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    move-object/from16 v44, v0

    if-nez v44, :cond_b

    const/16 v19, 0x0

    :goto_5
    move-object/from16 v0, v19

    move-object/from16 v1, v62

    move-object/from16 v2, v64

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/PhotoTaggeeData;->createMediaRefUserMap(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v20

    if-eqz v19, :cond_5

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_d

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/AndroidNotification;->createAudienceData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v37

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    move-object/from16 v3, v37

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v50

    goto/16 :goto_2

    :cond_b
    move-object/from16 v0, v58

    move-object/from16 v1, v44

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidNotification;->createMediaRefList(Ljava/lang/String;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v19

    goto :goto_5

    :cond_c
    const/16 v19, 0x0

    goto :goto_5

    :cond_d
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->createAudienceDataForYourCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v21

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move-object/from16 v18, v10

    invoke-static/range {v16 .. v21}, Lcom/google/android/apps/plus/service/AndroidNotification;->getPhotosSelectionActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/Map;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v50

    goto/16 :goto_2

    :cond_e
    const/16 v4, 0x8

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    const/16 v4, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    const/4 v4, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v4, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v23

    const-wide/16 v4, 0x0

    cmp-long v4, v23, v4

    if-eqz v4, :cond_5

    new-instance v21, Lcom/google/android/apps/plus/api/MediaRef;

    const/16 v25, 0x0

    const/16 v26, 0x0

    sget-object v27, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v21 .. v27}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    move-object/from16 v0, v32

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v4

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setForceLoadId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    const/16 v4, 0x15

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_f

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setDisableComments(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    :cond_f
    invoke-virtual/range {v38 .. v38}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v50

    if-eqz p3, :cond_5

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v25, p0

    move-object/from16 v26, p1

    move-object/from16 v27, p3

    move-object/from16 v28, v22

    invoke-static/range {v25 .. v33}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->addParentStack(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lvedroid/support/v4/app/TaskStackBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :pswitch_4
    const/16 v4, 0x31

    if-ne v7, v4, :cond_11

    const/16 v4, 0x14

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v39

    invoke-static {}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->getInstance()Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    move-result-object v4

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v61

    check-cast v61, Lcom/google/api/services/plusi/model/EntitySquaresData;

    invoke-static/range {v61 .. v61}, Lcom/google/android/apps/plus/content/EsNotificationData;->getNumSquarePosts(Lcom/google/api/services/plusi/model/EntitySquaresData;)I

    move-result v56

    invoke-static/range {v61 .. v61}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadSquarePosts(Lcom/google/api/services/plusi/model/EntitySquaresData;)I

    move-result v57

    const/4 v4, 0x1

    move/from16 v0, v56

    if-eq v0, v4, :cond_10

    const/4 v4, 0x1

    move/from16 v0, v57

    if-ne v0, v4, :cond_11

    :cond_10
    const/4 v4, 0x1

    move/from16 v0, v57

    if-ne v0, v4, :cond_12

    const/4 v4, 0x1

    :goto_6
    move-object/from16 v0, v61

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsNotificationData;->getSquarePostActivityId(Lcom/google/api/services/plusi/model/EntitySquaresData;Z)Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_11

    const/16 v16, 0x1

    const/16 v17, 0x0

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object v14, v10

    invoke-static/range {v11 .. v17}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZZ)Landroid/content/Intent;

    move-result-object v50

    :cond_11
    if-nez v50, :cond_5

    const/16 v4, 0x15

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    if-eqz v27, :cond_5

    const/16 v28, 0x0

    const/16 v30, 0x1

    move-object/from16 v25, p0

    move-object/from16 v26, p1

    move-object/from16 v29, v10

    invoke-static/range {v25 .. v30}, Lcom/google/android/apps/plus/phone/Intents;->getSquareStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v50

    goto/16 :goto_2

    :cond_12
    const/4 v4, 0x0

    goto :goto_6

    :cond_13
    const/16 v52, 0x0

    goto/16 :goto_3

    :catch_0
    move-exception v4

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static newViewNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Lvedroid/support/v4/app/TaskStackBuilder;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static newViewOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Lvedroid/support/v4/app/TaskStackBuilder;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/database/Cursor;

    invoke-static {p0}, Lvedroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v2

    invoke-static {p0, p1, p2, v2}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Lvedroid/support/v4/app/TaskStackBuilder;)Landroid/content/Intent;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v2, 0x0

    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lvedroid/support/v4/app/TaskStackBuilder;->getIntentCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v4}, Lvedroid/support/v4/app/TaskStackBuilder;->editIntentAt(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_1

    move-object v3, v2

    :goto_0
    return-object v3

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/phone/Intents;->getNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v1

    const-string v4, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0}, Lvedroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-object v3, v2

    goto :goto_0
.end method

.method public static shouldNotify(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_preference_enabled_key:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$bool;->notifications_preference_enabled_default_value:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    return v4
.end method

.method public static shouldVibrate(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->notifications_preference_vibrate_key:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$bool;->notifications_preference_vibrate_default_value:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    return v4
.end method

.method public static showCircleAddFailedNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    invoke-static {p0, p1, p2, v4, v6}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x14000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v4, v4

    invoke-static {p0, v4, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Lvedroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x1080027

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v6}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->cannot_add_to_circle_error_title:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->cannot_add_to_circle_error_message:I

    new-array v5, v6, [Ljava/lang/Object;

    aput-object p3, v5, v7

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":notifications:add:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v0}, Lvedroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public static showFullSizeFirstTimeNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getInstantUploadSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x14000000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-int v4, v4

    const/4 v5, 0x0

    invoke-static {p0, v4, v2, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Lvedroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/plus/R$drawable;->stat_notify_gplus:I

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->full_size_first_time_notification_title:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->full_size_first_time_notification_text:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->full_size_first_time_notification_text:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v0}, Lvedroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public static showPartyModeExpiredNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v9, 0x1

    const/4 v8, 0x0

    sget v5, Lcom/google/android/apps/plus/R$string;->party_mode_expired_text:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->stat_notify_quota:I

    const/4 v6, 0x0

    invoke-static {p0, p1, p3, p4, v6}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v6, "notif_type"

    const/16 v7, 0x3a

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v6, 0x14000000

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-static {p0, v6, v2, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Lvedroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v9}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v6, Lcom/google/android/apps/plus/R$string;->party_mode_expired_title:I

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    new-array v6, v9, [Ljava/lang/Object;

    aput-object p2, v6, v8

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    new-array v6, v9, [Ljava/lang/Object;

    aput-object p2, v6, v8

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const-string v6, "notification"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    invoke-virtual {v0}, Lvedroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v3, v6, v7, v8}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public static showQuotaNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;IIZ)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    sub-int v8, p3, p2

    const/4 v9, 0x0

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {p0, v4}, Lcom/google/android/apps/plus/phone/InstantUpload;->getSizeText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    if-eqz p4, :cond_0

    sget v7, Lcom/google/android/apps/plus/R$string;->full_size_no_quota_text:I

    :goto_0
    sget v6, Lcom/google/android/apps/plus/R$drawable;->stat_notify_quota:I

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getInstantUploadSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v8, 0x14000000

    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-int v8, v8

    const/4 v9, 0x0

    invoke-static {p0, v8, v2, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    new-instance v0, Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v0, p0}, Lvedroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Lvedroid/support/v4/app/NotificationCompat$Builder;

    sget v8, Lcom/google/android/apps/plus/R$string;->instant_upload_notification_title:I

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-virtual {p0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Lvedroid/support/v4/app/NotificationCompat$Builder;

    const-string v8, "notification"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    invoke-virtual {v0}, Lvedroid/support/v4/app/NotificationCompat$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v10

    invoke-virtual {v3, v8, v9, v10}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void

    :cond_0
    sget v7, Lcom/google/android/apps/plus/R$string;->full_size_low_quota_text:I

    goto :goto_0
.end method

.method public static showUpgradeRequiredNotification(Landroid/content/Context;)V
    .locals 9
    .param p0    # Landroid/content/Context;

    const-string v7, "notification"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget v7, Lcom/google/android/apps/plus/R$string;->signup_required_update_available:I

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v2, Landroid/app/Notification;

    sget v7, Lcom/google/android/apps/plus/R$drawable;->ic_stat_gplus:I

    invoke-direct {v2, v7, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    new-instance v1, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v7, 0x80000

    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v7, "market://details?id=com.google.android.apps.plus"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    const/high16 v7, 0x14000000

    invoke-virtual {v1, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    long-to-int v7, v7

    const/4 v8, 0x0

    invoke-static {p0, v7, v1, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    sget v7, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, p0, v7, v4, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v7, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x10

    iput v7, v2, Landroid/app/Notification;->flags:I

    iget v7, v2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v7, v7, 0x4

    iput v7, v2, Landroid/app/Notification;->defaults:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":notifications:upgrade"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v3, v7, v8, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void
.end method

.method public static declared-synchronized update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const-class v4, Lcom/google/android/apps/plus/service/AndroidNotification;

    monitor-enter v4

    :try_start_0
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->buildNotificationTag(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldNotify(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    monitor-exit v4

    return-void

    :cond_0
    :try_start_1
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->createNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/Notification;

    move-result-object v0

    if-eqz v0, :cond_2

    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v0, Landroid/app/Notification;->flags:I

    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Landroid/app/Notification;->flags:I

    iget v3, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v0, Landroid/app/Notification;->flags:I

    const/4 v3, -0x1

    iput v3, v0, Landroid/app/Notification;->ledARGB:I

    const/16 v3, 0x1f4

    iput v3, v0, Landroid/app/Notification;->ledOnMS:I

    const/16 v3, 0x7d0

    iput v3, v0, Landroid/app/Notification;->ledOffMS:I

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidNotification;->shouldVibrate(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v0, Landroid/app/Notification;->defaults:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Landroid/app/Notification;->defaults:I

    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3

    :cond_2
    const/4 v3, 0x1

    :try_start_2
    invoke-static {p0, p1, v3}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method
