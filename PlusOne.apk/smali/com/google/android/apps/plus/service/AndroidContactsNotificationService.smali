.class public Lcom/google/android/apps/plus/service/AndroidContactsNotificationService;
.super Landroid/app/IntentService;
.source "AndroidContactsNotificationService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "ContactsNotificationSvc"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "ContactsNotificationSvc"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ContactsNotificationSvc"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Contact opened in Contacts: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncRawContact(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method
