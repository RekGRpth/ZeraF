.class final Lcom/google/android/apps/plus/service/EsService$7;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/EsService;->processIntent1(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;II)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$7;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$context:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$intent:Landroid/content/Intent;

    const-string v4, "person_id"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$intent:Landroid/content/Intent;

    const-string v4, "refresh"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->refreshProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$7;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$intent:Landroid/content/Intent;

    new-instance v5, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v5}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$context:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x1

    invoke-static {v3, v4, v1, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->getProfileAndContactData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$7;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$7;->val$intent:Landroid/content/Intent;

    new-instance v5, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v5, v8, v7, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_1
.end method
