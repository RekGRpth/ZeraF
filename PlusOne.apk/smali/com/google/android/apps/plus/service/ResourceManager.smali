.class public abstract Lcom/google/android/apps/plus/service/ResourceManager;
.super Ljava/lang/Object;
.source "ResourceManager.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/ResourceManager$BlockingLoader;,
        Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;,
        Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;,
        Lcom/google/android/apps/plus/service/ResourceManager$ResourceData;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mLoaderThread:Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;

.field private final mMainThreadHandler:Landroid/os/Handler;

.field private final mPreloadConsumer:Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;

.field private mPreloadingResources:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;",
            "Lcom/google/android/apps/plus/service/Resource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;-><init>(Lcom/google/android/apps/plus/service/ResourceManager;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadConsumer:Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadingResources:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/service/ResourceManager;Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/service/ResourceManager;
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Finished preloading resource: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadingResources:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadConsumer:Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/service/ResourceManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/ResourceManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected final deliverHttpError(Lcom/google/android/apps/plus/service/Resource;II)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3, p3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/plus/service/ResourceManager$ResourceData;

    invoke-direct {v4, p1, p3}, Lcom/google/android/apps/plus/service/ResourceManager$ResourceData;-><init>(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final deliverResourceStatus(Lcom/google/android/apps/plus/service/Resource;I)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p2, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method protected final deliverResourceType(Lcom/google/android/apps/plus/service/Resource;I)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mMainThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    const/4 v3, 0x1

    return v3

    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/plus/service/Resource;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iput v3, v1, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->notifyConsumers()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/plus/service/Resource;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iput v3, v1, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    iput v3, v1, Lcom/google/android/apps/plus/service/Resource;->mHttpStatusCode:I

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->notifyConsumers()V

    goto :goto_0

    :pswitch_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/plus/service/ResourceManager$ResourceData;

    iget-object v1, v2, Lcom/google/android/apps/plus/service/ResourceManager$ResourceData;->resource:Lcom/google/android/apps/plus/service/Resource;

    iget-object v3, v2, Lcom/google/android/apps/plus/service/ResourceManager$ResourceData;->data:Ljava/lang/Object;

    iput-object v3, v1, Lcom/google/android/apps/plus/service/Resource;->mResource:Ljava/lang/Object;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iput v3, v1, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->notifyConsumers()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/plus/service/MediaResource;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iput v3, v1, Lcom/google/android/apps/plus/service/MediaResource;->mResourceType:I

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/MediaResource;->notifyConsumers()V

    goto :goto_0

    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/plus/service/ResourceConsumer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/service/ResourceConsumer;->bindResources()V

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/plus/service/ResourceConsumer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/service/ResourceConsumer;->unbindResources()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final isPreloadingResource(Lcom/google/android/apps/plus/service/Resource;)Z
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadConsumer:Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/service/Resource;->isRegistered(Lcom/google/android/apps/plus/service/ResourceConsumer;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getResourceConsumerCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final loadResource(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mLoaderThread:Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mLoaderThread:Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mLoaderThread:Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;->start()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mLoaderThread:Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/ResourceManager$LoaderThread;->loadResource(Lcom/google/android/apps/plus/service/Resource;)V

    return-void
.end method

.method public onFirstConsumerRegistered(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    return-void
.end method

.method public onLastConsumerUnregistered(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    return-void
.end method

.method protected final preloadResource(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Preloading resource: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadingResources:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceManager;->mPreloadConsumer:Lcom/google/android/apps/plus/service/ResourceManager$NotifyingConsumer;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/service/Resource;->register(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    return-void
.end method
