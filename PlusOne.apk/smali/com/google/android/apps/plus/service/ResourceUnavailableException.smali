.class public final Lcom/google/android/apps/plus/service/ResourceUnavailableException;
.super Ljava/lang/Exception;
.source "ResourceUnavailableException.java"


# instance fields
.field private final mHttpStatus:I

.field private final mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

.field private final mStatus:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;II)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;
    .param p2    # I
    .param p3    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Error obtaining resource "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/apps/plus/service/Resource;->statusToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " httpError: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceUnavailableException;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    iput p2, p0, Lcom/google/android/apps/plus/service/ResourceUnavailableException;->mStatus:I

    iput p3, p0, Lcom/google/android/apps/plus/service/ResourceUnavailableException;->mHttpStatus:I

    return-void
.end method
