.class public Lcom/google/android/apps/plus/service/SkyjamPlaybackService;
.super Landroid/app/Service;
.source "SkyjamPlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
.implements Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;
    }
.end annotation


# static fields
.field private static sAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private static sActivityId:Ljava/lang/String;

.field private static sCurrentTime:I

.field private static sListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sMusicUrl:Ljava/lang/String;

.field private static sSongName:Ljava/lang/String;

.field private static sStatus:Ljava/lang/String;

.field private static sTotalPlayableTime:I


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

.field private final mUpdateTimeRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sListeners:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService$1;-><init>(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$100()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sTotalPlayableTime:I

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sCurrentTime:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sCurrentTime:I

    return p0
.end method

.method static synthetic access$302(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Ljava/lang/String;

    sput-object p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/SkyjamPlaybackService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getTimeString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->dispatchStatusUpdate()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method private dispatchStatusUpdate()V
    .locals 5

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    const/4 v1, 0x1

    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/apps/plus/service/SkyjamPlaybackService$2;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService$2;-><init>(Lcom/google/android/apps/plus/service/SkyjamPlaybackService;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getPlaybackStatus(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->skyjam_status_stopped:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTimeString(I)Ljava/lang/String;
    .locals 6
    .param p1    # I

    div-int/lit16 p1, p1, 0x3e8

    div-int/lit8 v0, p1, 0x3c

    rem-int/lit8 v1, p1, 0x3c

    sget v2, Lcom/google/android/apps/plus/R$string;->skyjam_time_formatting:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static isPlaying(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logOut(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "music_account"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "music_url"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "song"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sSongName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "activity_id"

    sget-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sActivityId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_0
    return-void
.end method

.method public static registerListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private stop()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    const-string v0, "SkyjamPlaybackService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SkyjamPlaybackService"

    const-string v1, "stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v2, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$string;->skyjam_status_stopped:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->dispatchStatusUpdate()V

    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sSongName:Ljava/lang/String;

    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sput-object v2, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sActivityId:Ljava/lang/String;

    sput v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sCurrentTime:I

    sput v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sTotalPlayableTime:I

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v1, 0x6ab0

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public static unregisterListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;

    sget-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I

    const-string v0, "SkyjamPlaybackService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SkyjamPlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "buffering: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    const-string v0, "SkyjamPlaybackService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SkyjamPlaybackService"

    const-string v1, "completion"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stop()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stopSelf()V

    return-void
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mNotificationManager:Landroid/app/NotificationManager;

    sget v0, Lcom/google/android/apps/plus/R$string;->skyjam_status_stopped:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mHandler:Landroid/os/Handler;

    const-string v2, "SkyjamServiceThread"

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/plus/service/ServiceThread;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceThread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceThread;->quit()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const-string v0, "SkyjamPlaybackService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SkyjamPlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error: what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", extra="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stop()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stopSelf()V

    const/4 v0, 0x1

    return v0
.end method

.method public final onOperationComplete(Lcom/google/android/apps/plus/network/HttpOperation;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v7, :cond_4

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "SkyjamPlaybackService"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "SkyjamPlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Received server response: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v5, Lcom/google/android/apps/plus/json/JsonReader;

    new-instance v7, Ljava/io/StringReader;

    invoke-direct {v7, v6}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v7}, Lcom/google/android/apps/plus/json/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->beginObject()V

    :goto_0
    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    const-string v7, "durationMillis"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextInt()I

    move-result v0

    goto :goto_0

    :cond_1
    const-string v7, "playType"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_2
    const-string v7, "url"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->skipValue()V

    goto :goto_0

    :catch_0
    move-exception v7

    :cond_4
    :goto_1
    return-void

    :cond_5
    invoke-virtual {v5}, Lcom/google/android/apps/plus/json/JsonReader;->endObject()V

    if-eqz v2, :cond_6

    const-string v7, "full"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    const-string v7, "sp"

    invoke-virtual {v2, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_6
    sput v0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sTotalPlayableTime:I

    :goto_2
    const-string v7, "SkyjamPlaybackService"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_7

    const-string v7, "SkyjamPlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Total playable time set to "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v9, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sTotalPlayableTime:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ms"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    :try_start_1
    const-string v7, "SkyjamPlaybackService"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "SkyjamPlaybackService"

    const-string v8, "play"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v7, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v7, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v7, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v7}, Landroid/media/MediaPlayer;->prepareAsync()V

    sget v7, Lcom/google/android/apps/plus/R$string;->skyjam_status_buffering:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v7

    sput-object v7, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->dispatchStatusUpdate()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v7

    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stop()V

    goto :goto_1

    :cond_9
    const/4 v7, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    mul-int/lit16 v7, v7, 0x3e8

    sput v7, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sTotalPlayableTime:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 11
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v10, 0x1

    const/4 v9, 0x0

    const-string v5, "SkyjamPlaybackService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "SkyjamPlaybackService"

    const-string v6, "prepared"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-ne p1, v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->start()V

    sget v5, Lcom/google/android/apps/plus/R$string;->skyjam_status_playing:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    sget v7, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sCurrentTime:I

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getTimeString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    sget v7, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sTotalPlayableTime:I

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getTimeString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->dispatchStatusUpdate()V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mUpdateTimeRunnable:Ljava/lang/Runnable;

    const-wide/16 v7, 0x3e8

    invoke-virtual {v5, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    sget v5, Lcom/google/android/apps/plus/R$string;->skyjam_notification_playing_song:I

    new-array v6, v10, [Ljava/lang/Object;

    sget-object v7, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sSongName:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->skyjam_notification_playing_song_title:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->skyjam_notification_playing_song_subtitle:I

    new-array v6, v10, [Ljava/lang/Object;

    sget-object v7, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sSongName:Ljava/lang/String;

    aput-object v7, v6, v9

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Landroid/app/Notification;

    sget v5, Lcom/google/android/apps/plus/R$mipmap;->icon:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v0, v5, v2, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    sget-object v5, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sActivityId:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x8000000

    invoke-static {p0, v9, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, p0, v4, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v5, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v0, Landroid/app/Notification;->flags:I

    iget-object v5, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mNotificationManager:Landroid/app/NotificationManager;

    const/16 v6, 0x6ab0

    invoke-virtual {v5, v6, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    :cond_1
    return-void
.end method

.method public final onServiceThreadEnd()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stop()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/ServiceThread;->put(Landroid/content/Intent;)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final processIntent(Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "com.google.android.apps.plus.service.SkyjamPlaybackService.PLAY"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "music_url"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stop()V

    :cond_2
    const-string v1, "music_account"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    sput-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "music_url"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    const-string v1, "song"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sSongName:Ljava/lang/String;

    const-string v1, "activity_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sActivityId:Ljava/lang/String;

    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->skyjam_status_connecting:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sStatus:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->dispatchStatusUpdate()V

    new-instance v0, Lcom/google/android/apps/plus/network/HttpOperation;

    const-string v2, "GET"

    sget-object v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/plus/network/DefaultHttpRequestConfiguration;

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v5, "sj"

    invoke-direct {v4, p0, v1, v5}, Lcom/google/android/apps/plus/network/DefaultHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    sget-object v5, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x800

    invoke-direct {v6, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const/4 v7, 0x0

    move-object v1, p0

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_0

    :cond_3
    const-string v1, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "music_url"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->sMusicUrl:Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stop()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->stopSelf()V

    goto/16 :goto_0
.end method
