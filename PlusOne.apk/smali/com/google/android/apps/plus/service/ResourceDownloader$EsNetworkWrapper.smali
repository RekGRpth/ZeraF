.class final Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;
.super Ljava/lang/Object;
.source "ResourceDownloader.java"

# interfaces
.implements Lcom/android/volley/Network;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/ResourceDownloader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EsNetworkWrapper"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mNetwork:Lcom/android/volley/Network;

.field private final mPool:Lcom/android/volley/toolbox/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/volley/Network;Lcom/android/volley/toolbox/ByteArrayPool;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/volley/Network;
    .param p3    # Lcom/android/volley/toolbox/ByteArrayPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mNetwork:Lcom/android/volley/Network;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    return-void
.end method

.method private tryContentUri(Landroid/net/Uri;)[B
    .locals 7
    .param p1    # Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v1, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    invoke-direct {v1, v5}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;-><init>(Lcom/android/volley/toolbox/ByteArrayPool;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    const/16 v6, 0x400

    invoke-virtual {v5, v6}, Lcom/android/volley/toolbox/ByteArrayPool;->getBuf(I)[B

    move-result-object v0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    :goto_0
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v2}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v5

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    invoke-virtual {v5, v0}, Lcom/android/volley/toolbox/ByteArrayPool;->returnBuf([B)V

    :try_start_3
    invoke-virtual {v1}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    :goto_1
    return-object v4

    :cond_0
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    invoke-virtual {v1}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;->toByteArray()[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    invoke-virtual {v5, v0}, Lcom/android/volley/toolbox/ByteArrayPool;->returnBuf([B)V

    :try_start_5
    invoke-virtual {v1}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    :catch_1
    move-exception v5

    goto :goto_1

    :catch_2
    move-exception v5

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    invoke-virtual {v5, v0}, Lcom/android/volley/toolbox/ByteArrayPool;->returnBuf([B)V

    :try_start_6
    invoke-virtual {v1}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    :catch_3
    move-exception v5

    goto :goto_1

    :catchall_1
    move-exception v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mPool:Lcom/android/volley/toolbox/ByteArrayPool;

    invoke-virtual {v5, v0}, Lcom/android/volley/toolbox/ByteArrayPool;->returnBuf([B)V

    :try_start_7
    invoke-virtual {v1}, Lcom/android/volley/toolbox/PoolingByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :goto_2
    throw v4

    :catch_4
    move-exception v5

    goto :goto_1

    :catch_5
    move-exception v5

    goto :goto_2
.end method


# virtual methods
.method public final performRequest(Lcom/android/volley/Request;)Lcom/android/volley/NetworkResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/Request",
            "<*>;)",
            "Lcom/android/volley/NetworkResponse;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/VolleyError;
        }
    .end annotation

    const/16 v6, 0x13

    invoke-static {v6}, Landroid/os/Process;->setThreadPriority(I)V

    :try_start_0
    instance-of v6, p1, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;

    if-eqz v6, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->getResource()Lcom/google/android/apps/plus/service/Resource;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->tryContentUri(Landroid/net/Uri;)[B

    move-result-object v1

    if-eqz v1, :cond_1

    array-length v6, v1

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Loaded using content URI: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " resource: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_0
    new-instance v6, Lcom/android/volley/NetworkResponse;

    invoke-direct {v6, v1}, Lcom/android/volley/NetworkResponse;-><init>([B)V

    :goto_0
    return-object v6

    :cond_1
    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Failed to load using content URI: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " resource: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Downloading using URL: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->getDownloadUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " resource: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Resource;->getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/service/ResourceDownloader$EsNetworkWrapper;->mNetwork:Lcom/android/volley/Network;

    invoke-interface {v6, p1}, Lcom/android/volley/Network;->performRequest(Lcom/android/volley/Request;)Lcom/android/volley/NetworkResponse;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    :catch_0
    move-exception v4

    new-instance v6, Lcom/google/android/apps/plus/service/VolleyOutOfMemoryError;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/VolleyOutOfMemoryError;-><init>(Ljava/lang/OutOfMemoryError;)V

    throw v6
.end method
