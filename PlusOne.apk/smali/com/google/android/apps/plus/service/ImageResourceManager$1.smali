.class final Lcom/google/android/apps/plus/service/ImageResourceManager$1;
.super Lvedroid/support/v4/util/LruCache;
.source "ImageResourceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/ImageResourceManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/util/LruCache",
        "<",
        "Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;",
        "Lcom/google/android/apps/plus/service/ImageResource;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/ImageResourceManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/ImageResourceManager;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageResourceManager$1;->this$0:Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-direct {p0, p2}, Lvedroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Z
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    check-cast p3, Lcom/google/android/apps/plus/service/ImageResource;

    if-eqz p1, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Evicted image from cache: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ImageResource;->recycle()V

    :cond_1
    return-void
.end method

.method protected final bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/apps/plus/service/ImageResource;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ImageResource;->getSizeInBytes()I

    move-result v0

    return v0
.end method
