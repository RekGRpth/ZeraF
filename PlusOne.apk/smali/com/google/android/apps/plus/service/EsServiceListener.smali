.class public Lcom/google/android/apps/plus/service/EsServiceListener;
.super Ljava/lang/Object;
.source "EsServiceListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountActivated$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onAccountAdded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onAccountUpgraded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onAddPeopleToCirclesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onChangeNotificationsRequestComplete$6a63df5(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCircleSyncComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreateCircleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreateComment$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreateEventComment$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreatePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onCreateProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeclineInvitationComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeleteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeleteCirclesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeleteComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeleteEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onDeletePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDeleteProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onDismissSuggestedPeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEditActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEditModerationStateComplete$1b131e9(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEditSquareMembershipComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEventHomeRequestComplete$b5e9bbb(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onEventInviteComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onEventManageGuestComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetActivities$35a362dd(IZILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetActivity$51e3eb1f(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetActivityAudience$6db92636(ILcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetAlbumComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetAlbumListComplete$6a63df5(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onGetAllPhotoTilesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetEventInviteesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetNotificationSettings$434dcfc8(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/NotificationSettingsData;

    return-void
.end method

.method public onGetPhoto$4894d499(IJ)V
    .locals 0
    .param p1    # I
    .param p2    # J

    return-void
.end method

.method public onGetPhotoSettings$6e3d3b8d(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    return-void
.end method

.method public onGetPhotosOfUserComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetSettings(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetSquareComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetSquaresComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onGetStreamPhotosComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onImageThumbnailUploaded$51902fbe(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public onLocationQuery$260d7f24(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onModerateComment$56b78e3(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onModifyCirclePropertiesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onMutateProfileComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onMuteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # J
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onOobRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onPhotosHomeComplete$6a63df5(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # Z
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onPostActivityResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onPromoSetCircleMembershipComplete$2f8b9cab(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onReadEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onReadSquareMembersComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onRemovePeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onRemoveReportAndBan$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onReportAbuseRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onReshareActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSaveLocationSharingSettings(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/io/File;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSearchActivitiesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSetBlockedRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSetCircleMembershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSetCoverPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSetMutedRequestComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSetScrapbookInfoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSetVolumeControlsRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSharePhotosToEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onSyncNotifications$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onTagSuggestionApprovalComplete$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onUpdateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onUploadCoverPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method

.method public onUploadProfilePhotoComplete(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    return-void
.end method
