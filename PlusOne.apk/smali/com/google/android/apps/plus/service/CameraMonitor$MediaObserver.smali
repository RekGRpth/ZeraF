.class final Lcom/google/android/apps/plus/service/CameraMonitor$MediaObserver;
.super Landroid/database/ContentObserver;
.source "CameraMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/CameraMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MediaObserver"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/CameraMonitor$MediaObserver;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 9
    .param p1    # Z

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/CameraMonitor$MediaObserver;->context:Landroid/content/Context;

    # getter for: Lcom/google/android/apps/plus/service/CameraMonitor;->sIntent:Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/apps/plus/service/CameraMonitor;->access$000()Landroid/content/Intent;

    move-result-object v5

    invoke-static {v4, v8, v5, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/service/CameraMonitor$MediaObserver;->context:Landroid/content/Context;

    const-string v5, "alarm"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x2ee0

    add-long v2, v4, v6

    invoke-virtual {v0, v8, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    return-void
.end method
