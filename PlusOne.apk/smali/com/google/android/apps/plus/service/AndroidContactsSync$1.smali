.class final Lcom/google/android/apps/plus/service/AndroidContactsSync$1;
.super Ljava/lang/Object;
.source "AndroidContactsSync.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/AndroidContactsSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 5
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    const/4 v0, -0x1

    check-cast p1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    check-cast p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-boolean v1, p1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v1, :cond_2

    iget-boolean v1, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v1, :cond_4

    :cond_2
    iget-wide v1, p1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    iget-wide v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    iget-object v1, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_3
    iget-wide v1, p1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    iget-wide v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method
