.class public final Lcom/google/android/apps/plus/service/ServiceResult;
.super Ljava/lang/Object;
.source "ServiceResult.java"


# instance fields
.field private final mErrorCode:I

.field private final mException:Ljava/lang/Exception;

.field private final mReasonPhrase:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v0, 0xc8

    const-string v1, "Ok"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mErrorCode:I

    iput-object p2, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mReasonPhrase:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mException:Ljava/lang/Exception;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/network/HttpOperation;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mErrorCode:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mReasonPhrase:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getException()Ljava/lang/Exception;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mException:Ljava/lang/Exception;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_0

    const/16 v0, 0xc8

    move v1, v0

    :goto_0
    if-eqz p1, :cond_1

    const-string v0, "Ok"

    :goto_1
    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    :cond_1
    const-string v0, "Error"

    goto :goto_1
.end method


# virtual methods
.method public final getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mErrorCode:I

    return v0
.end method

.method public final getException()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mException:Ljava/lang/Exception;

    return-object v0
.end method

.method public final getReasonPhrase()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mReasonPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public final hasError()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mErrorCode:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const-string v0, "ServiceResult(errorCode=%d, reasonPhrase=%s, exception=%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mErrorCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mReasonPhrase:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ServiceResult;->mException:Ljava/lang/Exception;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
