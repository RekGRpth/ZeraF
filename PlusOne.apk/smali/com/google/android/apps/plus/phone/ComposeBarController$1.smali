.class final Lcom/google/android/apps/plus/phone/ComposeBarController$1;
.super Ljava/lang/Object;
.source "ComposeBarController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/ComposeBarController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/ComposeBarController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1    # Landroid/view/animation/Animation;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    # getter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$000(Lcom/google/android/apps/plus/phone/ComposeBarController;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    const/4 v1, 0x2

    # setter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$002(Lcom/google/android/apps/plus/phone/ComposeBarController;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$102(Lcom/google/android/apps/plus/phone/ComposeBarController;F)F

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    # setter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$002(Lcom/google/android/apps/plus/phone/ComposeBarController;I)I

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    # getter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$200(Lcom/google/android/apps/plus/phone/ComposeBarController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    # getter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$300(Lcom/google/android/apps/plus/phone/ComposeBarController;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_1
    # setter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$102(Lcom/google/android/apps/plus/phone/ComposeBarController;F)F

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$400(Lcom/google/android/apps/plus/phone/ComposeBarController;Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;->this$0:Lcom/google/android/apps/plus/phone/ComposeBarController;

    # getter for: Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->access$300(Lcom/google/android/apps/plus/phone/ComposeBarController;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method
