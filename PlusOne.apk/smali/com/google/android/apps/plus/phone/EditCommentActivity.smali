.class public Lcom/google/android/apps/plus/phone/EditCommentActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EditCommentActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field protected mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final invalidateMenu()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$menu;->edit_comment_menu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->createTitlebarButtons(I)V

    return-void
.end method

.method public final onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Lvedroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->onDiscard()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->edit_comment_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->setContentView(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->showTitlebar(Z)V

    sget v0, Lcom/google/android/apps/plus/R$string;->edit_comment:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->setTitlebarTitle(Ljava/lang/String;)V

    sget v0, Lcom/google/android/apps/plus/R$menu;->edit_comment_menu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->createTitlebarButtons(I)V

    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v3, 0x0

    sparse-switch p1, :sswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->edit_comment_cancel_prompt:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :sswitch_1
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x32c15 -> :sswitch_1
        0xdc072 -> :sswitch_0
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->edit_comment_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x102002c

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->onDiscard()V

    :goto_0
    return v1

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->menu_post:I

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->onPost()V

    goto :goto_0

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->menu_discard:I

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->onDiscard()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    sget v1, Lcom/google/android/apps/plus/R$id;->menu_post:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v1, 0x1

    return v1
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$id;->menu_post:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditCommentActivity;->mEditCommentFragment:Lcom/google/android/apps/plus/fragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->onDiscard()V

    :cond_0
    return-void
.end method
