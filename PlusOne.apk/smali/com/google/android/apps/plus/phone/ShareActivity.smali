.class public Lcom/google/android/apps/plus/phone/ShareActivity;
.super Lcom/google/android/apps/plus/phone/PostActivity;
.source "ShareActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;,
        Lcom/google/android/apps/plus/phone/ShareActivity$DialogListener;
    }
.end annotation


# instance fields
.field private mDialogListener:Landroid/content/DialogInterface$OnClickListener;

.field private mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

.field private mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/ShareActivity$DialogListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity$DialogListener;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity$LocationDialogListener;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method private copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {p3, p2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method private isThirdPartyPackageSecure()Z
    .locals 6

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    const-string v5, "from_signup"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v5, "calling_package"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected final getPostFragmentArguments()Landroid/os/Bundle;
    .locals 26

    invoke-super/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getPostFragmentArguments()Landroid/os/Bundle;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return-object v5

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v20, "com.google.android.apps.plus.CID"

    const-string v21, "cid"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.LOCATION_NAME"

    const-string v21, "location_name"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.EXTERNAL_ID"

    const-string v21, "external_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.FOOTER"

    const-string v21, "footer"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.LATITUDE"

    const-string v21, "latitude"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.LONGITUDE"

    const-string v21, "longitude"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.ADDRESS"

    const-string v21, "address"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    const-string v21, "content_deep_link_id"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    const-string v20, "com.google.android.apps.plus.CONTENT_DEEP_LINK_METADATA"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v10

    if-eqz v10, :cond_1

    const-string v20, "content_deep_link_metadata"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0, v10}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_1
    const-string v20, "com.google.android.apps.plus.IS_FROM_PLUSONE"

    const-string v21, "is_from_plusone"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    const-string v20, "android.intent.extra.TEXT"

    const-string v21, "android.intent.extra.TEXT"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v13}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_3

    const-string v20, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-virtual {v13}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    const-string v20, "url"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v13}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v20, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    const-string v20, "com.google.android.apps.plus.RECIPIENT_ID"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_4

    const-string v20, "com.google.android.apps.plus.RECIPIENT_NAME"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_4

    const-string v20, "com.google.android.apps.plus.BIRTHDAY_YEAR"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    if-nez v20, :cond_5

    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_5
    new-instance v6, Lcom/google/android/apps/plus/api/BirthdayData;

    const-string v20, "com.google.android.apps.plus.RECIPIENT_ID"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v21, "com.google.android.apps.plus.RECIPIENT_NAME"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    const-string v22, "com.google.android.apps.plus.BIRTHDAY_YEAR"

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v22

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/plus/api/BirthdayData;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    const-string v20, "birthday_data"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v20, "audience"

    new-instance v21, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v22, Lcom/google/android/apps/plus/content/PersonData;

    const-string v23, "com.google.android.apps.plus.RECIPIENT_ID"

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.google.android.apps.plus.RECIPIENT_NAME"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    invoke-direct/range {v22 .. v25}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct/range {v21 .. v22}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_6
    const-string v20, "com.google.android.apps.plus.GOOGLE_PLUS_SHARE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_7

    const-string v20, "android.intent.action.SEND"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    :cond_7
    const-string v20, "com.google.android.apps.plus.CONTENT_URL"

    const-string v21, "url"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/apps/plus/phone/ShareActivity;->copyStringExtraToArgs(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    if-eqz v20, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v20

    if-eqz v20, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v20

    const-string v21, "com.google.android.apps.plus.SENDER_ID"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_c

    const-string v20, "com.google.android.apps.plus.RECIPIENT_IDS"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    const-string v20, "com.google.android.apps.plus.RECIPIENT_DISPLAY_NAMES"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v16

    if-eqz v17, :cond_9

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    :goto_1
    if-eqz v16, :cond_c

    if-eqz v18, :cond_c

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    new-instance v15, Ljava/util/ArrayList;

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    invoke-direct {v15, v0}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v20, Lcom/google/android/apps/plus/util/Property;->ASPEN_MAX_INDIVIDUAL_ACLS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/util/Property;->getAsLong()Ljava/lang/Long;

    move-result-object v14

    const/4 v12, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v12, v0, :cond_a

    if-eqz v14, :cond_8

    int-to-long v0, v12

    move-wide/from16 v20, v0

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    cmp-long v20, v20, v22

    if-gez v20, :cond_a

    :cond_8
    new-instance v22, Lcom/google/android/apps/plus/content/PersonData;

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_9
    const/16 v18, 0x0

    goto :goto_1

    :cond_a
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_b

    const-string v20, "ShareActivity"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Only "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " recipients can be prepopulated in a post.  Some recipients were removed."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    const-string v20, "audience"

    new-instance v21, Lcom/google/android/apps/plus/content/AudienceData;

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v0, v15, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_c
    const-string v20, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    if-eqz v20, :cond_10

    const-string v20, "com.google.android.apps.plus.CONTENT_URL"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v20, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_d

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_d

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->isThirdPartyPackageSecure()Z

    move-result v20

    if-nez v20, :cond_e

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_e
    const-string v20, "com.google.android.apps.plus.CALL_TO_ACTION"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/api/CallToActionData;->fromExtras(Landroid/os/Bundle;)Lcom/google/android/apps/plus/api/CallToActionData;

    move-result-object v8

    if-nez v8, :cond_f

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_f
    const-string v20, "call_to_action"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v20, "com.google.android.apps.plus.SENDER_ID"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_10

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_10
    const-string v20, "api_info"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_0
.end method

.method protected final getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->post_share_button_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final getViewId()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->share_activity:I

    return v0
.end method

.method public final invalidateMenu()V
    .locals 2

    sget v0, Lcom/google/android/apps/plus/R$menu;->share_menu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->createTitlebarButtons(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->invalidateOptionsMenu()V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x0

    if-nez p1, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.SHARE_GOOGLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "com.google.android.apps.plus.API_KEY"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "com.google.android.apps.plus.CLIENT_ID"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "com.google.android.apps.plus.VERSION"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    const-string v0, "calling_package"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->isThirdPartyPackageSecure()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    :goto_0
    iput-object v7, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v0}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/ShareActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->finish()V

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/PostActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->showTitlebar(Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->frame_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/phone/ShareActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/ShareActivity$1;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->post_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/phone/ShareActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/ShareActivity$2;-><init>(Lcom/google/android/apps/plus/phone/ShareActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->setTitlebarTitle(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v13}, Lcom/google/android/apps/plus/phone/ShareActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0, v3}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "com.android.vending"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.google.android.music"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    const-string v8, "659910861946.apps.googleusercontent.com"

    :cond_5
    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v3, v9}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v10

    move-object v9, v3

    move-object v11, v5

    move-object v12, v0

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    move-object v7, v6

    goto/16 :goto_0

    :cond_6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_0

    :cond_7
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v8

    const-string v0, "android.support.v4.app.EXTRA_CALLING_PACKAGE"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v0, "calling_package"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v9, "com.google.android.apps.social"

    :goto_3
    const-string v0, "com.google.android.apps.plus.VERSION"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v6, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v9, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v10

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    move-object v1, v7

    move-object v2, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    move-object v7, v0

    goto/16 :goto_0

    :cond_a
    const-string v0, "ShareActivity.mInfo"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    goto/16 :goto_1

    :cond_b
    sget v0, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->setTitlebarTitle(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_c
    move-object v9, v0

    goto :goto_3
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v6, 0x1

    const/4 v1, 0x0

    const v5, 0x104000a

    const/4 v4, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return-object v1

    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->share_incorrect_account:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->share_connection_error:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :sswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->share_preview_error:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :sswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->share_preview_post_error:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v2, v5, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :sswitch_4
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ShareActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :sswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->location_provider_disabled:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->yes:I

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->no:I

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    :sswitch_6
    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v1

    goto/16 :goto_0

    :sswitch_7
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->post_location_dialog_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->sharebox_location_dialog_message:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mLocationDialogListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v5, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x31db -> :sswitch_1
        0x409e -> :sswitch_4
        0x5339 -> :sswitch_0
        0x58a1 -> :sswitch_3
        0x6e27 -> :sswitch_2
        0x1bfb7a8 -> :sswitch_5
        0x1d71d84 -> :sswitch_7
        0x7f0a003e -> :sswitch_6
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->share_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x102002c

    if-eq v0, v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->menu_discard:I

    if-ne v0, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->onDiscard(Z)V

    :goto_0
    return v1

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->menu_post:I

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->post()Z

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$id;->menu_post:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v1, 0x1

    return v1
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$id;->menu_post:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/PostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    if-eqz v0, :cond_1

    const-string v0, "ShareActivity.mInfo"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_1
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->finish()V

    return-void
.end method

.method protected final recordLaunchEvent()V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ShareActivity;->isFromThirdPartyApp(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "extra_platform_event"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v3, "com.google.android.apps.plus.GOOGLE_INTERACTIVE_POST"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v2}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v1, "com.google.android.apps.plus.CONTENT_DEEP_LINK_ID"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    :cond_1
    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ShareActivity;->mInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_OPEN_SHAREBOX:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    move v2, v1

    goto :goto_1
.end method

.method protected final showTitlebar(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const/4 v4, 0x0

    invoke-super {p0, p1, v4}, Lcom/google/android/apps/plus/phone/PostActivity;->showTitlebar(ZZ)V

    sget v2, Lcom/google/android/apps/plus/R$id;->title_layout:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ShareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->share_title_padding_left:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method
