.class public Lcom/google/android/apps/plus/phone/EditSquareAudienceActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "EditSquareAudienceActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;-><init>()V

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->host_dialog_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditSquareAudienceActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditSquareAudienceActivity;->finish()V

    :cond_0
    return-void
.end method
