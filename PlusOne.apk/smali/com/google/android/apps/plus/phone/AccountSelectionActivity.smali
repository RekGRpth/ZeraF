.class public Lcom/google/android/apps/plus/phone/AccountSelectionActivity;
.super Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.source "AccountSelectionActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "intent"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-static {p0, p2, v1, p1, p3}, Lcom/google/android/apps/plus/phone/Intents;->getHomeOobActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->isGooglePlayServicesAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/AccountSelectionActivity;->showAccountSelectionOrUpgradeAccount(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
