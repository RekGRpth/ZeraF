.class public Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PanoramaViewerActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PanoramaViewerActivity$GmsErrorDialogFragment;
    }
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

.field private mResource:Lcom/google/android/apps/plus/service/Resource;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private hideProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    return-void
.end method

.method private loadPanoramaInfo()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->hideProgressDialog()V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v2}, Lcom/google/android/gms/panorama/PanoramaClient;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ImageUtils;->isFileUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move-object v1, v0

    :cond_3
    if-nez v1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Resource;->getCacheFileName()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsProvider;->buildPanoramaUri(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v2, p0, v1}, Lcom/google/android/gms/panorama/PanoramaClient;->loadPanoramaInfoAndGrantAccess(Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->hideProgressDialog()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->showFailureMessage()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private showFailureMessage()V
    .locals 2

    sget v0, Lcom/google/android/apps/plus/R$string;->toast_panorama_viewer_failure:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v2, 0x1

    const/16 v3, 0x22

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    return-void
.end method

.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onConnected()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->loadPanoramaInfo()V

    return-void
.end method

.method public final onConnectionFailed$5d4cef71()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->showFailureMessage()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->clearCache()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "GMS_error"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity$GmsErrorDialogFragment;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity$GmsErrorDialogFragment;-><init>(IB)V

    const-string v3, "GMS_error"

    invoke-virtual {v2, v1, v3}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-direct {v1, p0, p0, p0}, Lcom/google/android/gms/panorama/PanoramaClient;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v1}, Lcom/google/android/gms/panorama/PanoramaClient;->connect()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "mediaref"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->bindResources()V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity$1;-><init>(Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/panorama/PanoramaClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mPanoramaClient:Lcom/google/android/gms/panorama/PanoramaClient;

    :cond_0
    return-void
.end method

.method public final onDisconnected()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    return-void
.end method

.method public final onPanoramaInfoLoaded$680664b4(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->finish()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->showFailureMessage()V

    goto :goto_0
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->loadPanoramaInfo()V

    return-void
.end method

.method protected final showProgressDialog()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/R$string;->loading_panorama:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    const-string v2, "progress"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PanoramaViewerActivity;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    :cond_0
    return-void
.end method
