.class public final Lcom/google/android/apps/plus/phone/CameraPhotoLoader;
.super Lcom/google/android/apps/plus/phone/CameraAlbumLoader;
.source "CameraPhotoLoader.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method


# virtual methods
.method protected final buildMatrixCursor(Landroid/content/Context;[Landroid/database/Cursor;[Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 18
    .param p1    # Landroid/content/Context;
    .param p2    # [Landroid/database/Cursor;
    .param p3    # [Landroid/net/Uri;

    new-instance v9, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v15, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v9, v15}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    const-wide/16 v10, -0x1

    const/4 v3, -0x1

    const/4 v6, 0x0

    :goto_1
    move-object/from16 v0, p2

    array-length v15, v0

    if-ge v6, v15, :cond_2

    aget-object v1, p2, v6

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v15

    if-nez v15, :cond_0

    const/4 v15, 0x1

    invoke-interface {v1, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-eqz v15, :cond_1

    const-wide/16 v4, 0x0

    :goto_2
    cmp-long v15, v4, v10

    if-lez v15, :cond_0

    move-wide v10, v4

    move v3, v6

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    const/4 v15, 0x1

    invoke-interface {v1, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    goto :goto_2

    :cond_2
    const/4 v15, -0x1

    if-eq v3, v15, :cond_3

    aget-object v2, p2, v3

    const/4 v15, 0x0

    :try_start_0
    invoke-interface {v2, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v15, 0x2

    invoke-interface {v2, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    aget-object v15, p3, v3

    invoke-static {v15, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoDataBytes(Landroid/content/Context;Landroid/net/Uri;)[B

    move-result-object v14

    invoke-virtual {v9}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    const-wide/16 v16, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :catchall_0
    move-exception v15

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    throw v15

    :cond_3
    return-object v9
.end method
