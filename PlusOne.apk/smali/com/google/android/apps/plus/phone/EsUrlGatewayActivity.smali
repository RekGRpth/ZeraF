.class public abstract Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EsUrlGatewayActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;
    }
.end annotation


# static fields
.field private static sKnownUnsupportedUri:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mActivityId:Ljava/lang/String;

.field protected mAlbumId:Ljava/lang/String;

.field protected mAuthKey:Ljava/lang/String;

.field protected mDesktopActivityId:Ljava/lang/String;

.field private mEventCreatorId:Ljava/lang/String;

.field private mEventId:Ljava/lang/String;

.field private mEventInvitationToken:Ljava/lang/String;

.field protected mGaiaId:Ljava/lang/String;

.field protected mHangoutDomain:Ljava/lang/String;

.field protected mHangoutId:Ljava/lang/String;

.field protected mHangoutServiceId:Ljava/lang/String;

.field protected mName:Ljava/lang/String;

.field protected mPhotoId:J

.field protected mProfileId:Ljava/lang/String;

.field protected mProfileIdValidated:Z

.field protected mRequestType:I

.field private mRsvpType:Ljava/lang/String;

.field private mSquareId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    return-void
.end method

.method private static parseLong(Ljava/lang/String;)J
    .locals 2
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private parseUri(Landroid/net/Uri;)V
    .locals 12
    .param p1    # Landroid/net/Uri;

    :goto_0
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v5

    const-string v7, "authkey"

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x2

    if-lt v7, v8, :cond_2

    const-string v7, "_"

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "notifications"

    const/4 v8, 0x1

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x3

    if-lt v8, v9, :cond_4

    const-string v8, "emlink"

    const/4 v9, 0x2

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "ngemlink"

    const/4 v9, 0x2

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_0
    const-string v7, "path"

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "/"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "http://plus.google.com"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto/16 :goto_0

    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x2

    if-lt v6, v7, :cond_3

    const-string v7, "u"

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x2

    invoke-interface {v5, v7, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    add-int/lit8 v6, v6, -0x2

    :cond_3
    if-lez v6, :cond_11

    const-string v7, "photos"

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_11

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_5

    const/4 v7, 0x3

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    :cond_4
    :goto_1
    return-void

    :cond_5
    const/4 v8, 0x2

    if-ne v7, v8, :cond_7

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "fromphone"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    const-string v8, "instantupload"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_6
    const/16 v7, 0xb

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto :goto_1

    :cond_7
    const/4 v8, 0x3

    if-ne v7, v8, :cond_a

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/4 v8, 0x2

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v9, "of"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    const/16 v7, 0xc

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string v9, "posts"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    const/16 v8, 0xd

    iput v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto :goto_1

    :cond_9
    const-string v9, "albums"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v8, 0x7

    iput v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto :goto_1

    :cond_a
    const/4 v8, 0x4

    if-ne v7, v8, :cond_e

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/4 v8, 0x2

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x3

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v10, "albums"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_b

    const-string v10, "album"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_b
    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    const-string v7, "profile"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    const/16 v7, 0x10

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_c
    const-string v7, "posts"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const/16 v7, 0xd

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_d
    iput-object v9, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v7, :cond_4

    const/16 v7, 0xe

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_e
    const/4 v8, 0x5

    if-ne v7, v8, :cond_4

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/4 v8, 0x2

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x3

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const/4 v10, 0x4

    invoke-interface {v5, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v11, "albums"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    const-string v7, "profile"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    invoke-static {v10}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    iget-wide v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_4

    const/16 v7, 0x11

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_f
    const-string v7, "posts"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    invoke-static {v10}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    iget-wide v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_4

    const/16 v7, 0x12

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_10
    iput-object v9, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v7, :cond_4

    iget-wide v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-wide/16 v9, 0x0

    cmp-long v7, v7, v9

    if-eqz v7, :cond_4

    const/16 v7, 0xf

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_11
    if-lez v6, :cond_16

    const-string v7, "hangouts"

    const/4 v8, 0x0

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x2

    if-lt v7, v8, :cond_4

    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8, v5}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v9, "hangouts"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v9, "_"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_12
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_13

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v7, 0x14

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_13
    const/4 v9, 0x2

    if-ne v7, v9, :cond_4

    const/4 v7, 0x0

    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/4 v9, 0x1

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v9, "google.com"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    sget-boolean v9, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v9, :cond_14

    const-string v7, "google.com"

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    invoke-static {v8}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v7, 0x16

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_14
    const-string v9, "lite"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_15

    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v7, 0x17

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_15
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    iput-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v7, 0x15

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_16
    if-nez v6, :cond_17

    const/4 v7, 0x1

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_17
    const/4 v7, 0x0

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v7, "settings"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "+"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_18

    const/16 v7, 0x1f

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_18
    const/4 v7, 0x1

    if-ne v6, v7, :cond_21

    const-string v7, "stream"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_19

    const/4 v7, 0x2

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_19
    const-string v7, "me"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1a

    const/4 v7, 0x4

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_1a
    const-string v7, "circles"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1b

    const/16 v7, 0x8

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_1b
    const-string v7, "hot"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1c

    const-string v7, "explore"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1d

    :cond_1c
    const/16 v7, 0x19

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_1d
    const-string v7, "events"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1e

    const/16 v7, 0x1c

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_1e
    const-string v7, "communities"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1f

    sget-object v7, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0x1d

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_1f
    const-string v7, "+"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_20

    const/4 v7, 0x1

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_20
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    const/16 v7, 0x13

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_21
    const/4 v7, 0x2

    if-ne v6, v7, :cond_29

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v7, "posts"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_22

    const-string v7, "stream"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_23

    :cond_22
    const/4 v7, 0x5

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_23
    const-string v7, "about"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_24

    const/4 v7, 0x6

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_24
    const-string v7, "photos"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_25

    const/4 v7, 0x7

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_25
    const-string v7, "circles"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_26

    const-string v7, "people"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_27

    :cond_26
    const-string v7, "find"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_27

    const/16 v7, 0x1a

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_1

    :cond_27
    const-string v7, "events"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_28

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {p0, v2, v7, v8, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->processEventUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_1

    :cond_28
    const-string v7, "communities"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    sget-object v7, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v7

    if-eqz v7, :cond_4

    const/16 v7, 0x1e

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mSquareId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_29
    const/4 v7, 0x3

    if-ne v6, v7, :cond_2d

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v7, "posts"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2a

    const/16 v7, 0x9

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mDesktopActivityId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_2a
    const-string v7, "digest"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2b

    const/4 v7, 0x5

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_2b
    const-string v7, "notifications"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2c

    const-string v7, "all"

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2c

    const-string v7, "mute"

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    const/16 v7, 0x18

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    goto/16 :goto_1

    :cond_2c
    const-string v7, "events"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x0

    invoke-direct {p0, v2, v3, v7, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->processEventUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_1

    :cond_2d
    const/4 v7, 0x5

    if-ne v6, v7, :cond_4

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v7, 0x3

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    const/4 v7, 0x4

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v7, "events"

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "rsvp"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x0

    invoke-direct {p0, v2, v7, v4, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->processEventUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_1
.end method

.method private processEventUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/net/Uri;

    const/16 v0, 0x1b

    iput v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRsvpType:Ljava/lang/String;

    const-string v0, "gpinv"

    invoke-virtual {p4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    return-void
.end method

.method private redirect(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    const/high16 v0, 0x2010000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v0, "from_url_gateway"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->overridePendingTransition(II)V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isReadyToRedirect()Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileIdValidated:Z

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final launchBrowser(Landroid/net/Uri;)V
    .locals 10
    .param p1    # Landroid/net/Uri;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v7, :cond_1

    sget-object v7, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashSet;

    if-nez v6, :cond_0

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    sget-object v7, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v6, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v7, 0x80000

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v7, 0x10000

    invoke-virtual {v3, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_2

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v0, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    new-instance v7, Landroid/content/ComponentName;

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v9, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    return-void

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz p1, :cond_1

    const-string v2, "request_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    const-string v2, "profile_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    const-string v2, "profile_id_validated"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileIdValidated:Z

    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    const-string v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    const-string v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mDesktopActivityId:Ljava/lang/String;

    const-string v2, "album_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    const-string v2, "photo_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-string v2, "hangout_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const-string v2, "hangout_domain"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const-string v2, "service-id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    const-string v2, "event_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    const-string v2, "event_creator_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    const-string v2, "event_invitation_token"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    const-string v2, "square_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mSquareId:Ljava/lang/String;

    :goto_0
    iget v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    const/16 v3, 0x1f

    if-eq v2, v3, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_0

    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v2, "customAppUri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "customAppUri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_2
    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseUri(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "request_type"

    iget v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "profile_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "profile_id_validated"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileIdValidated:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "album_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "photo_id"

    iget-wide v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "hangout_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "hangout_domain"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "service-id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_creator_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_invitation_token"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "square_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mSquareId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected final redirect()V
    .locals 12

    iget v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    :goto_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getCirclesActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getSuggestedPeopleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getWhatsHotCircleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_8
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_9
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_b
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photos_home_instant_upload_label:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "camerasync"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_c
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photos_of_user_label:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_d
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_view_default_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "posts"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_e
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_f
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setExternal(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_10
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_11
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_12
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    const-string v2, "posts"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAuthkey(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_13
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_14
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_15
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_16
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->WITH_EXTRAS:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_17
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getMuteActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_18
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getEventsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_19
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRsvpType:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAuthKey:Ljava/lang/String;

    const/4 v8, 0x1

    move-object v1, p0

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_1a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getSquaresActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_1b
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mSquareId:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/phone/Intents;->getSquareStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_9
        :pswitch_9
        :pswitch_a
        :pswitch_2
        :pswitch_8
        :pswitch_0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_10
        :pswitch_f
        :pswitch_11
        :pswitch_12
        :pswitch_9
        :pswitch_13
        :pswitch_15
        :pswitch_16
        :pswitch_14
        :pswitch_17
        :pswitch_5
        :pswitch_3
        :pswitch_19
        :pswitch_18
        :pswitch_1a
        :pswitch_1b
    .end packed-switch
.end method

.method protected final redirectToBrowser()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    const-string v5, "http"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "content"

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "https"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    :cond_1
    iget v5, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    const/16 v6, 0x1f

    if-ne v5, v6, :cond_2

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->launchBrowser(Landroid/net/Uri;)V

    :goto_0
    return-void

    :cond_2
    sget-object v5, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    if-eqz v3, :cond_3

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->launchBrowser(Landroid/net/Uri;)V

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v5, "url"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "unsupported"

    invoke-virtual {v1, v5, v6}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
