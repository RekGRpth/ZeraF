.class public abstract Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;
.super Lvedroid/support/v4/view/PagerAdapter;
.source "EsFragmentPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;,
        Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;
    }
.end annotation


# instance fields
.field private mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

.field private mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

.field private mFragmentCache:Lvedroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Lvedroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

.field private mPagerListener:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;


# direct methods
.method public constructor <init>(Lvedroid/support/v4/app/FragmentManager;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/FragmentManager;

    const/4 v0, 0x0

    invoke-direct {p0}, Lvedroid/support/v4/view/PagerAdapter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    new-instance v0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;

    const/4 v1, 0x5

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;-><init>(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentCache:Lvedroid/support/v4/util/LruCache;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;)Lvedroid/support/v4/app/FragmentTransaction;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    return-object v0
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    move-object v0, p3

    check-cast v0, Lvedroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Lvedroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->makeFragmentName(II)Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v2, v1, v0}, Lvedroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v2, v0}, Lvedroid/support/v4/app/FragmentTransaction;->detach(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    return-void
.end method

.method public final finishUpdate$3c7ec8c3()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    return-void
.end method

.method public abstract getItem(I)Lvedroid/support/v4/app/Fragment;
.end method

.method public instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->makeFragmentName(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentCache:Lvedroid/support/v4/util/LruCache;

    invoke-virtual {v2, v1}, Lvedroid/support/v4/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v2, v1}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v2, v0}, Lvedroid/support/v4/app/FragmentTransaction;->attach(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    if-eq v0, v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lvedroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    :cond_1
    return-object v0

    :cond_2
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->getItem(I)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3, v0, v1}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method

.method public final isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lvedroid/support/v4/app/Fragment;

    invoke-virtual {p2}, Lvedroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    move-object v1, p1

    :goto_0
    instance-of v2, v1, Landroid/view/View;

    if-eqz v2, :cond_1

    if-ne v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected makeFragmentName(II)Ljava/lang/String;
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:switcher:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # Ljava/lang/ClassLoader;

    return-void
.end method

.method public final saveState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final setFragmentPagerListener(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mPagerListener:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;

    return-void
.end method

.method public final setPrimaryItem$7e55ba3e(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lvedroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/Fragment;->setMenuVisibility(Z)V

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mPagerListener:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mPagerListener:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;->onPageActivated(Lvedroid/support/v4/app/Fragment;)V

    :cond_3
    return-void
.end method
