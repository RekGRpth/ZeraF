.class public Lcom/google/android/apps/plus/phone/AddedToCircleActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "AddedToCircleActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$layout;->added_to_circle_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->setContentView(I)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notif_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->showTitlebar(ZZ)V

    sget v1, Lcom/google/android/apps/plus/R$string;->added_to_circle_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->setTitlebarTitle(Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
