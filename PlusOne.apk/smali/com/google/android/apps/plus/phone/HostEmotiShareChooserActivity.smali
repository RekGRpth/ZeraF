.class public Lcom/google/android/apps/plus/phone/HostEmotiShareChooserActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostEmotiShareChooserActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedEmotiShareChooserFragment;-><init>()V

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->host_emotishare_chooser_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onUpButtonClick()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostEmotiShareChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PICK"

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostEmotiShareChooserActivity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onUpButtonClick()V

    goto :goto_0
.end method
