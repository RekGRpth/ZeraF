.class public final Lcom/google/android/apps/plus/phone/RecentImagesLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "RecentImagesLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/RecentImagesLoader$RecentImagesQuery;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "camerasync"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;->setUri(Landroid/net/Uri;)V

    sget-object v1, Lcom/google/android/apps/plus/phone/RecentImagesLoader$RecentImagesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;->setProjection([Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;->setSelectionArgs([Ljava/lang/String;)V

    const-string v1, "timestamp DESC LIMIT 10"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;->setSortOrder(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->loadRecentImagesTimestamp(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long v6, v0, v2

    const-wide/32 v8, 0x5265c00

    cmp-long v6, v6, v8

    if-ltz v6, :cond_0

    const-wide/32 v6, 0x19bfcc00

    sub-long v6, v0, v6

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "timestamp > "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;->setSelection(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v6

    return-object v6

    :cond_0
    const-wide v4, 0x7fffffffffffffffL

    goto :goto_0
.end method
