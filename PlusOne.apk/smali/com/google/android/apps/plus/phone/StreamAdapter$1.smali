.class final Lcom/google/android/apps/plus/phone/StreamAdapter$1;
.super Ljava/lang/Object;
.source "StreamAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

.field final synthetic val$gridView:Lcom/google/android/apps/plus/views/StreamGridView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/StreamAdapter;Lcom/google/android/apps/plus/views/StreamGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->val$gridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Lcom/google/android/apps/plus/views/StreamGridView;II)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/ComposeBarController;->onScroll(Lcom/google/android/apps/plus/views/StreamGridView;II)V

    :cond_0
    return-void
.end method

.method public final onTouchModeChanged(Lcom/google/android/apps/plus/views/StreamGridView;I)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p2    # I

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-result-object v4

    invoke-virtual {v4, p1, p2}, Lcom/google/android/apps/plus/phone/ComposeBarController;->onTouchModeChanged(Lcom/google/android/apps/plus/views/StreamGridView;I)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeoplePromoCardViewGroups:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$100(Lcom/google/android/apps/plus/phone/StreamAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeoplePromoCardViewGroups:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$100(Lcom/google/android/apps/plus/phone/StreamAdapter;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->val$gridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/StreamGridView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getAppearedOnScreenViews(Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeopleSuggestionLogger:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;
    invoke-static {v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->access$200(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->recordAppearedOnScreenViewsForLogging(Ljava/util/ArrayList;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
