.class final Lcom/google/android/apps/plus/phone/LocationController$1;
.super Ljava/lang/Object;
.source "LocationController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/LocationController;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/LocationController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "LocationController"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LocationController"

    const-string v1, "----> locationAcquisitionTimer: timeout, with location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->areSameLocations(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mReverseGeo:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$800(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$900(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$702(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_0

    :cond_5
    const-string v0, "LocationController"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "LocationController"

    const-string v1, "----> locationAcquisitionTimer: timeout, without location"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$1200(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$1;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mTimeout:J
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$1100(Lcom/google/android/apps/plus/phone/LocationController;)J

    move-result-wide v1

    const-wide/16 v3, 0x2

    div-long/2addr v1, v3

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
