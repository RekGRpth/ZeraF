.class public Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;
.implements Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;
.implements Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$AlbumDetailsQuery;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;,
        Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/analytics/InstrumentedActivity;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;",
        "Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;",
        "Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;"
    }
.end annotation


# static fields
.field private static final EVENT_NAME_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

.field private mAlbumCount:I

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAuthkey:Ljava/lang/String;

.field private mCurrentIndex:I

.field private mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mDeleteMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mEventId:Ljava/lang/String;

.field private mFailedListener:Landroid/content/DialogInterface$OnClickListener;

.field private mFragmentIsLoading:Z

.field private mFullScreen:Z

.field private mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

.field private mIsEmpty:Z

.field private mIsPaused:Z

.field private mIsStreamPost:Z

.field private mKeyboardIsVisible:Z

.field private mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private mMenuItemListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;",
            ">;"
        }
    .end annotation
.end field

.field private mOperationType:I

.field private mOwnerGaiaId:Ljava/lang/String;

.field private mPageHint:I

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mPhotoOfUserGaiaId:Ljava/lang/String;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPhotoUrl:Ljava/lang/String;

.field private mPreviousOrientation:I

.field private mRestartLoader:Z

.field private mRootView:Landroid/view/View;

.field private mScreenListeners:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mServiceListener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;

.field private mStreamId:Ljava/lang/String;

.field private mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->EVENT_NAME_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mServiceListener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;

    iput v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOperationType:I

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPageHint:I

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFailedListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, -0x1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    :goto_1
    return v0

    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/views/PhotoViewPager;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mDeleteMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mDeleteMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->hideProgressDialog()V

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOperationType:I

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/apps/plus/R$string;->operation_failed:I

    :goto_1
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    goto :goto_0

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->remove_photo_error:I

    goto :goto_1

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOperationType:I

    packed-switch v0, :pswitch_data_1

    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->finish()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x10
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mKeyboardIsVisible:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/util/Set;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->EVENT_NAME_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    return v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRestartLoader:Z

    return v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public static addParentStack(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lvedroid/support/v4/app/TaskStackBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lvedroid/support/v4/app/TaskStackBuilder;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p2, v1}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    if-eqz p4, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p2, v0}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    :cond_0
    return-void

    :cond_1
    if-eqz p5, :cond_2

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_2
    if-nez p6, :cond_3

    if-eqz p7, :cond_3

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pending"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private updateTitleAndSubtitle()V
    .locals 9

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v5

    add-int/lit8 v1, v5, 0x1

    iget v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    if-ltz v5, :cond_2

    move v0, v3

    :goto_0
    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    if-nez v5, :cond_0

    if-eqz v0, :cond_0

    if-gtz v1, :cond_4

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    instance-of v3, v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    check-cast v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->setTitle(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/HostActionBar;->invalidateActionBar()V

    return-void

    :cond_2
    move v0, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->photo_view_default_title:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->photo_view_count:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    iget v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private updateView(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/16 v3, 0x8

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    if-nez v0, :cond_2

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    if-nez v0, :cond_3

    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->camera_photo_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->photo_activity_empty:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final addScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final deletePhoto(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v8, 0x0

    const/4 v11, 0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mDeleteMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v6}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v9

    if-eqz v9, :cond_1

    move-object v1, v6

    :goto_1
    if-nez v1, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$plurals;->delete_remote_photo_dialog_message:I

    :goto_2
    sget v8, Lcom/google/android/apps/plus/R$plurals;->delete_photo_dialog_title:I

    invoke-virtual {v7, v8, v11}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v2, v11}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$plurals;->delete_photo:I

    invoke-virtual {v7, v10, v11}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v10

    sget v11, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v9, v10, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "photo_id"

    invoke-virtual {v8, v9, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "owner_id"

    invoke-virtual {v8, v9, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v8

    const-string v9, "delete_photo"

    invoke-virtual {v0, v8, v9}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    move-object v6, v8

    goto :goto_0

    :cond_1
    move-object v1, v8

    goto :goto_1

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$plurals;->delete_local_photo_dialog_message:I

    goto :goto_2
.end method

.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getFullScreen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final isFragmentActive(Lvedroid/support/v4/app/Fragment;)Z
    .locals 3
    .param p1    # Lvedroid/support/v4/app/Fragment;

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onActionBarInvalidated()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    :cond_0
    return-void
.end method

.method public final onActionButtonClicked(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onActionButtonClicked(I)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->toggleFullScreen()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 20
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v15

    const-string v2, "account"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v19, 0x0

    const/4 v13, -0x1

    if-eqz p1, :cond_4

    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.ITEM"

    const/4 v3, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.FULLSCREEN"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.CURRENT_REF"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.PREVIOUS_ORIENTATION"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPreviousOrientation:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v0, v2, Landroid/content/res/Configuration;->orientation:I

    move/from16 v17, v0

    const/4 v2, 0x2

    move/from16 v0, v17

    if-ne v0, v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPreviousOrientation:I

    move/from16 v0, v17

    if-eq v0, v2, :cond_0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPreviousOrientation:I

    const-string v2, "event_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "event_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    :cond_1
    const-string v2, "album_name"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "album_name"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    :cond_2
    :goto_1
    const-string v2, "owner_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "owner_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    :cond_3
    const-string v2, "mediarefs"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "mediarefs"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v18

    move-object/from16 v0, v18

    array-length v2, v0

    new-array v2, v2, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v14, 0x0

    :goto_2
    move-object/from16 v0, v18

    array-length v2, v0

    if-ge v14, v2, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v2, v18, v14

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    aput-object v2, v3, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_4
    const-string v2, "notif_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_5
    const-string v2, "refresh_album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "refresh_album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    if-nez v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_view_default_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    goto :goto_1

    :cond_7
    const-string v2, "album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "album_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    :cond_8
    const-string v2, "stream_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "stream_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    :cond_9
    const-string v2, "photos_of_user_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "photos_of_user_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    :cond_a
    const-string v2, "photo_url"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "photo_url"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoUrl:Ljava/lang/String;

    :cond_b
    const-string v2, "photo_ref"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    if-gez v13, :cond_c

    const-string v2, "photo_ref"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_c
    const-string v2, "page_hint"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    if-gez v13, :cond_d

    const-string v2, "page_hint"

    const/4 v3, -0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPageHint:I

    :cond_d
    const-string v2, "photo_index"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    if-gez v13, :cond_e

    const-string v2, "photo_index"

    const/4 v3, -0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    :cond_e
    const-string v2, "auth_key"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "auth_key"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAuthkey:Ljava/lang/String;

    :cond_f
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I

    if-eqz v19, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAuthkey:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-static {v0, v2, v1, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    :cond_10
    const-string v2, "is_stream_post"

    const/4 v3, 0x0

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsStreamPost:Z

    sget v2, Lcom/google/android/apps/plus/R$layout;->photo_one_up_activity:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->setContentView(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->title_bar:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->photo_activity_root_view:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    const-string v2, "force_load_id"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    const-string v2, "force_load_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v15, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    :goto_3
    const-string v2, "allow_plusone"

    const/4 v3, 0x1

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    const-string v2, "disable_photo_comments"

    const/4 v3, 0x0

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    new-instance v2, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->setFragmentPagerListener(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$OnFragmentPagerListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->photo_view_pager:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setAdapter(Lvedroid/support/v4/view/PagerAdapter;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setOnInterceptTouchListener(Lcom/google/android/apps/plus/views/PhotoViewPager$OnInterceptTouchListener;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v2

    const v3, 0x7f0a0029

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$2;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_3
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v5, 0x0

    const-string v4, "tag"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->photo_view_pending_dialog:I

    if-ne p1, v4, :cond_0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v4, "dialog_message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    :goto_0
    return-object v1

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$id;->photo_view_download_full_failed_dialog:I

    if-ne p1, v4, :cond_1

    new-instance v2, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/String;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->download_photo_retry:I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {v4, v5, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$id;->photo_view_download_nonfull_failed_dialog:I

    if-ne p1, v4, :cond_2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->download_photo_error:I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFailedListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 14
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_2

    array-length v0, v1

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    const/4 v0, 0x0

    aget-object v0, v1, v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "content:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/android/apps/plus/phone/CameraPhotoLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/CameraPhotoLoader;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoUrl:Ljava/lang/String;

    iget v10, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPageHint:I

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAuthkey:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsStreamPost:Z

    if-eqz v1, :cond_4

    iget-object v12, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    :goto_2
    move-object v1, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_0

    :cond_4
    const/4 v12, 0x0

    goto :goto_2

    :sswitch_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v13, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$AlbumDetailsQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;

    invoke-direct {v0, p0, p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$3;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/content/Context;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x511d1df3 -> :sswitch_2
        0x7f0a0029 -> :sswitch_0
        0x7f0a002a -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->photo_view_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x1

    const-string v4, "photo_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string v4, "owner_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "delete_photo"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v4, v0, v3}, Lcom/google/android/apps/plus/service/EsService;->deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$plurals;->delete_photo_pending:I

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x10

    iput v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOperationType:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "pending"

    invoke-virtual {v4, v5, v6}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onFragmentVisible(Lvedroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getItemPosition(Ljava/lang/Object;)I

    move-result v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 7
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsEmpty:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFragmentIsLoading:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    if-ne v4, v5, :cond_4

    :goto_3
    if-eqz v3, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x511d1df3

    invoke-virtual {v0, v1, v6, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lvedroid/support/v4/content/Loader;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_7

    if-nez v3, :cond_6

    if-eqz v1, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f0a002a

    invoke-virtual {v0, v1, v6, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0

    :cond_7
    if-nez v3, :cond_0

    :cond_8
    :goto_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateTitleAndSubtitle()V

    goto :goto_0

    :sswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    if-nez v2, :cond_9

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    :cond_9
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    if-ne v0, v5, :cond_a

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_b

    const/4 v0, -0x2

    :goto_5
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumCount:I

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateTitleAndSubtitle()V

    goto/16 :goto_0

    :cond_b
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_5

    :sswitch_2
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    if-nez v0, :cond_8

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x511d1df3 -> :sswitch_2
        0x7f0a0029 -> :sswitch_0
        0x7f0a002a -> :sswitch_1
    .end sparse-switch
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;

    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onPageActivated(Lvedroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Lvedroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onViewActivated()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateTitleAndSubtitle()V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    goto :goto_1
.end method

.method public final onPageScrollStateChanged(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public final onPageScrolled$486775f1(IF)V
    .locals 0
    .param p1    # I
    .param p2    # F

    return-void
.end method

.method public final onPageSelected(I)V
    .locals 8
    .param p1    # I

    const/4 v0, 0x1

    const/4 v5, 0x0

    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->isDataValid()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    move-object v6, v1

    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v6, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    move-object v0, v5

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void

    :cond_1
    move-object v6, v5

    goto :goto_0

    :cond_2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x3

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x5

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_3

    move v6, v0

    :goto_2
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v6, :cond_4

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_3
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    move v6, v0

    goto :goto_2

    :cond_4
    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_3
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mServiceListener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onPause()V

    return-void
.end method

.method public final onPhotoRemoved$1349ef()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v3, 0x1

    if-gt v1, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const v4, 0x7f0a0029

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/app/Dialog;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->photo_view_pending_dialog:I

    if-ne p1, v1, :cond_0

    instance-of v1, p2, Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    move-object v0, p2

    check-cast v0, Landroid/app/ProgressDialog;

    const-string v1, "dialog_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1    # Landroid/view/Menu;

    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v4

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;

    invoke-interface {v3, p1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_1

    :cond_1
    const/4 v5, 0x1

    return v5
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public final onRefreshButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mServiceListener:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$ServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "PhotoOneUp"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PhotoOneUp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Activity finished because it is associated with a signed-out account: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRestartLoader:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRestartLoader:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f0a0029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->finish()V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.ITEM"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.FULLSCREEN"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.CURRENT_REF"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.PREVIOUS_ORIENTATION"

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPreviousOrientation:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final onTouchIntercept(FF)Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;
    .locals 5
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    if-nez v1, :cond_0

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onInterceptMoveLeft$2548a39()Z

    move-result v1

    :cond_0
    if-nez v2, :cond_1

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onInterceptMoveRight$2548a39()Z

    move-result v2

    :cond_1
    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onViewActivated()V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_4

    if-eqz v2, :cond_3

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->BOTH:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    :goto_1
    return-object v4

    :cond_3
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->LEFT:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_1

    :cond_4
    if-eqz v2, :cond_5

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->RIGHT:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_1

    :cond_5
    sget-object v4, Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;->NONE:Lcom/google/android/apps/plus/views/PhotoViewPager$InterceptType;

    goto :goto_1
.end method

.method public final onUpButtonClick()V
    .locals 9

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "from_url_gateway"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {p0}, Lvedroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mOwnerGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mEventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mStreamId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAlbumName:Ljava/lang/String;

    move-object v0, p0

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->addParentStack(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lvedroid/support/v4/app/TaskStackBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lvedroid/support/v4/app/TaskStackBuilder;->startActivities()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mMenuItemListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public final toggleFullScreen()V
    .locals 3

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mKeyboardIsVisible:Z

    if-eqz v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mScreenListeners:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mFullScreen:Z

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;->onFullScreenChanged$25decb5(Z)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
