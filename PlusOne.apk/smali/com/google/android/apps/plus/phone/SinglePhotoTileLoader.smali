.class public final Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;
.super Lvedroid/support/v4/content/AsyncTaskLoader;
.source "SinglePhotoTileLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Lcom/google/api/services/plusi/model/DataPhoto;",
        ">;"
    }
.end annotation


# static fields
.field private static final DATA_PHOTO_JSON:Lcom/google/api/services/plusi/model/DataPhotoJson;

.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mSelectionArgs:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->DATA_PHOTO_JSON:Lcom/google/api/services/plusi/model/DataPhotoJson;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, p1}, Lvedroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->mSelectionArgs:[Ljava/lang/String;

    return-void
.end method

.method private loadInBackground()Lcom/google/api/services/plusi/model/DataPhoto;
    .locals 10

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v8, 0x0

    :try_start_0
    const-string v1, "all_tiles"

    sget-object v2, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->PROJECTION:[Ljava/lang/String;

    const-string v3, "view_id = ? AND tile_id = ?"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->mSelectionArgs:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->DATA_PHOTO_JSON:Lcom/google/api/services/plusi/model/DataPhotoJson;

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/DataPhotoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataPhoto;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v1

    :cond_1
    move-object v1, v9

    goto :goto_0

    :catchall_0
    move-exception v1

    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method


# virtual methods
.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->loadInBackground()Lcom/google/api/services/plusi/model/DataPhoto;

    move-result-object v0

    return-object v0
.end method

.method protected final onStartLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;->forceLoad()V

    return-void
.end method
