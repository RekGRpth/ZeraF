.class public Lcom/google/android/apps/plus/phone/HostPeopleActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostPeopleActivity.java"


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPeopleActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0
.end method
