.class public abstract Lcom/google/android/apps/plus/phone/HostedFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "HostedFragment.java"


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mCalled:Z

.field private mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

.field private mEndViewExtras:Landroid/os/Bundle;

.field private mPaused:Z

.field private mRecorded:Z

.field private mStartTime:J

.field private mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

.field private mStartViewExtras:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    :cond_0
    return-void
.end method

.method public final clearNavigationAction()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartViewExtras:Landroid/os/Bundle;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartTime:J

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndViewExtras:Landroid/os/Bundle;

    return-void
.end method

.method public final detachActionBar()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    return-void
.end method

.method public abstract getAccount()Lcom/google/android/apps/plus/content/EsAccount;
.end method

.method public final getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    return-object v0
.end method

.method public getExtrasForLogging()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSafeContext()Landroid/content/Context;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public abstract getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
.end method

.method protected final invalidateActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->invalidateActionBar()V

    :cond_0
    return-void
.end method

.method protected final isPaused()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mPaused:Z

    return v0
.end method

.method protected needsAsyncData()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onActionButtonClicked(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method protected onAsyncData()V
    .locals 8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    iget-wide v4, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartViewExtras:Landroid/os/Bundle;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndViewExtras:Landroid/os/Bundle;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    :cond_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mCalled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mCalled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did you forget to call super.onSetArguments()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mPaused:Z

    return-void
.end method

.method protected onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    return-void
.end method

.method public onPrimarySpinnerSelectionChange(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mPaused:Z

    return-void
.end method

.method protected onSetArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mCalled:Z

    return-void
.end method

.method public onUpButtonClicked()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public recordNavigationAction()V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v5

    move-object v0, p0

    move-object v3, v1

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void
.end method

.method public final recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p3    # Ljava/lang/Long;
    .param p4    # Landroid/os/Bundle;
    .param p5    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->needsAsyncData()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mRecorded:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    iput-object v4, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartViewExtras:Landroid/os/Bundle;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mStartTime:J

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/HostedFragment;->mEndViewExtras:Landroid/os/Bundle;

    goto :goto_0
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v1, v0, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method protected final recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;
    .param p2    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v1, v0, p1, v2, p2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public refresh()V
    .locals 0

    return-void
.end method

.method public shouldPersistStateToHost()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
