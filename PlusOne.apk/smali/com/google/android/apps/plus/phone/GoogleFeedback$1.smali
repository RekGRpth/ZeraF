.class final Lcom/google/android/apps/plus/phone/GoogleFeedback$1;
.super Ljava/lang/Object;
.source "GoogleFeedback.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$parentActivity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;->val$parentActivity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 10
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    :try_start_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v5, 0x0

    :try_start_1
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;->val$parentActivity:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v5}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_1

    const/high16 v7, 0x400000

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->access$000(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v6

    :cond_1
    if-eqz v6, :cond_2

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/graphics/Bitmap;->writeToParcel(Landroid/os/Parcel;I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    if-eqz v5, :cond_3

    const/4 v7, 0x0

    :try_start_2
    invoke-virtual {v5, v7}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_3
    :goto_0
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface {p2, v7, v4, v8, v9}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;->val$parentActivity:Landroid/app/Activity;

    invoke-virtual {v7, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    :goto_1
    return-void

    :catch_0
    move-exception v7

    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    if-eqz v5, :cond_3

    const/4 v7, 0x0

    :try_start_3
    invoke-virtual {v5, v7}, Landroid/view/View;->setDrawingCacheEnabled(Z)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v3

    :try_start_4
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;->val$parentActivity:Landroid/app/Activity;

    invoke-virtual {v7, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_1

    :catch_2
    move-exception v7

    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    if-eqz v5, :cond_3

    const/4 v7, 0x0

    :try_start_5
    invoke-virtual {v5, v7}, Landroid/view/View;->setDrawingCacheEnabled(Z)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/GoogleFeedback$1;->val$parentActivity:Landroid/app/Activity;

    invoke-virtual {v8, p0}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    throw v7

    :catchall_1
    move-exception v7

    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    if-eqz v5, :cond_4

    const/4 v8, 0x0

    :try_start_6
    invoke-virtual {v5, v8}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    :cond_4
    throw v7
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;

    return-void
.end method
