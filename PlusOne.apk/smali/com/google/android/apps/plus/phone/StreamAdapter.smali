.class public Lcom/google/android/apps/plus/phone/StreamAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "StreamAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;,
        Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    }
.end annotation


# static fields
.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field protected static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActivityIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

.field private mApprovedGraySpam:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mCachedSpans:Landroid/util/SparseIntArray;

.field private mCircleSettingsClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

.field private mCircleSettingsSpan:I

.field private mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

.field private mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

.field private mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private mMarkPostsAsRead:Z

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPeoplePromoCardViewGroups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;",
            ">;"
        }
    .end annotation
.end field

.field private mPeopleSuggestionLogger:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

.field private mPromoCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

.field private mPromoCircleDeltaManager:Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

.field private mSeenWhatsHot:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamGridObservers:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field protected mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

.field private mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

.field private mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

.field private mStreamRestorePosition:I

.field private mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

.field private final mViewerHasReadPosts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mVisibleIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamRestorePosition:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamGridObservers:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mSeenWhatsHot:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mApprovedGraySpam:Ljava/util/HashSet;

    new-instance v0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

    invoke-direct {v0}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCircleDeltaManager:Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

    new-instance v0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeopleSuggestionLogger:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeoplePromoCardViewGroups:Ljava/util/ArrayList;

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->addPartition(ZZ)V

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput-object p6, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    iput-object p8, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    iput-object p11, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    iput-object p9, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    iput-object p10, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    iput-object p7, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

    iput-object p13, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mMarkPostsAsRead:Z

    sget-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    :cond_0
    iput-object p12, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    new-instance v0, Lcom/google/android/apps/plus/phone/StreamAdapter$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/StreamAdapter;Lcom/google/android/apps/plus/views/StreamGridView;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;)V

    new-instance v0, Lcom/google/android/apps/plus/phone/StreamAdapter$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/StreamAdapter$2;-><init>(Lcom/google/android/apps/plus/phone/StreamAdapter;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/phone/ComposeBarController;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/StreamAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/StreamAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/StreamAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeoplePromoCardViewGroups:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/StreamAdapter;)Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/StreamAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeopleSuggestionLogger:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    return-object v0
.end method


# virtual methods
.method public final addPromoPersonCircleDelta(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/CircleData;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCircleDeltaManager:Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

    invoke-virtual {v1, p2, p1, p3, p4}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->addPromoCircleDelta(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    move-result-object v0

    return-object v0
.end method

.method public bindStreamHeaderView$23b74339(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCircleSettingsClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->bind(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;)V

    return-void
.end method

.method public bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 22
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getPositionForPartition(I)I

    move-result v17

    add-int v13, v11, v17

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup;

    move/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v11, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v9

    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v9, v0, :cond_0

    check-cast p3, Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/views/StreamGridView;->getMaximumAcceptableSpan()I

    move-result v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11, v9}, Landroid/util/SparseIntArray;->put(II)V

    :cond_0
    move-object/from16 v14, p1

    check-cast v14, Lcom/google/android/apps/plus/views/StreamCardViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1, v9}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->init(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)Lcom/google/android/apps/plus/views/StreamCardViewGroup;

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object/from16 v15, p1

    check-cast v15, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setAvatarClickListener(Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setPlusBarClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setAppLinkClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setViewedListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setMediaClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setGraySpamBarClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mApprovedGraySpam:Ljava/util/HashSet;

    move-object/from16 v17, v0

    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setGraySpamApproved(Z)V

    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v17

    const/16 v18, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v18

    if-nez v18, :cond_1

    const/16 v18, 0x10

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v18

    if-eqz v18, :cond_1

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/plus/content/DbWhatsHot;->deserialize([B)Lcom/google/android/apps/plus/content/DbWhatsHot;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/content/DbWhatsHot;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_1

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/content/DbWhatsHot;->getViewed()Z

    move-result v18

    if-nez v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mSeenWhatsHot:Ljava/util/HashSet;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->recordWhatsHotImpression(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mSeenWhatsHot:Ljava/util/HashSet;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    new-instance v8, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    const/16 v17, -0x2

    move/from16 v0, v17

    invoke-direct {v8, v0}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(I)V

    invoke-virtual {v14}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->getSpan()I

    move-result v17

    move/from16 v0, v17

    iput v0, v8, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    invoke-virtual {v14, v8}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->postInit(Lcom/google/android/apps/plus/util/StreamLayoutInfo;)V

    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v13, v0, :cond_4

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    move-object/from16 v7, p1

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTranslationY()F

    move-result v17

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x3

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/view/View;->setTranslationY(F)V

    const/high16 v17, 0x41200000

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/view/View;->setRotationX(F)V

    invoke-virtual {v7}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v17

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewPropertyAnimator;->rotationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v17

    const-wide/16 v18, 0x1f4

    invoke-virtual/range {v17 .. v19}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v17

    sget-object v18, Lcom/google/android/apps/plus/phone/StreamAdapter;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v6

    new-instance v17, Lcom/google/android/apps/plus/phone/StreamAdapter$3;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/plus/phone/StreamAdapter$3;-><init>(Lcom/google/android/apps/plus/phone/StreamAdapter;Landroid/view/View;)V

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseWithLayer()Z

    move-result v17

    if-eqz v17, :cond_3

    invoke-virtual {v6}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_3
    const-wide/16 v17, 0x32

    move-wide/from16 v0, v17

    invoke-virtual {v6, v0, v1}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

    move-object/from16 v17, v0

    if-eqz v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v13}, Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;->onViewUsed(I)V

    :cond_5
    return-void

    :cond_6
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/apps/plus/views/PromoCardViewGroup;

    move/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v12, p1

    check-cast v12, Lcom/google/android/apps/plus/views/PromoCardViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->setPromoActionListener(Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/analytics/OzViews;->getExtrasForLogging(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v17

    if-nez v17, :cond_7

    new-instance v17, Landroid/os/Bundle;

    invoke-direct/range {v17 .. v17}, Landroid/os/Bundle;-><init>()V

    :cond_7
    const-string v18, "extra_promo_group_id"

    const-string v19, "1"

    invoke-virtual/range {v17 .. v19}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v18, "extra_promo_type"

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->getPromoTypeForLogging()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    sget-object v21, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_ITEM_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v18

    move-object/from16 v4, v17

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;

    move/from16 v17, v0

    if-eqz v17, :cond_1

    const/16 v17, 0x1

    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v10, p1

    check-cast v10, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCircleDeltaManager:Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->getPromoDeltas(Ljava/lang/String;)Ljava/util/List;

    move-result-object v17

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->applyPromoCircleDeltas(Ljava/util/List;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeoplePromoCardViewGroups:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    instance-of v0, p1, Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/service/ResourceConsumer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/service/ResourceConsumer;->unbindResources()V

    :cond_0
    packed-switch p2, :pswitch_data_0

    :goto_0
    instance-of v0, p1, Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/plus/service/ResourceConsumer;

    invoke-interface {p1}, Lcom/google/android/apps/plus/service/ResourceConsumer;->bindResources()V

    :cond_1
    return-void

    :pswitch_0
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamHeaderView$23b74339(Landroid/view/View;Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p3, p5}, Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final changeStreamCursor(Landroid/database/Cursor;I)V
    .locals 6
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-super {p0, v3, p1}, Lcom/android/common/widget/EsCompositeCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_0
    if-ltz v4, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 p2, -0x1

    move v1, v3

    :goto_1
    if-nez v1, :cond_2

    if-ltz p2, :cond_7

    :cond_2
    move v0, v3

    :goto_2
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->triggerStreamObservers(ZI)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mSeenWhatsHot:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mApprovedGraySpam:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCircleDeltaManager:Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->clear()V

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    move v1, v3

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2
.end method

.method public final changeStreamHeaderCursor(Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v3

    if-eq v3, p1, :cond_1

    move v0, v1

    :goto_0
    invoke-super {p0, v2, p1}, Lcom/android/common/widget/EsCompositeCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    if-eqz v0, :cond_0

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->triggerStreamObservers(ZI)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method protected final getItemViewType(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getStreamHeaderViewType(I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getStreamItemViewType(I)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getStreamHashActivityIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStreamHeaderViewType(I)I
    .locals 1
    .param p1    # I

    const/16 v0, 0xa

    return v0
.end method

.method public getStreamItemViewType(I)I
    .locals 13
    .param p1    # I

    const/4 v6, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const-wide/16 v11, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v4

    invoke-interface {v4, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/16 v9, 0x11

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->retrieveStreamItemType(Ljava/lang/String;)I

    move-result v5

    if-ne v5, v7, :cond_3

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->retrievePromoType(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v7, :cond_0

    if-eq v3, v8, :cond_0

    if-ne v3, v6, :cond_2

    :cond_0
    const/16 v6, 0xc

    :cond_1
    :goto_0
    return v6

    :cond_2
    const/16 v6, 0xb

    goto :goto_0

    :cond_3
    const-wide/16 v9, 0x100

    and-long/2addr v9, v1

    cmp-long v9, v9, v11

    if-eqz v9, :cond_4

    const/4 v6, 0x5

    goto :goto_0

    :cond_4
    const-wide/16 v9, 0x200

    and-long/2addr v9, v1

    cmp-long v9, v9, v11

    if-eqz v9, :cond_5

    const/4 v6, 0x4

    goto :goto_0

    :cond_5
    const-wide/16 v9, 0x400

    and-long/2addr v9, v1

    cmp-long v9, v9, v11

    if-nez v9, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v6, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v6

    if-eqz v6, :cond_6

    const-wide/32 v9, 0x40000

    and-long/2addr v9, v1

    cmp-long v6, v9, v11

    if-eqz v6, :cond_6

    const/16 v6, 0x8

    goto :goto_0

    :cond_6
    const-wide/16 v9, 0x1000

    and-long/2addr v9, v1

    cmp-long v6, v9, v11

    if-eqz v6, :cond_7

    const/4 v6, 0x6

    goto :goto_0

    :cond_7
    const-wide/16 v9, 0x804

    and-long/2addr v9, v1

    cmp-long v6, v9, v11

    if-eqz v6, :cond_8

    move v6, v7

    goto :goto_0

    :cond_8
    const-wide/32 v9, 0x30000

    and-long/2addr v9, v1

    cmp-long v6, v9, v11

    if-eqz v6, :cond_9

    const/4 v6, 0x7

    goto :goto_0

    :cond_9
    const-wide/16 v9, 0x40

    and-long/2addr v9, v1

    cmp-long v6, v9, v11

    if-eqz v6, :cond_a

    const/16 v6, 0x9

    goto :goto_0

    :cond_a
    const-wide/16 v9, 0xa0

    and-long/2addr v9, v1

    cmp-long v6, v9, v11

    if-eqz v6, :cond_b

    move v6, v8

    goto :goto_0

    :cond_b
    const-wide/16 v8, 0x2000

    and-long/2addr v8, v1

    cmp-long v6, v8, v11

    if-eqz v6, :cond_c

    move v6, v7

    goto :goto_0

    :cond_c
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final initCircleSettings(ILcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCircleSettingsSpan:I

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCircleSettingsClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    return-void

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount(I)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final logPeopleSuggestionEvents()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPeopleSuggestionLogger:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "ANDROID_STREAM_SUGGESTION_CARD"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-void
.end method

.method public newStreamHeaderView$4b8874c5(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;

    new-instance v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(I)V

    iget v2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCircleSettingsSpan:I

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1
.end method

.method public newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v6, 0x1

    const-wide/16 v7, 0x0

    const/16 v5, 0x11

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->retrieveStreamItemType(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v6, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPostsData;->retrievePromoType(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v6, :cond_0

    const/4 v5, 0x2

    if-eq v3, v5, :cond_0

    const/4 v5, 0x3

    if-ne v3, v5, :cond_1

    :cond_0
    new-instance v5, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v5

    :cond_1
    new-instance v5, Lcom/google/android/apps/plus/views/PromoCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_2
    const-wide/16 v5, 0x100

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_3

    new-instance v5, Lcom/google/android/apps/plus/views/EventCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/EventCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    const-wide/16 v5, 0x200

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_4

    new-instance v5, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/HangoutCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_4
    const-wide/16 v5, 0x400

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_5

    new-instance v5, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/SkyjamCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_5
    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v5, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v5

    if-eqz v5, :cond_6

    const-wide/32 v5, 0x40000

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_6

    new-instance v5, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_6
    const-wide/16 v5, 0x1000

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_7

    new-instance v5, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_7
    const-wide/16 v5, 0x804

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_8

    new-instance v5, Lcom/google/android/apps/plus/views/LinksCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_8
    const-wide/32 v5, 0x30000

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_9

    new-instance v5, Lcom/google/android/apps/plus/views/SquareCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/SquareCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_9
    const-wide/16 v5, 0x40

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_a

    new-instance v5, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_a
    const-wide/16 v5, 0xa0

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_b

    new-instance v5, Lcom/google/android/apps/plus/views/ImageCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_b
    const-wide/16 v5, 0x2000

    and-long/2addr v5, v1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_c

    new-instance v5, Lcom/google/android/apps/plus/views/LinksCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/LinksCardViewGroup;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_c
    new-instance v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-direct {v5, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0, p1, p3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->newStreamHeaderView$4b8874c5(Landroid/content/Context;Landroid/database/Cursor;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p3, p5}, Lcom/google/android/apps/plus/phone/StreamAdapter;->newStreamView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onPause()V
    .locals 6

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    iget-object v4, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mViewerHasReadPosts:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    :cond_1
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    invoke-super {p0, p1}, Lcom/android/common/widget/EsCompositeCursorAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamGridObservers:Ljava/util/HashSet;

    check-cast p1, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final removePromoCircleDelta(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mPromoCircleDeltaManager:Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->removeDelta(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public resetAnimationState()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount(I)I

    move-result v0

    if-lez v0, :cond_0

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    :goto_0
    return-void

    :cond_0
    const/high16 v1, -0x80000000

    iput v1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mVisibleIndex:I

    goto :goto_0
.end method

.method public final setGraySpamApproved(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mApprovedGraySpam:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final setMarkPostsAsRead(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mMarkPostsAsRead:Z

    return-void
.end method

.method public final setStreamHashActivityIds(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mActivityIds:Ljava/util/ArrayList;

    return-void
.end method

.method public final setStreamRestorePosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamRestorePosition:I

    return-void
.end method

.method public final triggerStreamObservers(ZI)V
    .locals 5
    .param p1    # Z
    .param p2    # I

    const/4 v4, -0x1

    if-eqz p1, :cond_0

    iput v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamRestorePosition:I

    if-gez p2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    invoke-virtual {v3}, Landroid/util/SparseIntArray;->clear()V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamGridObservers:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    iget v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamRestorePosition:I

    invoke-virtual {v2, p1, p2, v3}, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->onChanged(ZII)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    invoke-virtual {v3}, Landroid/util/SparseIntArray;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-ltz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    if-lt v3, p2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mCachedSpans:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->removeAt(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    iput v4, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamRestorePosition:I

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    invoke-super {p0, p1}, Lcom/android/common/widget/EsCompositeCursorAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter;->mStreamGridObservers:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
