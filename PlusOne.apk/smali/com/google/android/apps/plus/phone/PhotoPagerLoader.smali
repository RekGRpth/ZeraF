.class public final Lcom/google/android/apps/plus/phone/PhotoPagerLoader;
.super Lcom/google/android/apps/plus/phone/PhotoCursorLoader;
.source "PhotoPagerLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;
    }
.end annotation


# instance fields
.field private final mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private final mPhotoUrl:Ljava/lang/String;

.field private final mStartPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # [Lcom/google/android/apps/plus/api/MediaRef;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;
    .param p10    # I
    .param p11    # Ljava/lang/String;
    .param p12    # Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, -0x1

    move/from16 v0, p10

    if-eq v0, v1, :cond_0

    const/4 v10, 0x1

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mOwnerGaiaId:Ljava/lang/String;

    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mPhotoUrl:Ljava/lang/String;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mStartPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void

    :cond_0
    const/4 v10, 0x0

    goto :goto_0
.end method

.method private static copyPhotoCursorRow(Landroid/database/Cursor;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    .locals 9
    .param p0    # Landroid/database/Cursor;
    .param p1    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    array-length v1, v1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-interface {p0, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    :goto_0
    aput-object v1, v0, v5

    invoke-interface {p0, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    :goto_1
    aput-object v1, v0, v6

    invoke-interface {p0, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    :goto_2
    aput-object v1, v0, v7

    invoke-interface {p0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    :goto_3
    aput-object v1, v0, v8

    const/4 v3, 0x5

    const/4 v1, 0x5

    invoke-interface {p0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v1, v2

    :goto_4
    aput-object v1, v0, v3

    const/4 v1, 0x6

    const/4 v3, 0x6

    invoke-interface {p0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_5
    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-interface {p0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_3
    invoke-interface {p0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_4
    const/4 v1, 0x5

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_4

    :cond_5
    const/4 v2, 0x6

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 12

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v6, -0x1

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v7, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    new-instance v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v7, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    :goto_0
    array-length v7, v6

    if-ge v5, v7, :cond_2

    aget-object v7, v6, v5

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    aget-object v8, v6, v5

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    aget-object v8, v6, v5

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    aget-object v8, v6, v5

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    aget-object v8, v6, v5

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    aget-object v8, v6, v5

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    goto :goto_1

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mPhotoUrl:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mPhotoUrl:Ljava/lang/String;

    new-instance v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v7, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    :cond_2
    :goto_2
    return-object v3

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->getLoaderUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->setUri(Landroid/net/Uri;)V

    sget-object v7, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->setProjection([Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->mStartPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v7

    invoke-interface {v4, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v4, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_5

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    cmp-long v9, v7, v9

    if-nez v9, :cond_5

    move v0, v5

    :goto_4
    if-lez v0, :cond_8

    new-instance v2, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v5, Lcom/google/android/apps/plus/phone/PhotoPagerLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v2, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v4, v2}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->copyPhotoCursorRow(Landroid/database/Cursor;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    invoke-interface {v4, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_4
    :goto_5
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    if-eq v5, v0, :cond_4

    invoke-static {v4, v2}, Lcom/google/android/apps/plus/phone/PhotoPagerLoader;->copyPhotoCursorRow(Landroid/database/Cursor;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    goto :goto_5

    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_6
    move v0, v6

    goto :goto_4

    :cond_7
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    move-object v3, v2

    goto :goto_2

    :cond_8
    move-object v3, v4

    goto :goto_2
.end method
