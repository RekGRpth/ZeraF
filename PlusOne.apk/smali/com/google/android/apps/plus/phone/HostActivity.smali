.class public abstract Lcom/google/android/apps/plus/phone/HostActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "HostActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    return-void
.end method

.method private attachActionBar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected addParentStack(Lvedroid/support/v4/app/TaskStackBuilder;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/TaskStackBuilder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Lvedroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Lvedroid/support/v4/app/TaskStackBuilder;

    return-void
.end method

.method protected getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v2
.end method

.method protected getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->host_activity:I

    return v0
.end method

.method protected final getDefaultFragmentContainerViewId()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->fragment_container:I

    return v0
.end method

.method protected final getHostActionBar()Lcom/google/android/apps/plus/views/HostActionBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    return-object v0
.end method

.method protected final isIntentAccountActive()Z
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "HostActivity"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "HostActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Activity finished because it is associated with a signed-out account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onActionBarInvalidated()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->attachActionBar()V

    return-void
.end method

.method public final onActionButtonClicked(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onActionButtonClicked(I)V

    :cond_0
    return-void
.end method

.method protected onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    return-void
.end method

.method public onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->attachActionBar()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostActivity;->setContentView(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/HostActionBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->attachActionBar()V

    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->host_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-nez v4, :cond_0

    :goto_0
    return v3

    :cond_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onPrimarySpinnerSelectionChange(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrimarySpinnerSelectionChange(I)V

    :cond_0
    return-void
.end method

.method public onRefreshButtonClicked()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onUpButtonClick()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->shouldUpRecreateTask()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lvedroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Lvedroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostActivity;->addParentStack(Lvedroid/support/v4/app/TaskStackBuilder;)V

    invoke-virtual {v0}, Lvedroid/support/v4/app/TaskStackBuilder;->startActivities()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected final replaceFragment(Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->replaceFragment(Lvedroid/support/v4/app/Fragment;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostActivity;->mHostedFragment:Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction()V

    :cond_0
    return-void
.end method

.method protected shouldUpRecreateTask()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lvedroid/support/v4/app/NavUtils;->shouldUpRecreateTask(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
