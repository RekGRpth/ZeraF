.class final Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;
.super Landroid/os/AsyncTask;
.source "AllPhotosTileAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LoadingTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

.field private final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v3, 0x0

    check-cast p1, [Ljava/lang/String;

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    aget-object v5, p1, v6

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/api/AllPhotosViewOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;->mContext:Landroid/content/Context;

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/AllPhotosViewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    goto :goto_0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter$LoadingTask;->mAdapter:Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;->access$100(Lcom/google/android/apps/plus/phone/AllPhotosTileAdapter;)V

    return-void
.end method
