.class final Lcom/google/android/apps/plus/phone/ConversationActivity$2;
.super Ljava/lang/Object;
.source "ConversationActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/ConversationActivity;->onAttachFragment(Lvedroid/support/v4/app/Fragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/ConversationActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSendPhoto(Ljava/lang/String;I)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    const-string v0, "content://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendLocalPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I

    :goto_1
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$700(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_2
    # invokes: Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$1100(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$700(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_3
    # invokes: Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$1200(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onSendTextMessage(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$700(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    # invokes: Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$800(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v5, 0x0

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$1000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->makePersonFromParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v7

    new-instance v6, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v6, v7}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2, v6, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->createConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mCreateConversationRequestId:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$302(Lcom/google/android/apps/plus/phone/ConversationActivity;I)I

    goto :goto_1
.end method

.method public final onTypingStatusChanged(Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V
    .locals 4
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity$2;->this$0:Lcom/google/android/apps/plus/phone/ConversationActivity;

    # getter for: Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendTypingRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;)I

    :cond_0
    return-void
.end method
