.class final Lcom/google/android/apps/plus/phone/LocationController$2;
.super Ljava/lang/Thread;
.source "LocationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/LocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/LocationController;

.field final synthetic val$location:Landroid/location/Location;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$1300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/LocationController;->access$1400(Lcom/google/android/apps/plus/phone/LocationController;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-direct {v5, v4, v3}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    const/4 v7, 0x0

    move-object v4, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->start()V

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mDisplayDebugToast:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$200(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "location_source"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "location_source"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->hasPreciseLocation()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->getPreciseLocation()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v11

    const-string v10, "finest_location"

    :cond_1
    :goto_0
    if-eqz v11, :cond_2

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$1300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "location_description"

    invoke-virtual {v9, v1, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->val$location:Landroid/location/Location;

    invoke-virtual {v1, v9}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$2;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$1200(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/phone/LocationController$2$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/LocationController$2$1;-><init>(Lcom/google/android/apps/plus/phone/LocationController$2;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->hasPlaceLocation()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->getPlaceLocation()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v11

    const-string v10, "finest_location"

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->hasCoarseLocation()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->getCoarseLocation()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v11

    const-string v10, "coarse_location"

    goto :goto_0
.end method
