.class final Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;
.super Ljava/lang/Object;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

.field final synthetic val$data:Landroid/database/Cursor;

.field final synthetic val$loader:Lvedroid/support/v4/content/Loader;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lvedroid/support/v4/content/Loader;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$data:Landroid/database/Cursor;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$loader:Lvedroid/support/v4/content/Loader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mIsPaused:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$600(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$data:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$702(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$802(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Z)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$data:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$900(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1000(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I

    move-result v0

    :goto_1
    if-gez v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$data:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1200(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1000(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/database/Cursor;Lcom/google/android/apps/plus/api/MediaRef;)I

    move-result v0

    :cond_2
    if-gez v0, :cond_3

    const/4 v0, 0x0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1300(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$loader:Lvedroid/support/v4/content/Loader;

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->setPageable(Lcom/google/android/apps/plus/phone/Pageable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1300(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->val$data:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mRootView:Landroid/view/View;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Landroid/view/View;

    move-result-object v2

    # invokes: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->updateView(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1400(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mViewPager:Lcom/google/android/apps/plus/views/PhotoViewPager;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1500(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)Lcom/google/android/apps/plus/views/PhotoViewPager;

    move-result-object v1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/plus/views/PhotoViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$4;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->mCurrentIndex:I
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->access$1100(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;)I

    move-result v0

    goto :goto_1
.end method
