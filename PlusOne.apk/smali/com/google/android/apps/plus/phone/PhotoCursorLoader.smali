.class public abstract Lcom/google/android/apps/plus/phone/PhotoCursorLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PhotoCursorLoader.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/Pageable;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mAlbumId:Ljava/lang/String;

.field private final mAuthkey:Ljava/lang/String;

.field private mCircleOffset:I

.field private mDataSourceIsLoading:Z

.field private final mEventId:Ljava/lang/String;

.field private mEventResumeToken:Ljava/lang/String;

.field private mHasMore:Z

.field private final mInitialPageCount:I

.field private mIsLoadingMore:Z

.field private mLoadLimit:I

.field private mNetworkRequestMade:Z

.field private final mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private mPageable:Z

.field mPageableLoadingListener:Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;

.field private final mPaging:Z

.field private final mPhotoOfUserGaiaId:Ljava/lang/String;

.field private final mPhotoUrl:Ljava/lang/String;

.field private final mStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Z
    .param p10    # I
    .param p11    # Ljava/lang/String;

    const/4 v0, -0x1

    invoke-static/range {p3 .. p8}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getNotificationUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    new-instance v1, Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v1, p0}, Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Lvedroid/support/v4/content/Loader;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    const/16 v1, 0x10

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p11, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAuthkey:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoUrl:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPaging:Z

    iput-boolean p9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    iput p10, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mInitialPageCount:I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v1, :cond_0

    if-eq p10, v0, :cond_0

    mul-int/lit8 v0, p10, 0x10

    :cond_0
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/PhotoCursorLoader;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoCursorLoader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mDataSourceIsLoading:Z

    return p1
.end method

.method private doNetworkRequest()V
    .locals 11

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mNetworkRequestMade:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mNetworkRequestMade:Z

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->invokeLoadingListener(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAuthkey:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/content/EsEventData;->readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->getEventResumeToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mNetworkRequestMade:Z

    :goto_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->invokeLoadingListener(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, "profiles_scrapbook"

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    new-instance v0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAuthkey:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    :cond_2
    :goto_2
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    if-eqz v1, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAuthkey:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    const-string v1, "PhotoCursorLoader"

    const-string v2, "No valid IDs to load photos for"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static getNotificationUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    if-eqz p3, :cond_2

    const-string v2, "profiles_scrapbook"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-nez p0, :cond_1

    const-string v2, "PhotoCursorLoader"

    const-string v3, "Viewing stream photos w/o a valid owner GAIA ID"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, p0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_2
    if-eqz p4, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1

    :cond_4
    if-eqz p1, :cond_0

    if-nez p0, :cond_5

    const-string v2, "PhotoCursorLoader"

    const-string v3, "Viewing album photos w/o a valid owner GAIA ID"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_1
.end method

.method private invokeLoadingListener(Z)V
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoCursorLoader;Z)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public esLoadInBackground()Landroid/database/Cursor;
    .locals 12

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v9

    if-nez v9, :cond_1

    const-string v7, "PhotoCursorLoader"

    const-string v8, "load NULL URI; return empty cursor"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getProjection()[Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    if-eqz v9, :cond_2

    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mIsLoadingMore:Z

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->doNetworkRequest()V

    :cond_2
    :goto_1
    iget v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v9, :cond_b

    iget v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_b

    move v0, v7

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getSortOrder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    if-eqz v9, :cond_d

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    if-nez v9, :cond_c

    const-string v9, "sort_index ASC"

    :goto_3
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->setSortOrder(Ljava/lang/String;)V

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getSortOrder()Ljava/lang/String;

    move-result-object v6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v6, :cond_f

    :goto_4
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LIMIT 0, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->setSortOrder(Ljava/lang/String;)V

    :cond_4
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_10

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v1

    :goto_5
    if-ne v1, v3, :cond_11

    move v2, v7

    :goto_6
    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v9, :cond_12

    if-nez v2, :cond_5

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_12

    :cond_5
    move v9, v7

    :goto_7
    iput-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    iput-boolean v8, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mIsLoadingMore:Z

    if-nez v1, :cond_6

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    const/4 v5, 0x0

    :cond_6
    if-nez v5, :cond_9

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mCircleOffset:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->doNetworkRequest()V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_13

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v1

    :goto_8
    if-ne v1, v3, :cond_14

    move v2, v7

    :goto_9
    iget v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mCircleOffset:I

    if-ne v1, v9, :cond_7

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_15

    :cond_7
    move v9, v7

    :goto_a
    iput-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v9, :cond_16

    if-nez v2, :cond_8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_16

    :cond_8
    :goto_b
    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    :cond_9
    if-eqz v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->setSortOrder(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    invoke-static {v9, v10, v11}, Lcom/google/android/apps/plus/content/EsEventData;->getEventResumeToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    goto/16 :goto_1

    :cond_b
    move v0, v8

    goto/16 :goto_2

    :cond_c
    const-string v9, "timestamp DESC"

    goto/16 :goto_3

    :cond_d
    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    if-eqz v9, :cond_e

    const-string v9, "pending_status DESC,timestamp DESC"

    goto/16 :goto_3

    :cond_e
    const-string v9, "timestamp DESC"

    goto/16 :goto_3

    :cond_f
    const-string v6, ""

    goto/16 :goto_4

    :cond_10
    move v1, v8

    goto :goto_5

    :cond_11
    move v2, v8

    goto :goto_6

    :cond_12
    move v9, v8

    goto :goto_7

    :cond_13
    move v1, v8

    goto :goto_8

    :cond_14
    move v2, v8

    goto :goto_9

    :cond_15
    move v9, v8

    goto :goto_a

    :cond_16
    move v7, v8

    goto :goto_b
.end method

.method public final getCurrentPage()I
    .locals 2

    const/4 v0, -0x1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    if-eq v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    div-int/lit8 v0, v0, 0x10

    :cond_0
    return v0
.end method

.method final getLoaderUri()Landroid/net/Uri;
    .locals 8

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoUrl:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getNotificationUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final hasMore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDataSourceLoading()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mDataSourceIsLoading:Z

    return v0
.end method

.method public final loadMore()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    add-int/lit8 v0, v0, 0x30

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mIsLoadingMore:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->onContentChanged()V

    :cond_0
    return-void
.end method

.method protected final onAbandon()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->onAbandon()V

    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->onStartLoading()V

    return-void
.end method

.method public final setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageableLoadingListener:Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;

    return-void
.end method
