.class public Lcom/google/android/apps/plus/phone/PhotosSelectionActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "PhotosSelectionActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic createDefaultFragment()Lvedroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PhotosSelectionFragment;-><init>()V

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->photos_selection_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setVisibility(I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notif_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosSelectionActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveRecentImagesTimestamp(Landroid/content/Context;J)V

    :cond_0
    return-void
.end method
