.class public final Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;
.super Ljava/lang/Object;
.source "PromoCircleDeltaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PromoCircleDelta"
.end annotation


# instance fields
.field private mActivityId:Ljava/lang/String;

.field private mCircleData:Lcom/google/android/apps/plus/content/CircleData;

.field private mPersonId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mActivityId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mPersonId:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v0, p3}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mCircleData:Lcom/google/android/apps/plus/content/CircleData;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mActivityId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final getCircleData()Lcom/google/android/apps/plus/content/CircleData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mCircleData:Lcom/google/android/apps/plus/content/CircleData;

    return-object v0
.end method

.method public final getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mPersonId:Ljava/lang/String;

    return-object v0
.end method
