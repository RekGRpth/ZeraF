.class public final Lcom/google/android/apps/plus/phone/PlacesAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PlacesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PlacesAdapter$LocationQuery;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method

.method public static getLocation(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 2
    .param p0    # Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const v5, 0x1020006

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    const v5, 0x1020016

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x1020005

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p3}, Lcom/google/android/apps/plus/phone/PlacesAdapter;->getLocation(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->isPrecise()Z

    move-result v5

    if-eqz v5, :cond_1

    sget v5, Lcom/google/android/apps/plus/R$drawable;->list_current:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v5, Lcom/google/android/apps/plus/R$string;->my_location:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v3, p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->isCoarse()Z

    move-result v5

    if-eqz v5, :cond_2

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_location_city:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v5, Lcom/google/android/apps/plus/R$string;->my_city:I

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v3, p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_location_grey:I

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbLocation;->getBestAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->location_row_layout:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
