.class public Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NetworkTransactionsActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 3

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->progress_spinner:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->network_transactions_fragment:I

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentById(I)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->setProgressBar(Landroid/widget/ProgressBar;)V

    return-void

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$id;->action_bar_progress_spinner_view:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/R$layout;->network_transactions:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->showTitlebar(Z)V

    sget v2, Lcom/google/android/apps/plus/R$string;->preferences_network_transactions_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->setTitlebarTitle(Ljava/lang/String;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->network_transactions_menu:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->createTitlebarButtons(I)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->network_transactions_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    move v1, v2

    :goto_0
    return v1

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$id;->clear:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$id;->network_transactions_fragment:I

    invoke-virtual {v1, v3}, Lvedroid/support/v4/app/FragmentManager;->findFragmentById(I)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/NetworkTransactionsListFragment;->clear()V

    move v1, v2

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->clear:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 0
    .param p1    # Landroid/view/Menu;

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NetworkTransactionsActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
