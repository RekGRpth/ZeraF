.class public Lcom/google/android/apps/plus/phone/OobProfilePhotoActivity;
.super Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.source "OobProfilePhotoActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_ADD_PROFILE_PHOTO_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onContinuePressed()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinuePressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onCreate(Landroid/os/Bundle;)V

    sget v3, Lcom/google/android/apps/plus/R$layout;->oob_profile_photo_activity:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/OobProfilePhotoActivity;->setContentView(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->info_title:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/OobProfilePhotoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobProfilePhotoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "account"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobProfilePhotoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->oob_profile_photo_title:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
