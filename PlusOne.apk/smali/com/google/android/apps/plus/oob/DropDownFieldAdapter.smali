.class final Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;
.super Landroid/widget/BaseAdapter;
.source "DropDownFieldAdapter.java"


# instance fields
.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->mItems:Ljava/util/List;

    return-void
.end method

.method private getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    return-object v0
.end method

.method private getTextView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    move-object v3, p2

    check-cast v3, Landroid/widget/TextView;

    if-nez v3, :cond_0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, p4, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    move-result-object v2

    iget-object v4, v2, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v3
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v0, 0x1090009

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getTextView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    sget v0, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getTextView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public final indexOf(Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;)I
    .locals 10
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getCount()I

    move-result v0

    :goto_0
    if-ge v2, v0, :cond_c

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    move-result-object v1

    iget-object v6, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    if-eqz v6, :cond_0

    if-nez p1, :cond_2

    :cond_0
    if-ne v6, p1, :cond_1

    move v3, v4

    :goto_1
    if-eqz v3, :cond_b

    :goto_2
    return v2

    :cond_1
    move v3, v5

    goto :goto_1

    :cond_2
    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    iget-object v7, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    iget-object v7, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    iget-object v7, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    if-eqz v3, :cond_3

    if-nez v7, :cond_6

    :cond_3
    if-ne v3, v7, :cond_5

    move v3, v4

    :goto_3
    if-eqz v3, :cond_a

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->gender:Lcom/google/api/services/plusi/model/MobileGender;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->gender:Lcom/google/api/services/plusi/model/MobileGender;

    if-eqz v3, :cond_4

    if-nez v6, :cond_9

    :cond_4
    if-ne v3, v6, :cond_8

    move v3, v4

    :goto_4
    if-eqz v3, :cond_a

    move v3, v4

    goto :goto_1

    :cond_5
    move v3, v5

    goto :goto_3

    :cond_6
    iget-object v8, v3, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v8, v3, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v3, v3, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v4

    goto :goto_3

    :cond_7
    move v3, v5

    goto :goto_3

    :cond_8
    move v3, v5

    goto :goto_4

    :cond_9
    iget-object v3, v3, Lcom/google/api/services/plusi/model/MobileGender;->type:Ljava/lang/String;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/MobileGender;->type:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    goto :goto_4

    :cond_a
    move v3, v5

    goto :goto_1

    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_c
    const/4 v2, -0x1

    goto :goto_2
.end method
