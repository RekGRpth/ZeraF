.class public final Lcom/google/android/apps/plus/oob/OutOfBoxInflater;
.super Ljava/lang/Object;
.source "OutOfBoxInflater.java"


# instance fields
.field private final mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mOuterLayout:Landroid/view/ViewGroup;

.field private final mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/google/android/apps/plus/views/BottomActionBar;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mOuterLayout:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final inflateFromResponse(Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 19
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxView;
    .param p2    # Lcom/google/android/apps/plus/oob/ActionCallback;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v15}, Landroid/view/ViewGroup;->removeAllViews()V

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->title:Ljava/lang/String;

    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mOuterLayout:Landroid/view/ViewGroup;

    sget v16, Lcom/google/android/apps/plus/R$id;->info_title:I

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->title:Ljava/lang/String;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->header:Ljava/lang/String;

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mOuterLayout:Landroid/view/ViewGroup;

    sget v16, Lcom/google/android/apps/plus/R$id;->info_header:I

    invoke-virtual/range {v15 .. v16}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->header:Ljava/lang/String;

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const/4 v11, 0x0

    sget v12, Lcom/google/android/apps/plus/R$id;->oob_item_0:I

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->field:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    move v4, v7

    add-int/lit8 v9, v7, -0x1

    :goto_0
    if-ltz v9, :cond_2

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    if-eqz v15, :cond_2

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v9, v9, -0x1

    goto :goto_0

    :cond_2
    const/4 v9, 0x0

    :goto_1
    if-ge v9, v4, :cond_12

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v15, :cond_a

    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    const-string v16, "TEXT_INPUT"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    sget v15, Lcom/google/android/apps/plus/R$layout;->signup_text_input:I

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v15, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object v5, v15

    :goto_3
    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    if-nez v15, :cond_3

    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    if-eqz v15, :cond_f

    :cond_3
    const/4 v15, 0x1

    :goto_4
    if-eqz v15, :cond_11

    move-object/from16 v0, p2

    invoke-virtual {v5, v6, v12, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    :goto_5
    invoke-virtual {v5}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getChildCount()I

    move-result v15

    add-int/2addr v15, v12

    invoke-virtual {v5, v15}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->setId(I)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v15, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getId()I

    move-result v15

    add-int/lit8 v12, v15, 0x1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v10

    const-string v15, "TEXT_INPUT"

    iget-object v0, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    move-object v11, v10

    check-cast v11, Landroid/widget/EditText;

    const/4 v15, 0x5

    invoke-virtual {v11, v15}, Landroid/widget/EditText;->setImeOptions(I)V

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_5
    const-string v16, "DROPDOWN"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    sget v15, Lcom/google/android/apps/plus/R$layout;->signup_spinner_input:I

    goto :goto_2

    :cond_6
    const-string v16, "CHECKBOX"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    sget v15, Lcom/google/android/apps/plus/R$layout;->signup_checkbox_input:I

    goto :goto_2

    :cond_7
    const-string v16, "HIDDEN"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    sget v15, Lcom/google/android/apps/plus/R$layout;->signup_hidden_input:I

    goto/16 :goto_2

    :cond_8
    const-string v16, "BIRTHDAY_INPUT"

    iget-object v0, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    sget v15, Lcom/google/android/apps/plus/R$layout;->signup_birthday_input:I

    goto/16 :goto_2

    :cond_9
    const-string v16, "OutOfBoxInflater"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "Input field has unsupported type: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v15, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    move-object v5, v15

    goto/16 :goto_3

    :cond_a
    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    if-eqz v15, :cond_b

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    sget v16, Lcom/google/android/apps/plus/R$layout;->signup_text_field:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object v5, v15

    goto/16 :goto_3

    :cond_b
    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->error:Lcom/google/api/services/plusi/model/OutOfBoxError;

    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    sget v16, Lcom/google/android/apps/plus/R$layout;->signup_error_field:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object v5, v15

    goto/16 :goto_3

    :cond_c
    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    if-eqz v15, :cond_d

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    sget v16, Lcom/google/android/apps/plus/R$layout;->signup_button_field:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object v5, v15

    goto/16 :goto_3

    :cond_d
    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    if-eqz v15, :cond_e

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    sget v16, Lcom/google/android/apps/plus/R$layout;->signup_image_field:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object v5, v15

    goto/16 :goto_3

    :cond_e
    const-string v15, "OutOfBoxInflater"

    const-string v16, "Field doesn\'t have content."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v15, 0x0

    move-object v5, v15

    goto/16 :goto_3

    :cond_f
    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v15, :cond_10

    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v15, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    if-eqz v15, :cond_10

    iget-object v15, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v15, v15, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-eqz v15, :cond_10

    const/4 v15, 0x1

    goto/16 :goto_4

    :cond_10
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_11
    const/4 v15, 0x0

    invoke-virtual {v5, v6, v12, v15}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    goto/16 :goto_5

    :cond_12
    if-eqz v11, :cond_13

    const/4 v15, 0x6

    invoke-virtual {v11, v15}, Landroid/widget/EditText;->setImeOptions(I)V

    :cond_13
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/BottomActionBar;->resetButtons()V

    move v9, v4

    move v13, v12

    :goto_6
    if-ge v9, v7, :cond_14

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    add-int/lit8 v12, v13, 0x1

    iget-object v0, v3, Lcom/google/api/services/plusi/model/OutOfBoxAction;->text:Ljava/lang/String;

    move-object/from16 v16, v0

    new-instance v17, Lcom/google/android/apps/plus/oob/OutOfBoxInflater$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/oob/OutOfBoxInflater$1;-><init>(Lcom/google/android/apps/plus/oob/OutOfBoxInflater;Lcom/google/android/apps/plus/oob/ActionCallback;Lcom/google/api/services/plusi/model/OutOfBoxAction;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v15, v13, v0, v3, v1}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    add-int/lit8 v9, v9, 0x1

    move v13, v12

    goto :goto_6

    :cond_14
    return-void
.end method
