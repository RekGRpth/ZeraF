.class public Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;
.super Ljava/lang/Object;
.source "OutOfBoxRequestParcelable.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-lez v1, :cond_0

    new-array v0, v1, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iput-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getRequest()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
