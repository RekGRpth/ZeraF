.class public final Lcom/google/android/apps/plus/oob/OobTextStyler;
.super Ljava/lang/Object;
.source "OobTextStyler.java"


# direct methods
.method public static applyStyle(Landroid/widget/TextView;Lcom/google/api/services/plusi/model/TextStyle;)V
    .locals 3
    .param p0    # Landroid/widget/TextView;
    .param p1    # Lcom/google/api/services/plusi/model/TextStyle;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "LEGAL_TEXT"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/TextStyle;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$dimen;->oob_legal_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    sget v1, Lcom/google/android/apps/plus/R$color;->oob_legal_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    sget v1, Lcom/google/android/apps/plus/R$color;->oob_legal_link_text_color:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/widget/TextView;->setLinkTextColor(I)V

    sget v1, Lcom/google/android/apps/plus/R$bool;->oob_legal_links_clickable:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setLinksClickable(Z)V

    :cond_0
    return-void
.end method
