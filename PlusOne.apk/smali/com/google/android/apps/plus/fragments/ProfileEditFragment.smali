.class public Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "ProfileEditFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;,
        Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;,
        Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;,
        Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/Fragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field mAddItemView:Landroid/widget/TextView;

.field mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

.field private mAutoAddEnabled:Z

.field private mCircleLoaderHasFinished:Z

.field mContentView:Landroid/widget/LinearLayout;

.field mDeletedFieldView:Landroid/view/View;

.field private mDomainId:Ljava/lang/String;

.field private mDomainName:Ljava/lang/String;

.field private mEditMode:I

.field private mEducations:Lcom/google/api/services/plusi/model/Educations;

.field private mEmployments:Lcom/google/api/services/plusi/model/Employments;

.field mFocusOverrideView:Landroid/view/View;

.field private mHasPublicCircle:Z

.field private mHelpDetails:Ljava/lang/String;

.field private mHelpTitle:Ljava/lang/String;

.field private mItemLoaderCallbacks:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;

.field private mItemViewIdsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;",
            ">;"
        }
    .end annotation
.end field

.field private mItemsJson:Ljava/lang/String;

.field private mItemsLoaderHasFinished:Z

.field private mLocations:Lcom/google/api/services/plusi/model/Locations;

.field private mModifiedViews:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mOriginalAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mOriginalCount:I

.field private mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

.field private mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

.field private mOriginalItemsJson:Ljava/lang/String;

.field private mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

.field private mOriginalRequiredScopeId:Lcom/google/api/services/plusi/model/SharingTargetId;

.field private final mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mProfilePendingRequestId:Ljava/lang/Integer;

.field mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

.field mScollView:Landroid/widget/ScrollView;

.field private mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

.field private mSharingRosterDataJson:Ljava/lang/String;

.field private mViewIdNextCurrent:I

.field private mViewIdNextEndDate:I

.field private mViewIdNextName:I

.field private mViewIdNextStartDate:I

.field private mViewIdNextTitleOrMajor:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    const/16 v0, 0x7d0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    const/16 v0, 0xfa0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;I)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # I

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->finishActivity(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeChangedField(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithOriginalValues()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/api/services/plusi/model/Employments;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Employments;)Lcom/google/api/services/plusi/model/Employments;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Lcom/google/api/services/plusi/model/Employments;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Employments;)Lcom/google/api/services/plusi/model/Employments;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Lcom/google/api/services/plusi/model/Employments;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/api/services/plusi/model/Educations;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Educations;)Lcom/google/api/services/plusi/model/Educations;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Lcom/google/api/services/plusi/model/Educations;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Educations;)Lcom/google/api/services/plusi/model/Educations;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Lcom/google/api/services/plusi/model/Educations;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/SharingRosterData;)Lcom/google/api/services/plusi/model/SharingRosterData;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Lcom/google/api/services/plusi/model/SharingRosterData;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsLoaderHasFinished:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithItemData()V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method private addChangedField(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    invoke-virtual {v4, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalCount:I

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-ne v4, v2, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    if-ne p1, v4, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    :goto_2
    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto :goto_2
.end method

.method private addItem()Landroid/view/View;
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Lcom/google/api/services/plusi/model/Employment;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Lcom/google/api/services/plusi/model/Education;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, v0, v0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private configureDataInfo(Landroid/view/View;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;Lcom/google/api/services/plusi/model/DateInfo;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;
    .param p3    # Lcom/google/api/services/plusi/model/DateInfo;

    sget v3, Lcom/google/android/apps/plus/R$id;->start:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iget v3, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->startDate:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setId(I)V

    if-eqz p3, :cond_0

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v3, :cond_0

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    sget v3, Lcom/google/android/apps/plus/R$id;->end:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iget v3, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->endDate:I

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setId(I)V

    if-eqz p3, :cond_1

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v3, :cond_1

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    if-eqz v3, :cond_1

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$id;->current:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget v3, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setId(I)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    if-eqz p3, :cond_2

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DateInfo;->current:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$id;->delete_item:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method private createEducations()Lcom/google/api/services/plusi/model/Educations;
    .locals 9

    new-instance v2, Lcom/google/api/services/plusi/model/Educations;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/Educations;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v5

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v7, v2, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    new-instance v1, Lcom/google/api/services/plusi/model/Education;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Education;-><init>()V

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/google/api/services/plusi/model/Education;->majorConcentration:Ljava/lang/String;

    new-instance v7, Lcom/google/api/services/plusi/model/DateInfo;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/DateInfo;-><init>()V

    iput-object v7, v1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->startDate:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    new-instance v8, Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-direct {v8}, Lcom/google/api/services/plusi/model/CoarseDate;-><init>()V

    iput-object v8, v7, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    :cond_0
    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->endDate:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    new-instance v8, Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-direct {v8}, Lcom/google/api/services/plusi/model/CoarseDate;-><init>()V

    iput-object v8, v7, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    :cond_1
    iget-object v7, v1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget v8, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getCurrent(Landroid/view/View;I)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v7, Lcom/google/api/services/plusi/model/DateInfo;->current:Ljava/lang/Boolean;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createMetadata()Lcom/google/api/services/plusi/model/Metadata;

    move-result-object v7

    iput-object v7, v2, Lcom/google/api/services/plusi/model/Educations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    return-object v2
.end method

.method private createEmployments()Lcom/google/api/services/plusi/model/Employments;
    .locals 9

    new-instance v2, Lcom/google/api/services/plusi/model/Employments;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/Employments;-><init>()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v5

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v7, v2, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_2

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    new-instance v1, Lcom/google/api/services/plusi/model/Employment;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Employment;-><init>()V

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->title:Ljava/lang/String;

    new-instance v7, Lcom/google/api/services/plusi/model/DateInfo;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/DateInfo;-><init>()V

    iput-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->startDate:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    new-instance v8, Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-direct {v8}, Lcom/google/api/services/plusi/model/CoarseDate;-><init>()V

    iput-object v8, v7, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    :cond_0
    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->endDate:I

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    new-instance v8, Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-direct {v8}, Lcom/google/api/services/plusi/model/CoarseDate;-><init>()V

    iput-object v8, v7, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v7, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    :cond_1
    iget-object v7, v1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    iget v8, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getCurrent(Landroid/view/View;I)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v7, Lcom/google/api/services/plusi/model/DateInfo;->current:Ljava/lang/Boolean;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createMetadata()Lcom/google/api/services/plusi/model/Metadata;

    move-result-object v7

    iput-object v7, v2, Lcom/google/api/services/plusi/model/Employments;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    return-object v2
.end method

.method private createJson()Ljava/lang/String;
    .locals 4

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v3, :pswitch_data_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createEmployments()Lcom/google/api/services/plusi/model/Employments;

    move-result-object v1

    invoke-static {}, Lcom/google/api/services/plusi/model/EmploymentsJson;->getInstance()Lcom/google/api/services/plusi/model/EmploymentsJson;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/api/services/plusi/model/EmploymentsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createEducations()Lcom/google/api/services/plusi/model/Educations;

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/EducationsJson;->getInstance()Lcom/google/api/services/plusi/model/EducationsJson;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/EducationsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_2
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createLocations(Z)Lcom/google/api/services/plusi/model/Locations;

    move-result-object v2

    invoke-static {}, Lcom/google/api/services/plusi/model/LocationsJson;->getInstance()Lcom/google/api/services/plusi/model/LocationsJson;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/api/services/plusi/model/LocationsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private createLocations(Z)Lcom/google/api/services/plusi/model/Locations;
    .locals 8
    .param p1    # Z

    new-instance v5, Lcom/google/api/services/plusi/model/Locations;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/Locations;-><init>()V

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v6, v4, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getCurrent(Landroid/view/View;I)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, v4, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "~~Internal~CurrentLocation."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v6, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    if-nez v6, :cond_1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    :cond_1
    iget-object v6, v5, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    iget v7, v4, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getEditedString(Landroid/view/View;I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    iput-object v0, v5, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createMetadata()Lcom/google/api/services/plusi/model/Metadata;

    move-result-object v6

    iput-object v6, v5, Lcom/google/api/services/plusi/model/Locations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    return-object v5
.end method

.method private createMetadata()Lcom/google/api/services/plusi/model/Metadata;
    .locals 11

    const/4 v10, 0x1

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    new-instance v4, Lcom/google/api/services/plusi/model/Metadata;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/Metadata;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    if-ne v2, v10, :cond_5

    if-nez v8, :cond_5

    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    if-eq v7, v10, :cond_5

    const/16 v9, 0x9

    if-ne v7, v9, :cond_1

    const-string v9, "PUBLIC"

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    const/16 v9, 0x8

    if-ne v7, v9, :cond_2

    const-string v9, "DOMAIN"

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v9, 0x7

    if-ne v7, v9, :cond_3

    const-string v9, "EXTENDED_CIRCLES"

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/4 v9, 0x5

    if-ne v7, v9, :cond_4

    const-string v9, "MY_CIRCLES"

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const/16 v9, 0x65

    if-ne v7, v9, :cond_0

    const-string v9, "PRIVATE"

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    goto :goto_0

    :cond_5
    const-string v9, "CUSTOM_CHIPS"

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    new-instance v9, Lcom/google/api/services/plusi/model/SharingRoster;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/SharingRoster;-><init>()V

    iput-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalRequiredScopeId:Lcom/google/api/services/plusi/model/SharingTargetId;

    iput-object v10, v9, Lcom/google/api/services/plusi/model/SharingRoster;->requiredScopeId:Lcom/google/api/services/plusi/model/SharingTargetId;

    iget-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, v9, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_6

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v1

    new-instance v6, Lcom/google/api/services/plusi/model/SharingTargetId;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/SharingTargetId;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :goto_2
    :pswitch_0
    iget-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_1
    const-string v9, "PUBLIC"

    iput-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_2

    :pswitch_2
    const-string v9, "DASHER_DOMAIN"

    iput-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_2

    :pswitch_3
    const-string v9, "EXTENDED_CIRCLES"

    iput-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_2

    :pswitch_4
    const-string v9, "YOUR_CIRCLES"

    iput-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_2

    :pswitch_5
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->getFocusCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v8, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUser(I)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v5

    new-instance v6, Lcom/google/api/services/plusi/model/SharingTargetId;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/SharingTargetId;-><init>()V

    new-instance v9, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    iput-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v9, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v9, v4, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private dismissProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private finishActivity(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "profile_edit_return_json"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "profile_edit_mode"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "profile_edit_items_json"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createJson()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lvedroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method

.method private getAudience(Lcom/google/api/services/plusi/model/Metadata;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 16
    .param p1    # Lcom/google/api/services/plusi/model/Metadata;

    :goto_0
    if-nez p1, :cond_2

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHasPublicCircle:Z

    if-eqz v10, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/content/CircleData;

    const-string v10, "0"

    const/16 v11, 0x9

    sget v12, Lcom/google/android/apps/plus/R$string;->acl_public:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v1, v10, v11, v12, v13}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    :goto_1
    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v10, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    :goto_2
    return-object v10

    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    if-eqz v10, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/content/CircleData;

    const-string v10, "v.domain"

    const/16 v11, 0x8

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-direct {v1, v10, v11, v12, v13}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_1

    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/content/CircleData;

    const-string v10, "1c"

    const/4 v11, 0x5

    sget v12, Lcom/google/android/apps/plus/R$string;->acl_your_circles:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {v1, v10, v11, v12, v13}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    goto :goto_1

    :cond_2
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    if-eqz v10, :cond_3

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    if-eqz v10, :cond_3

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_9

    :cond_3
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/api/services/plusi/model/Metadata;->scope:Ljava/lang/String;

    const-string v10, "PUBLIC"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v11, Lcom/google/android/apps/plus/content/CircleData;

    const-string v12, "0"

    const/16 v13, 0x9

    sget v14, Lcom/google/android/apps/plus/R$string;->acl_public:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v10, v11}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_2

    :cond_4
    const-string v10, "DOMAIN"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v11, Lcom/google/android/apps/plus/content/CircleData;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainId:Ljava/lang/String;

    const/16 v13, 0x8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    const/4 v15, 0x0

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v10, v11}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto/16 :goto_2

    :cond_5
    const-string v10, "EXTENDED_CIRCLES"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v11, Lcom/google/android/apps/plus/content/CircleData;

    const-string v12, "1f"

    const/4 v13, 0x7

    sget v14, Lcom/google/android/apps/plus/R$string;->acl_extended_network:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v10, v11}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto/16 :goto_2

    :cond_6
    const-string v10, "MY_CIRCLES"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v11, Lcom/google/android/apps/plus/content/CircleData;

    const-string v12, "1c"

    const/4 v13, 0x5

    sget v14, Lcom/google/android/apps/plus/R$string;->acl_your_circles:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v10, v11}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto/16 :goto_2

    :cond_7
    const-string v10, "PRIVATE"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v11, Lcom/google/android/apps/plus/content/CircleData;

    const-string v12, "v.private"

    const/16 v13, 0x65

    sget v14, Lcom/google/android/apps/plus/R$string;->acl_private:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v10, v11}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    goto/16 :goto_2

    :cond_8
    const/16 p1, 0x0

    goto/16 :goto_0

    :cond_9
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v5, v10, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v9

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v9, :cond_10

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/SharingTargetId;

    iget-object v10, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    if-eqz v10, :cond_e

    const-string v10, "PUBLIC"

    iget-object v11, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    new-instance v10, Lcom/google/android/apps/plus/content/CircleData;

    const-string v11, "0"

    const/16 v12, 0x9

    sget v13, Lcom/google/android/apps/plus/R$string;->acl_public:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_b
    const-string v10, "DASHER_DOMAIN"

    iget-object v11, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    new-instance v10, Lcom/google/android/apps/plus/content/CircleData;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainId:Ljava/lang/String;

    const/16 v12, 0x8

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_c
    const-string v10, "EXTENDED_CIRCLES"

    iget-object v11, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    new-instance v10, Lcom/google/android/apps/plus/content/CircleData;

    const-string v11, "1f"

    const/4 v12, 0x7

    sget v13, Lcom/google/android/apps/plus/R$string;->acl_extended_network:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_d
    const-string v10, "YOUR_CIRCLES"

    iget-object v11, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    new-instance v10, Lcom/google/android/apps/plus/content/CircleData;

    const-string v11, "1c"

    const/4 v12, 0x5

    sget v13, Lcom/google/android/apps/plus/R$string;->acl_your_circles:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-direct {v10, v11, v12, v13, v14}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_e
    iget-object v10, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    if-eqz v10, :cond_f

    iget-object v10, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getCircleNameFromSharingRoster(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v10, Lcom/google/android/apps/plus/content/CircleData;

    iget-object v11, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-direct {v10, v11, v12, v6, v13}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_f
    iget-object v10, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v10, :cond_a

    iget-object v10, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    if-eqz v10, :cond_a

    iget-object v10, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getPersonNameFromSharingRoster(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v10, Lcom/google/android/apps/plus/content/PersonData;

    iget-object v11, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-direct {v10, v11, v6, v12}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_10
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_11

    const/16 p1, 0x0

    goto/16 :goto_0

    :cond_11
    new-instance v10, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v10, v7, v2}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_2
.end method

.method private getCircleNameFromSharingRoster(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getNameFromSharingRoster(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCurrent(Landroid/view/View;I)Z
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getEditedString(Landroid/view/View;I)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getItemCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private getNameFromSharingRoster(ILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterDataJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/SharingRosterDataJson;->getInstance()Lcom/google/api/services/plusi/model/SharingRosterDataJson;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterDataJson:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/api/services/plusi/model/SharingRosterDataJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/SharingRosterData;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SharingRosterData;->targets:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SharingRosterData;->targets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SharingRosterData;->targets:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/SharingTarget;

    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->id:Lcom/google/api/services/plusi/model/SharingTargetId;

    if-eqz v3, :cond_1

    packed-switch p1, :pswitch_data_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->id:Lcom/google/api/services/plusi/model/SharingTargetId;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    invoke-static {v3, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->displayName:Ljava/lang/String;

    :goto_1
    return-object v3

    :pswitch_1
    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->id:Lcom/google/api/services/plusi/model/SharingTargetId;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->id:Lcom/google/api/services/plusi/model/SharingTargetId;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v3, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->displayName:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    const-string v3, "DASHER_DOMAIN"

    iget-object v4, v2, Lcom/google/api/services/plusi/model/SharingTarget;->id:Lcom/google/api/services/plusi/model/SharingTargetId;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lcom/google/api/services/plusi/model/SharingTarget;->displayName:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getPersonNameFromSharingRoster(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getNameFromSharingRoster(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getView(Lcom/google/api/services/plusi/model/Education;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;
    .locals 9
    .param p1    # Lcom/google/api/services/plusi/model/Education;
    .param p2    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    const/4 v6, 0x0

    if-nez p2, :cond_0

    new-instance p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    add-int/lit8 v0, v4, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    iget v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    add-int/lit8 v0, v5, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;-><init>(IIIII)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->profile_edit_item_education:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iget v0, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setId(I)V

    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    :goto_0
    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->title:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iget v0, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setId(I)V

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Education;->majorConcentration:Ljava/lang/String;

    :goto_1
    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    iget-object v6, p1, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    :cond_1
    invoke-direct {p0, v8, p2, v6}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->configureDataInfo(Landroid/view/View;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;Lcom/google/api/services/plusi/model/DateInfo;)V

    return-object v8

    :cond_2
    const-string v0, ""

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method private getView(Lcom/google/api/services/plusi/model/Employment;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;
    .locals 9
    .param p1    # Lcom/google/api/services/plusi/model/Employment;
    .param p2    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    const/4 v6, 0x0

    if-nez p2, :cond_0

    new-instance p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    add-int/lit8 v0, v4, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    iget v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    add-int/lit8 v0, v5, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;-><init>(IIIII)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->profile_edit_item_employment:I

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iget v0, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setId(I)V

    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    :goto_0
    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->title:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/EditText;

    iget v0, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setId(I)V

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/Employment;->title:Ljava/lang/String;

    :goto_1
    invoke-virtual {v7, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_1

    iget-object v6, p1, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    :cond_1
    invoke-direct {p0, v8, p2, v6}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->configureDataInfo(Landroid/view/View;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;Lcom/google/api/services/plusi/model/DateInfo;)V

    return-object v8

    :cond_2
    const-string v0, ""

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method private getView(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    if-nez p3, :cond_0

    new-instance p3, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    add-int/lit8 v0, v1, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    add-int/lit8 v0, v3, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    add-int/lit8 v0, v4, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    iget v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    add-int/lit8 v0, v5, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    move-object v0, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;-><init>(IIIII)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->profile_edit_item_location:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/EditText;

    iget v0, p3, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-virtual {v8, v0}, Landroid/widget/EditText;->setId(I)V

    invoke-virtual {v8, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->current:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iget v0, p3, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setId(I)V

    invoke-virtual {v6, v10}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v9, 0x1

    :goto_0
    invoke-virtual {v6, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->delete_item:I

    invoke-virtual {v10, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    return-object v10

    :cond_1
    const/4 v9, 0x0

    goto :goto_0
.end method

.method private onCancel()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mFocusOverrideView:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    sget v0, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_edit_items_exit_unsaved:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->finishActivity(I)V

    goto :goto_0
.end method

.method private removeChangedField(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    invoke-virtual {v4, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalCount:I

    if-nez v4, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v4

    if-nez v4, :cond_2

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    if-ne v4, v2, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v2

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method private removeFocus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mFocusOverrideView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mFocusOverrideView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    return-void
.end method

.method private showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_edit_update_error:I

    new-array v5, v3, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    move v2, v3

    :cond_1
    return v2

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private updateViewsWithDateInfoValues(Landroid/view/View;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;Lcom/google/api/services/plusi/model/DateInfo;)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;
    .param p3    # Lcom/google/api/services/plusi/model/DateInfo;

    const/4 v4, 0x1

    const/4 v7, 0x0

    if-eqz p3, :cond_0

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v8, :cond_0

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    if-eqz v8, :cond_0

    move v3, v4

    :goto_0
    if-eqz v3, :cond_1

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->start:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    :goto_1
    iget v8, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->startDate:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    new-instance v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    invoke-direct {v6, p0, v2, v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v6, v8, v7, v7, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz p3, :cond_2

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    if-eqz v8, :cond_2

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    if-eqz v8, :cond_2

    move v3, v4

    :goto_2
    if-eqz v3, :cond_3

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->end:Lcom/google/api/services/plusi/model/CoarseDate;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/CoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    :goto_3
    iget v8, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->endDate:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    new-instance v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    invoke-direct {v6, p0, v2, v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    if-eqz p3, :cond_4

    iget-object v8, p3, Lcom/google/api/services/plusi/model/DateInfo;->current:Ljava/lang/Boolean;

    invoke-static {v8}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v8

    if-eqz v8, :cond_4

    :goto_4
    iget v7, p2, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v1, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;

    invoke-direct {v1, p0, v2, v6, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/widget/EditText;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;Z)V

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v7

    invoke-virtual {v1, v0, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void

    :cond_0
    move v3, v7

    goto :goto_0

    :cond_1
    const-string v5, ""

    goto :goto_1

    :cond_2
    move v3, v7

    goto :goto_2

    :cond_3
    const-string v5, ""

    goto :goto_3

    :cond_4
    move v4, v7

    goto :goto_4
.end method

.method private updateViewsWithItemData()V
    .locals 10

    const/4 v5, 0x0

    iget v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    if-nez v5, :cond_1

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAutoAddEnabled:Z

    if-eqz v8, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addItem()Landroid/view/View;

    :cond_1
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAutoAddEnabled:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithOriginalValues()V

    return-void

    :pswitch_0
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v5, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/Employment;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v3, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    :goto_2
    invoke-direct {p0, v2, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Lcom/google/api/services/plusi/model/Employment;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    invoke-virtual {v8, v4, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    :pswitch_1
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v5, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/Education;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v3, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    :goto_4
    invoke-direct {p0, v1, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Lcom/google/api/services/plusi/model/Education;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    invoke-virtual {v8, v4, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    const/4 v7, 0x0

    goto :goto_4

    :pswitch_2
    const/4 v6, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v0, v8, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "~~Internal~CurrentLocation."

    invoke-virtual {v0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    const/16 v9, 0x1b

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    :goto_5
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v6, v5

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v5, :cond_7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-le v8, v3, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    :goto_7
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8, v0, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    invoke-virtual {v8, v4, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_5
    const/4 v8, 0x0

    invoke-direct {p0, v0, v0, v8}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;)Landroid/view/View;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    invoke-virtual {v8, v4, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v6, 0x1

    goto :goto_5

    :cond_6
    const/4 v7, 0x0

    goto :goto_7

    :cond_7
    move v5, v6

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updateViewsWithOriginalValues()V
    .locals 21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mModifiedViews:Ljava/util/HashSet;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v12

    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    if-nez v9, :cond_2

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-ge v6, v9, :cond_7

    if-ge v6, v12, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/Employment;

    move-object/from16 v11, v17

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    if-eqz v11, :cond_4

    iget-object v14, v11, Lcom/google/api/services/plusi/model/Employment;->employer:Ljava/lang/String;

    :goto_4
    iget v0, v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    new-instance v16, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v20}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz v11, :cond_5

    iget-object v14, v11, Lcom/google/api/services/plusi/model/Employment;->title:Ljava/lang/String;

    :goto_5
    iget v0, v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    new-instance v16, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v20}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz v11, :cond_6

    iget-object v4, v11, Lcom/google/api/services/plusi/model/Employment;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v15, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithDateInfoValues(Landroid/view/View;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;Lcom/google/api/services/plusi/model/DateInfo;)V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    :cond_3
    const/4 v11, 0x0

    goto :goto_3

    :cond_4
    const-string v14, ""

    goto :goto_4

    :cond_5
    const-string v14, ""

    goto :goto_5

    :cond_6
    const/4 v4, 0x0

    goto :goto_6

    :cond_7
    if-le v12, v9, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDeletedFieldView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V

    goto/16 :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v12

    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    if-nez v9, :cond_9

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_8
    const/4 v12, 0x0

    goto :goto_7

    :cond_9
    const/4 v6, 0x0

    :goto_8
    if-ge v6, v9, :cond_e

    if-ge v6, v12, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/Education;

    move-object/from16 v11, v17

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    if-eqz v11, :cond_b

    iget-object v14, v11, Lcom/google/api/services/plusi/model/Education;->school:Ljava/lang/String;

    :goto_a
    iget v0, v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    new-instance v16, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v20}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz v11, :cond_c

    iget-object v14, v11, Lcom/google/api/services/plusi/model/Education;->majorConcentration:Ljava/lang/String;

    :goto_b
    iget v0, v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    new-instance v16, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v20}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    if-eqz v11, :cond_d

    iget-object v4, v11, Lcom/google/api/services/plusi/model/Education;->dateInfo:Lcom/google/api/services/plusi/model/DateInfo;

    :goto_c
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v15, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithDateInfoValues(Landroid/view/View;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;Lcom/google/api/services/plusi/model/DateInfo;)V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_8

    :cond_a
    const/4 v11, 0x0

    goto :goto_9

    :cond_b
    const-string v14, ""

    goto :goto_a

    :cond_c
    const-string v14, ""

    goto :goto_b

    :cond_d
    const/4 v4, 0x0

    goto :goto_c

    :cond_e
    if-le v12, v9, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDeletedFieldView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v12

    :goto_d
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v9

    const/4 v10, 0x0

    if-nez v9, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    move-object/from16 v17, v0

    if-nez v17, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_0

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_10
    const/4 v12, 0x0

    goto :goto_d

    :cond_11
    const/4 v6, 0x0

    :goto_e
    if-ge v6, v9, :cond_14

    if-nez v6, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    const/4 v13, 0x1

    const/4 v10, 0x1

    :goto_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v0, v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    new-instance v16, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v14}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v20}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget v0, v15, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    new-instance v3, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v13}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Z)V

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v3, v2, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->onCheckedChanged(Landroid/widget/CompoundButton;Z)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_e

    :cond_12
    sub-int v7, v6, v10

    if-ge v7, v12, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v14, v17

    :goto_10
    const/4 v13, 0x0

    goto :goto_f

    :cond_13
    const-string v14, ""

    goto :goto_10

    :cond_14
    sub-int v17, v9, v10

    move/from16 v0, v17

    if-le v12, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDeletedFieldView:Landroid/view/View;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-eqz p3, :cond_0

    const-string v1, "audience"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/content/AudienceData;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeChangedField(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v7, Lcom/google/android/apps/plus/R$id;->cancel:I

    if-ne v3, v7, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->onCancel()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v7, Lcom/google/android/apps/plus/R$id;->save:I

    if-ne v3, v7, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeFocus()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    iget v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v7, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createEmployments()Lcom/google/api/services/plusi/model/Employments;

    move-result-object v8

    new-instance v7, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    new-instance v9, Lcom/google/api/services/plusi/model/User;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/User;-><init>()V

    iput-object v9, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iput-object v8, v9, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v8, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createMetadata()Lcom/google/api/services/plusi/model/Metadata;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plusi/model/Employments;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v8, v9, v7}, Lcom/google/android/apps/plus/service/EsService;->mutateProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    sget v7, Lcom/google/android/apps/plus/R$string;->profile_edit_updating:I

    const/4 v8, 0x0

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7, v10}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v8

    const-string v9, "req_pending"

    invoke-virtual {v7, v8, v9}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createEducations()Lcom/google/api/services/plusi/model/Educations;

    move-result-object v8

    new-instance v7, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    new-instance v9, Lcom/google/api/services/plusi/model/User;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/User;-><init>()V

    iput-object v9, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iput-object v8, v9, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v8, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createMetadata()Lcom/google/api/services/plusi/model/Metadata;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plusi/model/Educations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createLocations(Z)Lcom/google/api/services/plusi/model/Locations;

    move-result-object v8

    new-instance v7, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    new-instance v9, Lcom/google/api/services/plusi/model/User;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/User;-><init>()V

    iput-object v9, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iput-object v8, v9, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v8, v7, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createMetadata()Lcom/google/api/services/plusi/model/Metadata;

    move-result-object v9

    iput-object v9, v8, Lcom/google/api/services/plusi/model/Locations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    goto :goto_1

    :cond_2
    sget v7, Lcom/google/android/apps/plus/R$id;->add_item:I

    if-ne v3, v7, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addItem()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v7, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    invoke-static {v2}, Lcom/google/android/apps/plus/util/SoftInput;->show(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_3
    sget v7, Lcom/google/android/apps/plus/R$id;->delete_item:I

    if-ne v3, v7, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v0, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v7, Lcom/google/android/apps/plus/R$string;->profile_edit_item_remove_confirm:I

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v7, Lcom/google/android/apps/plus/R$string;->profile_edit_item_remove:I

    new-instance v8, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$3;

    invoke-direct {v8, p0, v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v7, 0x1040000

    new-instance v8, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$4;

    invoke-direct {v8, p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_4
    sget v7, Lcom/google/android/apps/plus/R$id;->audience:I

    if-ne v3, v7, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeFocus()V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainId:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHasPublicCircle:Z

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;

    move-result-object v1

    invoke-virtual {v1, p0, v10}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "simple_audience"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_3

    const-string v3, "items_json"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    const-string v3, "auto_add"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAutoAddEnabled:Z

    const-string v3, "items"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    const-string v3, "next_name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    const-string v3, "next_title"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    const-string v3, "next_start"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    const-string v3, "next_end"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    const-string v3, "next_current"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "profile_edit_mode"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    const-string v3, "account"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "help_title"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHelpTitle:Ljava/lang/String;

    const-string v3, "help_desc"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHelpDetails:Ljava/lang/String;

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mCircleLoaderHasFinished:Z

    const-string v3, "profile_edit_items_json"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz p1, :cond_4

    const-string v3, "original_items_json"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    :goto_1
    if-nez v1, :cond_0

    if-eqz v2, :cond_e

    :cond_0
    if-eqz v1, :cond_5

    const-string v3, "profile_edit_items_json"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    :cond_1
    const-string v3, "profile_edit_roster_json"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterDataJson:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v3, :pswitch_data_0

    :goto_3
    iput v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalCount:I

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v3, :pswitch_data_1

    move-object v3, v6

    move-object v7, v6

    :goto_4
    if-eqz v7, :cond_2

    iget-object v8, v7, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    if-eqz v8, :cond_2

    iget-object v7, v7, Lcom/google/api/services/plusi/model/Metadata;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/SharingRoster;->requiredScopeId:Lcom/google/api/services/plusi/model/SharingTargetId;

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalRequiredScopeId:Lcom/google/api/services/plusi/model/SharingTargetId;

    :cond_2
    iget v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalCount:I

    if-eqz v3, :cond_d

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    :goto_5
    add-int/2addr v3, v7

    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalCount:I

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsLoaderHasFinished:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_6
    return-void

    :cond_3
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAutoAddEnabled:Z

    goto/16 :goto_0

    :cond_4
    move v2, v5

    goto :goto_1

    :cond_5
    const-string v3, "original_items_json"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    invoke-static {}, Lcom/google/api/services/plusi/model/EmploymentsJson;->getInstance()Lcom/google/api/services/plusi/model/EmploymentsJson;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/google/api/services/plusi/model/EmploymentsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Employments;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/api/services/plusi/model/Employments;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Employments;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    goto :goto_3

    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    invoke-static {}, Lcom/google/api/services/plusi/model/EducationsJson;->getInstance()Lcom/google/api/services/plusi/model/EducationsJson;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/google/api/services/plusi/model/EducationsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Educations;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/google/api/services/plusi/model/Educations;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Educations;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    goto :goto_3

    :pswitch_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {}, Lcom/google/api/services/plusi/model/LocationsJson;->getInstance()Lcom/google/api/services/plusi/model/LocationsJson;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsJson:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/google/api/services/plusi/model/LocationsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Locations;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    goto/16 :goto_3

    :cond_8
    new-instance v3, Lcom/google/api/services/plusi/model/Locations;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Locations;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    goto/16 :goto_3

    :pswitch_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_9

    new-instance v3, Lcom/google/api/services/plusi/model/Employments;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Employments;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    move-object v3, v6

    move-object v7, v6

    goto/16 :goto_4

    :cond_9
    invoke-static {}, Lcom/google/api/services/plusi/model/EmploymentsJson;->getInstance()Lcom/google/api/services/plusi/model/EmploymentsJson;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/google/api/services/plusi/model/EmploymentsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Employments;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/Employments;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Employments;->employment:Ljava/util/List;

    goto/16 :goto_4

    :pswitch_4
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    new-instance v3, Lcom/google/api/services/plusi/model/Educations;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Educations;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    move-object v3, v6

    move-object v7, v6

    goto/16 :goto_4

    :cond_a
    invoke-static {}, Lcom/google/api/services/plusi/model/EducationsJson;->getInstance()Lcom/google/api/services/plusi/model/EducationsJson;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/google/api/services/plusi/model/EducationsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Educations;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/Educations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Educations;->education:Ljava/util/List;

    goto/16 :goto_4

    :pswitch_5
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    new-instance v3, Lcom/google/api/services/plusi/model/Locations;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/Locations;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    move-object v3, v6

    move-object v7, v6

    goto/16 :goto_4

    :cond_b
    invoke-static {}, Lcom/google/api/services/plusi/model/LocationsJson;->getInstance()Lcom/google/api/services/plusi/model/LocationsJson;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalItemsJson:Ljava/lang/String;

    invoke-virtual {v3, v7}, Lcom/google/api/services/plusi/model/LocationsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Locations;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v7, v3, Lcom/google/api/services/plusi/model/Locations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Locations;->currentLocation:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    move v3, v4

    :goto_7
    iput v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalCount:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Locations;->otherLocation:Ljava/util/List;

    goto/16 :goto_4

    :cond_c
    move v3, v5

    goto :goto_7

    :cond_d
    move v3, v5

    goto/16 :goto_5

    :cond_e
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsLoaderHasFinished:Z

    new-instance v3, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;

    invoke-direct {v3, p0, v5}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;B)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemLoaderCallbacks:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemLoaderCallbacks:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;

    invoke-virtual {v3, v4, v6, v5}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v3, 0xe

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "circle_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "circle_name"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "type"

    aput-object v6, v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v6, Lcom/google/android/apps/plus/R$layout;->profile_edit_items:I

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$id;->focus_override:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mFocusOverrideView:Landroid/view/View;

    new-instance v6, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDeletedFieldView:Landroid/view/View;

    sget v6, Lcom/google/android/apps/plus/R$id;->cancel:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v6, Lcom/google/android/apps/plus/R$id;->save:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/ImageTextButton;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSaveButton:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v6, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v6, Lcom/google/android/apps/plus/R$id;->scroller:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ScrollView;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mScollView:Landroid/widget/ScrollView;

    sget v6, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0xb

    if-lt v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v6

    new-instance v7, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$2;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V

    invoke-virtual {v6, v7}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v8, 0x8

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainId:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->hasPublicCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHasPublicCircle:Z

    iget v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v6, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    sget v6, Lcom/google/android/apps/plus/R$id;->add_item:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAddItemView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAddItemView:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAddItemView:Landroid/widget/TextView;

    invoke-virtual {v6, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v6, Lcom/google/android/apps/plus/R$id;->audience:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v6, p0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    sget v7, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setHint(I)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    sget-object v7, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_RIGHT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronDirection(Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mCircleLoaderHasFinished:Z

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setEnabled(Z)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->hideSeparator()V

    if-nez p3, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mFocusOverrideView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_2
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsLoaderHasFinished:Z

    if-eqz v6, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithItemData()V

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHelpTitle:Ljava/lang/String;

    if-eqz v6, :cond_4

    sget v6, Lcom/google/android/apps/plus/R$id;->help_title:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHelpTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v6, Lcom/google/android/apps/plus/R$id;->help_details:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHelpDetails:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v6, Lcom/google/android/apps/plus/R$id;->help:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateView(Landroid/view/View;)V

    return-object v4

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->profile_add_a_job:I

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->profile_add_a_school:I

    goto/16 :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->profile_add_a_place:I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->finishActivity(I)V

    :cond_0
    return-void
.end method

.method public final onDiscard()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->onCancel()V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v2, 0x1

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v1, :pswitch_data_2

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getAudience(Lcom/google/api/services/plusi/model/Metadata;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setEnabled(Z)V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mCircleLoaderHasFinished:Z

    goto :goto_0

    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mHasPublicCircle:Z

    goto :goto_1

    :pswitch_2
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainName:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mDomainId:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employments;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getAudience(Lcom/google/api/services/plusi/model/Metadata;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Employments;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Educations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getAudience(Lcom/google/api/services/plusi/model/Metadata;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Educations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getAudience(Lcom/google/api/services/plusi/model/Metadata;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mLocations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Locations;->metadata:Lcom/google/api/services/plusi/model/Metadata;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfileEditServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->dismissProgressDialog()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->showErrorToast(Lcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->finishActivity(I)V

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "profile_edit_items_json"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v4, "original_items_json"

    iget v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I

    packed-switch v3, :pswitch_data_0

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {p1, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string v3, "items_json"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->createJson()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "auto_add"

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAutoAddEnabled:Z

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_0
    invoke-static {}, Lcom/google/api/services/plusi/model/EmploymentsJson;->getInstance()Lcom/google/api/services/plusi/model/EmploymentsJson;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;

    invoke-virtual {v3, v5}, Lcom/google/api/services/plusi/model/EmploymentsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :pswitch_1
    invoke-static {}, Lcom/google/api/services/plusi/model/EducationsJson;->getInstance()Lcom/google/api/services/plusi/model/EducationsJson;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;

    invoke-virtual {v3, v5}, Lcom/google/api/services/plusi/model/EducationsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    const-string v3, "items"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemViewIdsList:Ljava/util/ArrayList;

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_3
    const-string v3, "next_name"

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextName:I

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "next_title"

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextTitleOrMajor:I

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "next_start"

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextStartDate:I

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "next_end"

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextEndDate:I

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "next_current"

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mViewIdNextCurrent:I

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onSetSimpleAudience(Ljava/lang/String;ILjava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x1

    if-lez p2, :cond_0

    new-instance v9, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v0, p1, p2, p3, v6}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-direct {v9, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/content/AudienceData;->equals(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v0

    if-ne v0, v6, :cond_1

    const-string v0, "v.private"

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v3, 0x0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_edit_item_acl_picker:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x5

    move v7, v6

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    invoke-virtual {p0, v0, v6}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected final updateView(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const v2, 0x1020004

    const/16 v3, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mItemsLoaderHasFinished:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mScollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method
