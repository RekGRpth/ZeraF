.class public final Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "NearbyStreamChangeLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mError:Z

.field private mHasStreamChanged:Z

.field private final mLocation:Lcom/google/android/apps/plus/content/DbLocation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 6

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/CheckNearbyStreamChangeOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/CheckNearbyStreamChangeOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/CheckNearbyStreamChangeOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/CheckNearbyStreamChangeOperation;->hasError()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mError:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mError:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/CheckNearbyStreamChangeOperation;->hasStreamChanged()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mHasStreamChanged:Z

    :cond_0
    return-object v4
.end method

.method public final hasError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mError:Z

    return v0
.end method

.method public final hasStreamChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->mHasStreamChanged:Z

    return v0
.end method
