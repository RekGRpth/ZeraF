.class public Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;
.super Lvedroid/support/v4/app/DialogFragment;
.source "PlusOnePeopleFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment$PeopleSetQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/DialogFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->dismiss()V

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v2

    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "plus_one_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v5, 0x0

    sget v3, Lcom/google/android/apps/plus/R$layout;->list_layout_acl:I

    invoke-virtual {p1, v3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v6}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->ok:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->cancel:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->plus_one_people_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "account"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    sget v3, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    return-object v2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isExtraPeopleViewIndex(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "total_plus_ones"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->setExtraPeopleCount(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
