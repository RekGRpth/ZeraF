.class final Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;
.super Ljava/lang/Object;
.source "ProfileEditFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckboxWatcher"
.end annotation


# instance fields
.field private final mLinkedEditText:Landroid/widget/EditText;

.field private mLinkedEditTextPreviousValue:Ljava/lang/String;

.field private final mLinkedEditTextWatcher:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

.field private final mOriginalCurrent:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/widget/EditText;Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;Z)V
    .locals 0
    .param p2    # Landroid/widget/EditText;
    .param p3    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;
    .param p4    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextWatcher:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    iput-boolean p4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mOriginalCurrent:Z

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextPreviousValue:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeChangedField(Landroid/view/View;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextWatcher:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    if-nez p2, :cond_2

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mOriginalCurrent:Z

    if-ne v1, p2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeChangedField(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    :goto_2
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextPreviousValue:Ljava/lang/String;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextPreviousValue:Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextWatcher:Lcom/google/android/apps/plus/fragments/ProfileEditFragment$EditTextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->mLinkedEditTextPreviousValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$CheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1500(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    goto :goto_2
.end method
