.class final Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
.source "MessageListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/MessageListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RTCServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    return-void
.end method


# virtual methods
.method public final onResponseReceived$1587694a(ILcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadSucceeded()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadFailed()V

    const-string v0, "MessageListFragment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "message load failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final onResponseTimeout(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadFailed()V

    const-string v0, "MessageListFragment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MessageListFragment"

    const-string v1, "message load timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public final onUserTypingStatusChanged(JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    cmp-long v1, v1, p1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mParticipantList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mParticipantList:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    :cond_0
    if-eqz v0, :cond_4

    if-eqz p5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$700(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;Ljava/lang/String;)V

    invoke-virtual {v1, p4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    const-string v1, "MessageListFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "MessageListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Typing status for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->removeCallbacksOnUiThread(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7918

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/util/ThreadUtil;->postDelayedOnUiThread(Ljava/lang/Runnable;J)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$700(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    const-string v1, "MessageListFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "MessageListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Typing status for non existing participant "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversation "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
