.class final Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;
.super Ljava/lang/Object;
.source "PhotoOneUpFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getHeight()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getHeight()I

    move-result v0

    iget v2, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v3, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    neg-int v5, v0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentOffset(I)V

    :cond_0
    return-void
.end method
