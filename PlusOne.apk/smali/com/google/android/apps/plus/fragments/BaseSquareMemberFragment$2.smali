.class final Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;
.super Ljava/lang/Object;
.source "BaseSquareMemberFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x1080027

    invoke-static {p1, p2, v1, v3, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final showErrorToast(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->access$100(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
