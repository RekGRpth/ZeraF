.class final Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;
.super Ljava/lang/Object;
.source "PhotoTileOneUpFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhotoDetailLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
