.class public Lcom/google/android/apps/plus/fragments/PostFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "PostFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;,
        Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

.field private mActivityId:Ljava/lang/String;

.field private mAlbumNameClearButton:Landroid/widget/ImageButton;

.field private mAlbumNameEditText:Landroid/widget/EditText;

.field private final mAlbumNameTextWatcher:Landroid/text/TextWatcher;

.field private mAlbumOwnerId:Ljava/lang/String;

.field private mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

.field private mAttachmentRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mAttachments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

.field private mCircleUsageType:I

.field private mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mContentDeepLinkId:Ljava/lang/String;

.field private mContentDeepLinkMetadata:Landroid/os/Bundle;

.field private mDisableLocation:Z

.field private mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

.field private mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

.field private mEmptyMediaView:Landroid/view/View;

.field private mFilterNullGaiaIds:Z

.field private mFocusOverrideView:Landroid/view/View;

.field private mFooterMessage:Ljava/lang/String;

.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mIsFromPlusOne:Z

.field private mLoadingMediaAttachments:Z

.field private mLoadingUrlPreview:Z

.field private mLoadingView:Landroid/view/View;

.field private mLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mLocationChecked:Z

.field private mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field private final mLocationListener:Landroid/location/LocationListener;

.field private mMediaContainer:Landroid/view/View;

.field private mMediaCount:Landroid/widget/TextView;

.field private mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

.field private mMediaGalleryView:Landroid/view/ViewGroup;

.field private final mMediaRefLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

.field private mOriginalText:Ljava/lang/String;

.field private mPendingPostId:Ljava/lang/Integer;

.field private mPreviewContainerView:Landroid/view/ViewGroup;

.field private mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

.field private mPreviewWrapperView:Landroid/view/ViewGroup;

.field private mProviderLocation:Landroid/location/Location;

.field private mRemoveEmotiShare:Z

.field private mRemoveLocation:Z

.field private mRemoveLocationView:Landroid/view/View;

.field private mRemovePreviewButton:Landroid/view/View;

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mResultMediaItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollView:Landroid/widget/ScrollView;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

.field private mTargetAlbumId:Ljava/lang/String;

.field private final mTextWatcher:Landroid/text/TextWatcher;

.field private mUrl:Ljava/lang/String;

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTextWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameTextWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationListener:Landroid/location/LocationListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$17;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$17;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaRefLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->insertCameraPhoto(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/PostFragment;)Z
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/DbLocation;)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Lcom/google/android/apps/plus/content/DbLocation;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/util/MentionTokenizer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateText(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/widget/ImageButton;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameClearButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PostFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$2002(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Landroid/location/Location;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/PostFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getCityLevelLocationPreference()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/api/CallToActionData;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/fragments/PostFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateViews(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/os/Bundle;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/api/ApiaryActivity;)Lcom/google/android/apps/plus/api/ApiaryActivity;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Lcom/google/android/apps/plus/api/ApiaryActivity;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    return-void
.end method

.method static synthetic access$3700(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumOwnerId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoShareActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    const-string v1, "camera_photos"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumType(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_share:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerInitiallySelected(Ljava/util/ArrayList;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setTakePhoto(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setTakeVideo(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PostFragment;
    .param p1    # Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    return-object v0
.end method

.method private addLocationListener()V
    .locals 8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x1

    const-wide/16 v4, 0xbb8

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationListener:Landroid/location/LocationListener;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    return-void
.end method

.method private addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->add(Lcom/google/android/apps/plus/api/MediaRef;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    return-void
.end method

.method private getCityLevelLocationPreference()Z
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "streams"

    invoke-virtual {v1, v2, v3}, Lvedroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "city_level_sharebox_location"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "city_level_sharebox_location"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const-string v1, "city_level_location"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_0
.end method

.method private getExtrasForLogging()Landroid/os/Bundle;
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    if-nez v0, :cond_1

    move-object v1, v2

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->isFromThirdPartyApp(Landroid/content/Intent;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "extra_platform_event"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v3

    if-lez v3, :cond_3

    const-string v3, "extra_square_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTarget(I)Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->addExtrasForLogging(Landroid/os/Bundle;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v2

    goto :goto_0
.end method

.method private static getLocationFromExtras(Landroid/os/Bundle;)Lcom/google/android/apps/plus/content/DbLocation;
    .locals 15
    .param p0    # Landroid/os/Bundle;

    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v12, :cond_0

    :goto_0
    return-object v12

    :cond_0
    const-string v0, "location_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "cid"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_1
    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v0, "latitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "longitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_0
    const-string v0, "latitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    const-string v0, "longitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v13

    const-wide v0, -0x3fa9800000000000L

    cmpl-double v0, v10, v0

    if-ltz v0, :cond_6

    const-wide v0, 0x4056800000000000L

    cmpg-double v0, v10, v0

    if-gtz v0, :cond_6

    const-wide v0, -0x3f99800000000000L

    cmpl-double v0, v13, v0

    if-ltz v0, :cond_6

    const-wide v0, 0x4066800000000000L

    cmpg-double v0, v13, v0

    if-gtz v0, :cond_6

    const-wide v0, 0x416312d000000000L

    mul-double/2addr v0, v10

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-wide v0, 0x416312d000000000L

    mul-double/2addr v0, v13

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    const/4 v9, 0x1

    :cond_2
    :goto_1
    const-string v0, "cid"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "cid"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x1

    :cond_3
    const-string v0, "location_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "location_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x1

    :cond_4
    const-string v0, "address"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "address"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x1

    :cond_5
    if-eqz v9, :cond_7

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x3

    const-wide/16 v7, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    move-object v12, v0

    goto/16 :goto_0

    :cond_6
    :try_start_1
    const-string v0, "PostFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PostFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "Provided latitude/longitude are out of range. latitude: "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "latitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", longitude: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "longitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PostFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PostFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "Can\'t parse latitude/longitude extras. latitude: "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "latitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", longitude: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "longitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method private getProductName(Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)Ljava/lang/String;
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    return-object v3

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v2, v1, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v4

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 11
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v10, 0x0

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eq v5, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v10, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "req_pending"

    invoke-virtual {v5, v6}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v1, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v5

    instance-of v5, v5, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v5, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v1

    :cond_3
    if-eqz v1, :cond_4

    iget-object v5, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    iget-object v6, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x1080027

    invoke-static {v5, v6, v7, v10, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v5, v6, v10}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->post_create_activity_error:I

    invoke-static {v5, v6, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->isInvitation()Z

    move-result v5

    if-eqz v5, :cond_6

    sget v3, Lcom/google/android/apps/plus/R$string;->square_invite_success:I

    :goto_1
    invoke-static {v0, v3, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-string v6, "streams"

    invoke-virtual {v5, v6, v9}, Lvedroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "want_sharebox_locations"

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v6, "city_level_sharebox_location"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getCityLevelLocationPreference()Z

    move-result v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v5, -0x1

    invoke-virtual {v0, v5}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$string;->share_post_success:I

    goto :goto_1
.end method

.method private handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "PostFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PostFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not retrieve preview: errorCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/16 v1, 0x6e27

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentActivity;->showDialog(I)V

    :cond_2
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$7;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private hasContentDeepLinkMetadata()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private insertCameraPhoto(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateResultMediaItems()V

    :goto_0
    instance-of v1, v7, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v1, :cond_0

    check-cast v7, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v7}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->hideInsertCameraPhotoDialog()V

    :cond_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->camera_photo_error:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v7, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private launchActivity(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFocusOverrideView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFocusOverrideView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    if-nez p2, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private loadLinkPreview()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    new-instance v3, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V

    invoke-virtual {v0, v1, v2, v3}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    return-void
.end method

.method private maybeExtractUrlFromString(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    if-nez v2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v2

    const-class v3, Landroid/text/style/URLSpan;

    invoke-virtual {v1, v4, v2, v3}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v2, v0

    if-lez v2, :cond_0

    aget-object v2, v0, v4

    invoke-virtual {v2}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    goto :goto_0
.end method

.method private removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->remove(Lcom/google/android/apps/plus/api/MediaRef;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumOwnerId:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private removeFromMediaGallery(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private removeLocationListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    return-void
.end method

.method private static setLocationText$681f095b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/view/View;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    const v2, 0x1020016

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-nez p1, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    sget v2, Lcom/google/android/apps/plus/R$id;->centered_text:I

    invoke-virtual {p0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez p2, :cond_1

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private updateLocation(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDisableLocation:Z

    if-eqz v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v8, Lcom/google/android/apps/plus/R$id;->location_progress:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    sget v8, Lcom/google/android/apps/plus/R$id;->location_marker:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    const/4 v7, 0x4

    const/4 v6, 0x4

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v8, :cond_5

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    sget v8, Lcom/google/android/apps/plus/R$drawable;->ic_location_active:I

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbLocation;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/DbLocation;->getBestAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {p1, v4, v10}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText$681f095b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocationView:Landroid/view/View;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocationView:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    sget v8, Lcom/google/android/apps/plus/R$id;->location_marker_progress_container:I

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz v6, :cond_3

    if-nez v7, :cond_7

    :cond_3
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v8, v10}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText$681f095b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const/4 v6, 0x0

    const/4 v8, 0x4

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    sget v8, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v10, v8}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText$681f095b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    sget v8, Lcom/google/android/apps/plus/R$drawable;->ic_location_grey:I

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v8, Lcom/google/android/apps/plus/R$string;->no_location_attached:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v10, v8}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText$681f095b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    const/16 v2, 0x8

    goto :goto_2
.end method

.method private updatePreviewContainer(Landroid/view/View;)V
    .locals 41
    .param p1    # Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v18

    const/16 v30, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/api/ApiaryActivity;->getEmbed(Ljava/lang/String;)Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-result-object v23

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->compose_item_background:I

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    if-eqz v23, :cond_5

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    if-eqz v4, :cond_5

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    if-eqz v4, :cond_5

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    if-eqz v4, :cond_5

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/AppInvite;->attribution:Lcom/google/api/services/plusi/model/Attribution;

    if-nez v4, :cond_4

    const/16 v35, 0x0

    :goto_0
    new-instance v20, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DeepLink;->renderedLabel:Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v35

    invoke-direct {v0, v4, v5, v1}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;-><init>(Lcom/google/api/services/plusi/model/DeepLinkData;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/AppInvite;->about:Lcom/google/api/services/plusi/model/EmbedClientItem;

    move-object/from16 v24, v0

    :goto_1
    if-eqz v24, :cond_0

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    if-eqz v4, :cond_6

    new-instance v21, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/WebPage;)V

    :cond_0
    :goto_2
    if-eqz v22, :cond_b

    new-instance v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;

    move-object/from16 v0, v18

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getAlbum()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getSong()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getPreviewUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v22 .. v22}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;->getMarketUrl()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v33, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->bg_taco_mediapattern:I

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    if-nez v4, :cond_1

    if-eqz v30, :cond_1c

    :cond_1
    const/16 v31, 0x0

    :goto_4
    move/from16 v34, v31

    if-eqz v33, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/16 v34, 0x0

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    move/from16 v0, v34

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;

    move/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewWrapperView:Landroid/view/ViewGroup;

    move/from16 v0, v34

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/16 v27, 0x8

    const/16 v25, 0x8

    if-eqz v34, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v26

    sget v4, Lcom/google/android/apps/plus/R$id;->media_buttons_layout:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    sget v4, Lcom/google/android/apps/plus/R$id;->album_name_layout:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;

    if-eqz v4, :cond_1d

    const/16 v4, 0x8

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const/16 v4, 0x8

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    if-lez v26, :cond_1e

    const/16 v27, 0x0

    :cond_3
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaContainer:Landroid/view/View;

    move/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmptyMediaView:Landroid/view/View;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_4
    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/AppInvite;->attribution:Lcom/google/api/services/plusi/model/Attribution;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/Attribution;->productName:Ljava/lang/String;

    move-object/from16 v35, v0

    goto/16 :goto_0

    :cond_5
    move-object/from16 v24, v23

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->videoObject:Lcom/google/api/services/plusi/model/VideoObject;

    if-eqz v4, :cond_7

    new-instance v21, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->videoObject:Lcom/google/api/services/plusi/model/VideoObject;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/VideoObject;)V

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    if-eqz v4, :cond_8

    new-instance v22, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>(Lcom/google/api/services/plusi/model/PlayMusicAlbum;)V

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicTrack:Lcom/google/api/services/plusi/model/PlayMusicTrack;

    if-eqz v4, :cond_9

    new-instance v22, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->playMusicTrack:Lcom/google/api/services/plusi/model/PlayMusicTrack;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/content/DbEmbedSkyjam;-><init>(Lcom/google/api/services/plusi/model/PlayMusicTrack;)V

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    if-eqz v4, :cond_a

    new-instance v21, Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v0, v24

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;-><init>(Lcom/google/api/services/plusi/model/Thing;)V

    goto/16 :goto_2

    :cond_a
    const/4 v4, 0x6

    const-string v5, "PostFragment"

    const-string v6, "Found an embed we don\'t understand without a THING!"

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    if-eqz v21, :cond_16

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getDescription()Ljava/lang/String;

    move-result-object v13

    if-eqz v20, :cond_f

    const/16 v17, 0x1

    :goto_7
    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getProductName(Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)Ljava/lang/String;

    move-result-object v16

    :goto_8
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getVideoUrl()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v4

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v5

    move-object/from16 v0, v39

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/plus/util/ImageUtils;->rewriteYoutubeMediaUrl(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v39

    move-object/from16 v1, v36

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    move-object/from16 v7, v36

    :cond_c
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    const/4 v3, 0x0

    const/4 v10, 0x3

    :goto_9
    if-eqz v20, :cond_15

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getLabelOrDefault(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    :goto_a
    new-instance v8, Lcom/google/android/apps/plus/views/LinkPreviewView;

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Lcom/google/android/apps/plus/views/LinkPreviewView;-><init>(Landroid/content/Context;)V

    const/4 v11, 0x0

    if-eqz v3, :cond_d

    const/16 v30, 0x1

    new-instance v11, Lcom/google/android/apps/plus/fragments/PostFragment$12;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$12;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    :cond_d
    const/4 v15, 0x0

    move-object v9, v3

    invoke-virtual/range {v8 .. v17}, Lcom/google/android/apps/plus/views/LinkPreviewView;->init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    if-eqz v4, :cond_e

    if-eqz v23, :cond_e

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->webPage:Lcom/google/api/services/plusi/model/WebPage;

    if-eqz v4, :cond_e

    new-instance v4, Lcom/google/android/apps/plus/fragments/PostFragment$13;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment$13;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/app/Activity;)V

    invoke-virtual {v8, v4}, Lcom/google/android/apps/plus/views/LinkPreviewView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$14;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$14;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_e
    move-object/from16 v33, v8

    goto/16 :goto_3

    :cond_f
    const/16 v17, 0x0

    goto/16 :goto_7

    :cond_10
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getVideoUrl()Ljava/lang/String;

    move-result-object v4

    :goto_b
    invoke-static {v4}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->makeLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_8

    :cond_11
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v4

    goto :goto_b

    :cond_12
    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v9

    if-eqz v9, :cond_13

    invoke-static/range {v39 .. v39}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    :goto_c
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getMediaType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v4, v5, :cond_14

    const/4 v10, 0x3

    goto/16 :goto_9

    :cond_13
    const/4 v8, 0x0

    goto :goto_c

    :cond_14
    const/4 v10, 0x2

    goto/16 :goto_9

    :cond_15
    const/4 v14, 0x0

    goto/16 :goto_a

    :cond_16
    const/16 v4, 0x6e27

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Landroid/app/Activity;->showDialog(I)V

    const/16 v33, 0x0

    goto/16 :goto_3

    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v4, :cond_1a

    new-instance v28, Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, v28

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_19

    const/16 v37, 0x0

    :goto_d
    move-object/from16 v0, v28

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    const/4 v4, 0x4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setImageResourceFlags(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getImageRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v4

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    new-instance v4, Lcom/google/android/apps/plus/fragments/PostFragment$15;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment$15;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/app/Activity;)V

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_error_gold_40:I

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setDefaultIcon(I)V

    const/4 v4, 0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setDefaultIconEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewWrapperView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v29

    const/4 v4, -0x1

    move-object/from16 v0, v29

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$dimen;->emotishare_preview_height:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    move-object/from16 v0, v29

    iput v4, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewWrapperView:Landroid/view/ViewGroup;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$16;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v5, v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment$16;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/app/Activity;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_18
    move-object/from16 v33, v28

    goto/16 :goto_3

    :cond_19
    const/16 v37, 0x1

    goto :goto_d

    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v4, :cond_1b

    new-instance v38, Lcom/google/android/apps/plus/views/SquarePreviewView;

    move-object/from16 v0, v38

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/SquarePreviewView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/SquarePreviewView;->init(Lcom/google/android/apps/plus/content/DbEmbedSquare;)V

    move-object/from16 v33, v38

    goto/16 :goto_3

    :cond_1b
    const/16 v33, 0x0

    goto/16 :goto_3

    :cond_1c
    const/16 v31, 0x8

    goto/16 :goto_4

    :cond_1d
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const/4 v4, 0x0

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaCount:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$plurals;->share_photo_count:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    aput-object v40, v9, v15

    move/from16 v0, v26

    invoke-virtual {v5, v6, v0, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_1e
    const/16 v25, 0x0

    goto/16 :goto_6
.end method

.method private updateResultMediaItems()V
    .locals 8

    const/16 v6, 0xfa

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    if-le v2, v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->post_max_photos:I

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_0
.end method

.method private static updateText(Landroid/view/View;)V
    .locals 4
    .param p0    # Landroid/view/View;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$id;->text_marker:I

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_text:I

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_text_grey:I

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_text_active:I

    goto :goto_1
.end method

.method private updateViews(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_1

    sget v3, Lcom/google/android/apps/plus/R$id;->footer_separator:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$id;->footer_message:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateText(Landroid/view/View;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public final canPost()Z
    .locals 10

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-nez v9, :cond_0

    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    if-eqz v9, :cond_1

    :cond_0
    :goto_0
    return v8

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/util/PeopleUtils;->isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v9, :cond_3

    move v5, v7

    :goto_1
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v9, :cond_4

    move v3, v7

    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    move v2, v7

    :goto_3
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Editable;->length()I

    move-result v9

    if-lez v9, :cond_6

    move v1, v7

    :goto_4
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v9, :cond_7

    move v4, v7

    :goto_5
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_8

    move v0, v7

    :goto_6
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v9, :cond_9

    move v6, v7

    :goto_7
    if-nez v5, :cond_2

    if-nez v3, :cond_2

    if-nez v2, :cond_2

    if-nez v1, :cond_2

    if-nez v4, :cond_2

    if-nez v0, :cond_2

    if-eqz v6, :cond_0

    :cond_2
    move v8, v7

    goto :goto_0

    :cond_3
    move v5, v8

    goto :goto_1

    :cond_4
    move v3, v8

    goto :goto_2

    :cond_5
    move v2, v8

    goto :goto_3

    :cond_6
    move v1, v8

    goto :goto_4

    :cond_7
    move v4, v8

    goto :goto_5

    :cond_8
    move v0, v8

    goto :goto_6

    :cond_9
    move v6, v8

    goto :goto_7
.end method

.method public final canPostToSquare()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->isInvitation()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final launchAclPicker()V
    .locals 11

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v1, v2, v10, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Lcom/google/android/apps/plus/R$string;->post_edit_audience_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCircleUsageType:I

    const/4 v5, 0x0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFilterNullGaiaIds:Z

    move v7, v6

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V

    return-void
.end method

.method public final launchSquarePicker()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v4, Lcom/google/android/apps/plus/R$string;->post_edit_audience_activity_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getEditSquareAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V

    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v3, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->isChild()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v2

    :goto_1
    if-eqz v3, :cond_5

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "streams"

    invoke-virtual {v3, v4, v2}, Lvedroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "want_sharebox_locations"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "want_sharebox_locations"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_1

    :cond_4
    const-string v4, "want_locations"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 15
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v12, -0x1

    move/from16 v0, p2

    if-eq v0, v12, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v12, -0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_0

    if-eqz p3, :cond_0

    const-string v12, "insert_photo_request_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v12, "insert_photo_request_id"

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    iput-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    instance-of v12, v1, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v12, :cond_0

    check-cast v1, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    goto :goto_0

    :pswitch_1
    if-eqz p3, :cond_0

    const-string v12, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v12, :cond_0

    const-string v12, "PostFragment"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v2

    array-length v6, v2

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v3, v2, v5

    const-string v12, "PostFragment"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Out circle id: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :pswitch_2
    if-eqz p3, :cond_0

    const-string v12, "location"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-nez v12, :cond_2

    const/4 v12, 0x1

    :goto_2
    iput-boolean v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    goto/16 :goto_0

    :cond_2
    const/4 v12, 0x0

    goto :goto_2

    :pswitch_3
    const-string v12, "android.intent.extra.TEXT"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->maybeExtractUrlFromString(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v12, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->loadLinkPreview()V

    goto/16 :goto_0

    :pswitch_4
    const-string v12, "typed_image_embed"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-nez v12, :cond_3

    const/4 v12, 0x1

    :goto_3
    iput-boolean v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveEmotiShare:Z

    goto/16 :goto_0

    :cond_3
    const/4 v12, 0x0

    goto :goto_3

    :cond_4
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iput-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    const-string v12, "mediarefs"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v9, :cond_0

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :pswitch_5
    const/4 v12, -0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_0

    const-string v12, "photo_remove_from_compose"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    const-string v12, "photo_remove_from_compose"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v10

    array-length v12, v10

    new-array v11, v12, [Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v4, 0x0

    :goto_5
    array-length v12, v10

    if-ge v4, v12, :cond_5

    aget-object v12, v10, v4

    check-cast v12, Lcom/google/android/apps/plus/api/MediaRef;

    aput-object v12, v11, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_5
    invoke-static {v11}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeFromMediaGallery(Ljava/util/List;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v13, 0x5

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v12, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v0, "account"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v0, "disable_location"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDisableLocation:Z

    const-string v0, "circle_usage_type"

    invoke-virtual {v11, v0, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCircleUsageType:I

    if-nez p1, :cond_13

    const-string v0, "external_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "external_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x20

    invoke-static {v2}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDisableLocation:Z

    if-nez v0, :cond_2

    invoke-static {v11}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLocationFromExtras(Landroid/os/Bundle;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->post_fragment_media_ref_loader_id:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaRefLoaderCallbacks:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v2, v1, v3}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    :cond_3
    const-string v0, "target_album_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "target_album_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;

    :cond_4
    const-string v0, "album_owner_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "album_owner_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumOwnerId:Ljava/lang/String;

    :cond_5
    const-string v0, "url"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "url"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    :cond_6
    const-string v0, "content_deep_link_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "content_deep_link_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    :cond_7
    const-string v0, "content_deep_link_metadata"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "content_deep_link_metadata"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    :cond_8
    const-string v0, "call_to_action"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "call_to_action"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/CallToActionData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    :cond_9
    const-string v0, "footer"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "footer"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    :cond_a
    const-string v0, "api_info"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "api_info"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    :goto_0
    const-string v0, "typed_image_embed"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "typed_image_embed"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    :cond_b
    const-string v0, "audience"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    const-string v0, "square_embed"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "square_embed"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    :cond_c
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->maybeExtractUrlFromString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    :cond_d
    const-string v0, "insert_photo_request_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "insert_photo_request_id"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v10

    instance-of v0, v10, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v0, :cond_e

    check-cast v10, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v10}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v0, :cond_12

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->hasContentDeepLinkMetadata()Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "PostFragment"

    invoke-static {v0, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "PostFragment"

    const-string v1, "Mobile deep-link IDs must specify metadata."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v12}, Lvedroid/support/v4/app/FragmentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    :cond_10
    :goto_1
    return-void

    :cond_11
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const-string v3, "com.google.android.apps.social"

    const-string v4, "com.google.android.apps.social"

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    move-object v4, v1

    move-object v5, v2

    move-object v9, v0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    goto/16 :goto_0

    :cond_12
    const-string v0, "is_from_plusone"

    invoke-virtual {v11, v0, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    const-string v0, "filter_null_gaia_ids"

    invoke-virtual {v11, v0, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFilterNullGaiaIds:Z

    goto :goto_1

    :cond_13
    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    :cond_14
    const-string v0, "prov_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "prov_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    :cond_15
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    :cond_16
    const-string v0, "insert_camera_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "insert_camera_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_17
    const-string v0, "preview_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "preview_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryActivity;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    :cond_18
    const-string v0, "emotishare_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "emotishare_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    :cond_19
    const-string v0, "emotishare"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "emotishare"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    :cond_1a
    const-string v0, "api_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "api_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    :cond_1b
    const-string v0, "footer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "footer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    :cond_1c
    const-string v0, "l_attachments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    const-string v0, "loading_attachments"

    invoke-virtual {p1, v0, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    :cond_1d
    const-string v0, "content_deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "content_deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    :cond_1e
    const-string v0, "content_deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const-string v0, "content_deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    :cond_1f
    const-string v0, "call_to_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    const-string v0, "call_to_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/CallToActionData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    :cond_20
    const-string v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    const-string v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    :cond_21
    const-string v0, "is_from_plusone"

    invoke-virtual {p1, v0, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    const-string v0, "filter_nul_gaia_ids"

    invoke-virtual {p1, v0, v12}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFilterNullGaiaIds:Z

    const-string v0, "square_embed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "square_embed"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    goto/16 :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/16 v10, 0x8

    const/4 v9, 0x0

    sget v5, Lcom/google/android/apps/plus/R$layout;->post_fragment:I

    invoke-virtual {p1, v5, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v5, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->photos_gallery:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/plus/R$id;->media_count:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaCount:Landroid/widget/TextView;

    sget v5, Lcom/google/android/apps/plus/R$id;->audience_view:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/AudienceView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v5, Lcom/google/android/apps/plus/R$id;->mention_scroll_view:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ScrollView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mScrollView:Landroid/widget/ScrollView;

    sget v5, Lcom/google/android/apps/plus/R$id;->compose_text:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    sget v5, Lcom/google/android/apps/plus/R$id;->share_preview_container:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_media_container:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmptyMediaView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->photos_container:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaContainer:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->remove_location:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocationView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->focus_override:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFocusOverrideView:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->share_preview_wrapper:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewWrapperView:Landroid/view/ViewGroup;

    sget v5, Lcom/google/android/apps/plus/R$id;->acl_overlay:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/AclDropDown;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    sget v5, Lcom/google/android/apps/plus/R$id;->remove_preview_button:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemovePreviewButton:Landroid/view/View;

    sget v5, Lcom/google/android/apps/plus/R$id;->album_title:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameEditText:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    sget v5, Lcom/google/android/apps/plus/R$id;->clear_text:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameClearButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameClearButton:Landroid/widget/ImageButton;

    new-instance v6, Lcom/google/android/apps/plus/fragments/PostFragment$8;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$8;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mScrollView:Landroid/widget/ScrollView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/ScrollView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-ge v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    :cond_0
    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-direct {v5, p0, v0, v6, v7}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/ViewGroup;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    instance-of v5, v5, Lcom/google/android/apps/plus/phone/ShareActivity;

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$integer;->compose_text_min_lines_big:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setMinLines(I)V

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v5, p0, v6, v7, v8}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Lvedroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    if-nez p3, :cond_2

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v6, Lcom/google/android/apps/plus/fragments/PostFragment$9;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$9;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    new-instance v6, Lcom/google/android/apps/plus/fragments/PostFragment$10;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$10;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/AudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/AudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v6, Lcom/google/android/apps/plus/R$id;->audience_button:I

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v6, Lcom/google/android/apps/plus/R$id;->chevron_icon:I

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    instance-of v5, v5, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    if-eqz v5, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    check-cast v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    sget-object v5, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronDirection(Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->post_open_acl_drop_down:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronContentDescription(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronVisibility(I)V

    :cond_3
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDisableLocation:Z

    if-eqz v5, :cond_4

    sget v5, Lcom/google/android/apps/plus/R$id;->location_container:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    sget v5, Lcom/google/android/apps/plus/R$id;->location_view:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v5, Lcom/google/android/apps/plus/R$id;->choose_media:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_media:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_link:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v5, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v5

    if-eqz v5, :cond_5

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_emotishare:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v9}, Landroid/view/View;->setVisibility(I)V

    sget v5, Lcom/google/android/apps/plus/R$id;->vertical_separator:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaContainer:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocationView:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewWrapperView:Landroid/view/ViewGroup;

    invoke-virtual {v5, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v5, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->loadLinkPreview()V

    :cond_6
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v5, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->hasContentDeepLinkMetadata()Z

    move-result v5

    if-eqz v5, :cond_7

    new-instance v5, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v5}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/api/ApiaryActivityFactory;->getApiaryActivity(Landroid/os/Bundle;Lcom/google/android/apps/plus/api/CallToActionData;)Lcom/google/android/apps/plus/api/ApiaryActivity;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V

    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    if-nez v5, :cond_8

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v5, :cond_9

    :cond_8
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmptyMediaView:Landroid/view/View;

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateViews(Landroid/view/View;)V

    if-nez p3, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "start_editing"

    invoke-virtual {v5, v6, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->requestFocus()Z

    :cond_a
    :goto_1
    return-object v4

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_b
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFocusOverrideView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    goto :goto_1
.end method

.method public final onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v2, "quit"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0
.end method

.method public final onDiscard(Z)V
    .locals 8
    .param p1    # Z

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    if-nez p1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AclDropDown;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    :goto_1
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    :goto_2
    if-eqz v3, :cond_5

    sget v3, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->post_quit_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v4, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "quit"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v3, ""

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    move v3, v4

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v3, :cond_4

    move v3, v4

    goto :goto_2

    :cond_4
    move v3, v5

    goto :goto_2

    :cond_5
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v3, :cond_6

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v1, v3, v0, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_3
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V

    return-void
.end method

.method public final onResume()V
    .locals 9

    const/4 v8, 0x0

    const/4 v5, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onResume()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->insertCameraPhoto(Ljava/lang/String;)V

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_d

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenLocationDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v6, 0x1d71d84

    invoke-virtual {v4, v6}, Lvedroid/support/v4/app/FragmentActivity;->showDialog(I)V

    :cond_3
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-nez v4, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->addLocationListener()V

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    if-eqz v4, :cond_6

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    if-nez v4, :cond_c

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    :cond_6
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-nez v4, :cond_7

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveEmotiShare:Z

    if-eqz v4, :cond_b

    :cond_7
    const/4 v2, 0x0

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveEmotiShare:Z

    if-eqz v4, :cond_e

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveEmotiShare:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_REMOVED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v6

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v0, v4, v5, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_8
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShareText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    :cond_b
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCircleUsageType:I

    invoke-virtual {v4, p0, v5, v6, v7}, Lcom/google/android/apps/plus/views/AclDropDown;->init(Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;Lcom/google/android/apps/plus/views/AudienceView;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$11;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$11;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateResultMediaItems()V

    return-void

    :cond_c
    move v4, v5

    goto/16 :goto_0

    :cond_d
    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    goto/16 :goto_1

    :cond_e
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShareText()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_0

    const-string v0, "location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    if-eqz v0, :cond_1

    const-string v0, "prov_location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v0, :cond_3

    const-string v0, "preview_result"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v0, :cond_4

    const-string v0, "emotishare_result"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShareResult:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v0, :cond_5

    const-string v0, "emotishare"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    if-eqz v0, :cond_6

    const-string v0, "api_info"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    if-eqz v0, :cond_7

    const-string v0, "footer"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    const-string v0, "l_attachments"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_8
    const-string v0, "loading_attachments"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_9

    const-string v0, "url"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    if-eqz v0, :cond_a

    const-string v0, "content_deep_link_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    if-eqz v0, :cond_b

    const-string v0, "content_deep_link_metadata"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eqz v0, :cond_c

    const-string v0, "call_to_action"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    if-eqz v0, :cond_d

    const-string v0, "text"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    const-string v0, "insert_camera_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v0, :cond_f

    const-string v0, "is_from_plusone"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    if-eqz v0, :cond_10

    const-string v0, "square_embed"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_10
    const-string v0, "filter_nul_gaia_ids"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFilterNullGaiaIds:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final post()Z
    .locals 34

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v24

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v27

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v5, :cond_2

    sget-object v23, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_1
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v33

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    move-object/from16 v2, v33

    move-object/from16 v3, v27

    invoke-static {v0, v5, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v31

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PeopleUtils;->isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchAclPicker()V

    const/4 v5, 0x0

    goto :goto_0

    :cond_2
    sget-object v23, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_1

    :cond_3
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    invoke-virtual {v10, v5}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTarget(I)Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_5

    const/4 v5, 0x0

    invoke-virtual {v10, v5}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTarget(I)Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v32

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v6, v7, v8, v9}, Lcom/google/android/apps/plus/phone/Intents;->getSelectSquareCategoryActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v29

    const/4 v5, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->canPost()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->share_body_empty:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, v24

    invoke-static {v0, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    const/4 v5, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v5, 0x0

    sget v6, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "req_pending"

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-interface/range {v31 .. v31}, Landroid/text/Spannable;->length()I

    move-result v5

    if-lez v5, :cond_11

    const/16 v28, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_7
    if-eqz v28, :cond_8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    if-eqz v5, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    if-eqz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_POST_SHAREBOX_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eqz v5, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_POST_SHAREBOX_CALL_TO_ACTION:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-eqz v5, :cond_d

    if-eqz v28, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getShareText()Ljava/lang/String;

    move-result-object v5

    invoke-static {v12, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_12

    sget-object v30, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_TEXT_UNMODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move-object/from16 v2, v33

    move-object/from16 v3, v27

    invoke-static {v0, v5, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v24

    move-object/from16 v1, v33

    move-object/from16 v2, v27

    invoke-static {v0, v5, v6, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_d
    new-instance v4, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v9}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    move-result-object v9

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_e

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v6}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v9

    invoke-static {v6, v7, v8, v9, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_e
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v7

    if-lez v7, :cond_f

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v6}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v9

    invoke-static {v6, v7, v8, v9, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_f
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v7

    if-lez v7, :cond_10

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v6}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v9

    invoke-static {v6, v7, v8, v9, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_10
    invoke-virtual/range {v24 .. v24}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v5, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    invoke-virtual/range {v29 .. v29}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v26

    if-eqz v26, :cond_13

    const-string v5, "birthday_data"

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/api/BirthdayData;

    :goto_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/api/ApiaryActivity;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;

    if-nez v7, :cond_15

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mContentDeepLinkId:Ljava/lang/String;

    const-string v7, "com.google.android.apps.plus.GOOGLE_BIRTHDAY_POST"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    const/16 v16, 0x0

    :goto_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    if-nez v7, :cond_17

    const/16 v18, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSquareEmbed:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-object/from16 v19, v0

    invoke-static/range {v25 .. v25}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_18

    const/16 v20, 0x0

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumOwnerId:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object v7, v4

    invoke-static/range {v5 .. v22}, Lcom/google/android/apps/plus/service/EsService;->postActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Lcom/google/android/apps/plus/api/ApiaryActivity;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ZLcom/google/android/apps/plus/api/BirthdayData;Lcom/google/android/apps/plus/content/DbEmbedEmotishare;Lcom/google/android/apps/plus/content/DbEmbedSquare;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_11
    const/16 v28, 0x0

    goto/16 :goto_3

    :cond_12
    sget-object v30, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_TEXT_MODIFIED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto/16 :goto_4

    :cond_13
    const/16 v17, 0x0

    goto/16 :goto_5

    :cond_14
    const/16 v17, 0x0

    goto/16 :goto_5

    :cond_15
    const/4 v13, 0x0

    goto :goto_6

    :cond_16
    const/16 v16, 0x1

    goto :goto_7

    :cond_17
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbEmotishareMetadata;->getEmbed()Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-result-object v18

    goto :goto_8

    :cond_18
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    goto :goto_9
.end method

.method public final setLocationChecked(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v0, :cond_2

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x1bfb7a8

    invoke-virtual {v1, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->addLocationListener()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenLocationDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x1d71d84

    invoke-virtual {v1, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    goto :goto_0
.end method

.method public final updatePostUI()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/plus/phone/PostActivity;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/apps/plus/phone/PostActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PostActivity;->invalidateMenu()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v1, v0, Lcom/google/android/apps/plus/phone/ShareActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/phone/ShareActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->invalidateMenu()V

    goto :goto_0
.end method
