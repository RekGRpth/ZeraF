.class public Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "HostedEventFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InstantShareConfirmationDialog"
.end annotation


# instance fields
.field private mCheckinButton:Landroid/widget/CheckBox;

.field private mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

.field private mFirstTime:Z

.field private mHasCheckedIn:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->PRIVATE:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    return-void
.end method

.method public constructor <init>(ZLcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;)V
    .locals 1
    .param p1    # Z
    .param p2    # Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->PRIVATE:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mHasCheckedIn:Z

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mCheckinButton:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mFirstTime:Z

    # invokes: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->turnOnInstantShare(ZZ)V
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$2100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ZZ)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1    # Landroid/os/Bundle;

    const/4 v12, 0x1

    const/4 v11, 0x0

    if-eqz p1, :cond_0

    const-string v10, "has_checked_in_id"

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mHasCheckedIn:Z

    const-string v10, "first_time_id"

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mFirstTime:Z

    const-string v10, "dialog_type"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->getDialogContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    sget v10, Lcom/google/android/apps/plus/R$layout;->event_instant_share_dialog_view:I

    const/4 v13, 0x0

    invoke-virtual {v4, v10, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    sget v10, Lcom/google/android/apps/plus/R$id;->checkin:I

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mCheckinButton:Landroid/widget/CheckBox;

    iget-object v13, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mCheckinButton:Landroid/widget/CheckBox;

    iget-boolean v10, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mHasCheckedIn:Z

    if-eqz v10, :cond_3

    move v10, v11

    :goto_0
    invoke-virtual {v13, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    sget v10, Lcom/google/android/apps/plus/R$id;->dialog_content:I

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget-object v10, Lcom/google/android/apps/plus/fragments/HostedEventFragment$12;->$SwitchMap$com$google$android$apps$plus$fragments$HostedEventFragment$DialogType:[I

    iget-object v13, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->ordinal()I

    move-result v13

    aget v10, v10, v13

    packed-switch v10, :pswitch_data_0

    sget v1, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_content:I

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    sget v10, Lcom/google/android/apps/plus/R$id;->link:I

    invoke-virtual {v6, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    sget v10, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_learn_more:I

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v10

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    sget v7, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_positive:I

    const-string v10, "event"

    invoke-virtual {v2, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    const-string v10, "hasUsedInstantShare"

    invoke-interface {v9, v10}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    move v11, v12

    :cond_1
    iput-boolean v11, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mFirstTime:Z

    iget-boolean v10, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mFirstTime:Z

    if-eqz v10, :cond_2

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const-string v11, "hasUsedInstantShare"

    invoke-interface {v10, v11, v12}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget v7, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_positive_first:I

    :cond_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v10, Lcom/google/android/apps/plus/R$string;->event_instant_share_dialog_title:I

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10, v7, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    sget v11, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v10, v11, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    return-object v10

    :cond_3
    const/16 v10, 0x8

    goto :goto_0

    :pswitch_0
    sget v1, Lcom/google/android/apps/plus/R$string;->event_instant_share_on_air_dialog_content:I

    goto :goto_1

    :pswitch_1
    sget v1, Lcom/google/android/apps/plus/R$string;->event_instant_share_public_dialog_content:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "has_checked_in_id"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mHasCheckedIn:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "first_time_id"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mFirstTime:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "dialog_type"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;->mDialogType:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
