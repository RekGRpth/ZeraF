.class public Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "BanPostAuthorDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;
    }
.end annotation


# instance fields
.field private mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

.field private mAlsoRemoveSection:Landroid/view/View;

.field private mAlsoReportCheckbox:Landroid/widget/CheckBox;

.field private mAlsoReportSection:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->updateEnabledState()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "gaia_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "square_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "activity_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method private updateEnabledState()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveSection:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v6, -0x1

    if-ne p2, v6, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v6

    instance-of v6, v6, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;

    if-eqz v6, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "gaia_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "square_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "activity_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v6}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;->onBanPostAuthor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onClick(Landroid/content/DialogInterface;I)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v6

    instance-of v6, v6, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$BanPostAuthorListener;

    goto :goto_0
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->getDialogContext()Landroid/content/Context;

    move-result-object v2

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$layout;->ban_user_dialog:I

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->also_remove_checkbox:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    sget v4, Lcom/google/android/apps/plus/R$id;->also_report_checkbox:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    sget v4, Lcom/google/android/apps/plus/R$id;->also_remove_section:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveSection:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->also_report_section:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportSection:Landroid/view/View;

    if-eqz p1, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    const-string v5, "also_remove"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    const-string v5, "also_report"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->updateEnabledState()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$1;-><init>(Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveSection:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$2;-><init>(Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportSection:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$3;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog$3;-><init>(Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->ban_user_dialog_title:I

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->apply:I

    invoke-virtual {v0, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v0, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v4, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "also_remove"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoRemoveCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "also_report"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BanPostAuthorDialog;->mAlsoReportCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
