.class final Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;
.super Ljava/lang/Object;
.source "BaseStreamSettingsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    sget v2, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_of_posts:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->mVelocityOptions:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "negative"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "velocity"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
