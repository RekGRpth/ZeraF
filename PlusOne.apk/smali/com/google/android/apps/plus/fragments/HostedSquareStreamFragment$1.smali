.class final Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedSquareStreamFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDeclineInvitationComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedSquareStreamFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedSquareStreamFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDeclineInvitationComplete() requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onEditSquareMembershipComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v4, 0x0

    const-string v1, "HostedSquareStreamFrag"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "HostedSquareStreamFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onEditSquareMembershipComplete() requestId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->mOperationType:I
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    sget v2, Lcom/google/android/apps/plus/R$string;->square_blocking_moderator_text:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    sget v3, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v1, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method public final onGetSquareComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v0, "HostedSquareStreamFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedSquareStreamFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGetSquareComplete() requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedSquareStreamFragment;->handleGetSquareServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method
