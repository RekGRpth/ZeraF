.class public Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;
.super Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.source "HostedPostSearchFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# instance fields
.field private mDelayedQuery:Ljava/lang/String;

.field private final mPostsSearchServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mQuery:Ljava/lang/String;

.field private mSearchMode:I

.field private mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mPostsSearchServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchMode:I

    return v0
.end method

.method private createAndRunDbCleanup(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/Runnable;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Runnable;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private doSearch()V
    .locals 4

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mFirstLoad:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->prepareLoaderUri()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->fetchContent(Z)V

    return-void
.end method


# virtual methods
.method protected final createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)Lcom/google/android/apps/plus/phone/StreamAdapter;
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v13, p13

    invoke-super/range {v0 .. v13}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected final fetchContent(Z)V
    .locals 8
    .param p1    # Z

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v4, "extra_search_query"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "extra_search_type"

    const-string v5, "0"

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SEARCHBOX_SELECT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v7

    invoke-static {v4, v5, v6, v7, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :cond_1
    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$2;

    invoke-direct {v3, p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;ZLandroid/app/Activity;)V

    if-eqz p1, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, v1, v4, v3}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->createAndRunDbCleanup(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final loadContent()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    const-string v0, "delayed_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mDelayedQuery:Ljava/lang/String;

    const-string v0, "search_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchMode:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->prepareLoaderUri()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mDelayedQuery:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onDestroy()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->createAndRunDbCleanup(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "query"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->hideSoftInput()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setMarkPostsAsRead(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, p2, v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeStreamCursor(Landroid/database/Cursor;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->checkResetAnimationState()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mEndOfStream:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mPreloadRequested:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mError:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->people_list_error:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->updateSpinner()V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->showContent(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mContinuationToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mEndOfStream:Z

    :goto_2
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mFirstLoad:Z

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mFirstLoad:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->fetchContent(Z)V

    goto :goto_2

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$string;->no_posts:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->showContent(Landroid/view/View;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mPostsSearchServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showSearchView()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    sget v1, Lcom/google/android/apps/plus/R$string;->search_posts_hint_text:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->requestFocus(Z)V

    return-void
.end method

.method public final onQueryClose()V
    .locals 0

    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mResetAnimationState:Z

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->doSearch()V

    return-void
.end method

.method public final onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mPostsSearchServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->loadContent()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mDelayedQuery:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mDelayedQuery:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mDelayedQuery:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->doSearch()V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->loadContent()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "delayed_query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mDelayedQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "search_mode"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method protected final prepareLoaderUri()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "com.google.android.apps.plus.INVALID_SEARCH_QUERY"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mPostsUri:Landroid/net/Uri;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mQuery:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchMode:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/SearchUtils;->getSearchKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mPostsUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public final setSelectionMode(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchMode:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mSearchMode:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->mResetAnimationState:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->doSearch()V

    :cond_0
    return-void
.end method
