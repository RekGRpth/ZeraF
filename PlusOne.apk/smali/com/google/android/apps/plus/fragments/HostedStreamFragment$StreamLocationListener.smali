.class final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;
.super Ljava/lang/Object;
.source "HostedStreamFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StreamLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 5
    .param p1    # Landroid/location/Location;

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-object p1, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prepareLoaderUri()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-boolean v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0, v1, v2, v3}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->fetchContent(Z)V

    goto :goto_0
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
