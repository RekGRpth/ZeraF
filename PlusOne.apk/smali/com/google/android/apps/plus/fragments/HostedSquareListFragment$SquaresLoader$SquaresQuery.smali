.class public interface abstract Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader$SquaresQuery;
.super Ljava/lang/Object;
.source "HostedSquareListFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$Query;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SquaresQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x11

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "square_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "square_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "photo_url"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "post_visibility"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "member_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "unread_count"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "inviter_gaia_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "inviter_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "inviter_photo_url"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "invitation_dismissed"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "is_member"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "suggested"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "row_type"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "header_type"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "square_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedSquareListFragment$SquaresLoader$SquaresQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
