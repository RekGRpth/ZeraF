.class final Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "HostedPeopleFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PeopleAdapter"
.end annotation


# instance fields
.field mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

.field mCircles:Landroid/database/Cursor;

.field mCirclesColumnWidth:I

.field mSuggestions:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Landroid/content/Context;)V
    .locals 5
    .param p2    # Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-direct {p0, p2}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;)V

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$600()Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$600()Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->addPartition(ZZ)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Landroid/content/Context;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
    .param p2    # Landroid/content/Context;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected final bindHeaderView$3ab248f1(Landroid/view/View;I)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-ne p2, v5, :cond_0

    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->find_people_your_circles:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesLeftPadding:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$1100()I

    move-result v2

    add-int/2addr v1, v2

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTopPadding:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$1200()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sYourCirclesTextSize:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$1300()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :cond_0
    return-void
.end method

.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    new-instance v3, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;

    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;-><init>(II)V

    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v4, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;

    const/4 v1, 0x2

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x3

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-direct {v4, v1, v2}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;-><init>(II)V

    :goto_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v2, v5, v6}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mSuggestions:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$1400(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->bind(Lcom/google/api/services/plusi/model/PeopleViewDataResponse;Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;Lcom/google/android/apps/plus/views/PeopleSuggestionsView$CardInfo;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;)V

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    :pswitch_2
    move-object v7, p1

    check-cast v7, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    const/4 v6, 0x0

    invoke-virtual {v7, v1, v2, v5, v6}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->setPadding(IIII)V

    const/4 v5, -0x1

    const/4 v2, -0x1

    const/4 v1, 0x3

    const/4 v6, 0x2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v1, 0x2

    :goto_2
    const/4 v6, 0x1

    invoke-interface {p3, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v1, 0x1

    :goto_3
    const/4 v6, 0x0

    invoke-interface {p3, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_1
    const/4 v2, 0x2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v5, 0x1

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    new-array v8, v1, [I

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v1, :cond_4

    packed-switch v6, :pswitch_data_1

    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :pswitch_3
    const/4 v10, 0x0

    aput v9, v8, v10

    goto :goto_5

    :pswitch_4
    const/4 v10, 0x1

    aput v5, v8, v10

    goto :goto_5

    :pswitch_5
    const/4 v10, 0x2

    aput v2, v8, v10

    goto :goto_5

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCircles:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    invoke-virtual {v7, v1, v8, v2}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;->bind(Landroid/database/Cursor;[ILcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final getCount()I
    .locals 1

    invoke-super {p0}, Lcom/android/common/widget/EsCompositeCursorAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method protected final getItemViewType(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final isEnabled$255f299(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method protected final newHeaderView(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # Landroid/view/ViewGroup;

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/android/common/widget/EsCompositeCursorAdapter;->newHeaderView(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    packed-switch p2, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/CirclesWithTopAvatarsGridLayout;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/plus/R$string;->create_new_circle:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    const/4 v3, 0x0

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTextSize:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$700()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$drawable;->plusone_button:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTopPadding:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$900()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mLayoutInfo:Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$800(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleTopPadding:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$900()I

    move-result v6

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCirclesColumnWidth:I

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sAddCircleHeight:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$1000()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setHostedPeopleData(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->setNotificationsEnabled(Z)V

    if-nez p1, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mSuggestions:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCircles:Landroid/database/Cursor;

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCirclesColumnWidth:I

    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->setNotificationsEnabled(Z)V

    return-void

    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->suggestionsLayoutCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->suggestionsData:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mSuggestions:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    iget-object v0, p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesLayoutCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->celebritiesData:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCelebrities:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sNewCircleCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->access$600()Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-object v0, p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesData:Landroid/database/Cursor;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCircles:Landroid/database/Cursor;

    iget v0, p1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$HostedPeopleData;->circlesColumnWidth:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$PeopleAdapter;->mCirclesColumnWidth:I

    goto :goto_0
.end method
