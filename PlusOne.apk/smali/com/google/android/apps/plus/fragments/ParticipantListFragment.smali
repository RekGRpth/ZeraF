.class public Lcom/google/android/apps/plus/fragments/ParticipantListFragment;
.super Lcom/google/android/apps/plus/fragments/EsListFragment;
.source "ParticipantListFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;,
        Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsListFragment",
        "<",
        "Landroid/widget/ListView;",
        "Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;",
        ">;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# static fields
.field private static sDefaultUserImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mConversationRowId:J

.field private mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

.field private mParticipantsUri:Landroid/net/Uri;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mSelectedCircleIdsResult:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/ParticipantListFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method private addParticipantsToCircle()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->add_to_circle_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0, v4}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mSelectedCircleIdsResult:Ljava/util/ArrayList;

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->addPeopleToCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/util/ParticipantParcelable;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mSelectedCircleIdsResult:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method protected final handleServiceCallback$b5e9bbb(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selected_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mSelectedCircleIdsResult:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->add_to_circle_button:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x1

    move-object v3, v2

    move-object v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v2, "request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v2, "participants"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "participants"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    if-eqz v1, :cond_1

    array-length v2, v1

    if-lez v2, :cond_1

    array-length v2, v1

    new-array v2, v2, [Lcom/google/android/apps/plus/util/ParticipantParcelable;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    aget-object v2, v1, v0

    check-cast v2, Lcom/google/android/apps/plus/util/ParticipantParcelable;

    aput-object v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->sDefaultUserImage:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->sDefaultUserImage:Landroid/graphics/Bitmap;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "conversation_row_id"

    invoke-virtual {v2, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mConversationRowId:J

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v3, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mConversationRowId:J

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantsUri:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mConversationRowId:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_1
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hangout_participants"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Ljava/util/Collection;

    new-instance v0, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/apps/plus/content/SimpleParticipantsLoader;-><init>(Landroid/content/Context;Ljava/util/Collection;[Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    if-ne p1, v5, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantsUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "participant_id!=? AND active=1"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "first_name ASC"

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantsUri:Landroid/net/Uri;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v2, Lcom/google/android/apps/plus/R$layout;->participant_list_fragment:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mListView:Landroid/widget/AbsListView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/widget/AbsListView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v2, Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v2, Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->add_to_circle_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method public final bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic onDestroyView()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDestroyView()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    check-cast p2, Lcom/google/android/apps/plus/views/ParticipantListItemView;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/ParticipantListItemView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v0}, Lvedroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 6
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x2

    const/4 v4, 0x1

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-eq v0, v4, :cond_0

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->showContent(Landroid/view/View;)V

    :cond_1
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/util/ParticipantParcelable;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    const/4 v0, 0x0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/util/ParticipantParcelable;

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/util/ParticipantParcelable;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mSelectedCircleIdsResult:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->addParticipantsToCircle()V

    :cond_4
    return-void

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->handleServiceCallback$b5e9bbb(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mSelectedCircleIdsResult:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->addParticipantsToCircle()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ParticipantListFragment$ParticipantCursorAdapter;->onResume()V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    if-eqz v0, :cond_1

    const-string v0, "participants"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ParticipantListFragment;->mParticipantArray:[Lcom/google/android/apps/plus/util/ParticipantParcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method
