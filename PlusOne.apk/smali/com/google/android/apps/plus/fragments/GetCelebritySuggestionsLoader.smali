.class public final Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;
.super Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;
.source "GetCelebritySuggestionsLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader",
        "<",
        "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mCacheExpiredTimeout:J

.field mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private mPreviewOnly:Z

.field private mSuggestions:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Lvedroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mPreviewOnly:Z

    const-wide/32 v0, 0x7fffffff

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mCacheExpiredTimeout:J

    return-void
.end method

.method private esLoadInBackground()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;
    .locals 19

    const/16 v18, 0x0

    new-instance v15, Landroid/os/ConditionVariable;

    invoke-direct {v15}, Landroid/os/ConditionVariable;-><init>()V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mSuggestions:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    if-nez v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "celebrities.json"

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mCacheExpiredTimeout:J

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/SuggestionsFileCache;->getCachedJson(Landroid/content/Context;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    :try_start_0
    invoke-static {}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-nez v18, :cond_3

    new-instance v1, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v15}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader$1;-><init>(Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;Landroid/os/ConditionVariable;)V

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->startThreaded()V

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getGaiaIdToPackedCircleIdsMap(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v13

    invoke-virtual {v15}, Landroid/os/ConditionVariable;->block()V

    if-nez v18, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->getResponse()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    move-result-object v18

    :cond_1
    if-eqz v18, :cond_7

    if-eqz v1, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "celebrities.json"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/SuggestionsFileCache;->saveJson(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->category:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v8, :cond_5

    invoke-interface {v10, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v12, v11, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPeopleData;->createMembershipsInSuggestedPerson(Ljava/util/List;Ljava/util/HashMap;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :catch_0
    move-exception v2

    const/16 v18, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v15}, Landroid/os/ConditionVariable;->open()V

    goto :goto_1

    :cond_4
    new-instance v18, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mSuggestions:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->category:Ljava/util/List;

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->category:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mSuggestions:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->previewCeleb:Ljava/util/List;

    move-object/from16 v0, v18

    iput-object v2, v0, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->previewCeleb:Ljava/util/List;

    invoke-virtual {v15}, Landroid/os/ConditionVariable;->open()V

    goto :goto_1

    :cond_5
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->previewCeleb:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_7

    move-object/from16 v0, v17

    invoke-static {v0, v13}, Lcom/google/android/apps/plus/content/EsPeopleData;->createMembershipsInSuggestedPerson(Ljava/util/List;Ljava/util/HashMap;)V

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_6
    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    if-eqz v2, :cond_6

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    if-eqz v1, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    :cond_7
    return-object v18
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->isReset()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mSuggestions:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic esLoadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->esLoadInBackground()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final onReset()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->cancelLoad()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mSuggestions:Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserverRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserver:Lvedroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->mObserverRegistered:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->forceLoad()V

    return-void
.end method

.method protected final onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/GetCelebritySuggestionsLoader;->cancelLoad()Z

    return-void
.end method
