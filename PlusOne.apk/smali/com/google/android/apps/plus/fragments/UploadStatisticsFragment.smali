.class public Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "UploadStatisticsFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;,
        Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;,
        Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;,
        Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;,
        Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;,
        Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;"
    }
.end annotation


# instance fields
.field protected mAdapter:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

.field protected mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    return-void
.end method

.method private showProgressDialog(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getSafeContext()Landroid/content/Context;
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->setHasOptionsMenu(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->upload_statistics_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 15
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    const-string v10, "args_action_list"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v10, "iu.UploadStatFragment"

    const-string v11, "No actions for option dialog"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    move/from16 v0, p1

    if-lt v0, v10, :cond_1

    const-string v10, "iu.UploadStatFragment"

    const-string v11, "Option selected outside the action list"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v10, "args_account_name"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v10, "args_media_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "args_media_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string v10, "args_upload_reason"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v10, "args_row_id"

    const-wide/16 v11, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11, v12}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v10, "iu.UploadStatFragment"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "iu.UploadStatFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "  -- on-demand upload; img: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v9, Landroid/content/ContentValues;

    const/4 v10, 0x5

    invoke-direct {v9, v10}, Landroid/content/ContentValues;-><init>(I)V

    const-wide/16 v10, 0x0

    cmp-long v10, v6, v10

    if-ltz v10, :cond_3

    const-string v10, "_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_3
    const-string v10, "upload_account"

    invoke-virtual {v9, v10, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "media_id"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v10, "media_url"

    invoke-virtual {v9, v10, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "upload_reason"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "upload_state"

    const/16 v11, 0x64

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    invoke-virtual {v10, v11, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_1
    sget v10, Lcom/google/android/apps/plus/R$string;->progress_remove_deleted_media:I

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->showProgressDialog(I)V

    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0xb

    if-ge v10, v11, :cond_4

    new-instance v10, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Long;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_4
    new-instance v10, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V

    sget-object v11, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Long;

    const/4 v13, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DeleteStatusTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 15
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    sget v11, Lcom/google/android/apps/plus/R$id;->tag_media_id:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget v11, Lcom/google/android/apps/plus/R$id;->tag_media_url:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget v11, Lcom/google/android/apps/plus/R$id;->tag_upload_reason:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v10

    sget v11, Lcom/google/android/apps/plus/R$id;->tag_row_id:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    const-string v11, "iu.UploadStatFragment"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_0

    const-string v11, "iu.UploadStatFragment"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "  -- item clicked; img: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    new-instance v7, Ljava/util/ArrayList;

    const/4 v11, 0x5

    invoke-direct {v7, v11}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Ljava/util/ArrayList;

    const/4 v11, 0x5

    invoke-direct {v5, v11}, Ljava/util/ArrayList;-><init>(I)V

    sget v11, Lcom/google/android/apps/plus/R$string;->menu_uploadstats_option_upload:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v9, :cond_1

    sget v11, Lcom/google/android/apps/plus/R$string;->menu_uploadstats_option_remove:I

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v6, v11, [Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    sget v11, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_options_title:I

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v11, 0x0

    invoke-virtual {v1, p0, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "args_action_list"

    invoke-virtual {v11, v12, v5}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "args_account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "args_media_url"

    invoke-virtual {v11, v12, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "args_media_id"

    invoke-virtual {v11, v12, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "args_upload_reason"

    invoke-virtual {v11, v12, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz v9, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "args_row_id"

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    invoke-virtual {v11, v12, v13, v14}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v11

    const-string v12, "tag_option_list"

    invoke-virtual {v1, v11, v12}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    const/16 v3, 0xb

    const/4 v1, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v2, Lcom/google/android/apps/plus/R$id;->remove_deleted_media:I

    if-ne v0, v2, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$string;->progress_remove_deleted_media:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->showProgressDialog(I)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v3, :cond_1

    new-instance v2, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V

    new-array v1, v1, [Ljava/lang/Long;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Long;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$DatabaseCleanupTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$id;->rescan_media:I

    if-ne v0, v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$string;->progress_rescan_media:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->showProgressDialog(I)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v3, :cond_3

    new-instance v2, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    sget v0, Lcom/google/android/apps/plus/R$string;->preferences_upload_stats_title:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1    # Landroid/view/Menu;

    const/4 v1, 0x1

    sget v0, Lcom/google/android/apps/plus/R$id;->remove_deleted_media:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v0, Lcom/google/android/apps/plus/R$id;->rescan_media:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void
.end method
