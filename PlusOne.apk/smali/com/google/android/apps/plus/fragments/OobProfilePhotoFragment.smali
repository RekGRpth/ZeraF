.class public Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "OobProfilePhotoFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAddPhotoButton:Landroid/widget/Button;

.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mChangePhotoButton:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mPhotoAdded:Ljava/lang/Boolean;

.field private final mProfileAndContactDataLoader:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mProfilePendingRequestId:Ljava/lang/Integer;

.field private final mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfileAndContactDataLoader:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "pending"

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->updateViews()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;)Lcom/google/android/apps/plus/views/AvatarView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;[B)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;
    .param p1    # [B

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->uploadProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    sget v0, Lcom/google/android/apps/plus/R$string;->setting_profile_photo:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private handlerInsertCameraPhoto$b5e9bbb(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    if-nez v0, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->camera_photo_error:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v3, Lcom/google/android/apps/plus/R$string;->change_photo_crop_title:I

    invoke-virtual {v7, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v2, v3, v0, v8}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method private updateViews()V
    .locals 7

    const/16 v4, 0x8

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v6, v2, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    if-eqz v6, :cond_1

    check-cast v2, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->isLastOobStep()Z

    move-result v2

    :goto_0
    if-eqz v2, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$string;->oob_profile_photo_desc_after_change_tap_done:I

    :goto_1
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mChangePhotoButton:Landroid/widget/Button;

    if-eqz v1, :cond_4

    move v2, v3

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAddPhotoButton:Landroid/widget/Button;

    if-nez v1, :cond_5

    :goto_3
    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    if-eqz v3, :cond_6

    check-cast v2, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getOobActionBar()Lcom/google/android/apps/plus/views/ClientOobActionBar;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->setRightButtonHighlight(Z)V

    :cond_0
    return-void

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$string;->oob_profile_photo_desc_after_change_tap_next:I

    goto :goto_1

    :cond_3
    sget v2, Lcom/google/android/apps/plus/R$string;->oob_profile_photo_desc_before_change:I

    goto :goto_1

    :cond_4
    move v2, v4

    goto :goto_2

    :cond_5
    move v3, v4

    goto :goto_3

    :cond_6
    const/4 v0, 0x0

    goto :goto_4
.end method


# virtual methods
.method public final doPickPhotoFromAlbums(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newAlbumsActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_profile:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setCropMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setHideCameraVideos(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final doRepositionCoverPhoto()V
    .locals 0

    return-void
.end method

.method public final doTakePhoto()V
    .locals 5

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    const-string v2, "camera-profile.jpg"

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentPhoto$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->change_photo_no_camera:I

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "camera-profile.jpg"

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_0

    :pswitch_1
    if-eqz p3, :cond_0

    const-string v2, "data"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$3;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;[B)V

    invoke-static {v2}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mChangePhotoButton:Landroid/widget/Button;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAddPhotoButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_1

    :cond_0
    const-string v1, "change_photo"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    sget v1, Lcom/google/android/apps/plus/R$string;->change_photo_dialog_title:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "change_photo"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p1, :cond_2

    const-string v2, "profile_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "profile_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v2, "photo_changed"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "photo_changed"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;

    :cond_1
    const-string v2, "camera_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "camera_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0x64

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfileAndContactDataLoader:Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v2, v3, v4, v5}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->oob_profile_photo_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->avatar_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->change_photo_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mChangePhotoButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mChangePhotoButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->add_photo_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAddPhotoButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mAddPhotoButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->info_title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mTitleView:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->updateViews()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "profile_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    const-string v0, "photo_changed"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mPhotoAdded:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    const-string v0, "camera_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobProfilePhotoFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    return-void
.end method
