.class final Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "EditAudienceFragment.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditAudienceAdapter"
.end annotation


# instance fields
.field private mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 15
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    move-object/from16 v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$000(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPersonId(Ljava/lang/String;)V

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v9, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$100(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setChecked(Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    if-eqz v10, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncomingAudienceIsReadOnly:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$200(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1

    :pswitch_1
    move-object/from16 v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$000(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPersonId(Ljava/lang/String;)V

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v9, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    const/4 v4, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIds(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$100(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setChecked(Z)V

    if-eqz v10, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncomingAudienceIsReadOnly:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$200(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    const/4 v4, 0x1

    :goto_2
    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setEnabled(Z)V

    invoke-static {v11}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v8

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeader(C)V

    :goto_3
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    goto/16 :goto_0

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    :cond_4
    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v13

    if-eq v13, v8, :cond_5

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeader(C)V

    goto :goto_3

    :cond_5
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    goto :goto_3

    :pswitch_2
    move-object/from16 v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/CircleListItemView;

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v6}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$300(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->setCircle(Ljava/lang/String;ILjava/lang/String;IZ)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$400(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/CircleListItemView;->setChecked(Z)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/CircleListItemView;->updateContentDescription()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final changeCursor(ILandroid/database/Cursor;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/database/Cursor;

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    const/4 v1, 0x1

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;-><init>(Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/common/widget/EsCompositeCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    return-void
.end method

.method protected final getItemViewType(II)I
    .locals 0
    .param p1    # I
    .param p2    # I

    return p1
.end method

.method public final getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final getPositionForSection(I)I
    .locals 3
    .param p1    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getPositionForPartition(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getPositionForSection(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getPositionForPartition(I)I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    sub-int v2, p1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSectionForPosition(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v1

    array-length v2, v1

    add-int/lit8 v2, v2, 0x1

    new-array v0, v2, [Ljava/lang/Object;

    const-string v2, "\u25ef"

    aput-object v2, v0, v4

    const/4 v2, 0x1

    array-length v3, v1

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method protected final newHeaderView(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # Landroid/view/ViewGroup;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$layout;->section_header:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    sget v1, Lcom/google/android/apps/plus/R$string;->edit_audience_header_added:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(I)V

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/google/android/apps/plus/R$string;->edit_audience_header_circles:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v2, 0x1

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnItemCheckedChangeListener(Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCheckBoxVisible(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$000(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/views/CircleListItemView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CircleListItemView;->setOnItemCheckedChangeListener(Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CircleListItemView;->setCheckBoxVisible(Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CircleListItemView;->updateContentDescription()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
