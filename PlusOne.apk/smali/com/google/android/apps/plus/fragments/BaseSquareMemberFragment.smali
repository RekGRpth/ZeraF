.class public abstract Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "BaseSquareMemberFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;
    }
.end annotation


# static fields
.field public static final SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

.field private static sErrorResIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static sLoggingActionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/analytics/OzActions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mErrorHandler:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;

.field protected mJoinability:I

.field protected mMembershipStatus:I

.field private mOperationAction:Ljava/lang/String;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field protected mSquareId:Ljava/lang/String;

.field protected mTotalMemberCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mErrorHandler:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 8
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v7, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "hsmlf_pending"

    invoke-virtual {v3, v4}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    const/16 v4, 0x69

    if-ne v3, v4, :cond_5

    new-instance v0, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->square_error_sole_owner_title:I

    sget v5, Lcom/google/android/apps/plus/R$string;->square_error_sole_owner_stepping_down:I

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;-><init>(Landroid/content/Context;II)V

    :cond_3
    :goto_1
    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mErrorHandler:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;->showErrorDialog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    :goto_2
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v0

    goto :goto_1

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    if-nez v4, :cond_7

    new-instance v4, Ljava/util/HashMap;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    sput-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "PROMOTE_MEMBER_TO_MODERATOR"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_promote_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "PROMOTE_MODERATOR_TO_OWNER"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_promote_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "DEMOTE_MODERATOR_TO_MEMBER"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_demote_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "DEMOTE_OWNER_TO_MEMBER"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_demote_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "DEMOTE_OWNER_TO_MODERATOR"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_demote_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "REMOVE_MEMBER_FROM_SQUARE"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_remove_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "BAN"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_ban_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "REMOVE_BAN"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_unban_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "APPROVE_JOIN_REQUEST"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_approve_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "IGNORE"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_ignore_member_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    const-string v5, "CANCEL_INVITATION"

    sget v6, Lcom/google/android/apps/plus/R$string;->square_cancel_invitation_error:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sErrorResIdMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-nez v3, :cond_8

    sget v3, Lcom/google/android/apps/plus/R$string;->operation_failed:I

    :goto_3
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mErrorHandler:Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;

    invoke-interface {v3, v1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment$ErrorHandler;->showErrorToast(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    goto :goto_3
.end method


# virtual methods
.method public final getExtrasForLogging()Landroid/os/Bundle;
    .locals 2

    const-string v0, "extra_square_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected final isSquareAdmin()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mMembershipStatus:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mMembershipStatus:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "square_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mSquareId:Ljava/lang/String;

    if-eqz p1, :cond_2

    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v2, "operation_action"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "operation_action"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    :cond_1
    const-string v2, "membership_status"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mMembershipStatus:I

    const-string v2, "joinability"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mJoinability:I

    :goto_0
    return-void

    :cond_2
    const-string v2, "square_membership"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mMembershipStatus:I

    const-string v2, "square_joinability"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mJoinability:I

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    const-string v3, "hsmlf_member_moderation"

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "gaia_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "moderation_actions"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p1, v3, :cond_1

    const-string v3, "SquareMembers"

    const-string v4, "Option selected outside the action list"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mSquareId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    invoke-static {v3, v4, v5, v2, v6}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/4 v3, 0x0

    sget v4, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "hsmlf_pending"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    if-nez v4, :cond_2

    new-instance v4, Ljava/util/HashMap;

    const/16 v5, 0xb

    invoke-direct {v4, v5}, Ljava/util/HashMap;-><init>(I)V

    sput-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "PROMOTE_MEMBER_TO_MODERATOR"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_PROMOTE_MEMBER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "PROMOTE_MODERATOR_TO_OWNER"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_PROMOTE_MODERATOR_TO_OWNER:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "DEMOTE_MODERATOR_TO_MEMBER"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_MODERATOR_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "DEMOTE_OWNER_TO_MEMBER"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_OWNER_TO_MEMBER:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "DEMOTE_OWNER_TO_MODERATOR"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_DEMOTE_OWNER_TO_MODERATOR:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "REMOVE_MEMBER_FROM_SQUARE"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_MEMBER_FROM_SQUARE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "BAN"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "REMOVE_BAN"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_REMOVE_BAN:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "APPROVE_JOIN_REQUEST"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_APPROVE_JOIN_REQUEST:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "IGNORE"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_IGNORE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    const-string v5, "CANCEL_INVITATION"

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_CANCEL_INVITATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    sget-object v4, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->sLoggingActionMap:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/OzActions;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v3, v4, v0, v5, v6}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "operation_action"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mOperationAction:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v0, "membership_status"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mMembershipStatus:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "joinability"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mJoinability:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public final onSquareMemberActionClick(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v0, 0x0

    const/4 v4, 0x5

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->isSquareAdmin()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "hsmlf_member_moderation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v4}, Ljava/util/ArrayList;-><init>(I)V

    iget v1, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mMembershipStatus:I

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4, p1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    if-eqz v1, :cond_5

    sget v1, Lcom/google/android/apps/plus/R$string;->square_step_down_to_moderator:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "DEMOTE_OWNER_TO_MODERATOR"

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v1, Lcom/google/android/apps/plus/R$string;->square_step_down_to_member:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "DEMOTE_OWNER_TO_MEMBER"

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-static {p2, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "moderation_actions"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "gaia_id"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    sget v1, Lcom/google/android/apps/plus/R$string;->square_step_down_to_member:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "DEMOTE_MODERATOR_TO_MEMBER"

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    packed-switch p3, :pswitch_data_0

    move v4, v3

    move v5, v3

    :goto_3
    if-eqz v4, :cond_8

    if-ne p3, v2, :cond_7

    if-eqz v1, :cond_8

    :cond_7
    sget v4, Lcom/google/android/apps/plus/R$string;->square_remove_member:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "REMOVE_MEMBER_FROM_SQUARE"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_8
    if-nez v5, :cond_3

    if-ne p3, v2, :cond_9

    if-eqz v1, :cond_3

    :cond_9
    sget v1, Lcom/google/android/apps/plus/R$string;->square_ban_user:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v1, "BAN"

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :pswitch_0
    sget v4, Lcom/google/android/apps/plus/R$string;->square_unban_user:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "REMOVE_BAN"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v3

    move v5, v2

    goto :goto_3

    :pswitch_1
    sget v4, Lcom/google/android/apps/plus/R$string;->square_approve_request:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "APPROVE_JOIN_REQUEST"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v4, Lcom/google/android/apps/plus/R$string;->square_ignore_request:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "IGNORE"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v3

    move v5, v3

    goto :goto_3

    :pswitch_2
    sget v4, Lcom/google/android/apps/plus/R$string;->square_cancel_invitation:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "CANCEL_INVITATION"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v3

    move v5, v3

    goto :goto_3

    :pswitch_3
    if-eqz v1, :cond_a

    sget v4, Lcom/google/android/apps/plus/R$string;->square_demote_to_moderator:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "DEMOTE_OWNER_TO_MODERATOR"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v4, Lcom/google/android/apps/plus/R$string;->square_demote_to_member:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "DEMOTE_OWNER_TO_MEMBER"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :pswitch_4
    if-eqz v1, :cond_a

    sget v4, Lcom/google/android/apps/plus/R$string;->square_promote_to_owner:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "PROMOTE_MODERATOR_TO_OWNER"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v4, Lcom/google/android/apps/plus/R$string;->square_demote_to_member:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "DEMOTE_MODERATOR_TO_MEMBER"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :pswitch_5
    if-eqz v1, :cond_a

    sget v4, Lcom/google/android/apps/plus/R$string;->square_promote_to_moderator:I

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "PROMOTE_MEMBER_TO_MODERATOR"

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    move v4, v2

    move v5, v3

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public final onSquareMemberClick(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v2, v3, v1, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
