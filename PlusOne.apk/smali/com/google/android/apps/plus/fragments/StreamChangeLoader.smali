.class public final Lcom/google/android/apps/plus/fragments/StreamChangeLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "StreamChangeLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mCircleId:Ljava/lang/String;

.field private mError:Z

.field private final mFromWidget:Z

.field private final mGaiaId:Ljava/lang/String;

.field private mHasStreamChanged:Z

.field private final mSquareStreamId:Ljava/lang/String;

.field private final mView:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput p3, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mView:I

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mCircleId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mGaiaId:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mFromWidget:Z

    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mSquareStreamId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 10

    const/4 v8, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mView:I

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mCircleId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mGaiaId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mSquareStreamId:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mFromWidget:Z

    move-object v9, v8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->hasError()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mError:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mError:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/CheckStreamChangeOperation;->hasStreamChanged()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mHasStreamChanged:Z

    :cond_0
    return-object v8
.end method

.method public final hasError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mError:Z

    return v0
.end method

.method public final hasStreamChanged()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->mHasStreamChanged:Z

    return v0
.end method
