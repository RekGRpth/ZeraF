.class public Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "CirclesMultipleSelectFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;
.implements Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;,
        Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/Fragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;",
        "Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

.field private mCircleIdSnapshot:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCircleUsageType:I

.field private mContactLoaded:Z

.field private mListView:Landroid/widget/ListView;

.field private mListener:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

.field private mNewCircleEnabled:Z

.field private mNewCircleIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOldCircleIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mPersonId:Ljava/lang/String;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mThemeContext:Landroid/view/ContextThemeWrapper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private parsePackedCircleIds(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    const/16 v2, 0x7c

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    if-nez v2, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListener:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListener:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

    invoke-interface {v2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;->onCircleSelectionChange()V

    :cond_3
    return-void
.end method


# virtual methods
.method public final getOriginalCircleIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getSelectedCircleIds()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/google/android/apps/plus/R$style;->CircleSelectorList:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mThemeContext:Landroid/view/ContextThemeWrapper;

    new-instance v0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mThemeContext:Landroid/view/ContextThemeWrapper;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;-><init>(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-static {p1}, Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;->needToShow(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "first_add"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;->show(Lvedroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onCirclePropertiesChange(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v6, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    if-eqz v4, :cond_4

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x1

    :cond_2
    :goto_1
    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->toast_circle_already_exists:I

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_1

    :cond_4
    const/4 v4, 0x0

    sget v5, Lcom/google/android/apps/plus/R$string;->new_circle_operation_pending:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "req_pending"

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_5

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v4, v5, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->createCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto/16 :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "new_circles"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    const-string v0, "existing_circle_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleUsageType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPersonId:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v6

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/String;

    const-string v4, "name"

    aput-object v4, v3, v8

    const-string v4, "packed_circle_ids"

    aput-object v4, v3, v7

    const-string v4, "person_id=?"

    new-array v5, v7, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPersonId:Ljava/lang/String;

    aput-object v7, v5, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleUsageType:I

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v8

    const-string v5, "circle_id"

    aput-object v5, v4, v7

    const-string v5, "circle_name"

    aput-object v5, v4, v9

    const/4 v5, 0x3

    const-string v6, "contact_count"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "type"

    aput-object v6, v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mThemeContext:Landroid/view/ContextThemeWrapper;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$layout;->circles_multiple_select_fragment:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleEnabled:Z

    if-eqz v3, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$layout;->circles_item_new_circle:I

    invoke-virtual {v1, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$string;->create_new_circle:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v2
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public final onItemCheckedChanged(Lcom/google/android/apps/plus/views/CheckableListItemView;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/CheckableListItemView;
    .param p2    # Z

    check-cast p1, Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleId()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListener:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListener:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;->onCircleSelectionChange()V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleEnabled:Z

    if-eqz v1, :cond_0

    if-nez p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->newInstance$47e87423()Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "new_circle_input"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    check-cast p2, Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/CircleListItemView;->toggle()V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 6
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPersonId:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mContactLoaded:Z

    if-eqz v0, :cond_e

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_3
    move v0, v2

    move v2, v1

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getView()Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    if-eqz v2, :cond_c

    move v2, v1

    :goto_3
    invoke-virtual {v5, v2}, Landroid/widget/ListView;->setVisibility(I)V

    const v2, 0x1020004

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_d

    :goto_4
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mContactLoaded:Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->parsePackedCircleIds(Ljava/lang/String;)V

    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;->notifyDataSetChanged()V

    goto :goto_1

    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->parsePackedCircleIds(Ljava/lang/String;)V

    goto :goto_5

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$CirclesCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    if-eqz v0, :cond_8

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    if-gez v0, :cond_6

    move v0, v1

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4, v0, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_7
    :goto_6
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    :cond_8
    iget v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleUsageType:I

    const/4 v4, 0x3

    if-ne v0, v4, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsAccountsData;->getStreamViewList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    goto/16 :goto_1

    :cond_9
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_5

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPersonId:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mOldCircleIds:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_2

    :cond_c
    move v2, v3

    goto/16 :goto_3

    :cond_d
    move v1, v3

    goto/16 :goto_4

    :cond_e
    move v0, v1

    move v2, v1

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "new_circles"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleIds:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v0, "existing_circle_ids"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleIdSnapshot:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final setCircleUsageType(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mCircleUsageType:I

    return-void
.end method

.method public final setNewCircleEnabled(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mNewCircleEnabled:Z

    return-void
.end method

.method public final setOnCircleSelectionListener(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mListener:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;

    return-void
.end method

.method public final setPersonId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->mPersonId:Ljava/lang/String;

    return-void
.end method
