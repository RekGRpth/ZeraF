.class public final Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;
.super Ljava/lang/Object;
.source "ProfileEditFragment.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemViewIds"
.end annotation


# instance fields
.field public current:I

.field public endDate:I

.field public name:I

.field public startDate:I

.field public titleOrMajor:I


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->name:I

    iput p2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->titleOrMajor:I

    iput p3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->startDate:I

    iput p4, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->endDate:I

    iput p5, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    return-void
.end method
