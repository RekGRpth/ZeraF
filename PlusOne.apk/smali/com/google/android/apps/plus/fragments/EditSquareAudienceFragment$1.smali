.class final Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "EditSquareAudienceFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGetSquaresComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->mLoaderError:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->access$100(Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    sget v2, Lcom/google/android/apps/plus/R$string;->people_list_error:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->updateSpinner()V

    goto :goto_0
.end method
