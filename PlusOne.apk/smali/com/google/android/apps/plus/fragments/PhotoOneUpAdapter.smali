.class public final Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PhotoOneUpAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/SettableItemAdapter;


# instance fields
.field private mCommentBackgroundPaint:Landroid/graphics/Paint;

.field private mContainerHeight:I

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeights:Landroid/util/SparseIntArray;

.field private mLeftoverPosition:I

.field private mLoading:Z

.field private final mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

.field private final mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mPhotoPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;Landroid/graphics/Paint;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Lcom/google/android/apps/plus/views/OneUpListener;
    .param p4    # Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;
    .param p5    # Landroid/graphics/Paint;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mPhotoPosition:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mCommentBackgroundPaint:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public final addFlaggedComment(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v9, 0x1

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object v8, p1

    check-cast v8, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    const/4 v9, 0x3

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x5

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setOwner(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v9, 0x13

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setCaption(Ljava/lang/String;)V

    const/16 v9, 0xb

    invoke-interface {p3, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_0

    const/16 v9, 0xb

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setDate(J)V

    :cond_0
    const/16 v9, 0x14

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setPlusOne([B)V

    const/4 v9, 0x6

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setAlbum(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->requestLayout()V

    goto :goto_0

    :pswitch_2
    move-object v8, p1

    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    const/4 v9, 0x2

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setCount(I)V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->invalidate()V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->requestLayout()V

    goto :goto_0

    :pswitch_3
    const/4 v9, 0x2

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v9, 0x9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    const/4 v6, 0x0

    if-eqz v1, :cond_1

    :try_start_0
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :cond_1
    :goto_1
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    move-object v8, p1

    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mCommentBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setCustomBackground(Landroid/graphics/Paint;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    const/4 v9, 0x3

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x5

    invoke-interface {p3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setAuthor(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v9, 0x8

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v0, v9, v3}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setComment(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v8, v6}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setPlusOne([B)V

    const/4 v9, 0x6

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setDate(J)V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->requestLayout()V

    goto/16 :goto_0

    :pswitch_4
    sget v9, Lcom/google/android/apps/plus/R$id;->loading_spinner:I

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {p1, v9}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_0

    :pswitch_5
    move-object v8, p1

    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mContainerHeight:I

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v9}, Landroid/util/SparseIntArray;->size()I

    move-result v9

    add-int/lit8 v2, v9, -0x1

    :goto_2
    if-lez v5, :cond_2

    if-ltz v2, :cond_2

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v9, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v9, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v9

    sub-int/2addr v5, v9

    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v8, v5}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->bind(I)V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->invalidate()V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->requestLayout()V

    goto/16 :goto_0

    :catch_0
    move-exception v9

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_1
    sget v2, Lcom/google/android/apps/plus/R$layout;->photo_one_up_info_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_comment_count_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_comment_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_4
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_loading_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_5
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_leftover_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public final removeFlaggedComment(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final setContainerHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mContainerHeight:I

    return-void
.end method

.method public final setFlaggedComments(Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    return-void
.end method

.method public final setItemHeight(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public final setLoading(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLoading:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLoading:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 4
    .param p1    # Landroid/database/Cursor;

    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mPhotoPosition:I

    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mPhotoPosition:I

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    iput v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    goto :goto_1
.end method
