.class final Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;
.super Ljava/lang/Object;
.source "EditAudienceFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$500(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v6

    if-eq v1, v4, :cond_2

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$600(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    if-ne v5, v6, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    array-length v6, v5

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_3

    aget-object v7, v5, v1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    array-length v6, v5

    move v1, v0

    :goto_2
    if-ge v1, v6, :cond_4

    aget-object v7, v5, v1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v3

    array-length v5, v3

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_5

    aget-object v6, v3, v1

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_6

    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method
