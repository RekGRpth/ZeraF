.class public Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedEventListFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/ItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/views/ItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

.field private mDataPresent:Z

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private final mHandler:Landroid/os/Handler;

.field private mInitialLoadDone:Z

.field private final mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mRefreshNeeded:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/HostedEventListFragment;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedEventListFragment;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedEventListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private fetchData()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->getEventHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_1
    return-void
.end method

.method private setCreationVisibility(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->createButton:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->createText:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->LANDING_CREATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCreateEventActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    instance-of v5, p1, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    if-eqz v5, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getEvent()Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, 0x0

    invoke-static {v5, v6, v2, v4, v7}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/R$id;->createButton:I

    if-ne v5, v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Lvedroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getCreateEventActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v1, "events_refresh"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    const-string v1, "events_initialload"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mInitialLoadDone:Z

    const-string v1, "events_datapresent"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v3, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "refresh"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment$EventsLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v0, Lcom/google/android/apps/plus/R$layout;->hosted_events_fragment:I

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v8

    sget v0, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EventCardAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/ColumnGridView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v0, Lcom/google/android/apps/plus/R$string;->no_events:I

    invoke-static {v8, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setupEmptyView(Landroid/view/View;I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->createButton:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/Button;

    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/widget/Button;->setClickable(Z)V

    invoke-virtual {v7, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v8
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventCardAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->changeCursor(Landroid/database/Cursor;)V

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setCreationVisibility(I)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showContent(Landroid/view/View;)V

    :goto_3
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mInitialLoadDone:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->invalidateActionBar()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    const/16 v2, 0x8

    goto :goto_2

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showContent(Landroid/view/View;)V

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->no_events:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->url_param_help_events:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->startExternalActivity(Landroid/content/Intent;)V

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_create_event:I

    sget v2, Lcom/google/android/apps/plus/R$string;->event_button_add_event_label:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    sget v0, Lcom/google/android/apps/plus/R$string;->home_screen_events_label:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->fetchData()V

    :cond_0
    return-void
.end method

.method protected final onResumeContentFetched(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResumeContentFetched(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "events_refresh"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mRefreshNeeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "events_initialload"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mInitialLoadDone:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "events_datapresent"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mDataPresent:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 0
    .param p1    # Landroid/text/style/URLSpan;

    return-void
.end method

.method public final refresh()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setCreationVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->fetchData()V

    return-void
.end method

.method protected final showContent(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    return-void
.end method

.method protected final showEmptyView(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    return-void
.end method
