.class public Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;
.super Lvedroid/support/v4/app/Fragment;
.source "OobContactsSyncFragment.java"


# instance fields
.field private mContactsStatsSyncChoice:Landroid/widget/CheckBox;

.field private mContactsSyncChoice:Landroid/widget/CheckBox;

.field private statsSyncOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public final commit()Z
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "account"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_IMPROVE_CONTACTS_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v1, v0, v2, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordImproveSuggestionsPreferenceChange(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLcom/google/android/apps/plus/analytics/OzViews;)V

    if-eqz v2, :cond_1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->disableWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    :goto_0
    const/4 v4, 0x1

    return v4

    :cond_1
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->enableAndPerformWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v14, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v15, 0xe

    if-ge v14, v15, :cond_1

    const/4 v14, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    sget v14, Lcom/google/android/apps/plus/R$layout;->oob_contacts_sync_fragment:I

    const/4 v15, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v14, v1, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    new-instance v15, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$1;

    invoke-direct/range {v15 .. v16}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    if-nez v14, :cond_0

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_sync_layout:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/View;->setVisibility(I)V

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_sync_checkbox:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v5, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_sync_checkbox_title:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_sync_checkbox_description:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v4, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox_description:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-static {v14}, Lcom/google/android/apps/plus/util/AndroidUtils;->hasTelephony(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_2

    sget v14, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_phone_summary:I

    :goto_1
    invoke-virtual {v10, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox_title:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    invoke-virtual {v14, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v14, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox_link:I

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v14, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_learn_more:I

    invoke-virtual {v10, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v11, Landroid/text/SpannableString;

    invoke-direct {v11, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    sget v15, Lcom/google/android/apps/plus/R$string;->url_param_help_stats_sync:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-static {v14, v6}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v9

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v9, v12}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    return-object v13

    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_0

    :cond_2
    sget v14, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_tablet_summary:I

    goto :goto_1
.end method
