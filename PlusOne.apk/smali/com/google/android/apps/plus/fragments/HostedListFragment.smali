.class public abstract Lcom/google/android/apps/plus/fragments/HostedListFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedListFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "ListViewType:Landroid/widget/AbsListView;",
        "AdapterType:",
        "Lcom/google/android/apps/plus/phone/EsCursorAdapter;",
        ">",
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/widget/AbsListView$OnScrollListener;"
    }
.end annotation


# instance fields
.field protected mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TAdapterType;"
        }
    .end annotation
.end field

.field protected mListView:Landroid/widget/AbsListView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "ListViewType;"
        }
    .end annotation
.end field

.field private mPrevScrollItemCount:I

.field private mPrevScrollPosition:I

.field private mScrollOffset:I

.field private mScrollPos:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mPrevScrollPosition:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mPrevScrollItemCount:I

    return-void
.end method


# virtual methods
.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollPos:I

    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollOffset:I

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollPos:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollOffset:I

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/AbsListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v1, p0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-static {}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onPause()V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onResume()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollPos:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollOffset:I

    :cond_0
    :goto_0
    const-string v0, "scroll_pos"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "scroll_off"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    return-void

    :cond_2
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollOffset:I

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mScrollOffset:I

    goto :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    if-lez p4, :cond_1

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mPrevScrollPosition:I

    if-ne v0, v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mPrevScrollItemCount:I

    :cond_0
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mPrevScrollPosition:I

    iput p4, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mPrevScrollItemCount:I

    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    return-void
.end method

.method protected final resetScrollPosition()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method
