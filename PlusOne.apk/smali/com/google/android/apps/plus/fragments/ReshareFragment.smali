.class public Lcom/google/android/apps/plus/fragments/ReshareFragment;
.super Lcom/google/android/apps/plus/fragments/AudienceFragment;
.source "ReshareFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/ReshareFragment$ServiceListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/AudienceFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

.field private mActivityId:Ljava/lang/String;

.field private mAuthorId:Ljava/lang/String;

.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mReshareInfo:Landroid/widget/TextView;

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mScrollView:Landroid/widget/ScrollView;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private final mTextWatcher:Landroid/text/TextWatcher;

.field private final onClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/ReshareFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mTextWatcher:Landroid/text/TextWatcher;

    new-instance v0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ReshareFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ReshareFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/ReshareFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/ReshareFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/AclDropDown;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/ReshareFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    return-object v0
.end method

.method private getExtrasForLogging()Landroid/os/Bundle;
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v3

    if-lez v3, :cond_0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "extra_square_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTarget(I)Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 8
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "req_pending"

    invoke-virtual {v3, v4}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_3

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_3

    sget v3, Lcom/google/android/apps/plus/R$string;->post_not_sent_title:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->post_restricted_mention_error:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->reshare_post_error:I

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->share_post_success:I

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method private launchActivity(Landroid/content/Intent;I)V
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public final canPostToSquare()Z
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_SQUARES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    return v0
.end method

.method public final launchAclPicker()V
    .locals 11

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v9

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v1, v2, v10, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Lcom/google/android/apps/plus/R$string;->post_edit_audience_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mCircleUsageType:I

    move v7, v6

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->launchActivity(Landroid/content/Intent;I)V

    return-void
.end method

.method public final launchSquarePicker()V
    .locals 6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v4, Lcom/google/android/apps/plus/R$string;->post_edit_audience_activity_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v5

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getEditSquareAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->launchActivity(Landroid/content/Intent;I)V

    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v4, -0x1

    if-eq p2, v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    if-eqz p3, :cond_0

    const-string v4, "audience"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v4, :cond_0

    const-string v4, "ReshareFragment"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    const-string v4, "ReshareFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Out circle id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    const-string v1, "circle_usage_type"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->setCirclesUsageType(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->setIncludePhoneOnlyContacts(Z)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->setIncludePlusPages(Z)V

    if-eqz p1, :cond_0

    const-string v1, "reshare_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "reshare_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v7, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_ACTIVITY:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->reshare_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->reshare_avatar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v1, Lcom/google/android/apps/plus/R$id;->reshare_info:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mReshareInfo:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$id;->acl_overlay:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AclDropDown;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    return-object v0
.end method

.method public final onDestroyView()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onDestroyView()V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    return-void
.end method

.method public final onDiscard()V
    .locals 6

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/apps/plus/R$string;->reshare_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->post_quit_question:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-ne p1, v0, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-static {p1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 7
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/16 v4, 0x14

    const/4 v6, 0x0

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAuthorId:Ljava/lang/String;

    const/16 v3, 0x15

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x16

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v3, v4, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mReshareInfo:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$string;->originally_shared:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 v3, 0x2

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAuthorId:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x4

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAuthorId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/apps/plus/service/EsService;->getActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "reshare_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v2, Lcom/google/android/apps/plus/R$id;->audience_button:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    sget v2, Lcom/google/android/apps/plus/R$id;->chevron_icon:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    instance-of v1, v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    check-cast v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronDirection(Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->post_open_acl_drop_down:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronContentDescription(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronVisibility(I)V

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$id;->mention_scroll_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mScrollView:Landroid/widget/ScrollView;

    sget v1, Lcom/google/android/apps/plus/R$id;->reshare_text:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v1, p0, v2, v5, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Lvedroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setShowPersonNameDialog(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mCircleUsageType:I

    invoke-virtual {v1, p0, v2, v3, v4}, Lcom/google/android/apps/plus/views/AclDropDown;->init(Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;Lcom/google/android/apps/plus/views/AudienceView;Lcom/google/android/apps/plus/content/EsAccount;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v5, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mCircleUsageType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_1
    return-void
.end method

.method public final reshare()Z
    .locals 9

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v7}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PeopleUtils;->isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/AudienceView;->performClick()Z

    :goto_0
    return v6

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v3, v7, v8, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->reshareActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/4 v7, 0x0

    sget v8, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v6}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "req_pending"

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v7, Lcom/google/android/apps/plus/analytics/OzActions;->RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v6, v7, v5, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    const/4 v6, 0x1

    goto :goto_0
.end method

.method protected final setupAudienceClickListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final updatePostUI()V
    .locals 0

    return-void
.end method
