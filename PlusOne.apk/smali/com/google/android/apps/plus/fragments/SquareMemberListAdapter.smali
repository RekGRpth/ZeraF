.class public final Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "SquareMemberListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;
    }
.end annotation


# static fields
.field private static final MESSAGE_PARTITION_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

.field private mContinuationToken:Ljava/lang/String;

.field private mIsSquareAdmin:Z

.field private mLoadMoreListener:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "remaining_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->MESSAGE_PARTITION_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;
    .param p4    # Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->addPartition(ZZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-boolean p2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mIsSquareAdmin:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mLoadMoreListener:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;

    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v8, 0x1

    const/4 v6, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mIsSquareAdmin:Z

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    invoke-virtual {v0, p3, v6, v7}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->init(Landroid/database/Cursor;ZLcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;)V

    goto :goto_0

    :pswitch_1
    const/4 v4, 0x0

    const-string v5, ""

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    packed-switch v7, :pswitch_data_1

    :cond_0
    :goto_1
    sget v7, Lcom/google/android/apps/plus/R$id;->progress_indicator:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-eqz v4, :cond_1

    :goto_2
    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    sget v6, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    const/4 v4, 0x1

    sget v7, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mLoadMoreListener:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mLoadMoreListener:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mContinuationToken:Ljava/lang/String;

    invoke-interface {v7, v8}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;->onLoadMoreMembers(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/R$plurals;->audience_hidden_user_count:I

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v3, v7, v2, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_1
    const/16 v6, 0x8

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final changeSquareMembersCursor(Landroid/database/Cursor;Ljava/lang/String;I)V
    .locals 6
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v4, p1}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->getCount(I)I

    move-result v0

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v2, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->MESSAGE_PARTITION_PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mContinuationToken:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v5, v1}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->changeCursor(ILandroid/database/Cursor;)V

    return-void

    :cond_1
    if-lez p3, :cond_0

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final getItemViewType(II)I
    .locals 0
    .param p1    # I
    .param p2    # I

    return p1
.end method

.method public final getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final getMemberCount()I
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->getCount(I)I

    move-result v0

    return v0
.end method

.method public final isLoading()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    packed-switch p2, :pswitch_data_0

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_member_list_item_view:I

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    sget v1, Lcom/google/android/apps/plus/R$layout;->square_member_list_message_view:I

    invoke-virtual {v0, v1, p5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final setIsSquareAdmin(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->mIsSquareAdmin:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->notifyDataSetChanged()V

    return-void
.end method
