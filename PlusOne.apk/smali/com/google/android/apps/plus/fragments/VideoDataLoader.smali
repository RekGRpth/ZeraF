.class public final Lcom/google/android/apps/plus/fragments/VideoDataLoader;
.super Lvedroid/support/v4/content/CursorLoader;
.source "VideoDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/VideoDataLoader$PhotoQuery;
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mLocalUri:Landroid/net/Uri;

.field private final mPhotoId:J

.field private final mPhotoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLandroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lvedroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mPhotoUrl:Ljava/lang/String;

    iput-wide p4, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mPhotoId:J

    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mLocalUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 12

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mPhotoId:J

    const-wide/16 v10, 0x0

    cmp-long v2, v4, v10

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mPhotoId:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/fragments/VideoDataLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    new-instance v6, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v2, Lcom/google/android/apps/plus/fragments/VideoDataLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v6, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v8, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mPhotoUrl:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;

    move-result-object v8

    :cond_1
    :goto_1
    if-eqz v8, :cond_3

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/api/services/plusi/model/DataVideoJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v9

    :goto_2
    sget-object v2, Lcom/google/android/apps/plus/fragments/VideoDataLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    array-length v2, v2

    new-array v7, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v9, v7, v2

    move-object v2, v6

    check-cast v2, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v2, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mLocalUri:Landroid/net/Uri;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->mLocalUri:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;

    move-result-object v8

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoDataLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
