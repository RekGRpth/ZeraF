.class final Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "EditSquareAudienceFragment.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditAudienceAdapter"
.end annotation


# instance fields
.field private mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->this$0:Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    const/4 v3, 0x1

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/api/ApiUtils;->prependProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x2

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    return-void
.end method

.method public final changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    const/4 v1, 0x2

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;-><init>(Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final getPositionForSection(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getPositionForSection(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getSectionForPosition(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSectionForPosition(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment$EditAudienceAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCheckBoxVisible(Z)V

    # getter for: Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->sDefaultSquareImage:Landroid/graphics/Bitmap;
    invoke-static {}, Lcom/google/android/apps/plus/fragments/EditSquareAudienceFragment;->access$000()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setDefaultAvatar(Landroid/graphics/Bitmap;)V

    return-object v0
.end method
