.class public Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;
.super Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
.source "CircleSettingsFragment.java"


# static fields
.field private static sCircleVelocityOptionValues:[I

.field private static sCircleVelocityOptions:[Ljava/lang/String;


# instance fields
.field private mCircleId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createBaseExtrasForLogging()Landroid/os/Bundle;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "extra_circle_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->mCircleId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "extra_start_view_extras"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected final getVolumeControTypeForLogging()I
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;->CIRCLE:Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;->value()I

    move-result v0

    return v0
.end method

.method protected final getVolumeSettingId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->mCircleId:Ljava/lang/String;

    return-object v0
.end method

.method protected final getVolumeSettingType()Ljava/lang/String;
    .locals 1

    const-string v0, "CIRCLE"

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x3

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    sget-object v1, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->sCircleVelocityOptions:[Ljava/lang/String;

    if-nez v1, :cond_0

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_more:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_standard:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_fewer:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->sCircleVelocityOptions:[Ljava/lang/String;

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->sCircleVelocityOptionValues:[I

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->sCircleVelocityOptions:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->mVelocityOptions:[Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->sCircleVelocityOptionValues:[I

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->mVelocityValues:[I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "circle_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/CircleSettingsFragment;->mCircleId:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x3
        0x2
        0x1
    .end array-data
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->from_this_circle:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->circle_settings_from_this_circle:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->subscription_section_title:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->circle_settings_circle_subscription_section:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->square_membership_section:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    return-object v0
.end method
