.class public Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "CirclePropertiesFragmentDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;
    }
.end annotation


# instance fields
.field private mInputTextView:Landroid/widget/TextView;

.field private mJustFollowingCheckBox:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mJustFollowingCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private checkPositiveButtonEnabled()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private isNewCircle()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "circle_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance$47e87423()Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;-><init>()V

    return-object v0
.end method

.method public static newInstance$50fd8769(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v1, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "circle_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "name"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "just_following"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getTargetFragment()Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->isNewCircle()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mJustFollowingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;->onCirclePropertiesChange(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onClick(Landroid/content/DialogInterface;I)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "circle_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getDialogContext()Landroid/content/Context;

    move-result-object v3

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$layout;->circle_properties_dialog:I

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    invoke-virtual {v5, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    sget v6, Lcom/google/android/apps/plus/R$string;->new_circle_dialog_hint:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setHint(I)V

    sget v5, Lcom/google/android/apps/plus/R$id;->just_following_checkbox:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mJustFollowingCheckBox:Landroid/widget/CheckBox;

    sget v5, Lcom/google/android/apps/plus/R$id;->just_following_layout:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$1;-><init>(Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    const-string v6, "name"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mJustFollowingCheckBox:Landroid/widget/CheckBox;

    const-string v6, "just_following"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    :goto_0
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->isNewCircle()Z

    move-result v5

    if-eqz v5, :cond_2

    sget v5, Lcom/google/android/apps/plus/R$string;->new_circle_dialog_title:I

    :goto_1
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v1, v5, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v5, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v1, v5, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->isNewCircle()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    const-string v6, "name"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mJustFollowingCheckBox:Landroid/widget/CheckBox;

    const-string v6, "just_following"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_2
    sget v5, Lcom/google/android/apps/plus/R$string;->circle_properties_dialog_title:I

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mInputTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string v0, "just_following"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->mJustFollowingCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final onStart()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onStart()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->checkPositiveButtonEnabled()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->checkPositiveButtonEnabled()V

    return-void
.end method
