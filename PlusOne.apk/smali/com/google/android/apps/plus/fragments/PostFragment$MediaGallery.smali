.class final Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;
.super Ljava/lang/Object;
.source "PostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaGallery"
.end annotation


# instance fields
.field private mGalleryView:Landroid/view/ViewGroup;

.field private mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/ViewGroup;)V
    .locals 4
    .param p2    # Landroid/content/Context;
    .param p4    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mImages:Ljava/util/ArrayList;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    const-string v2, "layout_inflater"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mLayoutInflater:Landroid/view/LayoutInflater;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    add-int/lit8 v3, v0, 0x1

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->add(Lcom/google/android/apps/plus/api/MediaRef;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private add(Lcom/google/android/apps/plus/api/MediaRef;I)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I

    const/4 v8, 0x1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mLayoutInflater:Landroid/view/LayoutInflater;

    sget v6, Lcom/google/android/apps/plus/R$layout;->compose_gallery_image_container:I

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    sget v5, Lcom/google/android/apps/plus/R$id;->image:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3700(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2, v8}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setIsAlbumCover(Z)V

    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;)V

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->photo_gallery_content_description:I

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v5, Lcom/google/android/apps/plus/R$id;->remove_image_button:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$2;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$2;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;Lcom/google/android/apps/plus/api/MediaRef;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3900(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View$OnClickListener;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final add(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->add(Lcom/google/android/apps/plus/api/MediaRef;I)V

    return-void
.end method

.method final remove(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mImages:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    sget v7, Lcom/google/android/apps/plus/R$id;->image:I

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    :goto_1
    if-ge v3, v1, :cond_2

    sget v7, Lcom/google/android/apps/plus/R$string;->photo_gallery_content_description:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    add-int/lit8 v10, v3, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v7, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$id;->image:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method
