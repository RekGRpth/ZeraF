.class final Lcom/google/android/apps/plus/fragments/ReshareFragment$2;
.super Ljava/lang/Object;
.source "ReshareFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ReshareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private toggleAclOverlay()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$400(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$400(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$400(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->showAclOverlay()V

    goto :goto_0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->audience_button:I

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->toggleAclOverlay()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->launchSquarePicker()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->launchAclPicker()V

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$id;->chevron_icon:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment$2;->toggleAclOverlay()V

    goto :goto_0
.end method
