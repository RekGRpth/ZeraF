.class final Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;
.super Ljava/lang/Object;
.source "PhotoOneUpFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sActionBarHeight:I
    invoke-static {}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$700()I

    move-result v2

    add-int/2addr v1, v2

    neg-int v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentOffset(I)V

    :cond_0
    return-void
.end method
