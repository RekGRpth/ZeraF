.class public Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedSquareSearchFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# static fields
.field protected static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->showContent(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->onCreate(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->setListener(Lcom/google/android/apps/plus/fragments/SquareSearchAdapter$SquareSearchAdapterListener;)V

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v2, Lcom/google/android/apps/plus/R$layout;->hosted_square_search_fragment:I

    invoke-virtual {p0, p1, p2, p3, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->grid:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    sget-object v2, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->square_card_min_width:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setMinColumnWidth(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v4, v4, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v5, v5, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v6, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v6, v6, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->updateView(Landroid/view/View;)V

    return-object v1
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showSearchView()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->search_squares_hint_text:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    return-void
.end method

.method public final onQueryClose()V
    .locals 0

    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1    # Ljava/lang/CharSequence;

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->setQueryString(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "extra_search_query"

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "extra_search_type"

    const-string v3, "6"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->SEARCHBOX_SELECT:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v2, v3, v4, v5, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public final onSquareSelected(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, v2, v2}, Lcom/google/android/apps/plus/phone/Intents;->getSquareStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->onStart()V

    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareSearchAdapter;->onStop()V

    return-void
.end method
