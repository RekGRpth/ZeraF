.class public Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedStreamFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;
.implements Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;
.implements Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
.implements Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;
.implements Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;
.implements Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;
.implements Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
.implements Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
.implements Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
.implements Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ContinuationTokenQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/EditModerationStateDialog$ModerationListener;",
        "Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;",
        "Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;",
        "Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;",
        "Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;",
        "Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;",
        "Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;",
        "Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;",
        "Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;",
        "Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;"
    }
.end annotation


# static fields
.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;

.field private static sNextPagePreloadTriggerRows:I

.field private static sUsePhotoOneUp:Z


# instance fields
.field protected mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

.field protected mAnimatingPlusOneActivityId:Ljava/lang/String;

.field private mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

.field protected mCircleId:Ljava/lang/String;

.field protected mCircleMemberCount:I

.field protected mCircleName:Ljava/lang/String;

.field protected mCircleRealId:Ljava/lang/String;

.field protected mCircleSettingsCursor:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;

.field protected mCircleVolume:I

.field protected mCirclesCursor:Landroid/database/Cursor;

.field protected mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

.field protected mContinuationToken:Ljava/lang/String;

.field private mCurrentSpinnerPosition:I

.field private mEditModerationStateRequestId:Ljava/lang/Integer;

.field protected mEndOfStream:Z

.field protected mError:Z

.field protected mFirstLoad:Z

.field private mFragmentCreated:Z

.field private mFragmentStartTime:J

.field protected mGaiaId:Ljava/lang/String;

.field protected mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

.field protected mLastClickedPosition:I

.field private mLastDeactivationTime:J

.field private mLastSpinnerPosition:I

.field protected mLocation:Landroid/location/Location;

.field protected mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field protected mLocationDisabledView:Landroid/view/View;

.field protected mLocationSettingsButton:Landroid/widget/Button;

.field protected mNearby:Z

.field private mNextSequencedLoaderId:I

.field private mOptionsMenuIsSubscribeVisible:Z

.field private mOptionsMenuSubscribeIcon:I

.field private mOptionsMenuSubscribeText:I

.field protected mPostsUri:Landroid/net/Uri;

.field protected mPreloadRequested:Z

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentImagesSyncTimestamp:J

.field private mRefreshDisabled:Z

.field private mRequestedModerationState:Ljava/lang/String;

.field protected mResetAnimationState:Z

.field private mScrollOffset:I

.field private mScrollPos:I

.field private mServerErrorRetryButton:Landroid/view/View;

.field private mServerErrorView:Landroid/view/View;

.field protected final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mStreamChangeLastCheckTimeMs:J

.field private mStreamHasChanged:Z

.field protected mStreamHashActivityIdsToRestore:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamLength:I

.field protected mStreamOwnerUserId:Ljava/lang/String;

.field protected mStreamRestorePosition:I

.field protected mSubscribedToCircle:Z

.field protected mView:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const-string v1, "circle_name"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "contact_count"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "semantic_hints"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "volume"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "notifications_enabled"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    sput-boolean v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sUsePhotoOneUp:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamRestorePosition:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Lcom/google/android/apps/plus/service/ServiceResult;ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/util/List;
    .param p5    # Ljava/util/List;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->removePromoCircleDelta(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    new-instance v4, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    invoke-direct {v4, v2, p3, v0}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->getCircleData()Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    const/16 v5, 0x12e

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/content/CircleData;->setType(I)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_2

    invoke-interface {p5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    new-instance v4, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    invoke-direct {v4, v2, p3, v0}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->getCircleData()Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    const/16 v5, 0x12d

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/content/CircleData;->setType(I)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_2
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->applyPromoCircleDeltasToView(Ljava/lang/String;Ljava/util/List;)V

    :cond_3
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;J)J
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateRefreshButton(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->handleOnEditModerationStateCallback(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onShakeAnimFinished()V

    return-void
.end method

.method private addLocationListener(Landroid/location/Location;)V
    .locals 12
    .param p1    # Landroid/location/Location;

    const/4 v11, 0x0

    const/16 v10, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x1

    const-wide/16 v4, 0xbb8

    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;

    invoke-direct {v7, p0, v11}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;B)V

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v9

    sget v0, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeProgressViewMessages()V

    const v0, 0x1020004

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeLocationListener()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->init()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v9, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private addPersonToCircle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 21
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;->needToShow(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v16, Landroid/os/Bundle;

    invoke-direct/range {v16 .. v16}, Landroid/os/Bundle;-><init>()V

    const-string v4, "person_id"

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "person_name"

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "circle_id"

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "circle_name"

    move-object/from16 v0, v16

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "suggestion_id"

    move-object/from16 v0, v16

    move-object/from16 v1, p5

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "activity_id"

    move-object/from16 v0, v16

    move-object/from16 v1, p6

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "first_circle_add"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/plus/fragments/FirstCircleAddDialogFactory;->show(Lvedroid/support/v4/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v8, v4

    const/4 v4, 0x0

    new-array v9, v4, [Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v10, p6

    invoke-static/range {v4 .. v10}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembershipFromPromo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v20

    new-instance v17, Lcom/google/android/apps/plus/content/CircleData;

    const/16 v4, 0x12d

    const/4 v5, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v4, v2, v5}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p6

    move/from16 v1, v20

    move-object/from16 v2, p1

    move-object/from16 v3, v17

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->addPromoPersonCircleDelta(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    move-result-object v18

    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->applyPromoCircleDeltasToView(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v11

    const-string v12, "ACCEPT"

    const-string v15, "ANDROID_STREAM_SUGGESTION_CARD"

    move-object/from16 v13, p1

    move-object/from16 v14, p5

    invoke-static/range {v10 .. v15}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private applyPromoCircleDeltasToView(Ljava/lang/String;Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;",
            ">;)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;

    if-eqz v3, :cond_1

    move-object v1, v2

    check-cast v1, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v1, p2, v3}, Lcom/google/android/apps/plus/views/PeoplePromoCardViewGroup;->applyPromoCircleDeltas(Ljava/util/List;Z)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private editModerationState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->editModerationState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRequestedModerationState:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showProgressDialog(I)V

    const-string v1, "extra_activity_id"

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    const-string v1, "extra_square_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0, p4, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    return-void
.end method

.method private static getViewForLogging(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "v.all.circles"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "v.whatshot"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0

    :cond_1
    const-string v0, "v.nearby"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0

    :cond_2
    const-string v0, "f."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0

    :cond_3
    const-string v0, "g."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0
.end method

.method private handleOnEditModerationStateCallback(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "req_pending"

    invoke-virtual {v5, v6}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lvedroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "APPROVED"

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRequestedModerationState:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$string;->restore_post_error:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v5, "REJECTED"

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRequestedModerationState:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget v3, Lcom/google/android/apps/plus/R$string;->remove_post_error:I

    goto :goto_0

    :cond_3
    sget v3, Lcom/google/android/apps/plus/R$string;->operation_failed:I

    goto :goto_0

    :cond_4
    const-string v5, "APPROVED"

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRequestedModerationState:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz p2, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v5, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setGraySpamApproved(Ljava/lang/String;)V

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v5

    add-int/lit8 v2, v5, -0x1

    :goto_2
    if-ltz v2, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v5, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    if-eqz v5, :cond_7

    move-object v4, v0

    check-cast v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->pokeGraySpamHeader()V

    goto :goto_1

    :cond_6
    if-nez p2, :cond_7

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->pokeGraySpamHeader()V

    :cond_7
    add-int/lit8 v2, v2, -0x1

    goto :goto_2
.end method

.method private initRecentImagesLoader()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method private onShakeAnimFinished()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->plus_one_glass:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneActivityId:Ljava/lang/String;

    return-void
.end method

.method private prefetchContent()V
    .locals 4

    const/4 v3, 0x4

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    const-string v1, "HostedStreamFrag"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "HostedStreamFrag"

    const-string v2, "prefetchContent - mPreloadRequested=true"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "HostedStreamFrag"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "HostedStreamFrag"

    const-string v2, "prefetchContent - posting the runnable returned false!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method

.method private removeLocationListener()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    :cond_0
    return-void
.end method

.method private startContinuationOrCircleSettingsLoader()V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleRealId:Ljava/lang/String;

    if-nez v1, :cond_0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleSettingsCursor:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;

    iput v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    invoke-virtual {v0, v4, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :goto_0
    return-void

    :cond_0
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    invoke-virtual {v0, v3, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method private startStreamOneUp(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .param p2    # Z

    invoke-virtual {p1, p2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getOneUpIntent(Z)Landroid/content/Intent;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v8, -0x1

    iput v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v8

    add-int/lit8 v3, v8, -0x1

    :goto_1
    if-ltz v3, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v8, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p1, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamGridView;->getFirstPosition()I

    move-result v8

    add-int/2addr v8, v3

    iput v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    :cond_2
    const-string v8, "event_id"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "owner_id"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "event_id"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v8, "owner_id"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->UPDATE_EVENT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v10, 0x0

    invoke-static {v8, v9, v0, v5, v10}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v8, "show_keyboard"

    invoke-virtual {v1, v8, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_4
    const-string v8, "photo_ref"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    const-string v8, "photo_ref"

    invoke-virtual {v4, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v7

    const-string v8, "extra_gaia_id"

    invoke-static {v8, v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v8, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_5
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private updateCircleInfo(Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleRealId:Ljava/lang/String;

    const-string v0, "v.all.circles"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleRealId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "v.whatshot"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleRealId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "v.nearby"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleRealId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleRealId:Ljava/lang/String;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getMemberCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleMemberCount:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getVolume()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleVolume:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->isSubscribed()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mSubscribedToCircle:Z

    return-void
.end method

.method private updateEmptyViewProgressText()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    :goto_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v1, Lcom/google/android/apps/plus/R$id;->list_empty_progress_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    goto :goto_0
.end method

.method private updateLocationHeader(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->stream_location_text:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/LocationController;->getFormattedAddress(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$string;->unknown_address:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private updateRefreshButton(Z)V
    .locals 4
    .param p1    # Z

    const-string v1, "HostedStreamFrag"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "HostedStreamFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Stream has changed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHasChanged:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHasChanged:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->updateRefreshButtonIcon(Z)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected final checkResetAnimationState()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->resetAnimationState()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    :cond_0
    return-void
.end method

.method protected createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)Lcom/google/android/apps/plus/phone/StreamAdapter;
    .locals 14
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p3    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Landroid/view/View$OnClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p7    # Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
    .param p8    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    .param p9    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;
    .param p10    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;
    .param p11    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;
    .param p12    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p13    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v13}, Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/StreamGridView;->getColumnCount()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->initCircleSettings(ILcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;)V

    return-object v0
.end method

.method protected doShowEmptyViewProgress(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->doShowEmptyViewProgress(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected fetchContent(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "HostedStreamFrag"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HostedStreamFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "fetchContent: No circles... reloading: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isEmpty()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyStream()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    if-nez v0, :cond_1

    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->fetchStreamContent(Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final fetchStreamContent(Z)Z
    .locals 9
    .param p1    # Z

    const/4 v5, 0x0

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v0, :cond_0

    :goto_0
    return v7

    :cond_0
    if-eqz p1, :cond_2

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    new-instance v3, Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-direct {v3, v7, v4}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILandroid/location/Location;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getNearbyActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    :goto_1
    if-eqz p1, :cond_6

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->getActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    if-nez v0, :cond_4

    goto :goto_0

    :cond_6
    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    goto :goto_2
.end method

.method protected getStreamHeaderCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleSettingsCursor:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;

    return-object v0
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected initCirclesLoader()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method protected isAdapterEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isAdapterEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isLocalDataAvailable(Landroid/database/Cursor;)Z
    .locals 1
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected needsAsyncData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/apps/plus/util/DeepLinkHelper;->handleActivityResult$6eb84b56(II)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    if-eqz p3, :cond_0

    const-string v3, "mediarefs"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "insert_photo_request_id"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "insert_photo_request_id"

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "insert_photo_request_id"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    const-string v3, "account"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForCompose(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onAppLinkClicked$5db46bd0(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    if-eqz p5, :cond_0

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    const-string v1, "HostedStreamFrag"

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    move-object v2, p1

    move-object v3, p2

    move-object v7, p4

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/plus/util/DeepLinkHelper;->launchDeepLink$5724b368(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p1}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final onAsyncData()V
    .locals 5

    const-wide/16 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onAsyncData()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentStartTime:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    instance-of v1, v0, Lcom/google/android/apps/plus/phone/ProfileActivity;

    if-eqz v1, :cond_0

    iput-wide v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentStartTime:J

    :cond_0
    return-void
.end method

.method public final onAvatarClicked(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final onCircleCountClicked()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleName:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getCirclePeopleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationSettingsButton:Landroid/widget/Button;

    if-ne p1, v3, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getLocationSettingActivityIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorRetryButton:Landroid/view/View;

    if-ne p1, v3, :cond_4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    const-string v3, "HostedStreamFrag"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "HostedStreamFrag"

    const-string v4, "onClick - mError=false"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isAdapterEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prefetchContent()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    goto :goto_0

    :cond_4
    instance-of v3, p1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    if-eqz v3, :cond_5

    check-cast p1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_post:I

    if-ne v1, v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getPostTextActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForCompose(Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$id;->compose_photos:I

    if-ne v1, v3, :cond_7

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoShareActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    const-string v4, "camera_photos"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumType(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->photo_picker_album_label_share:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setTakePhoto(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setTakeVideo(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    sget v3, Lcom/google/android/apps/plus/R$id;->compose_location:I

    if-ne v1, v3, :cond_8

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getCheckinActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForCompose(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    sget v3, Lcom/google/android/apps/plus/R$id;->compose_custom:I

    if-ne v1, v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_INSERT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getEmotiShareActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForCompose(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public final onCommentsClicked$40e28b52(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->hasComments()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onContentLinkClicked$599fa128(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedMedia;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/content/DbEmbedMedia;
    .param p6    # Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    invoke-virtual {p5}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v6

    if-eqz p6, :cond_1

    invoke-virtual {p6}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p6}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    const-string v1, "HostedStreamFrag"

    invoke-virtual {p6}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getClientPackageNames()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p6}, Lcom/google/android/apps/plus/content/DbEmbedDeepLink;->getDeepLinkId()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    move-object v2, p1

    move-object v3, p2

    move-object v7, p4

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/plus/util/DeepLinkHelper;->launchDeepLink$5724b368(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, v6, p2, p1}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v2, Lcom/google/android/apps/plus/util/DeepLinkHelper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v5, 0xa

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/apps/plus/util/DeepLinkHelper;-><init>(Lcom/google/android/apps/plus/phone/HostedFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAppLinkHelper:Lcom/google/android/apps/plus/util/DeepLinkHelper;

    if-eqz p1, :cond_4

    const-string v2, "scroll_pos"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    const-string v2, "scroll_off"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    const-string v2, "location"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/location/Location;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    const-string v2, "stream_length"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    const-string v2, "last_deactivation"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastDeactivationTime:J

    const-string v2, "error"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    const-string v2, "reset_animation"

    invoke-virtual {p1, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    const-string v2, "stream_change"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    const-string v2, "stream_change_flag"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHasChanged:Z

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentCreated:Z

    const-string v2, "edit_moderation_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "edit_moderation_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    const-string v2, "edit_moderation_state"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRequestedModerationState:Ljava/lang/String;

    :cond_0
    const-string v2, "subscribe_visible"

    invoke-virtual {p1, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOptionsMenuIsSubscribeVisible:Z

    const-string v2, "subscribe_text"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOptionsMenuSubscribeText:I

    const-string v2, "subscribe_icon"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOptionsMenuSubscribeIcon:I

    const-string v2, "stream_hash_activity_ids"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "stream_hash_activity_ids"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, "stream_restore_position"

    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setStreamHashActivityIds(Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setStreamRestorePosition(I)V

    :cond_1
    :goto_0
    const-string v2, "stream_last_spinner_position"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "stream_last_spinner_position"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastSpinnerPosition:I

    :cond_2
    const-string v2, "stream_next_sequenced_loader_id"

    const/4 v3, 0x4

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prepareLoaderUri()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->initCirclesLoader()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->initRecentImagesLoader()V

    return-void

    :cond_3
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHashActivityIdsToRestore:Ljava/util/ArrayList;

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamRestorePosition:I

    goto :goto_0

    :cond_4
    iput v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    iput v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentStartTime:J

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentCreated:Z

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 18
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x3

    sget-object v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v5, v6}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->AVATAR_ONLY_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    const-string v6, "interaction_sort_key DESC LIMIT 9"

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyStream()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    :cond_0
    const/4 v1, 0x3

    move/from16 v0, p1

    if-ne v0, v1, :cond_2

    const/16 v17, 0x1

    :goto_1
    const-string v7, "sort_index ASC"

    const/4 v1, -0x1

    move/from16 v0, v17

    if-eq v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIMIT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    :cond_1
    const/4 v1, 0x3

    move/from16 v0, p1

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v1, :cond_3

    const/16 v16, 0x1

    :goto_2
    const/4 v1, 0x3

    move/from16 v0, p1

    if-ne v0, v1, :cond_4

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ContinuationTokenQuery;->PROJECTION:[Ljava/lang/String;

    :goto_3
    new-instance v1, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    if-eqz v16, :cond_5

    const-string v5, "has_muted=0"

    :goto_4
    const/4 v6, 0x0

    const/4 v8, 0x4

    move/from16 v0, p1

    if-ne v0, v8, :cond_6

    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    :goto_5
    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    move/from16 v17, v0

    goto :goto_1

    :cond_3
    const/16 v16, 0x0

    goto :goto_2

    :cond_4
    sget-object v4, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_STREAM:[Ljava/lang/String;

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    :pswitch_3
    new-instance v8, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-direct/range {v8 .. v15}, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v1, v8

    goto/16 :goto_0

    :pswitch_4
    new-instance v1, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v5, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-direct {v5, v6, v8}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILandroid/location/Location;)V

    invoke-direct {v1, v2, v3, v5}, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;)V

    goto/16 :goto_0

    :pswitch_5
    new-instance v1, Lcom/google/android/apps/plus/phone/RecentImagesLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/phone/RecentImagesLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 21
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v2, Lcom/google/android/apps/plus/R$layout;->stream:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v19

    sget v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    if-nez v2, :cond_0

    move-object/from16 v0, v19

    iget v2, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v2, :cond_3

    const/4 v2, 0x6

    sput v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    :cond_0
    :goto_0
    sget v2, Lcom/google/android/apps/plus/R$id;->grid:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/StreamGridView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    new-instance v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    iget v3, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    const/4 v4, 0x0

    iget v6, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->paddingWidth:I

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v4, v6, v7}, Lcom/google/android/apps/plus/views/StreamGridView;->setPadding(IIII)V

    iget v3, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->totalColumns:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setColumnCount(I)V

    iget v3, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->separatorWidth:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setItemMargin(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    iget v3, v5, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamHeight:I

    div-int/lit8 v3, v3, 0xa

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setHoleThresholdPx(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->floating_compose_bar:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    sget v2, Lcom/google/android/apps/plus/R$id;->compose_post:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->compose_photos:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->compose_location:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->compose_custom:I

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v2, Lcom/google/android/apps/plus/phone/ComposeBarController;

    const/4 v3, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/apps/plus/phone/ComposeBarController;-><init>(Landroid/view/View;ZLcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v8, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    move-object/from16 v2, p0

    move-object/from16 v7, p0

    move-object/from16 v9, p0

    move-object/from16 v10, p0

    move-object/from16 v11, p0

    move-object/from16 v12, p0

    move-object/from16 v13, p0

    move-object/from16 v15, p0

    invoke-virtual/range {v2 .. v15}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/StreamGridView;Lcom/google/android/apps/plus/util/StreamLayoutInfo;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;Lcom/google/android/apps/plus/phone/ComposeBarController;Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHashActivityIdsToRestore:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHashActivityIdsToRestore:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setStreamHashActivityIds(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamRestorePosition:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setStreamRestorePosition(I)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHashActivityIdsToRestore:Ljava/util/ArrayList;

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamRestorePosition:I

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->no_posts:I

    move-object/from16 v0, v20

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->setupEmptyView(Landroid/view/View;I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->location_off:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->location_off_settings:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationSettingsButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationSettingsButton:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->transient_server_error:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorView:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->error_retry_button:I

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorRetryButton:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorRetryButton:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyStream()Z

    move-result v2

    if-eqz v2, :cond_2

    sget v2, Lcom/google/android/apps/plus/R$string;->no_posts:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    :cond_2
    if-nez p3, :cond_5

    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRecentImagesSyncTimestamp:J

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    return-object v20

    :cond_3
    const/16 v2, 0x8

    sput v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    goto/16 :goto_0

    :cond_4
    const/16 v2, 0x8

    goto/16 :goto_1

    :cond_5
    const-string v2, "recent_images_sync_timestamp"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRecentImagesSyncTimestamp:J

    goto :goto_2
.end method

.method public final onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;)V

    :cond_0
    return-void
.end method

.method public final onDetach()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDetach()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    :cond_2
    return-void
.end method

.method public onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const-string v0, "first_circle_add"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "person_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "circle_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "circle_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "suggestion_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addPersonToCircle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onDismissRecentImages(Z)V
    .locals 4
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRecentImagesSyncTimestamp:J

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveRecentImagesTimestamp(Landroid/content/Context;J)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->initRecentImagesLoader()V

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_DISMISS_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method public final onGraySpamBarClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->setTargetFragment(Lvedroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "hsf_moderation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditModerationStateDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public final onLaunchCircles(I)V
    .locals 8
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getCirclesActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    packed-switch p1, :pswitch_data_1

    :cond_0
    :goto_1
    return-void

    :pswitch_0
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPromoYouMayKnowSuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPromoCelebritySuggestionsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_FIND_FRIENDS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v4, v5, v3, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_3
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_SEE_MORE_YMK_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_2

    :pswitch_4
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->MIXIN_SEE_MORE_SUL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 20
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v10

    packed-switch v10, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-nez p2, :cond_2

    const-string v3, "HostedStreamFrag"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "HostedStreamFrag"

    const-string v4, "populatePrimarySpinner: No circles loaded"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    move-object/from16 v17, v3

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-eq v3, v4, :cond_5

    const/4 v6, 0x1

    :goto_2
    if-nez v6, :cond_7

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    :goto_3
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v7, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_a

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_a

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->clear()V

    :cond_3
    move/from16 v18, v3

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v3, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_9

    const/4 v7, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    move-object/from16 v19, v0

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZI)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_21

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    :goto_6
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    goto/16 :goto_0

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "streams"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lvedroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "circle"

    const-string v5, "v.all.circles"

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v17, v3

    goto/16 :goto_1

    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v5, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v5, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x1

    :goto_7
    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->setVolume(I)I

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->setSubscribed(Z)V

    const/4 v5, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->setMemberCount(I)I

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_22

    :cond_7
    move v3, v6

    goto/16 :goto_4

    :cond_8
    const/4 v5, 0x0

    goto :goto_7

    :cond_9
    const/4 v7, 0x0

    goto/16 :goto_5

    :cond_a
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateCircleInfo(Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startContinuationOrCircleSettingsLoader()V

    goto/16 :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    if-lt v3, v10, :cond_0

    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleMemberCount:I

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mSubscribedToCircle:Z

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v5, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;-><init>(IZLandroid/database/Cursor;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleSettingsCursor:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v3, v4, v5, v0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    if-lt v3, v10, :cond_0

    if-eqz p2, :cond_b

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_b

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    :goto_8
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v3, v4, v5, v0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto/16 :goto_0

    :cond_b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    goto :goto_8

    :pswitch_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    if-lt v3, v10, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getStreamHeaderCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeStreamHeaderCursor(Landroid/database/Cursor;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    packed-switch v3, :pswitch_data_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    if-eqz v3, :cond_11

    const-string v5, "com.google.android.apps.plus.search_key-"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    const/4 v3, 0x1

    :goto_9
    if-eqz v3, :cond_12

    const/4 v3, 0x0

    :goto_a
    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setMarkPostsAsRead(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeStreamCursor(Landroid/database/Cursor;I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isPaused()Z

    move-result v3

    if-nez v3, :cond_c

    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->checkResetAnimationState()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    if-nez v3, :cond_14

    :cond_d
    :goto_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v11

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    const-string v3, "HostedStreamFrag"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "HostedStreamFrag"

    const-string v4, "onLoadFinished - mPreloadRequested=false"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v3, :cond_17

    if-nez v11, :cond_17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->people_list_error:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    const/4 v9, 0x0

    :goto_c
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    const-string v3, "HostedStreamFrag"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v3, "HostedStreamFrag"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLoadFinished - mEndOfStream="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    if-eqz v14, :cond_10

    if-nez v11, :cond_10

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    :cond_10
    if-eqz v9, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onAsyncData()V

    goto/16 :goto_0

    :pswitch_4
    const/4 v3, 0x0

    goto/16 :goto_a

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_9

    :cond_12
    const/4 v3, 0x1

    goto/16 :goto_a

    :cond_13
    const/4 v3, 0x1

    goto/16 :goto_a

    :cond_14
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v4

    if-lt v3, v4, :cond_15

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto/16 :goto_b

    :cond_15
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    if-nez v3, :cond_16

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    if-eqz v3, :cond_d

    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/views/StreamGridView;->setFirstPositionAndOffsets(II)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto/16 :goto_b

    :cond_17
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isLocalDataAvailable(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showContent(Landroid/view/View;)V

    const/4 v9, 0x1

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToLast()Z

    const/16 v3, 0x19

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_18

    const/4 v3, 0x1

    :goto_d
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    goto/16 :goto_c

    :cond_18
    const/4 v3, 0x0

    goto :goto_d

    :cond_19
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    if-eqz v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "no_location_stream_key"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1a

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->fetchContent(Z)V

    :cond_1a
    const/4 v9, 0x0

    goto/16 :goto_c

    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v3, :cond_1c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v3, :cond_1d

    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->no_posts:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    :cond_1d
    const/4 v9, 0x1

    goto/16 :goto_c

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    sub-long v3, v12, v3

    const-wide/16 v5, 0x7530

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    move-object/from16 v16, p1

    check-cast v16, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->hasError()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/fragments/StreamChangeLoader;->hasStreamChanged()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateRefreshButton(Z)V

    goto/16 :goto_0

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    sub-long v3, v12, v3

    const-wide/16 v5, 0x7530

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    move-object/from16 v16, p1

    check-cast v16, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->hasError()Z

    move-result v3

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/fragments/NearbyStreamChangeLoader;->hasStreamChanged()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateRefreshButton(Z)V

    goto/16 :goto_0

    :pswitch_7
    if-eqz p2, :cond_0

    new-instance v15, Ljava/util/ArrayList;

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v15, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRecentImagesSyncTimestamp:J

    :cond_1e
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_20

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_e
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_1e

    :cond_1f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-virtual {v3, v15}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setRecentImageRefs(Ljava/util/ArrayList;)V

    goto/16 :goto_0

    :cond_20
    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_e

    :cond_21
    move/from16 v3, v18

    goto/16 :goto_6

    :cond_22
    move v3, v4

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onMediaClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;ZLcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p4    # Z
    .param p5    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .param p6    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz p4, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_2

    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    if-nez p5, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v5, v4, v0}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p5}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-boolean v5, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sUsePhotoOneUp:Z

    if-eqz v5, :cond_3

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setRefreshAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setDisableComments(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setIsStreamPost(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    const-string v5, "extra_gaia_id"

    invoke-static {v5, p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v5, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    invoke-direct {p0, p5, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->logPeopleSuggestionEvents()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeLocationListener()V

    return-void
.end method

.method public final onPersonAdded(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-static {v7, v0, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultCircleId(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v7, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultCircleName(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addPersonToCircle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onPersonClicked(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    const-string v2, "CLICK"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "ANDROID_STREAM_SUGGESTION_CARD"

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onPlusOneAnimFinished()V
    .locals 7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isPaused()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneActivityId:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v5, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v5, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    if-eqz v5, :cond_3

    move-object v4, v0

    check-cast v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneActivityId:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->togglePlusOne()V

    const/4 v1, 0x1

    :cond_2
    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onShakeAnimFinished()V

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public final onPlusOneClicked(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Lcom/google/android/apps/plus/views/UpdateCardViewGroup;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/DbPlusOneData;
    .param p3    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneActivityId:Ljava/lang/String;

    if-nez v4, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->isPostPlusOnePending(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/service/EsService;->deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    invoke-virtual {p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->togglePlusOne()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v3

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneActivityId:Ljava/lang/String;

    sget v4, Lcom/google/android/apps/plus/R$id;->plus_one_animator:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getPlusOneButtonAnimationCopies()Landroid/util/Pair;

    move-result-object v1

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v5, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p0, v4, v5}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->startPlusOneAnim(Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;Lcom/google/android/apps/plus/views/ClickableButton;Lcom/google/android/apps/plus/views/ClickableButton;)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->startDelayedShakeAnimation()V

    sget v4, Lcom/google/android/apps/plus/R$id;->plus_one_glass:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    const-wide/16 v6, 0x393

    invoke-virtual {v4, v5, v6, v7}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method protected onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHasChanged:Z

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->updateRefreshButtonIcon(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    return-void
.end method

.method public onPrimarySpinnerSelectionChange(I)V
    .locals 11
    .param p1    # I

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    if-eq v0, p1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    :goto_0
    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->clearNavigationAction()V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    :cond_0
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getCircleId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getCircleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleName:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getView()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    const/4 v4, 0x2

    if-ne v0, v4, :cond_6

    move v0, v9

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateEmptyViewProgressText()V

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateCircleInfo(Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;)V

    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v8

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_7

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addLocationListener(Landroid/location/Location;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prepareLoaderUri()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "circle_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "circle_name"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleName:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "view"

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v3, "streams"

    invoke-virtual {v0, v3, v10}, Lvedroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "circle"

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-lt v0, v4, :cond_8

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_3
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastSpinnerPosition:I

    if-eq v0, v3, :cond_2

    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    iput v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    iput v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    :cond_2
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastSpinnerPosition:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarController:Lcom/google/android/apps/plus/phone/ComposeBarController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->forceShow()V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startContinuationOrCircleSettingsLoader()V

    return-void

    :cond_5
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    goto/16 :goto_0

    :cond_6
    move v0, v10

    goto/16 :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeLocationListener()V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    sget v0, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_8
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_3
.end method

.method public final onRemoveActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v0, "REJECTED"

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_REMOVE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->editModerationState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method public final onReshareClicked(Ljava/lang/String;Z)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz p2, :cond_0

    const/16 v2, 0x9

    :goto_0
    invoke-static {v3, v4, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getReshareActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    if-eqz p2, :cond_1

    sget v2, Lcom/google/android/apps/plus/R$string;->reshare_dialog_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->reshare_dialog_message:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->reshare_dialog_positive_button:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/apps/plus/fragments/ConfirmIntentDialog;->newInstance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)Lvedroid/support/v4/app/DialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "confirm_reshare"

    invoke-virtual {v0, v2, v3}, Lvedroid/support/v4/app/DialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/4 v2, 0x5

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public final onRestoreActivity(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v0, "APPROVED"

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->GREY_SPAM_RESTORE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->editModerationState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method public onResume()V
    .locals 10

    const/4 v2, 0x0

    const/4 v9, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    const/4 v0, 0x1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prefetchContent()V

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v3, :cond_1

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addLocationListener(Landroid/location/Location;)V

    :cond_1
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v3, :cond_2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateRefreshButton(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    const-string v3, "HostedStreamFrag"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "HostedStreamFrag"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onResume mFragmentCreated: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentCreated:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mNewerReqId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", gaia id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", time diff (ms): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    sub-long/2addr v5, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentCreated:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v3, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x7530

    cmp-long v3, v3, v5

    if-lez v3, :cond_4

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {v3, v4, v9, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_4
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    invoke-direct {p0, v1, v9}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->handleOnEditModerationStateCallback(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    :cond_5
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4, v9, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_6
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentCreated:Z

    return-void

    :cond_7
    move v0, v2

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4, v9, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    goto :goto_1
.end method

.method protected final onResumeContentFetched(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/StreamGridView;->getFirstPosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/StreamGridView;->getItemMargin()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v1, v2, v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    :cond_0
    :goto_0
    const-string v1, "scroll_pos"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "scroll_off"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v1, :cond_2

    const-string v1, "location"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    const-string v1, "stream_length"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "last_deactivation"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastDeactivationTime:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "error"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "reset_animation"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "stream_change"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamChangeLastCheckTimeMs:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "stream_change_flag"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamHasChanged:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "recent_images_sync_timestamp"

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRecentImagesSyncTimestamp:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v1, "subscribe_visible"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOptionsMenuIsSubscribeVisible:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "subscribe_text"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOptionsMenuSubscribeText:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "subscribe_icon"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOptionsMenuSubscribeIcon:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, "edit_moderation_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEditModerationStateRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "edit_moderation_state"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRequestedModerationState:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-eqz v1, :cond_4

    const-string v1, "stream_hash_activity_ids"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getStreamHashActivityIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/StreamGridView;->getFirstPosition()I

    move-result v0

    :goto_1
    const-string v1, "stream_restore_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    const-string v1, "stream_last_spinner_position"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastSpinnerPosition:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "stream_next_sequenced_loader_id"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNextSequencedLoaderId:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_5
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto/16 :goto_0

    :cond_6
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto/16 :goto_0

    :cond_7
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastClickedPosition:I

    goto :goto_1
.end method

.method protected onSetArguments(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v1, "gaia_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamOwnerUserId:Ljava/lang/String;

    :goto_0
    const-string v1, "circle_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    const-string v1, "circle_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleName:Ljava/lang/String;

    const-string v1, "view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    :goto_1
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateEmptyViewProgressText()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamOwnerUserId:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    goto :goto_1
.end method

.method public onSettingsClicked()V
    .locals 8

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v5, v6}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getVolume()I

    move-result v5

    if-eqz v5, :cond_0

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getVolume()I

    move-result v4

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->isSubscribed()Z

    move-result v1

    new-instance v2, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;-><init>(ZIZ)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    invoke-static {v5, v6, v7, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCircleSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment$Settings;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    const/4 v4, 0x2

    goto :goto_1
.end method

.method public final onShareRecentImages(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v0, v2, p1}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForCompose(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SHARE_INSTANT_UPLOAD_PHOTOS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v4

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method public final onViewUsed(I)V
    .locals 2
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prefetchContent()V

    goto :goto_0
.end method

.method protected prepareLoaderUri()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "no_location_stream_key"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILandroid/location/Location;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    invoke-static {v2, v3, v0, v5, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method public refresh()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    if-eqz v2, :cond_1

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addLocationListener(Landroid/location/Location;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    iput v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamRestorePosition:I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeStreamHeaderCursor(Landroid/database/Cursor;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeStreamCursor(Landroid/database/Cursor;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v6, v4}, Lcom/google/android/apps/plus/phone/StreamAdapter;->triggerStreamObservers(ZI)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->finding_your_location:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->fetchContent(Z)V

    goto :goto_0
.end method

.method public final shouldPersistStateToHost()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final showContent(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected showEmptyStream()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_empty_stream"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected final showEmptyView(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->stream_location_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected final showEmptyViewProgress(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyViewProgress(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected final showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected final showProgressDialog(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method protected startActivityForCompose(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected startStreamOneUp(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "refresh"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected final updateServerErrorView()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorView:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
