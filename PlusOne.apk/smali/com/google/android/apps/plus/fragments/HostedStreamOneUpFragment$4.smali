.class final Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;
.super Ljava/lang/Object;
.source "HostedStreamOneUpFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->show(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;->val$view:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
