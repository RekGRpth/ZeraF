.class public Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;
.super Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;
.source "HostedSquareMemberListFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;"
    }
.end annotation


# static fields
.field private static final SQUARE_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

.field private mCurrentSpinnerPosition:I

.field private mListView:Landroid/widget/ListView;

.field private mLoaderError:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSquareMembersLoaderActive:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "membership_status"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "joinability"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->SQUARE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareMembersLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z

    return v0
.end method

.method private isLoading()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populatePrimarySpinner()V
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    if-nez v1, :cond_0

    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    sget v3, Lcom/google/android/apps/plus/R$string;->square_members:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    sget v3, Lcom/google/android/apps/plus/R$string;->square_members_moderators:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isSquareAdmin()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mJoinability:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    sget v3, Lcom/google/android/apps/plus/R$string;->square_members_awaiting_approval:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    sget v3, Lcom/google/android/apps/plus/R$string;->square_members_banned:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    sget v3, Lcom/google/android/apps/plus/R$string;->square_members_invited:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_2
    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/16 v3, 0x8

    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->showContent(Landroid/view/View;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->updateSpinner()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->no_square_members:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareMembersLoaderActive:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 0
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->onSearchRequested()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isSquareAdmin()Z

    move-result v2

    invoke-direct {v0, v1, v2, p0, p0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;-><init>(Landroid/content/Context;ZLcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter$OnLoadMoreMembersListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->populatePrimarySpinner()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 13
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v10, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v10

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;->getMemberListType()I

    move-result v5

    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isSquareAdmin()Z

    move-result v4

    sget-object v6, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->SQUARE_MEMBERS_PROJECTION:[Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZI[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    new-instance v6, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v7

    sget-object v9, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->SQUARE_PROJECTION:[Ljava/lang/String;

    move-object v11, v10

    move-object v12, v10

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v1, Lcom/google/android/apps/plus/R$layout;->square_member_list_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 6
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-nez p2, :cond_6

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareMembersLoaderActive:Z

    const/4 v0, 0x0

    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;

    if-eqz v2, :cond_5

    check-cast p1, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->isDataStale()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->refresh()V

    const-string v2, "SquareMembers"

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "SquareMembers"

    const-string v3, "Square member list stale - refreshing"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->getTotalMembers()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mTotalMemberCount:I

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    :cond_2
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mTotalMemberCount:I

    sub-int/2addr v2, v1

    if-lez v2, :cond_3

    const/16 v3, 0x1f4

    if-ge v1, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->getContinuationToken()Ljava/lang/String;

    move-result-object v0

    :cond_3
    const-string v3, "SquareMembers"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "SquareMembers"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onLoadFinished count="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " totalMembers="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mTotalMemberCount:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v1, v2

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {v2, p2, v0, v1}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->changeSquareMembersCursor(Landroid/database/Cursor;Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->updateView(Landroid/view/View;)V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mMembershipStatus:I

    if-eq v2, v4, :cond_7

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mMembershipStatus:I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isSquareAdmin()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->setIsSquareAdmin(Z)V

    move v1, v0

    :cond_7
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mJoinability:I

    if-eq v3, v2, :cond_8

    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mJoinability:I

    :goto_2
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->populatePrimarySpinner()V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoadMoreMembers(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment$MemberListSpinnerInfo;->getMemberListType()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SquareMemberListAdapter;->getMemberCount()I

    move-result v6

    rsub-int v5, v6, 0x1f4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareId:Ljava/lang/String;

    move-object v4, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->readSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mOlderReqId:Ljava/lang/Integer;

    const-string v0, "SquareMembers"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SquareMembers"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadMoreMembers maxToFetch="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_menu_search_holo_light:I

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_search:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mCurrentSpinnerPosition:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mCurrentSpinnerPosition:I

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mTotalMemberCount:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method protected final onResumeContentFetched(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->onResumeContentFetched(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mLoaderError:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lvedroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    :cond_0
    return-void
.end method

.method public final onSearchRequested()V
    .locals 5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mMembershipStatus:I

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mJoinability:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getSquareMemberSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;II)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final refresh()V
    .locals 6

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseSquareMemberFragment;->refresh()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mSquareId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->isSquareAdmin()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    const/4 v4, 0x0

    const/16 v5, 0x64

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->readSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->updateSpinner()V

    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
