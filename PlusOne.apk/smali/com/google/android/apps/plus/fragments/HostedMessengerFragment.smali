.class public Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedMessengerFragment.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$SuggestionsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ParticipantsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

.field private mConnected:Ljava/lang/Boolean;

.field private mConversationCursor:Landroid/database/Cursor;

.field private mConversationsUri:Landroid/net/Uri;

.field private mInvitationConversationBundle:Landroid/os/Bundle;

.field private mListView:Landroid/widget/ListView;

.field private final mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

.field private mRecordedConversationsEmpty:Z

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mScrollOffset:I

.field private mScrollPos:I

.field private mSuggestionCursor:Landroid/database/Cursor;

.field private mSuggestionsUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/view/View;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)Landroid/database/Cursor;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method private isLoading()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getConversationsLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method protected final doShowEmptyView(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected final doShowEmptyViewProgress(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getConversationsLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final needsAsyncData()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final onActionButtonClicked(I)V
    .locals 3
    .param p1    # I

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    if-eqz p1, :cond_1

    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    :goto_0
    if-eqz p1, :cond_0

    const-string v0, "InvitationConversationBundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRecordedConversationsEmpty:Z

    :cond_0
    return-void

    :cond_1
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v0, "ConversationList"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateLoader "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-ne p1, v6, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationsUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "is_visible=1 AND is_pending_leave=0"

    const-string v6, "latest_message_timestamp DESC"

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    :goto_0
    return-object v0

    :cond_1
    if-ne p1, v4, :cond_2

    new-instance v1, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionsUri:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$SuggestionsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "sequence ASC"

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "conversation_row_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ParticipantsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "participant_id!=?"

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "first_name ASC"

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0

    :cond_3
    move-object v0, v5

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v5, 0x0

    sget v2, Lcom/google/android/apps/plus/R$layout;->conversation_list_fragment:I

    invoke-virtual {p1, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/content/Context;Landroid/widget/AbsListView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->help_spacer:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 15
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getPartitionForPosition(I)I

    move-result v14

    if-nez v14, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v2, 0xd

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0xe

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v4, 0xe

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x3

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v7, 0x1

    :goto_1
    move-wide/from16 v4, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/plus/phone/Intents;->getConversationInvititationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x3

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move-wide/from16 v0, p4

    invoke-static {v3, v4, v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    if-eqz v8, :cond_0

    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v2, 0x3

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    invoke-static {v2, v3, v12, v4}, Lcom/google/android/apps/plus/phone/Intents;->getFakeConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Data$Participant;Z)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 10
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v4, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x1

    check-cast p2, Landroid/database/Cursor;

    const-string v0, "ConversationList"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onLoadFinished "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v9, :cond_7

    if-eqz p2, :cond_4

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "g:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_1
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v6, v2, v3, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v2, "e:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    goto :goto_1

    :cond_2
    const-string v2, "p:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_11

    move-object v2, v1

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v3, v4, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    const-string v1, "conversation_is_group"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_edit_audience_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x6

    move v6, v5

    move v7, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v9}, Lvedroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    :cond_5
    :goto_3
    return-void

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_7
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-eq v0, v5, :cond_8

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v4, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    :cond_9
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_10

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    :cond_a
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    if-nez v0, :cond_b

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    if-eqz v0, :cond_c

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    iput v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    iput v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_e

    :cond_d
    move v8, v5

    :cond_e
    if-eqz v8, :cond_f

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRecordedConversationsEmpty:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRecordedConversationsEmpty:Z

    :cond_f
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isLoading()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->onAsyncData()V

    goto :goto_3

    :cond_10
    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v4, :cond_a

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionCursor:Landroid/database/Cursor;

    goto :goto_4

    :cond_11
    move-object v0, v1

    move-object v2, v1

    goto/16 :goto_1
.end method

.method public final onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->url_param_help_messenger:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startExternalActivity(Landroid/content/Intent;)V

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method public final onPause()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->huddle_help_text:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    sget v0, Lcom/google/android/apps/plus/R$string;->home_screen_huddle_label:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    const/16 v0, 0x64

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_menu_start_new_huddle:I

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_new_conversation:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1    # Landroid/view/Menu;

    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public final onResume()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$id;->huddle_help_text:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "plusone_messenger_promo"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->huddle_help_text:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v5, Landroid/text/style/URLSpan;

    invoke-interface {v4, v7, v3, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/URLSpan;

    array-length v5, v3

    if-lez v5, :cond_0

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    aget-object v3, v3, v7

    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$1;

    invoke-direct {v7, p0, v3}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/text/style/URLSpan;)V

    const/16 v3, 0x21

    invoke-virtual {v5, v7, v6, v4, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    const-string v3, "conversation_row_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->inviteParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)I

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "InvitationConversationBundle"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    :cond_0
    const-string v0, "scroll_pos"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "scroll_off"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const/4 v2, 0x3

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationsUri:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionsUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    const-string v0, "ConversationList"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ConversationList"

    const-string v1, "setAccount"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v0, "reset_notifications"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->resetNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_1
    return-void
.end method

.method protected final showContent(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0x8

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
