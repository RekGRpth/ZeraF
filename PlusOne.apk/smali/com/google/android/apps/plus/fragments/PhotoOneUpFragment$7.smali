.class final Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;
.super Landroid/os/AsyncTask;
.source "PhotoOneUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

.field final synthetic val$toastError:Ljava/lang/String;

.field final synthetic val$toastSuccess:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->val$toastSuccess:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->val$toastError:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs doInBackground([Landroid/graphics/Bitmap;)Ljava/lang/Boolean;
    .locals 4
    .param p1    # [Landroid/graphics/Bitmap;

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    const-string v2, "StreamOneUp"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "StreamOneUp"

    const-string v3, "Exception setting wallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Landroid/graphics/Bitmap;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->doInBackground([Landroid/graphics/Bitmap;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->val$toastSuccess:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->val$toastError:Ljava/lang/String;

    goto :goto_0
.end method
