.class public Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;
.super Lcom/google/android/apps/plus/fragments/PeopleListFragment;
.source "PeopleSearchListFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# instance fields
.field private mQuery:Ljava/lang/String;

.field private mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v5, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v1, "query"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mQuery:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->getLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->getFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;-><init>(Landroid/content/Context;Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setOnClickListener(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setCircleUsageType(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setPublicProfileSearchEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setIncludePeopleInCircles(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setIncludePlusPages(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setAddToCirclesActionEnabled(Z)V

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    sget v2, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->sTopBottomPadding:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setTopBottomPadding(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showSearchView()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->search_people_hint_text:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    return-void
.end method

.method public final onQueryClose()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setQueryString(Ljava/lang/String;)V

    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .locals 5
    .param p1    # Ljava/lang/CharSequence;

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mQuery:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setQueryString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "extra_search_query"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mQuery:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_search_type"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->getActivity()Lvedroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SEARCHBOX_SELECT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onStart()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onStart()V

    return-void
.end method

.method public final onStop()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/PeopleListFragment;->onStart()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;->mSearchAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onStop()V

    return-void
.end method
