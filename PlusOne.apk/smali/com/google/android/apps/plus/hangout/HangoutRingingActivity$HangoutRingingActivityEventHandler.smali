.class final Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "HangoutRingingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HangoutRingingActivityEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)V

    return-void
.end method


# virtual methods
.method public final onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_audio_unmute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_audio_unmute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_audio_mute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleAudioMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$600(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_audio_mute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onSignedIn(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedIn(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mHasActed:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$200(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    move-result-object v2

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->sendHangoutRingStatus(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$400(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->mPendingFinishStatus:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$300(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$302(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->exit(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$500(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$RingStatus;)V

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Signed in! User jid = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    return-void
.end method

.method public final onVideoMuteChanged(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_video_unmute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_video_unmute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_video_mute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->toggleVideoMuteMenuButton:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->access$700(Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity$HangoutRingingActivityEventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_video_mute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
