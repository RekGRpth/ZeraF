.class public Lcom/google/android/apps/plus/hangout/LocalVideoView;
.super Lcom/google/android/apps/plus/hangout/HangoutVideoView;
.source "LocalVideoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;
    }
.end annotation


# instance fields
.field private audioVideoFailed:Z

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

.field private isRegistered:Z

.field private numPendingStartOutgoingVideoRequests:I

.field private selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

.field private selfFrameHeight:I

.field private selfFrameWidth:I

.field private final textureView:Landroid/view/TextureView;

.field private final toggleFlashButton:Landroid/widget/ImageButton;

.field private final videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, -0x2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v1, 0x140

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    const/16 v1, 0xf0

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    new-instance v1, Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/LocalVideoView;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    new-instance v1, Landroid/view/TextureView;

    invoke-direct {v1, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setVideoSurface(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-virtual {v1, v0}, Landroid/view/TextureView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    new-instance v1, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->textureView:Landroid/view/TextureView;

    invoke-direct {v1, p1, v2, v3, p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Landroid/view/TextureView;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getFlashToggleButton()Landroid/widget/ImageButton;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/plus/hangout/LocalVideoView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView$1;-><init>(Lcom/google/android/apps/plus/hangout/LocalVideoView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/LocalVideoView;Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;
    .param p1    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/LocalVideoView;)Lcom/google/android/apps/plus/hangout/VideoCapturer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    return v0
.end method

.method static synthetic access$210(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/LocalVideoView;)Lcom/google/android/apps/plus/hangout/Cameras$CameraType;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/hangout/LocalVideoView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/LocalVideoView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/hangout/LocalVideoView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/hangout/LocalVideoView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->audioVideoFailed:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/LocalVideoView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->updateFlashLightButtonState()V

    return-void
.end method

.method private restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->audioVideoFailed:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setLastUsedCameraType(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getSizeOfCapturedFrames(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Landroid/hardware/Camera$Size;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->onCameraOpenError(Ljava/lang/RuntimeException;)V

    goto :goto_0

    :cond_1
    const-string v1, "Starting outgoing video"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->startOutgoingVideo(II)V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->numPendingStartOutgoingVideoRequests:I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    goto :goto_0
.end method

.method private updateFlashLightButtonState()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_flash_off_holo_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->toggleFlashButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_flash_on_holo_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public final isVideoShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing()Z

    move-result v0

    return v0
.end method

.method public final onCameraOpenError(Ljava/lang/RuntimeException;)V
    .locals 1
    .param p1    # Ljava/lang/RuntimeException;

    const-string v0, "Video capturer failed to start"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideVideoSurface()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideLogo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->showCameraError()V

    return-void
.end method

.method public final onCapturingStateChanged$1385ff()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->updateFlashLightButtonState()V

    return-void
.end method

.method public final onMeasure$3b4dfe4b(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->selfFrameHeight:I

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->layoutVideo(IIII)V

    return-void
.end method

.method public final onPause()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    return-void
.end method

.method public final onResume()V
    .locals 4

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/LocalVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->isRegistered:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideLogo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->showVideoSurface()V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/LocalVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    :cond_4
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    goto :goto_1

    :cond_6
    const-string v1, "Not starting outgoing video because device is not capable."

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_0
.end method
