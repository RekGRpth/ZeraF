.class final Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;
.super Ljava/lang/Object;
.source "TabletFilmStripView.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParticipantContextMenuHelper"
.end annotation


# instance fields
.field private final mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-void
.end method


# virtual methods
.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 10
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    const/4 v9, 0x1

    new-instance v7, Landroid/view/MenuInflater;

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    sget v8, Lcom/google/android/apps/plus/R$menu;->hangout_participant_menu:I

    invoke-virtual {v7, v8, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_profile:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_pin_video:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_unpin_video:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_remote_mute:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_remote_mute_disabled:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_block:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    sget v7, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_block_disabled:I

    invoke-interface {p1, v7}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-static {v8}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$1000(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v8

    if-eq v7, v8, :cond_1

    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_0
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v7

    if-nez v7, :cond_2

    instance-of v7, p2, Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    if-eqz v7, :cond_2

    check-cast p2, Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->isAudioMuteStatusShowing()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelfProfile()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_0
    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :goto_2
    return-void

    :cond_1
    invoke-interface {v6, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_2
    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_3
    invoke-interface {v1, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sget v5, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_profile:I

    if-ne v3, v5, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/HangoutTile;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mDismissMenuTimer:Landroid/os/CountDownTimer;
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$1400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Landroid/os/CountDownTimer;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/CountDownTimer;->cancel()V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$1502(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Landroid/app/Dialog;)Landroid/app/Dialog;

    const/4 v4, 0x1

    :cond_0
    return v4

    :cond_1
    sget v5, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_pin_video:I

    if-ne v3, v5, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$1100(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0

    :cond_2
    sget v5, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_unpin_video:I

    if-ne v3, v5, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->unpinVideo()V
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$1200(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)V

    goto :goto_0

    :cond_3
    sget v5, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_remote_mute:I

    if-ne v3, v5, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$1300(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->remoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0

    :cond_4
    sget v5, Lcom/google/android/apps/plus/R$id;->menu_hangout_participant_block:I

    if-ne v3, v5, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->mMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;-><init>(ZLjava/io/Serializable;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v0, v4, v7}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
