.class public Lcom/google/android/apps/plus/hangout/ExitHistory;
.super Ljava/lang/Object;
.source "ExitHistory.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/ExitHistory;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/ExitHistory;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static exitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v1, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/ExitHistory;->findPrefs(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "EXIT_REPORTED"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    :cond_0
    return v1
.end method

.method public static exitedNormally(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v2, 0x0

    const/4 v4, -0x1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/ExitHistory;->findPrefs(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "LAST_ERROR"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private static findPrefs(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Landroid/content/SharedPreferences;
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v7, 0x0

    const/4 v3, 0x0

    const-class v0, Lcom/google/android/apps/plus/hangout/ExitHistory;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    const-string v0, "INFO_HAS_INFO"

    invoke-interface {v8, v0, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v9, v3

    :goto_0
    if-nez v9, :cond_2

    :cond_0
    :goto_1
    return-object v3

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->values()[Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v0

    const-string v1, "INFO_ROOM_TYPE"

    invoke-interface {v8, v1, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    aget-object v1, v0, v1

    const-string v0, "INFO_DOMAIN"

    const-string v2, ""

    invoke-interface {v8, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "INFO_ID"

    const-string v4, ""

    invoke-interface {v8, v0, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->None:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-object v5, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    move-object v9, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v9, p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v3, v8

    goto :goto_1
.end method

.method public static getError(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v2, 0x0

    const/4 v4, -0x1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/ExitHistory;->findPrefs(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "LAST_ERROR"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    move-result-object v2

    aget-object v2, v2, v0

    goto :goto_0
.end method

.method public static recordErrorExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;
    .param p2    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    .param p3    # Z

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/ExitHistory;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->ordinal()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->ordinal()I

    move-result v0

    invoke-static {p0, p1, v0, p3}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;IZ)V

    return-void
.end method

.method private static recordExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;IZ)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;
    .param p2    # I
    .param p3    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-class v1, Lcom/google/android/apps/plus/hangout/ExitHistory;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "INFO_HAS_INFO"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "INFO_ROOM_TYPE"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "INFO_DOMAIN"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "INFO_ID"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v1, "LAST_ERROR"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v1, "EXIT_REPORTED"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public static recordExitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {p0, p1, v1, v0}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;IZ)V

    return-void
.end method

.method public static recordNormalExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Z)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;
    .param p2    # Z

    const/4 v0, -0x1

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;IZ)V

    return-void
.end method
