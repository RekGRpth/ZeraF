.class final Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;
.super Ljava/lang/Object;
.source "VideoTextureView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/GLTextureView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/VideoTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Renderer"
.end annotation


# instance fields
.field private volatile mAttempt:I

.field private volatile mEnabled:Z

.field private volatile mInitializeRendererPending:Z

.field private mPendingHeight:I

.field private mPendingWidth:I

.field private mSurfaceSizePending:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/VideoTextureView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/VideoTextureView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;-><init>(Lcom/google/android/apps/plus/hangout/VideoTextureView;)V

    return-void
.end method

.method private handleFailure()Z
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mAttempt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mAttempt:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mAttempt:I

    const/16 v2, 0x1e

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mIsDecoding:Z
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$302(Lcom/google/android/apps/plus/hangout/VideoTextureView;Z)Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mEnabled:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mInitializeRendererPending:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mSurfaceSizePending:Z

    const/4 v0, 0x1

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Configuring native video renderer failed after "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mAttempt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " attempts"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :cond_1
    return v0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private initializeRenderer()V
    .locals 5

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mInitializeRendererPending:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mGcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$200(Lcom/google/android/apps/plus/hangout/VideoTextureView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$100(Lcom/google/android/apps/plus/hangout/VideoTextureView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->initializeIncomingVideoRenderer(I)Z

    move-result v1

    if-eqz v1, :cond_2

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mEnabled:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mInitializeRendererPending:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mEnabled:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mSurfaceSizePending:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mGcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$200(Lcom/google/android/apps/plus/hangout/VideoTextureView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$100(Lcom/google/android/apps/plus/hangout/VideoTextureView;)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mPendingWidth:I

    iget v4, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mPendingHeight:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoRendererSurfaceSize(III)Z

    move-result v1

    if-eqz v1, :cond_3

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mSurfaceSizePending:Z

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->handleFailure()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "initializeIncomingVideoRenderer failed. Rendering disabled"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_0

    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->handleFailure()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v1, "setIncomingVideoRendererSurfaceSize failed. Rendering disabled"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final onDrawFrame$62c01aa1()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->initializeRenderer()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mGcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$200(Lcom/google/android/apps/plus/hangout/VideoTextureView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    # getter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$100(Lcom/google/android/apps/plus/hangout/VideoTextureView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->renderIncomingVideoFrame(I)Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mIsDecoding:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$302(Lcom/google/android/apps/plus/hangout/VideoTextureView;Z)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onSurfaceChanged$4ccda93f(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mSurfaceSizePending:Z

    iput p2, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mPendingHeight:I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mPendingWidth:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mAttempt:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->initializeRenderer()V

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onSurfaceCreated$4a9c201c()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->initializeRenderer()V

    return-void
.end method

.method public final reinitialize()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mInitializeRendererPending:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->mAttempt:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->this$0:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/hangout/VideoTextureView;->mIsDecoding:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->access$302(Lcom/google/android/apps/plus/hangout/VideoTextureView;Z)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
