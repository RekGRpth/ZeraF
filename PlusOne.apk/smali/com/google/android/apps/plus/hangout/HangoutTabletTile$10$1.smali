.class final Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;
.super Ljava/lang/Object;
.source "HangoutTabletTile.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->onVideoSourceAboutToChange$1385ff()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

.field final synthetic val$snapshot:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->val$snapshot:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->val$snapshot:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->val$animIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showVideoSurface()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->access$2902(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10$1;->this$1:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getCurrentVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$3000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method
