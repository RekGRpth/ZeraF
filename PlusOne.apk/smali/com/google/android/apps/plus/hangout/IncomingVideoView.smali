.class public abstract Lcom/google/android/apps/plus/hangout/IncomingVideoView;
.super Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;
.source "IncomingVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/IncomingVideoView$1;,
        Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;,
        Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;,
        Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;,
        Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;
    }
.end annotation


# instance fields
.field private final avatarView:Landroid/widget/ImageView;

.field private final blockedView:Landroid/view/View;

.field private currentContent:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

.field protected currentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;

.field protected incomingVideoHeight:I

.field protected incomingVideoWidth:I

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field protected requestID:I

.field private showingUnknownAvatar:Z

.field private final videoPausedView:Landroid/view/View;

.field protected final videoView:Lcom/google/android/apps/plus/hangout/VideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;->NONE:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    iput v2, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    new-instance v1, Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/IncomingVideoView;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->hangout_incoming_video_view:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->video_view:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/VideoView;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoView:Lcom/google/android/apps/plus/hangout/VideoView;

    sget v1, Lcom/google/android/apps/plus/R$id;->video_avatar:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->avatarView:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$id;->blocked:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->blockedView:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->video_paused:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoPausedView:Landroid/view/View;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/IncomingVideoView;)Lcom/google/android/apps/plus/hangout/HangoutTile;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/IncomingVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/IncomingVideoView;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/IncomingVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->avatarView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/IncomingVideoView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/IncomingVideoView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->showingUnknownAvatar:Z

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/hangout/IncomingVideoView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/IncomingVideoView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->showingUnknownAvatar:Z

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/IncomingVideoView;)Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/IncomingVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    return-object v0
.end method


# virtual methods
.method final getRequestId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    return v0
.end method

.method public final onMeasure$3b4dfe4b(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->incomingVideoWidth:I

    iput p2, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->incomingVideoHeight:I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    iget v2, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->incomingVideoWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->incomingVideoHeight:I

    sget-object v4, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;->AUTO_ZOOM:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;

    const/16 v5, 0xf

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoParameters(IIILcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;I)V

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoView:Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoView;->onPause()V

    iget v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopIncomingVideo(I)V

    iput v3, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->requestID:I

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/IncomingVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->startVideo()V

    return-void
.end method

.method public setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-void
.end method

.method protected final setIncomingContent(Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    const/16 v1, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoView:Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/VideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->avatarView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->blockedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoPausedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->showingUnknownAvatar:Z

    sget-object v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$1;->$SwitchMap$com$google$android$apps$plus$hangout$IncomingVideoView$IncomingContentType:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoView:Lcom/google/android/apps/plus/hangout/VideoView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/VideoView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->avatarView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->blockedView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->videoPausedView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/IncomingVideoView$IncomingContentType;)V

    goto :goto_0
.end method

.method protected abstract startVideo()V
.end method
