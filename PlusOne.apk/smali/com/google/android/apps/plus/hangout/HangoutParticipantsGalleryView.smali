.class public Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;
.super Lcom/google/android/apps/plus/views/ParticipantsGalleryView;
.source "HangoutParticipantsGalleryView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$AvatarContextMenuHelper;,
        Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private avatarViewsByMeetingMember:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            "Lcom/google/android/apps/plus/views/OverlayedAvatarView;",
            ">;"
        }
    .end annotation
.end field

.field private currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private mainVideoRequestId:I

.field private final meetingMembers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation
.end field

.field private meetingMembersByAvatarView:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/views/OverlayedAvatarView;",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation
.end field

.field private pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembers:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;

    iput v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mainVideoRequestId:I

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setCurrentSpeaker(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mainVideoRequestId:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)Lcom/google/android/apps/plus/hangout/HangoutTile;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->unpinVideo()V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;
    .param p1    # Landroid/app/Dialog;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarContextMenuDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method private pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->updateMainVideoStreaming()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method private setCurrentSpeaker(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarViewsByMeetingMember:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarViewsByMeetingMember:Ljava/util/HashMap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1, v4}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipantLoudestSpeaker(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-void
.end method

.method private setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne p1, v1, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_pin:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setOverlayResource(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_circle_blocked:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setOverlayResource(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused()Z

    move-result v1

    if-eqz v1, :cond_4

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_video_pause:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setOverlayResource(I)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setOverlayResource(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTING:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v1, v2, :cond_5

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setParticipantType(I)V

    goto :goto_0

    :cond_5
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setParticipantType(I)V

    goto :goto_0
.end method

.method private unpinVideo()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->updateMainVideoStreaming()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method


# virtual methods
.method public final onAvatarClicked(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/views/OverlayedAvatarView;
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembersByAvatarView:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/MeetingMember;

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$AvatarContextMenuHelper;

    invoke-direct {v3, p0, v7, p2}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$AvatarContextMenuHelper;-><init>(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p1

    move-object v4, v3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/util/QuickActions;->show(Landroid/view/View;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/View$OnCreateContextMenuListener;Landroid/view/MenuItem$OnMenuItemClickListener;ZZ)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarContextMenuDialog:Landroid/app/Dialog;

    return-void
.end method

.method public final onAvatarDoubleClicked(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/OverlayedAvatarView;
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembersByAvatarView:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->unpinVideo()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->onAvatarClicked(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->dismissAvatarMenuDialog()V

    return-void
.end method

.method public final onResume()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    return-void
.end method

.method public final onShowParticipantList()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->getParticipantListActivityIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-void
.end method

.method final setMainVideoRequestId(I)V
    .locals 1
    .param p1    # I

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput p1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mainVideoRequestId:I

    return-void
.end method

.method public setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->removeAllParticipants()V

    const/4 v9, 0x0

    iput-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarViewsByMeetingMember:Ljava/util/HashMap;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembersByAvatarView:Ljava/util/HashMap;

    if-eqz p1, :cond_1

    new-instance v7, Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-direct {v7, v9}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->dismissAvatarMenuDialog()V

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembers:Ljava/util/List;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMembersOrderedByEntry()Ljava/util/List;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembers:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v9

    if-nez v9, :cond_0

    if-eqz v7, :cond_2

    invoke-virtual {v7, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    :goto_2
    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-ne v4, v10, :cond_3

    const/4 v10, 0x1

    invoke-virtual {p0, v9, v10}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipantLoudestSpeaker(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    :goto_3
    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->avatarViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v10, v4, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembersByAvatarView:Ljava/util/HashMap;

    invoke-virtual {v10, v9, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v5

    goto :goto_2

    :cond_3
    const/4 v10, 0x1

    invoke-virtual {p0, v9, v10}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    goto :goto_3

    :cond_4
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v7, v6}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v0

    const/4 v9, 0x0

    invoke-virtual {p0, v0, v9}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    goto :goto_4

    :cond_6
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v0

    const/4 v9, 0x0

    invoke-virtual {p0, v0, v9}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    goto :goto_5

    :cond_7
    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v9, :cond_8

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembers:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-interface {v9, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->currentSpeaker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setCurrentSpeaker(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_8
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v8

    if-eqz v8, :cond_a

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->meetingMembers:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :goto_7
    return-void

    :cond_9
    const/4 v9, 0x0

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setCurrentSpeaker(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_6

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->unpinVideo()V

    goto :goto_7
.end method
