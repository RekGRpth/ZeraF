.class final Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TabletFilmStripView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchListener"
.end annotation


# instance fields
.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private final mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p0}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;)Lcom/google/android/apps/plus/hangout/HangoutVideoView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    return-object v0
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$800(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    const/4 v1, 0x1

    return v1
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/HangoutTile;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, p4, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/HangoutTile;

    move-result-object v1

    check-cast v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->hideChild(Landroid/view/View;)V

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v1

    goto :goto_0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getY()F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_2

    cmpg-float v4, p4, v6

    if-gez v4, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener$1;

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener$1;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {v4, v5, v1, v6, v3}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    :cond_0
    return v2

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    cmpl-float v4, p4, v6

    if-lez v4, :cond_3

    move v0, v2

    :goto_1
    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mVideoView:Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onVideoClicked(Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$700(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
