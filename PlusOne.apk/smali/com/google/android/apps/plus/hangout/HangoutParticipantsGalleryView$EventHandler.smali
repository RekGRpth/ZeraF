.class final Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "HangoutParticipantsGalleryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)V

    return-void
.end method


# virtual methods
.method public final onCurrentSpeakerChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$000(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setCurrentSpeaker(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$100(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    return-void
.end method

.method public final onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$300(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$300(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method public final onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$300(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method public final onVideoSourceChanged(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoSourceChanged(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->mainVideoRequestId:I
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$200(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->pinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$000(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;)Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setCurrentSpeaker(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->access$100(Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    return-void
.end method
