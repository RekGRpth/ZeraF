.class public Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;
.super Landroid/app/Activity;
.source "CrashTriggerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$JavaCrashOnNativeThreadException;,
        Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$NativeCrashException;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.hangout.java_crash_signature"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$NativeCrashException;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$NativeCrashException;-><init>(B)V

    throw v0

    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$JavaCrashOnNativeThreadException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$JavaCrashOnNativeThreadException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity$1;-><init>(Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
