.class public Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
.super Ljava/lang/Object;
.source "GCommNativeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final GCOMM_NATIVE_LIB_API_LEVEL:I = 0x6

.field public static final HANGOUT:Ljava/lang/String; = "HANGOUT"

.field public static final HANGOUT_SYNC:Ljava/lang/String; = "HANGOUT_SYNC"

.field public static final INVALID_INCOMING_VIDEO_REQUEST_ID:I = 0x0

.field static final MAX_INCOMING_AUDIO_LEVEL:I = 0xff

.field static final MIN_INCOMING_AUDIO_LEVEL:I = 0x0

.field private static final SELF_MUC_JID_BEFORE_ENTERING_HANGOUT:Ljava/lang/String; = ""

.field public static final TRANSFER:Ljava/lang/String; = "TRANSFER"


# instance fields
.field private volatile account:Lcom/google/android/apps/plus/content/EsAccount;

.field private volatile clientInitiatedExit:Z

.field private final context:Landroid/content/Context;

.field private volatile hadSomeConnectedParticipant:Z

.field private volatile hangoutCreated:Z

.field private volatile hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private volatile isHangoutLite:Z

.field private volatile memberMucJidToMeetingMember:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation
.end field

.field private volatile membersCount:I

.field private volatile nativePeerObject:J

.field private volatile retrySignin:Z

.field private volatile ringInvitees:Z

.field private volatile roomHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;",
            ">;"
        }
    .end annotation
.end field

.field private volatile selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private volatile userJid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->$assertionsDisabled:Z

    const-string v0, "GCommNativeWrapper loading gcomm_ini"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    const-string v0, "gcomm_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    const-string v0, "GCommNativeWrapper done loading gcomm_ini"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->roomHistory:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    return-void
.end method

.method private static ToArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    array-length v5, p0

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    move-object v0, p0

    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public static initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/LinkageError;
        }
    .end annotation

    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStaticGetVersion()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GComm native lib API version:     "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    const-string v1, "GComm native wrapper API version: 6"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GComm native lib logging: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at level "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GComm native lib version mismatch.  Expected 6 but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/UnsupportedClassVersionError;

    invoke-direct {v1}, Ljava/lang/UnsupportedClassVersionError;-><init>()V

    throw v1

    :cond_0
    invoke-static/range {p0 .. p6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStaticInitialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "GComm native lib initialization failed"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/ExceptionInInitializerError;

    invoke-direct {v1}, Ljava/lang/ExceptionInInitializerError;-><init>()V

    throw v1

    :cond_1
    return-void
.end method

.method private native nativeBlockMedia(Ljava/lang/String;)V
.end method

.method private native nativeConnectAndSignin(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeConnectAndSigninFull(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method private native nativeCreateHangout()V
.end method

.method private native nativeEnterMeeting(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method private native nativeEnterMeetingWithCachedGreenRoomInfo(Z)V
.end method

.method private native nativeExitMeeting()V
.end method

.method private native nativeGetIncomingAudioVolume()I
.end method

.method private native nativeInitializeIncomingVideoRenderer(I)Z
.end method

.method private native nativeInviteToMeeting([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method private native nativeIsAudioMute()Z
.end method

.method private native nativeIsOutgoingVideoStarted()Z
.end method

.method private native nativeKickMeetingMember(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativePeerCreate()J
.end method

.method private native nativePeerDestroy(J)V
.end method

.method private native nativeProvideOutgoingVideoFrame([BJI)V
.end method

.method private native nativeRemoteMute(Ljava/lang/String;)V
.end method

.method private native nativeRenderIncomingVideoFrame(I)Z
.end method

.method private native nativeRequestVCard(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSendInstantMessage(Ljava/lang/String;)V
.end method

.method private native nativeSendInstantMessageToUser(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSetAudioMute(Z)V
.end method

.method private native nativeSetIncomingAudioVolume(I)V
.end method

.method private native nativeSetIncomingVideoParameters(IIIII)V
.end method

.method private native nativeSetIncomingVideoRendererSurfaceSize(III)Z
.end method

.method private native nativeSetIncomingVideoSourceToSpeakerIndex(II)V
.end method

.method private native nativeSetIncomingVideoSourceToUser(ILjava/lang/String;)V
.end method

.method private native nativeSetPresenceConnectionStatus(I)V
.end method

.method private native nativeSignoutAndDisconnect()V
.end method

.method public static native nativeSimulateCrash()V
.end method

.method private native nativeStartIncomingVideoForSpeakerIndex(IIII)I
.end method

.method private native nativeStartIncomingVideoForUser(Ljava/lang/String;III)I
.end method

.method private native nativeStartOutgoingVideo(II)V
.end method

.method public static native nativeStaticCleanup()V
.end method

.method private static native nativeStaticGetVersion()I
.end method

.method private static native nativeStaticInitialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Z
.end method

.method public static native nativeStaticSetDeviceCaptureType(I)V
.end method

.method private native nativeStopIncomingVideo(I)V
.end method

.method private native nativeStopOutgoingVideo()V
.end method

.method private native nativeUploadCallgrokLog()V
.end method

.method private onAudioMuteStateChanged(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x65

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method private onCallgrokLogUploadCompleted(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x3c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onCurrentSpeakerChanged(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x66

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onError(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const-string v1, "GCommNativeWrapper.onError: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    move-result-object v1

    aget-object v0, v1, p1

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUTHENTICATION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    if-ne v0, v1, :cond_0

    const-string v1, "Invalidating auth token..."

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "webupdates"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, -0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private onHangoutCreated(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutCreated:Z

    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Creation:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->ringInvitees:Z

    move-object v3, v2

    move-object v4, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x32

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onIncomingVideoFrameDimensionsChanged(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x6b

    new-instance v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;

    new-instance v3, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {v3, p2, p3}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;-><init>(ILcom/google/android/apps/plus/hangout/RectangleDimensions;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onIncomingVideoFrameReceived(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x6a

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onIncomingVideoStarted(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x68

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onInstantMessageReceived(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onInstantMessageReceived missing fromMucJid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/hangout/InstantMessage;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/apps/plus/hangout/InstantMessage;-><init>(Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x3b

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onMediaBlock(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v3, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setMediaBlocked(Z)V

    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 p3, 0x0

    :cond_1
    if-nez p3, :cond_2

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x6e

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    :cond_3
    return-void

    :cond_4
    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setMediaBlocked(Z)V

    goto :goto_0
.end method

.method private onMeetingEnterError(I)V
    .locals 3
    .param p1    # I

    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    move-result-object v1

    aget-object v0, v1, p1

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->HANGOUT_OVER:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->MissedCall:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clearMeetingState()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, -0x3

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private onMeetingExited()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clientInitiatedExit:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clearMeetingState()V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x36

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    :goto_0
    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onMeetingMediaStarted()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x35

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    return-void
.end method

.method private onMeetingMemberEntered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isHangoutLite:Z

    if-nez v1, :cond_1

    if-eqz p3, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring invalid user: JID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nickname="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ID=<empty> status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v6

    new-instance v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    add-int/lit8 v1, v4, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/MeetingMember;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    move-result-object v1

    aget-object v7, v1, p4

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->CONNECTING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-eq v7, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->JOINING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-ne v7, v1, :cond_4

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTING:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    :cond_3
    const-string v1, ""

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRequestVCard(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x37

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    goto :goto_1
.end method

.method private onMeetingMemberExited(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onMeetingMemberExited missing memberMucJid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->DISCONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x39

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private onMeetingMemberPresenceConnectionStateChanged(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v1, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onMeetingMemberPresenceConnectionStateChanged missing memberMucJid: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    move-result-object v3

    aget-object v0, v3, p2

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->CONNECTING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-eq v0, v3, :cond_2

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->JOINING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-ne v0, v3, :cond_4

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTING:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v3

    if-eq v2, v3, :cond_0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    if-nez v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v3, v4, :cond_3

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v4, 0x38

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    goto :goto_1
.end method

.method private onMucEntered(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x1

    new-instance v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    add-int/lit8 v1, v4, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    move-object v1, p1

    move-object v2, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/MeetingMember;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRequestVCard(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isHangoutLite:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x34

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method public static onNativeCrash()V
    .locals 1

    const-string v0, "GCommNativeWrapper.onNativeCrash - Crash from native code!!!"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportNativeCrash()V

    return-void
.end method

.method private onOutgoingVideoStarted()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x69

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    return-void
.end method

.method private onReceivedRoomHistory([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 13
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    sget-boolean v8, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->$assertionsDisabled:Z

    if-nez v8, :cond_0

    array-length v8, p1

    array-length v9, p2

    if-eq v8, v9, :cond_0

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    array-length v8, p1

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "@"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v7, v8, v10

    const/4 v2, 0x0

    :goto_0
    array-length v8, p1

    if-ge v2, v8, :cond_3

    aget-object v8, p1, v2

    const-string v9, "@"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v8, v6

    if-eq v8, v12, :cond_1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Bad format for room history: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, p1, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    aget-object v0, v6, v11

    aget-object v3, v6, v10

    move-object v1, v0

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "%s (%s)"

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v0, v9, v11

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    new-instance v4, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;

    invoke-direct {v4, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iput-object v5, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->roomHistory:Ljava/util/List;

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v9, 0x5

    invoke-static {v8, v9, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onRemoteMute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x6d

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method private onSignedIn(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->userJid:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onSignedOut()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    return-void
.end method

.method private onSigninTimeOutError()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v1, -0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    return-void
.end method

.method public static onUnhandledJavaException(Ljava/lang/Throwable;)V
    .locals 0
    .param p0    # Ljava/lang/Throwable;

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportJavaCrashFromNativeCode(Ljava/lang/Throwable;)V

    return-void
.end method

.method private onVCardResponse(Ljava/lang/String;Lcom/google/android/apps/plus/hangout/VCard;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/hangout/VCard;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onVCardResponse missing memberMucJid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setVCard(Lcom/google/android/apps/plus/hangout/VCard;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private onVideoPauseStateChanged(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setVideoPaused(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x6f

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private onVideoSourceChanged(ILjava/lang/String;Z)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {v0, p1, v1, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;-><init>(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x67

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void
.end method

.method private onVolumeChanged(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x70

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public blockMedia(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeBlockMedia(Ljava/lang/String;)V

    goto :goto_0
.end method

.method clearMeetingState()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutCreated:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->ringInvitees:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clientInitiatedExit:Z

    return-void
.end method

.method public connectAndSignin(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerCreate()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Created native peer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeConnectAndSignin(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public createHangout(Z)V
    .locals 4
    .param p1    # Z

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->ringInvitees:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeCreateHangout()V

    goto :goto_0
.end method

.method public enterMeeting(Lcom/google/android/apps/plus/service/Hangout$Info;ZZ)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;
    .param p2    # Z
    .param p3    # Z

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clearMeetingState()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v2, ""

    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->ordinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getServiceId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v3, ""

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getNick()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v5, ""

    :goto_3
    move-object v0, p0

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeEnterMeeting(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getServiceId()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getNick()Ljava/lang/String;

    move-result-object v5

    goto :goto_3
.end method

.method public exitMeeting()V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clientInitiatedExit:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeExitMeeting()V

    goto :goto_0
.end method

.method public getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->NONE:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeGetCurrentState()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public getHadSomeConnectedParticipantInPast()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    return v0
.end method

.method public getHangoutCreated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutCreated:Z

    return v0
.end method

.method public getHangoutDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHangoutId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHangoutInfo()Lcom/google/android/apps/plus/service/Hangout$Info;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-object v0
.end method

.method public getHangoutRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v0

    return-object v0
.end method

.method public getHasSomeConnectedParticipant()Z
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getIncomingAudioVolume()I
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeGetIncomingAudioVolume()I

    move-result v0

    goto :goto_0
.end method

.method public getIsHangoutLite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isHangoutLite:Z

    return v0
.end method

.method public getMeetingMember(Ljava/lang/String;)Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public getMeetingMemberCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public getMeetingMembersOrderedByEntry()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/google/android/apps/plus/hangout/MeetingMember$SortByEntryOrder;

    invoke-direct {v2}, Lcom/google/android/apps/plus/hangout/MeetingMember$SortByEntryOrder;-><init>()V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v1
.end method

.method public getRoomHistory()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->roomHistory:Ljava/util/List;

    return-object v0
.end method

.method public getSelfMeetingMember()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public getUserJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->userJid:Ljava/lang/String;

    return-object v0
.end method

.method public initializeIncomingVideoRenderer(I)Z
    .locals 4
    .param p1    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeInitializeIncomingVideoRenderer(I)Z

    move-result v0

    goto :goto_0
.end method

.method inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V
    .locals 20
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Z

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v18, 0x0

    cmp-long v1, v4, v18

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/api/ApiUtils;->removeCircleIdNamespaces(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object p1

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-virtual {v14, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v17, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v1

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v7

    array-length v12, v7

    const/4 v11, 0x0

    :goto_2
    if-ge v11, v12, :cond_5

    aget-object v16, v7, v11

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Person object with no id: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v14, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Skip adding: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v7

    array-length v12, v7

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v12, :cond_6

    aget-object v8, v7, v11

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "TRANSFER"

    move-object/from16 v0, p2

    if-eq v0, v1, :cond_7

    const-string v1, "Skipping invite since no one to invite"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v3, v1, [Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-object/from16 v1, p0

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeInviteToMeeting([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_0
.end method

.method public isAudioMute()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeIsAudioMute()Z

    move-result v0

    goto :goto_0
.end method

.method public isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isOutgoingVideoStarted()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeIsOutgoingVideoStarted()Z

    move-result v0

    goto :goto_0
.end method

.method public kickMeetingMember(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeKickMeetingMember(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public native nativeGetCurrentState()I
.end method

.method public provideOutgoingVideoFrame([BJI)V
    .locals 4
    .param p1    # [B
    .param p2    # J
    .param p4    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeProvideOutgoingVideoFrame([BJI)V

    goto :goto_0
.end method

.method public remoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRemoteMute(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public renderIncomingVideoFrame(I)Z
    .locals 4
    .param p1    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRenderIncomingVideoFrame(I)Z

    move-result v0

    goto :goto_0
.end method

.method public sendInstantMessage(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSendInstantMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendInstantMessageToUser(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSendInstantMessageToUser(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAudioMute(Z)V
    .locals 4
    .param p1    # Z

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetAudioMute(Z)V

    goto :goto_0
.end method

.method public setIncomingAudioVolume(I)V
    .locals 4
    .param p1    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-ltz p1, :cond_1

    const/16 v0, 0xff

    if-le p1, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "level is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingAudioVolume(I)V

    goto :goto_0
.end method

.method public setIncomingVideoParameters(IIILcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;I)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;
    .param p5    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;->ordinal()I

    move-result v4

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoParameters(IIIII)V

    goto :goto_0
.end method

.method public setIncomingVideoRendererSurfaceSize(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoRendererSurfaceSize(III)Z

    move-result v0

    goto :goto_0
.end method

.method public setIncomingVideoSourceToSpeakerIndex(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoSourceToSpeakerIndex(II)V

    goto :goto_0
.end method

.method public setIncomingVideoSourceToUser(ILjava/lang/String;)V
    .locals 4
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoSourceToUser(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setPresenceConnectionStatus(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetPresenceConnectionStatus(I)V

    goto :goto_0
.end method

.method public signoutAndDisconnect()V
    .locals 4

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSignoutAndDisconnect()V

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerDestroy(J)V

    iput-wide v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    goto :goto_0
.end method

.method public startIncomingVideoForSpeakerIndex(IIII)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStartIncomingVideoForSpeakerIndex(IIII)I

    move-result v0

    goto :goto_0
.end method

.method public startIncomingVideoForUser(Ljava/lang/String;III)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStartIncomingVideoForUser(Ljava/lang/String;III)I

    move-result v0

    goto :goto_0
.end method

.method public startOutgoingVideo(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStartOutgoingVideo(II)V

    goto :goto_0
.end method

.method public stopIncomingVideo(I)V
    .locals 4
    .param p1    # I

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStopIncomingVideo(I)V

    goto :goto_0
.end method

.method public stopOutgoingVideo()V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStopOutgoingVideo()V

    goto :goto_0
.end method

.method public uploadCallgrokLog()V
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeUploadCallgrokLog()V

    goto :goto_0
.end method
