.class final Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "HangoutActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MinorWarningDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;->this$0:Lcom/google/android/apps/plus/hangout/HangoutActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;->getDialogContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    sget v7, Lcom/google/android/apps/plus/R$layout;->hangout_minor_dialog:I

    invoke-virtual {v4, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v7, Lcom/google/android/apps/plus/R$id;->minorHangoutDontShow:I

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    sget v7, Lcom/google/android/apps/plus/R$id;->minorHangoutMessage:I

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->hangout_minor_warning_message:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;->this$0:Lcom/google/android/apps/plus/hangout/HangoutActivity;

    invoke-static {v5, v7, v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x1080027

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->hangout_minor_warning_header:I

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->hangout_minor_ok_button_text:I

    new-instance v9, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog$1;

    invoke-direct {v9, p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7
.end method
