.class public Lcom/google/android/apps/plus/hangout/SelfVideoView;
.super Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;
.source "SelfVideoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;,
        Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;
    }
.end annotation


# instance fields
.field private audioVideoFailed:Z

.field private cameraErrorView:Landroid/view/View;

.field private disableFlashLightSupport:Z

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;

.field private extraBottomOffset:I

.field private insetViewGroup:Landroid/view/ViewGroup;

.field private layoutMode:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private mVerticalGravity:F

.field private numPendingStartOutgoingVideoRequests:I

.field private selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

.field private selfFrameHeight:I

.field private selfFrameWidth:I

.field private surfaceView:Landroid/view/SurfaceView;

.field private toggleFlashLightButton:Landroid/widget/ImageButton;

.field private videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v1, 0x140

    iput v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    const/16 v1, 0xf0

    iput v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    sget-object v1, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layoutMode:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    new-instance v1, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/SelfVideoView;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->disableFlashLightSupport:Z

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->mVerticalGravity:F

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->hangout_self_video_view:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->inset:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->insetViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->insetViewGroup:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->surface_view:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->insetViewGroup:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->self_video_error:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->cameraErrorView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->insetViewGroup:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_toggle_flash_light_button:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/plus/hangout/SelfVideoView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView$1;-><init>(Lcom/google/android/apps/plus/hangout/SelfVideoView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3, p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Landroid/view/SurfaceHolder;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/SelfVideoView;Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;
    .param p1    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/VideoCapturer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/SelfVideoView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->updateFlashLightButtonState()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/SelfVideoView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I

    return v0
.end method

.method static synthetic access$210(Lcom/google/android/apps/plus/hangout/SelfVideoView;)I
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/HangoutTile;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Landroid/view/SurfaceView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/Cameras$CameraType;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/hangout/SelfVideoView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    return p1
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/hangout/SelfVideoView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    return p1
.end method

.method static synthetic access$802(Lcom/google/android/apps/plus/hangout/SelfVideoView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/SelfVideoView;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->audioVideoFailed:Z

    return v0
.end method

.method private restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->audioVideoFailed:Z

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setLastUsedCameraType(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getSizeOfCapturedFrames(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Landroid/hardware/Camera$Size;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onCameraOpenError(Ljava/lang/RuntimeException;)V

    goto :goto_0

    :cond_1
    const-string v1, "Starting outgoing video"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->startOutgoingVideo(II)V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    goto :goto_0
.end method

.method private updateFlashLightButtonState()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->disableFlashLightSupport:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_flash_off_holo_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_flash_on_holo_light:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public final layout(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v8, 0x0

    const/4 v6, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layoutMode:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    sget-object v10, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->INSET:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    if-ne v9, v10, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    iget v10, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    if-le v9, v10, :cond_0

    const-wide v9, 0x3fc999999999999aL

    int-to-double v11, v2

    mul-double/2addr v9, v11

    double-to-int v1, v9

    int-to-double v9, v1

    iget v11, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    int-to-double v11, v11

    mul-double/2addr v9, v11

    iget v11, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    int-to-double v11, v11

    div-double/2addr v9, v11

    double-to-int v0, v9

    :goto_0
    new-instance v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {v5, v1, v0}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    const-wide v9, 0x3f947ae147ae147bL

    int-to-double v11, v2

    mul-double/2addr v9, v11

    double-to-int v8, v9

    iget v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->extraBottomOffset:I

    add-int v6, v8, v9

    :goto_1
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    iget v9, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    iget v10, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-direct {v7, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10, v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layoutMode:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    sget-object v10, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->INSET:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    if-ne v9, v10, :cond_3

    const/16 v9, 0xc

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v9, 0xb

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_2
    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->insetViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v9, v7}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v9, "SelfView.layout: frame=%d,%d root=%d,%d self=%d,%d"

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    iget v12, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    iget v12, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x5

    iget v12, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v9, 0x0

    int-to-float v10, p2

    iget v11, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->mVerticalGravity:F

    mul-float/2addr v10, v11

    float-to-int v10, v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p0, v9, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setPadding(IIII)V

    return-void

    :cond_0
    const-wide v9, 0x3fc999999999999aL

    int-to-double v11, v2

    mul-double/2addr v9, v11

    double-to-int v0, v9

    int-to-double v9, v0

    iget v11, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    int-to-double v11, v11

    mul-double/2addr v9, v11

    iget v11, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    int-to-double v11, v11

    div-double/2addr v9, v11

    double-to-int v1, v9

    goto/16 :goto_0

    :cond_1
    iget v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    if-nez v9, :cond_2

    new-instance v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    const/16 v9, 0x64

    const/16 v10, 0x64

    invoke-direct {v5, v9, v10}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    goto/16 :goto_1

    :cond_2
    iget v9, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I

    iget v10, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I

    int-to-double v11, v9

    int-to-double v9, v10

    div-double v3, v11, v9

    invoke-static {v3, v4, p1, p2}, Lcom/google/android/apps/plus/hangout/Utils;->fitContentInContainer(DII)Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-result-object v5

    goto/16 :goto_1

    :cond_3
    const/16 v9, 0xd

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_2
.end method

.method public final onCameraOpenError(Ljava/lang/RuntimeException;)V
    .locals 2
    .param p1    # Ljava/lang/RuntimeException;

    const-string v0, "Video capturer failed to start"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->cameraErrorView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final onCapturingStateChanged$1385ff()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->updateFlashLightButtonState()V

    return-void
.end method

.method public final onMeasure$3b4dfe4b(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layout(II)V

    return-void
.end method

.method public final onPause()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    return-void
.end method

.method public final onResume()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->startCapturing()V

    return-void
.end method

.method public setExtraBottomOffset(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->extraBottomOffset:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->requestLayout()V

    return-void
.end method

.method public setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-void
.end method

.method public setLayoutMode(Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layoutMode:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    return-void
.end method

.method public setVerticalGravity(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->mVerticalGravity:F

    return-void
.end method

.method public setVisibleViewOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 3
    .param p1    # Landroid/view/View$OnTouchListener;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layoutMode:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, p1}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public final startCapturing()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    :cond_3
    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    goto :goto_1

    :cond_5
    const-string v2, "Not starting outgoing video because device is not capable."

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final turnOffFlashLightSupport()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->disableFlashLightSupport:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView;->toggleFlashLightButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method
