.class public final Lcom/google/android/apps/plus/api/ReshareActivityOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "ReshareActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostActivityRequest;",
        "Lcom/google/api/services/plusi/model/PostActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mContent:Ljava/lang/String;

.field private final mReshareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/apps/plus/content/AudienceData;

    const-string v3, "postactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mReshareId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mContent:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v4, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityResponse;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mReshareId:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromItemId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_2
    invoke-static {v4, v4, v4, v5, v5}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "DEFAULT"

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesAndOverwrite(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mReshareId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->resharedUpdateId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateText:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->squareStreams:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    new-instance v4, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;->squareId:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/google/api/services/plusi/model/RequestsPostActivityRequestSquareStreamInfo;->streamId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->squareStreams:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
