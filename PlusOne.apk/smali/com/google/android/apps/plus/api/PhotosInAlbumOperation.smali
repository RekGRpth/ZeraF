.class public final Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PhotosInAlbumOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;",
        "Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthkey:Ljava/lang/String;

.field private final mCollectionId:Ljava/lang/String;

.field private final mCoverOnly:Z

.field private mIsAlbum:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # Landroid/content/Intent;
    .param p8    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p9    # Ljava/lang/String;

    const-string v4, "photosinalbum"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCoverOnly:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAuthkey:Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mIsAlbum:Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;
    .param p6    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p7    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v11, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->onStartResultProcessing()V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->photo:Ljava/util/List;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->isDownloadable:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    const-string v1, "camerasync"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v4, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPhoto;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateInstantUploadCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCoverOnly:Z

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mIsAlbum:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;Ljava/lang/Boolean;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const-string v0, "CAMERA_SYNC"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "camerasync"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_1

    :cond_4
    const-string v0, "UPDATES_ALBUMS"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "posts"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_1

    :cond_5
    const-string v0, "PROFILE_PHOTOS"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "profile"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_1

    :cond_6
    const-string v0, "BUNCH_ALBUMS"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "messenger"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_1
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v0, 0x0

    const/4 v1, 0x1

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;

    new-instance v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;-><init>()V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnAlbumInfo:Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnComments:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnDownloadability:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnOwnerInfo:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPhotos:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPlusOnes:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnShapes:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnVideoUrls:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->collectionId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->ownerId:Ljava/lang/String;

    const-string v0, "UPLOAD_TIME_DESC"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->photosSortOrder:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAuthkey:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAuthkey:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->authkey:Ljava/lang/String;

    :cond_1
    iput-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCoverOnly:Z

    if-eqz v0, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->maxResults:Ljava/lang/Integer;

    :cond_2
    return-void
.end method
