.class public final Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetPhotosSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetPhotosSettingsRequest;",
        "Lcom/google/api/services/plusi/model/GetPhotosSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mSettings:Lcom/google/api/services/plusi/model/DataSettings;

.field private final mSettingsMask:I

.field private final mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const-string v3, "getphotossettings"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPhotosSettingsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetPhotosSettingsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPhotosSettingsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetPhotosSettingsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mUserId:Ljava/lang/String;

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mSettingsMask:I

    return-void
.end method


# virtual methods
.method public final getSettings()Lcom/google/api/services/plusi/model/DataSettings;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mSettings:Lcom/google/api/services/plusi/model/DataSettings;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetPhotosSettingsResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetPhotosSettingsResponse;->settings:Lcom/google/api/services/plusi/model/DataSettings;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mSettings:Lcom/google/api/services/plusi/model/DataSettings;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mSettings:Lcom/google/api/services/plusi/model/DataSettings;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mUserId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mSettings:Lcom/google/api/services/plusi/model/DataSettings;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataSettings;->enableDownloadPhoto:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateDownloadPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetPhotosSettingsRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mUserId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPhotosSettingsRequest;->ownerId:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->mSettingsMask:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPhotosSettingsRequest;->settingsToFetch:Ljava/lang/Integer;

    return-void
.end method
