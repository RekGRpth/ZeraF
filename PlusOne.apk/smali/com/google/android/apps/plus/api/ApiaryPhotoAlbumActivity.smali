.class public Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;
.super Lcom/google/android/apps/plus/api/ApiaryActivity;
.source "ApiaryPhotoAlbumActivity.java"


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mImageList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .locals 5
    .param p1    # Lcom/google/api/services/plusi/model/MediaLayout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;->mDisplayName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->clear()V

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    new-instance v3, Ljava/io/IOException;

    const-string v4, "empty media item"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v3, p1, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;->mDisplayName:Ljava/lang/String;

    return-void
.end method
