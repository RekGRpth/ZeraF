.class public final Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetPlusOnePeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;",
        "Lcom/google/api/services/plusi/model/GetPlusonePeopleResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mNumPeopleToReturn:I

.field private mPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPerson;",
            ">;"
        }
    .end annotation
.end field

.field private final mPlusOneId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const/4 v6, 0x0

    const-string v3, "getplusonepeople"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetPlusonePeopleRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPlusonePeopleResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetPlusonePeopleResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPlusOneId:Ljava/lang/String;

    const/16 v0, 0x32

    iput v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mNumPeopleToReturn:I

    return-void
.end method


# virtual methods
.method public final getPeople()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPerson;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPeople:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleResponse;->person:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPeople:Ljava/util/List;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mPlusOneId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;->plusoneId:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->mNumPeopleToReturn:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPlusonePeopleRequest;->numPeopleToReturn:Ljava/lang/Integer;

    return-void
.end method
