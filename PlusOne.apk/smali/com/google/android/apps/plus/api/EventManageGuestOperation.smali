.class public final Lcom/google/android/apps/plus/api/EventManageGuestOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "EventManageGuestOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventManageGuestsRequest;",
        "Lcom/google/api/services/plusi/model/EventManageGuestsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthKey:Ljava/lang/String;

.field private final mBlacklist:Z

.field private final mEmail:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;

.field private final mGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "eventmanageguests"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventManageGuestsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventManageGuestsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventManageGuestsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventManageGuestsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEventId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mAuthKey:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mBlacklist:Z

    iput-object p6, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mGaiaId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEmail:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/EventManageGuestsResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EventManageGuestsResponse;->success:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EventManageGuestsResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEventId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mBlacklist:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mGaiaId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEmail:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsEventData;->updateEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/EventManageGuestsRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventManageGuestsRequest;->eventId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mBlacklist:Z

    if-eqz v0, :cond_0

    const-string v0, "ADD"

    :goto_0
    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventManageGuestsRequest;->actionType:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventManageGuestsRequest;->invitee:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mAuthKey:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEventId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventManageGuestsRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    new-instance v0, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mGaiaId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->mEmail:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedsPerson;->email:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/EventManageGuestsRequest;->invitee:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const-string v0, "REMOVE"

    goto :goto_0
.end method
