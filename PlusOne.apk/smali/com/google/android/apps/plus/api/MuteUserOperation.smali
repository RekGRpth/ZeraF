.class public final Lcom/google/android/apps/plus/api/MuteUserOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "MuteUserOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MuteUserRequest;",
        "Lcom/google/api/services/plusi/model/MuteUserResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static sFactory:Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;


# instance fields
.field private final mDb:Lcom/google/android/apps/plus/content/PeopleData;

.field private mGaiaId:Ljava/lang/String;

.field private mIsMute:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/api/MuteUserOperation;->sFactory:Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/content/PeopleData;

    const-string v3, "muteuser"

    invoke-static {}, Lcom/google/api/services/plusi/model/MuteUserRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MuteUserRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MuteUserResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MuteUserResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mDb:Lcom/google/android/apps/plus/content/PeopleData;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/content/PeopleData;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/plus/api/MuteUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)V

    return-void
.end method

.method public static getFactory()Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/api/MuteUserOperation;->sFactory:Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/MuteUserResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mDb:Lcom/google/android/apps/plus/content/PeopleData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mGaiaId:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MuteUserResponse;->isMuted:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/content/PeopleData;->setMuteState(Ljava/lang/String;Z)Z

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/MuteUserRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mGaiaId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MuteUserRequest;->obfuscatedGaiaId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mIsMute:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MuteUserRequest;->shouldMute:Ljava/lang/Boolean;

    return-void
.end method

.method public final startThreaded(Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mGaiaId:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mIsMute:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/MuteUserOperation;->startThreaded()V

    return-void
.end method
