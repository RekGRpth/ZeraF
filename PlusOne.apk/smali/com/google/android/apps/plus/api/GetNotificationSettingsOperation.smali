.class public final Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetNotificationSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SettingsFetchRequest;",
        "Lcom/google/api/services/plusi/model/SettingsFetchResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "settingsfetch"

    invoke-static {}, Lcom/google/api/services/plusi/model/SettingsFetchRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SettingsFetchRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SettingsFetchResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SettingsFetchResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    return-void
.end method


# virtual methods
.method public final getNotificationSettings()Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/SettingsFetchResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SettingsFetchResponse;->settings:Lcom/google/api/services/plusi/model/OzDataSettings;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SettingsFetchResponse;->settings:Lcom/google/api/services/plusi/model/OzDataSettings;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzDataSettings;->notificationSettings:Lcom/google/api/services/plusi/model/DataNotificationSettings;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/api/ProtocolException;

    const-string v1, "Notification settings missing from response"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/api/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SettingsFetchResponse;->settings:Lcom/google/api/services/plusi/model/OzDataSettings;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/OzDataSettings;->notificationSettings:Lcom/google/api/services/plusi/model/DataNotificationSettings;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SettingsFetchResponse;->settings:Lcom/google/api/services/plusi/model/OzDataSettings;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/OzDataSettings;->mobileSettings:Lcom/google/api/services/plusi/model/DataMobileSettings;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/DataNotificationSettings;->deliveryOption:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, v4, Lcom/google/api/services/plusi/model/DataNotificationSettings;->categoryInfo:Ljava/util/List;

    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    iget-object v0, v5, Lcom/google/api/services/plusi/model/DataMobileSettings;->mobileNotificationType:Ljava/lang/String;

    if-nez v0, :cond_3

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/api/ProtocolException;

    const-string v1, "Invalid notification settings response"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/api/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iget-object v7, v4, Lcom/google/api/services/plusi/model/DataNotificationSettings;->deliveryOption:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v3, v2

    :goto_0
    if-ge v3, v8, :cond_6

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {v6, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :cond_4
    new-instance v10, Lcom/google/android/apps/plus/content/NotificationSetting;

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/content/NotificationSetting;-><init>(Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;)V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_6
    iget-object v3, v4, Lcom/google/api/services/plusi/model/DataNotificationSettings;->categoryInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    if-ge v2, v7, :cond_8

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsNotificationsSettingsCategoryInfo;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsNotificationsSettingsCategoryInfo;->description:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsNotificationsSettingsCategoryInfo;->category:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsNotificationsSettingsCategoryInfo;->description:Ljava/lang/String;

    invoke-direct {v9, v0, v1}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_8
    iget-object v0, v4, Lcom/google/api/services/plusi/model/DataNotificationSettings;->emailAddress:Ljava/lang/String;

    iget-object v1, v5, Lcom/google/api/services/plusi/model/DataMobileSettings;->mobileNotificationType:Ljava/lang/String;

    new-instance v2, Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-direct {v2, v0, v1, v8}, Lcom/google/android/apps/plus/content/NotificationSettingsData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    const/4 v2, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/SettingsFetchRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;-><init>()V

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;->fetchSettingsDescription:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;->fetchPlusPageSettings:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;->fetchAlternateEmailAddress:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;->fetchWhoCanNotifyMe:Ljava/lang/Boolean;

    sget-object v1, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->TYPE_GROUP_TO_FETCH:Ljava/util/List;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;->typeGroupToFetch:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SettingsFetchRequest;->notificationSettingsFetchParams:Lcom/google/api/services/plusi/model/DataNotificationSettingsFetchParams;

    return-void
.end method
