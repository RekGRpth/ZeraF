.class public final Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "PhotosCreateCommentOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;",
        "Lcom/google/api/services/plusi/model/PhotosCreateCommentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthkey:Ljava/lang/String;

.field private final mComment:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "photoscreatecomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosCreateCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosCreateCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosCreateCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mPhotoId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mOwnerId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mComment:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mAuthkey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->onStartResultProcessing()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mPhotoId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentResponse;->comment:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoCommentList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->obfuscatedOwnerId:Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mPhotoId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->photoId:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mComment:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->comment:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mAuthkey:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;->mAuthkey:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosCreateCommentRequest;->authkey:Ljava/lang/String;

    :cond_0
    return-void
.end method
