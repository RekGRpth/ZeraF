.class public final Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;
.super Ljava/lang/Object;
.source "MuteUserOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/api/MuteUserOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;-><init>()V

    return-void
.end method

.method public static build(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)Lcom/google/android/apps/plus/api/MuteUserOperation;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Intent;
    .param p3    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p4    # Lcom/google/android/apps/plus/content/PeopleData;

    new-instance v0, Lcom/google/android/apps/plus/api/MuteUserOperation;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MuteUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;B)V

    return-object v0
.end method
