.class public Lcom/google/android/apps/plus/api/MediaRef;
.super Ljava/lang/Object;
.source "MediaRef.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDisplayName:Ljava/lang/String;

.field private final mLocalUri:Landroid/net/Uri;

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private final mPhotoId:J

.field private final mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/MediaRef$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/api/MediaRef;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .locals 7
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    move-object v0, p0

    move-object v4, v1

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->valueOf(I)Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/net/Uri;
    .param p6    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/net/Uri;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    iput-wide p2, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    iput-object p4, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    iput-object p6, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/MediaRef;->hasUrl()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MediaRef has neither url nor local uri!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    move-object v0, p0

    move-object v4, p1

    move-object v5, v1

    move-object v6, v1

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v3, p1, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v3, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-wide v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    iget-wide v5, v0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v3, :cond_2

    if-eqz v4, :cond_2

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    :goto_1
    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    :cond_2
    if-nez v3, :cond_3

    if-nez v4, :cond_3

    move v3, v2

    goto :goto_1

    :cond_3
    move v3, v1

    goto :goto_1
.end method

.method public final getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getLocalUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getOwnerGaiaId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPhotoId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    return-wide v0
.end method

.method public final getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final hasLocalUri()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhotoId()Z
    .locals 4

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUrl()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    iget-wide v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    iget-wide v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_2
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [g-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", p-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
