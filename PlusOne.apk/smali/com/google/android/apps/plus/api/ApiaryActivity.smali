.class public Lcom/google/android/apps/plus/api/ApiaryActivity;
.super Ljava/lang/Object;
.source "ApiaryActivity.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/api/ApiaryActivity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

.field private mContentDeepLinkMetadata:Landroid/os/Bundle;

.field private mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryActivity$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiaryActivity$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryActivity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private update()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    if-nez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/MediaLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getEmbed(Ljava/lang/String;)Lcom/google/api/services/plusi/model/EmbedClientItem;
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->embedItem:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->embedItem:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    :goto_0
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/api/services/plusi/model/DeepLinkData;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DeepLinkData;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iput-object p1, v1, Lcom/google/api/services/plusi/model/DeepLinkData;->deepLinkId:Ljava/lang/String;

    :cond_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/EmbedClientItem;-><init>()V

    new-instance v2, Lcom/google/api/services/plusi/model/Thing;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/Thing;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    const-string v4, "title"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/Thing;->name:Ljava/lang/String;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    const-string v4, "description"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/Thing;->description:Ljava/lang/String;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    const-string v4, "thumbnailUrl"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/Thing;->imageUrl:Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    const-string v3, "THING"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    if-eqz v2, :cond_2

    new-instance v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedClientItem;-><init>()V

    new-instance v2, Lcom/google/api/services/plusi/model/AppInvite;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/AppInvite;-><init>()V

    iput-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iput-object v1, v2, Lcom/google/api/services/plusi/model/AppInvite;->about:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    new-instance v2, Lcom/google/api/services/plusi/model/DeepLink;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DeepLink;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v2, v2, Lcom/google/android/apps/plus/api/CallToActionData;->mLabel:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DeepLink;->deepLinkLabel:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v2, v2, Lcom/google/android/apps/plus/api/CallToActionData;->mLabel:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DeepLink;->label:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    new-instance v2, Lcom/google/api/services/plusi/model/DeepLinkData;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DeepLinkData;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v2, v2, Lcom/google/android/apps/plus/api/CallToActionData;->mDeepLinkId:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DeepLinkData;->deepLinkId:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->appInvite:Lcom/google/api/services/plusi/model/AppInvite;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AppInvite;->callToAction:Lcom/google/api/services/plusi/model/DeepLink;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DeepLink;->deepLink:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    iget-object v2, v2, Lcom/google/android/apps/plus/api/CallToActionData;->mUrl:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/DeepLinkData;->url:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    const-string v2, "APP_INVITE"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final getMediaJson()Ljava/lang/String;
    .locals 6

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-nez v5, :cond_1

    move-object v1, v4

    :goto_0
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    :goto_1
    return-object v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v1, v5, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->blackboxPreviewData:Ljava/util/List;

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1
.end method

.method public final setCallToActionMetadata(Lcom/google/android/apps/plus/api/CallToActionData;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/api/CallToActionData;

    iput-object p1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    return-void
.end method

.method public final setContentDeepLinkMetadata(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update()V

    return-void
.end method

.method public final setLinkPreview(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/LinkPreviewResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;->update()V

    return-void
.end method

.method protected update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/MediaLayout;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mContentDeepLinkMetadata:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ApiaryActivity;->mCallToActionButton:Lcom/google/android/apps/plus/api/CallToActionData;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method
