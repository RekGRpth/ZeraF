.class public final Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;
.super Lcom/google/android/apps/plus/api/ApiaryBatchOperation;
.source "SquaresReportAndBanOperation.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Z
    .param p10    # Z
    .param p11    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    new-instance v0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;

    const-string v5, "BAN"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p6

    move-object v4, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    if-eqz p10, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p11

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p9, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/EditModerationStateOperation;

    const-string v5, "REJECTED"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p6

    move-object v4, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/EditModerationStateOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    goto :goto_0
.end method
