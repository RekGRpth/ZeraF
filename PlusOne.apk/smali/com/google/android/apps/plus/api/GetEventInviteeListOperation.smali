.class public final Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "GetEventInviteeListOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetEventInviteeListRequest;",
        "Lcom/google/api/services/plusi/model/GetEventInviteeListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAuthKey:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;

.field private final mIncludeBlacklist:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    const-string v3, "geteventinviteelist"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetEventInviteeListRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetEventInviteeListRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetEventInviteeListResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetEventInviteeListResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mEventId:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mIncludeBlacklist:Z

    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mAuthKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    check-cast p1, Lcom/google/api/services/plusi/model/GetEventInviteeListResponse;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mEventId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetEventInviteeListResponse;->invitee:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInviteeList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/GetEventInviteeListRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetEventInviteeListRequest;->eventId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mIncludeBlacklist:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetEventInviteeListRequest;->includeAdminBlacklist:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mEventId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->mAuthKey:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventSelector;->authKey:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetEventInviteeListRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    return-void
.end method
