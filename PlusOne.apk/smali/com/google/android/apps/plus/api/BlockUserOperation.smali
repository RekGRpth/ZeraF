.class public final Lcom/google/android/apps/plus/api/BlockUserOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "BlockUserOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/BlockUserRequest;",
        "Lcom/google/api/services/plusi/model/BlockUserResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static sFactory:Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;


# instance fields
.field private final mDb:Lcom/google/android/apps/plus/content/PeopleData;

.field private mIsBlocked:Z

.field private mName:Ljava/lang/String;

.field private mPersonId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/api/BlockUserOperation;->sFactory:Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/content/PeopleData;

    const-string v3, "blockuser"

    invoke-static {}, Lcom/google/api/services/plusi/model/BlockUserRequestJson;->getInstance()Lcom/google/api/services/plusi/model/BlockUserRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/BlockUserResponseJson;->getInstance()Lcom/google/api/services/plusi/model/BlockUserResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mDb:Lcom/google/android/apps/plus/content/PeopleData;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p5    # Lcom/google/android/apps/plus/content/PeopleData;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/plus/api/BlockUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)V

    return-void
.end method

.method public static getFactory()Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/api/BlockUserOperation;->sFactory:Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

    return-object v0
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mDb:Lcom/google/android/apps/plus/content/PeopleData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mPersonId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mName:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mIsBlocked:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/PeopleData;->setBlockedState(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/BlockUserRequest;

    new-instance v0, Lcom/google/api/services/plusi/model/DataMembersToBlock;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataMembersToBlock;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/BlockUserRequest;->membersToBlock:Lcom/google/api/services/plusi/model/DataMembersToBlock;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/BlockUserRequest;->membersToBlock:Lcom/google/api/services/plusi/model/DataMembersToBlock;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mIsBlocked:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataMembersToBlock;->block:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/BlockUserRequest;->membersToBlock:Lcom/google/api/services/plusi/model/DataMembersToBlock;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataMembersToBlock;->members:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/DataMemberToBlock;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataMemberToBlock;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mPersonId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleMemberId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleMemberId;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataMemberToBlock;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataMemberToBlock;->name:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/BlockUserRequest;->membersToBlock:Lcom/google/api/services/plusi/model/DataMembersToBlock;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataMembersToBlock;->members:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final startThreaded(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mPersonId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/apps/plus/api/BlockUserOperation;->mIsBlocked:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/BlockUserOperation;->startThreaded()V

    return-void
.end method
