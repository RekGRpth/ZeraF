.class public final Lcom/google/android/apps/plus/api/ApiaryActivityFactory;
.super Ljava/lang/Object;
.source "ApiaryActivityFactory.java"


# direct methods
.method public static getApiaryActivity(Landroid/os/Bundle;Lcom/google/android/apps/plus/api/CallToActionData;)Lcom/google/android/apps/plus/api/ApiaryActivity;
    .locals 3
    .param p0    # Landroid/os/Bundle;
    .param p1    # Lcom/google/android/apps/plus/api/CallToActionData;

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Content deep-link metadata must not be null."

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const-string v1, "ApiaryActivityFactory"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "ApiaryActivityFactory"

    invoke-virtual {p0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiaryActivity;-><init>()V

    :try_start_0
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;->setContentDeepLinkMetadata(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_2

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/api/ApiaryActivity;->setCallToActionMetadata(Lcom/google/android/apps/plus/api/CallToActionData;)V

    :cond_2
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getApiaryActivity(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)Lcom/google/android/apps/plus/api/ApiaryActivity;
    .locals 5
    .param p0    # Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    const/4 v2, 0x0

    if-nez p0, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const-string v3, "ApiaryActivityFactory"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "ApiaryActivityFactory"

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v3, p0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Media layout must be specified"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    iget-object v3, p0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaLayout;

    const-string v3, "WEBPAGE"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiaryArticleActivity;-><init>()V

    :goto_0
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/api/ApiaryActivity;->setLinkPreview(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object v0

    :cond_4
    const-string v3, "VIDEO"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryVideoActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiaryVideoActivity;-><init>()V

    goto :goto_0

    :cond_5
    const-string v3, "SKYJAM_PREVIEW"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v0, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;-><init>()V

    goto :goto_0

    :cond_6
    const-string v3, "SKYJAM_PREVIEW_ALBUM"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    new-instance v0, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiarySkyjamActivity;-><init>()V

    goto :goto_0

    :cond_7
    const-string v3, "IMAGE"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v0, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/ApiaryPhotoAlbumActivity;-><init>()V

    goto :goto_0

    :cond_8
    move-object v0, v2

    goto :goto_1

    :catch_0
    move-exception v3

    move-object v0, v2

    goto :goto_1
.end method
