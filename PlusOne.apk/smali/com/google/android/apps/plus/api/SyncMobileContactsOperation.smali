.class public final Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;
.super Lcom/google/android/apps/plus/api/PlusiOperation;
.source "SyncMobileContactsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/api/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SyncMobileContactsRequest;",
        "Lcom/google/api/services/plusi/model/SyncMobileContactsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MobileContact;",
            ">;"
        }
    .end annotation
.end field

.field private final mDevice:Ljava/lang/String;

.field private mSuccess:Z

.field private mSyncType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Landroid/content/Intent;
    .param p7    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MobileContact;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    const-string v3, "syncmobilecontacts"

    invoke-static {}, Lcom/google/api/services/plusi/model/SyncMobileContactsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SyncMobileContactsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SyncMobileContactsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SyncMobileContactsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const-string v0, "FULL"

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSyncType:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSuccess:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mDevice:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mContacts:Ljava/util/List;

    iput-object p5, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSyncType:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    check-cast p1, Lcom/google/api/services/plusi/model/SyncMobileContactsResponse;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SyncMobileContactsResponse;->status:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SyncMobileContactsResponse;->status:Ljava/lang/String;

    const-string v2, "SUCCESS"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSuccess:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSuccess:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSyncType:Ljava/lang/String;

    const-string v2, "WIPEOUT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/GenericJson;

    check-cast p1, Lcom/google/api/services/plusi/model/SyncMobileContactsRequest;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mDevice:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SyncMobileContactsRequest;->device:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mSyncType:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SyncMobileContactsRequest;->type:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->mContacts:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SyncMobileContactsRequest;->contact:Ljava/util/List;

    return-void
.end method
