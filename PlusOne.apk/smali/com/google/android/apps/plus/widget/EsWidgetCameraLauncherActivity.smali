.class public Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "EsWidgetCameraLauncherActivity.java"


# instance fields
.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity$1;-><init>(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->insertCameraPhoto(Ljava/lang/String;)V

    return-void
.end method

.method private insertCameraPhoto(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;)Landroid/content/Intent;

    move-result-object v9

    const-string v1, "account"

    invoke-virtual {v9, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    const-string v1, "audience"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "audience"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/AudienceData;

    const-string v1, "audience"

    invoke-virtual {v9, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    const v1, 0x7f0a003e

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->dismissDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->finish()V

    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->camera_photo_error:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const v1, 0x7f0a003e

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->showDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "camera-p.jpg"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_1

    const-string v1, "camera-p.jpg"

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntentPhoto$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "insert_camera_photo_req_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "insert_camera_photo_req_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v0, 0x7f0a003e

    if-ne p1, v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lvedroid/support/v4/app/FragmentActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->insertCameraPhoto(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "insert_camera_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetCameraLauncherActivity;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
