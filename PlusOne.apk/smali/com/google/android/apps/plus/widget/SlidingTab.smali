.class public Lcom/google/android/apps/plus/widget/SlidingTab;
.super Landroid/view/ViewGroup;
.source "SlidingTab.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/widget/SlidingTab$Slider;,
        Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;
    }
.end annotation


# instance fields
.field private currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

.field private density:F

.field private leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

.field private mVibrator:Landroid/os/Vibrator;

.field private onTriggerListener:Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;

.field private rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

.field private targetZone:F

.field private tracking:Z

.field private triggered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/widget/SlidingTab;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->triggered:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->density:F

    new-instance v0, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_sliding_tab_jog_left:I

    sget v3, Lcom/google/android/apps/plus/R$drawable;->sliding_tab_jog_tab_target_green:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->sliding_tab_jog_tab_bar_left:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->sliding_tab_jog_tab_left:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;-><init>(Landroid/view/ViewGroup;IIII)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    new-instance v0, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_sliding_tab_jog_right:I

    sget v3, Lcom/google/android/apps/plus/R$drawable;->sliding_tab_jog_tab_target_red:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->sliding_tab_jog_tab_bar_right:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->sliding_tab_jog_tab_right:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;-><init>(Landroid/view/ViewGroup;IIII)V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    sget v0, Lcom/google/android/apps/plus/R$string;->decline_call:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/SlidingTab;->setRightHintText(I)V

    sget v0, Lcom/google/android/apps/plus/R$string;->take_call:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/SlidingTab;->setLeftHintText(I)V

    return-void
.end method

.method private declared-synchronized vibrate(J)V
    .locals 2
    .param p1    # J

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0, p1, p2}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    # getter for: Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;
    invoke-static {v10}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->access$000(Lcom/google/android/apps/plus/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    float-to-int v10, v6

    float-to-int v11, v7

    invoke-virtual {v1, v10, v11}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    iget-object v10, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    # getter for: Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;
    invoke-static {v10}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->access$000(Lcom/google/android/apps/plus/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    float-to-int v10, v6

    float-to-int v11, v7

    invoke-virtual {v1, v10, v11}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    iget-boolean v10, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    if-nez v10, :cond_0

    if-nez v3, :cond_0

    if-nez v5, :cond_0

    :goto_0
    return v8

    :cond_0
    packed-switch v0, :pswitch_data_0

    :goto_1
    move v8, v9

    goto :goto_0

    :pswitch_0
    iput-boolean v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    iput-boolean v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->triggered:Z

    const-wide/16 v10, 0x1e

    invoke-direct {p0, v10, v11}, Lcom/google/android/apps/plus/widget/SlidingTab;->vibrate(J)V

    if-eqz v3, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    iput-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    const v8, 0x3f2aaaab

    iput v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->targetZone:F

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->hide()V

    :goto_2
    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setState(I)V

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->showTarget()V

    goto :goto_1

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    iput-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    const v8, 0x3eaaaaaa

    iput v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->targetZone:F

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->hide()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    const/4 v5, 0x0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->layout(IIIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    const/4 v5, 0x1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->layout(IIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->invalidate()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/high16 v9, 0x3f000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    iget v7, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->density:F

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->getTabWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v2, v7

    iget v7, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->density:F

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->getTabHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v1, v7

    iget v7, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->density:F

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->getTabWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v4, v7

    iget v7, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->density:F

    iget-object v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->getTabHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v7, v9

    float-to-int v3, v7

    add-int v7, v2, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0, v5, v0}, Lcom/google/android/apps/plus/widget/SlidingTab;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-boolean v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    if-eqz v9, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    # getter for: Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->access$000(Lcom/google/android/apps/plus/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    move-result-object v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-boolean v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    if-nez v9, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    move v8, v7

    :cond_2
    return v8

    :pswitch_0
    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    # getter for: Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->tab:Landroid/widget/ImageView;
    invoke-static {v9}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->access$000(Lcom/google/android/apps/plus/widget/SlidingTab$Slider;)Landroid/widget/ImageView;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    # getter for: Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->text:Landroid/widget/TextView;
    invoke-static {v10}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->access$100(Lcom/google/android/apps/plus/widget/SlidingTab$Slider;)Landroid/widget/TextView;

    move-result-object v10

    float-to-int v11, v5

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int/2addr v11, v12

    invoke-virtual {v9, v11}, Landroid/view/View;->offsetLeftAndRight(I)V

    invoke-virtual {v10, v11}, Landroid/view/View;->offsetLeftAndRight(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->invalidate()V

    iget v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->targetZone:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getWidth()I

    move-result v10

    int-to-float v10, v10

    mul-float v3, v9, v10

    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    iget-object v10, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    if-ne v9, v10, :cond_6

    cmpl-float v9, v5, v3

    if-lez v9, :cond_5

    move v4, v7

    :goto_1
    iget-boolean v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->triggered:Z

    if-nez v9, :cond_3

    if-eqz v4, :cond_3

    iput-boolean v7, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->triggered:Z

    iput-boolean v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setState(I)V

    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->currentSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    iget-object v10, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    if-ne v9, v10, :cond_8

    const/4 v2, 0x0

    :goto_2
    const-wide/16 v9, 0x28

    invoke-direct {p0, v9, v10}, Lcom/google/android/apps/plus/widget/SlidingTab;->vibrate(J)V

    packed-switch v2, :pswitch_data_1

    :cond_3
    :goto_3
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v9

    int-to-float v9, v9

    cmpg-float v9, v6, v9

    if-gtz v9, :cond_4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v9, v6, v9

    if-gez v9, :cond_0

    :cond_4
    iput-boolean v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    iput-boolean v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->triggered:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->resetView()V

    goto :goto_0

    :cond_5
    move v4, v8

    goto :goto_1

    :cond_6
    cmpg-float v9, v5, v3

    if-gez v9, :cond_7

    move v4, v7

    goto :goto_1

    :cond_7
    move v4, v8

    goto :goto_1

    :cond_8
    const/4 v2, 0x1

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x10a0003

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v9

    :goto_4
    new-instance v10, Lcom/google/android/apps/plus/widget/SlidingTab$1;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/widget/SlidingTab$1;-><init>(Lcom/google/android/apps/plus/widget/SlidingTab;)V

    invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/widget/SlidingTab;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->onTriggerListener:Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->onTriggerListener:Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;

    goto :goto_3

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getContext()Landroid/content/Context;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$anim;->slide_out_left:I

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v9

    goto :goto_4

    :pswitch_3
    iput-boolean v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->tracking:Z

    iput-boolean v8, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->triggered:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->resetView()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final resetView()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->reset()V

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getWidth()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/SlidingTab;->getHeight()I

    move-result v5

    add-int/2addr v5, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/SlidingTab;->onLayout(ZIIII)V

    return-void
.end method

.method public setLeftHintText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setHintText(I)V

    return-void
.end method

.method public setLeftTabDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # Landroid/graphics/drawable/Drawable;
    .param p4    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setLeftTabResources(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->leftSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setResources(IIII)V

    return-void
.end method

.method public setOnTriggerListener(Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->onTriggerListener:Lcom/google/android/apps/plus/widget/SlidingTab$OnTriggerListener;

    return-void
.end method

.method public setRightHintText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setHintText(I)V

    return-void
.end method

.method public setRightTabDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/graphics/drawable/Drawable;
    .param p3    # Landroid/graphics/drawable/Drawable;
    .param p4    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setRightTabResources(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/SlidingTab;->rightSlider:Lcom/google/android/apps/plus/widget/SlidingTab$Slider;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/widget/SlidingTab$Slider;->setResources(IIII)V

    return-void
.end method
