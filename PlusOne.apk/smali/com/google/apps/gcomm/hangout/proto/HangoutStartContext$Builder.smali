.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HangoutStartContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private activityId_:Ljava/lang/Object;

.field private appData_:Ljava/lang/Object;

.field private appId_:Ljava/lang/Object;

.field private bitField0_:I

.field private callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

.field private circleId_:Ljava/lang/Object;

.field private contextId_:Ljava/lang/Object;

.field private create_:Z

.field private dEPRECATEDCallback_:Z

.field private externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

.field private flippy_:Z

.field private hangoutId_:Ljava/lang/Object;

.field private hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

.field private invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

.field private nick_:Ljava/lang/Object;

.field private profileId_:Lcom/google/protobuf/LazyStringList;

.field private referringUrl_:Ljava/lang/Object;

.field private shouldAutoInvite_:Z

.field private shouldMuteVideo_:Z

.field private source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

.field private topic_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutId_:Ljava/lang/Object;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->topic_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->referringUrl_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->circleId_:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->activityId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appData_:Ljava/lang/Object;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;->SANDBAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->nick_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;->NONE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->contextId_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$2500()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 1

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;-><init>()V

    return-object v0
.end method

.method private clear()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->topic_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->referringUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->circleId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->activityId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appData_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->flippy_:Z

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->dEPRECATEDCallback_:Z

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;->SANDBAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->create_:Z

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->nick_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;->NONE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldAutoInvite_:Z

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->contextId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldMuteVideo_:Z

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    return-object p0
.end method

.method private clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 2

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureInviteeIsMutable()V
    .locals 3

    const/high16 v2, 0x40000

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureProfileIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 10
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v9, 0x20000

    const v8, 0x8000

    const/4 v5, 0x1

    const/4 v6, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    goto :goto_0

    :sswitch_3
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->topic_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->referringUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->circleId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->ensureProfileIdIsMutable()V

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v7

    invoke-interface {v4, v7}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_7
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->activityId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_8
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x80

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_9
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appData_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x200

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->flippy_:Z

    goto/16 :goto_0

    :sswitch_b
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->dEPRECATEDCallback_:Z

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    goto/16 :goto_0

    :sswitch_d
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    move-result-object v1

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v7, 0x1000

    if-ne v4, v7, :cond_2

    move v4, v5

    :goto_1
    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-virtual {v1, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_2
    move v4, v6

    goto :goto_1

    :cond_3
    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x1000

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    goto/16 :goto_0

    :sswitch_e
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->create_:Z

    goto/16 :goto_0

    :sswitch_f
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x4000

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->nick_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_10
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v1

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/2addr v4, v8

    if-ne v4, v8, :cond_5

    move v4, v5

    :goto_2
    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-virtual {v1, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    :cond_4
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v4

    if-nez v4, :cond_6

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_5
    move v4, v6

    goto :goto_2

    :cond_6
    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/2addr v4, v8

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;->valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v7, 0x10000

    or-int/2addr v4, v7

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    goto/16 :goto_0

    :sswitch_12
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->newBuilder()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v1

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/2addr v4, v9

    if-ne v4, v9, :cond_8

    move v4, v5

    :goto_3
    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-virtual {v1, v4}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    :cond_7
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v4

    if-nez v4, :cond_9

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_8
    move v4, v6

    goto :goto_3

    :cond_9
    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/2addr v4, v9

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    goto/16 :goto_0

    :sswitch_13
    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;->newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;

    move-result-object v4

    if-nez v4, :cond_a

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_a
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->ensureInviteeIsMutable()V

    iget-object v7, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_14
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v7, 0x80000

    or-int/2addr v4, v7

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldAutoInvite_:Z

    goto/16 :goto_0

    :sswitch_15
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v7, 0x100000

    or-int/2addr v4, v7

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->contextId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_16
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v7, 0x200000

    or-int/2addr v4, v7

    iput v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldMuteVideo_:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb0 -> :sswitch_16
    .end sparse-switch
.end method


# virtual methods
.method public final buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .locals 10

    const/high16 v9, 0x80000

    const/high16 v8, 0x40000

    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    new-instance v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;B)V

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutId_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$2702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$2802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->topic_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$2902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->referringUrl_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3002(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->circleId_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3102(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_5
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3202(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x20

    :cond_6
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->activityId_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit8 v2, v2, 0x40

    :cond_7
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appId_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x80

    :cond_8
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appData_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x100

    :cond_9
    iget-boolean v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->flippy_:Z

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->flippy_:Z
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z

    and-int/lit16 v3, v0, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x200

    :cond_a
    iget-boolean v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->dEPRECATEDCallback_:Z

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->dEPRECATEDCallback_:Z
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z

    and-int/lit16 v3, v0, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    or-int/lit16 v2, v2, 0x400

    :cond_b
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    and-int/lit16 v3, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    or-int/lit16 v2, v2, 0x800

    :cond_c
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    and-int/lit16 v3, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    or-int/lit16 v2, v2, 0x1000

    :cond_d
    iget-boolean v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->create_:Z

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->create_:Z
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4002(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z

    and-int/lit16 v3, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    or-int/lit16 v2, v2, 0x2000

    :cond_e
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->nick_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4102(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    and-int v3, v0, v5

    if-ne v3, v5, :cond_f

    or-int/lit16 v2, v2, 0x4000

    :cond_f
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4202(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    and-int v3, v0, v6

    if-ne v3, v6, :cond_10

    or-int/2addr v2, v5

    :cond_10
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    and-int v3, v0, v7

    if-ne v3, v7, :cond_11

    or-int/2addr v2, v6

    :cond_11
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    iget v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/2addr v3, v8

    if-ne v3, v8, :cond_12

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    iget v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v4, -0x40001

    and-int/2addr v3, v4

    iput v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_12
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/util/List;)Ljava/util/List;

    and-int v3, v0, v9

    if-ne v3, v9, :cond_13

    or-int/2addr v2, v7

    :cond_13
    iget-boolean v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldAutoInvite_:Z

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldAutoInvite_:Z
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z

    const/high16 v3, 0x100000

    and-int/2addr v3, v0

    const/high16 v4, 0x100000

    if-ne v3, v4, :cond_14

    or-int/2addr v2, v8

    :cond_14
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->contextId_:Ljava/lang/Object;

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;

    const/high16 v3, 0x200000

    and-int/2addr v3, v0

    const/high16 v4, 0x200000

    if-ne v3, v4, :cond_15

    or-int/2addr v2, v9

    :cond_15
    iget-boolean v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldMuteVideo_:Z

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldMuteVideo_:Z
    invoke-static {v1, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;I)I

    return-object v1
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 5
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    const/high16 v4, 0x20000

    const v3, 0x8000

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasHangoutId()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutId_:Ljava/lang/Object;

    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasHangoutType()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    :cond_5
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasTopic()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getTopic()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->topic_:Ljava/lang/Object;

    :cond_7
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasReferringUrl()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getReferringUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->referringUrl_:Ljava/lang/Object;

    :cond_9
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasCircleId()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getCircleId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->circleId_:Ljava/lang/Object;

    :cond_b
    # getter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3200(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    # getter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3200(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_c
    :goto_1
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasActivityId()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getActivityId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->ensureProfileIdIsMutable()V

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->profileId_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$3200(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_e
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->activityId_:Ljava/lang/Object;

    :cond_f
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasAppId()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getAppId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appId_:Ljava/lang/Object;

    :cond_11
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasAppData()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getAppData()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->appData_:Ljava/lang/Object;

    :cond_13
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasFlippy()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getFlippy()Z

    move-result v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->flippy_:Z

    :cond_14
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasDEPRECATEDCallback()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDEPRECATEDCallback()Z

    move-result v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->dEPRECATEDCallback_:Z

    :cond_15
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getSource()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    move-result-object v0

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    :cond_17
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasInvitation()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getInvitation()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1a

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v2

    if-eq v1, v2, :cond_1a

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-static {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    :goto_2
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_18
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasCreate()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getCreate()Z

    move-result v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->create_:Z

    :cond_19
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasNick()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNick()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1a
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    goto :goto_2

    :cond_1b
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->nick_:Ljava/lang/Object;

    :cond_1c
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasLatencyMarks()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getLatencyMarks()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/2addr v1, v3

    if-ne v1, v3, :cond_1e

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v2

    if-eq v1, v2, :cond_1e

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-static {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    :goto_3
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_1d
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasCallback()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getCallback()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    move-result-object v0

    if-nez v0, :cond_1f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1e
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    goto :goto_3

    :cond_1f
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v2, 0x10000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    :cond_20
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasExternalKey()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getExternalKey()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    and-int/2addr v1, v4

    if-ne v1, v4, :cond_24

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v2

    if-eq v1, v2, :cond_24

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-static {v1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->newBuilder(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    :goto_4
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    or-int/2addr v0, v4

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_21
    # getter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4500(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_25

    # getter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4500(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    :cond_22
    :goto_5
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasShouldAutoInvite()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getShouldAutoInvite()Z

    move-result v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldAutoInvite_:Z

    :cond_23
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasContextId()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getContextId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_26

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_24
    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    goto :goto_4

    :cond_25
    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->ensureInviteeIsMutable()V

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->invitee_:Ljava/util/List;

    # getter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->access$4500(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_5

    :cond_26
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v2, 0x100000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->contextId_:Ljava/lang/Object;

    :cond_27
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasShouldMuteVideo()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getShouldMuteVideo()Z

    move-result v0

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    const/high16 v2, 0x200000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->bitField0_:I

    iput-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->shouldMuteVideo_:Z

    goto/16 :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method
