.class public final Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "ExternalEntityKey.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private domain_:Ljava/lang/Object;

.field private id_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;-><init>()V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;)V
    .locals 2
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    invoke-direct {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;-><init>(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;I)I
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .param p1    # I

    iput p1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .locals 1

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    return-object v0
.end method

.method private getDomainBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->access$100()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;
    .locals 1
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->access$100()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getDomain()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->domain_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->id_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDomainBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final hasDomain()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasId()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedIsInitialized:B

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->hasDomain()Z

    move-result v3

    if-nez v3, :cond_2

    iput-byte v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->hasId()Z

    move-result v3

    if-nez v3, :cond_3

    iput-byte v2, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_3
    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getSerializedSize()I

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDomainBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    return-void
.end method
