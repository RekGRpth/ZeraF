.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;
.super Landroid/os/AsyncTask;
.source "PicasaUploadAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCachedAlbumListTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mErrorCode:I

.field private mLastAlbumId:J

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;
    .param p2    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;

    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;-><init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1    # [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v11, 0x0

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mAccount:Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mDbHelper:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$700(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$800()Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->ALBUM_TABLE_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$900()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "account=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mAccount:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "album_id_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$500(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v1, v9, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mLastAlbumId:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v8

    :catch_0
    move-exception v10

    const-string v1, "PicasaUploadAsyncTask"

    const-string v2, "get album list from DB"

    invoke-static {v1, v2, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/4 v1, 0x5

    iput v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mErrorCode:I

    move-object v8, v11

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->doInBackground([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/database/Cursor;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mGetCachedAlbumListTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$1000(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;

    move-result-object v0

    if-eq p0, v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mErrorCode:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mAccount:Ljava/lang/String;

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mLastAlbumId:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onAlbumListReady(Ljava/lang/String;Landroid/database/Cursor;ZJ)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mErrorCode:I

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->mAccount:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onError(ILjava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$GetCachedAlbumListTask;->onPostExecute(Landroid/database/Cursor;)V

    return-void
.end method
