.class Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;
.super Ljava/lang/Object;
.source "PicasaUploadAsyncTask.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->addAccount(Landroid/app/Activity;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v3}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onAddAccountResult(Z)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    const-string v3, "setupSkipped"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v3}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onAddAccountResult(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v3, "PicasaUploadAsyncTask"

    const-string v4, "fail to add acount"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$1;->this$0:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->mListener:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;
    invoke-static {v3}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->access$100(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;)Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;->onError(ILjava/lang/String;)V

    goto :goto_0
.end method
