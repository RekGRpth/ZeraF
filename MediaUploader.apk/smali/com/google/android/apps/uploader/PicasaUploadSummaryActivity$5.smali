.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$5;
.super Ljava/lang/Object;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->createTabsApi11OrLater(Lvedroid/support/v4/app/FragmentManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$5;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 3
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$5;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Lvedroid/support/v4/view/ViewPager;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$5;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;
    invoke-static {v1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$300(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Lvedroid/support/v4/app/ListFragment;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1    # Landroid/app/ActionBar$Tab;
    .param p2    # Landroid/app/FragmentTransaction;

    return-void
.end method
