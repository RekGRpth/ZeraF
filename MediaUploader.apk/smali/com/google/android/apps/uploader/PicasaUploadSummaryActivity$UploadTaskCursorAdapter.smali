.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;
.super Lvedroid/support/v4/widget/ResourceCursorAdapter;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadTaskCursorAdapter"
.end annotation


# instance fields
.field private final mResolver:Landroid/content/ContentResolver;

.field final synthetic this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    const v0, 0x7f030007

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;I)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;I)V
    .locals 2
    .param p2    # Landroid/content/Context;
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p2, p3, v0, v1}, Lvedroid/support/v4/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->mResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method private createFutureTask(Landroid/content/ContentResolver;Landroid/net/Uri;J)Ljava/util/concurrent/FutureTask;
    .locals 7
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Landroid/net/Uri;
    .param p3    # J

    new-instance v6, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;Landroid/content/ContentResolver;Landroid/net/Uri;J)V

    invoke-direct {v6, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v6
.end method

.method private onClickSynchronized(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/picasasync/UploadTaskEntry;

    iget-wide v3, v6, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$400(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    if-nez v0, :cond_0

    const-string v7, "PicasaUploadSummary"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cancel upload "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-static {v3, v4}, Lcom/google/android/picasasync/PicasaFacade;->getUploadUri(J)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {p0, v7, v8, v3, v4}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->createFutureTask(Landroid/content/ContentResolver;Landroid/net/Uri;J)Ljava/util/concurrent/FutureTask;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$400(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v7

    invoke-virtual {v7, v3, v4, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mExecutorService:Ljava/util/concurrent/ExecutorService;
    invoke-static {v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$500(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    const/4 v5, 0x7

    :goto_0
    const v7, 0x7f04001d

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateMessage(I)I
    invoke-static {v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$600(I)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateColor(I)I
    invoke-static {v5}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$700(I)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v7}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$400(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v7

    invoke-virtual {v7, v3, v4}, Landroid/util/LongSparseArray;->remove(J)V

    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    invoke-virtual {v6}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v5

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 13
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    invoke-static/range {p3 .. p3}, Lcom/google/android/picasasync/UploadTaskEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/picasasync/UploadTaskEntry;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;
    invoke-static {v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$400(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;

    move-result-object v0

    iget-wide v3, v12, Lcom/google/android/picasasync/UploadTaskEntry;->id:J

    invoke-virtual {v0, v3, v4}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/FutureTask;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v10, :cond_0

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getState()I

    move-result v8

    :goto_0
    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getAlbumTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v6

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getThumbnail()[B

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->bindViewCommon(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI[B)V

    const v0, 0x7f040020

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ProgressBar;

    const/4 v0, 0x1

    if-ne v8, v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {v12}, Lcom/google/android/picasasync/UploadTaskEntry;->getPercentageUploaded()I

    move-result v0

    invoke-virtual {v11, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    :goto_1
    invoke-virtual {p1, v12}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    const/4 v8, 0x7

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v11, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method protected final bindViewCommon(Landroid/view/View;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI[B)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # J
    .param p8    # I
    .param p9    # [B

    const-string v5, "@"

    invoke-virtual {p3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v5, 0x0

    invoke-virtual {p3, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p3

    :cond_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object p5, p3

    :goto_0
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$800(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p4

    :cond_1
    const/4 v1, 0x0

    if-eqz p9, :cond_2

    const/4 v5, 0x0

    :try_start_0
    move-object/from16 v0, p9

    array-length v6, v0

    move-object/from16 v0, p9

    invoke-static {v0, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_2
    :goto_1
    if-nez v1, :cond_4

    const v5, 0x7f040018

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    const v6, 0x7f020002

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_2
    const v5, 0x7f040019

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-lez v5, :cond_5

    # getter for: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sFormatter:Landroid/text/format/Formatter;
    invoke-static {}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$900()Landroid/text/format/Formatter;

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->this$0:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    invoke-static {v5, p6, p7}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    :goto_3
    const v5, 0x7f04001c

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v5, 0x7f04001d

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateMessage(I)I
    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$600(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    const v5, 0x7f04001d

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    # invokes: Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateColor(I)I
    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->access$700(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    const v5, 0x7f04001b

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    goto/16 :goto_0

    :catch_0
    move-exception v4

    const-string v5, "PicasaUploadSummary"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "failed to generate thumbnail for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    const v5, 0x7f040018

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    :cond_5
    const-string v2, ""

    goto/16 :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;->onClickSynchronized(Landroid/view/View;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
