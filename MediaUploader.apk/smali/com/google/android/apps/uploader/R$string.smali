.class public final Lcom/google/android/apps/uploader/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final album_title_hint:I = 0x7f050024

.field public static final cancel:I = 0x7f050028

.field public static final connectivity_is_back:I = 0x7f050033

.field public static final create_album:I = 0x7f050027

.field public static final creating_album:I = 0x7f050029

.field public static final creating_album_done:I = 0x7f05002c

.field public static final creating_album_failed:I = 0x7f05002a

.field public static final creating_album_failed_no_network:I = 0x7f05002b

.field public static final network_error:I = 0x7f050030

.field public static final new_album_dialog_title:I = 0x7f050023

.field public static final no_active_upload:I = 0x7f05002e

.field public static final no_history:I = 0x7f05002f

.field public static final picasa_title_for_multiple:I = 0x7f050003

.field public static final picasa_title_for_single:I = 0x7f050002

.field public static final picasa_upload:I = 0x7f050000

.field public static final picasa_upload_account:I = 0x7f050005

.field public static final picasa_upload_account_error:I = 0x7f05000d

.field public static final picasa_upload_actionbar_tab_active_uploads:I = 0x7f050021

.field public static final picasa_upload_actionbar_tab_history:I = 0x7f050022

.field public static final picasa_upload_add_account:I = 0x7f05000b

.field public static final picasa_upload_album:I = 0x7f050006

.field public static final picasa_upload_caption:I = 0x7f050004

.field public static final picasa_upload_done:I = 0x7f05000a

.field public static final picasa_upload_get_album_list_error:I = 0x7f05000e

.field public static final picasa_upload_get_album_list_error_no_network:I = 0x7f05000f

.field public static final picasa_upload_paused:I = 0x7f050016

.field public static final picasa_upload_paused_auth:I = 0x7f050015

.field public static final picasa_upload_paused_missing_sdcard:I = 0x7f050013

.field public static final picasa_upload_paused_network:I = 0x7f050011

.field public static final picasa_upload_paused_quota_exceeded:I = 0x7f050014

.field public static final picasa_upload_paused_server_error:I = 0x7f050012

.field public static final picasa_upload_prepare:I = 0x7f050008

.field public static final picasa_upload_state_cancelled:I = 0x7f050019

.field public static final picasa_upload_state_cancelling:I = 0x7f05001e

.field public static final picasa_upload_state_duplicate:I = 0x7f05001d

.field public static final picasa_upload_state_empty:I = 0x7f050020

.field public static final picasa_upload_state_failed:I = 0x7f050018

.field public static final picasa_upload_state_invalid_metadata:I = 0x7f05001c

.field public static final picasa_upload_state_missing_file:I = 0x7f05001f

.field public static final picasa_upload_state_queued:I = 0x7f050017

.field public static final picasa_upload_state_quota_exceeded:I = 0x7f05001b

.field public static final picasa_upload_state_unauthorized:I = 0x7f05001a

.field public static final picasa_upload_toast_no_file_to_upload:I = 0x7f050010

.field public static final picasa_upload_upload:I = 0x7f050007

.field public static final picasa_upload_upload_start:I = 0x7f05000c

.field public static final picasa_upload_uploading_to:I = 0x7f050009

.field public static final public_album:I = 0x7f050025

.field public static final reload_albums:I = 0x7f050032

.field public static final retry:I = 0x7f050031

.field public static final reupload:I = 0x7f05002d

.field public static final share_to_picasa:I = 0x7f050001

.field public static final unlisted_album:I = 0x7f050026


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
