.class public Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "PicasaUploadSummaryActivity.java"

# interfaces
.implements Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MySummaryListFragment;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTaskListFragment;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;,
        Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lvedroid/support/v4/app/FragmentActivity;",
        "Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final sDateFormat:Ljava/text/DateFormat;

.field private static final sFormatter:Landroid/text/format/Formatter;


# instance fields
.field private mCancelFutureTasks:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;>;"
        }
    .end annotation
.end field

.field private mExecutorService:Ljava/util/concurrent/ExecutorService;

.field private mPreviousRequestCount:I

.field private mTabPager:Lvedroid/support/v4/view/ViewPager;

.field private mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

.field private mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

.field private mUploadRecordsObserver:Landroid/database/ContentObserver;

.field private mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

.field private mUploadTasksObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/text/format/Formatter;

    invoke-direct {v0}, Landroid/text/format/Formatter;-><init>()V

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sFormatter:Landroid/text/format/Formatter;

    invoke-static {}, Ljava/text/DateFormat;->getDateInstance()Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sDateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;

    return-void
.end method

.method static synthetic access$1000(J)Ljava/lang/String;
    .locals 1
    .param p0    # J

    invoke-static {p0, p1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getDate(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Lvedroid/support/v4/view/ViewPager;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Lvedroid/support/v4/app/ListFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Landroid/util/LongSparseArray;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mCancelFutureTasks:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mExecutorService:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$600(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateMessage(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(I)I
    .locals 1
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getStateColor(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900()Landroid/text/format/Formatter;
    .locals 1

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sFormatter:Landroid/text/format/Formatter;

    return-object v0
.end method

.method private createTabsApi10(Lvedroid/support/v4/app/FragmentManager;)V
    .locals 5
    .param p1    # Lvedroid/support/v4/app/FragmentManager;

    const v4, 0x7f04001f

    const v2, 0x7f04001e

    invoke-virtual {p0, v2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->setup()V

    const-string v2, "tab1"

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const v3, 0x7f050021

    invoke-virtual {p0, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    const-string v2, "tab2"

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const v3, 0x7f050022

    invoke-virtual {p0, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/TabHost$TabSpec;->setContent(I)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$3;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$4;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$4;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/widget/TabHost;)V

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;-><init>(Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/view/ViewPager;)V

    iput-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->addTabContent(Lvedroid/support/v4/app/Fragment;)V

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->addTabContent(Lvedroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private createTabsApi11OrLater(Lvedroid/support/v4/app/FragmentManager;)V
    .locals 7
    .param p1    # Lvedroid/support/v4/app/FragmentManager;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    const/4 v5, 0x0

    const/16 v6, 0xa

    invoke-virtual {v0, v5, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    new-instance v4, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$5;

    invoke-direct {v4, p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$5;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;)V

    new-instance v1, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$6;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/app/ActionBar;)V

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v5, v1}, Lvedroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lvedroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f050021

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v2, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    invoke-virtual {v2, v4}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    invoke-virtual {v0}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v5

    const v6, 0x7f050022

    invoke-virtual {v5, v6}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    invoke-virtual {v3, v4}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    new-instance v5, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-direct {v5, p1, v6}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;-><init>(Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/view/ViewPager;)V

    iput-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->addTabContent(Lvedroid/support/v4/app/Fragment;)V

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPagerAdapter:Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->addTabContent(Lvedroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private static getAlternativeDisplayName(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/net/Uri;

    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    const-string v1, "/"

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getDate(J)Ljava/lang/String;
    .locals 2
    .param p0    # J

    sget-object v0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->sDateFormat:Ljava/text/DateFormat;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getStateColor(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const v0, -0x777778

    goto :goto_0

    :pswitch_2
    const/high16 v0, -0x10000

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static getStateMessage(I)I
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f050020

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f050017

    goto :goto_0

    :pswitch_2
    const v0, 0x7f050018

    goto :goto_0

    :pswitch_3
    const v0, 0x7f050019

    goto :goto_0

    :pswitch_4
    const v0, 0x7f05001a

    goto :goto_0

    :pswitch_5
    const v0, 0x7f05001b

    goto :goto_0

    :pswitch_6
    const v0, 0x7f05001c

    goto :goto_0

    :pswitch_7
    const v0, 0x7f05001d

    goto :goto_0

    :pswitch_8
    const v0, 0x7f05001e

    goto :goto_0

    :pswitch_9
    const v0, 0x7f05001f

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
    .end packed-switch
.end method

.method private initApi11OrLater()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->requestWindowFeature(I)Z

    new-instance v0, Lvedroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0}, Lvedroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Lvedroid/support/v4/view/ViewPager;->setId(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/16 v11, 0xb

    const/high16 v10, 0x7f040000

    const/4 v9, 0x0

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v11, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->initApi11OrLater()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const-string v1, "tag-upload-tasks"

    const-string v0, "tag-upload-records"

    const-string v6, "tag-upload-tasks"

    invoke-virtual {v2, v6}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v6

    check-cast v6, Lvedroid/support/v4/app/ListFragment;

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    const-string v6, "tag-upload-records"

    invoke-virtual {v2, v6}, Lvedroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v6

    check-cast v6, Lvedroid/support/v4/app/ListFragment;

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    if-nez v6, :cond_0

    new-instance v6, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTaskListFragment;

    invoke-direct {v6, p0, v8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTaskListFragment;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;)V

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;

    invoke-direct {v7, p0, p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadTaskCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V

    invoke-virtual {v6, v7}, Lvedroid/support/v4/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    const-string v7, "tag-upload-tasks"

    invoke-virtual {v5, v10, v6, v7}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    if-nez v6, :cond_1

    new-instance v6, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MySummaryListFragment;

    invoke-direct {v6, p0, v8}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MySummaryListFragment;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;)V

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    new-instance v7, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;

    invoke-direct {v7, p0, p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$UploadRecordCursorAdapter;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/content/Context;)V

    invoke-virtual {v6, v7}, Lvedroid/support/v4/app/ListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    const-string v7, "tag-upload-records"

    invoke-virtual {v5, v10, v6, v7}, Lvedroid/support/v4/app/FragmentTransaction;->add(ILvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/FragmentTransaction;

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v5, v6}, Lvedroid/support/v4/app/FragmentTransaction;->hide(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v5, v6}, Lvedroid/support/v4/app/FragmentTransaction;->hide(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v5}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v2}, Lvedroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v6, v11, :cond_3

    invoke-direct {p0, v2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->createTabsApi11OrLater(Lvedroid/support/v4/app/FragmentManager;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v6

    invoke-virtual {v6, v9, v8, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getSupportLoaderManager()Lvedroid/support/v4/app/LoaderManager;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v8, p0}, Lvedroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Lvedroid/support/v4/app/LoaderManager$LoaderCallbacks;)Lvedroid/support/v4/content/Loader;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;

    invoke-direct {v6, p0, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$1;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/os/Handler;)V

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTasksObserver:Landroid/database/ContentObserver;

    new-instance v6, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$2;

    invoke-direct {v6, p0, v3}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$2;-><init>(Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;Landroid/os/Handler;)V

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTasksObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v6, v9, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    sget-object v6, Lcom/google/android/picasasync/PicasaFacade;->uploadRecordsUri:Landroid/net/Uri;

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v6, v9, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void

    :cond_2
    const v6, 0x7f030005

    invoke-virtual {p0, v6}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->setContentView(I)V

    invoke-virtual {p0, v10}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lvedroid/support/v4/view/ViewPager;

    iput-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->createTabsApi10(Lvedroid/support/v4/app/FragmentManager;)V

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Lvedroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Lvedroid/support/v4/content/CursorLoader;

    sget-object v2, Lcom/google/android/picasasync/PicasaFacade;->uploadsUri:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/picasasync/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v3

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lvedroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Lvedroid/support/v4/content/CursorLoader;

    sget-object v2, Lcom/google/android/picasasync/PicasaFacade;->uploadRecordsUri:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/picasasync/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v3

    const-string v6, "uploaded_time DESC"

    move-object v1, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lvedroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTasksObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v0}, Lvedroid/support/v4/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Lvedroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    const v1, 0x7f05000a

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/picasasync/PicasaUploadService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.uploader.CANCEL_NOTIFICATION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v3, v3}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    :cond_0
    :goto_0
    iput v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mPreviousRequestCount:I

    :goto_1
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mPreviousRequestCount:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mTabPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2, v3}, Lvedroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v0}, Lvedroid/support/v4/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Lvedroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Lvedroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lvedroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->onLoadFinished(Lvedroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Lvedroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Lvedroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadTaskFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v0}, Lvedroid/support/v4/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->mUploadRecordsFragment:Lvedroid/support/v4/app/ListFragment;

    invoke-virtual {v0}, Lvedroid/support/v4/app/ListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Lvedroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;->finish()V

    return-void
.end method
