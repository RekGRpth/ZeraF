.class public Lcom/google/android/apps/uploader/PicasaUploadActivity;
.super Lvedroid/support/v4/app/FragmentActivity;
.source "PicasaUploadActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/uploader/PicasaUploadActivity$1;,
        Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;
    }
.end annotation


# instance fields
.field private mAccount:Ljava/lang/String;

.field private mAccountsSpinner:Landroid/widget/Spinner;

.field private mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

.field private mAlbumsSpinner:Landroid/widget/Spinner;

.field private mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

.field private mCancelButton:Landroid/widget/Button;

.field private mContentUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mNewAlbumButton:Landroid/widget/ImageButton;

.field private mNoConnectionView:Landroid/view/View;

.field private mProgressSpinner:Landroid/app/ProgressDialog;

.field private mReloadAlbumsButton:Landroid/widget/Button;

.field private mReloadingAlbumsView:Landroid/view/View;

.field private mUploadButton:Landroid/widget/Button;

.field private mUploadButtonView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lvedroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/uploader/PicasaUploadActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/uploader/PicasaUploadActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->addNewAlbum(Ljava/lang/String;I)V

    return-void
.end method

.method private addNewAlbum(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->showProgressDialog()V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->addAlbum(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private checkConnectivity()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->hasNetworkConnectivity()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadingAlbumsView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNewAlbumButton:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    const v0, 0x7f050033

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadingAlbumsView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButtonView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNewAlbumButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadingAlbumsView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButtonView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNoConnectionView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private getSelectedAlbum()Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    sget-object v1, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v2, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    invoke-direct {v2}, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;-><init>()V

    invoke-virtual {v1, v0, v2}, Lcom/android/gallery3d/common/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    return-object v1
.end method

.method private getUrisFromIntent()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object v2

    :cond_0
    const-string v3, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v3, "PicasaUploadActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid intent action ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private hasNetworkConnectivity()Z
    .locals 2

    const-string v1, "connectivity"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private hideProgressDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method private onAccountSelected(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "PicasaUploadActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account selected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getCachedAlbumList(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private selectAlbum(J)Z
    .locals 8
    .param p1    # J

    sget-object v6, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/common/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getCount()I

    move-result v5

    :goto_0
    if-ge v1, v5, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v6, v2, p1

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->setSelection(I)V

    const/4 v6, 0x1

    :goto_1
    return v6

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private selectDefaultAlbum()V
    .locals 8

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const-string v7, "is_default"

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/common/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getCount()I

    move-result v4

    :goto_0
    if-ge v2, v4, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v2}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-eqz v6, :cond_0

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v5, v2}, Landroid/widget/Spinner;->setSelection(I)V

    :goto_2
    return-void

    :cond_0
    move v3, v5

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v5}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_2
.end method

.method private showNewAlbumDialog()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;-><init>(Lcom/google/android/apps/uploader/PicasaUploadActivity;Lcom/google/android/apps/uploader/PicasaUploadActivity$1;)V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getSupportFragmentManager()Lvedroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "new-album-dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/uploader/PicasaUploadActivity$NewAlbumDialogFragment;->show(Lvedroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private showProgressDialog()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    const v1, 0x7f050029

    invoke-virtual {p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mProgressSpinner:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method private upload()V
    .locals 7

    const v0, 0x7f040004

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getSelectedAlbum()Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mContentUris:Ljava/util/ArrayList;

    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/google/android/apps/uploader/PicasaUploadProgressReceiver;

    invoke-direct {v5, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->submitUploads(Ljava/lang/String;Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/ComponentName;)V

    return-void
.end method


# virtual methods
.method public onAccountListReady([Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # [Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v3, "PicasaUploadActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "accounts ready: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ","

    invoke-static {v5, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    array-length v3, p1

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    const v4, 0x7f05000b

    invoke-virtual {p0, v4}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->addAccount(Landroid/app/Activity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v3, 0x1090008

    invoke-direct {v0, p0, v3, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    const v3, 0x1090009

    invoke-virtual {v0, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    if-eqz p2, :cond_3

    const/4 v1, 0x0

    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, p1, v1

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getCount()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method public onAddAccountResult(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onAlbumCreated(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;J)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Ljava/lang/String;
    .param p4    # J

    const/4 v8, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->hideProgressDialog()V

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v6, p2}, Landroid/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    sget-object v6, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const-string v7, "_id"

    invoke-virtual {v6, v7}, Lcom/android/gallery3d/common/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6}, Landroid/widget/Spinner;->getCount()I

    move-result v5

    :goto_0
    if-ge v1, v5, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v6, v2, p4

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v6, v1}, Landroid/widget/Spinner;->setSelection(I)V

    const v6, 0x7f05002c

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object p3, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "couldn\'t find newly created album"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public onAlbumListReady(Ljava/lang/String;Landroid/database/Cursor;ZJ)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z
    .param p4    # J

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_7

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-virtual {v7, p1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getAlbumListFromServer(Ljava/lang/String;)V

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    invoke-virtual {v4, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_3
    const-string v7, "PicasaUploadActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cached album list for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    sget-object v7, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const-string v8, "_id"

    invoke-virtual {v7, v8}, Lcom/android/gallery3d/common/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v7}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_8

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    :goto_2
    iget-object v7, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-lez v8, :cond_4

    const/4 v6, 0x1

    :cond_4
    invoke-virtual {v7, v6}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v6, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v6, p2}, Landroid/widget/SimpleCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    cmp-long v6, v2, v4

    if-eqz v6, :cond_5

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->selectAlbum(J)Z

    move-result v6

    if-nez v6, :cond_0

    :cond_5
    cmp-long v4, p4, v4

    if-eqz v4, :cond_6

    invoke-direct {p0, p4, p5}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->selectAlbum(J)Z

    move-result v4

    if-nez v4, :cond_0

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->selectDefaultAlbum()V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->checkConnectivity()V

    if-eqz p2, :cond_0

    const-string v7, "PicasaUploadActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "got album list for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_8
    move-wide v2, v4

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->upload()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNewAlbumButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->showNewAlbumDialog()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadAlbumsButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNoConnectionView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadingAlbumsView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getAlbumListFromServer(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "PicasaUploadActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "whats clicked? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lvedroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getUrisFromIntent()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mContentUris:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mContentUris:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mContentUris:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "PicasaUploadActivity"

    const-string v1, "no file to upload"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const v0, 0x7f050010

    invoke-static {p0, v0, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->setContentView(I)V

    const v0, 0x7f040005

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v0, 0x7f040006

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    const v2, 0x1090008

    const/4 v3, 0x0

    new-array v4, v9, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v4, v8

    new-array v5, v9, [I

    const v1, 0x1020014

    aput v1, v5, v8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/SimpleCursorAdapter;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumAdapter:Landroid/widget/SimpleCursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v0, 0x7f04000b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButtonView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButtonView:Landroid/view/View;

    const v1, 0x7f04000d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mUploadButtonView:Landroid/view/View;

    const v1, 0x7f04000c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mCancelButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f040007

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNewAlbumButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNewAlbumButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, p0, p0, v1}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;-><init>(Landroid/content/Context;Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$Listener;Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mContentUris:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-le v6, v9, :cond_2

    const v0, 0x7f050003

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    const v0, 0x7f040008

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNoConnectionView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mNoConnectionView:Landroid/view/View;

    const v1, 0x7f040009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadAlbumsButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadAlbumsButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f04000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mReloadingAlbumsView:Landroid/view/View;

    goto/16 :goto_0

    :cond_2
    const v0, 0x7f050002

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method public onError(ILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->hideProgressDialog()V

    const-string v2, "PicasaUploadActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PicasaUploadAsyncTask error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " account="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->finish()V

    :cond_1
    return-void

    :pswitch_0
    const v1, 0x7f05000d

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAlbumsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->hasNetworkConnectivity()Z

    move-result v2

    if-eqz v2, :cond_3

    const v1, 0x7f05000e

    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->checkConnectivity()V

    goto :goto_0

    :cond_3
    const v1, 0x7f05000f

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->hasNetworkConnectivity()Z

    move-result v2

    if-eqz v2, :cond_4

    const v1, 0x7f05002a

    goto :goto_0

    :cond_4
    const v1, 0x7f05002b

    invoke-direct {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->checkConnectivity()V

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getAlbumListFromServer(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAccountsSpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p3}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->onAccountSelected(Ljava/lang/String;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    const-string v0, "PicasaUploadActivity"

    const-string v1, "onNothingSelected() when is this called?"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-virtual {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->cancel()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lvedroid/support/v4/app/FragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadActivity;->mAsyncTask:Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;

    invoke-virtual {v0}, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask;->getAccountList()V

    return-void
.end method

.method public onUploadRequestsSubmitted(Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;

    const/4 v4, 0x0

    const v0, 0x7f05000c

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const-string v0, "PicasaUploadActivity"

    const-string v1, "upload started on %s album=%s/%s photos=%d videos=%d requests=%d"

    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->account:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->album:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-object v4, v4, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->title:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->album:Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;

    iget-wide v4, v4, Lcom/google/android/apps/uploader/PicasaUploadDatabaseHelper$AlbumInfo;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget v4, p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->photoCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->videoCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    iget v4, p1, Lcom/google/android/apps/uploader/PicasaUploadAsyncTask$UploadRequestsSummary;->requestCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadActivity;->finish()V

    return-void
.end method
