.class public final Lcom/google/android/apps/uploader/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final accounts:I = 0x7f040005

.field public static final albums:I = 0x7f040006

.field public static final cancel:I = 0x7f04000c

.field public static final caption:I = 0x7f040004

.field public static final confirm:I = 0x7f04000d

.field public static final empty:I = 0x7f04001f

.field public static final footer_buttons:I = 0x7f04000b

.field public static final header_icon:I = 0x7f040001

.field public static final header_line_1:I = 0x7f040002

.field public static final network_error:I = 0x7f040008

.field public static final new_album:I = 0x7f040007

.field public static final notification_icon:I = 0x7f040014

.field public static final notification_progress:I = 0x7f040016

.field public static final notification_text:I = 0x7f040017

.field public static final notification_title:I = 0x7f040015

.field public static final picasa_access:I = 0x7f04000f

.field public static final picasa_album_title:I = 0x7f04000e

.field public static final picasa_cancel:I = 0x7f040012

.field public static final picasa_create:I = 0x7f040013

.field public static final picasa_private_access:I = 0x7f040011

.field public static final picasa_public_access:I = 0x7f040010

.field public static final reload_albums:I = 0x7f04000a

.field public static final retry_button:I = 0x7f040009

.field public static final summary_album_title:I = 0x7f04001b

.field public static final summary_file_name:I = 0x7f040019

.field public static final summary_file_size:I = 0x7f04001c

.field public static final summary_progressbar:I = 0x7f040020

.field public static final summary_state:I = 0x7f04001d

.field public static final summary_thumbnail:I = 0x7f040018

.field public static final summary_upload_time:I = 0x7f04001a

.field public static final tab_view:I = 0x7f040000

.field public static final tabhost:I = 0x7f04001e

.field public static final thumbnail:I = 0x7f040003


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
