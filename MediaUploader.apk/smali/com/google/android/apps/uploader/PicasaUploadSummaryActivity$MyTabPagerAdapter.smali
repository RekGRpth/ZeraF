.class Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;
.super Lvedroid/support/v4/app/FragmentPagerAdapter;
.source "PicasaUploadSummaryActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MyTabPagerAdapter"
.end annotation


# instance fields
.field private mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

.field private mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

.field private final mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

.field private final mFragments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lvedroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final mViewPager:Lvedroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Lvedroid/support/v4/app/FragmentManager;Lvedroid/support/v4/view/ViewPager;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/FragmentManager;
    .param p2    # Lvedroid/support/v4/view/ViewPager;

    invoke-direct {p0, p1}, Lvedroid/support/v4/app/FragmentPagerAdapter;-><init>(Lvedroid/support/v4/app/FragmentManager;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    iput-object p1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    iput-object p2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mViewPager:Lvedroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Lvedroid/support/v4/view/ViewPager;->setAdapter(Lvedroid/support/v4/view/PagerAdapter;)V

    return-void
.end method


# virtual methods
.method public addTabContent(Lvedroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Lvedroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    check-cast p3, Lvedroid/support/v4/app/Fragment;

    invoke-virtual {v0, p3}, Lvedroid/support/v4/app/FragmentTransaction;->hide(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Lvedroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lvedroid/support/v4/app/Fragment;
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvedroid/support/v4/app/Fragment;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragments:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x2

    goto :goto_1
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mFragmentManager:Lvedroid/support/v4/app/FragmentManager;

    invoke-virtual {v1}, Lvedroid/support/v4/app/FragmentManager;->beginTransaction()Lvedroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->getItem(I)Lvedroid/support/v4/app/Fragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurTransaction:Lvedroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1, v0}, Lvedroid/support/v4/app/FragmentTransaction;->show(Lvedroid/support/v4/app/Fragment;)Lvedroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lvedroid/support/v4/app/Fragment;

    invoke-virtual {p2}, Lvedroid/support/v4/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    move-object v0, p3

    check-cast v0, Lvedroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    if-eq v1, v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lvedroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    :cond_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lvedroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/uploader/PicasaUploadSummaryActivity$MyTabPagerAdapter;->mCurrentPrimaryItem:Lvedroid/support/v4/app/Fragment;

    :cond_2
    return-void
.end method

.method public startUpdate(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1    # Landroid/view/ViewGroup;

    return-void
.end method
