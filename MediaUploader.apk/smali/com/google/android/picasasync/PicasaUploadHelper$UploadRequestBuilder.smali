.class public Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;
.super Ljava/lang/Object;
.source "PicasaUploadHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PicasaUploadHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadRequestBuilder"
.end annotation


# instance fields
.field private mRequest:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Landroid/content/ContentValues;)V
    .locals 0
    .param p1    # Landroid/content/ContentValues;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->mRequest:Landroid/content/ContentValues;

    return-void
.end method


# virtual methods
.method public getRequest()Landroid/content/ContentValues;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->mRequest:Landroid/content/ContentValues;

    return-object v0
.end method

.method public setAlbumTitle(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->mRequest:Landroid/content/ContentValues;

    const-string v1, "album_title"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setCaption(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->mRequest:Landroid/content/ContentValues;

    const-string v1, "caption"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaUploadHelper$UploadRequestBuilder;->mRequest:Landroid/content/ContentValues;

    const-string v1, "display_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method
