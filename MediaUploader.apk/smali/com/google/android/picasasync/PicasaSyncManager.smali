.class Lcom/google/android/picasasync/PicasaSyncManager;
.super Ljava/lang/Object;
.source "PicasaSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;,
        Lcom/google/android/picasasync/PicasaSyncManager$GetNextSyncTask;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/picasasync/PicasaSyncManager;


# instance fields
.field private mBackgroundData:Z

.field private mConnected:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

.field private mHasWifiConnectivity:Z

.field private final mInvalidAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsPlugged:Z

.field private mIsRoaming:Z

.field private final mProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/picasasync/SyncTaskProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncHandler:Landroid/os/Handler;

.field private final mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mConnected:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "picasa-sync-manager"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/PicasaSyncManager;->initSyncHandler(Landroid/os/HandlerThread;)Landroid/os/Handler;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v0, Lcom/google/android/picasasync/PicasaSyncManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/PicasaSyncManager$1;-><init>(Lcom/google/android/picasasync/PicasaSyncManager;)V

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    return-void
.end method

.method private acceptSyncTask(Lcom/google/android/picasasync/SyncTask;)Z
    .locals 6
    .param p1    # Lcom/google/android/picasasync/SyncTask;

    const/4 v5, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isAutoSync()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because master auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v5}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    :goto_0
    return v1

    :cond_0
    new-instance v2, Landroid/accounts/Account;

    iget-object v3, p1, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "com.google.android.apps.uploader.PicasaUploadProvider"

    invoke-static {v2, v3}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v5}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isBackgroundSync()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for disabled background data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnBattery()Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on battery"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    if-nez v2, :cond_5

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnWifiOnly()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for non-wifi connection"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnRoaming()Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_5
    invoke-static {}, Lcom/google/android/picasasync/PicasaSyncManager;->isExternalStorageMounted()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnExternalStorageOnly()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "reject "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on external storage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v2, 0xb

    invoke-virtual {p1, v2}, Lcom/google/android/picasasync/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iget-object v4, p1, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "PicasaUploaderSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "reject "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for invalid account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_7
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/picasasync/PicasaSyncManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->setupTaskProviders()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateEnvironment()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/picasasync/PicasaSyncManager;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager;->updateBatteryState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasksInternal()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/picasasync/PicasaSyncManager;)Lcom/google/android/picasasync/PicasaSyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/picasasync/PicasaSyncManager;Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/picasasync/PicasaSyncManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/picasasync/PicasaSyncManager;->sInstance:Lcom/google/android/picasasync/PicasaSyncManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasasync/PicasaSyncManager;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/PicasaSyncManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasasync/PicasaSyncManager;->sInstance:Lcom/google/android/picasasync/PicasaSyncManager;

    :cond_0
    sget-object v0, Lcom/google/android/picasasync/PicasaSyncManager;->sInstance:Lcom/google/android/picasasync/PicasaSyncManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getNextSyncTask(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V
    .locals 5
    .param p1    # Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    const-string v3, "PicasaSyncManager.getNextSyncTask"

    invoke-static {v3}, Lcom/google/android/picasasync/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v2

    new-instance v1, Ljava/util/concurrent/FutureTask;

    new-instance v3, Lcom/google/android/picasasync/PicasaSyncManager$GetNextSyncTask;

    invoke-direct {v3, p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager$GetNextSyncTask;-><init>(Lcom/google/android/picasasync/PicasaSyncManager;Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V

    invoke-direct {v1, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {v2}, Lcom/google/android/picasasync/MetricsUtils;->end(I)V

    return-void

    :catch_0
    move-exception v0

    const-string v3, "PicasaUploaderSyncManager"

    const-string v4, "fail to get next task"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private initSyncHandler(Landroid/os/HandlerThread;)Landroid/os/Handler;
    .locals 2
    .param p1    # Landroid/os/HandlerThread;

    new-instance v0, Lcom/google/android/picasasync/PicasaSyncManager$2;

    invoke-virtual {p1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/picasasync/PicasaSyncManager$2;-><init>(Lcom/google/android/picasasync/PicasaSyncManager;Landroid/os/Looper;)V

    return-object v0
.end method

.method private static isExternalStorageMounted()Z
    .locals 2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "mounted_ro"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isValidAccount(Landroid/accounts/Account;)Z
    .locals 7
    .param p1    # Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v5, "com.google"

    invoke-virtual {v3, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_1
    return v5

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private static isWifiNetwork(I)Z
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_4

    iget-object v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/picasasync/SyncTaskProvider;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v6}, Lcom/google/android/picasasync/SyncTaskProvider;->collectTasks(Ljava/util/Collection;)V

    const/4 v4, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/picasasync/SyncTask;

    iput v0, v5, Lcom/google/android/picasasync/SyncTask;->mPriority:I

    invoke-direct {p0, v5}, Lcom/google/android/picasasync/PicasaSyncManager;->acceptSyncTask(Lcom/google/android/picasasync/SyncTask;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v4, :cond_1

    iget-object v7, v5, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_1
    move-object v4, v5

    goto :goto_1

    :cond_2
    if-eqz v4, :cond_3

    :goto_2
    return-object v4

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private onAccountInvalid(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "PicasaUploaderSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "account: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has been removed ?!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private performSyncInternal(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V
    .locals 8
    .param p1    # Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    const/4 v7, 0x0

    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    iget-object v1, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager;->getNextSyncTask(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V

    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    if-eqz v3, :cond_1

    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->account:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    iget-object v4, v4, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_0
    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    iget-object v4, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v3, v4}, Lcom/google/android/picasasync/SyncTask;->performSync(Landroid/content/SyncResult;)V

    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v3}, Landroid/content/SyncResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "PicasaUploaderSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sync complete w error:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    iput-object v7, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    :goto_1
    iget-wide v3, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const-string v3, "PicasaUploaderSyncManager"

    const-string v4, "stop sync session due to io error"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->cancelSync()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v3, "PicasaUploaderSyncManager"

    const-string v4, "perform sync fail"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-wide v3, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v1, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iput-object v7, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_2
    const-string v3, "PicasaUploaderSyncManager"

    const-string v4, "perform sync fail"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iput-object v7, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    goto :goto_1

    :catchall_0
    move-exception v3

    iput-object v7, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    throw v3
.end method

.method private setupTaskProviders()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/picasasync/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/UploadsManager;

    move-result-object v0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadsManager;->getManualPhotoUploadTaskProvider()Lcom/google/android/picasasync/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/picasasync/UploadsManager;->getManualVideoUploadTaskProvider()Lcom/google/android/picasasync/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private updateBatteryState(Ljava/lang/Boolean;)V
    .locals 6
    .param p1    # Ljava/lang/Boolean;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    if-nez p1, :cond_3

    new-instance v1, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v3, "PicasaUploaderSyncManager"

    const-string v4, "there is no battery info yet"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "plugged"

    const/4 v5, -0x1

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    :cond_2
    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    :cond_3
    const-string v3, "PicasaUploaderSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "battery info: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eq v3, v4, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasksInternal()V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private updateEnvironment()V
    .locals 12

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v2, 0x0

    iget-object v9, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/ConnectivityManager;

    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v9

    if-eqz v9, :cond_5

    move v1, v7

    :goto_0
    iget-boolean v9, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mConnected:Z

    if-eq v1, v9, :cond_0

    const/4 v2, 0x1

    iput-boolean v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mConnected:Z

    const-string v9, "PicasaUploaderSyncManager"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "active network: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v9

    invoke-static {v9}, Lcom/google/android/picasasync/PicasaSyncManager;->isWifiNetwork(I)Z

    move-result v9

    if-eqz v9, :cond_6

    move v3, v7

    :goto_1
    iget-boolean v9, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    if-eq v3, v9, :cond_1

    const/4 v2, 0x1

    iput-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    :cond_1
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v9

    if-eqz v9, :cond_7

    move v5, v7

    :goto_2
    iget-boolean v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    if-eq v5, v7, :cond_2

    const/4 v2, 0x1

    iput-boolean v5, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    :cond_2
    invoke-virtual {v6}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    iget-boolean v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    if-eq v7, v0, :cond_3

    const/4 v2, 0x1

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    const-string v7, "PicasaUploaderSyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "background data: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-eqz v2, :cond_4

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasksInternal()V

    :cond_4
    return-void

    :cond_5
    move v1, v8

    goto :goto_0

    :cond_6
    move v3, v8

    goto :goto_1

    :cond_7
    move v5, v8

    goto :goto_2
.end method

.method private updateTasksInternal()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v1, Landroid/accounts/Account;

    iget-object v0, v0, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/PicasaSyncManager;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const-class v4, Lcom/google/android/picasasync/PicasaUploadService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.uploader.START_UPLOAD"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.uploader.extra.ACCOUNT"

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    :cond_2
    iget-object v0, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->onAccountInvalid(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/PicasaSyncManager;->acceptSyncTask(Lcom/google/android/picasasync/SyncTask;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v0, "PicasaUploaderSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " due to environment change"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/picasasync/SyncTask;->cancelSync()V

    goto :goto_1

    :cond_4
    iget-object v0, v0, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->account:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/google/android/picasasync/SyncTask;->mPriority:I

    iget v3, v1, Lcom/google/android/picasasync/SyncTask;->mPriority:I

    if-ge v2, v3, :cond_0

    const-string v2, "PicasaUploaderSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cancel task: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/picasasync/SyncTask;->cancelSync()V

    goto/16 :goto_1
.end method


# virtual methods
.method public createSession(Ljava/lang/String;Landroid/content/SyncResult;)Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/SyncResult;

    new-instance v0, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-direct {v0, p1, p2}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;-><init>(Ljava/lang/String;Landroid/content/SyncResult;)V

    return-object v0
.end method

.method public onBatteryStateChanged(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    if-eqz p1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public onEnvironmentChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public performSync(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V
    .locals 5
    .param p1    # Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/picasasync/SyncTaskProvider;

    invoke-interface {v1}, Lcom/google/android/picasasync/SyncTaskProvider;->onSyncStart()V

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager;->performSyncInternal(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    monitor-enter p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-boolean v2, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mSyncCancelled:Z

    if-nez v2, :cond_1

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V

    :cond_1
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iput-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    return-void

    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v2

    iput-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    throw v2
.end method

.method public requestAccountSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public resetSyncStates()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public updateTasks(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
