.class Lcom/android/musicvis/vis2/Visualization2RS;
.super Lcom/android/musicvis/GenericWaveRS;
.source "Visualization2RS.java"


# direct methods
.method constructor <init>(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    const v0, 0x7f020003

    invoke-direct {p0, p1, p2, v0}, Lcom/android/musicvis/GenericWaveRS;-><init>(III)V

    return-void
.end method


# virtual methods
.method public start()V
    .locals 3

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/musicvis/AudioCapture;

    const/4 v1, 0x0

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2}, Lcom/android/musicvis/AudioCapture;-><init>(II)V

    iput-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    invoke-super {p0}, Lcom/android/musicvis/GenericWaveRS;->start()V

    return-void
.end method

.method public stop()V
    .locals 1

    invoke-super {p0}, Lcom/android/musicvis/GenericWaveRS;->stop()V

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v0}, Lcom/android/musicvis/AudioCapture;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    :cond_0
    return-void
.end method

.method public update()V
    .locals 7

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mAudioCapture:Lcom/android/musicvis/AudioCapture;

    invoke-virtual {v4, v5, v5}, Lcom/android/musicvis/AudioCapture;->getFormattedData(II)[I

    move-result-object v4

    iput-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    array-length v2, v4

    :cond_0
    if-nez v2, :cond_2

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v4, v4, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iput v5, v4, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    array-length v4, v4

    div-int/lit8 v3, v4, 0x8

    if-le v2, v3, :cond_3

    move v2, v3

    :cond_3
    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    iget v4, v4, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mWorldState:Lcom/android/musicvis/GenericWaveRS$WorldState;

    const/4 v5, 0x0

    iput v5, v4, Lcom/android/musicvis/GenericWaveRS$WorldState;->idle:I

    invoke-virtual {p0}, Lcom/android/musicvis/GenericWaveRS;->updateWorldState()V

    :cond_4
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_5

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mVizData:[I

    aget v0, v4, v1

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v5, v1, 0x8

    add-int/lit8 v5, v5, 0x1

    int-to-float v6, v0

    aput v6, v4, v5

    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    mul-int/lit8 v5, v1, 0x8

    add-int/lit8 v5, v5, 0x5

    neg-int v6, v0

    int-to-float v6, v6

    aput v6, v4, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/android/musicvis/GenericWaveRS;->mPointAlloc:Landroid/renderscript/Allocation;

    iget-object v5, p0, Lcom/android/musicvis/GenericWaveRS;->mPointData:[F

    invoke-virtual {v4, v5}, Landroid/renderscript/Allocation;->copyFromUnchecked([F)V

    goto :goto_0
.end method
