.class public Lcom/android/providers/drm/BootCompletedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BootCompletedReceiver.java"


# static fields
.field private static final DEVICE_ID_LEN:I = 0x20

.field private static final INVALID_DEVICE_ID:Ljava/lang/String; = "000000000000000"

.field private static final OLD_DEVICE_ID_FILE:Ljava/lang/String; = "/data/data/com.android.providers.drm/files/id/id.dat"

.field private static final TAG:Ljava/lang/String; = "DRM/BootCompletedReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static deviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 25
    .param p0    # Landroid/content/Context;

    const-string v11, "000000000000000"

    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: try to get IMEI as device id"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v22, "phone"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/telephony/TelephonyManager;

    move-object/from16 v20, v22

    check-cast v20, Landroid/telephony/TelephonyManager;

    if-eqz v20, :cond_2

    invoke-virtual/range {v20 .. v20}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_1

    :cond_0
    const-string v22, "DRM/BootCompletedReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "deviceId: Invalid imei: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v22, "000000000000000"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: try to check for old device id file /data/data/com.android.providers.drm/files/id/id.dat"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v7, Ljava/io/File;

    const-string v22, "/data/data/com.android.providers.drm/files/id/id.dat"

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    if-eqz v8, :cond_7

    const/16 v22, 0x20

    move/from16 v0, v22

    new-array v4, v0, [B

    move-object v2, v4

    array-length v0, v2

    move/from16 v16, v0

    const/4 v10, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v10, v0, :cond_3

    aget-byte v6, v2, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_1
    move-object v11, v13

    goto :goto_0

    :cond_2
    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: Invalid TelephonyManager."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v5

    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: the old device id file is not found."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    goto :goto_1

    :cond_3
    :try_start_1
    invoke-virtual {v8, v4}, Ljava/io/InputStream;->read([B)I

    move-result v19

    const/16 v17, 0x0

    const/4 v9, 0x0

    :goto_3
    array-length v0, v4

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v9, v0, :cond_4

    aget-byte v22, v4, v9

    if-nez v22, :cond_5

    move/from16 v17, v9

    :cond_4
    move/from16 v0, v17

    new-array v3, v0, [B

    const/4 v15, 0x0

    :goto_4
    array-length v0, v3

    move/from16 v22, v0

    move/from16 v0, v22

    if-ge v15, v0, :cond_6

    aget-byte v22, v4, v15

    aput-byte v22, v3, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_4

    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_6
    new-instance v12, Ljava/lang/String;

    const-string v22, "US-ASCII"

    invoke-static/range {v22 .. v22}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v3, v0}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v11, v12

    :cond_7
    :goto_5
    const-string v22, "000000000000000"

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: try to use mac address for device id."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v22, "wifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/net/wifi/WifiManager;

    move-object/from16 v21, v22

    check-cast v21, Landroid/net/wifi/WifiManager;

    if-eqz v21, :cond_c

    invoke-virtual/range {v21 .. v21}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v14

    if-nez v14, :cond_a

    const/16 v18, 0x0

    :goto_6
    if-eqz v18, :cond_8

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_b

    :cond_8
    const-string v22, "DRM/BootCompletedReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "deviceId: Invalid mac address: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    :goto_7
    const-string v22, "DRM/BootCompletedReceiver"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "deviceId: result: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-object v11

    :catch_1
    move-exception v5

    :goto_8
    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: I/O error when reading old devicd id file."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_a
    invoke-virtual {v14}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v18

    goto :goto_6

    :cond_b
    move-object/from16 v11, v18

    goto :goto_7

    :cond_c
    const-string v22, "DRM/BootCompletedReceiver"

    const-string v23, "deviceId: Invalid WifiManager."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catch_2
    move-exception v5

    move-object v11, v12

    goto :goto_8
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v3, "DRM/BootCompletedReceiver"

    const-string v4, "onReceive : BOOT_COMPLETED received."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/drm/OmaDrmClient;

    invoke-direct {v0, p1}, Lcom/mediatek/drm/OmaDrmClient;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/android/providers/drm/OmaDrmHelper;->loadDeviceId(Lcom/mediatek/drm/OmaDrmClient;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "DRM/BootCompletedReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BootCompletedReceiver : load device id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "000000000000000"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const-string v3, "DRM/BootCompletedReceiver"

    const-string v4, "BootCompletedReceiver : The device id is empty, try obtain it"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p1}, Lcom/android/providers/drm/BootCompletedReceiver;->deviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "DRM/BootCompletedReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BootCompletedReceiver : Obtained device id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "000000000000000"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "DRM/BootCompletedReceiver"

    const-string v4, "BootCompletedReceiver : Obtained device id is an invalid value"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v3, "DRM/BootCompletedReceiver"

    const-string v4, "BootCompletedReceiver : save device id."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0, v1}, Lcom/android/providers/drm/OmaDrmHelper;->saveDeviceId(Lcom/mediatek/drm/OmaDrmClient;Ljava/lang/String;)I

    :cond_2
    const-string v3, "DRM/BootCompletedReceiver"

    const-string v4, "BootCompletedReceiver : load secure timer and update time base."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Lcom/android/providers/drm/OmaDrmHelper;->loadClock(Lcom/mediatek/drm/OmaDrmClient;)I

    move-result v2

    invoke-static {v0}, Lcom/android/providers/drm/OmaDrmHelper;->updateTimeBase(Lcom/mediatek/drm/OmaDrmClient;)I

    goto :goto_0
.end method
