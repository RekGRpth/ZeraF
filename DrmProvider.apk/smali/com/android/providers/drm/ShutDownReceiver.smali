.class public Lcom/android/providers/drm/ShutDownReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShutDownReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/providers/drm/ShutDownReceiver$1;,
        Lcom/android/providers/drm/ShutDownReceiver$SaveClockTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DRM/ShutDownReceiver"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "DRM/ShutDownReceiver"

    const-string v1, "onReceive : ACTION_SHUTDOWN received."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/android/providers/drm/ShutDownReceiver$SaveClockTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/providers/drm/ShutDownReceiver$SaveClockTask;-><init>(Lcom/android/providers/drm/ShutDownReceiver;Lcom/android/providers/drm/ShutDownReceiver$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Context;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
