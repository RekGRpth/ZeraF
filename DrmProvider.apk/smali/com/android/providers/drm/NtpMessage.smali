.class public Lcom/android/providers/drm/NtpMessage;
.super Ljava/lang/Object;
.source "NtpMessage.java"


# instance fields
.field public mLeapIndicator:B

.field public mMode:B

.field public mOriginateTimestamp:D

.field public mPollInterval:B

.field public mPrecision:B

.field public mReceiveTimestamp:D

.field public mReferenceIdentifier:[B

.field public mReferenceTimestamp:D

.field public mRootDelay:D

.field public mRootDispersion:D

.field public mStratum:S

.field public mTransmitTimestamp:D

.field public mVersion:B


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    iput-byte v3, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    iput-short v0, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    iput-byte v3, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L

    div-double/2addr v0, v2

    const-wide v2, 0x41e0754fd0000000L

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(BBBSBBDD[BDDDD)V
    .locals 4
    .param p1    # B
    .param p2    # B
    .param p3    # B
    .param p4    # S
    .param p5    # B
    .param p6    # B
    .param p7    # D
    .param p9    # D
    .param p11    # [B
    .param p12    # D
    .param p14    # D
    .param p16    # D
    .param p18    # D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    iput-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    const/4 v2, 0x3

    iput-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    const/4 v2, 0x0

    iput-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    const/4 v2, 0x0

    iput-short v2, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    const/4 v2, 0x0

    iput-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    const/4 v2, 0x0

    iput-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    const/4 v2, 0x4

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    iput-byte p1, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    iput-byte p2, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    iput-byte p3, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    iput-short p4, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    iput-byte p5, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    iput-byte p6, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    iput-wide p7, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    iput-wide p9, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    iput-object p11, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    move-wide/from16 v0, p18

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>([B)V
    .locals 10
    .param p1    # [B

    const-wide/high16 v8, 0x40f0000000000000L

    const/4 v7, 0x3

    const-wide/high16 v5, 0x4070000000000000L

    const-wide/16 v1, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    iput-byte v7, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    iput-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    iput-short v4, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    iput-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    iput-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    iput-wide v1, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    aget-byte v0, p1, v4

    shr-int/lit8 v0, v0, 0x6

    and-int/lit8 v0, v0, 0x3

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    aget-byte v0, p1, v4

    shr-int/lit8 v0, v0, 0x3

    and-int/lit8 v0, v0, 0x7

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    aget-byte v0, p1, v4

    and-int/lit8 v0, v0, 0x7

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    const/4 v0, 0x1

    aget-byte v0, p1, v0

    invoke-static {v0}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v0

    iput-short v0, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    aget-byte v0, p1, v7

    iput-byte v0, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    const/4 v0, 0x4

    aget-byte v0, p1, v0

    int-to-double v0, v0

    mul-double/2addr v0, v5

    const/4 v2, 0x5

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v2

    int-to-double v2, v2

    add-double/2addr v0, v2

    const/4 v2, 0x6

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v5

    add-double/2addr v0, v2

    const/4 v2, 0x7

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v8

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    const/16 v0, 0x8

    aget-byte v0, p1, v0

    invoke-static {v0}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v5

    const/16 v2, 0x9

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v2

    int-to-double v2, v2

    add-double/2addr v0, v2

    const/16 v2, 0xa

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v5

    add-double/2addr v0, v2

    const/16 v2, 0xb

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v8

    add-double/2addr v0, v2

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    iget-object v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    const/16 v1, 0xc

    aget-byte v1, p1, v1

    aput-byte v1, v0, v4

    iget-object v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    const/4 v1, 0x1

    const/16 v2, 0xd

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    const/4 v1, 0x2

    const/16 v2, 0xe

    aget-byte v2, p1, v2

    aput-byte v2, v0, v1

    iget-object v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    const/16 v1, 0xf

    aget-byte v1, p1, v1

    aput-byte v1, v0, v7

    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/android/providers/drm/NtpMessage;->decodeTimestamp([BI)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/android/providers/drm/NtpMessage;->decodeTimestamp([BI)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    const/16 v0, 0x20

    invoke-static {p1, v0}, Lcom/android/providers/drm/NtpMessage;->decodeTimestamp([BI)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    const/16 v0, 0x28

    invoke-static {p1, v0}, Lcom/android/providers/drm/NtpMessage;->decodeTimestamp([BI)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public static decodeTimestamp([BI)D
    .locals 9
    .param p0    # [B
    .param p1    # I

    const-wide/16 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0x8

    if-ge v0, v3, :cond_0

    add-int v3, p1, v0

    aget-byte v3, p0, v3

    invoke-static {v3}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v3

    int-to-double v3, v3

    const-wide/high16 v5, 0x4000000000000000L

    rsub-int/lit8 v7, v0, 0x3

    mul-int/lit8 v7, v7, 0x8

    int-to-double v7, v7

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method public static encodeTimestamp([BID)V
    .locals 8
    .param p0    # [B
    .param p1    # I
    .param p2    # D

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    const-wide/high16 v3, 0x4000000000000000L

    rsub-int/lit8 v5, v2, 0x3

    mul-int/lit8 v5, v5, 0x8

    int-to-double v5, v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-int v3, p1, v2

    div-double v4, p2, v0

    double-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, p0, v3

    add-int v3, p1, v2

    aget-byte v3, p0, v3

    invoke-static {v3}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v3

    int-to-double v3, v3

    mul-double/2addr v3, v0

    sub-double/2addr p2, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x7

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide v6, 0x406fe00000000000L

    mul-double/2addr v4, v6

    double-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, p0, v3

    return-void
.end method

.method public static referenceIdentifierToString([BSB)Ljava/lang/String;
    .locals 9
    .param p0    # [B
    .param p1    # S
    .param p2    # B

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v7, 0x3

    const/4 v5, 0x1

    if-eqz p1, :cond_0

    if-ne p1, v5, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    :goto_0
    return-object v0

    :cond_1
    if-ne p2, v7, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v1, p0, v2

    invoke-static {v1}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v5

    invoke-static {v1}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v8

    invoke-static {v1}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v7

    invoke-static {v1}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne p2, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v2

    invoke-static {v1}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x4070000000000000L

    div-double/2addr v1, v3

    aget-byte v3, p0, v5

    invoke-static {v3}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v3

    int-to-double v3, v3

    const-wide/high16 v5, 0x40f0000000000000L

    div-double/2addr v3, v5

    add-double/2addr v1, v3

    aget-byte v3, p0, v8

    invoke-static {v3}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v3

    int-to-double v3, v3

    const-wide/high16 v5, 0x4170000000000000L

    div-double/2addr v3, v5

    add-double/2addr v1, v3

    aget-byte v3, p0, v7

    invoke-static {v3}, Lcom/android/providers/drm/NtpMessage;->unsignedByteToShort(B)S

    move-result v3

    int-to-double v3, v3

    const-wide/high16 v5, 0x41f0000000000000L

    div-double/2addr v3, v5

    add-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    const-string v0, ""

    goto/16 :goto_0
.end method

.method public static timestampToString(D)Ljava/lang/String;
    .locals 10
    .param p0    # D

    const-wide/16 v8, 0x0

    cmpl-double v8, p0, v8

    if-nez v8, :cond_0

    const-string v8, "0"

    :goto_0
    return-object v8

    :cond_0
    const-wide v8, 0x41e0754fd0000000L

    sub-double v6, p0, v8

    const-wide v8, 0x408f400000000000L

    mul-double/2addr v8, v6

    double-to-long v4, v8

    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "dd-MMM-yyyy HH:mm:ss"

    invoke-direct {v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    double-to-long v8, p0

    long-to-double v8, v8

    sub-double v1, p0, v8

    new-instance v8, Ljava/text/DecimalFormat;

    const-string v9, ".000000"

    invoke-direct {v8, v9}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public static unsignedByteToShort(B)S
    .locals 2
    .param p0    # B

    and-int/lit16 v0, p0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    and-int/lit8 v0, p0, 0x7f

    add-int/lit16 v0, v0, 0x80

    int-to-short v0, v0

    :goto_0
    return v0

    :cond_0
    int-to-short v0, p0

    goto :goto_0
.end method


# virtual methods
.method public toByteArray()[B
    .locals 14

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    const-wide/high16 v9, 0x40f0000000000000L

    const-wide/16 v7, 0xff

    const/16 v4, 0x30

    new-array v1, v4, [B

    iget-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    shl-int/lit8 v4, v4, 0x6

    iget-byte v5, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    shl-int/lit8 v5, v5, 0x3

    or-int/2addr v4, v5

    iget-byte v5, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    or-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v1, v11

    iget-short v4, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    int-to-byte v4, v4

    aput-byte v4, v1, v12

    iget-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    aput-byte v4, v1, v13

    const/4 v4, 0x3

    iget-byte v5, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    aput-byte v5, v1, v4

    iget-wide v4, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    mul-double/2addr v4, v9

    double-to-int v0, v4

    const/4 v4, 0x4

    shr-int/lit8 v5, v0, 0x18

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/4 v4, 0x5

    shr-int/lit8 v5, v0, 0x10

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/4 v4, 0x6

    shr-int/lit8 v5, v0, 0x8

    and-int/lit16 v5, v5, 0xff

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/4 v4, 0x7

    and-int/lit16 v5, v0, 0xff

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    iget-wide v4, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    mul-double/2addr v4, v9

    double-to-long v2, v4

    const/16 v4, 0x8

    const/16 v5, 0x18

    shr-long v5, v2, v5

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/16 v4, 0x9

    const/16 v5, 0x10

    shr-long v5, v2, v5

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/16 v4, 0xa

    const/16 v5, 0x8

    shr-long v5, v2, v5

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/16 v4, 0xb

    and-long v5, v2, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v1, v4

    const/16 v4, 0xc

    iget-object v5, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    aget-byte v5, v5, v11

    aput-byte v5, v1, v4

    const/16 v4, 0xd

    iget-object v5, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    aget-byte v5, v5, v12

    aput-byte v5, v1, v4

    const/16 v4, 0xe

    iget-object v5, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    aget-byte v5, v5, v13

    aput-byte v5, v1, v4

    const/16 v4, 0xf

    iget-object v5, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    aput-byte v5, v1, v4

    const/16 v4, 0x10

    iget-wide v5, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    invoke-static {v1, v4, v5, v6}, Lcom/android/providers/drm/NtpMessage;->encodeTimestamp([BID)V

    const/16 v4, 0x18

    iget-wide v5, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    invoke-static {v1, v4, v5, v6}, Lcom/android/providers/drm/NtpMessage;->encodeTimestamp([BID)V

    const/16 v4, 0x20

    iget-wide v5, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    invoke-static {v1, v4, v5, v6}, Lcom/android/providers/drm/NtpMessage;->encodeTimestamp([BID)V

    const/16 v4, 0x28

    iget-wide v5, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    invoke-static {v1, v4, v5, v6}, Lcom/android/providers/drm/NtpMessage;->encodeTimestamp([BID)V

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    const-wide v6, 0x408f400000000000L

    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "0.#E0"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const-wide/high16 v2, 0x4000000000000000L

    iget-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Leap indicator: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mLeapIndicator:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mMode:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Stratum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-short v2, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Poll: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mPollInterval:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Precision: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/android/providers/drm/NtpMessage;->mPrecision:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seconds) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Root delay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.00"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/android/providers/drm/NtpMessage;->mRootDelay:D

    mul-double/2addr v3, v6

    invoke-virtual {v2, v3, v4}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Root dispersion: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/text/DecimalFormat;

    const-string v3, "0.00"

    invoke-direct {v2, v3}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/android/providers/drm/NtpMessage;->mRootDispersion:D

    mul-double/2addr v3, v6

    invoke-virtual {v2, v3, v4}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Reference identifier: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceIdentifier:[B

    iget-short v3, p0, Lcom/android/providers/drm/NtpMessage;->mStratum:S

    iget-byte v4, p0, Lcom/android/providers/drm/NtpMessage;->mVersion:B

    invoke-static {v2, v3, v4}, Lcom/android/providers/drm/NtpMessage;->referenceIdentifierToString([BSB)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Reference timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mReferenceTimestamp:D

    invoke-static {v2, v3}, Lcom/android/providers/drm/NtpMessage;->timestampToString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Originate timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mOriginateTimestamp:D

    invoke-static {v2, v3}, Lcom/android/providers/drm/NtpMessage;->timestampToString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Receive timestamp:   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mReceiveTimestamp:D

    invoke-static {v2, v3}, Lcom/android/providers/drm/NtpMessage;->timestampToString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Transmit timestamp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/providers/drm/NtpMessage;->mTransmitTimestamp:D

    invoke-static {v2, v3}, Lcom/android/providers/drm/NtpMessage;->timestampToString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
