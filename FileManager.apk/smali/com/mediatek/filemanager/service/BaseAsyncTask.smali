.class abstract Lcom/mediatek/filemanager/service/BaseAsyncTask;
.super Landroid/os/AsyncTask;
.source "BaseAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Lcom/mediatek/filemanager/service/ProgressInfo;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BaseAsyncTask"


# instance fields
.field protected mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

.field protected mIsTaskFinished:Z

.field protected mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mIsTaskFinished:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    iput-object p2, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    return-void
.end method


# virtual methods
.method public isTaskBusy()Z
    .locals 3

    const-string v0, "BaseAsyncTask"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isTaskBusy,task status = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mIsTaskFinished:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "BaseAsyncTask"

    const-string v1, "isTaskBusy,retuen false."

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-string v0, "BaseAsyncTask"

    const-string v1, "isTaskBusy,retuen true."

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    const-string v0, "BaseAsyncTask"

    const-string v1, "onCancelled()"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    const/4 v1, -0x7

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mIsTaskFinished:Z

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    const-string v0, "BaseAsyncTask"

    const-string v1, "onPostExecute"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskResult(I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mIsTaskFinished:Z

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/BaseAsyncTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mIsTaskFinished:Z

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    const-string v0, "BaseAsyncTask"

    const-string v1, "onPreExecute"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    invoke-interface {v0}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskPrepare()V

    :cond_0
    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/mediatek/filemanager/service/ProgressInfo;)V
    .locals 3
    .param p1    # [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    aget-object v0, p1, v2

    if-eqz v0, :cond_0

    const-string v0, "BaseAsyncTask"

    const-string v1, "onProgressUpdate"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    aget-object v1, p1, v2

    invoke-interface {v0, v1}, Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;->onTaskProgress(Lcom/mediatek/filemanager/service/ProgressInfo;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/service/BaseAsyncTask;->onProgressUpdate([Lcom/mediatek/filemanager/service/ProgressInfo;)V

    return-void
.end method

.method protected removeListener()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    if-eqz v0, :cond_0

    const-string v0, "BaseAsyncTask"

    const-string v1, "removeListener"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    :cond_0
    return-void
.end method

.method public setListener(Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mListener:Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;

    return-void
.end method
