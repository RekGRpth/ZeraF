.class abstract Lcom/mediatek/filemanager/service/FileOperationTask;
.super Lcom/mediatek/filemanager/service/BaseAsyncTask;
.source "FileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/service/FileOperationTask$RenameTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$CreateFolderTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$CopyPasteFilesTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$CutPasteFilesTask;,
        Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;,
        Lcom/mediatek/filemanager/service/FileOperationTask$DeleteFilesTask;
    }
.end annotation


# static fields
.field protected static final BUFFER_SIZE:I = 0x200000

.field private static final TAG:Ljava/lang/String; = "FileOperationTask"

.field protected static final TOTAL:I = 0x64


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;


# direct methods
.method public constructor <init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;Landroid/content/Context;)V
    .locals 2
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager;
    .param p2    # Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;
    .param p3    # Landroid/content/Context;

    invoke-direct {p0, p1, p2}, Lcom/mediatek/filemanager/service/BaseAsyncTask;-><init>(Lcom/mediatek/filemanager/FileInfoManager;Lcom/mediatek/filemanager/service/FileManagerService$OperationEventListener;)V

    if-nez p3, :cond_0

    const-string v0, "FileOperationTask"

    const-string v1, "construct FileOperationTask exception! "

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iput-object p3, p0, Lcom/mediatek/filemanager/service/FileOperationTask;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/mediatek/filemanager/service/MediaStoreHelper;

    invoke-direct {v0, p3, p0}, Lcom/mediatek/filemanager/service/MediaStoreHelper;-><init>(Landroid/content/Context;Lcom/mediatek/filemanager/service/BaseAsyncTask;)V

    iput-object v0, p0, Lcom/mediatek/filemanager/service/FileOperationTask;->mMediaProviderHelper:Lcom/mediatek/filemanager/service/MediaStoreHelper;

    return-void
.end method

.method private calcNeedSpace(Ljava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)J"
        }
    .end annotation

    const-wide/16 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    :cond_0
    return-wide v2
.end method


# virtual methods
.method protected addItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/io/File;",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v0, p3}, Lcom/mediatek/filemanager/FileInfo;-><init>(Ljava/io/File;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/FileInfoManager;->addItem(Lcom/mediatek/filemanager/FileInfo;)V

    :cond_0
    return-void
.end method

.method checkFileNameAndRename(Ljava/io/File;)Ljava/io/File;
    .locals 4
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    move-object v0, p1

    :cond_0
    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "FileOperationTask"

    const-string v3, "checkFileNameAndRename,cancel."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    const-string v1, "FileOperationTask"

    const-string v2, "checkFileNameAndRename,file is not exist."

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/mediatek/filemanager/utils/FileUtils;->genrateNextNewName(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "FileOperationTask"

    const-string v3, "checkFileNameAndRename,retFile is null."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected copyFile([BLjava/io/File;Ljava/io/File;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I
    .locals 21
    .param p1    # [B
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .param p4    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile, invalid parameter."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v17, -0xe

    :goto_0
    return v17

    :cond_1
    const/4 v11, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    :try_start_0
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->createNewFile()Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile, create new file fail."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v18, -0xe

    if-eqz v11, :cond_2

    :try_start_1
    #Replaced unresolvable odex instruction with a throw
    throw v11

    :cond_2
    if-eqz v15, :cond_3

    #Replaced unresolvable odex instruction with a throw
    throw v15
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :cond_3
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    move/from16 v17, v18

    goto :goto_0

    :cond_4
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile, src file is not exist."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/16 v18, -0xe

    if-eqz v11, :cond_5

    :try_start_3
    #Replaced unresolvable odex instruction with a throw
    throw v11

    :cond_5
    if-eqz v15, :cond_6

    #Replaced unresolvable odex instruction with a throw
    throw v15
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    :cond_6
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    move/from16 v17, v18

    goto/16 :goto_0

    :cond_7
    :try_start_4
    new-instance v12, Ljava/io/FileInputStream;

    move-object/from16 v0, p2

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    new-instance v16, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_7

    const/4 v14, 0x0

    :goto_3
    :try_start_6
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/io/InputStream;->read([B)I

    move-result v14

    if-lez v14, :cond_e

    invoke-virtual/range {p0 .. p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_b

    const-string v3, "FileOperationTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "copyFile,commit copy file cancelled; break while loop thread id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,delete fail in copyFile()"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_8

    :cond_8
    const/16 v18, -0x7

    if-eqz v12, :cond_9

    :try_start_7
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    :cond_9
    if-eqz v16, :cond_a

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    :cond_a
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    move/from16 v17, v18

    goto/16 :goto_0

    :cond_b
    const/4 v3, 0x0

    :try_start_8
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v14}, Ljava/io/FileOutputStream;->write([BII)V

    int-to-long v3, v14

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateProgress(J)V

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/filemanager/service/FileOperationTask;->updateProgressWithTime(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/io/File;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    goto/16 :goto_3

    :catch_0
    move-exception v13

    move-object/from16 v15, v16

    move-object v11, v12

    :goto_5
    :try_start_9
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/16 v17, -0xe

    if-eqz v11, :cond_c

    :try_start_a
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_c
    if-eqz v15, :cond_d

    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :cond_d
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v18, v19

    :goto_6
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_e
    if-eqz v12, :cond_f

    :try_start_b
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    :cond_f
    if-eqz v16, :cond_10

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    :cond_10
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v18, v19

    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    move-object/from16 v15, v16

    move-object v11, v12

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    move-object/from16 v18, v3

    :goto_8
    if-eqz v11, :cond_11

    :try_start_c
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    :cond_11
    if-eqz v15, :cond_12

    invoke-virtual {v15}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_12
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    :goto_9
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catch_1
    move-exception v13

    :try_start_d
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception 2!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    const/16 v17, -0xe

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    goto :goto_9

    :catchall_1
    move-exception v3

    move-object/from16 v18, v3

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catch_2
    move-exception v13

    :try_start_e
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception 2!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    const/16 v17, -0xe

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v18, v19

    goto/16 :goto_6

    :catchall_2
    move-exception v3

    move-object/from16 v18, v3

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catch_3
    move-exception v13

    :try_start_f
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception 2!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    const/16 v17, -0xe

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    goto/16 :goto_1

    :catchall_3
    move-exception v3

    move-object/from16 v18, v3

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catch_4
    move-exception v13

    :try_start_10
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception 2!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    const/16 v17, -0xe

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    goto/16 :goto_2

    :catchall_4
    move-exception v3

    move-object/from16 v18, v3

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catch_5
    move-exception v13

    :try_start_11
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception 2!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    const/16 v17, -0xe

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    goto/16 :goto_4

    :catchall_5
    move-exception v3

    move-object/from16 v18, v3

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catch_6
    move-exception v13

    :try_start_12
    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,io exception 2!"

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    const/16 v17, -0xe

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v18, v19

    goto/16 :goto_7

    :catchall_6
    move-exception v3

    move-object/from16 v18, v3

    const-string v3, "FileOperationTask"

    const-string v4, "copyFile,update 100%."

    invoke-static {v3, v4}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x1

    new-array v0, v3, [Lcom/mediatek/filemanager/service/ProgressInfo;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual/range {p2 .. p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x64

    const-wide/16 v6, 0x64

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual/range {p4 .. p4}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v9

    invoke-direct/range {v3 .. v10}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v3, v19, v20

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    throw v18

    :catchall_7
    move-exception v3

    move-object/from16 v18, v3

    move-object v11, v12

    goto/16 :goto_8

    :catchall_8
    move-exception v3

    move-object/from16 v18, v3

    move-object/from16 v15, v16

    move-object v11, v12

    goto/16 :goto_8

    :catch_7
    move-exception v13

    goto/16 :goto_5

    :catch_8
    move-exception v13

    move-object v11, v12

    goto/16 :goto_5
.end method

.method protected deleteFile(Ljava/io/File;)Z
    .locals 5
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-nez p1, :cond_1

    new-array v2, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v4, -0xd

    invoke-direct {v3, v4, v0}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v3, v2, v1

    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    :goto_0
    move v0, v1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const-string v2, "FileOperationTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteFile fail,file name = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-array v2, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v4, -0xf

    invoke-direct {v3, v4, v0}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v3, v2, v1

    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getAllDeleteFile(Ljava/io/File;Ljava/util/List;)I
    .locals 7
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)I"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v5, "FileOperationTask"

    const-string v6, "getAllDeleteFile,cancel. "

    invoke-static {v5, v6}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x7

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {p2, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {p1}, Ljava/io/File;->canWrite()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v5, "FileOperationTask"

    const-string v6, "getAllDeleteFile,files is null. "

    invoke-static {v5, v6}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    invoke-virtual {p0, v1, p2}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllDeleteFile(Ljava/io/File;Ljava/util/List;)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p2, v5, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected getAllDeleteFiles(Ljava/util/List;Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)I"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3, p2}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllDeleteFile(Ljava/io/File;Ljava/util/List;)I

    move-result v2

    if-gez v2, :cond_0

    :cond_1
    return v2
.end method

.method protected getAllFile(Ljava/io/File;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I
    .locals 8
    .param p1    # Ljava/io/File;
    .param p3    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;",
            ")I"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/os/AsyncTask;->isCancelled()Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "FileOperationTask"

    const-string v7, "getAllFile, cancel."

    invoke-static {v6, v7}, Lcom/mediatek/filemanager/utils/LogUtils;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, -0x7

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {p3, v6, v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateTotal(J)V

    const-wide/16 v6, 0x1

    invoke-virtual {p3, v6, v7}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->updateTotalNumber(J)V

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    const/4 v5, -0x1

    goto :goto_0

    :cond_2
    move-object v0, v2

    array-length v4, v0

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    invoke-virtual {p0, v1, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllFile(Ljava/io/File;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v5

    if-ltz v5, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_0
.end method

.method protected getAllFileList(Ljava/util/List;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I
    .locals 4
    .param p3    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;",
            "Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;",
            ")I"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {p0, v3, p2, p3}, Lcom/mediatek/filemanager/service/FileOperationTask;->getAllFile(Ljava/io/File;Ljava/util/List;Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;)I

    move-result v2

    if-gez v2, :cond_0

    :cond_1
    return v2
.end method

.method protected getDstFile(Ljava/util/HashMap;Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/lang/String;",
            ")",
            "Ljava/io/File;"
        }
    .end annotation

    const-string v2, "FileOperationTask"

    const-string v3, "getDstFile."

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v0, p3

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/mediatek/filemanager/service/FileOperationTask;->checkFileNameAndRename(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    return-object v2
.end method

.method protected isEnoughSpace(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/lang/String;)Z
    .locals 8
    .param p1    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .param p2    # Ljava/lang/String;

    const-string v5, "FileOperationTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isEnoughSpace,dstFolder = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotal()J

    move-result-wide v3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v1

    cmp-long v5, v3, v1

    if-lez v5, :cond_0

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    const/4 v5, 0x1

    goto :goto_0
.end method

.method protected mkdir(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)Z
    .locals 5
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")Z"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "FileOperationTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mkdir,srcFile = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",dstFile = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p3}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return v0

    :cond_0
    new-array v2, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    new-instance v3, Lcom/mediatek/filemanager/service/ProgressInfo;

    const/16 v4, -0xe

    invoke-direct {v3, v4, v0}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(IZ)V

    aput-object v3, v2, v1

    invoke-virtual {p0, v2}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method

.method protected removeItem(Ljava/util/HashMap;Ljava/io/File;Ljava/io/File;)V
    .locals 2
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/io/File;",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;",
            "Ljava/io/File;",
            "Ljava/io/File;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-direct {v0, p3}, Lcom/mediatek/filemanager/FileInfo;-><init>(Ljava/io/File;)V

    iget-object v1, p0, Lcom/mediatek/filemanager/service/BaseAsyncTask;->mFileInfoManager:Lcom/mediatek/filemanager/FileInfoManager;

    invoke-virtual {v1, v0}, Lcom/mediatek/filemanager/FileInfoManager;->removeItem(Lcom/mediatek/filemanager/FileInfo;)V

    :cond_0
    return-void
.end method

.method protected updateProgressWithTime(Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;Ljava/io/File;)V
    .locals 10
    .param p1    # Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;
    .param p2    # Ljava/io/File;

    const-wide/16 v3, 0x64

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->needUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getProgress()J

    move-result-wide v0

    mul-long/2addr v0, v3

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotal()J

    move-result-wide v5

    div-long/2addr v0, v5

    long-to-int v2, v0

    const/4 v0, 0x1

    new-array v8, v0, [Lcom/mediatek/filemanager/service/ProgressInfo;

    const/4 v9, 0x0

    new-instance v0, Lcom/mediatek/filemanager/service/ProgressInfo;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getCurrentNumber()J

    move-result-wide v5

    long-to-int v5, v5

    invoke-virtual {p1}, Lcom/mediatek/filemanager/service/FileOperationTask$UpdateInfo;->getTotalNumber()J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/mediatek/filemanager/service/ProgressInfo;-><init>(Ljava/lang/String;IJIJ)V

    aput-object v0, v8, v9

    invoke-virtual {p0, v8}, Landroid/os/AsyncTask;->publishProgress([Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
