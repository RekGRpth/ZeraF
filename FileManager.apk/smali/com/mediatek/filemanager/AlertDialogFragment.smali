.class public Lcom/mediatek/filemanager/AlertDialogFragment;
.super Landroid/app/DialogFragment;
.source "AlertDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;,
        Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragment;,
        Lcom/mediatek/filemanager/AlertDialogFragment$ChoiceDialogFragmentBuilder;,
        Lcom/mediatek/filemanager/AlertDialogFragment$EditTextDialogFragment;,
        Lcom/mediatek/filemanager/AlertDialogFragment$EditDialogFragmentBuilder;,
        Lcom/mediatek/filemanager/AlertDialogFragment$AlertDialogFragmentBuilder;
    }
.end annotation


# static fields
.field private static final CANCELABLE:Ljava/lang/String; = "cancelable"

.field private static final ICON:Ljava/lang/String; = "icon"

.field public static final INVIND_RES_ID:I = -0x1

.field private static final LAYOUT:Ljava/lang/String; = "layout"

.field private static final MESSAGE:Ljava/lang/String; = "message"

.field private static final NEGATIVE_TITLE:Ljava/lang/String; = "negativeTitle"

.field private static final POSITIVE_TITLE:Ljava/lang/String; = "positiveTitle"

.field public static final TAG:Ljava/lang/String; = "AlertDialogFragment"

.field private static final TITLE:Ljava/lang/String; = "title"


# instance fields
.field private mDialogDismissListener:Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;

.field protected mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field protected mDoneListener:Landroid/content/DialogInterface$OnClickListener;

.field protected mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    iput-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;

    return-void
.end method


# virtual methods
.method protected createAlertDialogBuilder(Landroid/os/Bundle;)Landroid/app/AlertDialog$Builder;
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const/4 v12, -0x1

    const/4 v0, 0x0

    if-nez p1, :cond_6

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v1, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-eqz v0, :cond_5

    const-string v10, "title"

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v8

    if-eq v8, v12, :cond_0

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    :cond_0
    const-string v10, "icon"

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-eq v5, v12, :cond_1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    :cond_1
    const-string v10, "message"

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    const-string v10, "layout"

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-eq v6, v12, :cond_7

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v6, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    :cond_2
    :goto_1
    const-string v10, "negativeTitle"

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v12, :cond_3

    new-instance v10, Lcom/mediatek/filemanager/AlertDialogFragment$1;

    invoke-direct {v10, p0}, Lcom/mediatek/filemanager/AlertDialogFragment$1;-><init>(Lcom/mediatek/filemanager/AlertDialogFragment;)V

    invoke-virtual {v1, v2, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_3
    const-string v10, "positiveTitle"

    invoke-virtual {v0, v10, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v12, :cond_4

    invoke-virtual {v1, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    :cond_4
    new-instance v10, Lcom/mediatek/filemanager/utils/ToastHelper;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/mediatek/filemanager/utils/ToastHelper;-><init>(Landroid/content/Context;)V

    iput-object v10, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mToastHelper:Lcom/mediatek/filemanager/utils/ToastHelper;

    const-string v10, "cancelable"

    const/4 v11, 0x1

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    :cond_5
    return-object v1

    :cond_6
    move-object v0, p1

    goto :goto_0

    :cond_7
    if-eq v7, v12, :cond_2

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDoneListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDoneListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-interface {v0, p1, p2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/mediatek/filemanager/AlertDialogFragment;->createAlertDialogBuilder(Landroid/os/Bundle;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDialogDismissListener:Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDialogDismissListener:Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;

    invoke-interface {v0}, Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;->onDialogDismiss()V

    :cond_1
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public setDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface$OnDismissListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-void
.end method

.method public setOnDialogDismissListener(Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;)V
    .locals 0
    .param p1    # Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDialogDismissListener:Lcom/mediatek/filemanager/AlertDialogFragment$OnDialogDismissListener;

    return-void
.end method

.method public setOnDoneListener(Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface$OnClickListener;

    iput-object p1, p0, Lcom/mediatek/filemanager/AlertDialogFragment;->mDoneListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method
