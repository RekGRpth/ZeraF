.class public Lcom/mediatek/filemanager/FileInfoManager;
.super Ljava/lang/Object;
.source "FileInfoManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;
    }
.end annotation


# static fields
.field private static final MAX_LIST_SIZE:I = 0x14

.field public static final PASTE_MODE_COPY:I = 0x2

.field public static final PASTE_MODE_CUT:I = 0x1

.field public static final PASTE_MODE_UNKOWN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FileInfoManager"


# instance fields
.field private final mAddFilesInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mLastAccessPath:Ljava/lang/String;

.field protected mModifiedTime:J

.field private final mNavigationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;",
            ">;"
        }
    .end annotation
.end field

.field private final mPasteFilesInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPasteOperation:I

.field private final mRemoveFilesInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mShowFilesInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteOperation:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mModifiedTime:J

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addItem(Lcom/mediatek/filemanager/FileInfo;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfo;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addItemList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;)V"
        }
    .end annotation

    const-string v0, "FileInfoManager"

    const-string v1, "addItemList"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->v(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method protected addToNavigationList(Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;)V
    .locals 2
    .param p1    # Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected clearNavigationList()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public clearPasteList()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteOperation:I

    return-void
.end method

.method public getPasteCount()I
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPasteList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getPasteType()I
    .locals 1

    iget v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteOperation:I

    return v0
.end method

.method protected getPrevNavigation()Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;
    .locals 4

    :cond_0
    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;

    invoke-virtual {p0}, Lcom/mediatek/filemanager/FileInfoManager;->removeFromNavigationList()V

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->getRecordPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mediatek/filemanager/MountPointManager;->isRootPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShowFileList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "FileInfoManager"

    const-string v1, "getShowFileList"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    return-object v0
.end method

.method public isPasteItem(Lcom/mediatek/filemanager/FileInfo;)Z
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfo;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isPathModified(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-wide v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mModifiedTime:J

    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadFileInfoList(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v2, "FileInfoManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "loadFileInfoList,path = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",sortType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mModifiedTime:J

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    if-nez v0, :cond_1

    const-string v2, "FileInfoManager"

    const-string v3, "loadFileInfoList,file info is null!"

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFileParentPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/mediatek/filemanager/MountPointManager;->getInstance()Lcom/mediatek/filemanager/MountPointManager;

    move-result-object v2

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFileAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mediatek/filemanager/MountPointManager;->isMountPoint(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual {p0, p2}, Lcom/mediatek/filemanager/FileInfoManager;->sort(I)V

    return-void
.end method

.method protected removeFromNavigationList()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mNavigationList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public removeItem(Lcom/mediatek/filemanager/FileInfo;)V
    .locals 1
    .param p1    # Lcom/mediatek/filemanager/FileInfo;

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public savePasteList(ILjava/util/List;)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/mediatek/filemanager/FileInfo;",
            ">;)V"
        }
    .end annotation

    iput p1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteOperation:I

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public sort(I)V
    .locals 3
    .param p1    # I

    const-string v0, "FileInfoManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sort,sortType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    invoke-static {p1}, Lcom/mediatek/filemanager/FileInfoComparator;->getInstance(I)Lcom/mediatek/filemanager/FileInfoComparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public updateFileInfoList(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v2, "FileInfoManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateFileInfoList,currentPath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "sortType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mModifiedTime:J

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFileParentPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    iget-object v3, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual {p0, p2}, Lcom/mediatek/filemanager/FileInfoManager;->sort(I)V

    return-void
.end method

.method public updateOneFileInfoList(Ljava/lang/String;I)Lcom/mediatek/filemanager/FileInfo;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v1, "FileInfoManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateOneFileInfoList,path = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "sortType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mModifiedTime:J

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/filemanager/FileInfo;

    invoke-virtual {v0}, Lcom/mediatek/filemanager/FileInfo;->getFileParentPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mLastAccessPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mPasteFilesInfoList:Ljava/util/List;

    iget-object v2, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mRemoveFilesInfoList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-virtual {p0, p2}, Lcom/mediatek/filemanager/FileInfoManager;->sort(I)V

    return-object v0
.end method

.method public updateSearchList()V
    .locals 2

    const-string v0, "FileInfoManager"

    const-string v1, "updateSearchList"

    invoke-static {v0, v1}, Lcom/mediatek/filemanager/utils/LogUtils;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mShowFilesInfoList:Ljava/util/List;

    iget-object v1, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager;->mAddFilesInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method
