.class public Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;
.super Ljava/lang/Object;
.source "FileInfoManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/filemanager/FileInfoManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NavigationRecord"
.end annotation


# instance fields
.field private final mPath:Ljava/lang/String;

.field private final mSelectedFile:Lcom/mediatek/filemanager/FileInfo;

.field private final mTop:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/mediatek/filemanager/FileInfo;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mediatek/filemanager/FileInfo;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->mPath:Ljava/lang/String;

    iput-object p2, p0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->mSelectedFile:Lcom/mediatek/filemanager/FileInfo;

    iput p3, p0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->mTop:I

    return-void
.end method


# virtual methods
.method public getRecordPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedFile()Lcom/mediatek/filemanager/FileInfo;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->mSelectedFile:Lcom/mediatek/filemanager/FileInfo;

    return-object v0
.end method

.method public getTop()I
    .locals 1

    iget v0, p0, Lcom/mediatek/filemanager/FileInfoManager$NavigationRecord;->mTop:I

    return v0
.end method
