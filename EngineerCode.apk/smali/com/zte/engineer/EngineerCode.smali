.class public Lcom/zte/engineer/EngineerCode;
.super Landroid/app/Activity;
.source "EngineerCode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/engineer/EngineerCode$MyAdapter;,
        Lcom/zte/engineer/EngineerCode$ItemContent;
    }
.end annotation


# static fields
.field public static final AUTOTEST:Ljava/lang/String; = "AUTOTEST"

.field private static final EXTRAS_CAMERA_FACING:Ljava/lang/String; = "android.intent.extras.CAMERA_FACING"

.field public static final NORMALTEST:Ljava/lang/String; = "NORMALTEST"

.field public static final RESULT_FALSE:I = 0x14

.field public static final RESULT_PASS:I = 0xa

.field public static final STATE:Ljava/lang/String; = "StateArrayList"

.field public static final stringIDs:[I


# instance fields
.field adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

.field private autoTest:Z

.field private autoTestFactory:Z

.field items:[Lcom/zte/engineer/EngineerCode$ItemContent;

.field list:Landroid/widget/ListView;

.field private nowPosition:I

.field private startAuto:Z

.field testCompleted:I

.field testCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    return-void

    :array_0
    .array-data 4
        0x7f060008
        0x7f060020
        0x7f060065
        0x7f060007
        0x7f060089
        0x7f060009
        0x7f06000a
        0x7f06000b
        0x7f06000c
        0x7f060006
        0x7f06000e
        0x7f060010
        0x7f060011
        0x7f060012
        0x7f060013
        0x7f060014
        0x7f060018
        0x7f06001c
        0x7f06001d
        0x7f06001e
        0x7f06001b
        0x7f06001a
        0x7f06001f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->autoTest:Z

    iput-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->startAuto:Z

    iput-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->autoTestFactory:Z

    iput v0, p0, Lcom/zte/engineer/EngineerCode;->nowPosition:I

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/EngineerCode;I)V
    .locals 0
    .param p0    # Lcom/zte/engineer/EngineerCode;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/zte/engineer/EngineerCode;->implementItemClick(I)V

    return-void
.end method

.method private implementItemClick(I)V
    .locals 4
    .param p1    # I

    const/high16 v2, 0x10000000

    const/4 v3, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput p1, p0, Lcom/zte/engineer/EngineerCode;->nowPosition:I

    sget-object v1, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    aget v1, v1, p1

    sparse-switch v1, :sswitch_data_0

    const-class v1, Lcom/zte/engineer/NotSupportNotification;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "notification"

    sget-object v2, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    aget v2, v2, p1

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_1
    return-void

    :sswitch_0
    const-class v1, Lcom/zte/engineer/BatteryLog;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_1
    const-class v1, Lcom/zte/engineer/TouchScreenTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_2
    const-class v1, Lcom/zte/engineer/SIMTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_3
    const-class v1, Lcom/zte/engineer/LcdTestActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_4
    const-class v1, Lcom/zte/engineer/VibratorTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_5
    const-class v1, Lcom/zte/engineer/BacklightTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_6
    const-class v1, Lcom/zte/engineer/RingerTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_7
    const-class v1, Lcom/zte/engineer/KeyTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_8
    const-class v1, Lcom/zte/engineer/LcdOffTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_9
    const-class v1, Lcom/zte/engineer/SDcardTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_a
    const-class v1, Lcom/zte/engineer/MemoryTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_b
    const-class v1, Lcom/zte/engineer/BTAddressTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_c
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.bluetooth.BluetoothSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_d
    const-class v1, Lcom/zte/engineer/WifiAddressTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_e
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.wifi.WifiSettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_f
    const-class v1, Lcom/zte/engineer/ReciverTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_10
    const-class v1, Lcom/zte/engineer/AudioLoopTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_11
    const-class v1, Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_12
    const-string v1, "com.mediatek.FMRadio"

    const-string v2, "com.mediatek.FMRadio.FMRadioEMActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :sswitch_13
    const-string v1, "com.android.gallery3d"

    const-string v2, "com.android.camera.CameraLauncher"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_0

    :sswitch_14
    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "CAMAPP"

    const-string v2, "__________should to boot the back camera ..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_15
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    if-gtz v1, :cond_0

    const v1, 0x7f06008a

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_0
    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "android.intent.extras.CAMERA_FACING"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "CAMAPP"

    const-string v2, "__________should to boot the front camera ..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_16
    const-class v1, Lcom/zte/engineer/ImeiTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :sswitch_17
    const-class v1, Lcom/zte/engineer/ImsiTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :sswitch_18
    const-class v1, Lcom/zte/engineer/SensorTest;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto/16 :goto_0

    :sswitch_19
    const-string v1, "com.mediatek.ygps"

    const-string v2, "com.mediatek.ygps.YgpsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f060006 -> :sswitch_7
        0x7f060007 -> :sswitch_10
        0x7f060008 -> :sswitch_0
        0x7f060009 -> :sswitch_3
        0x7f06000a -> :sswitch_5
        0x7f06000b -> :sswitch_4
        0x7f06000c -> :sswitch_6
        0x7f06000d -> :sswitch_8
        0x7f06000e -> :sswitch_9
        0x7f060010 -> :sswitch_a
        0x7f060011 -> :sswitch_b
        0x7f060012 -> :sswitch_c
        0x7f060013 -> :sswitch_d
        0x7f060014 -> :sswitch_e
        0x7f060018 -> :sswitch_12
        0x7f060019 -> :sswitch_13
        0x7f06001a -> :sswitch_14
        0x7f06001b -> :sswitch_15
        0x7f06001c -> :sswitch_16
        0x7f06001d -> :sswitch_17
        0x7f06001e -> :sswitch_18
        0x7f06001f -> :sswitch_19
        0x7f060020 -> :sswitch_1
        0x7f060065 -> :sswitch_2
        0x7f06006b -> :sswitch_f
        0x7f060089 -> :sswitch_11
    .end sparse-switch
.end method

.method private updateCompletedNum()V
    .locals 7

    const v5, 0x7f080026

    invoke-virtual {p0, v5}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    const v5, 0x7f060004

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    array-length v1, v5

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v5, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lcom/zte/engineer/EngineerCode$ItemContent;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public autoTest(I)V
    .locals 2
    .param p1    # I

    iget-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->autoTest:Z

    if-eqz v0, :cond_0

    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    invoke-virtual {v1}, Lcom/zte/engineer/EngineerCode$MyAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/zte/engineer/EngineerCode;->nowPosition:I

    if-lt v0, v1, :cond_0

    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lcom/zte/engineer/EngineerCode;->implementItemClick(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->autoTest:Z

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, 0x1

    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$ItemContent;->setChecked(Z)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$ItemContent;->setPassed(Z)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$MyAdapter;->replaceItems([Lcom/zte/engineer/EngineerCode$ItemContent;)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/zte/engineer/EngineerCode;->updateCompletedNum()V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    invoke-virtual {p0, p1}, Lcom/zte/engineer/EngineerCode;->autoTest(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x14

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$ItemContent;->setChecked(Z)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v0, v0, p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$ItemContent;->setPassed(Z)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$MyAdapter;->replaceItems([Lcom/zte/engineer/EngineerCode$ItemContent;)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/zte/engineer/EngineerCode;->updateCompletedNum()V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    invoke-virtual {p0, p1}, Lcom/zte/engineer/EngineerCode;->autoTest(I)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    aget v0, v0, p1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v0, " "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$ItemContent;->setChecked(Z)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    aget-object v0, v0, p1

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$ItemContent;->setPassed(Z)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    invoke-virtual {v0, v1}, Lcom/zte/engineer/EngineerCode$MyAdapter;->replaceItems([Lcom/zte/engineer/EngineerCode$ItemContent;)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/zte/engineer/EngineerCode;->updateCompletedNum()V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    invoke-virtual {p0, p1}, Lcom/zte/engineer/EngineerCode;->autoTest(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f060012
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 21
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v3, "test"

    const-string v4, "onCreate"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "AUTOTEST"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/zte/engineer/EngineerCode;->autoTest:Z

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/zte/engineer/EngineerCode;->autoTestFactory:Z

    :cond_0
    const v3, 0x7f03000a

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setContentView(I)V

    const v3, 0x7f080027

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    sget-object v3, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    array-length v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/zte/engineer/EngineerCode;->testCount:I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/zte/engineer/EngineerCode;->testCompleted:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/zte/engineer/EngineerCode;->testCount:I

    new-array v3, v3, [Lcom/zte/engineer/EngineerCode$ItemContent;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    invoke-virtual/range {p0 .. p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const/4 v14, 0x0

    if-eqz p1, :cond_1

    const-string v3, "StateArrayList"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    :cond_1
    if-eqz v14, :cond_4

    const/4 v11, 0x0

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/4 v12, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/zte/engineer/EngineerCode;->testCount:I

    if-ge v12, v3, :cond_5

    const/4 v11, 0x0

    const/16 v18, 0x0

    sget-object v3, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    aget v3, v3, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual {v14}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v10

    array-length v15, v10

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v15, :cond_3

    aget-object v17, v10, v13

    move-object/from16 v3, v17

    check-cast v3, Lcom/zte/engineer/SaveItems;

    iget-object v3, v3, Lcom/zte/engineer/SaveItems;->title:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v3, v17

    check-cast v3, Lcom/zte/engineer/SaveItems;

    iget-boolean v11, v3, Lcom/zte/engineer/SaveItems;->checked:Z

    check-cast v17, Lcom/zte/engineer/SaveItems;

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/zte/engineer/SaveItems;->pass:Z

    move/from16 v18, v0

    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    new-instance v4, Lcom/zte/engineer/EngineerCode$ItemContent;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v18

    invoke-direct {v4, v0, v1, v11, v2}, Lcom/zte/engineer/EngineerCode$ItemContent;-><init>(Lcom/zte/engineer/EngineerCode;Ljava/lang/String;ZZ)V

    aput-object v4, v3, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_4
    const/4 v12, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/zte/engineer/EngineerCode;->testCount:I

    if-ge v12, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    new-instance v4, Lcom/zte/engineer/EngineerCode$ItemContent;

    sget-object v5, Lcom/zte/engineer/EngineerCode;->stringIDs:[I

    aget v5, v5, v12

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v6, v7}, Lcom/zte/engineer/EngineerCode$ItemContent;-><init>(Lcom/zte/engineer/EngineerCode;Ljava/lang/String;ZZ)V

    aput-object v4, v3, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_5
    new-instance v3, Lcom/zte/engineer/EngineerCode$MyAdapter;

    const v6, 0x7f030005

    const v7, 0x7f080016

    const v8, 0x7f080017

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    move-object/from16 v4, p0

    move-object/from16 v5, p0

    invoke-direct/range {v3 .. v9}, Lcom/zte/engineer/EngineerCode$MyAdapter;-><init>(Lcom/zte/engineer/EngineerCode;Landroid/content/Context;III[Lcom/zte/engineer/EngineerCode$ItemContent;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/zte/engineer/EngineerCode;->adapter:Lcom/zte/engineer/EngineerCode$MyAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    new-instance v16, Lcom/zte/engineer/EngineerCode$1;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/zte/engineer/EngineerCode$1;-><init>(Lcom/zte/engineer/EngineerCode;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/zte/engineer/EngineerCode;->list:Landroid/widget/ListView;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-boolean v1, p0, Lcom/zte/engineer/EngineerCode;->autoTestFactory:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/zte/engineer/EngineerCode;->autoTest:Z

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.Settings$PrivacySettingsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/zte/engineer/EngineerCode;->updateCompletedNum()V

    iget-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->startAuto:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zte/engineer/EngineerCode;->startAuto:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/zte/engineer/EngineerCode;->autoTest(I)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCode;->items:[Lcom/zte/engineer/EngineerCode$ItemContent;

    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    new-instance v5, Lcom/zte/engineer/SaveItems;

    invoke-virtual {v2}, Lcom/zte/engineer/EngineerCode$ItemContent;->isChecked()Z

    move-result v6

    invoke-virtual {v2}, Lcom/zte/engineer/EngineerCode$ItemContent;->isPassed()Z

    move-result v7

    invoke-virtual {v2}, Lcom/zte/engineer/EngineerCode$ItemContent;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/zte/engineer/SaveItems;-><init>(ZZLjava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "StateArrayList"

    invoke-virtual {p1, v5, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
