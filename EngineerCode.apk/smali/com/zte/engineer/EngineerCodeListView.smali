.class public Lcom/zte/engineer/EngineerCodeListView;
.super Landroid/app/Activity;
.source "EngineerCodeListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/engineer/EngineerCodeListView$1;,
        Lcom/zte/engineer/EngineerCodeListView$MyAdapter;,
        Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;
    }
.end annotation


# instance fields
.field private listView:Landroid/widget/ListView;

.field r:Landroid/content/res/Resources;

.field private stringsIDs:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->listView:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->r:Landroid/content/res/Resources;

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->stringsIDs:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f06006c
        0x7f06006d
        0x7f06006e
        0x7f06006f
    .end array-data
.end method

.method static synthetic access$100(Lcom/zte/engineer/EngineerCodeListView;)[I
    .locals 1
    .param p0    # Lcom/zte/engineer/EngineerCodeListView;

    iget-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->stringsIDs:[I

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->r:Landroid/content/res/Resources;

    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/zte/engineer/EngineerCodeListView$MyAdapter;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/zte/engineer/EngineerCodeListView$MyAdapter;-><init>(Lcom/zte/engineer/EngineerCodeListView;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/zte/engineer/EngineerCodeListView$MyOnItemClickListener;-><init>(Lcom/zte/engineer/EngineerCodeListView;Lcom/zte/engineer/EngineerCodeListView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/zte/engineer/EngineerCodeListView;->listView:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void
.end method
