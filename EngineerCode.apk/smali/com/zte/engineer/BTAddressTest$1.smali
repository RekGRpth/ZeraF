.class Lcom/zte/engineer/BTAddressTest$1;
.super Landroid/content/BroadcastReceiver;
.source "BTAddressTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/BTAddressTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/BTAddressTest;


# direct methods
.method constructor <init>(Lcom/zte/engineer/BTAddressTest;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const v8, 0x7f06004c

    const v3, 0x7f06004b

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "android.bluetooth.adapter.extra.STATE"

    const/high16 v2, -0x80000000

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    iget-object v1, v1, Lcom/zte/engineer/BTAddressTest;->mBluetoothStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    const v5, 0x7f060022

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    iput-object v2, v1, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    iget-object v1, v1, Lcom/zte/engineer/BTAddressTest;->mBluetoothAddress:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    iget-object v4, v4, Lcom/zte/engineer/BTAddressTest;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    iget-object v1, v1, Lcom/zte/engineer/BTAddressTest;->mBluetoothStatus:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    const v5, 0x7f060023

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    iget-object v1, v1, Lcom/zte/engineer/BTAddressTest;->mBluetoothAddress:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    invoke-virtual {v2, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/zte/engineer/BTAddressTest$1;->this$0:Lcom/zte/engineer/BTAddressTest;

    const v5, 0x7f060021

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
