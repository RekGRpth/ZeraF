.class public Lcom/zte/engineer/AutoTest;
.super Landroid/app/Activity;
.source "AutoTest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AutoLoopTest"


# instance fields
.field list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private unusefulcode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/zte/engineer/AutoTest;->unusefulcode:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    return-void
.end method

.method private initTestList()V
    .locals 7

    const/high16 v6, 0x10000000

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extras.CAMERA_FACING"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/UsbMainActivity;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/SIMTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/TestPhoneActivity;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extras.CAMERA_FACING"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.mediatek.FMRadio"

    const-string v4, "com.mediatek.FMRadio.FMRadioActivity"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/AutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/TouchScreenTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/RingerTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/LcdTestActivity;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/VibratorTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/BacklightTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/AudioLoopTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/SDcardTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/MemoryTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/KeyTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/BTAddressTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.bluetooth.BluetoothSettings"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/AutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.mediatek.ygps"

    const-string v3, "com.mediatek.ygps.YgpsActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/WifiAddressTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.wifi.WifiSettings"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/AutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/ImeiTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/SensorTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/AutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.Settings$PrivacySettingsActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "do_factory_reset"

    const-string v3, "FactoryMode"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "AutoLoopTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "list count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private newIntent(Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    add-int/lit8 v0, p1, 0x1

    iget v2, p0, Lcom/zte/engineer/AutoTest;->unusefulcode:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/zte/engineer/AutoTest;->unusefulcode:I

    iget v2, p0, Lcom/zte/engineer/AutoTest;->unusefulcode:I

    if-ge v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/zte/engineer/AutoTest;->initTestList()V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/zte/engineer/AutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
