.class public Lcom/zte/engineer/RingerTest;
.super Lcom/zte/engineer/ZteActivity;
.source "RingerTest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RingerTest"


# instance fields
.field private mMediaP:Landroid/media/MediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/RingerTest;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0    # Lcom/zte/engineer/RingerTest;

    iget-object v0, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    return-object v0
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 0
    .param p1    # I

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/RingerTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/RingerTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/RingerTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f080055

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f06000c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const/high16 v1, 0x7f040000

    invoke-static {p0, v1}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/zte/engineer/RingerTest$1;

    invoke-direct {v2, p0}, Lcom/zte/engineer/RingerTest$1;-><init>(Lcom/zte/engineer/RingerTest;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    const v1, 0x7f08005e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08005f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/zte/engineer/RingerTest;->mMediaP:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Lcom/zte/engineer/RingerTest$2;

    invoke-direct {v0, p0}, Lcom/zte/engineer/RingerTest$2;-><init>(Lcom/zte/engineer/RingerTest;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
