.class Lcom/zte/engineer/SensorTest$1;
.super Ljava/lang/Object;
.source "SensorTest.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zte/engineer/SensorTest;->initSensorListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/SensorTest;


# direct methods
.method constructor <init>(Lcom/zte/engineer/SensorTest;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1    # Landroid/hardware/SensorEvent;

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    invoke-static {v0, v1}, Lcom/zte/engineer/SensorTest;->access$002(Lcom/zte/engineer/SensorTest;F)F

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    invoke-static {v0, v1}, Lcom/zte/engineer/SensorTest;->access$102(Lcom/zte/engineer/SensorTest;F)F

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/zte/engineer/SensorTest;->access$202(Lcom/zte/engineer/SensorTest;F)F

    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->GsensorX:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$000(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->GsensorY:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$100(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->GsensorZ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$200(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->MagneticX:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$000(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->MagneticY:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$100(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->MagneticZ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$200(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->GyroscopeX:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$000(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->GyroscopeY:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$100(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->GyroscopeZ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$200(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->LightView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    const v2, 0x7f06005d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v3}, Lcom/zte/engineer/SensorTest;->access$000(Lcom/zte/engineer/SensorTest;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    iget-object v0, v0, Lcom/zte/engineer/SensorTest;->ProximityView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/zte/engineer/SensorTest$1;->this$0:Lcom/zte/engineer/SensorTest;

    invoke-static {v1}, Lcom/zte/engineer/SensorTest;->access$000(Lcom/zte/engineer/SensorTest;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
