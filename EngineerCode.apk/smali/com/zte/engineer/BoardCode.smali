.class public Lcom/zte/engineer/BoardCode;
.super Landroid/app/Activity;
.source "BoardCode.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private isLetterOrNumber(C)Z
    .locals 1
    .param p1    # C

    const/16 v0, 0x5a

    if-gt p1, v0, :cond_0

    const/16 v0, 0x41

    if-ge p1, v0, :cond_2

    :cond_0
    const/16 v0, 0x30

    if-lt p1, v0, :cond_1

    const/16 v0, 0x39

    if-le p1, v0, :cond_2

    :cond_1
    const/16 v0, 0x7a

    if-gt p1, v0, :cond_3

    const/16 v0, 0x61

    if-lt p1, v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v9, Landroid/widget/TextView;

    invoke-direct {v9, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/high16 v10, 0x41a00000

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextSize(F)V

    const-string v10, "phone"

    invoke-virtual {p0, v10}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/telephony/TelephonyManager;

    const/4 v2, 0x0

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/telephony/TelephonyManager;->getSN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x3f

    if-ge v10, v11, :cond_0

    const/16 v0, 0x30

    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x3e

    if-ge v10, v11, :cond_1

    const/16 v1, 0x30

    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x3d

    if-ge v10, v11, :cond_2

    const/16 v5, 0x30

    :goto_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x2b

    if-ge v10, v11, :cond_3

    const/16 v6, 0x30

    :goto_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x2e

    if-ge v10, v11, :cond_4

    const/16 v7, 0x30

    :goto_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v10

    const/16 v11, 0x10

    if-ge v10, v11, :cond_5

    move-object v4, v2

    :goto_5
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Cal And NSFT Status : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0, v5}, Lcom/zte/engineer/BoardCode;->isLetterOrNumber(C)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v10

    :goto_6
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0, v1}, Lcom/zte/engineer/BoardCode;->isLetterOrNumber(C)Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v10

    :goto_7
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0, v0}, Lcom/zte/engineer/BoardCode;->isLetterOrNumber(C)Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v10

    :goto_8
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "BT And Wifi Write Status : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0, v6}, Lcom/zte/engineer/BoardCode;->isLetterOrNumber(C)Z

    move-result v10

    if-eqz v10, :cond_9

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v10

    :goto_9
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-direct {p0, v7}, Lcom/zte/engineer/BoardCode;->isLetterOrNumber(C)Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-static {v7}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v10

    :goto_a
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Barcode : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v9}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void

    :cond_0
    const/16 v10, 0x3e

    invoke-virtual {v2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto/16 :goto_0

    :cond_1
    const/16 v10, 0x3d

    invoke-virtual {v2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto/16 :goto_1

    :cond_2
    const/16 v10, 0x3c

    invoke-virtual {v2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v5

    goto/16 :goto_2

    :cond_3
    const/16 v10, 0x2a

    invoke-virtual {v2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    goto/16 :goto_3

    :cond_4
    const/16 v10, 0x2d

    invoke-virtual {v2, v10}, Ljava/lang/String;->charAt(I)C

    move-result v7

    goto/16 :goto_4

    :cond_5
    const/4 v10, 0x0

    const/16 v11, 0x10

    invoke-virtual {v2, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_5

    :cond_6
    const-string v10, "0"

    goto/16 :goto_6

    :cond_7
    const-string v10, "0"

    goto/16 :goto_7

    :cond_8
    const-string v10, "0"

    goto/16 :goto_8

    :cond_9
    const-string v10, "0"

    goto :goto_9

    :cond_a
    const-string v10, "0"

    goto :goto_a
.end method
