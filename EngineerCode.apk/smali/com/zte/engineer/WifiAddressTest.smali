.class public Lcom/zte/engineer/WifiAddressTest;
.super Lcom/zte/engineer/ZteActivity;
.source "WifiAddressTest.java"


# instance fields
.field mWifiAddress:Landroid/widget/TextView;

.field mWifiStatus:Landroid/widget/TextView;

.field macAddress:Ljava/lang/String;

.field protected mwifistatusReceiver:Landroid/content/BroadcastReceiver;

.field private myHandler:Landroid/os/Handler;

.field private needclose:Z

.field wifiInfo:Landroid/net/wifi/WifiInfo;

.field wifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/WifiAddressTest;->needclose:Z

    new-instance v0, Lcom/zte/engineer/WifiAddressTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/WifiAddressTest$1;-><init>(Lcom/zte/engineer/WifiAddressTest;)V

    iput-object v0, p0, Lcom/zte/engineer/WifiAddressTest;->mwifistatusReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/zte/engineer/WifiAddressTest$2;

    invoke-direct {v0, p0}, Lcom/zte/engineer/WifiAddressTest$2;-><init>(Lcom/zte/engineer/WifiAddressTest;)V

    iput-object v0, p0, Lcom/zte/engineer/WifiAddressTest;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/WifiAddressTest;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/zte/engineer/WifiAddressTest;

    iget-object v0, p0, Lcom/zte/engineer/WifiAddressTest;->myHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/zte/engineer/WifiAddressTest;->needclose:Z

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lcom/zte/engineer/WifiAddressTest;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    iput-boolean v1, p0, Lcom/zte/engineer/WifiAddressTest;->needclose:Z

    :cond_0
    iget-object v0, p0, Lcom/zte/engineer/WifiAddressTest;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/zte/engineer/WifiAddressTest;->mwifistatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/WifiAddressTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/WifiAddressTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/WifiAddressTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const v3, 0x7f06004e

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030012

    invoke-virtual {p0, v2}, Landroid/app/Activity;->setContentView(I)V

    const v2, 0x7f080055

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080057

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mWifiStatus:Landroid/widget/TextView;

    const v2, 0x7f080058

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mWifiAddress:Landroid/widget/TextView;

    const v2, 0x7f060013

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v2, "wifi"

    invoke-virtual {p0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    iput-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->wifiManager:Landroid/net/wifi/WifiManager;

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2, v6}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    iput-boolean v6, p0, Lcom/zte/engineer/WifiAddressTest;->needclose:Z

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mWifiStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const v5, 0x7f060023

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mWifiAddress:Landroid/widget/TextView;

    const v3, 0x7f06004d

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mwifistatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const v2, 0x7f08005e

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f08005f

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->wifiInfo:Landroid/net/wifi/WifiInfo;

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->wifiInfo:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->macAddress:Ljava/lang/String;

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mWifiStatus:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    const v5, 0x7f060022

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/zte/engineer/WifiAddressTest;->mWifiAddress:Landroid/widget/TextView;

    const v3, 0x7f06004f

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/zte/engineer/WifiAddressTest;->macAddress:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
