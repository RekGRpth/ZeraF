.class public Lcom/zte/engineer/EarPhoneAudioLoopTest;
.super Lcom/zte/engineer/ZteActivity;
.source "EarPhoneAudioLoopTest.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "EarPhoneAudioLoopTest"


# instance fields
.field private isHeadsetConnect:I

.field private mAudioManager:Landroid/media/AudioManager;

.field private mDialog:Landroid/app/AlertDialog;

.field protected mHeadsetReceiver:Landroid/content/BroadcastReceiver;

.field private running:Z

.field private soundeffect:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    iput-object v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    iput-boolean v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->soundeffect:Z

    iput-boolean v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->running:Z

    iput-object v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mDialog:Landroid/app/AlertDialog;

    new-instance v0, Lcom/zte/engineer/EarPhoneAudioLoopTest$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/EarPhoneAudioLoopTest$1;-><init>(Lcom/zte/engineer/EarPhoneAudioLoopTest;)V

    iput-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/EarPhoneAudioLoopTest;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/zte/engineer/EarPhoneAudioLoopTest;

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zte/engineer/EarPhoneAudioLoopTest;)V
    .locals 0
    .param p0    # Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-direct {p0}, Lcom/zte/engineer/EarPhoneAudioLoopTest;->displayInsertEarphoneDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/zte/engineer/EarPhoneAudioLoopTest;)V
    .locals 0
    .param p0    # Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-direct {p0}, Lcom/zte/engineer/EarPhoneAudioLoopTest;->cancleInsertEarphoneDialog()V

    return-void
.end method

.method static synthetic access$302(Lcom/zte/engineer/EarPhoneAudioLoopTest;I)I
    .locals 0
    .param p0    # Lcom/zte/engineer/EarPhoneAudioLoopTest;
    .param p1    # I

    iput p1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->isHeadsetConnect:I

    return p1
.end method

.method static synthetic access$400(Lcom/zte/engineer/EarPhoneAudioLoopTest;)Z
    .locals 1
    .param p0    # Lcom/zte/engineer/EarPhoneAudioLoopTest;

    iget-boolean v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->running:Z

    return v0
.end method

.method private cancleInsertEarphoneDialog()V
    .locals 1

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method private displayInsertEarphoneDialog()V
    .locals 1

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->running:Z

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/EarPhoneAudioLoopTest;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/EarPhoneAudioLoopTest;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/EarPhoneAudioLoopTest;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08005e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v1, 0x7f030012

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(I)V

    const v1, 0x7f080055

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f060089

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "audio"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x4

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/media/AudioSystem;->getDeviceConnectionState(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->isHeadsetConnect:I

    const v1, 0x7f08005e

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08005f

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060088

    invoke-virtual {p0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/zte/engineer/EarPhoneAudioLoopTest$2;

    invoke-direct {v3, p0}, Lcom/zte/engineer/EarPhoneAudioLoopTest$2;-><init>(Lcom/zte/engineer/EarPhoneAudioLoopTest;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->running:Z

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->running:Z

    new-instance v1, Lcom/zte/engineer/EarPhoneAudioLoopTest$3;

    invoke-direct {v1, p0}, Lcom/zte/engineer/EarPhoneAudioLoopTest$3;-><init>(Lcom/zte/engineer/EarPhoneAudioLoopTest;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->running:Z

    iget-object v0, p0, Lcom/zte/engineer/EarPhoneAudioLoopTest;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "SET_LOOPBACK_TYPE=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    return-void
.end method
