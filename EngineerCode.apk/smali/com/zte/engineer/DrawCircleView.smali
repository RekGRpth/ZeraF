.class public Lcom/zte/engineer/DrawCircleView;
.super Landroid/view/SurfaceView;
.source "DrawCircleView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final TAG:Ljava/lang/String; = "DrawCircleView"


# instance fields
.field final RADIUS:I

.field bFirst:Z

.field private debug:Z

.field hadFill:[Z

.field mCanvas:Landroid/graphics/Canvas;

.field mCircleRects:[Landroid/graphics/Rect;

.field mCurPoint:Landroid/graphics/Point;

.field mHolder:Landroid/view/SurfaceHolder;

.field mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    new-array v0, v2, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    new-array v0, v2, [Z

    iput-object v0, p0, Lcom/zte/engineer/DrawCircleView;->hadFill:[Z

    const/16 v0, 0x46

    iput v0, p0, Lcom/zte/engineer/DrawCircleView;->RADIUS:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zte/engineer/DrawCircleView;->bFirst:Z

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mCanvas:Landroid/graphics/Canvas;

    iput-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mCurPoint:Landroid/graphics/Point;

    iput-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/DrawCircleView;->debug:Z

    invoke-virtual {p0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method

.method private drawHollowCircle()V
    .locals 9

    iget-object v5, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v1

    iget-object v5, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x428c0000

    iget-object v8, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v5, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private drawSolidCircle(I)V
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    const-string v1, "DrawCircleView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "________draw fill circle INDEX: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x428c0000

    iget-object v4, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/zte/engineer/DrawCircleView;->hadFill:[Z

    invoke-virtual {p0, v1}, Lcom/zte/engineer/DrawCircleView;->fillAll([Z)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/zte/engineer/DrawCircleView$1;

    invoke-direct {v2, p0}, Lcom/zte/engineer/DrawCircleView$1;-><init>(Lcom/zte/engineer/DrawCircleView;)V

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    iget-object v1, p0, Lcom/zte/engineer/DrawCircleView;->mHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    return-void
.end method

.method private initPaint()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mPaint:Landroid/graphics/Paint;

    const v1, -0xff0100

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method private setCircleRects([Landroid/graphics/Rect;)V
    .locals 10
    .param p1    # [Landroid/graphics/Rect;

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0x8c

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    rem-int/lit8 v2, v0, 0xa

    if-nez v2, :cond_0

    :goto_0
    const-string v2, "DrawCircleView"

    const-string v3, "____setCircleRects(...)______measure width: %d, measure height : %d"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v6, v6, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v2, p1, v6

    new-instance v2, Landroid/graphics/Rect;

    add-int/lit16 v3, v1, -0x8c

    invoke-direct {v2, v3, v6, v1, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v2, p1, v8

    new-instance v2, Landroid/graphics/Rect;

    add-int/lit16 v3, v0, -0x8c

    invoke-direct {v2, v6, v3, v7, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v2, p1, v9

    const/4 v2, 0x3

    new-instance v3, Landroid/graphics/Rect;

    add-int/lit16 v4, v1, -0x8c

    add-int/lit16 v5, v0, -0x8c

    invoke-direct {v3, v4, v5, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v3, p1, v2

    const/4 v2, 0x4

    new-instance v3, Landroid/graphics/Rect;

    div-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, -0x46

    div-int/lit8 v5, v0, 0x2

    add-int/lit8 v5, v5, -0x46

    div-int/lit8 v6, v1, 0x2

    add-int/lit8 v6, v6, 0x46

    div-int/lit8 v7, v0, 0x2

    add-int/lit8 v7, v7, 0x46

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v3, p1, v2

    return-void

    :cond_0
    div-int/lit8 v2, v0, 0xa

    mul-int/lit8 v0, v2, 0xa

    goto :goto_0
.end method


# virtual methods
.method fillAll([Z)Z
    .locals 7
    .param p1    # [Z

    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-boolean v1, v0, v2

    if-nez v1, :cond_0

    const-string v4, "DrawCircleView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "____fillAll____b :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v8, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v4, v5

    iget-object v5, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    array-length v2, v5

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v5, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    aget-object v5, v5, v1

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "DrawCircleView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "______draw stroken circle no."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/zte/engineer/DrawCircleView;->hadFill:[Z

    aput-boolean v8, v5, v1

    invoke-direct {p0, v1}, Lcom/zte/engineer/DrawCircleView;->drawSolidCircle(I)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v8
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-direct {p0}, Lcom/zte/engineer/DrawCircleView;->initPaint()V

    iget-object v0, p0, Lcom/zte/engineer/DrawCircleView;->mCircleRects:[Landroid/graphics/Rect;

    invoke-direct {p0, v0}, Lcom/zte/engineer/DrawCircleView;->setCircleRects([Landroid/graphics/Rect;)V

    invoke-direct {p0}, Lcom/zte/engineer/DrawCircleView;->drawHollowCircle()V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method
