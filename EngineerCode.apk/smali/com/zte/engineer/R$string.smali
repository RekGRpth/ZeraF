.class public final Lcom/zte/engineer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Bluetooth_address:I = 0x7f060074

.field public static final Board_serial:I = 0x7f060073

.field public static final CMMB_Chip:I = 0x7f06007d

.field public static final CMMB_Coupling:I = 0x7f06007e

.field public static final China_Mobile:I = 0x7f060093

.field public static final China_Mobile_num:I = 0x7f060094

.field public static final China_Telecom:I = 0x7f06008f

.field public static final China_Telecom_num:I = 0x7f060090

.field public static final China_Unicom:I = 0x7f060091

.field public static final China_Unicom_num:I = 0x7f060092

.field public static final GPS_Chip:I = 0x7f06007f

.field public static final GSM_Calibration:I = 0x7f06007a

.field public static final GSM_FinalTest:I = 0x7f06007c

.field public static final GSP_Coupling:I = 0x7f060080

.field public static final PSN_code:I = 0x7f060072

.field public static final SENSOR_ACCELEROMETER_Chip:I = 0x7f060083

.field public static final SENSOR_MAGNETIC_FIELD_Chip:I = 0x7f060084

.field public static final SIM:I = 0x7f060065

.field public static final SIM_INSERT:I = 0x7f060069

.field public static final SIM_NOT_INSERT:I = 0x7f06006a

.field public static final TD_Calibration:I = 0x7f060079

.field public static final TD_Coupling:I = 0x7f060085

.field public static final TD_FinalTest:I = 0x7f06007b

.field public static final WIFI_Chip:I = 0x7f060081

.field public static final WIFI_Coupling:I = 0x7f060082

.field public static final abt:I = 0x7f06009e

.field public static final accelerometer:I = 0x7f060059

.field public static final app_name:I = 0x7f060003

.field public static final audio_loop:I = 0x7f060007

.field public static final audio_receiver:I = 0x7f06006b

.field public static final audioloop:I = 0x7f060017

.field public static final back:I = 0x7f060001

.field public static final backlight:I = 0x7f06000a

.field public static final baseband_is:I = 0x7f060063

.field public static final battery_info:I = 0x7f060008

.field public static final battery_info_health_dead:I = 0x7f06003a

.field public static final battery_info_health_good:I = 0x7f060038

.field public static final battery_info_health_label:I = 0x7f060029

.field public static final battery_info_health_over_voltage:I = 0x7f06003b

.field public static final battery_info_health_overheat:I = 0x7f060039

.field public static final battery_info_health_unknown:I = 0x7f060037

.field public static final battery_info_health_unspecified_failure:I = 0x7f06003c

.field public static final battery_info_level_label:I = 0x7f060028

.field public static final battery_info_s:I = 0x7f060025

.field public static final battery_info_scale_label:I = 0x7f060027

.field public static final battery_info_status_charging:I = 0x7f060031

.field public static final battery_info_status_charging_ac:I = 0x7f060032

.field public static final battery_info_status_charging_usb:I = 0x7f060033

.field public static final battery_info_status_discharging:I = 0x7f060034

.field public static final battery_info_status_full:I = 0x7f060036

.field public static final battery_info_status_label:I = 0x7f060026

.field public static final battery_info_status_not_charging:I = 0x7f060035

.field public static final battery_info_status_unknown:I = 0x7f060030

.field public static final battery_info_technology_label:I = 0x7f06002a

.field public static final battery_info_temperature_label:I = 0x7f06002d

.field public static final battery_info_temperature_units:I = 0x7f06002e

.field public static final battery_info_uptime:I = 0x7f06002f

.field public static final battery_info_voltage_label:I = 0x7f06002b

.field public static final battery_info_voltage_units:I = 0x7f06002c

.field public static final battery_information:I = 0x7f06006f

.field public static final bt_address:I = 0x7f060011

.field public static final bt_address_is:I = 0x7f06004c

.field public static final bt_detect:I = 0x7f060012

.field public static final bt_no:I = 0x7f06009c

.field public static final bt_pass:I = 0x7f06009b

.field public static final bt_read_address:I = 0x7f06004d

.field public static final bt_status:I = 0x7f06004b

.field public static final bt_test:I = 0x7f0600a2

.field public static final bt_test_info:I = 0x7f0600a0

.field public static final btn_text_cancel:I = 0x7f06008c

.field public static final btn_text_ok:I = 0x7f06008b

.field public static final buildversion_is:I = 0x7f060064

.field public static final calibration_string:I = 0x7f0600aa

.field public static final camera:I = 0x7f060019

.field public static final camera_back:I = 0x7f06001a

.field public static final camera_front:I = 0x7f06001b

.field public static final camera_test:I = 0x7f0600a5

.field public static final camera_test_info:I = 0x7f0600a4

.field public static final completed:I = 0x7f060004

.field public static final device_info:I = 0x7f060062

.field public static final dialog_title:I = 0x7f06008d

.field public static final display_IMEI:I = 0x7f060050

.field public static final display_IMEI_1:I = 0x7f060051

.field public static final display_IMEI_2:I = 0x7f060052

.field public static final display_IMSI:I = 0x7f060053

.field public static final display_IMSI_1:I = 0x7f060054

.field public static final display_IMSI_2:I = 0x7f060055

.field public static final display_SIM:I = 0x7f060068

.field public static final display_SIM_1:I = 0x7f060066

.field public static final display_SIM_2:I = 0x7f060067

.field public static final earphone_audio_loop:I = 0x7f060089

.field public static final falsed:I = 0x7f060086

.field public static final flag:I = 0x7f060077

.field public static final flash_type:I = 0x7f060076

.field public static final fm_test:I = 0x7f060018

.field public static final fm_test_info:I = 0x7f0600a1

.field public static final getInfo:I = 0x7f06009a

.field public static final gps:I = 0x7f06001f

.field public static final gyroscope:I = 0x7f06005b

.field public static final hardware_version:I = 0x7f060071

.field public static final hbt:I = 0x7f06009d

.field public static final hello:I = 0x7f060000

.field public static final imei:I = 0x7f06001c

.field public static final imsi:I = 0x7f06001d

.field public static final insert_earphone_warning:I = 0x7f060088

.field public static final key_back:I = 0x7f060042

.field public static final key_home:I = 0x7f060041

.field public static final key_menu:I = 0x7f060040

.field public static final key_power:I = 0x7f06003d

.field public static final key_search:I = 0x7f060043

.field public static final key_test:I = 0x7f060006

.field public static final key_volume_down:I = 0x7f06003f

.field public static final key_volume_up:I = 0x7f06003e

.field public static final lcd:I = 0x7f060009

.field public static final lcd_off:I = 0x7f06000d

.field public static final light:I = 0x7f06005c

.field public static final light_is:I = 0x7f06005d

.field public static final magnetic:I = 0x7f06005a

.field public static final memory:I = 0x7f060010

.field public static final next:I = 0x7f060002

.field public static final no:I = 0x7f060097

.field public static final non_front_camera:I = 0x7f06008a

.field public static final note_phone:I = 0x7f060098

.field public static final off:I = 0x7f060023

.field public static final on:I = 0x7f060022

.field public static final others:I = 0x7f060095

.field public static final pass:I = 0x7f060087

.field public static final phone_test:I = 0x7f06006d

.field public static final power:I = 0x7f060024

.field public static final produce_information:I = 0x7f06006c

.field public static final proximity:I = 0x7f06005e

.field public static final radio_info:I = 0x7f060015

.field public static final recover_settings:I = 0x7f06006e

.field public static final ring:I = 0x7f060096

.field public static final ringer:I = 0x7f06000c

.field public static final sd2_info:I = 0x7f06000f

.field public static final sd_available:I = 0x7f06004a

.field public static final sd_info:I = 0x7f06000e

.field public static final sd_info_lable:I = 0x7f060099

.field public static final sd_mounted:I = 0x7f060046

.field public static final sd_removed:I = 0x7f060047

.field public static final sd_title:I = 0x7f060044

.field public static final sd_total:I = 0x7f060048

.field public static final sd_used:I = 0x7f060049

.field public static final sensor:I = 0x7f06001e

.field public static final serialnumber:I = 0x7f060061

.field public static final smssecurity:I = 0x7f060075

.field public static final software_version:I = 0x7f060070

.field public static final state:I = 0x7f060045

.field public static final test_flag:I = 0x7f060060

.field public static final test_flag_title:I = 0x7f060078

.field public static final test_item:I = 0x7f060005

.field public static final test_phone:I = 0x7f06008e

.field public static final totel:I = 0x7f0600a6

.field public static final totel_fail:I = 0x7f0600a8

.field public static final totel_pass:I = 0x7f0600a7

.field public static final touch_test:I = 0x7f06005f

.field public static final touchpanel:I = 0x7f060020

.field public static final touchscreen:I = 0x7f060016

.field public static final unknown:I = 0x7f060021

.field public static final version_number:I = 0x7f0600a9

.field public static final vibrator:I = 0x7f06000b

.field public static final wifi_address:I = 0x7f060013

.field public static final wifi_address_is:I = 0x7f06004f

.field public static final wifi_detect:I = 0x7f060014

.field public static final wifi_status:I = 0x7f06004e

.field public static final wifi_test:I = 0x7f0600a3

.field public static final wifi_test_info:I = 0x7f06009f

.field public static final x_is:I = 0x7f060056

.field public static final y_is:I = 0x7f060057

.field public static final z_is:I = 0x7f060058


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
