.class public Lcom/zte/engineer/MGAutoTest;
.super Landroid/app/Activity;
.source "MGAutoTest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MGAutoTest"


# instance fields
.field list:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private reCode:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private res:Landroid/content/res/Resources;

.field private result:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private unusefulcode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/zte/engineer/MGAutoTest;->unusefulcode:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/MGAutoTest;->reCode:Ljava/util/List;

    return-void
.end method

.method private initTestList()V
    .locals 7

    const v5, 0x7f06000b

    const/high16 v6, 0x10000000

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/UsbMainActivity;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060024

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/SIMTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060065

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/TestPhoneActivity;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f06006d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.mediatek.ygps"

    const-string v4, "com.mediatek.ygps.YgpsActivity"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/MGAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f06001f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/TouchScreenTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060020

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/RingerTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f06000c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/LcdTestActivity;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/VibratorTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/BacklightTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/AudioLoopTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/EarPhoneAudioLoopTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060089

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/SDcardTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f06000e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/MemoryTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/KeyTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/BTAddressTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060074

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.bluetooth.BluetoothSettings"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/MGAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/BlutoothTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f0600a2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/WifiAddressTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060013

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.wifi.WifiSettings"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/MGAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/WifiTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f0600a3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/ImeiTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f06001c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/SensorTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f06001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-string v3, "com.mediatek.FMRadio"

    const-string v4, "com.mediatek.FMRadio.FMRadioEMActivity"

    invoke-direct {p0, v3, v4}, Lcom/zte/engineer/MGAutoTest;->newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/FMTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f060018

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extras.CAMERA_FACING"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "android.intent.extras.CAMERA_FACING"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/CameraTest;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    iget-object v3, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    const v4, 0x7f0600a5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    const-class v3, Lcom/zte/engineer/ResultList;

    invoke-direct {p0, p0, v3}, Lcom/zte/engineer/MGAutoTest;->newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.Settings$PrivacySettingsActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "do_factory_reset"

    const-string v3, "FactoryMode"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "MGAutoTest"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "list count:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->reCode:Ljava/util/List;

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private newIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private newIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private newIntent(Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extras.CAMERA_FACING"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    add-int/lit8 v0, p1, 0x1

    const/16 v2, 0xa

    if-le p2, v2, :cond_0

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->reCode:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->reCode:Ljava/util/List;

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v2, p0, Lcom/zte/engineer/MGAutoTest;->unusefulcode:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/zte/engineer/MGAutoTest;->unusefulcode:I

    iget v2, p0, Lcom/zte/engineer/MGAutoTest;->unusefulcode:I

    if-ge v0, v2, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-ne v0, v2, :cond_3

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    const-string v3, "result"

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->result:Ljava/util/List;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    const-string v3, "recode"

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->reCode:Ljava/util/List;

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putIntegerArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/zte/engineer/MGAutoTest;->res:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/zte/engineer/MGAutoTest;->initTestList()V

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/zte/engineer/MGAutoTest;->list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method
