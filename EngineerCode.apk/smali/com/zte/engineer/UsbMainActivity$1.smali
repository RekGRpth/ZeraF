.class Lcom/zte/engineer/UsbMainActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "UsbMainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/UsbMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field intLevel:I

.field intScale:I

.field final synthetic this$0:Lcom/zte/engineer/UsbMainActivity;


# direct methods
.method constructor <init>(Lcom/zte/engineer/UsbMainActivity;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput v0, p0, Lcom/zte/engineer/UsbMainActivity$1;->intLevel:I

    iput v0, p0, Lcom/zte/engineer/UsbMainActivity$1;->intScale:I

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "level"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->intLevel:I

    const-string v1, "scale"

    const/16 v2, 0x64

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->intScale:I

    const-string v1, "Battery V"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "voltage"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "Battery T"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temperature"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u5f53\u524d\u7535\u538b\u4e3a\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "voltage"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryV:Ljava/lang/String;

    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\u5f53\u524d\u6e29\u5ea6\u4e3a\uff1a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "temperature"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryT:Ljava/lang/String;

    const-string v1, "status"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    const-string v1, "plugged"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u5145\u7535\u65b9\u5f0f: \u672a\u77e5\u9053\u72b6\u6001"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus2:Ljava/lang/String;

    :goto_1
    const-string v1, "health"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_2

    :cond_0
    :goto_2
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u72b6\u6001: \u5145\u7535\u72b6\u6001"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u72b6\u6001: \u653e\u7535\u72b6\u6001"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u72b6\u6001: \u672a\u5145\u7535"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u72b6\u6001: \u5145\u6ee1\u7535"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u72b6\u6001: \u672a\u77e5\u9053\u72b6\u6001"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u5145\u7535\u65b9\u5f0f: AC\u5145\u7535"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus2:Ljava/lang/String;

    goto :goto_1

    :pswitch_6
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u5f53\u524d\u5145\u7535\u65b9\u5f0f: USB\u5145\u7535"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus2:Ljava/lang/String;

    goto :goto_1

    :pswitch_7
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u7535\u6c60\u72b6\u6001: \u672a\u77e5\u9519\u8bef"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryTemp:Ljava/lang/String;

    goto :goto_2

    :pswitch_8
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u7535\u6c60\u72b6\u6001: \u72b6\u6001\u826f\u597d"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryTemp:Ljava/lang/String;

    goto :goto_2

    :pswitch_9
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u7535\u6c60\u72b6\u6001: \u7535\u6c60\u6ca1\u6709\u7535"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryTemp:Ljava/lang/String;

    goto :goto_2

    :pswitch_a
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u7535\u6c60\u72b6\u6001: \u7535\u6c60\u7535\u538b\u8fc7\u9ad8"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryTemp:Ljava/lang/String;

    goto :goto_2

    :pswitch_b
    iget-object v1, p0, Lcom/zte/engineer/UsbMainActivity$1;->this$0:Lcom/zte/engineer/UsbMainActivity;

    const-string v2, "\u7535\u6c60\u72b6\u6001: \u7535\u6c60\u8fc7\u70ed"

    iput-object v2, v1, Lcom/zte/engineer/UsbMainActivity;->BatteryTemp:Ljava/lang/String;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
