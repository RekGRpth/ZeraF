.class public Lcom/zte/engineer/LcdTestActivity;
.super Lcom/zte/engineer/ZteActivity;
.source "LcdTestActivity.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "TestActivity"

.field private static final TIMER_EVENT_TICK:I = 0x1


# instance fields
.field private mBGColor:I

.field private mButton:Landroid/widget/LinearLayout;

.field private mFSLl:Landroid/widget/LinearLayout;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    new-instance v0, Lcom/zte/engineer/LcdTestActivity$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/LcdTestActivity$1;-><init>(Lcom/zte/engineer/LcdTestActivity;)V

    iput-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/zte/engineer/LcdTestActivity;)I
    .locals 1
    .param p0    # Lcom/zte/engineer/LcdTestActivity;

    iget v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/zte/engineer/LcdTestActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/zte/engineer/LcdTestActivity;

    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zte/engineer/LcdTestActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0    # Lcom/zte/engineer/LcdTestActivity;

    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mButton:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zte/engineer/LcdTestActivity;)V
    .locals 0
    .param p0    # Lcom/zte/engineer/LcdTestActivity;

    invoke-direct {p0}, Lcom/zte/engineer/LcdTestActivity;->changeLCDColor()V

    return-void
.end method

.method private changeLCDColor()V
    .locals 2

    iget v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mFSLl:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void

    :sswitch_0
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    goto :goto_0

    :sswitch_1
    const v0, -0xff0100

    iput v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    goto :goto_0

    :sswitch_2
    const v0, -0xffff01

    iput v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    goto :goto_0

    :sswitch_3
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0xffff01 -> :sswitch_3
        -0xff0100 -> :sswitch_2
        -0x10000 -> :sswitch_1
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public finishSelf(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    return-void
.end method

.method public onBackPressed()V
    .locals 2

    iget v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    const/high16 v1, -0x1000000

    if-ne v0, v1, :cond_0

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/zte/engineer/LcdTestActivity;->finishSelf(I)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/LcdTestActivity;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/LcdTestActivity;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/LcdTestActivity;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080023
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/16 v1, 0x400

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    const-string v0, "TestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EMLcdTest.onCreate: this="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f080021

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mFSLl:Landroid/widget/LinearLayout;

    const v0, 0x7f080022

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mButton:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mButton:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f080024

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080023

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setDefaultKeyMode(I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mFSLl:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/zte/engineer/LcdTestActivity;->mBGColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/zte/engineer/LcdTestActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x578

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
