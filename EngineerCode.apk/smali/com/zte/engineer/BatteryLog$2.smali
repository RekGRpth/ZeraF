.class Lcom/zte/engineer/BatteryLog$2;
.super Landroid/content/BroadcastReceiver;
.source "BatteryLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/BatteryLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/BatteryLog;


# direct methods
.method constructor <init>(Lcom/zte/engineer/BatteryLog;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "plugged"

    const/4 v7, 0x0

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$100(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "level"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$200(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "scale"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$300(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "voltage"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v9, 0x7f06002c

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$500(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const-string v9, "temperature"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    invoke-static {v8, v9}, Lcom/zte/engineer/BatteryLog;->access$400(Lcom/zte/engineer/BatteryLog;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v9, 0x7f06002e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$600(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "technology"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v6, "status"

    const/4 v7, 0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v6, 0x2

    if-ne v4, v6, :cond_3

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060031

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    if-lez v3, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const/4 v6, 0x1

    if-ne v3, v6, :cond_2

    const v6, 0x7f060032

    :goto_0
    invoke-virtual {v8, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$700(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v6, "health"

    const/4 v7, 0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v6, 0x2

    if-ne v1, v6, :cond_7

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060038

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    invoke-static {v6}, Lcom/zte/engineer/BatteryLog;->access$800(Lcom/zte/engineer/BatteryLog;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void

    :cond_2
    const v6, 0x7f060033

    goto :goto_0

    :cond_3
    const/4 v6, 0x3

    if-ne v4, v6, :cond_4

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060034

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_4
    const/4 v6, 0x4

    if-ne v4, v6, :cond_5

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060035

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_5
    const/4 v6, 0x5

    if-ne v4, v6, :cond_6

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060036

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_6
    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060030

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_7
    const/4 v6, 0x3

    if-ne v1, v6, :cond_8

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060039

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_8
    const/4 v6, 0x4

    if-ne v1, v6, :cond_9

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f06003a

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_9
    const/4 v6, 0x5

    if-ne v1, v6, :cond_a

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f06003b

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_a
    const/4 v6, 0x6

    if-ne v1, v6, :cond_b

    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f06003c

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_b
    iget-object v6, p0, Lcom/zte/engineer/BatteryLog$2;->this$0:Lcom/zte/engineer/BatteryLog;

    const v7, 0x7f060037

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method
