.class public Lcom/zte/engineer/ProduceInfoListView;
.super Landroid/app/Activity;
.source "ProduceInfoListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/engineer/ProduceInfoListView$MyAdapter;,
        Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;
    }
.end annotation


# static fields
.field private static final BLUETOOTH_ADDRESS:I = 0x5

.field private static final BOARD_SERIAL:I = 0x4

.field private static final FILENAME:Ljava/lang/String; = "nand"

.field private static final FLAG:I = 0x3

.field private static final FLASH_TYPE:I = 0x6

.field private static final HARDWARE_VERSION:I = 0x1

.field private static final PATH:Ljava/lang/String; = "/proc/driver/"

.field private static final PSN_CODE:I = 0x2

.field private static final SMSSECURITY:I = 0x7

.field private static final SOFTWARE_VERSION:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ProduceInfoListView"

.field private static final VALUE_FLASHSIZE:Ljava/lang/String; = "total size"

.field private static final VALUE_ID:Ljava/lang/String; = "name"


# instance fields
.field private infos:[Ljava/lang/String;

.field private isManualTurnOn:Z

.field private listView:Landroid/widget/ListView;

.field private mBluetooth:Landroid/bluetooth/BluetoothAdapter;

.field protected mBluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private myAdapter:Lcom/zte/engineer/ProduceInfoListView$MyAdapter;

.field private r:Landroid/content/res/Resources;

.field private stringsIDs:[I


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v3, p0, Lcom/zte/engineer/ProduceInfoListView;->listView:Landroid/widget/ListView;

    iput-object v3, p0, Lcom/zte/engineer/ProduceInfoListView;->r:Landroid/content/res/Resources;

    iput-object v3, p0, Lcom/zte/engineer/ProduceInfoListView;->stringsIDs:[I

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ""

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    iput-object v3, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    iput-object v3, p0, Lcom/zte/engineer/ProduceInfoListView;->myAdapter:Lcom/zte/engineer/ProduceInfoListView$MyAdapter;

    iput-boolean v4, p0, Lcom/zte/engineer/ProduceInfoListView;->isManualTurnOn:Z

    new-instance v0, Lcom/zte/engineer/ProduceInfoListView$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/ProduceInfoListView$1;-><init>(Lcom/zte/engineer/ProduceInfoListView;)V

    iput-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$100(Lcom/zte/engineer/ProduceInfoListView;)[I
    .locals 1
    .param p0    # Lcom/zte/engineer/ProduceInfoListView;

    iget-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->stringsIDs:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/zte/engineer/ProduceInfoListView;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/zte/engineer/ProduceInfoListView;

    iget-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/zte/engineer/ProduceInfoListView;)Lcom/zte/engineer/ProduceInfoListView$MyAdapter;
    .locals 1
    .param p0    # Lcom/zte/engineer/ProduceInfoListView;

    iget-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->myAdapter:Lcom/zte/engineer/ProduceInfoListView$MyAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/zte/engineer/ProduceInfoListView;)Landroid/content/res/Resources;
    .locals 1
    .param p0    # Lcom/zte/engineer/ProduceInfoListView;

    iget-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->r:Landroid/content/res/Resources;

    return-object v0
.end method

.method private getInfos()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v14, 0x0

    sget-object v15, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v14, 0x1

    const-string v15, "WMAD"

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v15, 0x2

    const-string v13, "phone"

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/telephony/TelephonyManager;

    invoke-virtual {v13}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v14, v15

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v14, 0x3

    const-string v15, ""

    aput-object v15, v13, v14

    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v13

    invoke-virtual {v13}, Landroid/telephony/TelephonyManager;->getSN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v13

    const/16 v14, 0xc

    if-le v13, v14, :cond_0

    const/4 v13, 0x0

    const/16 v14, 0xc

    invoke-virtual {v1, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_0
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v8, v2

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v8, :cond_1

    aget-char v4, v2, v7

    invoke-virtual {v12, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v14, 0x4

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v14, 0x5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/zte/engineer/ProduceInfoListView;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v15}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    new-instance v6, Ljava/io/File;

    const-string v13, "/proc/driver/"

    const-string v14, "nand"

    invoke-direct {v6, v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->isFile()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-virtual {v6}, Ljava/io/File;->canRead()Z

    move-result v13

    if-eqz v13, :cond_5

    new-instance v9, Ljava/io/BufferedReader;

    new-instance v13, Ljava/io/FileReader;

    invoke-direct {v13, v6}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v9, v13}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v3, 0x0

    :cond_2
    invoke-virtual {v9}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    const-string v13, "ProduceInfoListView"

    invoke-static {v13, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v13, ","

    invoke-virtual {v3, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v8, v2

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v8, :cond_2

    aget-object v10, v2, v7

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    :try_start_1
    const-string v13, "name"

    invoke-virtual {v11, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/zte/engineer/ProduceInfoListView;->infos:[Ljava/lang/String;

    const/4 v14, 0x6

    const-string v15, ":"

    invoke-virtual {v10, v15}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    aget-object v15, v15, v16

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    :cond_3
    :goto_2
    const-string v13, "ProduceInfoListView"

    invoke-static {v13, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    const-string v13, "total size"

    invoke-virtual {v11, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v13

    if-eqz v13, :cond_3

    goto :goto_2

    :catch_0
    move-exception v5

    :try_start_2
    const-string v13, "ProduceInfoListView"

    invoke-virtual {v5}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    :catch_1
    move-exception v5

    const-string v13, "ProduceInfoListView"

    invoke-virtual {v5}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-void
.end method

.method private hasSmsSecurity()Z
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.zte.smssecurity"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    if-nez v1, :cond_0

    :goto_1
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private initStringList()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ProduceInfoListView;->hasSmsSecurity()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->stringsIDs:[I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->stringsIDs:[I

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x7f060070
        0x7f060071
        0x7f060072
        0x7f060077
        0x7f060073
        0x7f060074
        0x7f060076
        0x7f060075
    .end array-data

    :array_1
    .array-data 4
        0x7f060070
        0x7f060071
        0x7f060072
        0x7f060077
        0x7f060073
        0x7f060074
        0x7f060076
    .end array-data
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/zte/engineer/ProduceInfoListView;->initStringList()V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/zte/engineer/ProduceInfoListView;->isManualTurnOn:Z

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->r:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/zte/engineer/ProduceInfoListView;->getInfos()V

    new-instance v1, Landroid/widget/ListView;

    invoke-direct {v1, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->listView:Landroid/widget/ListView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    new-instance v1, Lcom/zte/engineer/ProduceInfoListView$MyAdapter;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/zte/engineer/ProduceInfoListView$MyAdapter;-><init>(Lcom/zte/engineer/ProduceInfoListView;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->myAdapter:Lcom/zte/engineer/ProduceInfoListView$MyAdapter;

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/zte/engineer/ProduceInfoListView;->myAdapter:Lcom/zte/engineer/ProduceInfoListView$MyAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->listView:Landroid/widget/ListView;

    new-instance v2, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;-><init>(Lcom/zte/engineer/ProduceInfoListView;Lcom/zte/engineer/ProduceInfoListView$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView;->listView:Landroid/widget/ListView;

    invoke-virtual {p0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/zte/engineer/ProduceInfoListView;->isManualTurnOn:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/zte/engineer/ProduceInfoListView;->mBluetooth:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method
