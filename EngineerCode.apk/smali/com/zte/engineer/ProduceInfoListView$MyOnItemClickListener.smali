.class Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;
.super Ljava/lang/Object;
.source "ProduceInfoListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/ProduceInfoListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyOnItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/ProduceInfoListView;


# direct methods
.method private constructor <init>(Lcom/zte/engineer/ProduceInfoListView;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/ProduceInfoListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/zte/engineer/ProduceInfoListView;Lcom/zte/engineer/ProduceInfoListView$1;)V
    .locals 0
    .param p1    # Lcom/zte/engineer/ProduceInfoListView;
    .param p2    # Lcom/zte/engineer/ProduceInfoListView$1;

    invoke-direct {p0, p1}, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;-><init>(Lcom/zte/engineer/ProduceInfoListView;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/ProduceInfoListView;

    invoke-static {v1}, Lcom/zte/engineer/ProduceInfoListView;->access$100(Lcom/zte/engineer/ProduceInfoListView;)[I

    move-result-object v1

    aget v1, v1, p3

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v1, "com.zte.smssecurity.action.SMS_SECURITY_SETTING"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/ProduceInfoListView;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "flag"

    iget-object v2, p0, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/ProduceInfoListView;

    invoke-static {v2}, Lcom/zte/engineer/ProduceInfoListView;->access$200(Lcom/zte/engineer/ProduceInfoListView;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/zte/engineer/ProduceInfoListView$MyOnItemClickListener;->this$0:Lcom/zte/engineer/ProduceInfoListView;

    const-class v2, Lcom/zte/engineer/TestFlag;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f060075
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
