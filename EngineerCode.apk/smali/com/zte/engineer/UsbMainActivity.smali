.class public Lcom/zte/engineer/UsbMainActivity;
.super Lcom/zte/engineer/ZteActivity;
.source "UsbMainActivity.java"


# static fields
.field private static final ACTION_NAME:Ljava/lang/String; = "android.intent.action.BATTERY_CHANGED"


# instance fields
.field BatteryStatus:Ljava/lang/String;

.field BatteryStatus2:Ljava/lang/String;

.field BatteryT:Ljava/lang/String;

.field BatteryTemp:Ljava/lang/String;

.field BatteryV:Ljava/lang/String;

.field private mBatInfoReceiver:Landroid/content/BroadcastReceiver;

.field tv_BatteryStatus:Landroid/widget/TextView;

.field tv_BatteryStatus2:Landroid/widget/TextView;

.field tv_BatteryT:Landroid/widget/TextView;

.field tv_BatteryTemp:Landroid/widget/TextView;

.field tv_BatteryV:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    new-instance v0, Lcom/zte/engineer/UsbMainActivity$1;

    invoke-direct {v0, p0}, Lcom/zte/engineer/UsbMainActivity$1;-><init>(Lcom/zte/engineer/UsbMainActivity;)V

    iput-object v0, p0, Lcom/zte/engineer/UsbMainActivity;->mBatInfoReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zte/engineer/UsbMainActivity;->mBatInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryStatus:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryStatus2:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->BatteryStatus2:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryV:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->BatteryV:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryT:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->BatteryT:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryTemp:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->BatteryTemp:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    goto :goto_0

    :pswitch_2
    const/16 v2, 0x14

    invoke-virtual {p0, v2}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08007d
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/16 v5, 0x8

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030016

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setContentView(I)V

    const-string v3, "\u70b9\u51fb\u6309\u94ae\u83b7\u53d6\u5bf9\u5e94\u65f6\u523b\u7684\u72b6\u6001"

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    const v3, 0x7f080078

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryStatus:Landroid/widget/TextView;

    const v3, 0x7f080079

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryStatus2:Landroid/widget/TextView;

    const v3, 0x7f08007a

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryV:Landroid/widget/TextView;

    const v3, 0x7f08007b

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryT:Landroid/widget/TextView;

    const v3, 0x7f08007c

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryTemp:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryT:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/zte/engineer/UsbMainActivity;->tv_BatteryV:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f08007d

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f08007e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v3, 0x7f08007f

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
