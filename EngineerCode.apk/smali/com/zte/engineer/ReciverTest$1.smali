.class Lcom/zte/engineer/ReciverTest$1;
.super Landroid/os/Handler;
.source "ReciverTest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/ReciverTest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zte/engineer/ReciverTest;


# direct methods
.method constructor <init>(Lcom/zte/engineer/ReciverTest;)V
    .locals 0

    iput-object p1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$000(Lcom/zte/engineer/ReciverTest;)Landroid/media/AudioManager;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setMode(I)V

    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$100(Lcom/zte/engineer/ReciverTest;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$100(Lcom/zte/engineer/ReciverTest;)Landroid/media/MediaPlayer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    :try_start_0
    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$100(Lcom/zte/engineer/ReciverTest;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$100(Lcom/zte/engineer/ReciverTest;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$100(Lcom/zte/engineer/ReciverTest;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$100(Lcom/zte/engineer/ReciverTest;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    iget-object v1, p0, Lcom/zte/engineer/ReciverTest$1;->this$0:Lcom/zte/engineer/ReciverTest;

    invoke-static {v1}, Lcom/zte/engineer/ReciverTest;->access$000(Lcom/zte/engineer/ReciverTest;)Landroid/media/AudioManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->setMode(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
