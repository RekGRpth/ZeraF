.class public Lcom/zte/engineer/KeyTest;
.super Lcom/zte/engineer/ZteActivity;
.source "KeyTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zte/engineer/KeyTest$keyTestManager;,
        Lcom/zte/engineer/KeyTest$keyAndTextId;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "KeyTest"


# instance fields
.field keyAndTextIdArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/zte/engineer/KeyTest$keyAndTextId;",
            ">;"
        }
    .end annotation
.end field

.field manager:Lcom/zte/engineer/KeyTest$keyTestManager;

.field private querty:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/zte/engineer/ZteActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zte/engineer/KeyTest;->querty:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    return-void
.end method

.method private addItem(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Lcom/zte/engineer/KeyTest$keyAndTextId;

    invoke-direct {v0, p0, p1, p2}, Lcom/zte/engineer/KeyTest$keyAndTextId;-><init>(Lcom/zte/engineer/KeyTest;II)V

    iget-object v1, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private getTextId(I)I
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/zte/engineer/KeyTest$keyAndTextId;

    iget v2, v2, Lcom/zte/engineer/KeyTest$keyAndTextId;->keyCode:I

    if-ne v2, p1, :cond_1

    const-string v2, "KeyTest"

    const-string v3, "find keyCode in Array"

    invoke-static {v2, v3}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/zte/engineer/KeyTest$keyAndTextId;

    iget v2, v2, Lcom/zte/engineer/KeyTest$keyAndTextId;->textId:I

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_0
.end method

.method private init(Z)V
    .locals 3
    .param p1    # Z

    const v2, 0x7f080020

    const v1, 0x7f08001f

    const v0, 0x7f030007

    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/zte/engineer/KeyTest;->initKeyAndText(Z)V

    new-instance v0, Lcom/zte/engineer/KeyTest$keyTestManager;

    iget-object v1, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/zte/engineer/KeyTest$keyTestManager;-><init>(Lcom/zte/engineer/KeyTest;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/zte/engineer/KeyTest;->manager:Lcom/zte/engineer/KeyTest$keyTestManager;

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private initKeyAndText(Z)V
    .locals 0
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/zte/engineer/KeyTest;->initKeyAndTextIdArrayQwerty()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/zte/engineer/KeyTest;->initKeyAndTextIdArray()V

    goto :goto_0
.end method

.method private initKeyAndTextIdArray()V
    .locals 2

    iget-object v0, p0, Lcom/zte/engineer/KeyTest;->keyAndTextIdArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/16 v0, 0x18

    const v1, 0x7f08001a

    invoke-direct {p0, v0, v1}, Lcom/zte/engineer/KeyTest;->addItem(II)V

    const/16 v0, 0x19

    const v1, 0x7f08001b

    invoke-direct {p0, v0, v1}, Lcom/zte/engineer/KeyTest;->addItem(II)V

    const/16 v0, 0x52

    const v1, 0x7f08001c

    invoke-direct {p0, v0, v1}, Lcom/zte/engineer/KeyTest;->addItem(II)V

    const/4 v0, 0x3

    const v1, 0x7f08001d

    invoke-direct {p0, v0, v1}, Lcom/zte/engineer/KeyTest;->addItem(II)V

    const/4 v0, 0x4

    const v1, 0x7f08001e

    invoke-direct {p0, v0, v1}, Lcom/zte/engineer/KeyTest;->addItem(II)V

    return-void
.end method

.method private initKeyAndTextIdArrayQwerty()V
    .locals 0

    return-void
.end method

.method private performanceKeyEvent(Landroid/view/KeyEvent;)V
    .locals 4
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/zte/engineer/KeyTest;->getTextId(I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v2, p0, Lcom/zte/engineer/KeyTest;->manager:Lcom/zte/engineer/KeyTest$keyTestManager;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/zte/engineer/KeyTest$keyTestManager;->remove(I)V

    iget-object v2, p0, Lcom/zte/engineer/KeyTest;->manager:Lcom/zte/engineer/KeyTest$keyTestManager;

    invoke-virtual {v2}, Lcom/zte/engineer/KeyTest$keyTestManager;->getRemainnings()I

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/KeyEvent;

    const/4 v3, 0x1

    const-string v0, "KeyTest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keycode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zte/engineer/Util;->log(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "HOME_KEY_TEST"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0, p1}, Lcom/zte/engineer/KeyTest;->performanceKeyEvent(Landroid/view/KeyEvent;)V

    return v3
.end method

.method public onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onAttachedToWindow()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/16 v1, 0xa

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/zte/engineer/ZteActivity;->finishSelf(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08001f
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/zte/engineer/ZteActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7d4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, -0x80000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "HOME_KEY_TEST"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-boolean v0, p0, Lcom/zte/engineer/KeyTest;->querty:Z

    invoke-direct {p0, v0}, Lcom/zte/engineer/KeyTest;->init(Z)V

    return-void
.end method

.method protected onPause()V
    .locals 3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "HOME_KEY_TEST"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "HOME_KEY_TEST"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
