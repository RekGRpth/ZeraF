.class public final Lcom/zte/engineer/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zte/engineer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final BatteryStatus:I = 0x7f080078

.field public static final BatteryStatus2:I = 0x7f080079

.field public static final BatteryT:I = 0x7f08007b

.field public static final BatteryTemp:I = 0x7f08007c

.field public static final BatteryV:I = 0x7f08007a

.field public static final China_Mobile:I = 0x7f08006f

.field public static final China_Telecom:I = 0x7f080073

.field public static final China_Unicom:I = 0x7f080071

.field public static final MainList:I = 0x7f080027

.field public static final autotest:I = 0x7f080029

.field public static final bt_China_Mobile:I = 0x7f080070

.field public static final bt_China_Telecom:I = 0x7f080074

.field public static final bt_China_Unicom:I = 0x7f080072

.field public static final bt_false:I = 0x7f08000b

.field public static final bt_no:I = 0x7f080077

.field public static final bt_others:I = 0x7f080076

.field public static final bt_pass:I = 0x7f08000a

.field public static final button1:I = 0x7f08007d

.field public static final camera_false:I = 0x7f080015

.field public static final camera_pass:I = 0x7f080014

.field public static final checkbox:I = 0x7f080017

.field public static final circleView:I = 0x7f080000

.field public static final completed:I = 0x7f080026

.field public static final flag_CMMB_Chip:I = 0x7f080065

.field public static final flag_CMMB_Coupling:I = 0x7f080066

.field public static final flag_GPS_Chip:I = 0x7f080067

.field public static final flag_GSM_Calibration:I = 0x7f080062

.field public static final flag_GSM_FinalTest:I = 0x7f080064

.field public static final flag_GSP_Coupling:I = 0x7f080068

.field public static final flag_SENSOR_ACCELEROMETER_Chip:I = 0x7f08006b

.field public static final flag_SENSOR_MAGNETIC_FIELD_Chip:I = 0x7f08006c

.field public static final flag_TD_Calibration:I = 0x7f080061

.field public static final flag_TD_Coupling:I = 0x7f08006d

.field public static final flag_TD_FinalTest:I = 0x7f080063

.field public static final flag_WIFI_Chip:I = 0x7f080069

.field public static final flag_WIFI_Coupling:I = 0x7f08006a

.field public static final flag_test_ok:I = 0x7f08006e

.field public static final fm_false:I = 0x7f080019

.field public static final fm_pass:I = 0x7f080018

.field public static final fullscreen_linelayout:I = 0x7f080021

.field public static final fullscreen_linelayout_button:I = 0x7f080022

.field public static final g_sensor_x:I = 0x7f080048

.field public static final g_sensor_y:I = 0x7f080049

.field public static final g_sensor_z:I = 0x7f08004a

.field public static final gridview:I = 0x7f08002a

.field public static final gyroscope_x:I = 0x7f08004b

.field public static final gyroscope_y:I = 0x7f08004c

.field public static final gyroscope_z:I = 0x7f08004d

.field public static final handtest:I = 0x7f080028

.field public static final health:I = 0x7f080005

.field public static final items:I = 0x7f080044

.field public static final lcd_test_false_button:I = 0x7f080024

.field public static final lcd_test_pass_button:I = 0x7f080023

.field public static final level:I = 0x7f080003

.field public static final light_lux:I = 0x7f080051

.field public static final list_text:I = 0x7f080025

.field public static final magnetic_x:I = 0x7f08004e

.field public static final magnetic_y:I = 0x7f08004f

.field public static final magnetic_z:I = 0x7f080050

.field public static final mccmnc:I = 0x7f08002c

.field public static final multibutton_button:I = 0x7f080013

.field public static final multibutton_button_1:I = 0x7f08000f

.field public static final multibutton_button_2:I = 0x7f080010

.field public static final multibutton_button_3:I = 0x7f080011

.field public static final multibutton_button_4:I = 0x7f080012

.field public static final multibutton_textview_summy:I = 0x7f08000e

.field public static final multibutton_textview_title:I = 0x7f08000d

.field public static final name:I = 0x7f080045

.field public static final netlock:I = 0x7f08002b

.field public static final no:I = 0x7f08007f

.field public static final normal_false_button:I = 0x7f08003d

.field public static final normal_pass_button:I = 0x7f08003c

.field public static final normal_textlayout:I = 0x7f08002d

.field public static final normal_textview:I = 0x7f08002e

.field public static final normal_textview1:I = 0x7f08002f

.field public static final normal_textview10:I = 0x7f080038

.field public static final normal_textview11:I = 0x7f080039

.field public static final normal_textview12:I = 0x7f08003a

.field public static final normal_textview13:I = 0x7f08003b

.field public static final normal_textview2:I = 0x7f080030

.field public static final normal_textview3:I = 0x7f080031

.field public static final normal_textview4:I = 0x7f080032

.field public static final normal_textview5:I = 0x7f080033

.field public static final normal_textview6:I = 0x7f080034

.field public static final normal_textview7:I = 0x7f080035

.field public static final normal_textview8:I = 0x7f080036

.field public static final normal_textview9:I = 0x7f080037

.field public static final others:I = 0x7f080075

.field public static final pass:I = 0x7f08007e

.field public static final produce_text:I = 0x7f08003f

.field public static final produce_text_title:I = 0x7f08003e

.field public static final proximity:I = 0x7f080052

.field public static final rectangleView:I = 0x7f080001

.field public static final resultlist:I = 0x7f080043

.field public static final s_key_back:I = 0x7f08001e

.field public static final s_key_home:I = 0x7f08001d

.field public static final s_key_menu:I = 0x7f08001c

.field public static final s_key_test_false:I = 0x7f080020

.field public static final s_key_test_pass:I = 0x7f08001f

.field public static final s_key_volume_down:I = 0x7f08001b

.field public static final s_key_volume_up:I = 0x7f08001a

.field public static final scale:I = 0x7f080004

.field public static final sensor_false:I = 0x7f080054

.field public static final sensor_linear:I = 0x7f080047

.field public static final sensor_pass:I = 0x7f080053

.field public static final singlebutton_false_button:I = 0x7f08005f

.field public static final singlebutton_pass_button:I = 0x7f08005e

.field public static final singlebutton_textview:I = 0x7f080055

.field public static final singlebutton_textview_1:I = 0x7f080056

.field public static final singlebutton_textview_2:I = 0x7f080057

.field public static final singlebutton_textview_3:I = 0x7f080058

.field public static final singlebutton_textview_4:I = 0x7f080059

.field public static final singlebutton_textview_5:I = 0x7f08005a

.field public static final singlebutton_textview_6:I = 0x7f08005b

.field public static final singlebutton_textview_7:I = 0x7f08005c

.field public static final singlebutton_textview_8:I = 0x7f08005d

.field public static final singlebutton_view:I = 0x7f08000c

.field public static final state:I = 0x7f080046

.field public static final status:I = 0x7f080002

.field public static final technology:I = 0x7f080008

.field public static final temperature:I = 0x7f080007

.field public static final test_flag_title:I = 0x7f080060

.field public static final text:I = 0x7f080016

.field public static final totel:I = 0x7f080040

.field public static final totelfail:I = 0x7f080042

.field public static final totelpass:I = 0x7f080041

.field public static final uptime:I = 0x7f080009

.field public static final voltage:I = 0x7f080006

.field public static final wifi_false:I = 0x7f080081

.field public static final wifi_pass:I = 0x7f080080


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
