.class public Lcom/android/stk/EventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "EventReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/stk/EventReceiver;Landroid/content/Context;I)V
    .locals 0
    .param p0    # Lcom/android/stk/EventReceiver;
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/android/stk/EventReceiver;->sendDownloadEvent(Landroid/content/Context;I)V

    return-void
.end method

.method private sendDownloadEvent(Landroid/content/Context;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/4 v2, 0x2

    new-array v1, v2, [I

    const/4 v2, 0x0

    const/4 v3, 0x6

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/android/stk/StkAppService;->STK_GEMINI_BROADCAST_ALL:I

    aput v3, v1, v2

    const-string v2, "op"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string v2, "downLoad event id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/android/stk/StkAppService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    move-object v1, p1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/android/stk/EventReceiver$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/android/stk/EventReceiver$1;-><init>(Lcom/android/stk/EventReceiver;Ljava/lang/String;Landroid/content/Context;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void
.end method
