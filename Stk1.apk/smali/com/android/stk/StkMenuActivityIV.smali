.class public Lcom/android/stk/StkMenuActivityIV;
.super Landroid/app/ListActivity;
.source "StkMenuActivityIV.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Stk4-MA "


# instance fields
.field private mMenuInstance:Lcom/android/stk/StkMenuInstance;

.field private final mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

.field private mProgressView:Landroid/widget/ProgressBar;

.field private final mSIMStateChangeFilter:Landroid/content/IntentFilter;

.field private final mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mTitleIconView:Landroid/widget/ImageView;

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    new-instance v0, Lcom/android/stk/StkMenuInstance;

    invoke-direct {v0}, Lcom/android/stk/StkMenuInstance;-><init>()V

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iput-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mTitleTextView:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mTitleIconView:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mProgressView:Landroid/widget/ProgressBar;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mSIMStateChangeFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/android/stk/StkMenuActivityIV$1;

    invoke-direct {v0, p0}, Lcom/android/stk/StkMenuActivityIV$1;-><init>(Lcom/android/stk/StkMenuActivityIV;)V

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/stk/StkMenuActivityIV$2;

    invoke-direct {v0, p0}, Lcom/android/stk/StkMenuActivityIV$2;-><init>(Lcom/android/stk/StkMenuActivityIV;)V

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/stk/StkMenuActivityIV;)Lcom/android/stk/StkMenuInstance;
    .locals 1
    .param p0    # Lcom/android/stk/StkMenuActivityIV;

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    return-object v0
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_0
    return v2

    :pswitch_0
    iget-object v3, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {v3}, Lcom/android/stk/StkMenuInstance;->cancelTimeOut()V

    iget-object v3, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/android/stk/StkMenuInstance;->mAcceptUsersInput:Z

    iget-object v3, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget v4, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v3, v4}, Lcom/android/stk/StkMenuInstance;->getSelectedItem(I)Lcom/android/internal/telephony/cat/Item;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    const/16 v4, 0xb

    iget v5, v1, Lcom/android/internal/telephony/cat/Item;->id:I

    invoke-virtual {v3, v4, v5, v2}, Lcom/android/stk/StkMenuInstance;->sendResponse(IIZ)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v0, "Stk4-MA "

    const-string v1, "onCreate+"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Landroid/app/Activity;->requestWindowFeature(I)Z

    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f070016

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mTitleTextView:Landroid/widget/TextView;

    const v0, 0x7f070015

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mTitleIconView:Landroid/widget/ImageView;

    const v0, 0x7f070018

    invoke-virtual {p0, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Landroid/app/ListActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mOnCreateContextMenuListener:Landroid/view/View$OnCreateContextMenuListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/stk/StkMenuInstance;->handleOnCreate(Landroid/content/Context;Landroid/content/Intent;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Stk4-MA "

    const-string v1, "finish!"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mSIMStateChangeFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v0, "Stk4-MA "

    const-string v1, "onCreate-"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1    # Landroid/view/Menu;

    const v4, 0x7f060007

    const/4 v3, 0x3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    const v0, 0x7f060006

    invoke-interface {p1, v1, v2, v2, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x2

    invoke-interface {p1, v1, v3, v0, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    const/4 v0, 0x4

    invoke-interface {p1, v1, v0, v3, v4}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    return v2
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v0, "Stk4-MA "

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {v1, p1, p2}, Lcom/android/stk/StkMenuInstance;->handleKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p3, v1}, Lcom/android/stk/StkMenuInstance;->handleListItemClick(ILandroid/widget/ProgressBar;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    const-string v0, "Stk4-MA "

    const-string v1, "onNewIntent"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/stk/StkMenuInstance;->handleNewIntent(Landroid/content/Intent;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Stk4-MA "

    const-string v1, "finish!"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1    # Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget-object v2, p0, Lcom/android/stk/StkMenuActivityIV;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v1, p1, v2}, Lcom/android/stk/StkMenuInstance;->handleOptionItemSelected(Landroid/view/MenuItem;Landroid/widget/ProgressBar;)Z

    move-result v0

    const-string v1, "Stk4-MA "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onOptionsItemSelected, result: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "Stk4-MA "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause, sim id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget v2, v2, Lcom/android/stk/StkMenuInstance;->mSimId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {v0}, Lcom/android/stk/StkMenuInstance;->handlePause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {v0, p1}, Lcom/android/stk/StkMenuInstance;->handlePrepareOptionMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    const-string v1, "STATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/android/stk/StkMenuInstance;->mState:I

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    const-string v0, "MENU"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/cat/Menu;

    iput-object v0, v1, Lcom/android/stk/StkMenuInstance;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    return-void
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v1, "Stk4-MA "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onResume, sim id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget v3, v3, Lcom/android/stk/StkMenuInstance;->mSimId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget-object v2, p0, Lcom/android/stk/StkMenuActivityIV;->mTitleIconView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/android/stk/StkMenuActivityIV;->mTitleTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/android/stk/StkMenuActivityIV;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v2, v3, p0, v4}, Lcom/android/stk/StkMenuInstance;->handleResume(Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/app/ListActivity;Landroid/widget/ProgressBar;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060042

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/stk/StkMenuInstance;->showTextToast(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f060040

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/stk/StkMenuInstance;->showTextToast(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "STATE"

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget v1, v1, Lcom/android/stk/StkMenuInstance;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "MENU"

    iget-object v1, p0, Lcom/android/stk/StkMenuActivityIV;->mMenuInstance:Lcom/android/stk/StkMenuInstance;

    iget-object v1, v1, Lcom/android/stk/StkMenuInstance;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
