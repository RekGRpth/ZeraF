.class public Lcom/android/stk/StkDialogActivityIII;
.super Landroid/app/Activity;
.source "StkDialogActivityIII.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Stk3-DA"

.field private static final TEXT:Ljava/lang/String; = "text"


# instance fields
.field private mDialogInstance:Lcom/android/stk/StkDialogInstance;

.field private final mSIMStateChangeFilter:Landroid/content/IntentFilter;

.field private final mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/stk/StkDialogInstance;

    invoke-direct {v0}, Lcom/android/stk/StkDialogInstance;-><init>()V

    iput-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mSIMStateChangeFilter:Landroid/content/IntentFilter;

    new-instance v0, Lcom/android/stk/StkDialogActivityIII$1;

    invoke-direct {v0, p0}, Lcom/android/stk/StkDialogActivityIII$1;-><init>(Lcom/android/stk/StkDialogActivityIII;)V

    iput-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/stk/StkDialogActivityIII;)Lcom/android/stk/StkDialogInstance;
    .locals 1
    .param p0    # Lcom/android/stk/StkDialogActivityIII;

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    return-object v0
.end method

.method private init()V
    .locals 7

    const/4 v6, 0x3

    const-string v4, "Stk3-DA"

    const-string v5, "init"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x7f070010

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v4, 0x7f070006

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v4, 0x7f070012

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-boolean v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    if-nez v4, :cond_3

    :cond_0
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    const/4 v5, 0x6

    if-ge v4, v5, :cond_2

    :cond_1
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    const/16 v4, 0xaa

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMinWidth(I)V

    :cond_2
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    if-nez v4, :cond_4

    const v4, 0x1080523

    invoke-virtual {v3, v6, v4}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    :goto_0
    return-void

    :cond_4
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v5, v5, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v5, v5, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v6, v4}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {v0, p1}, Lcom/android/stk/StkDialogInstance;->handleOnClick(Landroid/view/View;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const-string v4, "Stk3-DA"

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Stk3-DA"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate - mbSendResp["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-boolean v6, v6, Lcom/android/stk/StkDialogInstance;->mbSendResp:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iput-object p0, v4, Lcom/android/stk/StkDialogInstance;->parent:Landroid/app/Activity;

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/stk/StkDialogInstance;->initFromIntent(Landroid/content/Intent;)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    if-nez v4, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v7}, Landroid/app/Activity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x7f030005

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setContentView(I)V

    const v4, 0x7f070010

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v4, 0x7f070006

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v4, 0x7f070012

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    invoke-virtual {p0, v4}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-boolean v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->iconSelfExplanatory:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    if-nez v4, :cond_4

    :cond_1
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    const/4 v5, 0x6

    if-ge v4, v5, :cond_3

    :cond_2
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    const/16 v4, 0xaa

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMinWidth(I)V

    :cond_3
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v4, v4, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v4, v4, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    if-nez v4, :cond_5

    const-string v4, "Stk3-DA"

    const-string v5, "onCreate icon is null"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    const v4, 0x1080523

    invoke-virtual {v3, v7, v4}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    :goto_1
    const/high16 v4, 0x8000000

    invoke-virtual {v3, v4}, Landroid/view/Window;->clearFlags(I)V

    iget-object v4, p0, Lcom/android/stk/StkDialogActivityIII;->mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

    iget-object v5, p0, Lcom/android/stk/StkDialogActivityIII;->mSIMStateChangeFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v4, v5}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_0

    :cond_5
    const-string v4, "Stk3-DA"

    const-string v5, "onCreate icon is not null"

    invoke-static {v4, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v5, v5, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v5, v5, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    invoke-direct {v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v7, v4}, Landroid/view/Window;->setFeatureDrawable(ILandroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {v0}, Lcom/android/stk/StkDialogInstance;->handleOnDestroy()V

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mSIMStateChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {v0, p1, p2}, Lcom/android/stk/StkDialogInstance;->handleOnKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {v0, p1}, Lcom/android/stk/StkDialogInstance;->handleOnNewIntent(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/android/stk/StkDialogActivityIII;->init()V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {v0}, Lcom/android/stk/StkDialogInstance;->handleOnPause()V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    const-string v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/cat/TextMessage;

    iput-object v0, v1, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    const-string v0, "Stk3-DA"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRestoreInstanceState - ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v2, v2, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    invoke-virtual {v0}, Lcom/android/stk/StkDialogInstance;->handleOnResume()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "Stk3-DA"

    const-string v1, "onSaveInstanceState"

    invoke-static {v0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "text"

    iget-object v1, p0, Lcom/android/stk/StkDialogActivityIII;->mDialogInstance:Lcom/android/stk/StkDialogInstance;

    iget-object v1, v1, Lcom/android/stk/StkDialogInstance;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
