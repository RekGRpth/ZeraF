.class public Lcom/google/android/marvin/utils/HighlightBoundsView;
.super Landroid/view/View;
.source "HighlightBoundsView.java"


# instance fields
.field private final SCREEN_LOCATION:[I

.field private mHighlightColor:I

.field private final mMatrix:Landroid/graphics/Matrix;

.field private final mNodes:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private final mPaint:Landroid/graphics/Paint;

.field private final mTemp:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mTemp:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mNodes:Ljava/util/HashSet;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/high16 v0, -0x10000

    iput v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mHighlightColor:I

    return-void
.end method


# virtual methods
.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    neg-int v3, v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mHighlightColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mNodes:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mTemp:Landroid/graphics/Rect;

    invoke-virtual {v1, v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mTemp:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->SCREEN_LOCATION:[I

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/utils/HighlightBoundsView;->getLocationOnScreen([I)V

    return-void
.end method

.method public setHighlightColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/marvin/utils/HighlightBoundsView;->mHighlightColor:I

    return-void
.end method
