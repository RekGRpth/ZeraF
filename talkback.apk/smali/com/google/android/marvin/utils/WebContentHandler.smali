.class public Lcom/google/android/marvin/utils/WebContentHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "WebContentHandler.java"


# instance fields
.field private final mAriaRoleToDesc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mInputTypeToDesc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOutputBuilder:Ljava/lang/StringBuilder;

.field private mPostorderTextStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTagToDesc:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mInputTypeToDesc:Ljava/util/Map;

    iput-object p2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mAriaRoleToDesc:Ljava/util/Map;

    iput-object p3, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mTagToDesc:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 1
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2, p3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/marvin/utils/WebContentHandler;->fixWhiteSpace()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public fixWhiteSpace()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method public getOutput()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public startDocument()V
    .locals 1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 14
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;

    invoke-virtual {p0}, Lcom/google/android/marvin/utils/WebContentHandler;->fixWhiteSpace()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v12, "aria-label"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v12, "alt"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v12, "title"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v2, :cond_3

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    const-string v12, "role"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mAriaRoleToDesc:Ljava/util/Map;

    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v12, "type"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mTagToDesc:Ljava/util/Map;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    if-eqz v6, :cond_5

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v12, v6}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    const-string v12, "value"

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_2

    move-object/from16 v3, p3

    const-string v12, "input"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    if-eqz v9, :cond_1

    move-object v3, v9

    :cond_1
    const-string v12, "checkbox"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "radio"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    invoke-virtual {p0}, Lcom/google/android/marvin/utils/WebContentHandler;->fixWhiteSpace()V

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-void

    :cond_3
    if-eqz v1, :cond_4

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    if-eqz v8, :cond_0

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mOutputBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_5
    const-string v12, "input"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    if-eqz v9, :cond_7

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mInputTypeToDesc:Ljava/util/Map;

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    if-eqz v10, :cond_6

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v12, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_6
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_7
    if-eqz v7, :cond_8

    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    invoke-virtual {v12, v7}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_8
    iget-object v12, p0, Lcom/google/android/marvin/utils/WebContentHandler;->mPostorderTextStack:Ljava/util/Stack;

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
