.class Lcom/google/android/marvin/talkback/CallStateMonitor;
.super Landroid/content/BroadcastReceiver;
.source "CallStateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;
    }
.end annotation


# static fields
.field private static final STATE_CHANGED_FILTER:Landroid/content/IntentFilter;


# instance fields
.field private final mHandler:Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/marvin/talkback/CallStateMonitor;->STATE_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    new-instance v0, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;-><init>(Lcom/google/android/marvin/talkback/CallStateMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mHandler:Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/CallStateMonitor;Landroid/content/Intent;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/CallStateMonitor;
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/CallStateMonitor;->internalOnReceive(Landroid/content/Intent;)V

    return-void
.end method

.method private internalOnReceive(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const-string v1, "state"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/marvin/talkback/TalkBackService;->isServiceActive()Z

    move-result v1

    if-nez v1, :cond_1

    const-class v1, Lcom/google/android/marvin/talkback/CallStateMonitor;

    const/4 v2, 0x5

    const-string v3, "Service not initialized during broadcast."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentCallState()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/content/IntentFilter;
    .locals 1

    sget-object v0, Lcom/google/android/marvin/talkback/CallStateMonitor;->STATE_CHANGED_FILTER:Landroid/content/IntentFilter;

    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/CallStateMonitor;->mHandler:Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;

    invoke-virtual {v0, p2}, Lcom/google/android/marvin/talkback/CallStateMonitor$CallStateHandler;->onReceive(Landroid/content/Intent;)V

    return-void
.end method
