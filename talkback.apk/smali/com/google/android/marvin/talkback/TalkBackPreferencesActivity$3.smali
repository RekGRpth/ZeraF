.class Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;
.super Ljava/lang/Object;
.source "TalkBackPreferencesActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, p2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->confirmDisableExploreByTouch()V
    invoke-static {v1}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$300(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;)V

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity$3;->this$0:Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->setTouchExplorationRequested(Z)Z
    invoke-static {v1, v0}, Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;->access$000(Lcom/google/android/marvin/talkback/TalkBackPreferencesActivity;Z)Z

    move-result v1

    goto :goto_0
.end method
