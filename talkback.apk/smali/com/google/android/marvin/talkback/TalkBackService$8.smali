.class Lcom/google/android/marvin/talkback/TalkBackService$8;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$8;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x3

    const-string v1, "A shared preference changed: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {p0, v0, v1, v2}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$8;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # invokes: Lcom/google/android/marvin/talkback/TalkBackService;->reloadPreferences()V
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$800(Lcom/google/android/marvin/talkback/TalkBackService;)V

    return-void
.end method
