.class Lcom/google/android/marvin/talkback/TalkBackService$7;
.super Ljava/lang/Object;
.source "TalkBackService.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/CursorController$CursorControllerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/TalkBackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/TalkBackService$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionPerformed(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$700(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/TalkBackService$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mProcessorFollowFocus:Lcom/google/android/marvin/talkback/ProcessorFollowFocus;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->access$700(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/ProcessorFollowFocus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/marvin/talkback/ProcessorFollowFocus;->onActionPerformed(I)V

    :cond_0
    return-void
.end method

.method public onGranularityChanged(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;Z)V
    .locals 5
    .param p1    # Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;
    .param p2    # Z

    const/4 v4, 0x0

    if-eqz p2, :cond_0

    iget v1, p1, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->resId:I

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/TalkBackService$7;->this$0:Lcom/google/android/marvin/talkback/TalkBackService;

    # getter for: Lcom/google/android/marvin/talkback/TalkBackService;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->access$500(Lcom/google/android/marvin/talkback/TalkBackService;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v4, v4, v3}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method
