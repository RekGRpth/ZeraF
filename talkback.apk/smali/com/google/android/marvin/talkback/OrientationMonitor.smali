.class public Lcom/google/android/marvin/talkback/OrientationMonitor;
.super Ljava/lang/Object;
.source "OrientationMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mHandler:Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;

.field private mIsScreenOn:Z

.field private mLastOrientation:I

.field private final mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;

    invoke-direct {v0, p0}, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;-><init>(Lcom/google/android/marvin/talkback/OrientationMonitor;)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mHandler:Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mIsScreenOn:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/OrientationMonitor;I)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/OrientationMonitor;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/OrientationMonitor;->announceCurrentRotation(I)V

    return-void
.end method

.method private announceCurrentRotation(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x2

    iget-boolean v1, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mIsScreenOn:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const v0, 0x7f0a00ab

    :goto_1
    iget-object v1, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v4, v4, v3}, Lcom/google/android/marvin/talkback/SpeechController;->speak(Ljava/lang/CharSequence;IILandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a00ac

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mLastOrientation:I

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mLastOrientation:I

    iget-object v1, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mHandler:Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;

    invoke-virtual {v1, v0}, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;->startAnnounceTimeout(I)V

    goto :goto_0
.end method

.method public setScreenState(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/marvin/talkback/OrientationMonitor;->mIsScreenOn:Z

    return-void
.end method
