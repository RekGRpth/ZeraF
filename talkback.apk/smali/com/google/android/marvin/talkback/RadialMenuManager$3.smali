.class Lcom/google/android/marvin/talkback/RadialMenuManager$3;
.super Ljava/lang/Object;
.source "RadialMenuManager.java"

# interfaces
.implements Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/RadialMenuManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/RadialMenuManager;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHide(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHintSpeechPending:Z
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$800(Lcom/google/android/marvin/talkback/RadialMenuManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$500(Lcom/google/android/marvin/talkback/RadialMenuManager;)Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/SpeechController;->interrupt()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # operator-- for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I
    invoke-static {v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$710(Lcom/google/android/marvin/talkback/RadialMenuManager;)I

    return-void
.end method

.method public onShow(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V
    .locals 5
    .param p1    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    check-cast p1, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuOverlay;->getMenu()Lcom/googlecode/eyesfree/widget/RadialMenu;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$100(Lcom/google/android/marvin/talkback/RadialMenuManager;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # getter for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mRadialMenuHint:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$000(Lcom/google/android/marvin/talkback/RadialMenuManager;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # invokes: Lcom/google/android/marvin/talkback/RadialMenuManager;->playScaleForMenu(Landroid/view/Menu;)V
    invoke-static {v1, v0}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$600(Lcom/google/android/marvin/talkback/RadialMenuManager;Landroid/view/Menu;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/RadialMenuManager$3;->this$0:Lcom/google/android/marvin/talkback/RadialMenuManager;

    # operator++ for: Lcom/google/android/marvin/talkback/RadialMenuManager;->mIsRadialMenuShowing:I
    invoke-static {v1}, Lcom/google/android/marvin/talkback/RadialMenuManager;->access$708(Lcom/google/android/marvin/talkback/RadialMenuManager;)I

    return-void
.end method
