.class public Lcom/google/android/marvin/talkback/FullScreenReadController;
.super Ljava/lang/Object;
.source "FullScreenReadController.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/TalkBackService$EventListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    }
.end annotation


# instance fields
.field private mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

.field private mCursorController:Lcom/google/android/marvin/talkback/CursorController;

.field private mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

.field private mNodeSpokenRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

.field private mService:Lcom/google/android/marvin/talkback/TalkBackService;

.field private mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 3
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSpeechController()Lcom/google/android/marvin/talkback/SpeechController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getCursorController()Lcom/google/android/marvin/talkback/CursorController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const-string v1, "power"

    invoke-virtual {p1, v1}, Lcom/google/android/marvin/talkback/TalkBackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const v1, 0x20000006

    const-string v2, "FullScreenReadController"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;)V

    new-instance v1, Lcom/google/android/marvin/talkback/FullScreenReadController$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/FullScreenReadController$1;-><init>(Lcom/google/android/marvin/talkback/FullScreenReadController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mNodeSpokenRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/FullScreenReadController;)Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/FullScreenReadController;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/FullScreenReadController;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/FullScreenReadController;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveForward()V

    return-void
.end method

.method private currentNodeHasWebContent()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0
.end method

.method private moveForward()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/CursorController;->next(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mFeedbackController:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const v1, 0x7f08002e

    const v2, 0x3fa66666

    const/high16 v3, 0x3f800000

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(IFF)V

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->currentNodeHasWebContent()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveIntoWebContent()V

    :cond_1
    return-void
.end method

.method private moveIntoWebContent()V
    .locals 4

    const/16 v3, 0x10

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    sget-object v2, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_BEGINNING:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-ne v1, v2, :cond_0

    const/4 v1, -0x1

    invoke-static {v0, v1, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationAtGranularityAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    :cond_0
    const/4 v1, 0x1

    invoke-static {v0, v1, v3}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->performNavigationAtGranularityAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;II)Z

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->ENTERING_WEB_CONTENT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    goto :goto_0
.end method


# virtual methods
.method public getReadingState()Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    return-object v0
.end method

.method public interrupt()V
    .locals 1

    sget-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void
.end method

.method public process(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->getReadingState()Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    move-result-object v0

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x2b8408

    invoke-static {p1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->eventMatchesAnyType(Landroid/view/accessibility/AccessibilityEvent;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    goto :goto_0
.end method

.method public setReadingState(Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;)V
    .locals 7
    .param p1    # Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "FullScreenReadController"

    const/4 v3, 0x2

    const-string v4, "Continuous reading switching to mode: %s"

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->name()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCurrentState:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mSpeechController:Lcom/google/android/marvin/talkback/SpeechController;

    sget-object v3, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    if-eq p1, v3, :cond_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mNodeSpokenRunnable:Lcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/marvin/talkback/SpeechController;->setShouldInjectAutoReadingCallbacks(ZLcom/google/android/marvin/talkback/SpeechController$UtteranceCompleteRunnable;)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public shutdown()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->interrupt()V

    return-void
.end method

.method public startReadingFromBeginning()Z
    .locals 7

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v6}, Lcom/google/android/marvin/talkback/TalkBackService;->getRootInActiveWindow()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    new-instance v0, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v0, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v5}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v6, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_5

    :cond_2
    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    new-array v6, v5, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    if-eqz v1, :cond_0

    sget-object v4, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_BEGINNING:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    sget-object v6, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    invoke-virtual {v4, v6}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;)Z

    iget-object v4, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_3
    iget-object v4, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v4}, Lcom/google/android/marvin/talkback/CursorController;->clearCursor()V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v4, v1}, Lcom/google/android/marvin/talkback/CursorController;->setCursor(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->currentNodeHasWebContent()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveIntoWebContent()V

    :cond_4
    move v4, v5

    goto :goto_0

    :cond_5
    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-static {v1, v5}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    goto :goto_1
.end method

.method public startReadingFromNextNode()Z
    .locals 3

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/CursorController;->getCursor()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_NEXT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/FullScreenReadController;->setReadingState(Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mCursorController:Lcom/google/android/marvin/talkback/CursorController;

    sget-object v2, Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;->DEFAULT:Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;

    invoke-virtual {v1, v2}, Lcom/google/android/marvin/talkback/CursorController;->setGranularity(Lcom/google/android/marvin/talkback/CursorGranularityManager$CursorGranularity;)Z

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/marvin/talkback/FullScreenReadController;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->currentNodeHasWebContent()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveIntoWebContent()V

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/marvin/talkback/FullScreenReadController;->moveForward()V

    goto :goto_1
.end method
