.class public Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
.super Ljava/lang/Object;
.source "EventSpeechRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$Pair;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;,
        Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;
    }
.end annotation


# static fields
.field private static final mResourceIdentifier:Ljava/util/regex/Pattern;

.field private static final sEventTypeNameToValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sQueueModeNameToQueueModeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sTempBuilder:Ljava/lang/StringBuilder;


# instance fields
.field private mCachedXmlString:Ljava/lang/String;

.field private final mContext:Lcom/google/android/marvin/talkback/TalkBackService;

.field private final mCustomEarcons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mCustomVibrations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mEarcons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

.field private final mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

.field private final mMetadata:Landroid/os/Bundle;

.field private mNode:Lorg/w3c/dom/Node;

.field private mPackageName:Ljava/lang/String;

.field private final mPropertyMatchers:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$PropertyMatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final mRuleIndex:I

.field private final mVibrationPatterns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const-string v0, "@([\\w\\.]+:)?\\w+/\\w+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mResourceIdentifier:Ljava/util/regex/Pattern;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sTempBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_CLICKED"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_LONG_CLICKED"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_SELECTED"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_FOCUSED"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_TEXT_CHANGED"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_WINDOW_STATE_CHANGED"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_NOTIFICATION_STATE_CHANGED"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_HOVER_ENTER"

    const/16 v2, 0x80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_HOVER_EXIT"

    const/16 v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_EXPLORATION_GESTURE_START"

    const/16 v2, 0x200

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_EXPLORATION_GESTURE_END"

    const/16 v2, 0x400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_WINDOW_CONTENT_CHANGED"

    const/16 v2, 0x800

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_SCROLLED"

    const/16 v2, 0x1000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_TEXT_SELECTION_CHANGED"

    const/16 v2, 0x2000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_ANNOUNCEMENT"

    const/16 v2, 0x4000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_ACCESSIBILITY_FOCUSED"

    const v2, 0x8000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED"

    const/high16 v2, 0x10000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY"

    const/high16 v2, 0x20000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_INTERACTION_START"

    const/high16 v2, 0x100000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_TOUCH_INTERACTION_END"

    const/high16 v2, 0x200000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_GESTURE_DETECTION_START"

    const/high16 v2, 0x40000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    const-string v1, "TYPE_GESTURE_DETECTION_END"

    const/high16 v2, 0x80000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    const-string v1, "INTERRUPT"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    const-string v1, "QUEUE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    const-string v1, "UNINTERRUPTIBLE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Node;I)V
    .locals 9
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2    # Lorg/w3c/dom/Node;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mEarcons:Ljava/util/List;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mVibrationPatterns:Ljava/util/List;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomEarcons:Ljava/util/List;

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomVibrations:Ljava/util/List;

    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;

    const-string v7, "undefined_package_name"

    iput-object v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    :goto_0
    if-ge v5, v2, :cond_4

    invoke-interface {v1, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "metadata"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->populateMetadata(Lorg/w3c/dom/Node;)V

    goto :goto_1

    :cond_2
    const-string v7, "filter"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createFilter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    move-result-object v3

    goto :goto_1

    :cond_3
    const-string v7, "formatter"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createFormatter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    move-result-object v4

    goto :goto_1

    :cond_4
    instance-of v7, v4, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    if-eqz v7, :cond_5

    move-object v7, v4

    check-cast v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    invoke-interface {v7, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;->initialize(Lcom/google/android/marvin/talkback/TalkBackService;)V

    :cond_5
    instance-of v7, v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    if-eqz v7, :cond_6

    move-object v7, v3

    check-cast v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;

    invoke-interface {v7, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$ContextBasedRule;->initialize(Lcom/google/android/marvin/talkback/TalkBackService;)V

    :cond_6
    iput-object v3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    iput-object v4, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    iput p3, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I

    return-void
.end method

.method static synthetic access$000(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/w3c/dom/Node;

    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0    # Lorg/w3c/dom/Node;

    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPropertyMatchers:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)Lcom/google/android/marvin/talkback/TalkBackService;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    return-object v0
.end method

.method static synthetic access$700(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lorg/w3c/dom/Node;

    invoke-static {p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;)I
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    iget v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I

    return v0
.end method

.method static synthetic access$900(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private createFilter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;
    .locals 7
    .param p1    # Lorg/w3c/dom/Node;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    :goto_0
    if-ge v3, v2, :cond_2

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "custom"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    invoke-direct {p0, v5, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createNewInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    :goto_1
    return-object v5

    :cond_2
    new-instance v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;

    iget-object v6, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {v5, p0, v6, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFilter;-><init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Landroid/content/Context;Lorg/w3c/dom/Node;)V

    goto :goto_1
.end method

.method private createFormatter(Lorg/w3c/dom/Node;)Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
    .locals 7
    .param p1    # Lorg/w3c/dom/Node;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    :goto_0
    if-ge v3, v2, :cond_2

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "custom"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    const-class v6, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    invoke-direct {p0, v5, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->createNewInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    :goto_1
    return-object v5

    :cond_2
    new-instance v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;

    invoke-direct {v5, p0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$DefaultFormatter;-><init>(Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;Lorg/w3c/dom/Node;)V

    goto :goto_1
.end method

.method private createNewInstance(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v2}, Lcom/google/android/marvin/talkback/TalkBackService;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    const-class v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const/4 v3, 0x6

    const-string v4, "Rule: #%d. Could not load class: \'%s\'."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mRuleIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    invoke-static {v2, v3, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static createSpeechRules(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Document;)Ljava/util/ArrayList;
    .locals 14
    .param p0    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p1    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Lorg/w3c/dom/Document;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v13, 0x1

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    return-object v6

    :cond_1
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/Element;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v4, 0x0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    :goto_0
    if-ge v4, v2, :cond_0

    invoke-interface {v1, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v7

    if-eq v7, v13, :cond_2

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    invoke-direct {v5, p0, v0, v4}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;-><init>(Lcom/google/android/marvin/talkback/TalkBackService;Lorg/w3c/dom/Node;I)V

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    const-class v7, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const/4 v8, 0x5

    const-string v9, "Failed loading speech rule: %s"

    new-array v10, v13, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v7, v8, v9, v10}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static getLocalizedTextContent(Landroid/content/Context;Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lorg/w3c/dom/Node;

    invoke-static {p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method private static final getPropertyType(Ljava/lang/String;)I
    .locals 1
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isBooleanProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isFloatProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isIntegerProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->isStringProperty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPropertyValue(Landroid/content/Context;Ljava/lang/String;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/Object;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;

    new-instance v0, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p3}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    const-string v1, "eventType"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "packageName"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v1, "className"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v1, "classNameStrict"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getClassName()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_3
    const-string v1, "text"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventAggregateText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_4
    const-string v1, "beforeText"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getBeforeText()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_5
    const-string v1, "contentDescription"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_6
    const-string v1, "contentDescriptionOrText"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p3}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    :cond_7
    const-string v1, "eventTime"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getEventTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_8
    const-string v1, "itemCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_9
    const-string v1, "currentItemIndex"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_a
    const-string v1, "fromIndex"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getFromIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_b
    const-string v1, "toIndex"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getToIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_c
    const-string v1, "scrollable"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->isScrollable()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    :cond_d
    const-string v1, "scrollX"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getScrollX()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_e
    const-string v1, "scrollY"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getScrollY()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_f
    const-string v1, "recordCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-static {p3}, Lvedroid/support/v4/view/accessibility/AccessibilityEventCompat;->getRecordCount(Landroid/view/accessibility/AccessibilityEvent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_10
    const-string v1, "checked"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    :cond_11
    const-string v1, "enabled"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    :cond_12
    const-string v1, "fullScreen"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isFullScreen()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    :cond_13
    const-string v1, "password"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->isPassword()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_0

    :cond_14
    const-string v1, "addedCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getAddedCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_15
    const-string v1, "removedCount"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getRemovedCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_16
    const-string v1, "versionCode"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_17
    const-string v1, "versionName"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual {p3}, Landroid/view/accessibility/AccessibilityEvent;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionName(Landroid/content/Context;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :cond_18
    const-string v1, "platformRelease"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    goto/16 :goto_0

    :cond_19
    const-string v1, "platformSdk"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_0

    :cond_1a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown property : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v3, -0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    sget-object v5, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mResourceIdentifier:Ljava/util/regex/Pattern;

    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5, v4, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_0

    const-class v4, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const/4 v5, 0x6

    const-string v6, "Failed to load resource: %s"

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object v0, v4

    goto :goto_1
.end method

.method private static getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 4
    .param p0    # Lorg/w3c/dom/Node;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sTempBuilder:Ljava/lang/StringBuilder;

    invoke-static {p0, v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContentRecursive(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    return-object v1
.end method

.method private static getTextContentRecursive(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V
    .locals 6
    .param p0    # Lorg/w3c/dom/Node;
    .param p1    # Ljava/lang/StringBuilder;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    :goto_0
    if-ge v3, v2, :cond_1

    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {v0, p1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContentRecursive(Lorg/w3c/dom/Node;Ljava/lang/StringBuilder;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 3
    .param p0    # Lorg/w3c/dom/Node;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-le v0, v2, :cond_0

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method private static isBooleanProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "checked"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "enabled"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fullScreen"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "scrollable"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "password"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isFloatProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "eventTime"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static isIntegerProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "eventType"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "itemCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "currentItemIndex"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "fromIndex"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "toIndex"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "scrollX"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "scrollY"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "recordCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "addedCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "removedCount"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "queuing"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "versionCode"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "platformSdk"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isStringProperty(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "packageName"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "className"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "classNameStrict"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "text"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "beforeText"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "contentDescription"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "contentDescriptionOrText"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "versionName"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "platformRelease"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Comparable",
            "<*>;"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v7, 0x5

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v3, "eventType"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v2, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sEventTypeNameToValueMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Comparable;

    :goto_0
    return-object v2

    :cond_0
    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getPropertyType(Ljava/lang/String;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown property: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const-string v4, "Property \'%s\' not float."

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v3, v7, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    :try_start_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v0

    const-class v3, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;

    const-string v4, "Property \'%s\' not int."

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p0, v5, v6

    invoke-static {v3, v7, v4, v5}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_3
    move-object v2, p1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private populateMetadata(Lorg/w3c/dom/Node;)V
    .locals 11
    .param p1    # Lorg/w3c/dom/Node;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_6

    invoke-interface {v3, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getUnqualifiedNodeName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getTextContent(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    const-string v9, "queuing"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    sget-object v9, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->sQueueModeNameToQueueModeMap:Ljava/util/HashMap;

    invoke-virtual {v9, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v9, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    :cond_1
    const-string v9, "earcon"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v9, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mEarcons:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const-string v9, "vibration"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v9, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v5

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mVibrationPatterns:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v9, "customEarcon"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomEarcons:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v10, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const-string v9, "customVibration"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomVibrations:Ljava/util/List;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-static {v10, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->getResourceIdentifierContent(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_5
    invoke-static {v7, v6}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->parsePropertyValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Comparable;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v9, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_6
    return-void
.end method


# virtual methods
.method public applyFilter(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v0, p1, v1}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;->accept(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;)Z

    move-result v0

    goto :goto_0
.end method

.method public applyFormatter(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/Utterance;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mContext:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v0, p1, v1, p2}, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;->format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mMetadata:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomEarcons:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p2}, Lcom/google/android/marvin/talkback/Utterance;->getCustomVibrations()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCustomVibrations:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getFilter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFilter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFilter;

    return-object v0
.end method

.method public getFormatter()Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mFormatter:Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCachedXmlString:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    invoke-static {v0}, Lcom/google/android/marvin/utils/NodeUtils;->asXmlString(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCachedXmlString:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mNode:Lorg/w3c/dom/Node;

    :cond_0
    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/EventSpeechRule;->mCachedXmlString:Ljava/lang/String;

    return-object v0
.end method
