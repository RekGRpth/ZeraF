.class public Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;
.super Ljava/lang/Object;
.source "ProgressBarFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# static fields
.field private static sRecentlyExplored:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getProgressPercent(Landroid/view/accessibility/AccessibilityEvent;)F
    .locals 6
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getItemCount()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getCurrentItemIndex()I

    move-result v2

    int-to-float v3, v2

    int-to-float v4, v0

    div-float v1, v3, v4

    const/high16 v3, 0x42c80000

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000

    invoke-static {v5, v1}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    mul-float/2addr v3, v4

    return v3
.end method

.method private shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 4
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v2, 0x0

    new-instance v0, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v0, p1}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityRecordCompat;->getSource()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isFocused()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isAccessibilityFocused()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1, v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static updateRecentlyExplored(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_0
    if-eqz p0, :cond_1

    invoke-static {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->sRecentlyExplored:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    goto :goto_0
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 11
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->shouldDropEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x2

    const-string v5, "Dropping unwanted progress bar event"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {p0, v4, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return v3

    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityEventUtils;->getEventTextOrDescription(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move v3, v4

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/ProgressBarFormatter;->getProgressPercent(Landroid/view/accessibility/AccessibilityEvent;)F

    move-result v0

    const-wide/high16 v5, 0x4000000000000000L

    float-to-double v7, v0

    const-wide/high16 v9, 0x4049000000000000L

    div-double/2addr v7, v9

    const-wide/high16 v9, 0x3ff0000000000000L

    sub-double/2addr v7, v9

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    double-to-float v1, v5

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getCustomEarcons()Ljava/util/List;

    move-result-object v3

    const v5, 0x7f080031

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "earcon_rate"

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getMetadata()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "earcon_volume"

    const/high16 v6, 0x3f000000

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    move v3, v4

    goto :goto_0
.end method
