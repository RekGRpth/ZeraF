.class public final Lcom/google/android/marvin/talkback/formatter/TextFormatters;
.super Ljava/lang/Object;
.source "TextFormatters.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/formatter/TextFormatters$SelectedTextFormatter;,
        Lcom/google/android/marvin/talkback/formatter/TextFormatters$ChangedTextFormatter;
    }
.end annotation


# static fields
.field private static sAwaitingSelection:Z

.field private static sChangedPackage:Ljava/lang/CharSequence;

.field private static sChangedTimestamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelection:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sAwaitingSelection:Z

    return p0
.end method

.method static synthetic access$100()J
    .locals 2

    sget-wide v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J

    return-wide v0
.end method

.method static synthetic access$102(J)J
    .locals 0
    .param p0    # J

    sput-wide p0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedTimestamp:J

    return-wide p0
.end method

.method static synthetic access$200()Ljava/lang/CharSequence;
    .locals 1

    sget-object v0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$202(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0    # Ljava/lang/CharSequence;

    sput-object p0, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->sChangedPackage:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$300(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-static {p0}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Ljava/lang/CharSequence;II)Z
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I
    .param p2    # I

    invoke-static {p0, p1, p2}, Lcom/google/android/marvin/talkback/formatter/TextFormatters;->areValidIndices(Ljava/lang/CharSequence;II)Z

    move-result v0

    return v0
.end method

.method private static areValidIndices(Ljava/lang/CharSequence;II)Z
    .locals 1
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I
    .param p2    # I

    if-ltz p1, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-gt p2, v0, :cond_0

    if-gt p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getEventText(Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 2
    .param p0    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    goto :goto_0
.end method
