.class public Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;
.super Ljava/lang/Object;
.source "NotificationFormatter.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/formatter/EventSpeechRule$AccessibilityEventFormatter;


# instance fields
.field private final mNotificationHistory:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/app/Notification;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    return-void
.end method

.method private addToHistory(Landroid/app/Notification;)V
    .locals 2
    .param p1    # Landroid/app/Notification;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method private extractNotification(Landroid/view/accessibility/AccessibilityEvent;)Landroid/app/Notification;
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getParcelableData()Landroid/os/Parcelable;

    move-result-object v0

    instance-of v1, v0, Landroid/app/Notification;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Landroid/app/Notification;

    goto :goto_0
.end method

.method private getTypeText(Landroid/content/Context;Landroid/app/Notification;)Ljava/lang/CharSequence;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/app/Notification;

    iget v0, p2, Landroid/app/Notification;->icon:I

    invoke-static {p1, v0}, Lcom/google/android/marvin/talkback/NotificationType;->getNotificationTypeFromIcon(Landroid/content/Context;I)Lcom/google/android/marvin/talkback/NotificationType;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/NotificationType;->getValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private declared-synchronized isRecent(Landroid/app/Notification;)Z
    .locals 3
    .param p1    # Landroid/app/Notification;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->removeFromHistory(Landroid/app/Notification;)Landroid/app/Notification;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v1, v0, Landroid/app/Notification;->when:J

    iput-wide v1, p1, Landroid/app/Notification;->when:J

    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->addToHistory(Landroid/app/Notification;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    monitor-exit p0

    return v1

    :cond_0
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p1, Landroid/app/Notification;->when:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private notificationsAreEqual(Landroid/app/Notification;Landroid/app/Notification;)Z
    .locals 5
    .param p1    # Landroid/app/Notification;
    .param p2    # Landroid/app/Notification;

    const/4 v2, 0x0

    iget-object v3, p1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iget-object v4, p2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    iget-object v1, p2, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-direct {p0, v0, v1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->remoteViewsAreEqual(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private remoteViewsAreEqual(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)Z
    .locals 7
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Landroid/widget/RemoteViews;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-ne p1, p2, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    move v4, v5

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v0

    invoke-virtual {p2}, Landroid/widget/RemoteViews;->getLayoutId()I

    move-result v2

    if-eq v0, v2, :cond_0

    move v4, v5

    goto :goto_0
.end method

.method private removeFromHistory(Landroid/app/Notification;)Landroid/app/Notification;
    .locals 8
    .param p1    # Landroid/app/Notification;

    iget-object v4, p0, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->mNotificationHistory:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Notification;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, v3, Landroid/app/Notification;->when:J

    sub-long v0, v4, v6

    const-wide/32 v4, 0xea60

    cmp-long v4, v0, v4

    if-lez v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v3}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->notificationsAreEqual(Landroid/app/Notification;Landroid/app/Notification;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    :goto_1
    return-object v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public format(Landroid/view/accessibility/AccessibilityEvent;Lcom/google/android/marvin/talkback/TalkBackService;Lcom/google/android/marvin/talkback/Utterance;)Z
    .locals 7
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;
    .param p2    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p3    # Lcom/google/android/marvin/talkback/Utterance;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->extractNotification(Landroid/view/accessibility/AccessibilityEvent;)Landroid/app/Notification;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->isRecent(Landroid/app/Notification;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {p3}, Lcom/google/android/marvin/talkback/Utterance;->getText()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2, v1}, Lcom/google/android/marvin/talkback/formatter/NotificationFormatter;->getTypeText(Landroid/content/Context;Landroid/app/Notification;)Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v2, v1, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v3, v6, v5

    invoke-static {v0, v6}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    new-array v6, v4, [Ljava/lang/Object;

    aput-object v2, v6, v5

    invoke-static {v0, v6}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    :goto_1
    move v5, v4

    goto :goto_0

    :cond_4
    move v4, v5

    goto :goto_1
.end method
