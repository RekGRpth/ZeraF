.class public Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "TalkBackShortcutPreferencesActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040002

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;->addPreferencesFromResource(I)V

    return-void
.end method
