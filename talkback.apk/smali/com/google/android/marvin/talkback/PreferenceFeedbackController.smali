.class public Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
.super Lcom/googlecode/eyesfree/utils/FeedbackController;
.source "PreferenceFeedbackController.java"


# instance fields
.field private final mContentObserver:Landroid/database/ContentObserver;

.field private final mCustomResources:Lcom/google/android/marvin/talkback/CustomResourceMapper;

.field private final mHandler:Landroid/os/Handler;

.field private final mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final mPrefs:Landroid/content/SharedPreferences;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/FeedbackController;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/marvin/talkback/PreferenceFeedbackController$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController$1;-><init>(Lcom/google/android/marvin/talkback/PreferenceFeedbackController;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    new-instance v1, Lcom/google/android/marvin/talkback/PreferenceFeedbackController$2;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController$2;-><init>(Lcom/google/android/marvin/talkback/PreferenceFeedbackController;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mContentObserver:Landroid/database/ContentObserver;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    new-instance v1, Lcom/google/android/marvin/talkback/CustomResourceMapper;

    invoke-direct {v1, p1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mCustomResources:Lcom/google/android/marvin/talkback/CustomResourceMapper;

    const-string v1, "enabled_accessibility_services"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPrefs:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->updatePreferences(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/PreferenceFeedbackController;Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/PreferenceFeedbackController;
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->updatePreferences(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/PreferenceFeedbackController;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private updatePreferences(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f0a000c

    const v8, 0x7f0a0025

    invoke-static {p1, v2, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getIntFromStringPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)I

    move-result v6

    const-string v7, "com.google.android.marvin.kickback"

    invoke-static {v0, v7}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v1

    const-string v7, "com.google.android.marvin.soundback"

    invoke-static {v0, v7}, Lcom/googlecode/eyesfree/utils/PackageManagerUtils;->getVersionCode(Landroid/content/Context;Ljava/lang/CharSequence;)I

    move-result v3

    const/4 v7, 0x5

    if-lt v1, v7, :cond_1

    const-string v7, "com.google.android.marvin.kickback"

    invoke-static {v0, v7}, Lcom/google/android/marvin/utils/SecureSettingsUtils;->isAccessibilityServiceEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    :goto_1
    const/4 v7, 0x7

    if-lt v3, v7, :cond_2

    const-string v7, "com.google.android.marvin.soundback"

    invoke-static {v0, v7}, Lcom/google/android/marvin/utils/SecureSettingsUtils;->isAccessibilityServiceEnabled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    :goto_2
    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->setHapticEnabled(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->setAuditoryEnabled(Z)V

    invoke-virtual {p0, v6}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->setVolume(I)V

    goto :goto_0

    :cond_1
    const v7, 0x7f0a0007

    const v8, 0x7f0c0002

    invoke-static {p1, v2, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v5

    goto :goto_1

    :cond_2
    const v7, 0x7f0a0008

    const v8, 0x7f0c0003

    invoke-static {p1, v2, v7, v8}, Lcom/googlecode/eyesfree/utils/SharedPreferencesUtils;->getBooleanPref(Landroid/content/SharedPreferences;Landroid/content/res/Resources;II)Z

    move-result v4

    goto :goto_2
.end method


# virtual methods
.method public playCustomSound(I)V
    .locals 1
    .param p1    # I

    const/high16 v0, 0x3f800000

    invoke-virtual {p0, p1, v0, v0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(IFF)V

    return-void
.end method

.method public playCustomSound(IFF)V
    .locals 3
    .param p1    # I
    .param p2    # F
    .param p3    # F

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mCustomResources:Lcom/google/android/marvin/talkback/CustomResourceMapper;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->getResourceIdForPreference(I)I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid key resource ID"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playSound(IFF)Z

    return-void
.end method

.method public playCustomSoundConditional(ZII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    if-eqz p1, :cond_0

    move v0, p2

    :goto_0
    if-gtz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, p3

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    goto :goto_1
.end method

.method public playCustomVibration(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mCustomResources:Lcom/google/android/marvin/talkback/CustomResourceMapper;

    invoke-virtual {v1, p1}, Lcom/google/android/marvin/talkback/CustomResourceMapper;->getResourceIdForPreference(I)I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid key resource ID"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playVibration(I)Z

    return-void
.end method

.method public shutdown()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPrefs:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mPreferenceChangeListener:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    invoke-super {p0}, Lcom/googlecode/eyesfree/utils/FeedbackController;->shutdown()V

    return-void
.end method
