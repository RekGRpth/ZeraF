.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule2.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

.field private final mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 6
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v0, 0x7f030003

    const v1, 0x7f0a0118

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    new-instance v0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$1;

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090003

    const v3, 0x1020014

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    const v0, 0x7f080047

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAppsAdapter:Lcom/google/android/marvin/talkback/tutorial/AppsAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setSkipVisible(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setBackVisible(Z)V

    invoke-virtual {p0, v5}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setNextVisible(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->setFinishVisible(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger1()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger2()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger3()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger4()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger5()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger6()V

    return-void
.end method

.method private onTrigger0()V
    .locals 3

    const v0, 0x7f0a0119

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger1()V
    .locals 3

    const v0, 0x7f0a011a

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$3;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$3;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger2()V
    .locals 3

    const v0, 0x7f0a011b

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$4;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$4;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger3()V
    .locals 3

    const v0, 0x7f0a011c

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$5;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$5;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setInstrumentation(Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;)V

    return-void
.end method

.method private onTrigger4()V
    .locals 3

    const v0, 0x7f0a011d

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$6;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$6;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger5()V
    .locals 3

    const v0, 0x7f0a011e

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->mAllApps:Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$7;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2$7;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;)V

    invoke-virtual {v0, v1}, Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView;->setInstrumentation(Lcom/google/android/marvin/talkback/tutorial/InstrumentedListView$ListViewListener;)V

    return-void
.end method

.method private onTrigger6()V
    .locals 6

    const/4 v5, 0x1

    const v0, 0x7f0a011f

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a010b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->addInstruction(IZ[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onShown()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule2;->onTrigger0()V

    return-void
.end method
