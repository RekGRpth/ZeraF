.class Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;
.super Lcom/google/android/marvin/talkback/tutorial/TutorialModule;
.source "TouchTutorialModule3.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;)V
    .locals 4
    .param p1    # Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v0, 0x7f030004

    const v1, 0x7f0a0120

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/marvin/talkback/tutorial/TutorialModule;-><init>(Lcom/google/android/marvin/talkback/tutorial/AccessibilityTutorialActivity;II)V

    const v0, 0x7f080048

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setSkipVisible(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setBackVisible(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setNextVisible(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->setFinishVisible(Z)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger1()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V
    .locals 0
    .param p0    # Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger2()V

    return-void
.end method

.method private onTrigger0()V
    .locals 3

    const v0, 0x7f0a0121

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$1;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$1;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger1()V
    .locals 3

    const v0, 0x7f0a0122

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->mTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$2;

    invoke-direct {v1, p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3$2;-><init>(Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    return-void
.end method

.method private onTrigger2()V
    .locals 6

    const/4 v5, 0x1

    const v0, 0x7f0a0123

    new-array v1, v5, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a010d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v5, v1}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->addInstruction(IZ[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public onShown()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/marvin/talkback/tutorial/TouchTutorialModule3;->onTrigger0()V

    return-void
.end method
