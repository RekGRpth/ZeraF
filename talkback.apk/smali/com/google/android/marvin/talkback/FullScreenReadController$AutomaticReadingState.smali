.class public final enum Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
.super Ljava/lang/Enum;
.source "FullScreenReadController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/FullScreenReadController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AutomaticReadingState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

.field public static final enum ENTERING_WEB_CONTENT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

.field public static final enum READING_FROM_BEGINNING:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

.field public static final enum READING_FROM_NEXT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

.field public static final enum STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v2}, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    new-instance v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    const-string v1, "READING_FROM_BEGINNING"

    invoke-direct {v0, v1, v3}, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_BEGINNING:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    new-instance v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    const-string v1, "READING_FROM_NEXT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_NEXT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    new-instance v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    const-string v1, "ENTERING_WEB_CONTENT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->ENTERING_WEB_CONTENT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->STOPPED:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_BEGINNING:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->READING_FROM_NEXT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->ENTERING_WEB_CONTENT:Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->$VALUES:[Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    return-object v0
.end method

.method public static values()[Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;
    .locals 1

    sget-object v0, Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->$VALUES:[Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    invoke-virtual {v0}, [Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/marvin/talkback/FullScreenReadController$AutomaticReadingState;

    return-object v0
.end method
