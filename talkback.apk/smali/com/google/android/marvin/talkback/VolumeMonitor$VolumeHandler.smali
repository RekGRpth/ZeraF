.class Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "VolumeMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/VolumeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VolumeHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/VolumeMonitor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/VolumeMonitor;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public clearReleaseControl()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    return-void
.end method

.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/VolumeMonitor;)V
    .locals 6
    .param p1    # Landroid/os/Message;
    .param p2    # Lcom/google/android/marvin/talkback/VolumeMonitor;

    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v0, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnVolumeChanged(III)V
    invoke-static {p2, v5, v3, v0}, Lcom/google/android/marvin/talkback/VolumeMonitor;->access$000(Lcom/google/android/marvin/talkback/VolumeMonitor;III)V

    goto :goto_0

    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnControlAcquired(I)V
    invoke-static {p2, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor;->access$100(Lcom/google/android/marvin/talkback/VolumeMonitor;I)V

    goto :goto_0

    :pswitch_2
    # invokes: Lcom/google/android/marvin/talkback/VolumeMonitor;->internalOnReleaseControl()V
    invoke-static {p2}, Lcom/google/android/marvin/talkback/VolumeMonitor;->access$200(Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/marvin/talkback/VolumeMonitor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/VolumeMonitor;)V

    return-void
.end method

.method public onControlAcquired(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->removeMessages(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v2, p1, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public onVolumeChanged(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, p2, p3, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method

.method public releaseControlDelayed()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->clearReleaseControl()V

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x7d0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/VolumeMonitor$VolumeHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
