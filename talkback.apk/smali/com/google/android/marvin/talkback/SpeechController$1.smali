.class Lcom/google/android/marvin/talkback/SpeechController$1;
.super Ljava/lang/Object;
.source "SpeechController.java"

# interfaces
.implements Lcom/google/android/marvin/utils/ProximitySensor$ProximityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/SpeechController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/SpeechController;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/SpeechController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/SpeechController$1;->this$0:Lcom/google/android/marvin/talkback/SpeechController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProximityChanged(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/marvin/talkback/SpeechController$1;->this$0:Lcom/google/android/marvin/talkback/SpeechController;

    # getter for: Lcom/google/android/marvin/talkback/SpeechController;->mService:Lcom/google/android/marvin/talkback/TalkBackService;
    invoke-static {v0}, Lcom/google/android/marvin/talkback/SpeechController;->access$100(Lcom/google/android/marvin/talkback/SpeechController;)Lcom/google/android/marvin/talkback/TalkBackService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/marvin/talkback/TalkBackService;->interruptAllFeedback()V

    :cond_0
    return-void
.end method
