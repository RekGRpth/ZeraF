.class Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;
.super Ljava/lang/Object;
.source "GestureChangeNotificationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    # invokes: Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->clearPreviouslyConfiguredMappings()V
    invoke-static {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->access$000(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    # invokes: Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->dismissNotification()V
    invoke-static {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->access$100(Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/marvin/talkback/TalkBackShortcutPreferencesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity$2;->this$0:Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;

    invoke-virtual {v1}, Lcom/google/android/marvin/talkback/GestureChangeNotificationActivity;->finish()V

    return-void
.end method
