.class Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;
.super Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;
.source "OrientationMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/OrientationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OrientationHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/googlecode/eyesfree/utils/WeakReferenceHandler",
        "<",
        "Lcom/google/android/marvin/talkback/OrientationMonitor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/OrientationMonitor;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/utils/WeakReferenceHandler;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/OrientationMonitor;)V
    .locals 1
    .param p1    # Landroid/os/Message;
    .param p2    # Lcom/google/android/marvin/talkback/OrientationMonitor;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/google/android/marvin/talkback/OrientationMonitor;->announceCurrentRotation(I)V
    invoke-static {p2, v0}, Lcom/google/android/marvin/talkback/OrientationMonitor;->access$000(Lcom/google/android/marvin/talkback/OrientationMonitor;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic handleMessage(Landroid/os/Message;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/os/Message;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/google/android/marvin/talkback/OrientationMonitor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;->handleMessage(Landroid/os/Message;Lcom/google/android/marvin/talkback/OrientationMonitor;)V

    return-void
.end method

.method public startAnnounceTimeout(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;->removeMessages(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v2, p1, v1}, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0xfa

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/marvin/talkback/OrientationMonitor$OrientationHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method
