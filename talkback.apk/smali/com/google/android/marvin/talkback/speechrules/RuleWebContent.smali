.class public Lcom/google/android/marvin/talkback/speechrules/RuleWebContent;
.super Ljava/lang/Object;
.source "RuleWebContent.java"

# interfaces
.implements Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {p2}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->isScriptInjectionEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;
    .locals 6
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/marvin/talkback/TalkBackService;",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            ">;"
        }
    .end annotation

    invoke-static {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    new-instance v1, Lcom/google/android/marvin/utils/RadialMenuInflater;

    invoke-direct {v1, p1}, Lcom/google/android/marvin/utils/RadialMenuInflater;-><init>(Landroid/content/Context;)V

    const v5, 0x7f0f0003

    invoke-virtual {v1, v5}, Lcom/google/android/marvin/utils/RadialMenuInflater;->inflate(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    new-instance v5, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;

    invoke-direct {v5, v4}, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent$WebMenuItemClickListener;-><init>(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/content/Context;

    const v0, 0x7f0a00b8

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
