.class public Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;
.super Ljava/lang/Object;
.source "NodeSpeechRuleProcessor.java"


# static fields
.field private static final COMPARATOR:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

.field private static final mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

.field private static final mRules:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->COMPARATOR:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v0, Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    invoke-direct {v0}, Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;

    const-class v2, Landroid/widget/Spinner;

    const v3, 0x7f0a005d

    const v4, 0x7f0a0074

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleHintTemplate;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    sget-object v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleNonTextViews;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;

    const-class v2, Landroid/widget/RadioButton;

    const v3, 0x7f0a0061

    invoke-direct {v1, v2, v3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;

    const-class v2, Landroid/widget/CompoundButton;

    const v3, 0x7f0a0060

    invoke-direct {v1, v2, v3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;

    const-class v2, Landroid/widget/Button;

    const v3, 0x7f0a0052

    invoke-direct {v1, v2, v3}, Lcom/google/android/marvin/talkback/speechrules/RuleSimpleTemplate;-><init>(Ljava/lang/Class;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSeekBar;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleSeekBar;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleContainer;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleContainer;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleViewGroup;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleViewGroup;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleDefault;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    return-void
.end method

.method private appendDescriptionForTree(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;Landroid/view/accessibility/AccessibilityEvent;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 8
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Ljava/lang/StringBuilder;
    .param p3    # Landroid/view/accessibility/AccessibilityEvent;
    .param p4    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move-object v5, p3

    :goto_1
    invoke-direct {p0, p1, v5}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getDescriptionForNode(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    invoke-static {p2, v6}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendMetadataToBuilder(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;)V

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->getSortedChildren(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/TreeSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-static {v6, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isAccessibilityFocusable(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-direct {p0, v1, p2, p3, p4}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendDescriptionForTree(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;Landroid/view/accessibility/AccessibilityEvent;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    :cond_5
    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method private appendMetadataToBuilder(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isCheckable()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRuleSwitch:Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/marvin/talkback/speechrules/RuleSwitch;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f0a006b

    :goto_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p2, v1}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0a006c

    goto :goto_0
.end method

.method private appendRootMetadataToBuilder(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;)V
    .locals 4
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    const v3, 0x7f0a006d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {p2, v0}, Lcom/googlecode/eyesfree/utils/StringBuilderUtils;->appendWithSeparator(Ljava/lang/StringBuilder;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method

.method private formatTextWithLabel(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;)V
    .locals 8
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    const/4 v7, 0x0

    invoke-static {p1}, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->getLabeledBy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v1, v0, v3, v3}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendDescriptionForTree(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;Landroid/view/accessibility/AccessibilityEvent;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    const v4, 0x7f0a004e

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p2, v5, v7

    const/4 v6, 0x1

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private getDescriptionForNode(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;

    sget-object v2, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p1}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    const-string v3, "Processing node using %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;->format(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getSortedChildren(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/TreeSet;
    .locals 4
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")",
            "Ljava/util/TreeSet",
            "<",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/TreeSet;

    sget-object v3, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->COMPARATOR:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;

    invoke-direct {v1, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p1, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    return-object v1
.end method


# virtual methods
.method public getDescriptionForTree(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Landroid/view/accessibility/AccessibilityEvent;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 2
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Landroid/view/accessibility/AccessibilityEvent;
    .param p3    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendDescriptionForTree(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;Landroid/view/accessibility/AccessibilityEvent;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->formatTextWithLabel(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->appendRootMetadataToBuilder(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getHintForNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    sget-object v2, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mRules:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;

    instance-of v2, v1, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRuleProcessor;->mContext:Landroid/content/Context;

    invoke-interface {v1, v2, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeSpeechRule;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    const-string v3, "Processing node hint using %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {p0, v2, v3, v4}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    check-cast v1, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;

    invoke-interface {v1, p1, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeHintRule;->getHintText(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
