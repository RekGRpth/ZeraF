.class public Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;
.super Ljava/lang/Object;
.source "NodeMenuProcessor.java"


# static fields
.field private static final mRules:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mService:Lcom/google/android/marvin/talkback/TalkBackService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mRules:Ljava/util/LinkedList;

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleSpannables;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleSpannables;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleWebContent;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleEditText;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mRules:Ljava/util/LinkedList;

    new-instance v1, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;

    invoke-direct {v1}, Lcom/google/android/marvin/talkback/speechrules/RuleViewPager;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;)V
    .locals 0
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    return-void
.end method


# virtual methods
.method public prepareMenuForNode(Lcom/googlecode/eyesfree/widget/RadialMenu;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 12
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const v11, 0x7f0a00b7

    const/4 v8, 0x0

    const/4 v9, 0x1

    if-nez p2, :cond_0

    :goto_0
    return v8

    :cond_0
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->clear()V

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    sget-object v10, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mRules:Ljava/util/LinkedList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v4, v10, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v5

    if-ne v5, v9, :cond_3

    invoke-virtual {v3, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;

    iget-object v10, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v8, v10, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;->getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    goto :goto_2

    :cond_3
    if-le v5, v9, :cond_6

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;

    iget-object v8, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v4, v8}, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;->getUserFriendlyMenuName(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/googlecode/eyesfree/widget/RadialMenu;->addSubMenu(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-interface {v4, v8, p2}, Lcom/google/android/marvin/talkback/speechrules/NodeMenuRule;->getMenuItemsForNode(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v7, v2}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->add(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    goto :goto_4

    :cond_5
    invoke-virtual {v7}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->size()I

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->add(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/google/android/marvin/talkback/speechrules/NodeMenuProcessor;->mService:Lcom/google/android/marvin/talkback/TalkBackService;

    invoke-virtual {v8, v11}, Lcom/google/android/marvin/talkback/TalkBackService;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/googlecode/eyesfree/widget/RadialMenu;->add(Ljava/lang/CharSequence;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    :cond_7
    move v8, v9

    goto/16 :goto_0
.end method
