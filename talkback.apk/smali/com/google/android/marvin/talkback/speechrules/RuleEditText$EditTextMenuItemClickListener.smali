.class Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;
.super Ljava/lang/Object;
.source "RuleEditText.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/marvin/talkback/speechrules/RuleEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EditTextMenuItemClickListener"
.end annotation


# instance fields
.field private final mFeedback:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

.field private final mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;


# direct methods
.method public constructor <init>(Lcom/google/android/marvin/talkback/TalkBackService;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 1
    .param p1    # Lcom/google/android/marvin/talkback/TalkBackService;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/marvin/talkback/TalkBackService;->getFeedbackController()Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mFeedback:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    iput-object p2, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/16 v5, 0x10

    const/4 v3, 0x1

    if-nez p1, :cond_0

    iget-object v4, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :goto_0
    return v3

    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v4, 0x7f08003f

    if-ne v1, v4, :cond_2

    const-string v4, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v5, 0x200

    invoke-virtual {v4, v5, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    :goto_1
    if-nez v2, :cond_1

    iget-object v4, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mFeedback:Lcom/google/android/marvin/talkback/PreferenceFeedbackController;

    const v5, 0x7f08002e

    invoke-virtual {v4, v5}, Lcom/google/android/marvin/talkback/PreferenceFeedbackController;->playCustomSound(I)V

    :cond_1
    iget-object v4, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_0

    :cond_2
    const v4, 0x7f080040

    if-ne v1, v4, :cond_3

    const-string v4, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/google/android/marvin/talkback/speechrules/RuleEditText$EditTextMenuItemClickListener;->mNode:Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/16 v5, 0x100

    invoke-virtual {v4, v5, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->performAction(ILandroid/os/Bundle;)Z

    move-result v2

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
