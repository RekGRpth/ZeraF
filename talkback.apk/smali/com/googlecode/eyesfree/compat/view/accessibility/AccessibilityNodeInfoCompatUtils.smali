.class public Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoCompatUtils.java"


# static fields
.field private static final CLASS_AccessibilityNodeInfo:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final METHOD_getLabelFor:Ljava/lang/reflect/Method;

.field private static final METHOD_getLabeledBy:Ljava/lang/reflect/Method;

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const-class v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->TAG:Ljava/lang/String;

    const-string v0, "android.view.accessibility.AccessibilityNodeInfo"

    invoke-static {v0}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->CLASS_AccessibilityNodeInfo:Ljava/lang/Class;

    sget-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->CLASS_AccessibilityNodeInfo:Ljava/lang/Class;

    const-string v1, "getLabelFor"

    new-array v2, v3, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->METHOD_getLabelFor:Ljava/lang/reflect/Method;

    sget-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->CLASS_AccessibilityNodeInfo:Ljava/lang/Class;

    const-string v1, "getLabeledBy"

    new-array v2, v3, [Ljava/lang/Class;

    invoke-static {v0, v1, v2}, Lcom/googlecode/eyesfree/compat/CompatUtils;->getMethod(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->METHOD_getLabeledBy:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLabeledBy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 5
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getInfo()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v3, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->TAG:Ljava/lang/String;

    const-string v4, "Compat node was missing internal node"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    sget-object v3, Lcom/googlecode/eyesfree/compat/view/accessibility/AccessibilityNodeInfoCompatUtils;->METHOD_getLabeledBy:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3, v4}, Lcom/googlecode/eyesfree/compat/CompatUtils;->invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-direct {v2, v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;-><init>(Ljava/lang/Object;)V

    goto :goto_0
.end method
