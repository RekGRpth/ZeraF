.class public Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoUtils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TopToBottomLeftToRightComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
        ">;"
    }
.end annotation


# instance fields
.field private final mFirstBounds:Landroid/graphics/Rect;

.field private final mSecondBounds:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;->mFirstBounds:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;->mSecondBounds:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public compare(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)I
    .locals 8
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;->mFirstBounds:Landroid/graphics/Rect;

    invoke-virtual {p1, v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;->mSecondBounds:Landroid/graphics/Rect;

    invoke-virtual {p2, v4}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    iget v7, v4, Landroid/graphics/Rect;->top:I

    if-gt v6, v7, :cond_1

    const/4 v2, -0x1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    if-lt v6, v7, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    iget v6, v1, Landroid/graphics/Rect;->left:I

    iget v7, v4, Landroid/graphics/Rect;->left:I

    sub-int v2, v6, v7

    if-nez v2, :cond_0

    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int v5, v6, v7

    if-eqz v5, :cond_3

    move v2, v5

    goto :goto_0

    :cond_3
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v6, v7

    if-eqz v0, :cond_4

    move v2, v0

    goto :goto_0

    :cond_4
    iget v6, v1, Landroid/graphics/Rect;->right:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    sub-int v3, v6, v7

    if-eqz v3, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->hashCode()I

    move-result v6

    invoke-virtual {p2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->hashCode()I

    move-result v7

    sub-int v2, v6, v7

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    check-cast p2, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;->compare(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)I

    move-result v0

    return v0
.end method
