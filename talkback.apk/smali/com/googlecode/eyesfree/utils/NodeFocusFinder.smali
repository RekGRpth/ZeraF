.class public Lcom/googlecode/eyesfree/utils/NodeFocusFinder;
.super Ljava/lang/Object;
.source "NodeFocusFinder.java"


# direct methods
.method public static focusSearch(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 3
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1    # I

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->unOwned(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-object v1

    :pswitch_1
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->nextInOrder()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->release()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->previousInOrder()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoRef;->release()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
