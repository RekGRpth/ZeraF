.class Lcom/googlecode/eyesfree/utils/FeedbackController$3;
.super Ljava/lang/Object;
.source "FeedbackController.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/googlecode/eyesfree/utils/FeedbackController;->playMidiScale(IIIIII)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;

.field final synthetic val$file:Ljava/io/File;

.field final synthetic val$scalePlayer:Landroid/media/MediaPlayer;


# direct methods
.method constructor <init>(Lcom/googlecode/eyesfree/utils/FeedbackController;Landroid/media/MediaPlayer;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$3;->this$0:Lcom/googlecode/eyesfree/utils/FeedbackController;

    iput-object p2, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$3;->val$scalePlayer:Landroid/media/MediaPlayer;

    iput-object p3, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$3;->val$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$3;->val$scalePlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/FeedbackController$3;->val$file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    return-void
.end method
