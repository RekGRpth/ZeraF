.class public Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;
.super Ljava/lang/Object;
.source "AccessibilityNodeInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;,
        Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$TopToBottomLeftToRightComparator;,
        Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;
    }
.end annotation


# static fields
.field private static final FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

.field public static final FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

.field private static final FILTER_SCROLL_BACKWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

.field private static final FILTER_SCROLL_FORWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

.field private static final FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

.field private static final SUPPORTS_VISIBILITY:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->SUPPORTS_VISIBILITY:Z

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$1;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$1;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLLABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$2;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$2;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$3;

    invoke-direct {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$3;-><init>()V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;-><init>(I)V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_FORWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    new-instance v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;-><init>(I)V

    sput-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_BACKWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-static {p0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isScrollable(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    return v0
.end method

.method public static findFocusFromHover(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    sget-object v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SHOULD_FOCUS:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getSelfOrMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    return-object v0
.end method

.method private static getMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :try_start_0
    invoke-static {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object p1

    :goto_1
    if-eqz p1, :cond_3

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {p2, p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    :cond_2
    :try_start_2
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object p1

    goto :goto_1

    :cond_3
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes(Ljava/util/Collection;)V

    throw v1
.end method

.method public static getNodeText(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Ljava/lang/CharSequence;
    .locals 4
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v2, 0x0

    if-nez p0, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v3

    if-gtz v3, :cond_0

    :cond_2
    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v1}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v3

    if-lez v3, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_0
.end method

.method public static getRoot(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 2
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    :cond_1
    move-object v0, v1

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    if-nez v1, :cond_1

    goto :goto_0
.end method

.method public static getSelfOrMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2, p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    goto :goto_0
.end method

.method private static hasMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Z
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static hasNonActionableSpeakingChildren(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v11, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_4

    :try_start_0
    invoke-virtual {p1, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_0

    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Child %d is null, skipping it"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v5, v3, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-nez v5, :cond_1

    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Child %d is invisible, skipping it"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-array v5, v3, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    :cond_1
    :try_start_2
    sget-object v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-interface {v5, p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Child %d is focusable, skipping it"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    new-array v5, v3, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    :cond_2
    :try_start_3
    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSpeakingNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-class v5, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const/4 v6, 0x2

    const-string v7, "Does have actionable speaking children (child %d)"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v5, v6, v7, v8}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    new-array v5, v3, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_2
    return v3

    :cond_3
    new-array v5, v3, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v5, v4

    invoke-static {v5}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_1

    :catchall_0
    move-exception v5

    new-array v3, v3, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v4

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v5

    :cond_4
    const-class v3, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v5, "Does not have non-actionable speaking children"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v3, v11, v5, v6}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v3, v4

    goto :goto_2
.end method

.method private static hasText(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 2
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isAccessibilityFocusable(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isActionableForAccessibility(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_3

    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isTopLevelScrollItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isTopLevelScrollItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSpeakingNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasNonActionableSpeakingChildren(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static isActionableForAccessibility(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 3
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isClickable()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isLongClickable()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v2, 0x5

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {p0, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x10
        0x20
        0x1
        0x400
        0x800
    .end array-data
.end method

.method public static isEdgeListItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_BACKWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    const/4 v3, -0x1

    invoke-static {p0, p1, v2, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isMatchingEdgeListItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;I)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_SCROLL_FORWARD:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeActionFilter;

    invoke-static {p0, p1, v2, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isMatchingEdgeListItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;I)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private static isMatchingEdgeListItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;I)Z
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;
    .param p3    # I

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-static {v6, p1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_0

    new-array v6, v7, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    :goto_0
    return v4

    :cond_0
    :try_start_1
    invoke-static {p1, p3}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_1

    invoke-static {p0, v1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->shouldFocusNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v3, v1

    invoke-static {v3, p3}, Lcom/googlecode/eyesfree/utils/NodeFocusFinder;->focusSearch(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v1

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v6

    new-array v7, v7, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v7, v4

    aput-object v1, v7, v5

    aput-object v2, v7, v8

    invoke-static {v7}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v6

    :cond_1
    if-eqz v1, :cond_2

    :try_start_2
    invoke-virtual {v1, v0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    new-array v6, v7, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    :try_start_3
    invoke-static {v6, v1, p2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->getMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {v0, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v6

    if-nez v6, :cond_4

    new-array v6, v7, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v4, v5

    goto :goto_0

    :cond_4
    new-array v6, v7, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v6, v4

    aput-object v1, v6, v5

    aput-object v2, v6, v8

    invoke-static {v6}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0
.end method

.method private static isScrollable(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isScrollable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {p0, v0}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->supportsAnyAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z

    move-result v0

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1000
        0x2000
    .end array-data
.end method

.method private static isSpeakingNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasText(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, has text"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isCheckable()Z

    move-result v2

    if-eqz v2, :cond_1

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, is checkable"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/WebInterfaceUtils;->hasWebContent(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, has web content"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasNonActionableSpeakingChildren(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Speaking, has non-actionable speaking children"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v1}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static isTopLevelScrollItem(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getParent()Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    new-array v2, v2, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isScrollable(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    new-array v3, v2, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v3, 0x3

    :try_start_2
    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/widget/AdapterView;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/widget/ScrollView;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-class v5, Landroid/widget/HorizontalScrollView;

    aput-object v5, v3, v4

    invoke-static {p0, v0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/widget/Spinner;

    aput-object v5, v3, v4

    invoke-static {p0, v0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesAnyClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    if-nez v3, :cond_3

    new-array v3, v2, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v3, v1

    invoke-static {v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    move v1, v2

    goto :goto_0

    :cond_3
    new-array v2, v2, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    goto :goto_0

    :catchall_0
    move-exception v3

    new-array v2, v2, [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    aput-object v0, v2, v1

    invoke-static {v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    throw v3
.end method

.method public static isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 1
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    sget-boolean v0, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->SUPPORTS_VISIBILITY:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->isVisibleToUser()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static varargs nodeMatchesAnyClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[Ljava/lang/Class;)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "[",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-static {p0, p1, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->nodeMatchesClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static nodeMatchesClassByName(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/CharSequence;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Ljava/lang/CharSequence;

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getClassName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v1

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, p0, v2, v0, p2}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    goto :goto_0
.end method

.method public static nodeMatchesClassByType(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Ljava/lang/Class;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getClassName()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->getInstance()Lcom/googlecode/eyesfree/utils/ClassLoadingManager;

    move-result-object v1

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getPackageName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, p0, v2, v0, p2}, Lcom/googlecode/eyesfree/utils/ClassLoadingManager;->checkInstanceOf(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/Class;)Z

    move-result v3

    goto :goto_0
.end method

.method public static recycleNodes(Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;)V"
        }
    .end annotation

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    goto :goto_1

    :cond_2
    invoke-interface {p0}, Ljava/util/Collection;->clear()V

    goto :goto_0
.end method

.method public static varargs recycleNodes([Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 4
    .param p0    # [Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-nez p0, :cond_1

    :cond_0
    return-void

    :cond_1
    move-object v0, p0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static searchFromBfs(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p2    # Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    const/4 v5, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    invoke-static {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-interface {p2, p0, v3}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v5

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    invoke-virtual {v3, v2}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChild(I)Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v0

    if-nez v0, :cond_4

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v4, v0}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static shouldFocusNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    const/4 v1, 0x1

    const/4 v4, 0x2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isVisibleOrLegacy(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-nez v2, :cond_1

    const-class v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v2, "Don\'t focus, node is not visible"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-interface {v2, p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;->accept(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p1}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getChildCount()I

    move-result v2

    if-gtz v2, :cond_2

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Focus, node is focusable and has no children"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {p0, p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->isSpeakingNode(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Focus, node is focusable and has something to speak"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_3
    const-class v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v2, "Don\'t focus, node is focusable but has nothing to speak"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->FILTER_ACCESSIBILITY_FOCUSABLE:Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;

    invoke-static {p0, p1, v2}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasMatchingPredecessor(Landroid/content/Context;Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils$NodeFilter;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {p1}, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;->hasText(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-class v2, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v3, "Focus, node has text and no focusable predecessors"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v4, v3, v0}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_5
    const-class v1, Lcom/googlecode/eyesfree/utils/AccessibilityNodeInfoUtils;

    const-string v2, "Don\'t focus, failed all focusability tests"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v4, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static varargs supportsAnyAction(Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;[I)Z
    .locals 6
    .param p0    # Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .param p1    # [I

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lvedroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getActions()I

    move-result v4

    move-object v1, p1

    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget v0, v1, v2

    and-int v5, v4, v0

    if-ne v5, v0, :cond_0

    const/4 v5, 0x1

    :goto_1
    return v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method
