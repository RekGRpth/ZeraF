.class public Lcom/googlecode/eyesfree/widget/RadialMenuView;
.super Landroid/view/SurfaceView;
.source "RadialMenuView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;,
        Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;
    }
.end annotation


# instance fields
.field private final mAccessDelegate:Lvedroid/support/v4/view/AccessibilityDelegateCompat;

.field private final mCachedCornerBound:Landroid/graphics/RectF;

.field private final mCachedCornerPath:Landroid/graphics/Path;

.field private final mCachedCornerPathReverse:Landroid/graphics/Path;

.field private mCachedCornerPathWidth:F

.field private final mCachedExtremeBound:Landroid/graphics/RectF;

.field private final mCachedInnerBound:Landroid/graphics/RectF;

.field private final mCachedInnerPath:Landroid/graphics/Path;

.field private mCachedMenuSize:I

.field private final mCachedOuterBound:Landroid/graphics/RectF;

.field private final mCachedOuterPath:Landroid/graphics/Path;

.field private final mCachedOuterPathReverse:Landroid/graphics/Path;

.field private mCachedOuterPathWidth:F

.field private mCenter:Landroid/graphics/PointF;

.field private final mCornerFillColor:I

.field private final mCornerRadius:I

.field private final mCornerRadiusSq:I

.field private final mCornerStrokeColor:I

.field private final mCornerTextFillColor:I

.field private mDisplayDot:Z

.field private final mEntryPoint:Landroid/graphics/PointF;

.field private final mExtremeRadius:I

.field private final mExtremeRadiusSq:I

.field private mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

.field private mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

.field private final mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

.field private mHolder:Landroid/view/SurfaceHolder;

.field private final mInnerFillColor:I

.field private final mInnerRadius:I

.field private final mInnerRadiusSq:I

.field private final mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

.field private final mLongPressListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

.field private mMaybeSingleTap:Z

.field private final mOuterFillColor:I

.field private final mOuterRadius:I

.field private final mOuterStrokeColor:I

.field private final mPaint:Landroid/graphics/Paint;

.field private final mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

.field private mRootMenuOffset:F

.field private final mSelectionFilter:Landroid/graphics/ColorFilter;

.field private final mSingleTapRadiusSq:I

.field private final mSpacing:I

.field private mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

.field private final mSubMenuFilter:Landroid/graphics/ColorFilter;

.field private mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

.field private mSubMenuOffset:F

.field private final mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private final mTempMatrix:Landroid/graphics/Matrix;

.field private final mTextFillColor:I

.field private final mTextSize:I

.field private mTouchExplorer:Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

.field private final mUseNodeProvider:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/googlecode/eyesfree/widget/RadialMenu;Z)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p3    # Z

    const/4 v8, 0x1

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mEntryPoint:Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerBound:Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerPath:Landroid/graphics/Path;

    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    sget-object v6, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LIFT_TO_ACTIVATE:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    const/4 v6, 0x0

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedMenuSize:I

    const/4 v6, 0x0

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    new-instance v6, Lcom/googlecode/eyesfree/widget/RadialMenuView$1;

    invoke-direct {v6, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$1;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mAccessDelegate:Lvedroid/support/v4/view/AccessibilityDelegateCompat;

    new-instance v6, Lcom/googlecode/eyesfree/widget/RadialMenuView$2;

    invoke-direct {v6, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$2;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    new-instance v6, Lcom/googlecode/eyesfree/widget/RadialMenuView$3;

    invoke-direct {v6, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$3;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLongPressListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    new-instance v6, Lcom/googlecode/eyesfree/widget/RadialMenuView$4;

    invoke-direct {v6, p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView$4;-><init>(Lcom/googlecode/eyesfree/widget/RadialMenuView;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    iput-object p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLayoutListener:Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;

    invoke-virtual {v6, v7}, Lcom/googlecode/eyesfree/widget/RadialMenu;->setLayoutListener(Lcom/googlecode/eyesfree/widget/RadialMenu$MenuLayoutListener;)V

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v6, Lcom/googlecode/eyesfree/widget/LongPressHandler;

    invoke-direct {v6, p1}, Lcom/googlecode/eyesfree/widget/LongPressHandler;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mLongPressListener:Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;

    invoke-virtual {v6, v7}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->setListener(Lcom/googlecode/eyesfree/widget/LongPressHandler$LongPressListener;)V

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    const/4 v6, -0x3

    invoke-interface {v2, v6}, Landroid/view/SurfaceHolder;->setFormat(I)V

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v2, v6}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSingleTapRadiusSq:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$dimen;->inner_radius:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$dimen;->outer_radius:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterRadius:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$dimen;->corner_radius:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadius:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$dimen;->extreme_radius:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$dimen;->spacing:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSpacing:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$dimen;->text_size:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->inner_fill:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerFillColor:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->outer_fill:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterFillColor:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->outer_stroke:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterStrokeColor:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->text_fill:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextFillColor:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->corner_fill:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerFillColor:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->corner_stroke:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerStrokeColor:I

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->corner_text_fill:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerTextFillColor:I

    const/4 v6, 0x2

    new-array v0, v6, [I

    fill-array-data v0, :array_0

    new-instance v6, Landroid/graphics/drawable/GradientDrawable;

    sget-object v7, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v6, v7, v0}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v6, v8}, Landroid/graphics/drawable/GradientDrawable;->setGradientType(I)V

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v7, v7

    const/high16 v8, 0x40000000

    mul-float/2addr v7, v8

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/GradientDrawable;->setGradientRadius(F)V

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    const/16 v7, 0x80

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->selection_overlay:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    sget v6, Lcom/googlecode/eyesfree/widget/R$color;->submenu_overlay:I

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    new-instance v6, Landroid/graphics/PorterDuffColorFilter;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v4, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionFilter:Landroid/graphics/ColorFilter;

    new-instance v6, Landroid/graphics/PorterDuffColorFilter;

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v6, v5, v7}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuFilter:Landroid/graphics/ColorFilter;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    mul-int/2addr v6, v7

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadiusSq:I

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadius:I

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadius:I

    mul-int/2addr v6, v7

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadiusSq:I

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    iget v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    mul-int/2addr v6, v7

    iput v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadiusSq:I

    iput-boolean p3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mUseNodeProvider:Z

    iget-boolean v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mUseNodeProvider:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mAccessDelegate:Lvedroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {p0, v6}, Lvedroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Lvedroid/support/v4/view/AccessibilityDelegateCompat;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        -0x1000000
    .end array-data
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTouchExplorer:Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

    return-object v0
.end method

.method static synthetic access$002(Lcom/googlecode/eyesfree/widget/RadialMenuView;Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;)Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;
    .locals 0
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTouchExplorer:Lcom/googlecode/eyesfree/widget/RadialMenuView$RadialMenuHelper;

    return-object p1
.end method

.method static synthetic access$102(Lcom/googlecode/eyesfree/widget/RadialMenuView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenuItem;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    return-object v0
.end method

.method static synthetic access$300(Lcom/googlecode/eyesfree/widget/RadialMenuView;Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 0
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-direct {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemLongPressed(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    return-void
.end method

.method static synthetic access$400(Lcom/googlecode/eyesfree/widget/RadialMenuView;)Lcom/googlecode/eyesfree/widget/RadialMenu;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    return-object v0
.end method

.method static synthetic access$500(Lcom/googlecode/eyesfree/widget/RadialMenuView;FF)Landroid/util/Pair;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/widget/RadialMenuView;
    .param p1    # F
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->computeTouchedMenuItem(FF)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private computeTouchedCorner(FF)Landroid/util/Pair;
    .locals 11
    .param p1    # F
    .param p2    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getHeight()I

    move-result v6

    const/4 v5, 0x0

    :goto_0
    const/4 v8, 0x4

    if-ge v5, v8, :cond_3

    iget-object v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v8, v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCorner(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCornerLocation(I)Landroid/graphics/PointF;

    move-result-object v3

    iget v8, v3, Landroid/graphics/PointF;->x:F

    int-to-float v10, v7

    mul-float/2addr v8, v10

    sub-float v1, p1, v8

    iget v8, v3, Landroid/graphics/PointF;->y:F

    int-to-float v10, v6

    mul-float/2addr v8, v10

    sub-float v2, p2, v8

    mul-float v8, v1, v1

    mul-float v10, v2, v2

    add-float v4, v8, v10

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadiusSq:I

    int-to-float v8, v8

    cmpg-float v8, v4, v8

    if-gez v8, :cond_0

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadiusSq:I

    int-to-float v8, v8

    cmpg-float v8, v4, v8

    if-gez v8, :cond_2

    new-instance v8, Landroid/util/Pair;

    invoke-direct {v8, v0, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_1
    return-object v8

    :cond_2
    new-instance v8, Landroid/util/Pair;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-direct {v8, v0, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    move-object v8, v9

    goto :goto_1
.end method

.method private computeTouchedMenuItem(FF)Landroid/util/Pair;
    .locals 25
    .param p1    # F
    .param p2    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair",
            "<",
            "Lcom/googlecode/eyesfree/widget/RadialMenuItem;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    new-instance v13, Landroid/util/Pair;

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v13, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-object/from16 v20, v0

    if-eqz v20, :cond_1

    move-object/from16 v0, p0

    iget v10, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuOffset:F

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    sub-float v7, p1, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    sub-float v8, p2, v20

    mul-float v20, v7, v7

    mul-float v21, v8, v8

    add-float v16, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadiusSq:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_2

    new-instance v20, Landroid/util/Pair;

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-direct/range {v20 .. v22}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_2
    return-object v20

    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget v10, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    goto :goto_1

    :cond_2
    invoke-direct/range {p0 .. p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->computeTouchedCorner(FF)Landroid/util/Pair;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Ljava/lang/Float;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Float;->floatValue()F

    move-result v20

    cmpg-float v20, v20, v16

    if-gez v20, :cond_5

    :cond_3
    new-instance v21, Landroid/util/Pair;

    iget-object v0, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    iget-object v0, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v20, v0

    if-nez v20, :cond_4

    const/16 v20, 0x1

    :goto_3
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v20, v21

    goto :goto_2

    :cond_4
    const/16 v20, 0x0

    goto :goto_3

    :cond_5
    float-to-double v0, v7

    move-wide/from16 v20, v0

    float-to-double v0, v8

    move-wide/from16 v22, v0

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    const-wide v20, 0x4076800000000000L

    invoke-virtual {v9}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v22

    move/from16 v0, v22

    int-to-double v0, v0

    move-wide/from16 v22, v0

    div-double v17, v20, v22

    const-wide/high16 v20, 0x4000000000000000L

    div-double v20, v17, v20

    float-to-double v0, v10

    move-wide/from16 v22, v0

    sub-double v11, v20, v22

    const-wide v20, 0x4066800000000000L

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    sub-double v20, v20, v22

    add-double v20, v20, v11

    const-wide v22, 0x4076800000000000L

    rem-double v14, v20, v22

    const-wide/16 v20, 0x0

    cmpg-double v20, v14, v20

    if-gez v20, :cond_6

    const-wide v20, 0x4076800000000000L

    add-double v14, v14, v20

    :cond_6
    div-double v20, v14, v17

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v19, v0

    if-ltz v19, :cond_7

    invoke-virtual {v9}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_8

    :cond_7
    const/16 v20, 0x6

    const-string v21, "Invalid wedge index: %d"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, v22

    invoke-static {v0, v1, v2, v3}, Lcom/googlecode/eyesfree/utils/LogUtils;->log(Ljava/lang/Object;ILjava/lang/String;[Ljava/lang/Object;)V

    const/16 v20, 0x0

    goto/16 :goto_2

    :cond_8
    new-instance v21, Landroid/util/Pair;

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadiusSq:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    cmpg-float v20, v16, v20

    if-gez v20, :cond_9

    const/16 v20, 0x1

    :goto_4
    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v20, v21

    goto/16 :goto_2

    :cond_9
    const/16 v20, 0x0

    goto :goto_4
.end method

.method private static createBounds(Landroid/graphics/RectF;II)V
    .locals 5
    .param p0    # Landroid/graphics/RectF;
    .param p1    # I
    .param p2    # I

    int-to-float v3, p1

    const/high16 v4, 0x40000000

    div-float v0, v3, v4

    int-to-float v3, p2

    sub-float v1, v0, v3

    int-to-float v3, p2

    add-float v2, v0, v3

    invoke-virtual {p0, v1, v1, v2, v2}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method private static distSq(Landroid/graphics/PointF;FF)F
    .locals 4
    .param p0    # Landroid/graphics/PointF;
    .param p1    # F
    .param p2    # F

    iget v2, p0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v2

    iget v2, p0, Landroid/graphics/PointF;->y:F

    sub-float v1, p2, v2

    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    return v2
.end method

.method private drawCancel(Landroid/graphics/Canvas;IIF)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # F

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v7, v0, Landroid/graphics/PointF;->y:F

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    int-to-float v10, v0

    const/high16 v0, 0x40800000

    div-float v9, v10, v0

    new-instance v8, Landroid/graphics/RectF;

    sub-float v0, v6, v10

    sub-float v1, v7, v10

    add-float v2, v6, v10

    add-float v3, v7, v10

    invoke-direct {v8, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :goto_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterFillColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterStrokeColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextFillColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sub-float v1, v6, v9

    sub-float v2, v7, v9

    add-float v3, v6, v9

    add-float v4, v7, v9

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-float v1, v6, v9

    sub-float v2, v7, v9

    sub-float v3, v6, v9

    add-float v4, v7, v9

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0
.end method

.method private drawCenterDot(Landroid/graphics/Canvas;II)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I

    const/high16 v6, 0x40000000

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getContext()Landroid/content/Context;

    move-result-object v2

    int-to-float v5, p2

    div-float v0, v5, v6

    int-to-float v5, p3

    div-float v1, v5, v6

    iget v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    int-to-float v4, v5

    new-instance v3, Landroid/graphics/RectF;

    sub-float v5, v0, v4

    sub-float v6, v1, v4

    add-float v7, v0, v4

    add-float v8, v1, v4

    invoke-direct {v3, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerFillColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerTextFillColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    sget v5, Lcom/googlecode/eyesfree/widget/R$string;->tap_and:I

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0, v1, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget v5, Lcom/googlecode/eyesfree/widget/R$string;->hold:I

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    int-to-float v6, v6

    add-float/2addr v6, v1

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private drawCorner(Landroid/graphics/Canvas;IIFI)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # F
    .param p5    # I

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCorner(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v13

    if-nez v13, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p5 .. p5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCornerRotation(I)F

    move-result v11

    invoke-static/range {p5 .. p5}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getCornerLocation(I)Landroid/graphics/PointF;

    move-result-object v8

    iget v2, v8, Landroid/graphics/PointF;->x:F

    move/from16 v0, p2

    int-to-float v4, v0

    mul-float v9, v2, v4

    iget v2, v8, Landroid/graphics/PointF;->y:F

    move/from16 v0, p3

    int-to-float v4, v0

    mul-float v10, v2, v4

    invoke-virtual {v13}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v13, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :goto_1
    iput v11, v13, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    move/from16 v0, p4

    move/from16 v1, p4

    invoke-virtual {v2, v11, v0, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    sub-float v4, v9, p4

    sub-float v5, v10, p4

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerFillColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerStrokeColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerTextFillColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathWidth:F

    invoke-static {v2, v12, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getEllipsizedText(Landroid/graphics/Paint;Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v3

    const/high16 v2, 0x42b40000

    cmpg-float v2, v11, v2

    if-gez v2, :cond_1

    const/high16 v2, -0x3d4c0000

    cmpl-float v2, v11, v2

    if-gtz v2, :cond_2

    :cond_1
    const/high16 v2, 0x43870000

    cmpl-float v2, v11, v2

    if-lez v2, :cond_5

    :cond_2
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    :goto_2
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v13}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    :cond_4
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    :cond_5
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    neg-int v2, v2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    goto :goto_2
.end method

.method private drawWedge(Landroid/graphics/Canvas;IIFILcom/googlecode/eyesfree/widget/RadialMenu;F)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # F
    .param p5    # I
    .param p6    # Lcom/googlecode/eyesfree/widget/RadialMenu;
    .param p7    # F

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v2, :cond_1

    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuOffset:F

    :goto_0
    move-object/from16 v0, p6

    move/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v11

    invoke-virtual {v11}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p7

    add-float v9, v2, v8

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {v11, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSelectionFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    :goto_1
    iput v9, v11, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    move/from16 v0, p4

    move/from16 v1, p4

    invoke-virtual {v2, v9, v0, v1}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float v4, v4, p4

    iget-object v5, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float v5, v5, p4

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerFillColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterFillColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x40000000

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterStrokeColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextFillColor:I

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    int-to-float v4, v4

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathWidth:F

    invoke-static {v2, v10, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getEllipsizedText(Landroid/graphics/Paint;Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v3

    const/high16 v2, 0x42b40000

    cmpg-float v2, v9, v2

    if-ltz v2, :cond_0

    const/high16 v2, 0x43870000

    cmpl-float v2, v9, v2

    if-lez v2, :cond_4

    :cond_0
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    :goto_2
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void

    :cond_1
    iget v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v11}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    :cond_3
    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    :cond_4
    iget-object v4, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    const/4 v5, 0x0

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mTextSize:I

    neg-int v2, v2

    int-to-float v6, v2

    iget-object v7, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mPaint:Landroid/graphics/Paint;

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawTextOnPath(Ljava/lang/String;Landroid/graphics/Path;FFLandroid/graphics/Paint;)V

    goto :goto_2
.end method

.method private getArcLength(FF)F
    .locals 2
    .param p1    # F
    .param p2    # F

    const v0, 0x40490fdb

    mul-float/2addr v0, p2

    const/high16 v1, 0x40000000

    mul-float/2addr v0, v1

    const/high16 v1, 0x43b40000

    div-float v1, p1, v1

    mul-float/2addr v0, v1

    return v0
.end method

.method private static getEllipsizedText(Landroid/graphics/Paint;Ljava/lang/String;F)Ljava/lang/String;
    .locals 8
    .param p0    # Landroid/graphics/Paint;
    .param p1    # Ljava/lang/String;
    .param p2    # F

    const/4 v7, 0x0

    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    cmpg-float v4, v3, p2

    if-gtz v4, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const-string v4, "\u2026"

    invoke-virtual {p0, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    const/4 v4, 0x1

    sub-float v5, p2, v0

    const/4 v6, 0x0

    invoke-virtual {p0, p1, v4, v5, v6}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    move-result v1

    const/16 v4, 0x20

    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v2

    if-lez v2, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u2026"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u2026"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private invalidateCachedShapes()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    :goto_0
    invoke-virtual {v9}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v10

    if-gtz v10, :cond_1

    :goto_1
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_0

    :cond_1
    const/high16 v14, 0x43b40000

    int-to-float v15, v10

    div-float v13, v14, v15

    const/high16 v14, 0x40000000

    div-float v14, v13, v14

    const/high16 v15, 0x42b40000

    add-float v11, v14, v15

    const/high16 v6, 0x42b40000

    const/high16 v4, 0x43070000

    move-object/from16 v0, p0

    iget v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    mul-int/lit8 v7, v14, 0x2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerBound:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mInnerRadius:I

    invoke-static {v14, v7, v15}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mOuterRadius:I

    invoke-static {v14, v7, v15}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCornerRadius:I

    invoke-static {v14, v7, v15}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    invoke-static {v14, v7, v15}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->createBounds(Landroid/graphics/RectF;II)V

    move-object/from16 v0, p0

    iget v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSpacing:I

    int-to-float v14, v14

    sub-float v14, v13, v14

    sub-float v8, v14, v11

    const/high16 v14, 0x40000000

    div-float v14, v13, v14

    sub-float v1, v14, v11

    move-object/from16 v0, p0

    iget v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSpacing:I

    int-to-float v14, v14

    sub-float v12, v14, v11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerPath:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->rewind()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerBound:Landroid/graphics/RectF;

    sub-float v16, v12, v8

    move/from16 v0, v16

    invoke-virtual {v14, v15, v8, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v16, v8, v12

    move/from16 v0, v16

    invoke-virtual {v14, v15, v12, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedInnerPath:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->rewind()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v16, v8, v1

    move/from16 v0, v16

    invoke-virtual {v14, v15, v1, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    sub-float v16, v12, v8

    move/from16 v0, v16

    invoke-virtual {v14, v15, v8, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v16, v1, v12

    move/from16 v0, v16

    invoke-virtual {v14, v15, v12, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPath:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->close()V

    sub-float v14, v8, v12

    move-object/from16 v0, p0

    iget v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v15, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getArcLength(FF)F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathWidth:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->rewind()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v16, v12, v1

    move/from16 v0, v16

    invoke-virtual {v14, v15, v1, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    sub-float v16, v8, v12

    move/from16 v0, v16

    invoke-virtual {v14, v15, v12, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterBound:Landroid/graphics/RectF;

    sub-float v16, v1, v8

    move/from16 v0, v16

    invoke-virtual {v14, v15, v8, v0}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedOuterPathReverse:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->close()V

    const/high16 v3, -0x3dcc0000

    const/high16 v2, -0x3d4c0000

    const/high16 v5, -0x3cf90000

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->rewind()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    const/high16 v16, -0x3d4c0000

    const/high16 v17, 0x42340000

    invoke-virtual/range {v14 .. v17}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    const/high16 v16, -0x3dcc0000

    const/high16 v17, -0x3d4c0000

    invoke-virtual/range {v14 .. v17}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    const/high16 v16, -0x3cf90000

    const/high16 v17, 0x42340000

    invoke-virtual/range {v14 .. v17}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPath:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->close()V

    const/high16 v14, 0x42b40000

    move-object/from16 v0, p0

    iget v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v15, v15

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getArcLength(FF)F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathWidth:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->rewind()V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    const/high16 v16, -0x3d4c0000

    const/high16 v17, -0x3dcc0000

    invoke-virtual/range {v14 .. v17}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedExtremeBound:Landroid/graphics/RectF;

    const/high16 v16, -0x3cf90000

    const/high16 v17, 0x42b40000

    invoke-virtual/range {v14 .. v17}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerBound:Landroid/graphics/RectF;

    const/high16 v16, -0x3dcc0000

    const/high16 v17, -0x3dcc0000

    invoke-virtual/range {v14 .. v17}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedCornerPathReverse:Landroid/graphics/Path;

    invoke-virtual {v14}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, p0

    iput v10, v0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedMenuSize:I

    goto/16 :goto_1
.end method

.method private onEnter(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mEntryPoint:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    return-void
.end method

.method private onItemFocused(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 3
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    if-ne v1, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    :goto_1
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    if-nez p1, :cond_2

    invoke-virtual {v0, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->clearSelection(I)Z

    :goto_2
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->sendAccessibilityEvent(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_2

    :cond_3
    invoke-virtual {v0, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->selectMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_2
.end method

.method private onItemLongPressed(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    sget-object v1, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v0

    iget v1, p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V

    goto :goto_0
.end method

.method private onItemSelected(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V
    .locals 4
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    :goto_0
    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    if-nez p1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    :goto_1
    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->sendAccessibilityEvent(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->hasSubMenu()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->getSubMenu()Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    move-result-object v1

    iget v2, p1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->offset:F

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v0, p1, v3}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialMenuItem;->isCorner()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    invoke-virtual {v1, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v0, p1, v2}, Lcom/googlecode/eyesfree/widget/RadialMenu;->performMenuItem(Lcom/googlecode/eyesfree/widget/RadialMenuItem;I)Z

    goto :goto_1
.end method

.method private onMove(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    const/4 v3, 0x0

    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayDot:Z

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayDot:Z

    invoke-virtual {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->displayAt(FF)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mEntryPoint:Landroid/graphics/PointF;

    invoke-static {v1, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->distSq(Landroid/graphics/PointF;FF)F

    move-result v1

    iget v2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSingleTapRadiusSq:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    iput-boolean v3, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->computeTouchedMenuItem(FF)Landroid/util/Pair;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_3

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemFocused(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    goto :goto_0
.end method

.method private onUp(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->computeTouchedMenuItem(FF)Landroid/util/Pair;

    move-result-object v0

    iget-boolean v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mMaybeSingleTap:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-direct {p0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemSelected(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    goto :goto_0
.end method

.method private setSubMenu(Lcom/googlecode/eyesfree/widget/RadialSubMenu;F)V
    .locals 2
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialSubMenu;
    .param p2    # F

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    iput p2, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuOffset:F

    invoke-direct {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidateCachedShapes()V

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->onShow()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    sget-object v1, Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;->LONG_PRESS:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/googlecode/eyesfree/widget/RadialSubMenu;->getItem(I)Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onItemFocused(Lcom/googlecode/eyesfree/widget/RadialMenuItem;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public displayAt(FF)V
    .locals 2
    .param p1    # F
    .param p2    # F

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayDot:Z

    iput-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    iput-object v1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mFocusedItem:Lcom/googlecode/eyesfree/widget/RadialMenuItem;

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    return-void
.end method

.method public displayDot()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayDot:Z

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidate()V

    return-void
.end method

.method public invalidate()V
    .locals 14

    const/high16 v11, 0x40000000

    const/4 v13, 0x0

    invoke-super {p0}, Landroid/view/SurfaceView;->invalidate()V

    iget-object v8, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHolder:Landroid/view/SurfaceHolder;

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {v8}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v13, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->getHeight()I

    move-result v3

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayDot:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    int-to-float v10, v2

    div-float/2addr v10, v11

    iput v10, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    int-to-float v10, v3

    div-float/2addr v10, v11

    iput v10, v0, Landroid/graphics/PointF;->y:F

    :cond_3
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    iget-object v10, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    int-to-float v11, v2

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCenter:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    int-to-float v12, v3

    div-float/2addr v11, v12

    invoke-virtual {v0, v10, v11}, Landroid/graphics/drawable/GradientDrawable;->setGradientCenter(FF)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v13, v13, v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mGradientBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mDisplayDot:Z

    if-eqz v0, :cond_4

    invoke-direct {p0, v1, v2, v3}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawCenterDot(Landroid/graphics/Canvas;II)V

    invoke-interface {v8, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    if-eqz v0, :cond_6

    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenu:Lcom/googlecode/eyesfree/widget/RadialSubMenu;

    :goto_1
    invoke-virtual {v6}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v9

    const/high16 v0, 0x43b40000

    int-to-float v10, v9

    div-float v7, v0, v10

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mExtremeRadius:I

    int-to-float v4, v0

    iget v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mCachedMenuSize:I

    invoke-virtual {v6}, Lcom/googlecode/eyesfree/widget/RadialMenu;->size()I

    move-result v10

    if-eq v0, v10, :cond_5

    invoke-direct {p0}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->invalidateCachedShapes()V

    :cond_5
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawCancel(Landroid/graphics/Canvas;IIF)V

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v9, :cond_7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawWedge(Landroid/graphics/Canvas;IIFILcom/googlecode/eyesfree/widget/RadialMenu;F)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_6
    iget-object v6, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenu:Lcom/googlecode/eyesfree/widget/RadialMenu;

    goto :goto_1

    :cond_7
    const/4 v5, 0x0

    :goto_3
    const/4 v0, 0x4

    if-ge v5, v0, :cond_8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->drawCorner(Landroid/graphics/Canvas;IIFI)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_8
    invoke-interface {v8, v1}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mUseNodeProvider:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    if-nez v4, :cond_0

    const/16 v3, 0x140

    :goto_0
    if-nez v0, :cond_1

    const/16 v2, 0x1e0

    :goto_1
    invoke-virtual {p0, v3, v2}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onEnter(FF)V

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onMove(FF)V

    :goto_1
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mHandler:Lcom/googlecode/eyesfree/widget/LongPressHandler;

    invoke-virtual {v0, p0, p1}, Lcom/googlecode/eyesfree/widget/LongPressHandler;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/widget/RadialMenuView;->onUp(FF)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setOffset(I)V
    .locals 1
    .param p1    # I

    int-to-float v0, p1

    iput v0, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mRootMenuOffset:F

    return-void
.end method

.method public setSubMenuMode(Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/RadialMenuView;->mSubMenuMode:Lcom/googlecode/eyesfree/widget/RadialMenuView$SubMenuMode;

    return-void
.end method
