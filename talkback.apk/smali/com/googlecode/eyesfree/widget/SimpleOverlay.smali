.class public Lcom/googlecode/eyesfree/widget/SimpleOverlay;
.super Ljava/lang/Object;
.source "SimpleOverlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/googlecode/eyesfree/widget/SimpleOverlay$SilentFrameLayout;,
        Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;
    }
.end annotation


# instance fields
.field private final mContentView:Landroid/view/ViewGroup;

.field private final mContext:Landroid/content/Context;

.field private mKeyListener:Landroid/view/View$OnKeyListener;

.field private mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

.field private final mParams:Landroid/view/WindowManager$LayoutParams;

.field private mVisible:Z

.field private final mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContext:Landroid/content/Context;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    new-instance v0, Lcom/googlecode/eyesfree/widget/SimpleOverlay$1;

    invoke-direct {v0, p0, p1}, Lcom/googlecode/eyesfree/widget/SimpleOverlay$1;-><init>(Lcom/googlecode/eyesfree/widget/SimpleOverlay;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x7d3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)Landroid/view/View$OnKeyListener;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mKeyListener:Landroid/view/View$OnKeyListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/widget/SimpleOverlay;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public getParams()Landroid/view/WindowManager$LayoutParams;
    .locals 2

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    return-object v0
.end method

.method public final hide()V
    .locals 2

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;->onHide(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V

    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->onHide()V

    goto :goto_0
.end method

.method public isVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    return v0
.end method

.method protected onHide()V
    .locals 0

    return-void
.end method

.method protected onShow()V
    .locals 0

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setListener(Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;)V
    .locals 0
    .param p1    # Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    iput-object p1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    return-void
.end method

.method public setParams(Landroid/view/WindowManager$LayoutParams;)V
    .locals 3
    .param p1    # Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public final show()V
    .locals 3

    iget-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mContentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mVisible:Z

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->mListener:Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;

    invoke-interface {v0, p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay$SimpleOverlayListener;->onShow(Lcom/googlecode/eyesfree/widget/SimpleOverlay;)V

    :cond_1
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/widget/SimpleOverlay;->onShow()V

    goto :goto_0
.end method
