.class public Lcom/leff/midi/event/meta/EndOfTrack;
.super Lcom/leff/midi/event/meta/MetaEvent;
.source "EndOfTrack.java"


# direct methods
.method public constructor <init>(JJ)V
    .locals 7
    .param p1    # J
    .param p3    # J

    const/16 v5, 0x2f

    new-instance v6, Lcom/leff/midi/util/VariableLengthInt;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Lcom/leff/midi/util/VariableLengthInt;-><init>(I)V

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/leff/midi/event/meta/MetaEvent;-><init>(JJILcom/leff/midi/util/VariableLengthInt;)V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/leff/midi/event/MidiEvent;)I
    .locals 6
    .param p1    # Lcom/leff/midi/event/MidiEvent;

    const/4 v0, -0x1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/leff/midi/event/meta/EndOfTrack;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/leff/midi/event/meta/EndOfTrack;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    :goto_0
    move v1, v0

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/leff/midi/event/meta/EndOfTrack;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v2}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getDelta()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/leff/midi/event/meta/EndOfTrack;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v2}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getDelta()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v1, v0

    goto :goto_1

    :cond_3
    instance-of v0, p1, Lcom/leff/midi/event/meta/EndOfTrack;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    goto :goto_1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {p0, p1}, Lcom/leff/midi/event/meta/EndOfTrack;->compareTo(Lcom/leff/midi/event/MidiEvent;)I

    move-result v0

    return v0
.end method

.method protected getEventSize()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public writeToFile(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/leff/midi/event/meta/MetaEvent;->writeToFile(Ljava/io/OutputStream;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method
