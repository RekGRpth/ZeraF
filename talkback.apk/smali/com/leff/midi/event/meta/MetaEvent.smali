.class public abstract Lcom/leff/midi/event/meta/MetaEvent;
.super Lcom/leff/midi/event/MidiEvent;
.source "MetaEvent.java"


# instance fields
.field protected mLength:Lcom/leff/midi/util/VariableLengthInt;

.field protected mType:I


# direct methods
.method protected constructor <init>(JJILcom/leff/midi/util/VariableLengthInt;)V
    .locals 1
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # Lcom/leff/midi/util/VariableLengthInt;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/leff/midi/event/MidiEvent;-><init>(JJ)V

    and-int/lit16 v0, p5, 0xff

    iput v0, p0, Lcom/leff/midi/event/meta/MetaEvent;->mType:I

    iput-object p6, p0, Lcom/leff/midi/event/meta/MetaEvent;->mLength:Lcom/leff/midi/util/VariableLengthInt;

    return-void
.end method


# virtual methods
.method protected writeToFile(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-super {p0, p1, v0}, Lcom/leff/midi/event/MidiEvent;->writeToFile(Ljava/io/OutputStream;Z)V

    const/16 v0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    iget v0, p0, Lcom/leff/midi/event/meta/MetaEvent;->mType:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public writeToFile(Ljava/io/OutputStream;Z)V
    .locals 0
    .param p1    # Ljava/io/OutputStream;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/leff/midi/event/meta/MetaEvent;->writeToFile(Ljava/io/OutputStream;)V

    return-void
.end method
