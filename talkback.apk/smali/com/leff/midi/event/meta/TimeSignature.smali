.class public Lcom/leff/midi/event/meta/TimeSignature;
.super Lcom/leff/midi/event/meta/MetaEvent;
.source "TimeSignature.java"


# instance fields
.field private mDenominator:I

.field private mDivision:I

.field private mMeter:I

.field private mNumerator:I


# direct methods
.method public constructor <init>()V
    .locals 9

    const-wide/16 v1, 0x0

    const/4 v5, 0x4

    const/16 v7, 0x18

    const/16 v8, 0x8

    move-object v0, p0

    move-wide v3, v1

    move v6, v5

    invoke-direct/range {v0 .. v8}, Lcom/leff/midi/event/meta/TimeSignature;-><init>(JJIIII)V

    return-void
.end method

.method public constructor <init>(JJIIII)V
    .locals 7
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    const/16 v5, 0x58

    new-instance v6, Lcom/leff/midi/util/VariableLengthInt;

    const/4 v0, 0x4

    invoke-direct {v6, v0}, Lcom/leff/midi/util/VariableLengthInt;-><init>(I)V

    move-object v0, p0

    move-wide v1, p1

    move-wide v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/leff/midi/event/meta/MetaEvent;-><init>(JJILcom/leff/midi/util/VariableLengthInt;)V

    invoke-virtual {p0, p5, p6, p7, p8}, Lcom/leff/midi/event/meta/TimeSignature;->setTimeSignature(IIII)V

    return-void
.end method

.method private log2(I)I
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public compareTo(Lcom/leff/midi/event/MidiEvent;)I
    .locals 7
    .param p1    # Lcom/leff/midi/event/MidiEvent;

    const/4 v1, -0x1

    const/4 v2, 0x1

    iget-wide v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    iget-wide v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mTick:J

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getTick()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v3}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getDelta()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDelta:Lcom/leff/midi/util/VariableLengthInt;

    invoke-virtual {v3}, Lcom/leff/midi/util/VariableLengthInt;->getValue()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {p1}, Lcom/leff/midi/event/MidiEvent;->getDelta()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_3

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    instance-of v3, p1, Lcom/leff/midi/event/meta/TimeSignature;

    if-nez v3, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    move-object v0, p1

    check-cast v0, Lcom/leff/midi/event/meta/TimeSignature;

    iget v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    iget v4, v0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    iget v4, v0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    if-lt v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    iget v4, v0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    if-eq v3, v4, :cond_7

    iget v3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    iget v4, v0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    if-lt v3, v4, :cond_0

    move v1, v2

    goto :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/leff/midi/event/MidiEvent;

    invoke-virtual {p0, p1}, Lcom/leff/midi/event/meta/TimeSignature;->compareTo(Lcom/leff/midi/event/MidiEvent;)I

    move-result v0

    return v0
.end method

.method protected getEventSize()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public getRealDenominator()I
    .locals 4

    const-wide/high16 v0, 0x4000000000000000L

    iget v2, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public setTimeSignature(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    invoke-direct {p0, p2}, Lcom/leff/midi/event/meta/TimeSignature;->log2(I)I

    move-result v0

    iput v0, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    iput p3, p0, Lcom/leff/midi/event/meta/TimeSignature;->mMeter:I

    iput p4, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDivision:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/leff/midi/event/meta/MetaEvent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/leff/midi/event/meta/TimeSignature;->getRealDenominator()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToFile(Ljava/io/OutputStream;)V
    .locals 1
    .param p1    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/leff/midi/event/meta/MetaEvent;->writeToFile(Ljava/io/OutputStream;)V

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    iget v0, p0, Lcom/leff/midi/event/meta/TimeSignature;->mNumerator:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    iget v0, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDenominator:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    iget v0, p0, Lcom/leff/midi/event/meta/TimeSignature;->mMeter:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    iget v0, p0, Lcom/leff/midi/event/meta/TimeSignature;->mDivision:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method
