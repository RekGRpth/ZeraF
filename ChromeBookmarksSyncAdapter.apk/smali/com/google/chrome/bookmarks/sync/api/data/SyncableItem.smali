.class public Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;
.super Ljava/lang/Object;
.source "SyncableItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem$DeleteBuilder;,
        Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem$ModifyBuilder;,
        Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem$InsertBuilder;,
        Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem$Builder;
    }
.end annotation


# static fields
.field public static DELETE_TYPE:I

.field public static INSERT_TYPE:I

.field public static MODIFY_TYPE:I


# instance fields
.field private final mItemEntity:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->INSERT_TYPE:I

    const/4 v0, 0x2

    sput v0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->MODIFY_TYPE:I

    const/4 v0, 0x3

    sput v0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->DELETE_TYPE:I

    return-void
.end method

.method protected constructor <init>(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;I)V
    .locals 0
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->mItemEntity:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;

    iput p2, p0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->mType:I

    return-void
.end method


# virtual methods
.method public getSyncEntity()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;
    .locals 1

    iget-object v0, p0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->mItemEntity:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$SyncEntity;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/chrome/bookmarks/sync/api/data/SyncableItem;->mType:I

    return v0
.end method
