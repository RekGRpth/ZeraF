.class public final Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientToServerResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;,
        Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;


# instance fields
.field private authenticate_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

.field private clearUserData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

.field private clientCommand_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

.field private commit_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

.field private errorCode_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

.field private errorMessage_:Ljava/lang/String;

.field private getUpdates_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

.field private hasAuthenticate:Z

.field private hasClearUserData:Z

.field private hasClientCommand:Z

.field private hasCommit:Z

.field private hasErrorCode:Z

.field private hasErrorMessage:Z

.field private hasGetUpdates:Z

.field private hasProfilingData:Z

.field private hasStoreBirthday:Z

.field private hasStreamData:Z

.field private hasStreamMetadata:Z

.field private memoizedSerializedSize:I

.field private migratedDataTypeId_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private profilingData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

.field private storeBirthday_:Ljava/lang/String;

.field private streamData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

.field private streamMetadata_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;-><init>(Z)V

    sput-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;->internalForceInit()V

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    invoke-direct {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorMessage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->storeBirthday_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->migratedDataTypeId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;)V
    .locals 0
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorMessage_:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->storeBirthday_:Ljava/lang/String;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->migratedDataTypeId_:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$23300(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->migratedDataTypeId_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$23302(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->migratedDataTypeId_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$23402(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasCommit:Z

    return p1
.end method

.method static synthetic access$23500(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->commit_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    return-object v0
.end method

.method static synthetic access$23502(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->commit_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    return-object p1
.end method

.method static synthetic access$23602(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasGetUpdates:Z

    return p1
.end method

.method static synthetic access$23700(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getUpdates_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    return-object v0
.end method

.method static synthetic access$23702(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getUpdates_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    return-object p1
.end method

.method static synthetic access$23802(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasAuthenticate:Z

    return p1
.end method

.method static synthetic access$23900(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->authenticate_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    return-object v0
.end method

.method static synthetic access$23902(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->authenticate_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    return-object p1
.end method

.method static synthetic access$24002(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClearUserData:Z

    return p1
.end method

.method static synthetic access$24100(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clearUserData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    return-object v0
.end method

.method static synthetic access$24102(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clearUserData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    return-object p1
.end method

.method static synthetic access$24202(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamMetadata:Z

    return p1
.end method

.method static synthetic access$24300(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamMetadata_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    return-object v0
.end method

.method static synthetic access$24302(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamMetadata_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    return-object p1
.end method

.method static synthetic access$24402(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamData:Z

    return p1
.end method

.method static synthetic access$24500(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    return-object v0
.end method

.method static synthetic access$24502(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    return-object p1
.end method

.method static synthetic access$24602(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorCode:Z

    return p1
.end method

.method static synthetic access$24702(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorCode_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    return-object p1
.end method

.method static synthetic access$24802(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorMessage:Z

    return p1
.end method

.method static synthetic access$24902(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorMessage_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$25002(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStoreBirthday:Z

    return p1
.end method

.method static synthetic access$25102(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->storeBirthday_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$25202(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClientCommand:Z

    return p1
.end method

.method static synthetic access$25300(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clientCommand_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    return-object v0
.end method

.method static synthetic access$25302(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clientCommand_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    return-object p1
.end method

.method static synthetic access$25402(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasProfilingData:Z

    return p1
.end method

.method static synthetic access$25500(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->profilingData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    return-object v0
.end method

.method static synthetic access$25502(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->profilingData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->commit_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getUpdates_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->authenticate_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clearUserData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamMetadata_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;->UNKNOWN:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorCode_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clientCommand_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->profilingData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    return-void
.end method

.method public static newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;
    .locals 1

    # invokes: Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->create()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;
    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->access$23100()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->mergeFrom(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .locals 1
    .param p0    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    # invokes: Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->buildParsed()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    invoke-static {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->access$23000(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .locals 1
    .param p0    # Ljava/io/InputStream;
    .param p1    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    # invokes: Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->buildParsed()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    invoke-static {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;->access$23000(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAuthenticate()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->authenticate_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    return-object v0
.end method

.method public getClearUserData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clearUserData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    return-object v0
.end method

.method public getClientCommand()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->clientCommand_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    return-object v0
.end method

.method public getCommit()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->commit_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public getErrorCode()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorCode_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->errorMessage_:Ljava/lang/String;

    return-object v0
.end method

.method public getGetUpdates()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getUpdates_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    return-object v0
.end method

.method public getMigratedDataTypeIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->migratedDataTypeId_:Ljava/util/List;

    return-object v0
.end method

.method public getProfilingData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->profilingData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    iget v3, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->memoizedSerializedSize:I

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    move v4, v3

    :goto_0
    return v4

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasCommit()Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getCommit()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasGetUpdates()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getGetUpdates()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_2
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasAuthenticate()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x3

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getAuthenticate()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_3
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorCode()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getErrorCode()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;->getNumber()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v5

    add-int/2addr v3, v5

    :cond_4
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorMessage()Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x5

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getErrorMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_5
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStoreBirthday()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x6

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStoreBirthday()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_6
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClientCommand()Z

    move-result v5

    if-eqz v5, :cond_7

    const/4 v5, 0x7

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getClientCommand()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_7
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasProfilingData()Z

    move-result v5

    if-eqz v5, :cond_8

    const/16 v5, 0x8

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getProfilingData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_8
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClearUserData()Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0x9

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getClearUserData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_9
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamMetadata()Z

    move-result v5

    if-eqz v5, :cond_a

    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStreamMetadata()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_a
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamData()Z

    move-result v5

    if-eqz v5, :cond_b

    const/16 v5, 0xb

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStreamData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v5

    add-int/2addr v3, v5

    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getMigratedDataTypeIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_1

    :cond_c
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getMigratedDataTypeIdList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v3, v5

    iput v3, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->memoizedSerializedSize:I

    move v4, v3

    goto/16 :goto_0
.end method

.method public getStoreBirthday()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->storeBirthday_:Ljava/lang/String;

    return-object v0
.end method

.method public getStreamData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamData_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    return-object v0
.end method

.method public getStreamMetadata()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->streamMetadata_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    return-object v0
.end method

.method public hasAuthenticate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasAuthenticate:Z

    return v0
.end method

.method public hasClearUserData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClearUserData:Z

    return v0
.end method

.method public hasClientCommand()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClientCommand:Z

    return v0
.end method

.method public hasCommit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasCommit:Z

    return v0
.end method

.method public hasErrorCode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorCode:Z

    return v0
.end method

.method public hasErrorMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorMessage:Z

    return v0
.end method

.method public hasGetUpdates()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasGetUpdates:Z

    return v0
.end method

.method public hasProfilingData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasProfilingData:Z

    return v0
.end method

.method public hasStoreBirthday()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStoreBirthday:Z

    return v0
.end method

.method public hasStreamData()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamData:Z

    return v0
.end method

.method public hasStreamMetadata()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamMetadata:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasCommit()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getCommit()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasGetUpdates()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getGetUpdates()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasAuthenticate()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getAuthenticate()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamData()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStreamData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;
    .locals 1

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasCommit()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getCommit()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$CommitResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasGetUpdates()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getGetUpdates()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasAuthenticate()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getAuthenticate()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$AuthenticateResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorCode()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getErrorCode()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse$ErrorType;->getNumber()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasErrorMessage()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStoreBirthday()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStoreBirthday()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClientCommand()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getClientCommand()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientCommand;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasProfilingData()Z

    move-result v2

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getProfilingData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_7
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasClearUserData()Z

    move-result v2

    if-eqz v2, :cond_8

    const/16 v2, 0x9

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getClearUserData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClearUserDataResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_8
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamMetadata()Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0xa

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStreamMetadata()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesMetadataResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_9
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->hasStreamData()Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0xb

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getStreamData()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$GetUpdatesStreamingResponse;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_a
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ClientToServerResponse;->getMigratedDataTypeIdList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    goto :goto_0

    :cond_b
    return-void
.end method
