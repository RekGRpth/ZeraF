.class public final Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChromiumExtensionsActivity"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;


# instance fields
.field private bookmarkWritesSinceLastCommit_:I

.field private extensionId_:Ljava/lang/String;

.field private hasBookmarkWritesSinceLastCommit:Z

.field private hasExtensionId:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;-><init>(Z)V

    sput-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;->internalForceInit()V

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    invoke-direct {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->extensionId_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->bookmarkWritesSinceLastCommit_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;)V
    .locals 0
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->extensionId_:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->bookmarkWritesSinceLastCommit_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$7302(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasExtensionId:Z

    return p1
.end method

.method static synthetic access$7402(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->extensionId_:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$7502(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasBookmarkWritesSinceLastCommit:Z

    return p1
.end method

.method static synthetic access$7602(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;I)I
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
    .param p1    # I

    iput p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->bookmarkWritesSinceLastCommit_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;
    .locals 1

    # invokes: Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;->create()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;
    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;->access$7100()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;->mergeFrom(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBookmarkWritesSinceLastCommit()I
    .locals 1

    iget v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->bookmarkWritesSinceLastCommit_:I

    return v0
.end method

.method public getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;

    move-result-object v0

    return-object v0
.end method

.method public getExtensionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->extensionId_:Ljava/lang/String;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasExtensionId()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->getExtensionId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeStringSize(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasBookmarkWritesSinceLastCommit()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->getBookmarkWritesSinceLastCommit()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasBookmarkWritesSinceLastCommit()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasBookmarkWritesSinceLastCommit:Z

    return v0
.end method

.method public hasExtensionId()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasExtensionId:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;
    .locals 1

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasExtensionId()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->getExtensionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeString(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->hasBookmarkWritesSinceLastCommit()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ChromiumExtensionsActivity;->getBookmarkWritesSinceLastCommit()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    return-void
.end method
