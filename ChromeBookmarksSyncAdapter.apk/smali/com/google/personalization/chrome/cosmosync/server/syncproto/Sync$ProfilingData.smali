.class public final Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProfilingData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;


# instance fields
.field private fileDataReadTime_:J

.field private fileDataWriteTime_:J

.field private hasFileDataReadTime:Z

.field private hasFileDataWriteTime:Z

.field private hasMetaDataReadTime:Z

.field private hasMetaDataWriteTime:Z

.field private hasTotalRequestTime:Z

.field private hasUserLookupTime:Z

.field private memoizedSerializedSize:I

.field private metaDataReadTime_:J

.field private metaDataWriteTime_:J

.field private totalRequestTime_:J

.field private userLookupTime_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;-><init>(Z)V

    sput-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;->internalForceInit()V

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    invoke-direct {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataWriteTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataWriteTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->userLookupTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataReadTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataReadTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->totalRequestTime_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;)V
    .locals 0
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 2
    .param p1    # Z

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataWriteTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataWriteTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->userLookupTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataReadTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataReadTime_:J

    iput-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->totalRequestTime_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1002(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;J)J
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataReadTime_:J

    return-wide p1
.end method

.method static synthetic access$1102(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataReadTime:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;J)J
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataReadTime_:J

    return-wide p1
.end method

.method static synthetic access$1302(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasTotalRequestTime:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;J)J
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->totalRequestTime_:J

    return-wide p1
.end method

.method static synthetic access$302(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataWriteTime:Z

    return p1
.end method

.method static synthetic access$402(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;J)J
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataWriteTime_:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataWriteTime:Z

    return p1
.end method

.method static synthetic access$602(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;J)J
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataWriteTime_:J

    return-wide p1
.end method

.method static synthetic access$702(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasUserLookupTime:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;J)J
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->userLookupTime_:J

    return-wide p1
.end method

.method static synthetic access$902(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataReadTime:Z

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    return-object v0
.end method

.method private initFields()V
    .locals 0

    return-void
.end method

.method public static newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;
    .locals 1

    # invokes: Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;->create()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;
    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;->access$100()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;->mergeFrom(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;

    move-result-object v0

    return-object v0
.end method

.method public getFileDataReadTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataReadTime_:J

    return-wide v0
.end method

.method public getFileDataWriteTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->fileDataWriteTime_:J

    return-wide v0
.end method

.method public getMetaDataReadTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataReadTime_:J

    return-wide v0
.end method

.method public getMetaDataWriteTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->metaDataWriteTime_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 5

    iget v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataWriteTime()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getMetaDataWriteTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataWriteTime()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getFileDataWriteTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasUserLookupTime()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getUserLookupTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataReadTime()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getMetaDataReadTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataReadTime()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x5

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getFileDataReadTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasTotalRequestTime()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x6

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getTotalRequestTime()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public getTotalRequestTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->totalRequestTime_:J

    return-wide v0
.end method

.method public getUserLookupTime()J
    .locals 2

    iget-wide v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->userLookupTime_:J

    return-wide v0
.end method

.method public hasFileDataReadTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataReadTime:Z

    return v0
.end method

.method public hasFileDataWriteTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataWriteTime:Z

    return v0
.end method

.method public hasMetaDataReadTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataReadTime:Z

    return v0
.end method

.method public hasMetaDataWriteTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataWriteTime:Z

    return v0
.end method

.method public hasTotalRequestTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasTotalRequestTime:Z

    return v0
.end method

.method public hasUserLookupTime()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasUserLookupTime:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataWriteTime()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getMetaDataWriteTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataWriteTime()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getFileDataWriteTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasUserLookupTime()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getUserLookupTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasMetaDataReadTime()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getMetaDataReadTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasFileDataReadTime()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getFileDataReadTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->hasTotalRequestTime()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$ProfilingData;->getTotalRequestTime()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_5
    return-void
.end method
