.class public final Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;
.super Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;
.source "Sync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EntitySpecifics"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage",
        "<",
        "Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;",
        ">;"
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;


# instance fields
.field private encrypted_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

.field private hasEncrypted:Z

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;-><init>(Z)V

    sput-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync;->internalForceInit()V

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    invoke-direct {v0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->initFields()V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->memoizedSerializedSize:I

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->initFields()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;)V
    .locals 0
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$1;

    invoke-direct {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;-><init>()V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$1802(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;Z)Z
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->hasEncrypted:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->encrypted_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;
    .locals 0
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;
    .param p1    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    iput-object p1, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->encrypted_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;->getDefaultInstance()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->encrypted_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    return-void
.end method

.method public static newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;
    .locals 1

    # invokes: Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;->create()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;
    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;->access$1600()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;
    .locals 1
    .param p0    # Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;->mergeFrom(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;
    .locals 1

    sget-object v0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->defaultInstance:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->getDefaultInstanceForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;

    move-result-object v0

    return-object v0
.end method

.method public getEncrypted()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;
    .locals 1

    iget-object v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->encrypted_:Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    iget v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->hasEncrypted()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->getEncrypted()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->extensionsSerializedSize()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public hasEncrypted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->hasEncrypted:Z

    return v0
.end method

.method public final isInitialized()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->extensionsAreInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;
    .locals 1

    invoke-static {}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->newBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->newBuilderForType()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;
    .locals 1

    invoke-static {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->newBuilder(Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;)Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->toBuilder()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics$Builder;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->getSerializedSize()I

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->newExtensionWriter()Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->hasEncrypted()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/personalization/chrome/cosmosync/server/syncproto/Sync$EntitySpecifics;->getEncrypted()Lcom/google/personalization/chrome/cosmosync/server/syncproto/Encryption$EncryptedData;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1, p1}, Lcom/google/protobuf/GeneratedMessageLite$ExtendableMessage$ExtensionWriter;->writeUntil(ILcom/google/protobuf/CodedOutputStream;)V

    return-void
.end method
