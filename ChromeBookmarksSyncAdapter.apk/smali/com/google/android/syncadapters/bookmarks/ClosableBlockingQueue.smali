.class public Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;
.super Ljava/lang/Object;
.source "ClosableBlockingQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue$QueueClosedException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final mCapacity:I

.field private volatile mIsClosed:Z

.field private final mNotEmpty:Ljava/util/concurrent/locks/Condition;

.field private final mNotFull:Ljava/util/concurrent/locks/Condition;

.field final mQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mIsClosed:Z

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotEmpty:Ljava/util/concurrent/locks/Condition;

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotFull:Ljava/util/concurrent/locks/Condition;

    iput p1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mCapacity:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mIsClosed:Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotEmpty:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public kill()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mIsClosed:Z

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotFull:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotEmpty:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public put(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue$QueueClosedException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    iget v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mCapacity:I

    if-lt v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mIsClosed:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotFull:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotFull:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signal()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    :cond_1
    :try_start_3
    iget-boolean v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mIsClosed:Z

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue$QueueClosedException;

    invoke-direct {v1}, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue$QueueClosedException;-><init>()V

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotEmpty:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v1, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-void
.end method

.method public take()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mIsClosed:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotEmpty:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotEmpty:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->signal()V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2

    :cond_0
    :try_start_3
    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mNotFull:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->mQueueLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    goto :goto_1
.end method

.method public take(I)Ljava/util/ArrayList;
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<TE;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    if-lez p1, :cond_1

    move v3, p1

    :goto_0
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/syncadapters/bookmarks/ClosableBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    return-object v2

    :cond_1
    const/4 v3, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
