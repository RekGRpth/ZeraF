.class Lcom/mediatek/smsreg/SmsRegService$4;
.super Landroid/telephony/PhoneStateListener;
.source "SmsRegService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/smsreg/SmsRegService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/smsreg/SmsRegService;


# direct methods
.method constructor <init>(Lcom/mediatek/smsreg/SmsRegService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/smsreg/SmsRegService$4;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 4
    .param p1    # Landroid/telephony/ServiceState;

    const/4 v3, 0x1

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service state change sim gemini:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService$4;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v0}, Lcom/mediatek/smsreg/SmsRegService;->access$000(Lcom/mediatek/smsreg/SmsRegService;)V

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService$4;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v0}, Lcom/mediatek/smsreg/SmsRegService;->access$100(Lcom/mediatek/smsreg/SmsRegService;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService$4;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v0}, Lcom/mediatek/smsreg/SmsRegService;->access$100(Lcom/mediatek/smsreg/SmsRegService;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService$4;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v0}, Lcom/mediatek/smsreg/SmsRegService;->access$200(Lcom/mediatek/smsreg/SmsRegService;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService$4;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v0, v3}, Lcom/mediatek/smsreg/SmsRegService;->access$300(Lcom/mediatek/smsreg/SmsRegService;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "SmsReg/Service"

    const-string v1, "Sim2 do not need to register or sim2 is not the right operator"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
