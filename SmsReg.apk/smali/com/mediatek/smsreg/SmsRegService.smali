.class public Lcom/mediatek/smsreg/SmsRegService;
.super Landroid/app/Service;
.source "SmsRegService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;,
        Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;
    }
.end annotation


# static fields
.field private static final SMS_SENDING_RESULT_TAG:Ljava/lang/String; = "SMS_SENDING_RESULT"

.field private static final TAG:Ljava/lang/String; = "SmsReg/Service"


# instance fields
.field mHandler:Landroid/os/Handler;

.field private mIMSI:[Ljava/lang/String;

.field private mInfoPersistentor:Lcom/mediatek/smsreg/InfoPersistentor;

.field private mIsSendMsg:Ljava/lang/Boolean;

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPhoneStateListenerGemini:Landroid/telephony/PhoneStateListener;

.field private mReadyGemini:[Ljava/lang/Boolean;

.field private mSavedIMSI:Ljava/lang/String;

.field private final mSearchNetDelay:J

.field private mServiceAlive:Ljava/lang/Boolean;

.field private mSimIMSI:Ljava/lang/String;

.field private mSimStateReceiver:Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

.field private mSlotGemini:[Ljava/lang/Boolean;

.field private mSmsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

.field private mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

.field private mSmsReceivedReceiver:Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

.field private mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

.field private mTimer:Ljava/util/Timer;

.field private mTimerTask:Ljava/util/TimerTask;

.field private mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;


# direct methods
.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIsSendMsg:Ljava/lang/Boolean;

    new-array v0, v4, [Ljava/lang/String;

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimIMSI:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mServiceAlive:Ljava/lang/Boolean;

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimer:Ljava/util/Timer;

    const-wide/32 v0, 0x15f90

    iput-wide v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSearchNetDelay:J

    new-array v0, v4, [Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSlotGemini:[Ljava/lang/Boolean;

    new-array v0, v4, [Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mReadyGemini:[Ljava/lang/Boolean;

    new-instance v0, Lcom/mediatek/smsreg/SmsRegService$2;

    invoke-direct {v0, p0}, Lcom/mediatek/smsreg/SmsRegService$2;-><init>(Lcom/mediatek/smsreg/SmsRegService;)V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/mediatek/smsreg/SmsRegService$3;

    invoke-direct {v0, p0}, Lcom/mediatek/smsreg/SmsRegService$3;-><init>(Lcom/mediatek/smsreg/SmsRegService;)V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    new-instance v0, Lcom/mediatek/smsreg/SmsRegService$4;

    invoke-direct {v0, p0}, Lcom/mediatek/smsreg/SmsRegService$4;-><init>(Lcom/mediatek/smsreg/SmsRegService;)V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListenerGemini:Landroid/telephony/PhoneStateListener;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/smsreg/SmsRegService;)V
    .locals 0
    .param p0    # Lcom/mediatek/smsreg/SmsRegService;

    invoke-direct {p0}, Lcom/mediatek/smsreg/SmsRegService;->getSimCardMatchCustomizedGemini()V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/smsreg/SmsRegService;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/mediatek/smsreg/SmsRegService;

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mediatek/smsreg/SmsRegService;)Ljava/lang/Boolean;
    .locals 1
    .param p0    # Lcom/mediatek/smsreg/SmsRegService;

    invoke-direct {p0}, Lcom/mediatek/smsreg/SmsRegService;->isNeedRegisterGemini()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/mediatek/smsreg/SmsRegService;I)V
    .locals 0
    .param p0    # Lcom/mediatek/smsreg/SmsRegService;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/smsreg/SmsRegService;->sendRegisterMessageGemini(I)V

    return-void
.end method

.method private getSendPendingIntent(I)Landroid/app/PendingIntent;
    .locals 6
    .param p1    # I

    const-string v3, "SmsReg/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get Pending Intent begin, simId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.DM_REGISTER_SMS_RECEIVED"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "SmsReg/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "put extra SimID, SimID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SimID"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v3, p1}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "SmsReg/Service"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "put extra mIMSI, mIMSI = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "mIMSI"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v3, "SmsReg/Service"

    const-string v4, "get Pending Intent end"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2
.end method

.method private getSimCardMatchCustomized()V
    .locals 10

    const/4 v9, 0x0

    iget-object v6, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v6}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getOperatorName()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v6}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getNetworkNumber()[Ljava/lang/String;

    move-result-object v4

    const-string v6, "SmsReg/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "the operator Id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", operatorNumber.length = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v6, v9}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimState(I)I

    move-result v5

    const/4 v6, 0x5

    if-ne v6, v5, :cond_5

    iget-object v6, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v6, v9}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimOperator(I)Ljava/lang/String;

    move-result-object v1

    const-string v6, "SmsReg/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "there is a sim card is ready the operator is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    const-string v6, "SmsReg/Service"

    const-string v7, "operator is null, do nothing. "

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v2, 0x0

    :goto_1
    array-length v6, v4

    if-ge v2, v6, :cond_3

    aget-object v0, v4, v2

    if-eqz v0, :cond_4

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "SmsReg/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "the ready sim card operator is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget-object v8, v4, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v6, v9}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimIMSI:Ljava/lang/String;

    const-string v6, "SmsReg/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "the current imsi is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimIMSI:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    array-length v6, v4

    if-lt v2, v6, :cond_1

    const-string v6, "SmsReg/Service"

    const-string v7, "There is no sim card operator is matched currentoperator number."

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    const-string v6, "SmsReg/Service"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Sim state is not ready, state = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getSimCardMatchCustomizedGemini()V
    .locals 10

    iget-object v7, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v7}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getOperatorName()Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v7}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getNetworkNumber()[Ljava/lang/String;

    move-result-object v5

    const-string v7, "SmsReg/Service"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "the operator Id = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", operatorNumber.length = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v5

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    sget-object v7, Lcom/mediatek/smsreg/SmsRegConst;->GEMSIM:[I

    array-length v7, v7

    if-ge v2, v7, :cond_4

    iget-object v7, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    sget-object v8, Lcom/mediatek/smsreg/SmsRegConst;->GEMSIM:[I

    aget v8, v8, v2

    invoke-virtual {v7, v8}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimState(I)I

    move-result v6

    const/4 v7, 0x5

    if-ne v7, v6, :cond_1

    iget-object v7, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    sget-object v8, Lcom/mediatek/smsreg/SmsRegConst;->GEMSIM:[I

    aget v8, v8, v2

    invoke-virtual {v7, v8}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimOperator(I)Ljava/lang/String;

    move-result-object v1

    const-string v7, "SmsReg/Service"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "there is a sim card is ready the operator is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    const-string v7, "SmsReg/Service"

    const-string v8, "operator is null, continue next one. "

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_2
    array-length v7, v5

    if-ge v3, v7, :cond_1

    aget-object v0, v5, v3

    const-string v7, "SmsReg/Service"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "the phone is for the operator[ "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    iget-object v8, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    sget-object v9, Lcom/mediatek/smsreg/SmsRegConst;->GEMSIM:[I

    aget v9, v9, v2

    invoke-virtual {v8, v9}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSubscriberId(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    const-string v7, "SmsReg/Service"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "current mIMSI["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method private isNeedRegister()Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mInfoPersistentor:Lcom/mediatek/smsreg/InfoPersistentor;

    invoke-virtual {v0}, Lcom/mediatek/smsreg/InfoPersistentor;->getSavedIMSI()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "the mSavedIMSI = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The saved mIMSI =["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The current mIMSI =["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimIMSI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimIMSI:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimIMSI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SmsReg/Service"

    const-string v1, "The SIM card and device have rigistered"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/smsreg/SmsRegService;->stopService()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "SmsReg/Service"

    const-string v1, "The sim card in this phone is not registered, need register"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private isNeedRegisterGemini()Ljava/lang/Boolean;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mInfoPersistentor:Lcom/mediatek/smsreg/InfoPersistentor;

    invoke-virtual {v0}, Lcom/mediatek/smsreg/InfoPersistentor;->getSavedIMSI()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "the mSavedIMSI = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The saved mIMSI =["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The current mIMSI0 =["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SmsReg/Service"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The current mIMSI1 =["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v2, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v0, v0, v3

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SmsReg/Service"

    const-string v1, "The SIM1 have registered already."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/smsreg/SmsRegService;->stopService()V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v0, v0, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    aget-object v0, v0, v4

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService;->mSavedIMSI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SmsReg/Service"

    const-string v1, "The SIM2 have registered already."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/smsreg/SmsRegService;->stopService()V

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "SmsReg/Service"

    const-string v1, "The sim card in this phone is not registered, need register"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private sendRegisterMessage()V
    .locals 15

    const/4 v2, 0x0

    const/4 v8, 0x0

    const-string v0, "SmsReg/Service"

    const-string v3, "send register message begin..."

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v0, v8}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimCountryIso(I)Ljava/lang/String;

    move-result-object v12

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-virtual {v0, v8}, Lcom/mediatek/telephony/TelephonyManagerEx;->getNetworkCountryIso(I)Ljava/lang/String;

    move-result-object v9

    const-string v0, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "simCountryIso = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " networkIso= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v12, :cond_1

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-virtual {v0, v3, v8}, Lcom/mediatek/smsreg/SmsBuilder;->getSmsContent(Lcom/mediatek/smsreg/ConfigInfoGenerator;I)Ljava/lang/String;

    move-result-object v13

    const-string v0, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SmsRegMsg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v13, :cond_5

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v0}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSmsNumber()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v0}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSmsPort()Ljava/lang/Short;

    move-result-object v11

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v0}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSrcPort()Ljava/lang/Short;

    move-result-object v14

    const-string v0, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Operator\'s sms number = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Operator\'s sms port = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Src port = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v0}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getOperatorName()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

    if-eqz v0, :cond_4

    const-string v0, "cu"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cmcc"

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIsSendMsg:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v8}, Lcom/mediatek/smsreg/SmsRegService;->getSendPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v6

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

    invoke-virtual {v11}, Ljava/lang/Short;->shortValue()S

    move-result v3

    invoke-virtual {v14}, Ljava/lang/Short;->shortValue()S

    move-result v4

    invoke-virtual {v13}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move-object v7, v2

    invoke-virtual/range {v0 .. v8}, Lcom/mediatek/telephony/SmsManagerEx;->sendDataMessage(Ljava/lang/String;Ljava/lang/String;SS[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;I)V

    const-string v0, "SmsReg/Service"

    const-string v2, "send register message end, RegMsg is send out!"

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mIsSendMsg:Ljava/lang/Boolean;

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "SmsReg/Service"

    const-string v2, "RegMsg has been sent already. "

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v0, "SmsReg/Service"

    const-string v2, "RegMsg is not send, it is not the operator cu or cmcc"

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v0, "SmsReg/Service"

    const-string v2, "Send RegMsg failed, mSmsManager is null"

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const-string v0, "SmsReg/Service"

    const-string v2, "Send RegMsg failed, The Sms Register message is null"

    invoke-static {v0, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mServiceAlive:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method private sendRegisterMessageGemini(I)V
    .locals 16
    .param p1    # I

    const-string v1, "SmsReg/Service"

    const-string v3, "send register message gemini begin..."

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/mediatek/telephony/TelephonyManagerEx;->getSimCountryIso(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/mediatek/telephony/TelephonyManagerEx;->getNetworkCountryIso(I)Ljava/lang/String;

    move-result-object v10

    const-string v1, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "simCountryIso = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " networkIso= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v13, :cond_5

    invoke-virtual {v13, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mSmsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    move/from16 v0, p1

    invoke-virtual {v1, v3, v0}, Lcom/mediatek/smsreg/SmsBuilder;->getSmsContent(Lcom/mediatek/smsreg/ConfigInfoGenerator;I)Ljava/lang/String;

    move-result-object v14

    const-string v1, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SmsRegMsg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v1}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSmsNumber()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v1}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSmsPort()Ljava/lang/Short;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v1}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSrcPort()Ljava/lang/Short;

    move-result-object v15

    const-string v1, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Operator\'s sms number = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Operator\'s sms port = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SmsReg/Service"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Src port = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v1}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getOperatorName()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

    if-eqz v1, :cond_3

    const-string v1, "cu"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "cmcc"

    invoke-virtual {v11, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mIsSendMsg:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct/range {p0 .. p1}, Lcom/mediatek/smsreg/SmsRegService;->getSendPendingIntent(I)Landroid/app/PendingIntent;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

    const/4 v3, 0x0

    invoke-virtual {v12}, Ljava/lang/Short;->shortValue()S

    move-result v4

    invoke-virtual {v15}, Ljava/lang/Short;->shortValue()S

    move-result v5

    invoke-virtual {v14}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    const/4 v8, 0x0

    move/from16 v9, p1

    invoke-virtual/range {v1 .. v9}, Lcom/mediatek/telephony/SmsManagerEx;->sendDataMessage(Ljava/lang/String;Ljava/lang/String;SS[BLandroid/app/PendingIntent;Landroid/app/PendingIntent;I)V

    const-string v1, "SmsReg/Service"

    const-string v3, "send register message end, RegMsg gemini is send out!"

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mIsSendMsg:Ljava/lang/Boolean;

    :goto_0
    return-void

    :cond_1
    const-string v1, "SmsReg/Service"

    const-string v3, "RegMsg gemini has been sent already. "

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v1, "SmsReg/Service"

    const-string v3, "RegMsg is not send, it is not the operator cu or cmcc"

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v1, "SmsReg/Service"

    const-string v3, "Send RegMsg failed, mSmsManager is null"

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v1, "SmsReg/Service"

    const-string v3, "Send RegMsg failed, The Sms Register message is null"

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/mediatek/smsreg/SmsRegService;->mServiceAlive:Ljava/lang/Boolean;

    goto :goto_0

    :cond_5
    const-string v1, "SmsReg/Service"

    const-string v3, "SimCountryIso is not equals with NetworkCountryIso, do nothing"

    invoke-static {v1, v3}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-static {}, Lcom/mediatek/telephony/SmsManagerEx;->getDefault()Lcom/mediatek/telephony/SmsManagerEx;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

    const-string v0, "SmsReg/Service"

    const-string v1, "SmsRegService onCreate."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "/system/etc/dm/smsSelfRegConfig.xml"

    invoke-static {v0}, Lcom/mediatek/smsreg/XMLGenerator;->getInstance(Ljava/lang/String;)Lcom/mediatek/smsreg/ConfigInfoGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    if-nez v0, :cond_0

    const-string v0, "SmsReg/Service"

    const-string v1, "Init XMLGenerator error!"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/mediatek/smsreg/SmsBuilder;

    invoke-direct {v0, p0}, Lcom/mediatek/smsreg/SmsBuilder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    new-instance v0, Lcom/mediatek/telephony/TelephonyManagerEx;

    invoke-direct {v0, p0}, Lcom/mediatek/telephony/TelephonyManagerEx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    if-nez v0, :cond_1

    const-string v0, "SmsReg/Service"

    const-string v1, "TelephonyManager service is not exist!"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/mediatek/smsreg/InfoPersistentor;

    invoke-direct {v0}, Lcom/mediatek/smsreg/InfoPersistentor;-><init>()V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mInfoPersistentor:Lcom/mediatek/smsreg/InfoPersistentor;

    goto :goto_0
.end method

.method public onDestory()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "SmsReg/Service"

    const-string v1, "SmsRegService destory"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsManager:Lcom/mediatek/telephony/SmsManagerEx;

    iput-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iput-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mInfoPersistentor:Lcom/mediatek/smsreg/InfoPersistentor;

    iput-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    iput-object v2, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 14
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const-string v10, "SmsReg/Service"

    const-string v11, "SmsReg service on start"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super/range {p0 .. p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mXmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    if-nez v10, :cond_1

    const-string v10, "SmsReg/Service"

    const-string v11, "XMLGenerator instance init error!"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const-string v10, "SmsReg/Service"

    const-string v11, "intent is null!"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v10, "SmsReg/Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SmsReg service onStart, action = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_3

    const-string v10, "SmsReg/Service"

    const-string v11, "intent action is null!"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v10, "BOOTCOMPLETED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    new-instance v10, Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

    invoke-direct {v10}, Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;-><init>()V

    iput-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimStateReceiver:Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimStateReceiver:Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

    new-instance v11, Landroid/content/IntentFilter;

    const-string v12, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v11, v12}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v10, v11}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v10, Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

    invoke-direct {v10}, Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;-><init>()V

    iput-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsReceivedReceiver:Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsReceivedReceiver:Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

    new-instance v11, Landroid/content/IntentFilter;

    const-string v12, "android.intent.action.DM_REGISTER_SMS_RECEIVED"

    invoke-direct {v11, v12}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v10, v11}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v10, Lcom/mediatek/smsreg/SmsRegService$1;

    invoke-direct {v10, p0}, Lcom/mediatek/smsreg/SmsRegService$1;-><init>(Lcom/mediatek/smsreg/SmsRegService;)V

    iput-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimerTask:Ljava/util/TimerTask;

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimer:Ljava/util/Timer;

    iget-object v11, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimerTask:Ljava/util/TimerTask;

    const-wide/32 v12, 0x15f90

    invoke-virtual {v10, v11, v12, v13}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_4
    :goto_1
    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mServiceAlive:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "SmsReg/Service"

    const-string v11, "mServiceAlive is false"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mediatek/smsreg/SmsRegService;->stopService()V

    goto/16 :goto_0

    :cond_5
    const-string v10, "SIM_STATE_CHANGED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    const-string v10, "ss"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "LOADED"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_6

    const-string v10, "SmsReg/Service"

    const-string v11, "sim state is not loaded"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const-string v10, "phone"

    invoke-static {v10}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v6

    const/4 v2, 0x0

    :goto_2
    sget-object v10, Lcom/mediatek/smsreg/SmsRegConst;->GEMSIM:[I

    array-length v10, v10

    if-ge v2, v10, :cond_8

    :try_start_0
    invoke-interface {v6, v2}, Lcom/android/internal/telephony/ITelephony;->isSimInsert(I)Z

    move-result v10

    if-nez v10, :cond_7

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSlotGemini:[Ljava/lang/Boolean;

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v10, v2

    const-string v10, "SmsReg/Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Slot["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] has no sim card."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_7
    const-string v10, "SmsReg/Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Slot["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] has sim card."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    const-string v10, "SmsReg/Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "* RemoteException:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_0

    :cond_8
    const-string v10, "simId"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const/4 v10, -0x1

    if-eq v3, v10, :cond_0

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mReadyGemini:[Ljava/lang/Boolean;

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    aput-object v11, v10, v3

    const-string v10, "SmsReg/Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Slot["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] is ready."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSlotGemini:[Ljava/lang/Boolean;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_9

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mReadyGemini:[Ljava/lang/Boolean;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_b

    :cond_9
    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mSlotGemini:[Ljava/lang/Boolean;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_a

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mReadyGemini:[Ljava/lang/Boolean;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_b

    :cond_a
    const-string v10, "SmsReg/Service"

    const-string v11, "-------- Both slot checked finished --------"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "SmsReg/Service"

    const-string v11, "sim state is loaded"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "SmsReg/Service"

    const-string v11, "Regist service state listener for sim1."

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v11, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13}, Lcom/mediatek/telephony/TelephonyManagerEx;->listen(Landroid/telephony/PhoneStateListener;II)V

    const-string v10, "SmsReg/Service"

    const-string v11, "Regist service state listener gemini for sim2."

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v11, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListenerGemini:Landroid/telephony/PhoneStateListener;

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual {v10, v11, v12, v13}, Lcom/mediatek/telephony/TelephonyManagerEx;->listen(Landroid/telephony/PhoneStateListener;II)V

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mIsSendMsg:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_e

    invoke-direct {p0}, Lcom/mediatek/smsreg/SmsRegService;->getSimCardMatchCustomizedGemini()V

    const/4 v8, -0x1

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    const/4 v11, 0x0

    aget-object v10, v10, v11

    if-eqz v10, :cond_c

    const/4 v8, 0x0

    :goto_4
    invoke-direct {p0}, Lcom/mediatek/smsreg/SmsRegService;->isNeedRegisterGemini()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-direct {p0, v8}, Lcom/mediatek/smsreg/SmsRegService;->sendRegisterMessageGemini(I)V

    goto/16 :goto_1

    :cond_b
    const-string v10, "SmsReg/Service"

    const-string v11, "-------- Wait for another sim card ready --------"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_c
    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mIMSI:[Ljava/lang/String;

    const/4 v11, 0x1

    aget-object v10, v10, v11

    if-eqz v10, :cond_d

    const/4 v8, 0x1

    goto :goto_4

    :cond_d
    const-string v10, "SmsReg/Service"

    const-string v11, "No sim card or the sim card is not the customized operator"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_e
    const-string v10, "SmsReg/Service"

    const-string v11, "the register message has been sent"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_f
    const-string v10, "REGISTER_SMS_RECEIVED"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    const-string v10, "SmsReg/Service"

    const-string v11, "broadcast REGISTER_SMS_RECEIVED has received"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "SMS_SENDING_RESULT"

    const/4 v11, -0x1

    invoke-virtual {p1, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/4 v10, -0x1

    if-ne v7, v10, :cond_11

    const-string v10, "SmsReg/Service"

    const-string v11, "Save the IMSI"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "mIMSI"

    invoke-virtual {p1, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v10, "SmsReg/Service"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "The IMSI to save is  = ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_10

    const-string v10, ""

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_10

    new-instance v5, Lcom/mediatek/smsreg/InfoPersistentor;

    invoke-direct {v5}, Lcom/mediatek/smsreg/InfoPersistentor;-><init>()V

    invoke-virtual {v5, v4}, Lcom/mediatek/smsreg/InfoPersistentor;->setSavedIMSI(Ljava/lang/String;)V

    :cond_10
    :goto_5
    invoke-virtual {p0}, Lcom/mediatek/smsreg/SmsRegService;->stopService()V

    goto/16 :goto_1

    :cond_11
    const-string v10, "SmsReg/Service"

    const-string v11, "Sms sending failed."

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :cond_12
    const-string v10, "SmsReg/Service"

    const-string v11, "Get the wrong intent"

    invoke-static {v10, v11}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iput-object v10, p0, Lcom/mediatek/smsreg/SmsRegService;->mServiceAlive:Ljava/lang/Boolean;

    goto/16 :goto_1
.end method

.method public onStop()V
    .locals 2

    const-string v0, "SmsReg/Service"

    const-string v1, "SmsRegService stop"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected stopService()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const-string v0, "SmsReg/Service"

    const-string v1, "stop service."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_0

    const-string v0, "SmsReg/Service"

    const-string v1, "unRegist service state listener for sim1."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v4, v4}, Lcom/mediatek/telephony/TelephonyManagerEx;->listen(Landroid/telephony/PhoneStateListener;II)V

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListenerGemini:Landroid/telephony/PhoneStateListener;

    if-eqz v0, :cond_1

    const-string v0, "SmsReg/Service"

    const-string v1, "unRegist service state listener gemini for sim2."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTelephonyManager:Lcom/mediatek/telephony/TelephonyManagerEx;

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListenerGemini:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v4, v2}, Lcom/mediatek/telephony/TelephonyManagerEx;->listen(Landroid/telephony/PhoneStateListener;II)V

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mPhoneStateListenerGemini:Landroid/telephony/PhoneStateListener;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimStateReceiver:Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

    if-eqz v0, :cond_2

    const-string v0, "SmsReg/Service"

    const-string v1, "unRegist sim state receiver."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimStateReceiver:Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mSimStateReceiver:Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsReceivedReceiver:Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

    if-eqz v0, :cond_3

    const-string v0, "SmsReg/Service"

    const-string v1, "unRegist smsReceived receiver."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsReceivedReceiver:Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mSmsReceivedReceiver:Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;

    :cond_3
    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_4

    const-string v0, "SmsReg/Service"

    const-string v1, "cancel timer."

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimerTask:Ljava/util/TimerTask;

    if-eqz v0, :cond_4

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegService;->mTimerTask:Ljava/util/TimerTask;

    :cond_4
    invoke-virtual {p0}, Landroid/app/Service;->stopSelf()V

    return-void
.end method
