.class Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;
.super Ljava/lang/Object;
.source "CallNotifier.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/phone/CallNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AccelerometerSensorEventListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/phone/CallNotifier;


# direct methods
.method private constructor <init>(Lcom/android/phone/CallNotifier;)V
    .locals 0

    iput-object p1, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/phone/CallNotifier;Lcom/android/phone/CallNotifier$1;)V
    .locals 0
    .param p1    # Lcom/android/phone/CallNotifier;
    .param p2    # Lcom/android/phone/CallNotifier$1;

    invoke-direct {p0, p1}, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;-><init>(Lcom/android/phone/CallNotifier;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1    # Landroid/hardware/Sensor;
    .param p2    # I

    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 14
    .param p1    # Landroid/hardware/SensorEvent;

    iget-object v10, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v11, 0x0

    aget v10, v10, v11

    float-to-double v4, v10

    iget-object v10, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v11, 0x1

    aget v10, v10, v11

    float-to-double v6, v10

    iget-object v10, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v11, 0x2

    aget v10, v10, v11

    float-to-double v8, v10

    mul-double v10, v4, v4

    mul-double v12, v6, v6

    add-double/2addr v10, v12

    mul-double v12, v8, v8

    add-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    div-double v10, v8, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->acos(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    const-wide v10, 0x4060e00000000000L

    cmpl-double v10, v0, v10

    if-lez v10, :cond_3

    const/4 v2, 0x1

    :goto_0
    const-wide v10, 0x4046800000000000L

    cmpg-double v10, v0, v10

    if-gez v10, :cond_4

    const/4 v3, 0x1

    :goto_1
    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "degrees="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isFaceUp="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", isFaceDown="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", v=("

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "), mFaceState="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    # getter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v12}, Lcom/android/phone/CallNotifier;->access$000(Lcom/android/phone/CallNotifier;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    # invokes: Lcom/android/phone/CallNotifier;->log(Ljava/lang/String;)V
    invoke-static {v10, v11}, Lcom/android/phone/CallNotifier;->access$100(Lcom/android/phone/CallNotifier;Ljava/lang/String;)V

    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    # getter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v10}, Lcom/android/phone/CallNotifier;->access$000(Lcom/android/phone/CallNotifier;)I

    move-result v10

    if-nez v10, :cond_0

    if-eqz v2, :cond_5

    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    const/4 v11, 0x2

    # setter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v10, v11}, Lcom/android/phone/CallNotifier;->access$002(Lcom/android/phone/CallNotifier;I)I

    :cond_0
    :goto_2
    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    # getter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v10}, Lcom/android/phone/CallNotifier;->access$000(Lcom/android/phone/CallNotifier;)I

    move-result v10

    const/4 v11, 0x2

    if-ne v10, v11, :cond_1

    if-eqz v3, :cond_1

    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    const/4 v11, 0x0

    # setter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v10, v11}, Lcom/android/phone/CallNotifier;->access$002(Lcom/android/phone/CallNotifier;I)I

    :cond_1
    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    invoke-virtual {v10}, Lcom/android/phone/CallNotifier;->isRinging()Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    # getter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v10}, Lcom/android/phone/CallNotifier;->access$000(Lcom/android/phone/CallNotifier;)I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    if-eqz v2, :cond_2

    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    invoke-virtual {v10}, Lcom/android/phone/CallNotifier;->silenceRinger()V

    :cond_2
    return-void

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_5
    if-eqz v3, :cond_0

    iget-object v10, p0, Lcom/android/phone/CallNotifier$AccelerometerSensorEventListener;->this$0:Lcom/android/phone/CallNotifier;

    const/4 v11, 0x1

    # setter for: Lcom/android/phone/CallNotifier;->mFaceState:I
    invoke-static {v10, v11}, Lcom/android/phone/CallNotifier;->access$002(Lcom/android/phone/CallNotifier;I)I

    goto :goto_2
.end method
