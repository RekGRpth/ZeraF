.class Lcom/mediatek/settings/VoiceMailSetting$3;
.super Landroid/os/Handler;
.source "VoiceMailSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/VoiceMailSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/settings/VoiceMailSetting;


# direct methods
.method constructor <init>(Lcom/mediatek/settings/VoiceMailSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/AsyncResult;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    iget-boolean v2, v2, Lcom/mediatek/settings/VoiceMailSetting;->mVMChangeCompletedSuccesfully:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    iget-object v2, v2, Lcom/mediatek/settings/VoiceMailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    if-eqz v2, :cond_4

    :cond_0
    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    iget-boolean v2, v2, Lcom/mediatek/settings/VoiceMailSetting;->mFwdChangesRequireRollback:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$400(Lcom/mediatek/settings/VoiceMailSetting;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    const-string v2, "All VM reverts done"

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$200(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    const/16 v3, 0x25b

    invoke-static {v2, v3}, Lcom/mediatek/settings/VoiceMailSetting;->access$600(Lcom/mediatek/settings/VoiceMailSetting;I)V

    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    invoke-virtual {v2}, Lcom/mediatek/settings/VoiceMailSetting;->onRevertDone()V

    :cond_2
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    iput-object v1, v2, Lcom/mediatek/settings/VoiceMailSetting;->mVoicemailChangeResult:Landroid/os/AsyncResult;

    const-string v2, "VM revert complete msg"

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$200(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/mediatek/settings/VoiceMailSetting$3;->this$0:Lcom/mediatek/settings/VoiceMailSetting;

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$300(Lcom/mediatek/settings/VoiceMailSetting;)Ljava/util/Map;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in reverting fwd# "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v1, Landroid/os/AsyncResult;->exception:Ljava/lang/Throwable;

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$200(Ljava/lang/String;)V

    :goto_2
    const-string v2, "FWD revert complete msg "

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$200(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Success in reverting fwd# "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/settings/VoiceMailSetting;->access$200(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
