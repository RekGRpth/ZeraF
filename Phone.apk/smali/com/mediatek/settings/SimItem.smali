.class public Lcom/mediatek/settings/SimItem;
.super Ljava/lang/Object;
.source "SimItem.java"


# instance fields
.field public mIsSim:Z

.field public mSiminfo:Landroid/provider/Telephony$SIMInfo;

.field public mState:I


# direct methods
.method public constructor <init>(Landroid/provider/Telephony$SIMInfo;)V
    .locals 2
    .param p1    # Landroid/provider/Telephony$SIMInfo;

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/settings/SimItem;->mIsSim:Z

    const/4 v0, 0x5

    iput v0, p0, Lcom/mediatek/settings/SimItem;->mState:I

    iput-object p1, p0, Lcom/mediatek/settings/SimItem;->mSiminfo:Landroid/provider/Telephony$SIMInfo;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/settings/SimItem;->mIsSim:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/mediatek/settings/SimItem;->mIsSim:Z

    goto :goto_0
.end method
