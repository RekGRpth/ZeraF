.class public Lcom/mediatek/settings/CheckSimPreference;
.super Landroid/preference/CheckBoxPreference;
.source "CheckSimPreference.java"


# static fields
.field private static final DBG:Z = true

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/SimPreference"


# instance fields
.field private mSimColor:I

.field private mSimIconNumber:Ljava/lang/String;

.field private mSimName:Ljava/lang/String;

.field private mSimNumber:Ljava/lang/String;

.field private mSimSlot:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/mediatek/settings/CheckSimPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private getSimStatusImge(I)I
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    const v0, 0x2020112

    goto :goto_0

    :pswitch_2
    const v0, 0x20200ff

    goto :goto_0

    :pswitch_3
    const v0, 0x20200f8

    goto :goto_0

    :pswitch_4
    const v0, 0x2020119

    goto :goto_0

    :pswitch_5
    const v0, 0x2020117

    goto :goto_0

    :pswitch_6
    const v0, 0x20200ef

    goto :goto_0

    :pswitch_7
    const v0, 0x2020118

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public onBindView(Landroid/view/View;)V
    .locals 11
    .param p1    # Landroid/view/View;

    const/16 v10, 0x8

    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->onBindView(Landroid/view/View;)V

    const v8, 0x7f0800cc

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    :try_start_0
    const-string v8, "phone"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimSlot:I

    invoke-interface {v1, v8}, Lcom/android/internal/telephony/ITelephony;->getSimIndicatorStateGemini(I)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/mediatek/settings/CheckSimPreference;->getSimStatusImge(I)I

    move-result v4

    const/4 v8, -0x1

    if-ne v4, v8, :cond_5

    const/16 v8, 0x8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const v8, 0x7f0800cb

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_1

    iget v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimColor:I

    if-ltz v8, :cond_6

    iget v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimColor:I

    const/4 v9, 0x3

    if-gt v8, v9, :cond_6

    sget-object v8, Landroid/provider/Telephony;->SIMBackgroundDarkRes:[I

    iget v9, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimColor:I

    aget v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    :goto_1
    const v8, 0x7f0800d0

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    if-eqz v6, :cond_2

    iget-object v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimNumber:Ljava/lang/String;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimNumber:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimNumber:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_2
    const v8, 0x7f0800cf

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimName:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const v8, 0x7f0800cd

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    if-eqz v5, :cond_4

    iget-object v8, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimIconNumber:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    return-void

    :cond_5
    const/4 v8, 0x0

    :try_start_1
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_6
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_7
    invoke-virtual {v6, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public setSimColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimColor:I

    return-void
.end method

.method public setSimIconNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimIconNumber:Ljava/lang/String;

    return-void
.end method

.method public setSimName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimName:Ljava/lang/String;

    return-void
.end method

.method public setSimNumber(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimNumber:Ljava/lang/String;

    return-void
.end method

.method public setSimSlot(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CheckSimPreference;->mSimSlot:I

    return-void
.end method
