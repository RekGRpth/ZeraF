.class public Lcom/mediatek/settings/CellBroadcastChannel;
.super Ljava/lang/Object;
.source "CellBroadcastChannel.java"


# instance fields
.field private mChannelId:I

.field private mChannelName:Ljava/lang/String;

.field private mChannelState:Z

.field private mKeyId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Z)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mKeyId:I

    iput p2, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelId:I

    iput-object p3, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelName:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelState:Z

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelId:I

    iput-object p2, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelName:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelState:Z

    return-void
.end method


# virtual methods
.method public getChannelId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelId:I

    return v0
.end method

.method public getChannelName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelName:Ljava/lang/String;

    return-object v0
.end method

.method public getChannelState()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelState:Z

    return v0
.end method

.method public getKeyId()I
    .locals 1

    iget v0, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mKeyId:I

    return v0
.end method

.method public setChannelId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelId:I

    return-void
.end method

.method public setChannelName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelName:Ljava/lang/String;

    return-void
.end method

.method public setChannelState(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mChannelState:Z

    return-void
.end method

.method public setKeyId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CellBroadcastChannel;->mKeyId:I

    return-void
.end method
