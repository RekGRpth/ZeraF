.class public Lcom/mediatek/settings/CallBarring;
.super Lcom/android/phone/TimeConsumingPreferenceActivity;
.source "CallBarring.java"

# interfaces
.implements Lcom/mediatek/settings/CallBarringInterface;


# static fields
.field private static final BUTTON_ALL_INCOMING_EXCEPT:Ljava/lang/String; = "all_incoming_except_key"

.field private static final BUTTON_ALL_INCOMING_KEY:Ljava/lang/String; = "all_incoming_key"

.field private static final BUTTON_ALL_OUTING_KEY:Ljava/lang/String; = "all_outing_international_key"

.field private static final BUTTON_CALL_BARRING_KEY:Ljava/lang/String; = "all_outing_key"

.field private static final BUTTON_CHANGE_PASSWORD_KEY:Ljava/lang/String; = "change_password_key"

.field private static final BUTTON_DEACTIVATE_KEY:Ljava/lang/String; = "deactivate_all_key"

.field private static final BUTTON_OUT_INTERNATIONAL_EXCEPT:Ljava/lang/String; = "all_outing_except_key"

.field private static final DBG:Z = true

.field public static final DEFAULT_SIM:I = 0x2

.field private static final LOG_TAG:Ljava/lang/String; = "Settings/CallBarring"


# instance fields
.field private mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

.field private mCallCancel:Lcom/mediatek/settings/CallBarringResetPreference;

.field private mCallChangePassword:Lcom/mediatek/settings/CallBarringChangePassword;

.field private mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

.field private mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

.field private mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

.field private mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

.field private mCheckedPreferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mErrorState:I

.field private mFirstResume:Z

.field private mInitIndex:I

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPassword:Ljava/lang/String;

.field private mPreferences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/preference/Preference;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResetIndex:I

.field private mSimId:I

.field private mVtSetting:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCheckedPreferences:Ljava/util/ArrayList;

    iput v1, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    iput v1, p0, Lcom/mediatek/settings/CallBarring;->mResetIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/settings/CallBarring;->mPassword:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/settings/CallBarring;->mErrorState:I

    iput-boolean v1, p0, Lcom/mediatek/settings/CallBarring;->mFirstResume:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    iput-boolean v1, p0, Lcom/mediatek/settings/CallBarring;->mVtSetting:Z

    new-instance v0, Lcom/mediatek/settings/CallBarring$1;

    invoke-direct {v0, p0}, Lcom/mediatek/settings/CallBarring$1;-><init>(Lcom/mediatek/settings/CallBarring;)V

    iput-object v0, p0, Lcom/mediatek/settings/CallBarring;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/settings/CallBarring;)I
    .locals 1
    .param p0    # Lcom/mediatek/settings/CallBarring;

    iget v0, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    return v0
.end method

.method private doGetCallState(Landroid/preference/Preference;)V
    .locals 2
    .param p1    # Landroid/preference/Preference;

    instance-of v0, p1, Lcom/mediatek/settings/CallBarringBasePreference;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/mediatek/settings/CallBarringBasePreference;

    const/4 v0, 0x0

    iget v1, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    invoke-virtual {p1, p0, v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->init(Lcom/mediatek/phone/TimeConsumingPreferenceListener;ZI)V

    :cond_0
    return-void
.end method

.method private initial()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v1, "AO"

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const v1, 0x7f0d00ba

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmTitle(I)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v1, "OI"

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const v1, 0x7f0d00bb

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmTitle(I)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v1, "OX"

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    const v1, 0x7f0d00bc

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmTitle(I)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v1, "AI"

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const v1, 0x7f0d00bd

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmTitle(I)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v1, "IR"

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmFacility(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    const v1, 0x7f0d00be

    invoke-virtual {v0, v1}, Lcom/mediatek/settings/CallBarringBasePreference;->setmTitle(I)V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mCallCancel:Lcom/mediatek/settings/CallBarringResetPreference;

    invoke-virtual {v0, p0}, Lcom/mediatek/settings/CallBarringResetPreference;->setListener(Lcom/mediatek/phone/TimeConsumingPreferenceListener;)V

    return-void
.end method

.method private startUpdate()V
    .locals 4

    const/4 v3, 0x0

    iput v3, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget v2, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/Preference;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/mediatek/settings/CallBarring;->doGetCallState(Landroid/preference/Preference;)V

    invoke-static {v3}, Lcom/android/phone/PhoneUtils;->setMmiFinished(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public doCallBarringRefresh(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d00c2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "AO"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_0
    const-string v1, "OI"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_1
    const-string v1, "OX"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_2
    const-string v1, "AI"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_3
    const-string v1, "IR"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_4
    return-void
.end method

.method public doCancelAllState()V
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1}, Landroid/preference/Preference;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d00c2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v1, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    return-void
.end method

.method public getErrorState()I
    .locals 1

    iget v0, p0, Lcom/mediatek/settings/CallBarring;->mErrorState:I

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const/16 v0, 0x12c

    invoke-virtual {p0}, Lcom/mediatek/settings/CallBarring;->getErrorState()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x1

    const/16 v6, 0x200

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/mediatek/settings/CallSettings;->isMultipleSim()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "simId"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    :cond_0
    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.SIM_INFO_UPDATE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mIntentFilter:Landroid/content/IntentFilter;

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v3, v4}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "ISVT"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/mediatek/settings/CallBarring;->mVtSetting:Z

    const-string v3, "CallBarring"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sim Id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ISVT = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/mediatek/settings/CallBarring;->mVtSetting:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v3, 0x7f060000

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/preference/PreferenceActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string v3, "all_outing_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringBasePreference;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v3, "all_outing_international_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringBasePreference;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v3, "all_outing_except_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringBasePreference;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v3, "all_incoming_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringBasePreference;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v3, "all_incoming_except_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringBasePreference;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    const-string v3, "deactivate_all_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringResetPreference;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallCancel:Lcom/mediatek/settings/CallBarringResetPreference;

    const-string v3, "change_password_key"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/settings/CallBarringChangePassword;

    iput-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallChangePassword:Lcom/mediatek/settings/CallBarringChangePassword;

    invoke-direct {p0}, Lcom/mediatek/settings/CallBarring;->initial()V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, p0}, Lcom/mediatek/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/mediatek/settings/CallBarringInterface;)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, p0}, Lcom/mediatek/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/mediatek/settings/CallBarringInterface;)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, p0}, Lcom/mediatek/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/mediatek/settings/CallBarringInterface;)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, p0}, Lcom/mediatek/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/mediatek/settings/CallBarringInterface;)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, p0}, Lcom/mediatek/settings/CallBarringBasePreference;->setRefreshInterface(Lcom/mediatek/settings/CallBarringInterface;)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallCancel:Lcom/mediatek/settings/CallBarringResetPreference;

    iget v4, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    invoke-virtual {v3, p0, v4}, Lcom/mediatek/settings/CallBarringResetPreference;->setCallBarringInterface(Lcom/mediatek/settings/CallBarringInterface;I)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallChangePassword:Lcom/mediatek/settings/CallBarringChangePassword;

    iget v4, p0, Lcom/mediatek/settings/CallBarring;->mSimId:I

    invoke-virtual {v3, p0, v4}, Lcom/mediatek/settings/CallBarringChangePassword;->setTimeConsumingListener(Lcom/mediatek/phone/TimeConsumingPreferenceListener;I)V

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "sub_title_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "sub_title_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_1
    iget-boolean v3, p0, Lcom/mediatek/settings/CallBarring;->mVtSetting:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallAllOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v6}, Lcom/mediatek/settings/CallBarringBasePreference;->setServiceClass(I)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v6}, Lcom/mediatek/settings/CallBarringBasePreference;->setServiceClass(I)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInternationalOutButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v6}, Lcom/mediatek/settings/CallBarringBasePreference;->setServiceClass(I)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v6}, Lcom/mediatek/settings/CallBarringBasePreference;->setServiceClass(I)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallInButton2:Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v3, v6}, Lcom/mediatek/settings/CallBarringBasePreference;->setServiceClass(I)V

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mCallCancel:Lcom/mediatek/settings/CallBarringResetPreference;

    invoke-virtual {v3, v6}, Lcom/mediatek/settings/CallBarringResetPreference;->setServiceClass(I)V

    :cond_2
    iput-boolean v7, p0, Lcom/mediatek/settings/CallBarring;->mFirstResume:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/settings/CallBarring;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onFinished(Landroid/preference/Preference;Z)V
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Z

    iget v3, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    iget-object v4, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget v4, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mediatek/settings/CallBarringBasePreference;

    invoke-virtual {v0}, Lcom/mediatek/settings/CallBarringBasePreference;->isSuccess()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    const-string v3, "Settings/CallBarring"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onFinished() is called (init part) mInitIndex is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "is reading?  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    iget v4, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/preference/Preference;

    invoke-direct {p0, v2}, Lcom/mediatek/settings/CallBarring;->doGetCallState(Landroid/preference/Preference;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onFinished(Landroid/preference/Preference;Z)V

    return-void

    :cond_1
    iget v1, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    :goto_1
    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/preference/Preference;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/mediatek/settings/CallBarring;->mPreferences:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :pswitch_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/android/phone/TimeConsumingPreferenceActivity;->onResume()V

    iget-boolean v0, p0, Lcom/mediatek/settings/CallBarring;->mFirstResume:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/settings/CallBarring;->mFirstResume:Z

    invoke-direct {p0}, Lcom/mediatek/settings/CallBarring;->startUpdate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/phone/PhoneUtils;->getMmiFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/settings/CallBarring;->startUpdate()V

    goto :goto_0
.end method

.method public resetIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CallBarring;->mInitIndex:I

    return-void
.end method

.method public setErrorState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mediatek/settings/CallBarring;->mErrorState:I

    return-void
.end method
