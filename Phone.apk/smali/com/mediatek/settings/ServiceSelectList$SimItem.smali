.class Lcom/mediatek/settings/ServiceSelectList$SimItem;
.super Ljava/lang/Object;
.source "ServiceSelectList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/settings/ServiceSelectList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimItem"
.end annotation


# static fields
.field public static final DESCRIPTION_LIST_ITEM_SIMID:J = -0x2L

.field public static final OFF_LIST_ITEM_SIMID:J = -0x1L


# instance fields
.field public mColor:I

.field public mDispalyNumberFormat:I

.field public mHas3GCapability:Z

.field public mIsSim:Z

.field public mName:Ljava/lang/String;

.field public mNumber:Ljava/lang/String;

.field public mSimID:J

.field public mSlot:I

.field public mState:I

.field final synthetic this$0:Lcom/mediatek/settings/ServiceSelectList;


# direct methods
.method public constructor <init>(Lcom/mediatek/settings/ServiceSelectList;Landroid/provider/Telephony$SIMInfo;)V
    .locals 4
    .param p2    # Landroid/provider/Telephony$SIMInfo;

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mHas3GCapability:Z

    iput-boolean v0, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    iput-object v3, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mName:Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mDispalyNumberFormat:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mColor:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSlot:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSimID:J

    const/4 v2, 0x5

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mState:I

    iput-boolean v0, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    iget-object v2, p2, Landroid/provider/Telephony$SIMInfo;->mDisplayName:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mName:Ljava/lang/String;

    iget-object v2, p2, Landroid/provider/Telephony$SIMInfo;->mNumber:Ljava/lang/String;

    iput-object v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    iget v2, p2, Landroid/provider/Telephony$SIMInfo;->mDispalyNumberFormat:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mDispalyNumberFormat:I

    iget v2, p2, Landroid/provider/Telephony$SIMInfo;->mColor:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mColor:I

    iget v2, p2, Landroid/provider/Telephony$SIMInfo;->mSlot:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSlot:I

    iget-wide v2, p2, Landroid/provider/Telephony$SIMInfo;->mSimId:J

    iput-wide v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSimID:J

    invoke-static {p1}, Lcom/mediatek/settings/ServiceSelectList;->access$400(Lcom/mediatek/settings/ServiceSelectList;)Lcom/android/phone/PhoneInterfaceManager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSlot:I

    invoke-static {p1}, Lcom/mediatek/settings/ServiceSelectList;->access$400(Lcom/mediatek/settings/ServiceSelectList;)Lcom/android/phone/PhoneInterfaceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneInterfaceManager;->get3GCapabilitySIM()I

    move-result v3

    if-ne v2, v3, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mHas3GCapability:Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/mediatek/settings/ServiceSelectList;Ljava/lang/String;IJ)V
    .locals 4
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # J

    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->this$0:Lcom/mediatek/settings/ServiceSelectList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mHas3GCapability:Z

    iput-boolean v0, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    iput-object v3, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mName:Ljava/lang/String;

    iput-object v3, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mNumber:Ljava/lang/String;

    iput v1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mDispalyNumberFormat:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mColor:I

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSlot:I

    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSimID:J

    const/4 v2, 0x5

    iput v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mState:I

    iput-object p2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mName:Ljava/lang/String;

    iput p3, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mColor:I

    iput-boolean v1, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mIsSim:Z

    iput-wide p4, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSimID:J

    invoke-static {p1}, Lcom/mediatek/settings/ServiceSelectList;->access$400(Lcom/mediatek/settings/ServiceSelectList;)Lcom/android/phone/PhoneInterfaceManager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mSlot:I

    invoke-static {p1}, Lcom/mediatek/settings/ServiceSelectList;->access$400(Lcom/mediatek/settings/ServiceSelectList;)Lcom/android/phone/PhoneInterfaceManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/phone/PhoneInterfaceManager;->get3GCapabilitySIM()I

    move-result v3

    if-ne v2, v3, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/mediatek/settings/ServiceSelectList$SimItem;->mHas3GCapability:Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
