.class public Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;
.super Ljava/lang/Object;
.source "CallHistoryAsync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/phone/provider/CallHistoryAsync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddCallArgs"
.end annotation


# instance fields
.field public final mContext:Landroid/content/Context;

.field public final mCountryISO:Ljava/lang/String;

.field public final mIsMultiSim:Z

.field public final mNumber:Ljava/lang/String;

.field public final mSlotId:I

.field public final mStart:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JIZ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # I
    .param p7    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;->mNumber:Ljava/lang/String;

    iput-object p3, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;->mCountryISO:Ljava/lang/String;

    iput-wide p4, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;->mStart:J

    iput p6, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;->mSlotId:I

    iput-boolean p7, p0, Lcom/mediatek/phone/provider/CallHistoryAsync$AddCallArgs;->mIsMultiSim:Z

    return-void
.end method
