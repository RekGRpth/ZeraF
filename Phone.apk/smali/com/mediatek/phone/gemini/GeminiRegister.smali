.class public Lcom/mediatek/phone/gemini/GeminiRegister;
.super Ljava/lang/Object;
.source "GeminiRegister.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "GeminiRegister"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dial(Ljava/lang/Object;Lcom/android/internal/telephony/Phone;Ljava/lang/String;I)Lcom/android/internal/telephony/Connection;
    .locals 7
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/android/internal/telephony/Phone;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/internal/telephony/CallStateException;
        }
    .end annotation

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    invoke-interface {p1}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    const/4 v3, 0x1

    :goto_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->isGeminiSupport()Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v3, :cond_2

    move v1, p3

    invoke-static {p3}, Lcom/mediatek/phone/gemini/GeminiUtils;->isValidSlot(I)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "persist.radio.default_sim"

    const/4 v5, -0x1

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-virtual {p0, p1, p2, v1}, Lcom/android/internal/telephony/gemini/MTKCallManager;->dialGemini(Lcom/android/internal/telephony/Phone;Ljava/lang/String;I)Lcom/android/internal/telephony/Connection;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/CallManager;->dial(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)Lcom/android/internal/telephony/Connection;
    :try_end_0
    .catch Lcom/android/internal/telephony/CallStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v4, Lcom/android/internal/telephony/CallStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "cannot dial, numberToDial:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", slotId:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/android/internal/telephony/CallStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static getSlotIdByRegisterEvent(I[I)I
    .locals 3
    .param p0    # I
    .param p1    # [I

    const/4 v2, -0x1

    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    invoke-static {p0, p1}, Lcom/mediatek/phone/gemini/GeminiUtils;->getIndexInArray(I[I)I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    aget v2, v0, v1

    :cond_0
    return v2
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "GeminiRegister"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static pickBestSlotForEmergencyCall(Lcom/android/internal/telephony/Phone;Ljava/lang/String;)I
    .locals 8
    .param p0    # Lcom/android/internal/telephony/Phone;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->isGeminiSupport()Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v1, p0

    check-cast v1, Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v2

    array-length v0, v2

    new-array v4, v0, [Z

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_1

    aget v6, v2, v3

    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->isRadioOnGemini(I)Z

    move-result v6

    aput-boolean v6, v4, v3

    aget v6, v2, v3

    invoke-virtual {v1, v6}, Lcom/android/internal/telephony/gemini/GeminiPhone;->getServiceStateGemini(I)Landroid/telephony/ServiceState;

    move-result-object v6

    invoke-virtual {v6}, Landroid/telephony/ServiceState;->getState()I

    move-result v5

    aget-boolean v6, v4, v3

    if-eqz v6, :cond_0

    if-nez v5, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pickBestSlotForEmergencyCallm, radio on & in service, slot:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v2, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/phone/gemini/GeminiRegister;->log(Ljava/lang/String;)V

    aget v6, v2, v3

    :goto_1
    return v6

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v0, :cond_3

    aget-boolean v6, v4, v3

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "pickBestSlotForEmergencyCallm, radio on, slot:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v2, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/mediatek/phone/gemini/GeminiRegister;->log(Ljava/lang/String;)V

    aget v6, v2, v3

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    const-string v6, "pickBestSlotForEmergencyCallm, no gemini"

    invoke-static {v6}, Lcom/mediatek/phone/gemini/GeminiRegister;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getDefaultSlot()I

    move-result v6

    goto :goto_1
.end method

.method public static registerForCrssSuppServiceNotification(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForCrssSuppServiceNotificationGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForCrssSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForDisconnect(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v1

    array-length v0, v1

    new-array v3, v0, [I

    const/4 v2, 0x0

    :goto_0
    array-length v4, v3

    if-ge v2, v4, :cond_0

    aput p2, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0, p1, v3}, Lcom/mediatek/phone/gemini/GeminiRegister;->registerForDisconnect(Ljava/lang/Object;Landroid/os/Handler;[I)V

    return-void
.end method

.method public static registerForDisconnect(Ljava/lang/Object;Landroid/os/Handler;[I)V
    .locals 7
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v4, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v4, :cond_1

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    array-length v4, p2

    array-length v5, v0

    if-lt v4, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    aget v3, p2, v1

    aget v4, v0, v1

    invoke-virtual {v2, p1, v3, v6, v4}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForDisconnectGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    aget v3, p2, v3

    invoke-virtual {p0, p1, v3, v6}, Lcom/android/internal/telephony/CallManager;->registerForDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public static registerForIncomingRing(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForIncomingRingGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForIncomingRing(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForMmiComplete(Ljava/lang/Object;Landroid/os/Handler;[I)V
    .locals 7
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v4, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    array-length v4, p2

    array-length v5, v0

    if-lt v4, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    aget v3, p2, v1

    aget v4, v0, v1

    invoke-virtual {v2, p1, v3, v6, v4}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForMmiCompleteGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    aget v3, p2, v3

    invoke-virtual {p0, p1, v3, v6}, Lcom/android/internal/telephony/CallManager;->registerForMmiComplete(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public static registerForMmiInitiate(Ljava/lang/Object;Landroid/os/Handler;[I)V
    .locals 7
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v4, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    array-length v4, p2

    array-length v5, v0

    if-lt v4, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    aget v3, p2, v1

    aget v4, v0, v1

    invoke-virtual {v2, p1, v3, v6, v4}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForMmiInitiateGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    aget v3, p2, v3

    invoke-virtual {p0, p1, v3, v6}, Lcom/android/internal/telephony/CallManager;->registerForMmiInitiate(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public static registerForNetworkLocked(Lcom/android/internal/telephony/Phone;Landroid/os/Handler;[I)V
    .locals 5
    .param p0    # Lcom/android/internal/telephony/Phone;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    array-length v3, p2

    array-length v4, v0

    if-lt v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v1, 0x0

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_2

    aget v3, v0, v1

    invoke-static {p0, v3}, Lcom/mediatek/phone/gemini/GeminiUtils;->getIccCard(Lcom/android/internal/telephony/Phone;I)Lcom/android/internal/telephony/IccCard;

    move-result-object v2

    if-eqz v2, :cond_0

    aget v3, p2, v1

    const/4 v4, 0x0

    invoke-interface {v2, p1, v3, v4}, Lcom/android/internal/telephony/IccCard;->registerForNetworkLocked(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static registerForNewRingingConnection(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForNewRingingConnectionGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForNewRingingConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForPostDialCharacter(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForPostDialCharacterGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForPostDialCharacter(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForPreciseCallStateChanged(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/mediatek/phone/gemini/GeminiRegister;->registerForPreciseCallStateChanged(Ljava/lang/Object;Landroid/os/Handler;ILjava/lang/Object;)V

    return-void
.end method

.method public static registerForPreciseCallStateChanged(Ljava/lang/Object;Landroid/os/Handler;ILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, p3, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForPreciseCallStateChangedGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/telephony/CallManager;->registerForPreciseCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForRingbackTone(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 8
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v7, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v6, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v6, :cond_0

    move-object v5, p0

    check-cast v5, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v1

    move-object v0, v1

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget v2, v0, v3

    invoke-virtual {v5, p1, p2, v7, v2}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForRingbackToneGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v7}, Lcom/android/internal/telephony/CallManager;->registerForRingbackTone(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForServiceStateChanged(Lcom/android/internal/telephony/Phone;Landroid/os/Handler;[I)V
    .locals 7
    .param p0    # Lcom/android/internal/telephony/Phone;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->isGeminiSupport()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/android/internal/telephony/gemini/GeminiPhone;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v1

    array-length v4, p2

    array-length v5, v1

    if-lt v4, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v2, 0x0

    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_2

    aget v3, v1, v2

    invoke-virtual {v0, p1, v3}, Lcom/android/internal/telephony/gemini/GeminiPhone;->unregisterForServiceStateChangedGemini(Landroid/os/Handler;I)V

    aget v3, p2, v2

    aget v4, v1, v2

    invoke-virtual {v0, p1, v3, v6, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->registerForServiceStateChangedGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {p0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForServiceStateChanged(Landroid/os/Handler;)V

    aget v3, p2, v3

    invoke-interface {p0, p1, v3, v6}, Lcom/android/internal/telephony/Phone;->registerForServiceStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public static registerForSpeechInfo(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v1

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlotCount()I

    move-result v0

    new-array v3, v0, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aput p2, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p0, p1, v3}, Lcom/mediatek/phone/gemini/GeminiRegister;->registerForSpeechInfo(Ljava/lang/Object;Landroid/os/Handler;[I)V

    return-void
.end method

.method public static registerForSpeechInfo(Ljava/lang/Object;Landroid/os/Handler;[I)V
    .locals 7
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    const/4 v6, 0x0

    const/4 v3, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v4, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    array-length v4, p2

    array-length v5, v0

    if-lt v4, v5, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-static {v3}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_2

    aget v3, p2, v1

    aget v4, v0, v1

    invoke-virtual {v2, p1, v3, v6, v4}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForSpeechInfoGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    aget v3, p2, v3

    invoke-virtual {p0, p1, v3, v6}, Lcom/android/internal/telephony/CallManager;->registerForSpeechInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_2
    return-void
.end method

.method public static registerForSuppServiceFailed(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForSuppServiceFailedGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForSuppServiceFailed(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForSuppServiceNotification(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForSuppServiceNotificationGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForSuppServiceNotification(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForUnknownConnection(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForUnknownConnectionGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForUnknownConnection(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForVtReplaceDisconnect(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForVtReplaceDisconnectGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForVtReplaceDisconnect(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForVtRingInfo(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForVtRingInfoGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForVtRingInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static registerForVtStatusInfo(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v4, 0x0

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, p2, v4, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->registerForVtStatusInfoGemini(Landroid/os/Handler;ILjava/lang/Object;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1, p2, v4}, Lcom/android/internal/telephony/CallManager;->registerForVtStatusInfo(Landroid/os/Handler;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public static unregisterForCrssSuppServiceNotification(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForCrssSuppServiceNotificationGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForCrssSuppServiceNotification(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForDisconnect(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForDisconnectGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForDisconnect(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForInCallVoicePrivacyOff(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v2, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForInCallVoicePrivacyOffGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForInCallVoicePrivacyOff(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForInCallVoicePrivacyOn(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v2, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForInCallVoicePrivacyOnGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForInCallVoicePrivacyOn(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForIncomingRing(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForIncomingRingGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForIncomingRing(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForMmiComplete(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForMmiCompleteGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForMmiComplete(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForMmiInitiate(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForMmiInitiateGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForMmiInitiate(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForNewRingingConnection(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForNewRingingConnectionGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForNewRingingConnection(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForPostDialCharacter(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForPostDialCharacterGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForPostDialCharacter(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForPreciseCallStateChanged(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForPreciseCallStateChangedGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForPreciseCallStateChanged(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForRingbackTone(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForDisconnectGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForRingbackTone(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForServiceStateChanged(Lcom/android/internal/telephony/Phone;Landroid/os/Handler;[I)V
    .locals 5
    .param p0    # Lcom/android/internal/telephony/Phone;
    .param p1    # Landroid/os/Handler;
    .param p2    # [I

    const/4 v3, 0x0

    if-eqz p0, :cond_2

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->isGeminiSupport()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    array-length v2, p2

    array-length v4, v0

    if-lt v2, v4, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljunit/framework/Assert;->assertTrue(Z)V

    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_2

    aget v2, p2, v1

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/GeminiPhone;

    aget v4, v0, v1

    invoke-virtual {v2, p1, v4}, Lcom/android/internal/telephony/gemini/GeminiPhone;->unregisterForServiceStateChangedGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v2, v3

    goto :goto_0

    :cond_1
    invoke-interface {p0, p1}, Lcom/android/internal/telephony/Phone;->unregisterForServiceStateChanged(Landroid/os/Handler;)V

    :cond_2
    aget v2, p2, v3

    invoke-virtual {p1, v2}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public static unregisterForSpeechInfo(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForSpeechInfoGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForSpeechInfo(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForSpeechInfo(Ljava/lang/Object;Landroid/os/Handler;I)V
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v0, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-virtual {p0, p1, p2}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForSpeechInfoGemini(Landroid/os/Handler;I)V

    :goto_0
    return-void

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForSpeechInfo(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public static unregisterForSuppServiceFailed(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForSuppServiceFailedGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForSuppServiceFailed(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForSuppServiceNotification(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForSuppServiceNotificationGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForSuppServiceNotification(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForUnknownConnection(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForUnknownConnectionGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForUnknownConnection(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForVtReplaceDisconnect(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForVtReplaceDisconnectGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForVtReplaceDisconnect(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForVtRingInfo(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForVtRingInfoGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForVtRingInfo(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method

.method public static unregisterForVtStatusInfo(Ljava/lang/Object;Landroid/os/Handler;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Landroid/os/Handler;

    invoke-static {p0}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    instance-of v3, p0, Lcom/android/internal/telephony/gemini/MTKCallManager;

    if-eqz v3, :cond_0

    move-object v2, p0

    check-cast v2, Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-static {}, Lcom/mediatek/phone/gemini/GeminiUtils;->getSlots()[I

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget v3, v0, v1

    invoke-virtual {v2, p1, v3}, Lcom/android/internal/telephony/gemini/MTKCallManager;->unregisterForVtStatusInfoGemini(Landroid/os/Handler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    check-cast p0, Lcom/android/internal/telephony/CallManager;

    invoke-virtual {p0, p1}, Lcom/android/internal/telephony/CallManager;->unregisterForVtStatusInfo(Landroid/os/Handler;)V

    :cond_1
    return-void
.end method
