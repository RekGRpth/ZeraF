.class public final Lcom/mediatek/phone/vt/VTInCallScreenFlags;
.super Ljava/lang/Object;
.source "VTInCallScreenFlags.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;
    }
.end annotation


# static fields
.field private static sVTInCallScreenFlags:Lcom/mediatek/phone/vt/VTInCallScreenFlags;


# instance fields
.field public mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

.field public mVTFrontCameraNow:Z

.field public mVTHasReceiveFirstFrame:Z

.field public mVTHideMeNow:Z

.field public mVTInControlRes:Z

.field public mVTInEndingCall:Z

.field public mVTInLocalBrightnessSetting:Z

.field public mVTInLocalContrastSetting:Z

.field public mVTInLocalZoomSetting:Z

.field public mVTInSnapshot:Z

.field public mVTInSwitchCamera:Z

.field public mVTIsInflate:Z

.field public mVTIsMT:Z

.field public mVTPeerBigger:Z

.field public mVTSettingReady:Z

.field public mVTShouldCloseVTManager:Z

.field public mVTSlotId:I

.field public mVTSurfaceChangedH:Z

.field public mVTSurfaceChangedL:Z

.field public mVTVideoConnected:Z

.field public mVTVideoReady:Z

.field public mVTVoiceAnswer:Z

.field public mVTVoiceAnswerPhoneNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;

    invoke-direct {v0}, Lcom/mediatek/phone/vt/VTInCallScreenFlags;-><init>()V

    sput-object v0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->sVTInCallScreenFlags:Lcom/mediatek/phone/vt/VTInCallScreenFlags;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    invoke-direct {v0, p0}, Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;-><init>(Lcom/mediatek/phone/vt/VTInCallScreenFlags;)V

    iput-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    invoke-virtual {p0}, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->reset()V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/phone/vt/VTInCallScreenFlags;
    .locals 1

    sget-object v0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->sVTInCallScreenFlags:Lcom/mediatek/phone/vt/VTInCallScreenFlags;

    return-object v0
.end method


# virtual methods
.method public reset()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTIsMT:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTHideMeNow:Z

    iput-boolean v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTFrontCameraNow:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSettingReady:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSurfaceChangedL:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSurfaceChangedH:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVideoConnected:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVideoReady:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTHasReceiveFirstFrame:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInLocalZoomSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInLocalBrightnessSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInLocalContrastSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInControlRes:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInEndingCall:Z

    iput-boolean v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTShouldCloseVTManager:Z

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;->reset()V

    :cond_0
    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInSnapshot:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInSwitchCamera:Z

    iput-boolean v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTPeerBigger:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVoiceAnswer:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVoiceAnswerPhoneNumber:Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/phone/ext/ExtensionManager;->getInstance()Lcom/mediatek/phone/ext/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->getVTInCallScreenFlagsExtension()Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;->reset()V

    iput v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSlotId:I

    return-void
.end method

.method public resetPartial()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTIsMT:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTHideMeNow:Z

    iput-boolean v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTFrontCameraNow:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSettingReady:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSurfaceChangedL:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSurfaceChangedH:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVideoConnected:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVideoReady:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTHasReceiveFirstFrame:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInLocalZoomSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInLocalBrightnessSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInLocalContrastSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInControlRes:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInEndingCall:Z

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;->reset()V

    :cond_0
    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInSnapshot:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTInSwitchCamera:Z

    iput-boolean v2, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTPeerBigger:Z

    iput-boolean v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVoiceAnswer:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTVoiceAnswerPhoneNumber:Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/phone/ext/ExtensionManager;->getInstance()Lcom/mediatek/phone/ext/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->getVTInCallScreenFlagsExtension()Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;->reset()V

    iput v1, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTSlotId:I

    return-void
.end method

.method public resetTiming()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/phone/vt/VTInCallScreenFlags;->mVTConnectionStarttime:Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;

    invoke-virtual {v0}, Lcom/mediatek/phone/vt/VTInCallScreenFlags$VTConnectionStarttime;->reset()V

    :cond_0
    return-void
.end method
