.class public final Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;
.super Ljava/lang/Object;
.source "VTInCallScreenFlagsLoopback.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;
    }
.end annotation


# static fields
.field private static sVTInCallScreenFlags:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;


# instance fields
.field public mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

.field public mVTFrontCameraNow:Z

.field public mVTHasReceiveFirstFrame:Z

.field public mVTHideMeNow:Z

.field public mVTInControlRes:Z

.field public mVTInEndingCall:Z

.field public mVTInLocalBrightnessSetting:Z

.field public mVTInLocalContrastSetting:Z

.field public mVTInLocalZoomSetting:Z

.field public mVTInSnapshot:Z

.field public mVTInSwitchCamera:Z

.field public mVTIsInflate:Z

.field public mVTIsMT:Z

.field public mVTPeerBigger:Z

.field public mVTSettingReady:Z

.field public mVTShouldCloseVTManager:Z

.field public mVTSlotId:I

.field public mVTSurfaceChangedH:Z

.field public mVTSurfaceChangedL:Z

.field public mVTVideoConnected:Z

.field public mVTVideoReady:Z

.field public mVTVoiceAnswer:Z

.field public mVTVoiceAnswerPhoneNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    invoke-direct {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;-><init>()V

    sput-object v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->sVTInCallScreenFlags:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;)V

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->reset()V

    return-void
.end method

.method public static getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;
    .locals 1

    sget-object v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->sVTInCallScreenFlags:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    return-object v0
.end method


# virtual methods
.method public reset()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTIsMT:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    iput-boolean v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTFrontCameraNow:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSettingReady:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoConnected:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoReady:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInControlRes:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInEndingCall:Z

    iput-boolean v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTShouldCloseVTManager:Z

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;->reset()V

    :cond_0
    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInSnapshot:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInSwitchCamera:Z

    iput-boolean v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVoiceAnswer:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVoiceAnswerPhoneNumber:Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/phone/ext/ExtensionManager;->getInstance()Lcom/mediatek/phone/ext/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->getVTInCallScreenFlagsExtension()Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;->reset()V

    iput v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSlotId:I

    return-void
.end method

.method public resetPartial()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTIsMT:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    iput-boolean v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTFrontCameraNow:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSettingReady:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoConnected:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoReady:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInControlRes:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInEndingCall:Z

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;->reset()V

    :cond_0
    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInSnapshot:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInSwitchCamera:Z

    iput-boolean v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    iput-boolean v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVoiceAnswer:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVoiceAnswerPhoneNumber:Ljava/lang/String;

    invoke-static {}, Lcom/mediatek/phone/ext/ExtensionManager;->getInstance()Lcom/mediatek/phone/ext/ExtensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/ExtensionManager;->getVTInCallScreenFlagsExtension()Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/VTInCallScreenFlagsExtension;->reset()V

    iput v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSlotId:I

    return-void
.end method

.method public resetTiming()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;->reset()V

    :cond_0
    return-void
.end method
