.class public Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
.super Landroid/widget/RelativeLayout;
.source "VTInCallScreenLoopback.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/PopupMenu$OnDismissListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$InCallVideoSettingLocalEffectListener;,
        Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$DialogCancelTimer;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final DELAYED_CLEANUP_AFTER_DISCONNECT:I = 0x92

.field private static final DELAYED_CLEANUP_AFTER_DISCONNECT2:I = 0x93

.field private static final HIDDEN:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "VTInCallScreenLoopback"

.field private static final SECOND_TO_MILLISECOND:I = 0x3e8

.field private static final VDBG:Z = true

.field private static final VISIBLE:I = 0xff

.field private static final VT_MEDIA_ERROR_VIDEO_FAIL:I = 0x1

.field private static final VT_MEDIA_OCCUPIED:I = 0x1

.field private static final VT_MEDIA_RECORDER_ERROR_UNKNOWN:I = 0x1

.field private static final VT_MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:I = 0x321

.field private static final VT_MEDIA_RECORDER_NO_I_FRAME:I = 0x7fff

.field private static final VT_PEER_SNAPSHOT_FAIL:I = 0x7f

.field private static final VT_PEER_SNAPSHOT_OK:I = 0x7e

.field private static final VT_TAKE_PEER_PHOTO_DISK_MIN_SIZE:I = 0xf4240

.field private static final WAITING_TIME_FOR_ASK_VT_SHOW_ME:I = 0x5


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAudioModePopup:Landroid/widget/PopupMenu;

.field private mAudioModePopupVisible:Z

.field private mCM:Lcom/android/internal/telephony/CallManager;

.field private mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

.field private mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

.field private mExtension:Lcom/mediatek/phone/ext/VTInCallScreenExtension;

.field private mHandler:Landroid/os/Handler;

.field private mHighVideoHolder:Landroid/view/SurfaceHolder;

.field private mInCallVideoSettingDialog:Landroid/app/AlertDialog;

.field private mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

.field private mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

.field private mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

.field private mLocaleChanged:Z

.field private mLowVideoHolder:Landroid/view/SurfaceHolder;

.field private mVTAudio:Landroid/widget/CompoundButton;

.field private mVTDialpad:Landroid/widget/CompoundButton;

.field private mVTHangUp:Landroid/widget/ImageButton;

.field private mVTHangUpWrapper:Landroid/widget/LinearLayout;

.field private mVTHighDown:Landroid/widget/ImageButton;

.field private mVTHighUp:Landroid/widget/ImageButton;

.field private mVTHighVideo:Landroid/view/SurfaceView;

.field private mVTInCallCanvas:Landroid/view/ViewGroup;

.field private mVTLowDown:Landroid/widget/ImageButton;

.field private mVTLowUp:Landroid/widget/ImageButton;

.field private mVTLowVideo:Landroid/view/SurfaceView;

.field private mVTMTAsker:Landroid/app/AlertDialog;

.field private mVTMute:Landroid/widget/CompoundButton;

.field private mVTOverflowMenu:Landroid/widget/ImageButton;

.field private mVTPopupMenu:Landroid/widget/PopupMenu;

.field private mVTPowerManager:Landroid/os/PowerManager;

.field mVTRecorderEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVTRecorderSelector:Landroid/app/AlertDialog;

.field private mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

.field private mVTSwapVideo:Landroid/widget/CompoundButton;

.field private mVTVoiceReCallDialog:Landroid/app/AlertDialog;

.field private mVTVoiceRecordingIcon:Landroid/widget/ImageView;

.field private mVTWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$1;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$1;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    const-string v0, "VTInCallScreen constructor..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "- this = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$1;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$1;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$1;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$1;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    iget-object v0, v0, Lcom/android/phone/PhoneGlobals;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTSettingToVTManager()V

    return-void
.end method

.method static synthetic access$1000(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showVTLocalZoom()V

    return-void
.end method

.method static synthetic access$1100(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showVTLocalBrightness()V

    return-void
.end method

.method static synthetic access$1200(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showVTLocalContrast()V

    return-void
.end method

.method static synthetic access$1300(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTInCallVideoSettingLocalEffect()V

    return-void
.end method

.method static synthetic access$1400(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTInCallVideoSettingLocalNightMode()V

    return-void
.end method

.method static synthetic access$1500(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTInCallVideoSettingPeerQuality()V

    return-void
.end method

.method static synthetic access$1600(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateLocalZoomOrBrightness()V

    return-void
.end method

.method static synthetic access$1800(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onReceiveVTManagerReady()V

    return-void
.end method

.method static synthetic access$2000(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->makeVoiceReCall(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderSelector:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderSelector:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;I)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->startRecord(I)V

    return-void
.end method

.method static synthetic access$2400(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTHideMeClick()V

    return-void
.end method

.method static synthetic access$300(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Lcom/android/internal/telephony/CallManager;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTReceiveFirstFrame()V

    return-void
.end method

.method static synthetic access$600(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$902(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0    # Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;
    .param p1    # Landroid/app/AlertDialog;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private closeVTInCallCanvas()V
    .locals 3

    const/16 v2, 0x8

    const-string v0, "closeVTInCallCanvas!"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissAudioModePopup()V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_3
    return-void
.end method

.method private closeVTManager()V
    .locals 3

    const-string v0, "closeVTManager()!"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissVTDialogs()V

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    const v1, 0x8001

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const-string v0, "VT_VOICE_RECORDING"

    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "VT_VIDEO_RECORDING"

    invoke-static {v0}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->isVTRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->stopRecord()V

    :cond_1
    const-string v0, "- call VTManager onDisconnected ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->onDisconnected()V

    const-string v0, "- finish call VTManager onDisconnected ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const-string v0, "- set VTManager close ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->setVTClose()V

    const-string v0, "- finish set VTManager close ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTInControlRes()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "VTInCallScreenLoopback:closeVTManager:sendIntent_CALL_END"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.phone.extra.VT_CALL_END"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTInControlRes(Z)V

    :cond_2
    return-void
.end method

.method private constructPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    const v1, 0x7f100004

    invoke-virtual {v0, v1}, Landroid/widget/PopupMenu;->inflate(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setupMenuItems(Landroid/view/Menu;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    return-object v0
.end method

.method private dismissAudioModePopup()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopupVisible:Z

    :cond_0
    return-void
.end method

.method private dismissVideoSettingDialogs()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    :cond_3
    return-void
.end method

.method private getVTInControlRes()Z
    .locals 1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInControlRes:Z

    return v0
.end method

.method private handleAudioButtonClick()V
    .locals 0

    return-void
.end method

.method private hideLocalZoomOrBrightness()V
    .locals 2

    const/16 v1, 0x8

    const-string v0, "hideLocalZoomOrBrightness()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private isDialerOpened()Z
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    invoke-virtual {v0}, Lcom/android/phone/DTMFTwelveKeyDialer;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "VTInCallScreenLoopback"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private makeVoiceReCall(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "makeVoiceReCall(), number is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " slot is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL"

    const-string v2, "tel"

    const/4 v3, 0x0

    invoke-static {v2, p1, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "com.android.phone.extra.slot"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "com.android.phone.extra.international"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/ContextWrapper;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method private okToRecordVoice()Z
    .locals 1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoConnected:Z

    return v0
.end method

.method private onReceiveVTManagerReady()V
    .locals 3

    const/4 v2, 0x1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoReady:Z

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    const-string v0, "Incallscreen, before call setting"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/phone/PhoneUtils;->isDMLocked()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Now DM not locked, VTManager.getInstance().unlockPeerVideo() start;"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->unlockPeerVideo()V

    const-string v0, "Now DM not locked, VTManager.getInstance().unlockPeerVideo() end;"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-object v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mShowLocalMT:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTIsMT:Z

    if-eqz v0, :cond_0

    const-string v0, "- VTSettingUtils.getInstance().mShowLocalMT : 1 !"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const-string v0, "Incallscreen, before enableAlwaysAskSettings"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->enableAlwaysAskSettings(I)V

    const-string v0, "Incallscreen, after enableAlwaysAskSettings"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/phone/PhoneGlobals;->getInCallScreenInstance()Lcom/android/phone/InCallScreen;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$13;

    invoke-direct {v2, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$13;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$12;

    invoke-direct {v2, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$12;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$11;

    invoke-direct {v1, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$11;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$DialogCancelTimer;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    invoke-direct {v0, p0, v1, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$DialogCancelTimer;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;ILandroid/app/AlertDialog;)V

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$DialogCancelTimer;->start()V

    :cond_0
    return-void
.end method

.method private onVTHideMeClick()V
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    const-string v2, "onVTHideMeClick()..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->getState()Lcom/mediatek/vt/VTManager$State;

    move-result-object v2

    sget-object v3, Lcom/mediatek/vt/VTManager$State;->READY:Lcom/mediatek/vt/VTManager$State;

    if-eq v2, v3, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->getState()Lcom/mediatek/vt/VTManager$State;

    move-result-object v2

    sget-object v3, Lcom/mediatek/vt/VTManager$State;->CONNECTED:Lcom/mediatek/vt/VTManager$State;

    if-eq v2, v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v1, v3}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v3

    iget-boolean v3, v3, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    if-nez v3, :cond_4

    :goto_2
    iput-boolean v0, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v2

    iget-object v2, v2, Lcom/mediatek/settings/VTSettingUtils;->mPicToReplaceLocal:Ljava/lang/String;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathDefault()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v2

    iget-object v2, v2, Lcom/mediatek/settings/VTSettingUtils;->mPicToReplaceLocal:Ljava/lang/String;

    const-string v3, "1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    const/4 v3, 0x2

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v3

    iget v3, v3, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSlotId:I

    invoke-static {v3}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private onVTHideMeClick2()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "onVTHideMeClick2()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-object v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mPicToReplaceLocal:Ljava/lang/String;

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v1

    iget v1, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSlotId:I

    invoke-static {v1}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathUserselect(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    :goto_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v3, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-object v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mPicToReplaceLocal:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    const/4 v1, 0x2

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/settings/VTAdvancedSetting;->getPicPathDefault()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/mediatek/vt/VTManager;->setLocalVideoType(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private onVTInCallVideoSetting()V
    .locals 2

    const-string v1, "onVTInCallVideoSetting() ! "

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$4;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$4;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    return-void
.end method

.method private onVTInCallVideoSettingLocalEffect()V
    .locals 11

    const-string v9, "onVTInCallVideoSettingLocalEffect() ! "

    invoke-direct {p0, v9}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/vt/VTManager;->getSupportedColorEffects()Ljava/util/List;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-gtz v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070015

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070014

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    array-length v6, v3

    :goto_1
    if-ge v5, v6, :cond_3

    aget-object v9, v3, v5

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v9

    if-ltz v9, :cond_2

    aget-object v9, v3, v5

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    aget-object v9, v1, v5

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onVTInCallVideoSettingLocalEffect() : entryValues2.size() - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mediatek/vt/VTManager;->getColorEffect()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    new-instance v7, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$InCallVideoSettingLocalEffectListener;

    invoke-direct {v7, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$InCallVideoSettingLocalEffectListener;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    invoke-virtual {v7, v4}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$InCallVideoSettingLocalEffectListener;->setValues(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private onVTInCallVideoSettingLocalNightMode()V
    .locals 2

    const-string v1, "onVTInCallVideoSettingLocalNightMode() ! "

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$5;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$5;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    return-void
.end method

.method private onVTInCallVideoSettingPeerQuality()V
    .locals 2

    const-string v1, "onVTInCallVideoSettingPeerQuality() ! "

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$6;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$6;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    return-void
.end method

.method private onVTReceiveFirstFrame()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "onVTReceiveFirstFrame() ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    if-nez v0, :cond_1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private onVTShowDialpad()V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "onVTShowDialpad() ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    invoke-virtual {v0}, Lcom/android/phone/DTMFTwelveKeyDialer;->isOpened()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "onShowHideDialpad(): Set mInCallTitle VISIBLE"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    invoke-virtual {v0, v1}, Lcom/android/phone/DTMFTwelveKeyDialer;->closeDialer(Z)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    invoke-virtual {v0, v1}, Lcom/android/phone/DTMFTwelveKeyDialer;->openDialer(Z)V

    return-void
.end method

.method private onVTSwapVideos()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "onVTSwapVideos() ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v3

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->setVTVisible(Z)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTLocalPeerDisplay()V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/VTManager;->setVTVisible(Z)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showVTLocalZoom()V

    :cond_2
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showVTLocalBrightness()V

    :cond_3
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showVTLocalContrast()V

    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method private onVTSwitchCameraClick()V
    .locals 3

    const/4 v1, 0x0

    const-string v0, "onVTSwitchCameraClick()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->getState()Lcom/mediatek/vt/VTManager$State;

    move-result-object v0

    sget-object v2, Lcom/mediatek/vt/VTManager$State;->READY:Lcom/mediatek/vt/VTManager$State;

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->getState()Lcom/mediatek/vt/VTManager$State;

    move-result-object v0

    sget-object v2, Lcom/mediatek/vt/VTManager$State;->CONNECTED:Lcom/mediatek/vt/VTManager$State;

    if-eq v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInSwitchCamera:Z

    if-eqz v0, :cond_1

    const-string v0, "VTManager is handling switchcamera now, so returns this time."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$3;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$3;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTFrontCameraNow:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTFrontCameraNow:Z

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private onVTTakePeerPhotoClick()V
    .locals 2

    const-string v0, "onVTTakePeerPhotoClick()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->getState()Lcom/mediatek/vt/VTManager$State;

    move-result-object v0

    sget-object v1, Lcom/mediatek/vt/VTManager$State;->CONNECTED:Lcom/mediatek/vt/VTManager$State;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInSnapshot:Z

    if-eqz v0, :cond_2

    const-string v0, "VTManager is handling snapshot now, so returns this time."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/android/phone/PhoneUtils;->isExternalStorageMounted()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/32 v0, 0xf4240

    invoke-static {v0, v1}, Lcom/android/phone/PhoneUtils;->diskSpaceAvailable(J)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$2;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$2;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private onVoiceVideoRecordClick(Landroid/view/MenuItem;)V
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const-string v1, "onVoiceVideoRecordClick"

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/android/phone/PhoneUtils;->isExternalStorageMounted()Z

    move-result v1

    if-eqz v1, :cond_0

    const-wide/32 v1, 0x200000

    invoke-static {v1, v2}, Lcom/android/phone/PhoneUtils;->diskSpaceAvailable(J)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0
.end method

.method private openVTInCallCanvas()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "openVTInCallCanvas!"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method private setVTInControlRes(Z)V
    .locals 1
    .param p1    # Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean p1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInControlRes:Z

    return-void
.end method

.method private setVTSettingToVTManager()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mToReplacePeer:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mediatek/vt/VTManager;->enableHideYou(I)V

    :goto_0
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mShowLocalMO:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->enableHideMe(I)V

    :goto_1
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-object v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mShowLocalMT:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->incomingVideoDispaly(I)V

    :goto_2
    const-string v0, "Incallscreen, after call setting"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->enableHideYou(I)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mediatek/vt/VTManager;->enableHideMe(I)V

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-object v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mShowLocalMT:Ljava/lang/String;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/mediatek/vt/VTManager;->incomingVideoDispaly(I)V

    goto :goto_2

    :cond_3
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/VTManager;->incomingVideoDispaly(I)V

    goto :goto_2
.end method

.method private showAudioModePopup()V
    .locals 11

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v6, "showAudioModePopup()..."

    invoke-direct {p0, v6}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissAudioModePopup()V

    iget-object v6, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v6}, Landroid/widget/PopupMenu;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v6

    const v9, 0x7f100001

    iget-object v10, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v10}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v10

    invoke-virtual {v6, v9, v10}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object v6, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v6, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    iget-object v6, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v6, p0}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    :cond_0
    iget-object v6, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v6}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    const v6, 0x7f080134

    invoke-interface {v2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const v6, 0x7f080135

    invoke-interface {v2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v6, 0x7f080136

    invoke-interface {v2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/phone/PhoneGlobals;->isHeadsetPlugged()Z

    move-result v4

    if-nez v4, :cond_2

    move v6, v7

    :goto_0
    invoke-interface {v1, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    if-nez v4, :cond_1

    move v8, v7

    :cond_1
    invoke-interface {v1, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v6, 0x7f080137

    invoke-interface {v2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v6, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v6}, Landroid/widget/PopupMenu;->show()V

    iput-boolean v7, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopupVisible:Z

    return-void

    :cond_2
    move v6, v8

    goto :goto_0
.end method

.method private showGenericErrorDialog(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    return-void
.end method

.method private showReCallDialogEx(ILjava/lang/String;I)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const-string v4, "showReCallDialogEx... "

    invoke-direct {p0, v4}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    new-instance v1, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$7;

    invoke-direct {v1, p0, p2, p3}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$7;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;Ljava/lang/String;I)V

    new-instance v2, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$8;

    invoke-direct {v2, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$8;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$9;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$9;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private showStartVTRecorderDialog()V
    .locals 4

    const-string v1, "showStartVTRecorderDialog() ..."

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderEntries:Ljava/util/ArrayList;

    if-nez v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderEntries:Ljava/util/ArrayList;

    :goto_0
    const-string v1, "VT_VIDEO_RECORDING"

    invoke-static {v1}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VT_VIDEO_RECORDING"

    invoke-static {v1}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderEntries:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0068

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v1, "VT_VOICE_RECORDING"

    invoke-static {v1}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderEntries:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0069

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const-string v1, "VT_VIDEO_RECORDING"

    invoke-static {v1}, Lcom/android/phone/PhoneUtils;->isSupportFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderEntries:Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d006a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$10;

    invoke-direct {v0, p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback$10;-><init>(Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;)V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderEntries:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private showVTLocalBrightness()V
    .locals 4

    const v3, 0x7f0200da

    const v1, 0x7f0200d9

    const/4 v2, 0x0

    const-string v0, "showVTLocalBrightness()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoReady:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1
.end method

.method private showVTLocalContrast()V
    .locals 4

    const v3, 0x7f0200dc

    const v1, 0x7f0200db

    const/4 v2, 0x0

    const-string v0, "showVTLocalContrast()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoReady:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1
.end method

.method private showVTLocalZoom()V
    .locals 4

    const v3, 0x7f0200e5

    const v1, 0x7f0200e4

    const/4 v2, 0x0

    const-string v0, "showVTLocalZoom()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoReady:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_1
.end method

.method private startRecord(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const-string v2, "startVTRecorder() ..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/android/phone/PhoneUtils;->getDiskAvailableSize()J

    move-result-wide v2

    const-wide/32 v4, 0x200000

    sub-long v0, v2, v4

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    const/4 v2, 0x2

    if-ne v2, p1, :cond_1

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    invoke-static {v2}, Lcom/mediatek/phone/recording/PhoneRecorder;->getInstance(Landroid/content/Context;)Lcom/mediatek/phone/recording/PhoneRecorder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/phone/recording/PhoneRecorder;->ismFlagRecord()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "startRecord"

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->startVoiceRecord(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-lez p1, :cond_0

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1, v6}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->startVideoRecord(IJI)V

    invoke-virtual {p0, v6}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVideoCallRecordState(I)V

    goto :goto_0

    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, v2, v0

    if-nez v2, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0033

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showToast(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0034

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showToast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stopVideoRecord()V
    .locals 1

    const-string v0, "stopVideoRecorder() ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->stopVideoRecord()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVideoCallRecordState(I)V

    return-void
.end method

.method private updateAudioButton(Lcom/android/phone/InCallControlState;)V
    .locals 12
    .param p1    # Lcom/android/phone/InCallControlState;

    const/4 v7, 0x1

    const/16 v9, 0xff

    const/4 v8, 0x0

    const-string v10, "updateAudioButton()..."

    invoke-direct {p0, v10}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-boolean v10, p1, Lcom/android/phone/InCallControlState;->bluetoothEnabled:Z

    if-eqz v10, :cond_2

    const-string v10, "- updateAudioButton: \'popup menu action button\' mode..."

    invoke-direct {p0, v10}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v3, 0x1

    iget-boolean v7, p1, Lcom/android/phone/InCallControlState;->bluetoothIndicatorOn:Z

    if-eqz v7, :cond_0

    const/4 v1, 0x1

    :goto_0
    iget-object v7, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v7}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "- \'layers\' drawable: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const v7, 0x7f08012c

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    if-eqz v3, :cond_5

    move v7, v9

    :goto_1
    invoke-virtual {v10, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    const v7, 0x7f08012d

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    if-eqz v1, :cond_6

    move v7, v9

    :goto_2
    invoke-virtual {v10, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    const v7, 0x7f08012e

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    if-eqz v2, :cond_7

    move v7, v9

    :goto_3
    invoke-virtual {v10, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    const v7, 0x7f08012f

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    if-eqz v5, :cond_8

    move v7, v9

    :goto_4
    invoke-virtual {v10, v7}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    const v7, 0x7f080130

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v4, :cond_9

    :goto_5
    invoke-virtual {v7, v9}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    return-void

    :cond_0
    iget-boolean v7, p1, Lcom/android/phone/InCallControlState;->speakerOn:Z

    if-eqz v7, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v10, p1, Lcom/android/phone/InCallControlState;->speakerEnabled:Z

    if-eqz v10, :cond_4

    const-string v10, "- updateAudioButton: \'speaker toggle\' mode..."

    invoke-direct {p0, v10}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v10, v7}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v10, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    iget-boolean v11, p1, Lcom/android/phone/InCallControlState;->speakerOn:Z

    invoke-virtual {v10, v11}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v6, 0x1

    iget-boolean v5, p1, Lcom/android/phone/InCallControlState;->speakerOn:Z

    iget-boolean v10, p1, Lcom/android/phone/InCallControlState;->speakerOn:Z

    if-nez v10, :cond_3

    move v4, v7

    :goto_6
    goto/16 :goto_0

    :cond_3
    move v4, v8

    goto :goto_6

    :cond_4
    const-string v7, "- updateAudioButton: disabled..."

    invoke-direct {p0, v7}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v7, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v7, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v6, 0x1

    const/4 v4, 0x1

    goto/16 :goto_0

    :cond_5
    move v7, v8

    goto :goto_1

    :cond_6
    move v7, v8

    goto :goto_2

    :cond_7
    move v7, v8

    goto :goto_3

    :cond_8
    move v7, v8

    goto :goto_4

    :cond_9
    move v9, v8

    goto :goto_5
.end method

.method private updateLocalZoomOrBrightness()V
    .locals 2

    const-string v0, "updateLocalZoomOrBrightness()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateVTInCallButtons()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v4, "updateVTInCallButtons()..."

    invoke-direct {p0, v4}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v4

    iget-boolean v4, v4, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInEndingCall:Z

    if-nez v4, :cond_0

    invoke-static {}, Lcom/mediatek/phone/vt/VTCallUtils;->isVTIdle()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :goto_0
    const-string v4, "updateVTInCallButtons() : update mVTMute \'s src !"

    invoke-direct {p0, v4}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-static {}, Lcom/android/phone/PhoneUtils;->getMute()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const-string v4, "updateVTInCallButtons() : update mVTSwapVideo \'s src !"

    invoke-direct {p0, v4}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v5

    iget-boolean v5, v5, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-nez v5, :cond_3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const-string v2, "updateVTInCallButtons() : update mVTDialpad \'s src !"

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const-string v2, "updateVTInCallButtons() : update mVTAudio \'s src !"

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v2}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v2}, Lcom/android/internal/telephony/CallManager;->getActiveFgCallState()Lcom/android/internal/telephony/Call$State;

    move-result-object v0

    sget-object v2, Lcom/android/internal/telephony/PhoneConstants$State;->OFFHOOK:Lcom/android/internal/telephony/PhoneConstants$State;

    if-ne v1, v2, :cond_4

    sget-object v2, Lcom/android/internal/telephony/Call$State;->ALERTING:Lcom/android/internal/telephony/Call$State;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/android/internal/telephony/Call$State;->ACTIVE:Lcom/android/internal/telephony/Call$State;

    if-ne v0, v2, :cond_4

    :cond_1
    const-string v2, "updateVTInCallButtons() :phone state is OFFHOOK orcall state is ALERTING or ACTIVE,not dismiss !"

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_2
    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-static {}, Lcom/mediatek/phone/vt/VTCallUtils;->isVTActive()Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v4, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v4, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v5

    iget-boolean v5, v5, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissAudioModePopup()V

    goto :goto_2
.end method

.method private updateVTLocalPeerDisplay()V
    .locals 3

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/vt/VTManager;->setDisplay(Landroid/view/SurfaceHolder;Landroid/view/SurfaceHolder;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1, v2}, Lcom/mediatek/vt/VTManager;->setDisplay(Landroid/view/SurfaceHolder;Landroid/view/SurfaceHolder;)V

    goto :goto_0
.end method


# virtual methods
.method public dismissVTDialogs()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "dismissVTDialogs() ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingDialog:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalEffectDialog:Landroid/app/AlertDialog;

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingLocalNightmodeDialog:Landroid/app/AlertDialog;

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mInCallVideoSettingPeerQualityDialog:Landroid/app/AlertDialog;

    :cond_3
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMTAsker:Landroid/app/AlertDialog;

    :cond_4
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceReCallDialog:Landroid/app/AlertDialog;

    :cond_5
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderSelector:Landroid/app/AlertDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderSelector:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iput-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTRecorderSelector:Landroid/app/AlertDialog;

    :cond_6
    return-void
.end method

.method public getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getVTScreenMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

    return-object v0
.end method

.method public handleOnScreenMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "- handleOnScreenMenuItemClick: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "  title: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    :pswitch_1
    return v0

    :pswitch_2
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTSwitchCameraClick()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTTakePeerPhotoClick()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTHideMeClick()V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTSwapVideos()V

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, p1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVoiceVideoRecordClick(Landroid/view/MenuItem;)V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTInCallVideoSetting()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f080134
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public initCommonVTState()V
    .locals 1

    invoke-static {}, Lcom/android/phone/PhoneUtils;->isDMLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "- Now DM locked, VTManager.getInstance().lockPeerVideo() start"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->lockPeerVideo()V

    const-string v0, "- Now DM locked, VTManager.getInstance().lockPeerVideo() end"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissAudioModePopup()V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTLocalPeerDisplay()V

    return-void
.end method

.method public initDialingSuccessVTState()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mExtension:Lcom/mediatek/phone/ext/VTInCallScreenExtension;

    invoke-virtual {v0}, Lcom/mediatek/phone/ext/VTInCallScreenExtension;->initDialingSuccessVTState()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mShowLocalMO:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTHideMeClick2()V

    goto :goto_0
.end method

.method public initVTInCallScreen()V
    .locals 8

    const v7, 0x7f080116

    const/16 v6, 0x8

    const/4 v5, 0x1

    const v4, 0x7f08011b

    const/4 v3, 0x0

    const-string v0, "initVTInCallCanvas()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPowerManager:Landroid/os/PowerManager;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPowerManager:Landroid/os/PowerManager;

    const v1, 0x2000000a

    const-string v2, "VTWakeLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    const v0, 0x7f08010e

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTInCallCanvas:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f080110

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f080111

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f08011c

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f08011d

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    const v0, 0x7f08010f

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const v0, 0x7f080114

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const v0, 0x7f080115

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const v0, 0x7f080117

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const v0, 0x7f080118

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const v0, 0x7f080119

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    const v0, 0x7f08011a

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUpWrapper:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f080112

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200b0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    const v1, 0x7f080115

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    const v1, 0x7f080115

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    const v1, 0x7f080117

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    const v1, 0x7f080115

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    const v1, 0x7f080118

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    :goto_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    const v1, 0x7f080117

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    const v1, 0x7f080118

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    :goto_2
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    const v1, 0x7f080115

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    const v1, 0x7f080117

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    const v1, 0x7f080118

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v7}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    const v1, 0x7f080117

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    const v1, 0x7f080119

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    const v1, 0x7f080117

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/view/View;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTMute:Landroid/widget/CompoundButton;

    const v1, 0x7f080119

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusDownId(I)V

    goto/16 :goto_1

    :cond_2
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    const v1, 0x7f080119

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusRightId(I)V

    goto/16 :goto_2
.end method

.method public internalAnswerVTCallPre()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "internalAnswerVTCallPre()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/vt/VTCallUtils;->isVTActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->closeVTManager()V

    const-string v0, "internalAnswerVTCallPre: set VTInCallScreenFlagsLoopback.getInstance().mVTShouldCloseVTManager = false"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v3, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTShouldCloseVTManager:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->resetPartial()V

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->reset()V

    const-string v0, "VTInCallScreenLoopback:closeVTManager:sendIntent_CALL_START_YET"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTInControlRes()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "VTInCallScreenLoopback:closeVTManager:sendIntent_CALL_START_GOES"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.phone.extra.VT_CALL_START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0, v4}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTInControlRes(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->registerForVTPhoneStates()V

    const-string v0, "- set VTManager open ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCMGemini:Lcom/android/internal/telephony/gemini/MTKCallManager;

    invoke-virtual {v0, v1, v2, v3}, Lcom/mediatek/vt/VTManager;->setVTOpen(Landroid/content/Context;Ljava/lang/Object;I)V

    const-string v0, "- finish set VTManager open ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-object v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mShowLocalMT:Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTHideMeClick2()V

    :cond_2
    invoke-static {}, Lcom/android/phone/PhoneUtils;->isDMLocked()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "- Now DM locked, VTManager.getInstance().lockPeerVideo() start"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->lockPeerVideo()V

    const-string v0, "- Now DM locked, VTManager.getInstance().lockPeerVideo() end"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :cond_3
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    if-eqz v0, :cond_4

    const-string v0, "- set VTManager ready ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->setVTReady()V

    const-string v0, "- finish set VTManager ready ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_4
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v4, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSettingReady:Z

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick(View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick: unexpected click from ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (View = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_1
    const-string v2, "onClick: VTHighVideo..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iput-boolean v4, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iput-boolean v4, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iput-boolean v4, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    goto :goto_0

    :pswitch_2
    const-string v2, "onClick: VTLowVideo..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTPeerBigger:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iput-boolean v4, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iput-boolean v4, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iput-boolean v4, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    goto :goto_0

    :pswitch_3
    const-string v2, "onClick: VTMute"

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    const-string v2, "onClick: VTSpeaker..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->handleAudioButtonClick()V

    goto :goto_0

    :pswitch_5
    const-string v2, "onClick: VTDialpad..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    const-string v2, "onClick: VTSwapVideo..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->onVTSwapVideos()V

    goto :goto_0

    :pswitch_7
    const-string v2, "onClick: VTHangUp..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInEndingCall:Z

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v2

    iget-object v2, v2, Lcom/android/phone/PhoneGlobals;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-static {v2}, Lcom/android/phone/PhoneUtils;->hangup(Lcom/android/internal/telephony/CallManager;)Z

    goto/16 :goto_0

    :pswitch_8
    const-string v2, "onClick: VTLowUp..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->incZoom()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->incBrightness()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_2
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->incContrast()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_9
    const-string v2, "onClick: VTHighUp..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->incZoom()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_3
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-eqz v2, :cond_4

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->incBrightness()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->incContrast()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_a
    const-string v2, "onClick: VTLowDown..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->decZoom()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_5
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->decBrightness()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->decContrast()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_b
    const-string v2, "onClick: VTHighDown..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->decZoom()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecZoom()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_7
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    if-eqz v2, :cond_8

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->decBrightness()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecBrightness()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_8
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v2

    iget-boolean v2, v2, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/vt/VTManager;->decContrast()Z

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighUp:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canIncContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighDown:Landroid/widget/ImageButton;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mediatek/vt/VTManager;->canDecContrast()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_0

    :pswitch_c
    const-string v2, "onClick: VTOverflowMenu..."

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_9
    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-direct {p0, v2}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->constructPopupMenu(Landroid/view/View;)Landroid/widget/PopupMenu;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f08010f
        :pswitch_1
        :pswitch_9
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_c
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_a
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 3

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->unregisterForVTPhoneStates()V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_2
    const-string v0, "closeVTmanger VTInCallScreenLoopback:onDestory"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTInControlRes()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VTInCallScreenLoopback:onDestory:sentIntent:VTCallUtils.VT_CALL_END"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.phone.extra.VT_CALL_END"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTInControlRes(Z)V

    goto :goto_0
.end method

.method public onDismiss(Landroid/widget/PopupMenu;)V
    .locals 2
    .param p1    # Landroid/widget/PopupMenu;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "- onDismiss: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopupVisible:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v0, "onKeyDown"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mExtension:Lcom/mediatek/phone/ext/VTInCallScreenExtension;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/phone/ext/VTInCallScreenExtension;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-virtual {p0, p1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->handleOnScreenMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-virtual {p0, p1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->handleOnScreenMenuItemClick(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    sget-object v1, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_OPEN:Lcom/android/phone/Constants$VTScreenMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mExtension:Lcom/mediatek/phone/ext/VTInCallScreenExtension;

    invoke-virtual {v0, p1}, Lcom/mediatek/phone/ext/VTInCallScreenExtension;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onReceiveVTManagerStartCounter()V
    .locals 5

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v1

    iget-object v1, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    iget-wide v1, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;->mStarttime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->hasActiveRingingCall()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getFirstActiveRingingCall()Lcom/android/internal/telephony/Call;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v1

    iget-object v1, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;->mStarttime:J

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v1

    iget-object v1, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTConnectionStarttime:Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;

    iget-object v2, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v2}, Lcom/android/internal/telephony/CallManager;->getActiveFgCall()Lcom/android/internal/telephony/Call;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/Call;->getLatestConnection()Lcom/android/internal/telephony/Connection;

    move-result-object v2

    iput-object v2, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback$VTConnectionStarttime;->mConnection:Lcom/android/internal/telephony/Connection;

    invoke-static {}, Lcom/android/phone/PhoneGlobals;->getInstance()Lcom/android/phone/PhoneGlobals;

    move-result-object v1

    iget-object v1, v1, Lcom/android/phone/PhoneGlobals;->notificationMgr:Lcom/android/phone/NotificationMgr;

    invoke-virtual {v1}, Lcom/android/phone/NotificationMgr;->updateInCallNotification()V

    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 3

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mCM:Lcom/android/internal/telephony/CallManager;

    invoke-virtual {v1}, Lcom/android/internal/telephony/CallManager;->getState()Lcom/android/internal/telephony/PhoneConstants$State;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStop: state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    sget-object v1, Lcom/android/internal/telephony/PhoneConstants$State;->IDLE:Lcom/android/internal/telephony/PhoneConstants$State;

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    invoke-virtual {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->setVTScreenMode(Lcom/android/phone/Constants$VTScreenMode;)V

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->resetVTFlags()V

    :cond_0
    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissAudioModePopup()V

    const-string v1, "VTInCallScreenLoopback:onStop"

    invoke-direct {p0, v1}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->closeVTManager()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mExtension:Lcom/mediatek/phone/ext/VTInCallScreenExtension;

    invoke-virtual {v0, p1, p2}, Lcom/mediatek/phone/ext/VTInCallScreenExtension;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "MotionEvent.ACTION_DOWN"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public refreshAudioModePopup()V
    .locals 1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopupVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mAudioModePopup:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showAudioModePopup()V

    :cond_0
    return-void
.end method

.method registerForVTPhoneStates()V
    .locals 2

    const-string v0, "- VTManager.getInstance().registerVTListener() start ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/VTManager;->registerVTListener(Landroid/os/Handler;)V

    const-string v0, "- VTManager.getInstance().registerVTListener() end ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    return-void
.end method

.method public resetVTFlags()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "resetVTFlags()..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->reset()V

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissVTDialogs()V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHighVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    return-void
.end method

.method setDialer(Lcom/android/phone/DTMFTwelveKeyDialer;)V
    .locals 0
    .param p1    # Lcom/android/phone/DTMFTwelveKeyDialer;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mDialer:Lcom/android/phone/DTMFTwelveKeyDialer;

    return-void
.end method

.method setVTLoopBackInstance(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public setVTScreenMode(Lcom/android/phone/Constants$VTScreenMode;)V
    .locals 2
    .param p1    # Lcom/android/phone/Constants$VTScreenMode;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setVTScreenMode : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v1

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    if-eq v0, p1, :cond_0

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->openVTInCallCanvas()V

    const-string v0, "setVTScreenMode : mVTWakeLock.acquire() "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v1

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    if-ne v0, p1, :cond_1

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->closeVTInCallCanvas()V

    const-string v0, "setVTScreenMode : mVTWakeLock.release() "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    iput-object p1, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTScreenMode:Lcom/android/phone/Constants$VTScreenMode;

    return-void
.end method

.method public setVTVisible(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    if-eqz v0, :cond_0

    const-string v0, "VTManager.getInstance().setVTVisible(true) start ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/VTManager;->setVTVisible(Z)V

    const-string v0, "VTManager.getInstance().setVTVisible(true) end ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "VTManager.getInstance().setVTVisible(false) start ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mediatek/vt/VTManager;->setVTVisible(Z)V

    const-string v0, "VTManager.getInstance().setVTVisible(false) start ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setupMenuItems(Landroid/view/Menu;)V
    .locals 12
    .param p1    # Landroid/view/Menu;

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-static {}, Lcom/android/phone/PhoneUtils;->isDMLocked()Z

    move-result v8

    if-eqz v8, :cond_0

    :goto_0
    return-void

    :cond_0
    const v8, 0x7f08013c

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    const v8, 0x7f08013d

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    const v8, 0x7f08013f

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const v8, 0x7f08013e

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    const v8, 0x7f080140

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    const v8, 0x7f080141

    invoke-interface {p1, v8}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/vt/VTManager;->getCameraSensorCount()I

    move-result v1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "setupMenuItems() : VTManager.getInstance().getCameraSensorCount() == "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const/4 v8, 0x2

    if-ne v8, v1, :cond_2

    move v8, v9

    :goto_1
    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v8

    iget-boolean v8, v8, Lcom/mediatek/settings/VTSettingUtils;->mEnableBackCamera:Z

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v8

    iget-boolean v8, v8, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    if-nez v8, :cond_3

    move v0, v9

    :goto_2
    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->isDialerOpened()Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v8

    iget-boolean v8, v8, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoConnected:Z

    if-eqz v8, :cond_4

    move v8, v9

    :goto_3
    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v8

    iget-boolean v8, v8, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    if-nez v8, :cond_5

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f0d0042

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    :goto_4
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v8

    iget-boolean v8, v8, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHasReceiveFirstFrame:Z

    invoke-interface {v3, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v7, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    const v8, 0x7f0d006f

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->okToRecordVoice()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v7, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->isVTRecording()Z

    move-result v8

    if-eqz v8, :cond_1

    const v8, 0x7f0d0070

    invoke-interface {v7, v8}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_1
    :goto_5
    invoke-interface {v6, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v8

    iget-boolean v8, v8, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTVideoConnected:Z

    invoke-interface {v6, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_2
    move v8, v10

    goto/16 :goto_1

    :cond_3
    move v0, v10

    goto :goto_2

    :cond_4
    move v8, v10

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v11, 0x7f0d0043

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_4

    :cond_6
    invoke-static {}, Lcom/mediatek/phone/DualTalkUtils;->isSupportDualTalk()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_5
.end method

.method public showReCallDialog(ILjava/lang/String;I)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const-string v0, "showReCallDialog... "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/settings/VTSettingUtils;->mAutoDropBack:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showToast(Ljava/lang/String;)V

    invoke-direct {p0, p2, p3}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->makeVoiceReCall(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->showReCallDialogEx(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method public stopRecord()V
    .locals 2

    const-string v0, "stopRecord"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorder;->isRecording()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "stopVoiceRecord"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->stopVoiceRecord()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-static {}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getInstance()Lcom/mediatek/phone/recording/PhoneRecorderHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/phone/recording/PhoneRecorderHandler;->getPhoneRecorderState()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, "stopVideoRecord"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->stopVideoRecord()V

    goto :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "surfaceChanged : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_0

    const-string v0, "surfaceChanged : HighVideo , set mVTSurfaceChangedH = true"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_1

    const-string v0, "surfaceChanged : LowVideo , set mVTSurfaceChangedL = true"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTLocalPeerDisplay()V

    const-string v0, "surfaceChanged : VTManager.getInstance().setVTVisible(true) start ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->setVTVisible(Z)V

    const-string v0, "surfaceChanged : VTManager.getInstance().setVTVisible(true) end ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_2
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSettingReady:Z

    if-eqz v0, :cond_3

    const-string v0, "- set VTManager ready ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->setVTReady()V

    const-string v0, "- finish set VTManager ready ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSettingReady:Z

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "surfacedChanged_afterReady_State= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->getState()Lcom/mediatek/vt/VTManager$State;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V

    :cond_4
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "surfaceCreated : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "surfaceDestroyed : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHighVideoHolder:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_0

    const-string v0, "surfaceDestroyed : HighVideo, set mVTSurfaceChangedH = false"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mLowVideoHolder:Landroid/view/SurfaceHolder;

    if-ne p1, v0, :cond_1

    const-string v0, "surfaceDestroyed : LowVideo, set mVTSurfaceChangedL = false"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedH:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTSurfaceChangedL:Z

    if-nez v0, :cond_2

    const-string v0, "surfaceDestroyed : VTManager.getInstance().setVTVisible(false) start ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/mediatek/vt/VTManager;->setVTVisible(Z)V

    const-string v0, "surfaceDestroyed : VTManager.getInstance().setVTVisible(false) end ..."

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_2
    return-void
.end method

.method unregisterForVTPhoneStates()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mediatek/vt/VTManager;->getmVTListener()Landroid/os/Handler;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const-string v0, "- mHandler does not equal to VTManager.getInstance().getmVTListener(), just return"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Lcom/mediatek/vt/VTManager;->setDisplay(Landroid/view/SurfaceHolder;Landroid/view/SurfaceHolder;)V

    const-string v0, "- VTManager.getInstance().unregisterVTListener() start ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/vt/VTManager;->getInstance()Lcom/mediatek/vt/VTManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mediatek/vt/VTManager;->unregisterVTListener()V

    const-string v0, "- VTManager.getInstance().unregisterVTListener() end ! "

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateVTScreen(Lcom/android/phone/Constants$VTScreenMode;)V
    .locals 4
    .param p1    # Lcom/android/phone/Constants$VTScreenMode;

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateVTScreen : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTLowVideo:Landroid/view/SurfaceView;

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTHangUp:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateVTScreen : VTInCallScreenFlagsLoopback.getInstance().mVTHideMeNow - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v1

    iget-boolean v1, v1, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateVTScreen : VTSettingUtils.getInstance().mEnableBackCamera - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/mediatek/settings/VTSettingUtils;->getInstance()Lcom/mediatek/settings/VTSettingUtils;

    move-result-object v1

    iget-boolean v1, v1, Lcom/mediatek/settings/VTSettingUtils;->mEnableBackCamera:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mediatek/phone/vt/VTCallUtils;->isVTActive()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->dismissVideoSettingDialogs()V

    sget-object v0, Lcom/android/phone/Constants$VTScreenMode;->VT_SCREEN_CLOSE:Lcom/android/phone/Constants$VTScreenMode;

    invoke-virtual {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->getVTScreenMode()Lcom/android/phone/Constants$VTScreenMode;

    move-result-object v1

    if-eq v0, v1, :cond_1

    :cond_1
    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iget-boolean v0, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTHideMeNow:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalZoomSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalBrightnessSetting:Z

    invoke-static {}, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->getInstance()Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;

    move-result-object v0

    iput-boolean v2, v0, Lcom/mediatek/vt/loopback/VTInCallScreenFlagsLoopback;->mVTInLocalContrastSetting:Z

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    :cond_2
    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->updateVTInCallButtons()V

    invoke-static {}, Lcom/android/phone/PhoneUtils;->isDMLocked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTDialpad:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTAudio:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTOverflowMenu:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTSwapVideo:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->hideLocalZoomOrBrightness()V

    :cond_3
    const-string v0, "updateVTScreen end"

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateVideoCallRecordState(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "updateVideoCallRecordState(), state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/mediatek/vt/loopback/VTInCallScreenLoopback;->mVTVoiceRecordingIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
