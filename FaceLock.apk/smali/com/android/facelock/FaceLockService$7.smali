.class Lcom/android/facelock/FaceLockService$7;
.super Ljava/lang/Object;
.source "FaceLockService.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/FaceLockService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/FaceLockService;


# direct methods
.method constructor <init>(Lcom/android/facelock/FaceLockService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/FaceLockService$7;->this$0:Lcom/android/facelock/FaceLockService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$7;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v0

    const/high16 v1, 0x3f800000

    iput v1, v0, Lcom/android/facelock/Spotlight;->mSize:F

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$7;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mBackground:Landroid/view/View;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$1500(Lcom/android/facelock/FaceLockService;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$7;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/facelock/Spotlight;->invalidate()V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/android/facelock/FaceLockService$7;->this$0:Lcom/android/facelock/FaceLockService;

    # getter for: Lcom/android/facelock/FaceLockService;->mSpotlight:Lcom/android/facelock/Spotlight;
    invoke-static {v0}, Lcom/android/facelock/FaceLockService;->access$1700(Lcom/android/facelock/FaceLockService;)Lcom/android/facelock/Spotlight;

    move-result-object v0

    iget-object v0, v0, Lcom/android/facelock/Spotlight;->mIntroAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    return-void
.end method
