.class Lcom/android/facelock/Draw;
.super Landroid/view/View;
.source "Draw.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/facelock/Draw$DrawListener;
    }
.end annotation


# static fields
.field private static final MAX_PROGRESS_COUNT:F

.field private static final POINTS:[[F


# instance fields
.field DRAW_CHECK:I

.field DRAW_COMPLETE:I

.field DRAW_INITIAL:I

.field DRAW_PROGRESS:I

.field private mCheckAlpha:I

.field private mCheckAlphaAnim:Landroid/animation/ValueAnimator;

.field private mCheckMark:Landroid/graphics/Bitmap;

.field mCount:I

.field volatile mEnrolling:Z

.field private mFinishAlpha:I

.field private mFinishGlow:Landroid/graphics/Bitmap;

.field private mGlowPaint:Landroid/graphics/Paint;

.field mLastElapsedTime:J

.field private mListener:Lcom/android/facelock/Draw$DrawListener;

.field private mLoadAlpha:I

.field mMaxCount:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPointAlpha:F

.field private mPointAlphaAnim:Landroid/animation/ValueAnimator;

.field private mProgress:F

.field private mProgressCount:I

.field volatile mStartTime:J

.field private mState:I

.field private mWhiteDot:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    const/16 v0, 0x1f

    new-array v0, v0, [[F

    const/4 v1, 0x0

    new-array v2, v3, [F

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [F

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    new-array v1, v3, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    const/4 v1, 0x3

    new-array v2, v3, [F

    fill-array-data v2, :array_3

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [F

    fill-array-data v2, :array_4

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [F

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [F

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [F

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [F

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [F

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [F

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [F

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v3, [F

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v3, [F

    fill-array-data v2, :array_d

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v3, [F

    fill-array-data v2, :array_e

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v3, [F

    fill-array-data v2, :array_f

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v3, [F

    fill-array-data v2, :array_10

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v3, [F

    fill-array-data v2, :array_11

    aput-object v2, v0, v1

    const/16 v1, 0x12

    new-array v2, v3, [F

    fill-array-data v2, :array_12

    aput-object v2, v0, v1

    const/16 v1, 0x13

    new-array v2, v3, [F

    fill-array-data v2, :array_13

    aput-object v2, v0, v1

    const/16 v1, 0x14

    new-array v2, v3, [F

    fill-array-data v2, :array_14

    aput-object v2, v0, v1

    const/16 v1, 0x15

    new-array v2, v3, [F

    fill-array-data v2, :array_15

    aput-object v2, v0, v1

    const/16 v1, 0x16

    new-array v2, v3, [F

    fill-array-data v2, :array_16

    aput-object v2, v0, v1

    const/16 v1, 0x17

    new-array v2, v3, [F

    fill-array-data v2, :array_17

    aput-object v2, v0, v1

    const/16 v1, 0x18

    new-array v2, v3, [F

    fill-array-data v2, :array_18

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v2, v3, [F

    fill-array-data v2, :array_19

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    new-array v2, v3, [F

    fill-array-data v2, :array_1a

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    new-array v2, v3, [F

    fill-array-data v2, :array_1b

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    new-array v2, v3, [F

    fill-array-data v2, :array_1c

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    new-array v2, v3, [F

    fill-array-data v2, :array_1d

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    new-array v2, v3, [F

    fill-array-data v2, :array_1e

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/facelock/Draw;->POINTS:[[F

    sget-object v0, Lcom/android/facelock/Draw;->POINTS:[[F

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    int-to-float v0, v0

    sput v0, Lcom/android/facelock/Draw;->MAX_PROGRESS_COUNT:F

    return-void

    :array_0
    .array-data 4
        0x0
        -0x40800000
    .end array-data

    :array_1
    .array-data 4
        0x3e23d70a
        -0x40851eb8
    .end array-data

    :array_2
    .array-data 4
        0x3eae147b
        -0x40947ae1
    .end array-data

    :array_3
    .array-data 4
        0x3f000000
        -0x40ae147b
    .end array-data

    :array_4
    .array-data 4
        0x3f23d70a
        -0x40cccccd
    .end array-data

    :array_5
    .array-data 4
        0x3f3d70a4
        -0x40f5c28f
    .end array-data

    :array_6
    .array-data 4
        0x3f47ae14
        -0x4147ae14
    .end array-data

    :array_7
    .array-data 4
        0x3f4ccccd
        -0x41c7ae14
    .end array-data

    :array_8
    .array-data 4
        0x3f47ae14
        0x0
    .end array-data

    :array_9
    .array-data 4
        0x3f400000
        0x3e4ccccd
    .end array-data

    :array_a
    .array-data 4
        0x3f333333
        0x3ebd70a4
    .end array-data

    :array_b
    .array-data 4
        0x3f1eb852
        0x3f0a3d71
    .end array-data

    :array_c
    .array-data 4
        0x3f051eb8
        0x3f35c28f
    .end array-data

    :array_d
    .array-data 4
        0x3ecccccd
        0x3f547ae1
    .end array-data

    :array_e
    .array-data 4
        0x3e851eb8
        0x3f70a3d7
    .end array-data

    :array_f
    .array-data 4
        0x3da3d70a
        0x3f800000
    .end array-data

    :array_10
    .array-data 4
        -0x425c28f6
        0x3f800000
    .end array-data

    :array_11
    .array-data 4
        -0x417ae148
        0x3f70a3d7
    .end array-data

    :array_12
    .array-data 4
        -0x41333333
        0x3f547ae1
    .end array-data

    :array_13
    .array-data 4
        -0x40fae148
        0x3f35c28f
    .end array-data

    :array_14
    .array-data 4
        -0x40e147ae
        0x3f0a3d71
    .end array-data

    :array_15
    .array-data 4
        -0x40cccccd
        0x3ebd70a4
    .end array-data

    :array_16
    .array-data 4
        -0x40c00000
        0x3e4ccccd
    .end array-data

    :array_17
    .array-data 4
        -0x40b851ec
        0x0
    .end array-data

    :array_18
    .array-data 4
        -0x40b33333
        -0x41c7ae14
    .end array-data

    :array_19
    .array-data 4
        -0x40b851ec
        -0x4147ae14
    .end array-data

    :array_1a
    .array-data 4
        -0x40c28f5c
        -0x40f5c28f
    .end array-data

    :array_1b
    .array-data 4
        -0x40dc28f6
        -0x40cccccd
    .end array-data

    :array_1c
    .array-data 4
        -0x41000000
        -0x40ae147b
    .end array-data

    :array_1d
    .array-data 4
        -0x4151eb85
        -0x40947ae1
    .end array-data

    :array_1e
    .array-data 4
        -0x41dc28f6
        -0x40851eb8
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const-wide/16 v6, 0x320

    const/4 v5, -0x1

    const/4 v2, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v3, p0, Lcom/android/facelock/Draw;->DRAW_INITIAL:I

    iput v2, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    iput v4, p0, Lcom/android/facelock/Draw;->DRAW_CHECK:I

    const/4 v1, 0x3

    iput v1, p0, Lcom/android/facelock/Draw;->DRAW_COMPLETE:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/facelock/Draw;->mGlowPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setColor(I)V

    iput v3, p0, Lcom/android/facelock/Draw;->mCount:I

    iput v2, p0, Lcom/android/facelock/Draw;->mMaxCount:I

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/android/facelock/Draw;->setProgress(F)V

    const/high16 v1, 0x3f800000

    iput v1, p0, Lcom/android/facelock/Draw;->mPointAlpha:F

    iput v3, p0, Lcom/android/facelock/Draw;->mLoadAlpha:I

    const/16 v1, 0xff

    iput v1, p0, Lcom/android/facelock/Draw;->mFinishAlpha:I

    iput v3, p0, Lcom/android/facelock/Draw;->mCheckAlpha:I

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/facelock/Draw;->mStartTime:J

    const-wide/16 v1, 0x2710

    iput-wide v1, p0, Lcom/android/facelock/Draw;->mLastElapsedTime:J

    iput-boolean v3, p0, Lcom/android/facelock/Draw;->mEnrolling:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/facelock/Draw;->mListener:Lcom/android/facelock/Draw$DrawListener;

    iget v1, p0, Lcom/android/facelock/Draw;->DRAW_INITIAL:I

    iput v1, p0, Lcom/android/facelock/Draw;->mState:I

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020003

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/facelock/Draw;->mFinishGlow:Landroid/graphics/Bitmap;

    const v1, 0x7f020002

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/facelock/Draw;->mWhiteDot:Landroid/graphics/Bitmap;

    const/high16 v1, 0x7f020000

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/android/facelock/Draw;->mCheckMark:Landroid/graphics/Bitmap;

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/android/facelock/Draw$1;

    invoke-direct {v2, p0}, Lcom/android/facelock/Draw$1;-><init>(Lcom/android/facelock/Draw;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v1, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v5}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    iget-object v1, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    iget-object v1, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/android/facelock/Draw;->mCheckAlphaAnim:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/android/facelock/Draw;->mCheckAlphaAnim:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/android/facelock/Draw$2;

    invoke-direct {v2, p0}, Lcom/android/facelock/Draw$2;-><init>(Lcom/android/facelock/Draw;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, p0, Lcom/android/facelock/Draw;->mCheckAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v1, p0, Lcom/android/facelock/Draw;->mCheckAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000
        0x3e19999a
    .end array-data

    :array_1
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method static synthetic access$002(Lcom/android/facelock/Draw;F)F
    .locals 0
    .param p0    # Lcom/android/facelock/Draw;
    .param p1    # F

    iput p1, p0, Lcom/android/facelock/Draw;->mPointAlpha:F

    return p1
.end method

.method static synthetic access$102(Lcom/android/facelock/Draw;I)I
    .locals 0
    .param p0    # Lcom/android/facelock/Draw;
    .param p1    # I

    iput p1, p0, Lcom/android/facelock/Draw;->mCheckAlpha:I

    return p1
.end method

.method static synthetic access$202(Lcom/android/facelock/Draw;I)I
    .locals 0
    .param p0    # Lcom/android/facelock/Draw;
    .param p1    # I

    iput p1, p0, Lcom/android/facelock/Draw;->mState:I

    return p1
.end method

.method static synthetic access$300(Lcom/android/facelock/Draw;)Lcom/android/facelock/Draw$DrawListener;
    .locals 1
    .param p0    # Lcom/android/facelock/Draw;

    iget-object v0, p0, Lcom/android/facelock/Draw;->mListener:Lcom/android/facelock/Draw$DrawListener;

    return-object v0
.end method


# virtual methods
.method beginGlow()V
    .locals 1

    sget v0, Lcom/android/facelock/Draw;->MAX_PROGRESS_COUNT:F

    invoke-virtual {p0, v0}, Lcom/android/facelock/Draw;->setProgress(F)V

    iget v0, p0, Lcom/android/facelock/Draw;->DRAW_CHECK:I

    iput v0, p0, Lcom/android/facelock/Draw;->mState:I

    iget-object v0, p0, Lcom/android/facelock/Draw;->mCheckAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method finish()V
    .locals 1

    iget v0, p0, Lcom/android/facelock/Draw;->mMaxCount:I

    iput v0, p0, Lcom/android/facelock/Draw;->mCount:I

    return-void
.end method

.method isDone()Z
    .locals 2

    iget v0, p0, Lcom/android/facelock/Draw;->mState:I

    iget v1, p0, Lcom/android/facelock/Draw;->DRAW_CHECK:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/facelock/Draw;->mState:I

    iget v1, p0, Lcom/android/facelock/Draw;->DRAW_COMPLETE:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method newFace()V
    .locals 4

    iget v2, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    iput v2, p0, Lcom/android/facelock/Draw;->mState:I

    iget-object v2, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->end()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/android/facelock/Draw;->mStartTime:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/android/facelock/Draw;->mLastElapsedTime:J

    iput-wide v0, p0, Lcom/android/facelock/Draw;->mStartTime:J

    iget v2, p0, Lcom/android/facelock/Draw;->mCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/facelock/Draw;->mCount:I

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->invalidate()V

    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 1
    .param p1    # Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/facelock/Draw;->mCheckAlpha:I

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->invalidate()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    iget-object v0, p0, Lcom/android/facelock/Draw;->mPointAlphaAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->isDone()Z

    move-result v10

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/facelock/Draw;->mCheckAlpha:I

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v10, p0, Lcom/android/facelock/Draw;->mCheckMark:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getWidth()I

    move-result v11

    iget-object v12, p0, Lcom/android/facelock/Draw;->mCheckMark:Landroid/graphics/Bitmap;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    sub-int/2addr v11, v12

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getHeight()I

    move-result v12

    iget-object v13, p0, Lcom/android/facelock/Draw;->mCheckMark:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    sub-int/2addr v12, v13

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    iget-object v13, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v11, v12, v13}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    iget v10, p0, Lcom/android/facelock/Draw;->mState:I

    iget v11, p0, Lcom/android/facelock/Draw;->DRAW_INITIAL:I

    if-ne v10, v11, :cond_5

    iget-object v10, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/facelock/Draw;->mPointAlpha:F

    const/high16 v12, 0x437f0000

    mul-float/2addr v11, v12

    float-to-int v11, v11

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_1
    :goto_1
    const/4 v6, 0x0

    :goto_2
    sget-object v10, Lcom/android/facelock/Draw;->POINTS:[[F

    array-length v10, v10

    if-ge v6, v10, :cond_a

    sget-object v10, Lcom/android/facelock/Draw;->POINTS:[[F

    aget-object v10, v10, v6

    const/4 v11, 0x0

    aget v10, v10, v11

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getWidth()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v10, v11

    const v11, 0x3ebd70a4

    mul-float/2addr v10, v11

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float v4, v10, v11

    sget-object v10, Lcom/android/facelock/Draw;->POINTS:[[F

    aget-object v10, v10, v6

    const/4 v11, 0x1

    aget v10, v10, v11

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getWidth()I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v10, v11

    const v11, 0x3ebd70a4

    mul-float/2addr v10, v11

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->getHeight()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    int-to-float v11, v11

    add-float v5, v10, v11

    iget-object v10, p0, Lcom/android/facelock/Draw;->mFinishGlow:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float v0, v4, v10

    iget-object v10, p0, Lcom/android/facelock/Draw;->mFinishGlow:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float v1, v5, v10

    iget-object v10, p0, Lcom/android/facelock/Draw;->mWhiteDot:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float v8, v4, v10

    iget-object v10, p0, Lcom/android/facelock/Draw;->mWhiteDot:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    div-int/lit8 v10, v10, 0x2

    int-to-float v10, v10

    sub-float v9, v5, v10

    iget-object v10, p0, Lcom/android/facelock/Draw;->mWhiteDot:Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v8, v9, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget v10, p0, Lcom/android/facelock/Draw;->mState:I

    iget v11, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    if-ne v10, v11, :cond_2

    iget v10, p0, Lcom/android/facelock/Draw;->mProgressCount:I

    add-int/lit8 v10, v10, -0x2

    if-ge v6, v10, :cond_9

    iget-object v10, p0, Lcom/android/facelock/Draw;->mGlowPaint:Landroid/graphics/Paint;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_2
    :goto_3
    iget v10, p0, Lcom/android/facelock/Draw;->mState:I

    iget v11, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    if-lt v10, v11, :cond_3

    iget-object v10, p0, Lcom/android/facelock/Draw;->mFinishGlow:Landroid/graphics/Bitmap;

    iget-object v11, p0, Lcom/android/facelock/Draw;->mGlowPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v10, v0, v1, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    :cond_4
    iget v10, p0, Lcom/android/facelock/Draw;->mState:I

    iget v11, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    if-ne v10, v11, :cond_0

    iget-object v10, p0, Lcom/android/facelock/Draw;->mPaint:Landroid/graphics/Paint;

    const/16 v11, 0xff

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_0

    :cond_5
    iget-boolean v10, p0, Lcom/android/facelock/Draw;->mEnrolling:Z

    if-nez v10, :cond_6

    iget v10, p0, Lcom/android/facelock/Draw;->mCount:I

    iget v11, p0, Lcom/android/facelock/Draw;->mMaxCount:I

    if-lt v10, v11, :cond_1

    :cond_6
    iget v10, p0, Lcom/android/facelock/Draw;->mState:I

    iget v11, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    if-ne v10, v11, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/android/facelock/Draw;->mStartTime:J

    sub-long/2addr v10, v12

    long-to-float v3, v10

    iget-wide v10, p0, Lcom/android/facelock/Draw;->mLastElapsedTime:J

    long-to-float v10, v10

    div-float v2, v3, v10

    const/high16 v10, 0x3f800000

    cmpl-float v10, v2, v10

    if-lez v10, :cond_7

    const/high16 v2, 0x3f800000

    :cond_7
    iget v10, p0, Lcom/android/facelock/Draw;->mCount:I

    add-int/lit8 v10, v10, -0x1

    int-to-float v10, v10

    add-float/2addr v10, v2

    sget v11, Lcom/android/facelock/Draw;->MAX_PROGRESS_COUNT:F

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/android/facelock/Draw;->mMaxCount:I

    int-to-float v11, v11

    div-float v7, v10, v11

    iget v10, p0, Lcom/android/facelock/Draw;->mProgress:F

    const v11, 0x3e051eb8

    add-float/2addr v10, v11

    cmpl-float v10, v7, v10

    if-lez v10, :cond_8

    iget v10, p0, Lcom/android/facelock/Draw;->mProgress:F

    const v11, 0x3e051eb8

    add-float v7, v10, v11

    :cond_8
    iget v10, p0, Lcom/android/facelock/Draw;->mProgress:F

    cmpl-float v10, v7, v10

    if-lez v10, :cond_1

    invoke-virtual {p0, v7}, Lcom/android/facelock/Draw;->setProgress(F)V

    const/high16 v10, 0x437f0000

    iget v11, p0, Lcom/android/facelock/Draw;->mProgressCount:I

    int-to-float v11, v11

    sub-float v11, v7, v11

    mul-float/2addr v10, v11

    float-to-int v10, v10

    iput v10, p0, Lcom/android/facelock/Draw;->mLoadAlpha:I

    float-to-double v10, v7

    const-wide v12, 0x3f847ae147ae147bL

    add-double/2addr v10, v12

    sget v12, Lcom/android/facelock/Draw;->MAX_PROGRESS_COUNT:F

    float-to-double v12, v12

    cmpl-double v10, v10, v12

    if-ltz v10, :cond_1

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->beginGlow()V

    goto/16 :goto_1

    :cond_9
    iget v10, p0, Lcom/android/facelock/Draw;->mProgressCount:I

    add-int/lit8 v10, v10, -0x1

    if-ge v6, v10, :cond_3

    iget-object v10, p0, Lcom/android/facelock/Draw;->mGlowPaint:Landroid/graphics/Paint;

    iget v11, p0, Lcom/android/facelock/Draw;->mLoadAlpha:I

    invoke-virtual {v10, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_3

    :cond_a
    iget v10, p0, Lcom/android/facelock/Draw;->mState:I

    iget v11, p0, Lcom/android/facelock/Draw;->DRAW_PROGRESS:I

    if-ne v10, v11, :cond_b

    invoke-virtual {p0}, Lcom/android/facelock/Draw;->invalidate()V

    :cond_b
    return-void
.end method

.method setListener(Lcom/android/facelock/Draw$DrawListener;)V
    .locals 0
    .param p1    # Lcom/android/facelock/Draw$DrawListener;

    iput-object p1, p0, Lcom/android/facelock/Draw;->mListener:Lcom/android/facelock/Draw$DrawListener;

    return-void
.end method

.method setProgress(F)V
    .locals 1
    .param p1    # F

    float-to-int v0, p1

    iput v0, p0, Lcom/android/facelock/Draw;->mProgressCount:I

    iput p1, p0, Lcom/android/facelock/Draw;->mProgress:F

    return-void
.end method
