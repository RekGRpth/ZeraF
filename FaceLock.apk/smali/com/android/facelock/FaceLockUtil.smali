.class public final Lcom/android/facelock/FaceLockUtil;
.super Ljava/lang/Object;
.source "FaceLockUtil.java"


# static fields
.field private static GALLERY_FILE_NAME:Ljava/lang/String;

.field private static TEMP_FILE_NAME:Ljava/lang/String;

.field public static final sEnrollStateString:[Ljava/lang/String;

.field private static sGalleryFilename:Ljava/lang/String;

.field private static sGalleryVariance:[F

.field private static sHaveSettings:Z

.field private static final sLock:Ljava/lang/Object;

.field private static sMeanGallerySimilarity:[F

.field private static sNumEnrollments:[I

.field private static final sSetupSettings:Lcom/android/facelock/LockScreenSettings;

.field private static sTempGalleryFilename:Ljava/lang/String;

.field private static final sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

.field public static final sUnlockStateString:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ENROLL_STATE_FACE"

    aput-object v1, v0, v4

    const-string v1, "ENROLL_STATE_NO_FACE"

    aput-object v1, v0, v3

    const-string v1, "ENROLL_STATE_NO_LANDMARKS"

    aput-object v1, v0, v5

    const-string v1, "ENROLL_STATE_OFF_CENTER"

    aput-object v1, v0, v6

    const-string v1, "ENROLL_STATE_DONE"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "ENROLL_STATE_ERROR"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sEnrollStateString:[Ljava/lang/String;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "UNLOCK_STATE_ALLOW"

    aput-object v1, v0, v4

    const-string v1, "UNLOCK_STATE_DENY"

    aput-object v1, v0, v3

    const-string v1, "UNLOCK_STATE_MAYBE"

    aput-object v1, v0, v5

    const-string v1, "UNLOCK_STATE_NO_FACE"

    aput-object v1, v0, v6

    const-string v1, "UNLOCK_STATE_NO_LANDMARKS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "UNLOCK_STATE_FACE_OFF_CENTER"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "UNLOCK_STATE_LIVELINESS_START"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "UNLOCK_STATE_LIVELINESS_MOTION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "UNLOCK_STATE_ERROR"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockStateString:[Ljava/lang/String;

    const-string v0, "temp.gal"

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->TEMP_FILE_NAME:Ljava/lang/String;

    const-string v0, "lockscreen.gal"

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->GALLERY_FILE_NAME:Ljava/lang/String;

    new-instance v0, Lcom/android/facelock/LockScreenSettings;

    invoke-direct {v0}, Lcom/android/facelock/LockScreenSettings;-><init>()V

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    new-instance v0, Lcom/android/facelock/LockScreenSettings;

    invoke-direct {v0}, Lcom/android/facelock/LockScreenSettings;-><init>()V

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sput-boolean v4, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    new-array v0, v3, [F

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sMeanGallerySimilarity:[F

    new-array v0, v3, [F

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sGalleryVariance:[F

    new-array v0, v3, [I

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sNumEnrollments:[I

    const-string v0, "facelock_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteGalleryFile(Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->GALLERY_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const-string v1, "FULFaceLockUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleting "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    goto :goto_0
.end method

.method public static deleteTempGalleryFile(Ljava/lang/String;)Z
    .locals 2
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->TEMP_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    goto :goto_0
.end method

.method private static native finalizeJni(Z)V
.end method

.method public static getGalleryVariance()F
    .locals 2

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sGalleryVariance:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public static getMeanGallerySimilarity()F
    .locals 2

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sMeanGallerySimilarity:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public static getNumEnrollments()I
    .locals 2

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sNumEnrollments:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private static getSettings(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 6
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;

    const/high16 v5, 0x41200000

    const v4, 0x400ccccd

    const/4 v1, 0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/android/facelock/FaceLockUtil;->TEMP_FILE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sTempGalleryFilename:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/android/facelock/FaceLockUtil;->GALLERY_FILE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/facelock/FaceLockUtil;->sGalleryFilename:Ljava/lang/String;

    sget-object v2, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v0, "facelock_use_n_max"

    invoke-static {p0, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v2, Lcom/android/facelock/LockScreenSettings;->use_n_max:Z

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_enrollment_threshold"

    const/high16 v3, 0x40000000

    invoke-static {p0, v2, v3}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v2

    iput v2, v0, Lcom/android/facelock/LockScreenSettings;->detection_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_recognition_threshold"

    invoke-static {p0, v2, v4}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v2

    iput v2, v0, Lcom/android/facelock/LockScreenSettings;->recognition_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_min_face_size"

    const/4 v3, 0x4

    invoke-static {p0, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/android/facelock/LockScreenSettings;->min_face_size:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_min_eye_distance"

    const/high16 v3, 0x41c80000

    invoke-static {p0, v2, v3}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v2

    iput v2, v0, Lcom/android/facelock/LockScreenSettings;->min_eye_distance:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_max_enroll_images"

    const/16 v3, 0xa

    invoke-static {p0, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/android/facelock/LockScreenSettings;->max_enroll_images:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_max_gallery_size"

    const/16 v3, 0x14

    invoke-static {p0, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/android/facelock/LockScreenSettings;->max_gallery_size:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v2, "facelock_num_threads"

    invoke-static {p0, v2, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->num_threads:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_min_image_width"

    const/16 v2, 0x78

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->min_image_width:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_min_image_height"

    const/16 v2, 0xa0

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->min_image_height:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_black_screen_threshold"

    const/16 v2, 0xf

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->black_screen_threshold:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_liveliness_threshold"

    const v2, 0x3e23d70a

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->liveliness_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_liveliness_recognition_threshold"

    invoke-static {p0, v1, v4}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->liveliness_recognition_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_small_eye_width"

    const/16 v2, 0x1e

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->small_eye_width:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_small_eye_height"

    const/16 v2, 0x12

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->small_eye_height:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_large_eye_width"

    const/16 v2, 0x2a

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->large_eye_width:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_large_eye_height"

    const/16 v2, 0x1a

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->large_eye_height:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_max_rotation_change"

    invoke-static {p0, v1, v5}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_rotation_change:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_max_scale_change"

    const v2, 0x3dcccccd

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_scale_change:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_max_center_movement"

    invoke-static {p0, v1, v5}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_center_movement:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget-boolean v1, v1, Lcom/android/facelock/LockScreenSettings;->use_n_max:Z

    iput-boolean v1, v0, Lcom/android/facelock/LockScreenSettings;->use_n_max:Z

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    const-string v1, "facelock_detection_threshold"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/provider/Settings$Secure;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v1

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->detection_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->recognition_threshold:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->recognition_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->min_face_size:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->min_face_size:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->min_eye_distance:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->min_eye_distance:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->max_enroll_images:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_enroll_images:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->max_gallery_size:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_gallery_size:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->num_threads:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->num_threads:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->min_image_width:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->min_image_width:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->min_image_height:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->min_image_height:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->black_screen_threshold:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->black_screen_threshold:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->liveliness_threshold:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->liveliness_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->liveliness_recognition_threshold:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->liveliness_recognition_threshold:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->small_eye_width:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->small_eye_width:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->small_eye_height:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->small_eye_height:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->large_eye_width:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->large_eye_width:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->large_eye_height:I

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->large_eye_height:I

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->max_rotation_change:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_rotation_change:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->max_scale_change:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_scale_change:F

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v1, v1, Lcom/android/facelock/LockScreenSettings;->max_center_movement:F

    iput v1, v0, Lcom/android/facelock/LockScreenSettings;->max_center_movement:F

    return-void

    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static declared-synchronized initialize(ZLandroid/content/ContentResolver;Ljava/lang/String;)Z
    .locals 4
    .param p0    # Z
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    const-class v1, Lcom/android/facelock/FaceLockUtil;

    monitor-enter v1

    if-nez p2, :cond_0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    :try_start_0
    sget-object v2, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    sget-boolean v0, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    if-nez v0, :cond_1

    invoke-static {p1, p2}, Lcom/android/facelock/FaceLockUtil;->getSettings(Landroid/content/ContentResolver;Ljava/lang/String;)V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p0, :cond_2

    :try_start_2
    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sMeanGallerySimilarity:[F

    const/4 v2, 0x0

    const/high16 v3, -0x3e600000

    aput v3, v0, v2

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sGalleryVariance:[F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v0, v2

    :cond_2
    if-eqz p0, :cond_3

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    :goto_1
    invoke-static {v0, p0}, Lcom/android/facelock/FaceLockUtil;->initializeJni(Lcom/android/facelock/LockScreenSettings;Z)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    :try_start_5
    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sUnlockSettings:Lcom/android/facelock/LockScreenSettings;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1
.end method

.method private static native initializeJni(Lcom/android/facelock/LockScreenSettings;Z)Z
.end method

.method public static moveGalleryFile(Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/android/facelock/FaceLockUtil;->TEMP_FILE_NAME:Ljava/lang/String;

    invoke-direct {v1, p0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    sget-object v3, Lcom/android/facelock/FaceLockUtil;->GALLERY_FILE_NAME:Ljava/lang/String;

    invoke-direct {v0, p0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setWriteFlagJni()V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static native setWriteFlagJni()V
.end method

.method public static setupEnroll([BIII)I
    .locals 1
    .param p0    # [B
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1, p2, p3}, Lcom/android/facelock/FaceLockUtil;->setupEnrollJni([BIII)I

    move-result v0

    return v0
.end method

.method private static native setupEnrollJni([BIII)I
.end method

.method public static setupFinalize()V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/facelock/FaceLockUtil;->finalizeJni(Z)V

    return-void
.end method

.method public static setupGetMaxEnrollImages()I
    .locals 2

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sSetupSettings:Lcom/android/facelock/LockScreenSettings;

    iget v0, v0, Lcom/android/facelock/LockScreenSettings;->max_enroll_images:I

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static setupReadGallery()Z
    .locals 2

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sGalleryFilename:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/facelock/FaceLockUtil;->setupReadGalleryJni(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static native setupReadGalleryJni(Ljava/lang/String;)Z
.end method

.method public static setupWriteGallery()Z
    .locals 2

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->setWriteFlagJni()V

    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sGalleryFilename:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/facelock/FaceLockUtil;->setupWriteGalleryJni(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static native setupWriteGalleryJni(Ljava/lang/String;)Z
.end method

.method public static setupWriteTempGallery()Z
    .locals 2

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/facelock/FaceLockUtil;->sTempGalleryFilename:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/facelock/FaceLockUtil;->setupWriteGalleryJni(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static unlockCompareFace([BIIZI[F[F)I
    .locals 1
    .param p0    # [B
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # I
    .param p5    # [F
    .param p6    # [F

    invoke-static/range {p0 .. p6}, Lcom/android/facelock/FaceLockUtil;->unlockCompareFaceJni([BIIZI[F[F)I

    move-result v0

    return v0
.end method

.method private static native unlockCompareFaceJni([BIIZI[F[F)I
.end method

.method public static unlockDetectLiveliness([BIII[Z[Z)I
    .locals 1
    .param p0    # [B
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [Z
    .param p5    # [Z

    invoke-static/range {p0 .. p5}, Lcom/android/facelock/FaceLockUtil;->unlockDetectLivelinessJni([BIII[Z[Z)I

    move-result v0

    return v0
.end method

.method private static native unlockDetectLivelinessJni([BIII[Z[Z)I
.end method

.method private static native unlockGetGalleryStatisticsJni([I[F[F)V
.end method

.method public static unlockIsBlackScreen([BII)Z
    .locals 1
    .param p0    # [B
    .param p1    # I
    .param p2    # I

    invoke-static {p0, p1, p2}, Lcom/android/facelock/FaceLockUtil;->unlockIsBlackScreenJni([BII)Z

    move-result v0

    return v0
.end method

.method private static native unlockIsBlackScreenJni([BII)Z
.end method

.method public static unlockLoadRecognitionModels()Z
    .locals 1

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->unlockLoadRecognitionModelsJni()Z

    move-result v0

    return v0
.end method

.method private static native unlockLoadRecognitionModelsJni()Z
.end method

.method public static unlockReadGallery()Z
    .locals 4

    sget-object v2, Lcom/android/facelock/FaceLockUtil;->sLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-boolean v1, Lcom/android/facelock/FaceLockUtil;->sHaveSettings:Z

    if-nez v1, :cond_0

    const/4 v0, 0x0

    monitor-exit v2

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sGalleryFilename:Ljava/lang/String;

    invoke-static {v1}, Lcom/android/facelock/FaceLockUtil;->unlockReadGalleryJni(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v1, Lcom/android/facelock/FaceLockUtil;->sNumEnrollments:[I

    sget-object v2, Lcom/android/facelock/FaceLockUtil;->sMeanGallerySimilarity:[F

    sget-object v3, Lcom/android/facelock/FaceLockUtil;->sGalleryVariance:[F

    invoke-static {v1, v2, v3}, Lcom/android/facelock/FaceLockUtil;->unlockGetGalleryStatisticsJni([I[F[F)V

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private static native unlockReadGalleryJni(Ljava/lang/String;)Z
.end method
