.class public Lcom/android/facelock/LockScreenSettings;
.super Ljava/lang/Object;
.source "LockScreenSettings.java"


# instance fields
.field public black_screen_threshold:I

.field public detection_threshold:F

.field public large_eye_height:I

.field public large_eye_width:I

.field public liveliness_recognition_threshold:F

.field public liveliness_threshold:F

.field public max_center_movement:F

.field public max_enroll_images:I

.field public max_gallery_size:I

.field public max_rotation_change:F

.field public max_scale_change:F

.field public min_eye_distance:F

.field public min_face_size:I

.field public min_image_height:I

.field public min_image_width:I

.field public num_threads:I

.field public recognition_threshold:F

.field public small_eye_height:I

.field public small_eye_width:I

.field public use_n_max:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
