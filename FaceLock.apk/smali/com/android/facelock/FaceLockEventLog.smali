.class public Lcom/android/facelock/FaceLockEventLog;
.super Ljava/lang/Object;
.source "FaceLockEventLog.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mCancelPressed:Z

.field private mDetectionTimeout:Z

.field public mDoneNeeded:Z

.field private mLivelinessFail:Z

.field private mLivelinessNoFace:Z

.field private mLivelinessTimeout:Z

.field private mLoginComplete:Z

.field private mMaxDetectionConfidence:F

.field private mMaxRecognitionConfidence:F

.field private mNoTemplate:Z

.field private mNumBlackFrames:I

.field private mNumMotionTriggered:I

.field private mOnCreateStartTime:J

.field private mRecognitionTimeout:Z

.field private mUseLiveliness:Z

.field private mWatchdogTimeout:Z


# direct methods
.method constructor <init>(J)V
    .locals 3
    .param p1    # J

    const/high16 v1, -0x40400000

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "FULFaceLockEventLog"

    iput-object v0, p0, Lcom/android/facelock/FaceLockEventLog;->TAG:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mUseLiveliness:Z

    iput v2, p0, Lcom/android/facelock/FaceLockEventLog;->mNumBlackFrames:I

    iput v1, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxRecognitionConfidence:F

    iput v1, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxDetectionConfidence:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/facelock/FaceLockEventLog;->mOnCreateStartTime:J

    iput v2, p0, Lcom/android/facelock/FaceLockEventLog;->mNumMotionTriggered:I

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mLoginComplete:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mDetectionTimeout:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mRecognitionTimeout:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessTimeout:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mWatchdogTimeout:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mCancelPressed:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mNoTemplate:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessFail:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessNoFace:Z

    iput-boolean v2, p0, Lcom/android/facelock/FaceLockEventLog;->mDoneNeeded:Z

    iput-wide p1, p0, Lcom/android/facelock/FaceLockEventLog;->mOnCreateStartTime:J

    return-void
.end method

.method private combineInts(III)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    shl-int v0, v1, p3

    if-lt p2, v0, :cond_0

    shl-int v0, v1, p3

    add-int/lit8 p2, v0, -0x1

    :cond_0
    shl-int v0, p1, p3

    add-int/2addr v0, p2

    return v0
.end method

.method private formatConfidence(F)I
    .locals 3
    .param p1    # F

    const/high16 v1, -0x40400000

    sub-float v1, p1, v1

    const/high16 v2, 0x42c80000

    mul-float/2addr v1, v2

    float-to-int v0, v1

    if-gez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x800

    if-lt v0, v1, :cond_0

    const/16 v0, 0x7ff

    goto :goto_0
.end method

.method private getDetectionInt()I
    .locals 3

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxDetectionConfidence:F

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockEventLog;->formatConfidence(F)I

    move-result v0

    iget v1, p0, Lcom/android/facelock/FaceLockEventLog;->mNumBlackFrames:I

    const/4 v2, 0x5

    invoke-direct {p0, v0, v1, v2}, Lcom/android/facelock/FaceLockEventLog;->combineInts(III)I

    move-result v0

    return v0
.end method

.method private getEnrollmentBits()I
    .locals 1

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->getNumEnrollments()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private getFormattedMeanGallerySimilarity()I
    .locals 1

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->getMeanGallerySimilarity()F

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockEventLog;->formatConfidence(F)I

    move-result v0

    return v0
.end method

.method private getNumMotionTriggered()I
    .locals 2

    const/4 v0, 0x7

    iget v1, p0, Lcom/android/facelock/FaceLockEventLog;->mNumMotionTriggered:I

    if-gt v1, v0, :cond_0

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mNumMotionTriggered:I

    :cond_0
    return v0
.end method

.method private getRecognitionInt()I
    .locals 3

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxRecognitionConfidence:F

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockEventLog;->formatConfidence(F)I

    move-result v0

    invoke-direct {p0}, Lcom/android/facelock/FaceLockEventLog;->getEnrollmentBits()I

    move-result v1

    const/4 v2, 0x5

    invoke-direct {p0, v0, v1, v2}, Lcom/android/facelock/FaceLockEventLog;->combineInts(III)I

    move-result v0

    return v0
.end method

.method private getVarianceInt()I
    .locals 4

    const v1, 0xffff

    invoke-static {}, Lcom/android/facelock/FaceLockUtil;->getGalleryVariance()F

    move-result v2

    const/high16 v3, 0x44000000

    mul-float/2addr v2, v3

    float-to-int v0, v2

    if-le v0, v1, :cond_0

    move v0, v1

    :cond_0
    return v0
.end method


# virtual methods
.method public blackFrame()V
    .locals 1

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mNumBlackFrames:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/facelock/FaceLockEventLog;->mNumBlackFrames:I

    return-void
.end method

.method public cancelPressed()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mCancelPressed:Z

    return-void
.end method

.method public detectionTimeout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mDetectionTimeout:Z

    return-void
.end method

.method public doneNeeded()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mDoneNeeded:Z

    return-void
.end method

.method public livelinessFail()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessFail:Z

    return-void
.end method

.method public livelinessNoFace()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessNoFace:Z

    return-void
.end method

.method public livelinessTimeout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessTimeout:Z

    return-void
.end method

.method public loginComplete()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mLoginComplete:Z

    return-void
.end method

.method public motionTriggered()V
    .locals 1

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mNumMotionTriggered:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/facelock/FaceLockEventLog;->mNumMotionTriggered:I

    return-void
.end method

.method public noTemplate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mNoTemplate:Z

    return-void
.end method

.method public recognitionTimeout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mRecognitionTimeout:Z

    return-void
.end method

.method public updateConfidences(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxRecognitionConfidence:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxRecognitionConfidence:F

    iput p2, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxDetectionConfidence:F

    :cond_0
    return-void
.end method

.method public updateDetectionConfidence(F)V
    .locals 2
    .param p1    # F

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxRecognitionConfidence:F

    const/high16 v1, -0x40400000

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxDetectionConfidence:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    iput p1, p0, Lcom/android/facelock/FaceLockEventLog;->mMaxDetectionConfidence:F

    :cond_0
    return-void
.end method

.method public useLiveliness()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mUseLiveliness:Z

    return-void
.end method

.method public watchdogTimeout()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/facelock/FaceLockEventLog;->mWatchdogTimeout:Z

    return-void
.end method

.method public writeEvent()V
    .locals 11

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v7

    iget-wide v9, p0, Lcom/android/facelock/FaceLockEventLog;->mOnCreateStartTime:J

    sub-long/2addr v7, v9

    long-to-int v0, v7

    const/16 v4, 0x1fff

    if-le v0, v4, :cond_0

    const/16 v0, 0x1fff

    :cond_0
    invoke-direct {p0}, Lcom/android/facelock/FaceLockEventLog;->getRecognitionInt()I

    move-result v4

    shl-int/lit8 v4, v4, 0x10

    invoke-direct {p0}, Lcom/android/facelock/FaceLockEventLog;->getDetectionInt()I

    move-result v7

    add-int v1, v4, v7

    invoke-direct {p0}, Lcom/android/facelock/FaceLockEventLog;->getFormattedMeanGallerySimilarity()I

    move-result v4

    shl-int/lit8 v4, v4, 0x15

    invoke-direct {p0}, Lcom/android/facelock/FaceLockEventLog;->getVarianceInt()I

    move-result v7

    shl-int/lit8 v7, v7, 0x5

    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mUseLiveliness:Z

    if-eqz v4, :cond_1

    const/16 v4, 0x10

    :goto_0
    add-int/2addr v4, v7

    invoke-direct {p0}, Lcom/android/facelock/FaceLockEventLog;->getNumMotionTriggered()I

    move-result v7

    shl-int/lit8 v7, v7, 0x1

    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mLoginComplete:Z

    if-eqz v4, :cond_2

    move v4, v6

    :goto_1
    add-int v2, v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mDoneNeeded:Z

    if-eqz v4, :cond_3

    const/high16 v4, -0x80000000

    :goto_2
    iget-boolean v7, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessNoFace:Z

    if-eqz v7, :cond_4

    const/high16 v7, 0x800000

    :goto_3
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessFail:Z

    if-eqz v4, :cond_5

    const/high16 v4, 0x400000

    :goto_4
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mNoTemplate:Z

    if-eqz v4, :cond_6

    const/high16 v4, 0x200000

    :goto_5
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mCancelPressed:Z

    if-eqz v4, :cond_7

    const/high16 v4, 0x100000

    :goto_6
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mWatchdogTimeout:Z

    if-eqz v4, :cond_8

    const/high16 v4, 0x80000

    :goto_7
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mLivelinessTimeout:Z

    if-eqz v4, :cond_9

    const/high16 v4, 0x40000

    :goto_8
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mRecognitionTimeout:Z

    if-eqz v4, :cond_a

    const/high16 v4, 0x20000

    :goto_9
    add-int/2addr v7, v4

    iget-boolean v4, p0, Lcom/android/facelock/FaceLockEventLog;->mDetectionTimeout:Z

    if-eqz v4, :cond_b

    const/high16 v4, 0x10000

    :goto_a
    add-int/2addr v4, v7

    shl-int/lit8 v7, v0, 0x3

    add-int v3, v4, v7

    const v4, 0x12cd8

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v7, v6

    const/4 v5, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v7, v5

    invoke-static {v4, v7}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    return-void

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    move v4, v5

    goto :goto_2

    :cond_4
    move v7, v5

    goto :goto_3

    :cond_5
    move v4, v5

    goto :goto_4

    :cond_6
    move v4, v5

    goto :goto_5

    :cond_7
    move v4, v5

    goto :goto_6

    :cond_8
    move v4, v5

    goto :goto_7

    :cond_9
    move v4, v5

    goto :goto_8

    :cond_a
    move v4, v5

    goto :goto_9

    :cond_b
    move v4, v5

    goto :goto_a
.end method
