.class public Lcom/android/facelock/FaceLockPreferences;
.super Ljava/lang/Object;
.source "FaceLockPreferences.java"


# instance fields
.field private mPreferenceEditor:Landroid/content/SharedPreferences$Editor;

.field private mPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.android.facelock_preferences"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferences:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method private clampCameraDelay(I)I
    .locals 2
    .param p1    # I

    move v0, p1

    const/16 v1, 0x1f4

    if-ge p1, v1, :cond_1

    const/16 v0, 0x1f4

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x7d0

    if-le p1, v1, :cond_0

    const/16 v0, 0x7d0

    goto :goto_0
.end method

.method private getStoredCameraDelay()I
    .locals 3

    iget-object v0, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "camera_delay"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getCameraDelay()I
    .locals 2

    invoke-direct {p0}, Lcom/android/facelock/FaceLockPreferences;->getStoredCameraDelay()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/facelock/FaceLockPreferences;->clampCameraDelay(I)I

    move-result v1

    return v1
.end method

.method public updateCameraDelay(I)V
    .locals 5
    .param p1    # I

    invoke-direct {p0}, Lcom/android/facelock/FaceLockPreferences;->getStoredCameraDelay()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v2, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v3, "camera_delay"

    invoke-direct {p0, p1}, Lcom/android/facelock/FaceLockPreferences;->clampCameraDelay(I)I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :goto_0
    iget-object v2, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_0
    int-to-float v2, v0

    const v3, 0x3f666666

    mul-float/2addr v2, v3

    int-to-float v3, p1

    const v4, 0x3dccccd0

    mul-float/2addr v3, v4

    add-float v1, v2, v3

    iget-object v2, p0, Lcom/android/facelock/FaceLockPreferences;->mPreferenceEditor:Landroid/content/SharedPreferences$Editor;

    const-string v3, "camera_delay"

    float-to-int v4, v1

    invoke-direct {p0, v4}, Lcom/android/facelock/FaceLockPreferences;->clampCameraDelay(I)I

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method
