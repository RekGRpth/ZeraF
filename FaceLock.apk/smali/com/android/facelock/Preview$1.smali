.class Lcom/android/facelock/Preview$1;
.super Ljava/lang/Thread;
.source "Preview.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/facelock/Preview;->startPreviewIfReady()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/facelock/Preview;


# direct methods
.method constructor <init>(Lcom/android/facelock/Preview;)V
    .locals 0

    iput-object p1, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v2, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    # getter for: Lcom/android/facelock/Preview;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/facelock/Preview;->access$000(Lcom/android/facelock/Preview;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    # getter for: Lcom/android/facelock/Preview;->mSurfaceAvailable:Z
    invoke-static {v2}, Lcom/android/facelock/Preview;->access$100(Lcom/android/facelock/Preview;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    # getter for: Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/android/facelock/Preview;->access$200(Lcom/android/facelock/Preview;)Landroid/hardware/Camera;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    # getter for: Lcom/android/facelock/Preview;->mCamera:Landroid/hardware/Camera;
    invoke-static {v2}, Lcom/android/facelock/Preview;->access$200(Lcom/android/facelock/Preview;)Landroid/hardware/Camera;

    move-result-object v2

    iget-object v4, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    # getter for: Lcom/android/facelock/Preview;->mTextureView:Landroid/view/TextureView;
    invoke-static {v4}, Lcom/android/facelock/Preview;->access$300(Lcom/android/facelock/Preview;)Landroid/view/TextureView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    iget-object v2, p0, Lcom/android/facelock/Preview$1;->this$0:Lcom/android/facelock/Preview;

    # invokes: Lcom/android/facelock/Preview;->setupPreview()V
    invoke-static {v2}, Lcom/android/facelock/Preview;->access$400(Lcom/android/facelock/Preview;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v3

    return-void

    :catch_0
    move-exception v1

    const-string v2, "FULPreview"

    const-string v4, "IOException caused by onSurfaceTextureAvailable()"

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :catch_1
    move-exception v0

    :try_start_3
    const-string v2, "FULPreview"

    const-string v4, "Runtime Exception during Preview startup"

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
