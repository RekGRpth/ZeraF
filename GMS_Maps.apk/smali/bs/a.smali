.class public Lbs/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/util/ArrayList;

.field private final d:Lbu/f;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private g:I

.field private h:I

.field private i:I

.field private j:F

.field private k:F


# direct methods
.method public constructor <init>(II)V
    .locals 4

    const/16 v3, 0xff

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbs/a;->e:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbs/a;->f:Landroid/graphics/Paint;

    iput v2, p0, Lbs/a;->g:I

    iput v2, p0, Lbs/a;->h:I

    const/16 v0, 0x14

    iput v0, p0, Lbs/a;->i:I

    iput p1, p0, Lbs/a;->a:I

    iput p2, p0, Lbs/a;->b:I

    iget-object v0, p0, Lbs/a;->e:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lbs/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lbs/a;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lbs/a;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lbs/a;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Lbu/f;

    int-to-float v1, p1

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    int-to-float v2, p2

    const/high16 v3, 0x3f400000

    mul-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lbu/f;-><init>(FF)V

    iput-object v0, p0, Lbs/a;->d:Lbu/f;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    return-void
.end method

.method private b()V
    .locals 4

    const/high16 v3, 0x40000000

    const v0, 0x3dcccccd

    iget v1, p0, Lbs/a;->a:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lbs/a;->a:I

    int-to-float v1, v1

    sub-float/2addr v1, v0

    div-float/2addr v1, v3

    iget v2, p0, Lbs/a;->j:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iget v1, p0, Lbs/a;->a:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    sub-float/2addr v0, v1

    iput v0, p0, Lbs/a;->k:F

    return-void
.end method

.method private b(Landroid/graphics/Canvas;Z)V
    .locals 4

    const/high16 v2, 0x3f000000

    const/16 v1, 0xff

    iget-object v0, p0, Lbs/a;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lbs/a;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    if-eqz p2, :cond_0

    iput v2, p0, Lbs/a;->j:F

    :cond_0
    iget v0, p0, Lbs/a;->j:F

    sub-float/2addr v0, v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget-object v1, p0, Lbs/a;->d:Lbu/f;

    iget v2, p0, Lbs/a;->a:I

    int-to-float v2, v2

    const/high16 v3, 0x40000000

    div-float/2addr v2, v3

    add-float/2addr v0, v2

    iget-object v2, p0, Lbs/a;->d:Lbu/f;

    iget v2, v2, Lbu/f;->b:F

    invoke-virtual {v1, v0, v2}, Lbu/f;->a(FF)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lbs/a;->g:I

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbs/b;

    invoke-virtual {v0}, Lbs/b;->b()V

    iget v0, p0, Lbs/a;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbs/a;->g:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lbs/a;->j:F

    return-void
.end method

.method public a(Landroid/graphics/Canvas;Z)V
    .locals 4

    const/4 v2, 0x0

    iget v0, p0, Lbs/a;->h:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lbs/a;->h:I

    iget v1, p0, Lbs/a;->i:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lbs/a;->g:I

    if-lez v0, :cond_1

    move v1, v2

    :goto_0
    iget v0, p0, Lbs/a;->g:I

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbs/b;

    invoke-virtual {v0}, Lbs/b;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lbs/b;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget v0, p0, Lbs/a;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbs/a;->g:I

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lbs/a;->b()V

    invoke-direct {p0, p1, p2}, Lbs/a;->b(Landroid/graphics/Canvas;Z)V

    iget v0, p0, Lbs/a;->k:F

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbs/b;

    iget-object v2, p0, Lbs/a;->d:Lbu/f;

    invoke-virtual {v0, v2}, Lbs/b;->a(Lbu/f;)V

    invoke-virtual {v0, p1}, Lbs/b;->a(Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public a(Lbs/c;)V
    .locals 8

    const/4 v1, 0x0

    iput v1, p0, Lbs/a;->g:I

    iput v1, p0, Lbs/a;->h:I

    invoke-virtual {p1}, Lbs/c;->a()I

    move-result v0

    iget-object v2, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v2, v0, v2

    if-lez v2, :cond_0

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lbs/a;->c:Ljava/util/ArrayList;

    new-instance v4, Lbs/b;

    iget v5, p0, Lbs/a;->a:I

    iget v6, p0, Lbs/a;->b:I

    iget-object v7, p0, Lbs/a;->d:Lbu/f;

    invoke-direct {v4, v5, v6, v7}, Lbs/b;-><init>(IILbu/f;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-gez v2, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v3, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget v3, p0, Lbs/a;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lbs/a;->g:I

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lbs/a;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbs/b;

    add-int/lit8 v2, v1, 0x1

    iget v4, p0, Lbs/a;->g:I

    if-le v1, v4, :cond_4

    invoke-virtual {v0}, Lbs/b;->e()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lbs/b;->a()V

    :cond_3
    :goto_3
    move v1, v2

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lbs/b;->b()V

    goto :goto_3

    :cond_5
    return-void
.end method
