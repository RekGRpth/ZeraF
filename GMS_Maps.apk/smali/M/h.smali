.class LM/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field a:D

.field b:D

.field c:D

.field d:D

.field e:D

.field f:Lq/c;

.field g:LM/i;

.field h:LO/D;

.field i:D

.field j:Lo/T;

.field k:I


# direct methods
.method constructor <init>(ILq/c;LM/i;LR/m;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LM/h;->k:I

    iput-object p2, p0, LM/h;->f:Lq/c;

    iput-object p3, p0, LM/h;->g:LM/i;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LM/h;->j:Lo/T;

    invoke-virtual {p2}, Lq/c;->c()Lo/T;

    move-result-object v0

    invoke-virtual {p2}, Lq/c;->d()Lo/T;

    move-result-object v1

    iget-object v2, p3, LM/i;->d:Lo/T;

    const/4 v3, 0x1

    iget-object v4, p0, LM/h;->j:Lo/T;

    invoke-static {v0, v1, v2, v3, v4}, Lo/T;->a(Lo/T;Lo/T;Lo/T;ZLo/T;)V

    invoke-virtual {p2}, Lq/c;->c()Lo/T;

    move-result-object v0

    invoke-virtual {p2}, Lq/c;->d()Lo/T;

    move-result-object v1

    iget-object v2, p0, LM/h;->j:Lo/T;

    invoke-static {v0, v1, v2}, Lo/T;->d(Lo/T;Lo/T;Lo/T;)F

    move-result v0

    float-to-double v0, v0

    iput-wide v0, p0, LM/h;->i:D

    invoke-virtual {p2}, Lq/c;->b()Lo/af;

    move-result-object v0

    iget-object v1, p0, LM/h;->j:Lo/T;

    iget-object v2, p0, LM/h;->f:Lq/c;

    invoke-virtual {v2}, Lq/c;->e()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {p3, v1, v2, v3, v0}, LM/i;->a(Lo/T;DLo/af;)D

    move-result-wide v0

    iput-wide v0, p0, LM/h;->c:D

    invoke-virtual {p4}, LR/m;->a()D

    move-result-wide v0

    iput-wide v0, p0, LM/h;->d:D

    return-void
.end method

.method private a(Ljava/text/DecimalFormat;D)Ljava/lang/String;
    .locals 2

    const-wide/16 v0, 0x0

    cmpl-double v0, p2, v0

    if-nez v0, :cond_0

    const-string v0, "-inf"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2, p3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method a()D
    .locals 2

    iget-wide v0, p0, LM/h;->a:D

    return-wide v0
.end method

.method a(Z)D
    .locals 5

    iget-wide v0, p0, LM/h;->c:D

    iget-wide v2, p0, LM/h;->d:D

    mul-double/2addr v0, v2

    iput-wide v0, p0, LM/h;->a:D

    if-eqz p1, :cond_0

    iget-object v0, p0, LM/h;->g:LM/i;

    iget-object v1, p0, LM/h;->j:Lo/T;

    iget-object v2, p0, LM/h;->f:Lq/c;

    invoke-virtual {v2}, Lq/c;->e()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, LM/i;->a(Lo/T;D)LO/D;

    move-result-object v0

    iput-object v0, p0, LM/h;->h:LO/D;

    iget-object v0, p0, LM/h;->h:LO/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/h;->g:LM/i;

    iget-object v1, p0, LM/h;->h:LO/D;

    iget-object v2, p0, LM/h;->j:Lo/T;

    iget-object v3, p0, LM/h;->f:Lq/c;

    invoke-virtual {v3}, Lq/c;->e()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v0, v1, v2, v3, v4}, LM/i;->a(LO/D;Lo/T;D)D

    move-result-wide v0

    iput-wide v0, p0, LM/h;->b:D

    iget-wide v0, p0, LM/h;->a:D

    iget-object v2, p0, LM/h;->g:LM/i;

    iget-wide v3, p0, LM/h;->b:D

    invoke-virtual {v2, v3, v4}, LM/i;->b(D)D

    move-result-wide v2

    mul-double/2addr v0, v2

    iput-wide v0, p0, LM/h;->a:D

    :cond_0
    iget-wide v0, p0, LM/h;->a:D

    return-wide v0
.end method

.method public a(LM/h;)I
    .locals 4

    iget-wide v0, p0, LM/h;->a:D

    iget-wide v2, p1, LM/h;->a:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, LM/h;->a:D

    iget-wide v2, p1, LM/h;->a:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(D)V
    .locals 2

    iget-wide v0, p0, LM/h;->a:D

    div-double/2addr v0, p1

    iput-wide v0, p0, LM/h;->a:D

    return-void
.end method

.method b()V
    .locals 4

    iget-wide v0, p0, LM/h;->d:D

    iget-wide v2, p0, LM/h;->e:D

    add-double/2addr v0, v2

    iput-wide v0, p0, LM/h;->d:D

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LM/h;->e:D

    return-void
.end method

.method b(D)V
    .locals 2

    iget-wide v0, p0, LM/h;->e:D

    cmpl-double v0, p1, v0

    if-lez v0, :cond_0

    iput-wide p1, p0, LM/h;->e:D

    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    const-wide/high16 v0, 0x3ff0000000000000L

    iput-wide v0, p0, LM/h;->d:D

    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, LM/h;

    invoke-virtual {p0, p1}, LM/h;->a(LM/h;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "#.##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Candidate[id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LM/h;->k:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",L:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LM/h;->a:D

    invoke-direct {p0, v0, v2, v3}, LM/h;->a(Ljava/text/DecimalFormat;D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",OnRouteL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LM/h;->b:D

    invoke-direct {p0, v0, v2, v3}, LM/h;->a(Ljava/text/DecimalFormat;D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",EmitL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LM/h;->c:D

    invoke-direct {p0, v0, v2, v3}, LM/h;->a(Ljava/text/DecimalFormat;D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",TransL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, LM/h;->d:D

    invoke-direct {p0, v0, v2, v3}, LM/h;->a(Ljava/text/DecimalFormat;D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",Pos:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, LM/h;->i:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",B:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LM/h;->f:Lq/c;

    invoke-virtual {v1}, Lq/c;->e()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",P:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LM/h;->j:Lo/T;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
