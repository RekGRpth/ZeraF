.class public LM/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:LM/v;

.field private c:LM/j;

.field private d:LM/r;

.field private e:LM/f;

.field private f:LM/a;

.field private g:LM/c;

.field private h:LM/c;

.field private i:LM/c;

.field private j:LM/E;

.field private k:LM/B;

.field private final l:LM/u;

.field private final m:Ljava/util/List;

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, LM/u;

    invoke-direct {v0, p0, v3}, LM/u;-><init>(LM/n;LM/o;)V

    iput-object v0, p0, LM/n;->l:LM/u;

    invoke-static {}, Lcom/google/common/collect/bx;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LM/n;->m:Ljava/util/List;

    iput v2, p0, LM/n;->n:I

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LM/n;->o:Z

    iput-boolean v2, p0, LM/n;->p:Z

    iput-boolean v1, p0, LM/n;->q:Z

    iput-object p1, p0, LM/n;->a:Landroid/content/Context;

    new-instance v0, LM/x;

    invoke-direct {v0, p1, v3}, LM/x;-><init>(Landroid/content/Context;LM/o;)V

    iput-object v0, p0, LM/n;->b:LM/v;

    new-instance v0, LM/j;

    invoke-direct {v0}, LM/j;-><init>()V

    iput-object v0, p0, LM/n;->c:LM/j;

    new-instance v0, LM/g;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/g;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->g:LM/c;

    new-instance v0, LM/D;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/D;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->h:LM/c;

    invoke-direct {p0, v2}, LM/n;->b(I)V

    invoke-direct {p0, p1}, LM/n;->a(Landroid/content/Context;)V

    invoke-direct {p0, p1}, LM/n;->b(Landroid/content/Context;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public static final a()F
    .locals 2

    invoke-static {}, LM/n;->b()F

    move-result v0

    const v1, 0x3f2aacda

    mul-float/2addr v0, v1

    return v0
.end method

.method static synthetic a(LM/n;)LM/v;
    .locals 1

    iget-object v0, p0, LM/n;->b:LM/v;

    return-object v0
.end method

.method static synthetic a(LM/n;Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, LM/n;->b(Landroid/content/Context;)V

    return-void
.end method

.method private a(LM/v;)V
    .locals 2

    invoke-virtual {p0}, LM/n;->g()V

    iput-object p1, p0, LM/n;->b:LM/v;

    iget-object v0, p0, LM/n;->e:LM/f;

    iget-object v1, p0, LM/n;->b:LM/v;

    invoke-virtual {v0, v1}, LM/f;->a(LM/v;)V

    invoke-virtual {p0}, LM/n;->e()V

    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, LM/o;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1, p1}, LM/o;-><init>(LM/n;Landroid/os/Handler;Landroid/content/Context;)V

    invoke-static {p1, v0}, LaH/x;->a(Landroid/content/Context;Landroid/database/ContentObserver;)V

    return-void
.end method

.method public static final b()F
    .locals 1

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->n()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method static synthetic b(LM/n;)LM/c;
    .locals 1

    iget-object v0, p0, LM/n;->i:LM/c;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    new-instance v1, LM/f;

    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->b:LM/v;

    invoke-direct {v1, v2, v3, v0}, LM/f;-><init>(LM/b;LM/v;Lcom/google/googlenav/common/a;)V

    iput-object v1, p0, LM/n;->e:LM/f;

    new-instance v1, LM/r;

    iget-object v2, p0, LM/n;->c:LM/j;

    invoke-direct {v1, p0, v2}, LM/r;-><init>(LM/n;LM/b;)V

    iput-object v1, p0, LM/n;->d:LM/r;

    new-instance v1, LM/E;

    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->c:LM/j;

    invoke-virtual {v3}, LM/j;->c()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, LM/E;-><init>(LM/b;Landroid/os/Handler;Lcom/google/googlenav/common/a;)V

    iput-object v1, p0, LM/n;->j:LM/E;

    new-instance v0, LM/a;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/a;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->f:LM/a;

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "gps"

    iget-object v2, p0, LM/n;->e:LM/f;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_gps_fixup"

    iget-object v2, p0, LM/n;->d:LM/r;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "network"

    iget-object v2, p0, LM/n;->d:LM/r;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_base_location"

    iget-object v2, p0, LM/n;->f:LM/a;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    if-nez p1, :cond_0

    iget-object v0, p0, LM/n;->g:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    invoke-direct {p0}, LM/n;->m()V

    :goto_0
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_bearing_noise_reduction"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    new-instance v0, LM/B;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-direct {v0, v1}, LM/B;-><init>(LM/b;)V

    iput-object v0, p0, LM/n;->k:LM/B;

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "android_orientation"

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    return-void

    :cond_0
    iget-object v0, p0, LM/n;->h:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LaH/x;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, LM/n;->o:Z

    iget-boolean v0, p0, LM/n;->o:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LM/n;->q:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, LM/n;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, LM/n;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LM/n;->q:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LM/n;->e()V

    goto :goto_0
.end method

.method static synthetic c(LM/n;)LM/f;
    .locals 1

    iget-object v0, p0, LM/n;->e:LM/f;

    return-object v0
.end method

.method private c(I)V
    .locals 4

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_bearing_noise_reduction"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/b;

    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->i:LM/c;

    invoke-interface {v3}, LM/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LM/j;->b(Ljava/lang/String;LM/b;)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, LM/n;->g:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    invoke-direct {p0}, LM/n;->m()V

    :goto_1
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_bearing_noise_reduction"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LM/n;->k:LM/B;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LM/b;

    iget-object v2, p0, LM/n;->c:LM/j;

    iget-object v3, p0, LM/n;->i:LM/c;

    invoke-interface {v3}, LM/c;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LM/j;->a(Ljava/lang/String;LM/b;)V

    goto :goto_2

    :cond_1
    invoke-direct {p0}, LM/n;->n()V

    iget-object v0, p0, LM/n;->h:LM/c;

    iput-object v0, p0, LM/n;->i:LM/c;

    goto :goto_1

    :cond_2
    return-void
.end method

.method private m()V
    .locals 3

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->j:LM/E;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "da_tunnel_heartbeat"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->e:LM/f;

    invoke-virtual {v0, v1, v2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    return-void
.end method

.method private n()V
    .locals 3

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->j:LM/E;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "da_tunnel_heartbeat"

    iget-object v2, p0, LM/n;->i:LM/c;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_hmm"

    iget-object v2, p0, LM/n;->e:LM/f;

    invoke-virtual {v0, v1, v2}, LM/j;->b(Ljava/lang/String;LM/b;)V

    return-void
.end method

.method private o()V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v4, "gps"

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v4, "gps"

    invoke-interface {v0, v4}, LM/v;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :goto_0
    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v4

    invoke-virtual {v4}, LR/m;->k()I

    move-result v4

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v5

    sub-long v5, v2, v5

    int-to-long v7, v4

    cmp-long v4, v5, v7

    if-gez v4, :cond_0

    iget-object v4, p0, LM/n;->c:LM/j;

    invoke-virtual {v4, v0}, LM/j;->onLocationChanged(Landroid/location/Location;)V

    :goto_1
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v4, "network"

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v4, "network"

    invoke-interface {v0, v4}, LM/v;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0xea60

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-virtual {v1, v0}, LM/j;->onLocationChanged(Landroid/location/Location;)V

    :goto_3
    return-void

    :cond_0
    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0, v9, v1}, LM/j;->a(ILandroid/os/Bundle;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v2, "network"

    invoke-virtual {v0, v2, v9, v1}, LM/j;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    goto :goto_3

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 2

    iget v0, p0, LM/n;->n:I

    if-eq v0, p1, :cond_0

    invoke-direct {p0, p1}, LM/n;->c(I)V

    iput p1, p0, LM/n;->n:I

    :cond_0
    iget-object v0, p0, LM/n;->c:LM/j;

    new-instance v1, LM/q;

    invoke-direct {v1, p0, p1}, LM/q;-><init>(LM/n;I)V

    invoke-virtual {v0, v1}, LM/j;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(J)V
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v1, "gps"

    iget-object v5, p0, LM/n;->c:LM/j;

    move-wide v2, p1

    invoke-interface/range {v0 .. v5}, LM/v;->a(Ljava/lang/String;JFLM/b;)V

    :cond_0
    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LM/n;->b:LM/v;

    const-string v1, "network"

    iget-object v5, p0, LM/n;->c:LM/j;

    move-wide v2, p1

    invoke-interface/range {v0 .. v5}, LM/v;->a(Ljava/lang/String;JFLM/b;)V

    :cond_1
    return-void
.end method

.method public a(LL/G;)V
    .locals 1

    invoke-virtual {p1}, LL/G;->a()LM/z;

    move-result-object v0

    invoke-direct {p0, v0}, LM/n;->a(LM/v;)V

    return-void
.end method

.method public a(LM/b;)V
    .locals 2

    iget-object v0, p0, LM/n;->c:LM/j;

    iget-object v1, p0, LM/n;->i:LM/c;

    invoke-interface {v1}, LM/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->c:LM/j;

    const-string v1, "driveabout_orientation_filter"

    invoke-virtual {v0, v1, p1}, LM/j;->a(Ljava/lang/String;LM/b;)V

    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(LM/s;)V
    .locals 1

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0, p1}, LM/v;->a(LM/s;)V

    return-void
.end method

.method public a(LO/z;)V
    .locals 2

    iget-object v0, p0, LM/n;->c:LM/j;

    new-instance v1, LM/p;

    invoke-direct {v1, p0, p1}, LM/p;-><init>(LM/n;LO/z;)V

    invoke-virtual {v0, v1}, LM/j;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(Ljava/lang/String;LM/b;)V
    .locals 1

    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0, p1, p2}, LM/j;->a(Ljava/lang/String;LM/b;)V

    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, LM/n;->i:LM/c;

    invoke-interface {v0}, LM/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, LM/n;->d:LM/r;

    invoke-virtual {v0}, LM/r;->b()Z

    move-result v0

    return v0
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LM/n;->p:Z

    invoke-virtual {p0}, LM/n;->e()V

    return-void
.end method

.method public e()V
    .locals 2

    iget-boolean v0, p0, LM/n;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LM/n;->p:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LM/n;->q:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LM/n;->d:LM/r;

    invoke-virtual {v0}, LM/r;->a()V

    iget-object v0, p0, LM/n;->j:LM/E;

    invoke-virtual {v0}, LM/E;->b()V

    invoke-direct {p0}, LM/n;->o()V

    invoke-virtual {p0}, LM/n;->f()V

    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->l:LM/u;

    invoke-interface {v0, v1}, LM/v;->a(Landroid/location/GpsStatus$Listener;)Z

    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-interface {v0, v1}, LM/v;->b(LM/b;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LM/n;->q:Z

    goto :goto_0
.end method

.method public f()V
    .locals 2

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v0

    invoke-virtual {v0}, LR/m;->l()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LM/n;->a(J)V

    return-void
.end method

.method public g()V
    .locals 2

    iget-boolean v0, p0, LM/n;->q:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LM/n;->q:Z

    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0}, LM/j;->d()V

    iget-object v0, p0, LM/n;->j:LM/E;

    invoke-virtual {v0}, LM/E;->a()V

    iget-object v0, p0, LM/n;->e:LM/f;

    invoke-virtual {v0}, LM/f;->a()V

    invoke-virtual {p0}, LM/n;->h()V

    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->l:LM/u;

    invoke-interface {v0, v1}, LM/v;->b(Landroid/location/GpsStatus$Listener;)V

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->b()V

    goto :goto_0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-interface {v0, v1}, LM/v;->a(LM/b;)V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->f()V

    return-void
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->e()V

    return-void
.end method

.method public k()V
    .locals 2

    iget-object v0, p0, LM/n;->b:LM/v;

    iget-object v1, p0, LM/n;->c:LM/j;

    invoke-interface {v0, v1}, LM/v;->a(LM/b;)V

    iget-object v0, p0, LM/n;->b:LM/v;

    invoke-interface {v0}, LM/v;->b()V

    iget-object v0, p0, LM/n;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, LM/n;->c:LM/j;

    invoke-virtual {v0}, LM/j;->b()V

    return-void
.end method

.method public l()V
    .locals 3

    new-instance v0, LM/x;

    iget-object v1, p0, LM/n;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LM/x;-><init>(Landroid/content/Context;LM/o;)V

    invoke-direct {p0, v0}, LM/n;->a(LM/v;)V

    return-void
.end method
