.class public LaT/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
.implements Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;


# static fields
.field static a:Ljava/lang/String;

.field private static f:LaT/a;


# instance fields
.field private final b:Ljava/util/List;

.field private volatile c:LaT/f;

.field private final d:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

.field private volatile g:Li/f;

.field private volatile h:I

.field private i:J

.field private final j:Ljava/util/List;

.field private volatile k:I

.field private final l:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private volatile m:Lo/aq;

.field private volatile n:I

.field private volatile o:Ljava/util/concurrent/CountDownLatch;

.field private volatile p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "offlineAreaManagerStorage"

    sput-object v0, LaT/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaT/a;->i:J

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iput-object p1, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LaT/a;->j:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p0}, LaT/a;->r()V

    return-void
.end method

.method static synthetic a(LaT/a;I)I
    .locals 0

    iput p1, p0, LaT/a;->h:I

    return p1
.end method

.method static synthetic a(LaT/a;J)J
    .locals 0

    iput-wide p1, p0, LaT/a;->i:J

    return-wide p1
.end method

.method static synthetic a(LaT/a;)LaT/f;
    .locals 1

    iget-object v0, p0, LaT/a;->c:LaT/f;

    return-object v0
.end method

.method static synthetic a(LaT/a;LaT/f;)LaT/f;
    .locals 0

    iput-object p1, p0, LaT/a;->c:LaT/f;

    return-object p1
.end method

.method static synthetic a(LaT/a;Lo/aq;)Lo/aq;
    .locals 0

    iput-object p1, p0, LaT/a;->m:Lo/aq;

    return-object p1
.end method

.method static synthetic a(LaT/a;LaT/l;)V
    .locals 0

    invoke-direct {p0, p1}, LaT/a;->a(LaT/l;)V

    return-void
.end method

.method private a(LaT/l;)V
    .locals 3

    iget-object v1, p0, LaT/a;->j:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/m;

    invoke-interface {v0, p1}, LaT/m;->onOfflineDataUpdate(LaT/l;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V
    .locals 2

    const-class v1, LaT/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, LaT/a;->f:LaT/a;

    if-nez v0, :cond_0

    new-instance v0, LaT/a;

    invoke-direct {v0, p0}, LaT/a;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;)V

    sput-object v0, LaT/a;->f:LaT/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v1}, LaT/f;->e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->u()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    const/16 v0, 0x79

    invoke-static {v0, p0, p1}, Lbm/m;->b(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-static {p1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p1, "o"

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "o"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private a(Z)V
    .locals 11

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput v2, p0, LaT/a;->n:I

    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;

    if-eqz p1, :cond_0

    iget-object v1, p0, LaT/a;->m:Lo/aq;

    if-nez v1, :cond_3

    :cond_0
    move v1, v3

    :goto_0
    move v4, v1

    :goto_1
    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-static {v1}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->c()Lo/aq;

    move-result-object v6

    if-eqz v6, :cond_6

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaT/f;

    invoke-static {v1}, LaT/f;->b(LaT/f;)I

    move-result v7

    if-nez v7, :cond_1

    invoke-static {v1}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1, v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->a(Lo/aq;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    :goto_2
    if-nez v1, :cond_7

    array-length v7, v0

    move v5, v2

    :goto_3
    if-ge v5, v7, :cond_2

    aget-object v8, v0, v5

    invoke-virtual {v8}, LaT/f;->a()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_4

    invoke-static {v8}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v8

    invoke-interface {v8, v6}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->a(Lo/aq;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v1, v3

    :cond_2
    if-nez v1, :cond_7

    iget v1, p0, LaT/a;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LaT/a;->n:I

    if-eqz v4, :cond_5

    iget-object v1, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    move v1, v4

    :goto_4
    move v4, v1

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    iget-object v1, p0, LaT/a;->m:Lo/aq;

    invoke-virtual {v6, v1}, Lo/aq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    move v1, v3

    goto :goto_4

    :cond_6
    return-void

    :cond_7
    move v1, v4

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_2
.end method

.method static synthetic b(LaT/a;I)I
    .locals 0

    iput p1, p0, LaT/a;->p:I

    return p1
.end method

.method private static b(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/16 v0, 0x352

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const/16 v0, 0x355

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x33b

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(LaT/a;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    return-object v0
.end method

.method private static c(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string v0, "0"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "1"

    goto :goto_0

    :pswitch_1
    const-string v0, "2"

    goto :goto_0

    :pswitch_2
    const-string v0, "3"

    goto :goto_0

    :pswitch_3
    const-string v0, "4"

    goto :goto_0

    :pswitch_4
    const-string v0, "5"

    goto :goto_0

    :pswitch_5
    const-string v0, "6"

    goto :goto_0

    :pswitch_6
    const-string v0, "7"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method

.method static synthetic d(LaT/a;)I
    .locals 1

    iget v0, p0, LaT/a;->h:I

    return v0
.end method

.method static synthetic e(LaT/a;)Lo/aq;
    .locals 1

    iget-object v0, p0, LaT/a;->m:Lo/aq;

    return-object v0
.end method

.method static synthetic f(LaT/a;)J
    .locals 2

    iget-wide v0, p0, LaT/a;->i:J

    return-wide v0
.end method

.method static synthetic g(LaT/a;)I
    .locals 1

    iget v0, p0, LaT/a;->p:I

    return v0
.end method

.method static synthetic h(LaT/a;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    iget-object v0, p0, LaT/a;->o:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method public static i()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/clientparam/f;->a()Lcom/google/googlenav/clientparam/EnableFeatureParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/clientparam/EnableFeatureParameters;->isOfflineMapsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, LaT/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, v1}, LaT/b;-><init>(Las/c;)V

    invoke-virtual {v0}, LaT/b;->g()V

    goto :goto_0
.end method

.method static synthetic i(LaT/a;)[LaT/f;
    .locals 1

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v0

    return-object v0
.end method

.method public static j()LaT/a;
    .locals 1

    sget-object v0, LaT/a;->f:LaT/a;

    if-nez v0, :cond_0

    sget-object v0, LaT/a;->f:LaT/a;

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    sget-object v0, LaT/a;->f:LaT/a;

    iget-object v0, v0, LaT/a;->o:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v0, LaT/a;->f:LaT/a;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic j(LaT/a;)[LaT/f;
    .locals 1

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v0

    return-object v0
.end method

.method public static k()LaT/a;
    .locals 1

    sget-object v0, LaT/a;->f:LaT/a;

    return-object v0
.end method

.method static synthetic k(LaT/a;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, LaT/a;->y()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(LaT/a;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;
    .locals 1

    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    return-object v0
.end method

.method static synthetic q()LaT/a;
    .locals 1

    sget-object v0, LaT/a;->f:LaT/a;

    return-object v0
.end method

.method private r()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, LaT/a;->o:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, LaT/d;

    invoke-direct {v2, p0}, LaT/d;-><init>(LaT/a;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    return-void
.end method

.method private declared-synchronized s()[LaT/f;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized t()[LaT/f;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    new-array v1, v1, [LaT/f;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaT/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private u()I
    .locals 2

    iget-object v0, p0, LaT/a;->g:Li/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private v()V
    .locals 3

    const/4 v2, 0x1

    iput v2, p0, LaT/a;->h:I

    const/4 v0, -0x1

    iput v0, p0, LaT/a;->k:I

    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {v0, v2}, LaT/f;->a(LaT/f;I)I

    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-interface {v0, p0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->a(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method private w()V
    .locals 2

    const/4 v0, 0x2

    iput v0, p0, LaT/a;->h:I

    const/4 v0, -0x1

    iput v0, p0, LaT/a;->k:I

    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    invoke-virtual {p0}, LaT/a;->d()V

    iget-object v0, p0, LaT/a;->c:LaT/f;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LaT/f;->a(LaT/f;I)I

    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    invoke-interface {v0, p0, p0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->b(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Lcom/google/android/apps/gmm/map/internal/store/prefetch/u;)V

    return-void
.end method

.method private declared-synchronized x()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    iput v0, p0, LaT/a;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->c:LaT/f;

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaT/f;

    invoke-static {v0}, LaT/f;->b(LaT/f;)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v0}, LaT/a;->b(LaT/f;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0, v0}, LaT/a;->a(LaT/f;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private y()Ljava/lang/String;
    .locals 1

    iget v0, p0, LaT/a;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaT/a;->p:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public H_()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->g:Li/f;

    iget v0, p0, LaT/a;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, LaT/a;->h:I

    :cond_0
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    invoke-virtual {p0}, LaT/a;->m()V

    return-void
.end method

.method public declared-synchronized a(LaT/f;)I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-direct {p0}, LaT/a;->u()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {p1, v0}, LaT/f;->a(LaT/f;I)I

    iget v0, p0, LaT/a;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    invoke-virtual {p0}, LaT/a;->m()V

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-object p1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {p0}, LaT/a;->d()V

    invoke-direct {p0}, LaT/a;->v()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    iput-object v0, p0, LaT/a;->m:Lo/aq;

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, LaT/a;->g:Li/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaT/a;->g:Li/f;

    invoke-interface {v0}, Li/f;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->g:Li/f;

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, LaT/a;->h:I

    const-string v0, "f"

    invoke-static {p1}, LaT/a;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LaT/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-static {p1}, LaT/a;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a(Ljava/lang/String;)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    invoke-virtual {p0}, LaT/a;->m()V

    return-void
.end method

.method public a(II)V
    .locals 3

    sub-int v0, p1, p2

    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, p1

    iget v1, p0, LaT/a;->k:I

    if-eq v0, v1, :cond_0

    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    iget v2, p0, LaT/a;->h:I

    invoke-virtual {v1, v2}, LaT/l;->a(I)LaT/l;

    move-result-object v1

    invoke-virtual {v1, v0}, LaT/l;->b(I)LaT/l;

    move-result-object v1

    sub-int v2, p1, p2

    mul-int/lit8 v2, v2, 0xf

    invoke-virtual {v1, v2}, LaT/l;->c(I)LaT/l;

    move-result-object v1

    mul-int/lit8 v2, p1, 0xf

    invoke-virtual {v1, v2}, LaT/l;->d(I)LaT/l;

    move-result-object v1

    invoke-direct {p0, v1}, LaT/a;->a(LaT/l;)V

    iput v0, p0, LaT/a;->k:I

    :cond_0
    return-void
.end method

.method public a(LaT/m;)V
    .locals 3

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, LaT/a;->j:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Las/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    new-instance v2, LaT/e;

    invoke-direct {v2, p0, p1}, LaT/e;-><init>(LaT/a;LaT/m;)V

    invoke-direct {v0, v1, v2}, Las/b;-><init>(Las/c;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Las/b;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Li/f;)V
    .locals 0

    iput-object p1, p0, LaT/a;->g:Li/f;

    return-void
.end method

.method public a(Lo/aq;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public declared-synchronized b()V
    .locals 3

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LaT/a;->g:Li/f;

    iget v0, p0, LaT/a;->h:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    invoke-virtual {p0}, LaT/a;->m()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :pswitch_0
    :try_start_1
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, LaT/l;->b(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    iget-object v0, p0, LaT/a;->c:LaT/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LaT/f;->a(LaT/f;I)I

    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LaT/f;->b(J)V

    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    const/4 v1, 0x0

    iget-object v2, p0, LaT/a;->c:LaT/f;

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    const-string v0, "t"

    invoke-direct {p0, v0}, LaT/a;->a(Ljava/lang/String;)V

    invoke-direct {p0}, LaT/a;->x()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_1
    :try_start_2
    iget-object v0, p0, LaT/a;->c:LaT/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LaT/f;->a(LaT/f;I)I

    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0}, LaT/f;->g()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    :cond_0
    const-string v0, "l"

    invoke-direct {p0, v0}, LaT/a;->a(Ljava/lang/String;)V

    invoke-direct {p0}, LaT/a;->x()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(LaT/m;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, LaT/a;->j:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaT/a;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public declared-synchronized b(LaT/f;)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaT/a;->c:LaT/f;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, LaT/a;->p()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x3

    invoke-static {p1, v0}, LaT/f;->a(LaT/f;I)I

    iget v0, p0, LaT/a;->h:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :goto_1
    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V

    invoke-virtual {p0}, LaT/a;->m()V

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iput-object p1, p0, LaT/a;->c:LaT/f;

    invoke-direct {p0}, LaT/a;->w()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lo/aq;
    .locals 1

    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/aq;

    return-object v0
.end method

.method public declared-synchronized c(LaT/f;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LaT/a;->c:LaT/f;

    if-ne p1, v2, :cond_0

    invoke-virtual {p0}, LaT/a;->p()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v2, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, LaT/f;->a()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-static {p1, v1}, LaT/f;->a(LaT/f;I)I

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v1

    iget-object v2, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v1, v2}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v1

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v1

    iget v2, p0, LaT/a;->h:I

    invoke-virtual {v1, v2}, LaT/l;->a(I)LaT/l;

    move-result-object v1

    invoke-direct {p0, v1}, LaT/a;->a(LaT/l;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-virtual {p1}, LaT/f;->a()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    iget-object v2, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v1

    iget-object v2, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v1, v2}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v1

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v2

    invoke-virtual {v1, v2}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v1

    iget v2, p0, LaT/a;->h:I

    invoke-virtual {v1, v2}, LaT/l;->a(I)LaT/l;

    move-result-object v1

    invoke-direct {p0, v1}, LaT/a;->a(LaT/l;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_3
    move v0, v1

    goto/16 :goto_0
.end method

.method public declared-synchronized d()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {v0}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->d()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, LaT/a;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->m:Lo/aq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, LaT/a;->l:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    iget-object v0, p0, LaT/a;->c:LaT/f;

    invoke-static {v0}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->f()I

    move-result v0

    return v0
.end method

.method public g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, LaT/a;->n:I

    return v0
.end method

.method public declared-synchronized l()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    iput v0, p0, LaT/a;->h:I

    invoke-virtual {p0}, LaT/a;->n()V

    const/4 v0, 0x0

    iput-object v0, p0, LaT/a;->c:LaT/f;

    iget-object v0, p0, LaT/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, LaT/a;->d:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    invoke-virtual {p0}, LaT/a;->m()V

    new-instance v0, LaT/l;

    invoke-direct {v0}, LaT/l;-><init>()V

    invoke-direct {p0}, LaT/a;->s()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->a([LaT/f;)LaT/l;

    move-result-object v0

    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-virtual {v0, v1}, LaT/l;->a(LaT/f;)LaT/l;

    move-result-object v0

    invoke-direct {p0}, LaT/a;->t()[LaT/f;

    move-result-object v1

    invoke-virtual {v0, v1}, LaT/l;->b([LaT/f;)LaT/l;

    move-result-object v0

    iget v1, p0, LaT/a;->h:I

    invoke-virtual {v0, v1}, LaT/l;->a(I)LaT/l;

    move-result-object v0

    invoke-direct {p0, v0}, LaT/a;->a(LaT/l;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method m()V
    .locals 2

    new-instance v0, LaT/c;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LaT/c;-><init>(LaT/a;Las/c;)V

    invoke-virtual {v0}, LaT/c;->g()V

    return-void
.end method

.method public n()V
    .locals 2

    iget-object v0, p0, LaT/a;->g:Li/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaT/a;->e:Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    iget-object v1, p0, LaT/a;->g:Li/f;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->a(Li/f;)V

    :cond_0
    return-void
.end method

.method public declared-synchronized o()Z
    .locals 3

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget v1, p0, LaT/a;->h:I

    if-eqz v1, :cond_0

    iget v1, p0, LaT/a;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v1, p0, LaT/a;->c:LaT/f;

    invoke-static {v1}, LaT/f;->c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->d()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, LaT/a;->a(Z)V

    invoke-direct {p0}, LaT/a;->v()V

    invoke-virtual {p0}, LaT/a;->m()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized p()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaT/a;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, LaT/a;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, LaT/a;->w()V

    invoke-virtual {p0}, LaT/a;->m()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
