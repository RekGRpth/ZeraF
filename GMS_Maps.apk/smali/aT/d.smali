.class LaT/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LaT/a;


# direct methods
.method constructor <init>(LaT/a;)V
    .locals 0

    iput-object p1, p0, LaT/d;->a:LaT/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/4 v10, 0x2

    const/4 v2, 0x0

    const/4 v13, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide/16 v0, 0x0

    sget-object v3, LA/c;->a:LA/c;

    invoke-static {v3}, Lr/C;->b(LA/c;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v0, LA/c;->a:LA/c;

    invoke-static {v0}, Lr/C;->c(LA/c;)Lr/z;

    move-result-object v0

    invoke-interface {v0}, Lr/z;->f()J

    move-result-wide v0

    :goto_0
    :try_start_0
    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v6, 0x4

    invoke-static {v3, v6}, LaT/a;->a(LaT/a;I)I

    new-instance v6, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, LbM/r;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v3

    sget-object v7, LaT/a;->a:Ljava/lang/String;

    invoke-interface {v3, v7}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v7, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-nez v7, :cond_3

    :cond_0
    iget-object v2, p0, LaT/d;->a:LaT/a;

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/a;J)J

    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v3, "Tile store should be set if not unit tests"

    invoke-static {v4, v3}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    iget-object v7, p0, LaT/d;->a:LaT/a;

    monitor-enter v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3}, LaT/a;->b(LaT/a;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v8, 0x0

    invoke-static {v3, v8}, LaT/a;->a(LaT/a;LaT/f;)LaT/f;

    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v8, 0x0

    invoke-static {v3, v8}, LaT/a;->a(LaT/a;Lo/aq;)Lo/aq;

    iget-object v3, p0, LaT/d;->a:LaT/a;

    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v8

    invoke-static {v3, v8, v9}, LaT/a;->a(LaT/a;J)J

    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3}, LaT/a;->f(LaT/a;)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-wide v8

    cmp-long v3, v8, v0

    if-eqz v3, :cond_9

    move v3, v5

    :goto_2
    :try_start_4
    iget-object v8, p0, LaT/d;->a:LaT/a;

    const/4 v9, 0x2

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v9

    invoke-static {v8, v9}, LaT/a;->a(LaT/a;I)I

    iget-object v8, p0, LaT/d;->a:LaT/a;

    invoke-static {v8}, LaT/a;->d(LaT/a;)I

    move-result v8

    if-eq v8, v5, :cond_4

    iget-object v8, p0, LaT/d;->a:LaT/a;

    invoke-static {v8}, LaT/a;->d(LaT/a;)I

    move-result v8

    if-ne v8, v10, :cond_5

    :cond_4
    iget-object v8, p0, LaT/d;->a:LaT/a;

    const/4 v9, 0x0

    invoke-static {v8, v9}, LaT/a;->a(LaT/a;I)I

    :cond_5
    iget-object v8, p0, LaT/d;->a:LaT/a;

    if-nez v3, :cond_6

    const/4 v9, 0x3

    invoke-virtual {v6, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v9

    if-eqz v9, :cond_6

    const/4 v2, 0x3

    invoke-virtual {v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, Lo/aq;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lo/aq;

    move-result-object v2

    :cond_6
    invoke-static {v8, v2}, LaT/a;->a(LaT/a;Lo/aq;)Lo/aq;

    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    iget-object v8, p0, LaT/d;->a:LaT/a;

    const/4 v9, 0x0

    invoke-static {v8, v9}, LaT/a;->b(LaT/a;I)I

    :goto_3
    if-ge v4, v2, :cond_d

    const/4 v8, 0x1

    invoke-virtual {v6, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v8

    invoke-static {v8}, LaT/f;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;

    move-result-object v8

    iget-object v9, p0, LaT/d;->a:LaT/a;

    iget-object v10, p0, LaT/d;->a:LaT/a;

    invoke-static {v10}, LaT/a;->g(LaT/a;)I

    move-result v10

    new-instance v11, Ljava/lang/Integer;

    invoke-virtual {v8}, LaT/f;->f()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    invoke-static {v9, v10}, LaT/a;->b(LaT/a;I)I

    invoke-static {v8}, LaT/f;->b(LaT/f;)I

    move-result v9

    if-eqz v9, :cond_7

    invoke-static {v8}, LaT/f;->b(LaT/f;)I

    move-result v9

    if-ne v9, v13, :cond_a

    :cond_7
    if-eqz v3, :cond_8

    const/4 v9, 0x4

    invoke-static {v8, v9}, LaT/f;->a(LaT/f;I)I

    :cond_8
    iget-object v9, p0, LaT/d;->a:LaT/a;

    invoke-static {v9}, LaT/a;->b(LaT/a;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :catch_0
    move-exception v2

    iget-object v2, p0, LaT/d;->a:LaT/a;

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/a;J)J

    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_1

    :cond_9
    move v3, v4

    goto/16 :goto_2

    :cond_a
    :try_start_5
    invoke-static {v8}, LaT/f;->b(LaT/f;)I

    move-result v9

    if-ne v9, v5, :cond_c

    iget-object v9, p0, LaT/d;->a:LaT/a;

    invoke-static {v9, v8}, LaT/a;->a(LaT/a;LaT/f;)LaT/f;

    goto :goto_4

    :catchall_0
    move-exception v2

    :goto_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v2

    move v4, v3

    :goto_6
    iget-object v3, p0, LaT/d;->a:LaT/a;

    invoke-static {v3, v0, v1}, LaT/a;->a(LaT/a;J)J

    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    if-eqz v4, :cond_b

    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-virtual {v0}, LaT/a;->m()V

    :cond_b
    throw v2

    :cond_c
    :try_start_7
    iget-object v9, p0, LaT/d;->a:LaT/a;

    invoke-static {v9}, LaT/a;->c(LaT/a;)Ljava/util/concurrent/ConcurrentLinkedQueue;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_d
    monitor-exit v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    iget-object v2, p0, LaT/d;->a:LaT/a;

    invoke-static {v2, v0, v1}, LaT/a;->a(LaT/a;J)J

    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-static {v0}, LaT/a;->h(LaT/a;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    if-eqz v3, :cond_1

    iget-object v0, p0, LaT/d;->a:LaT/a;

    invoke-virtual {v0}, LaT/a;->m()V

    goto/16 :goto_1

    :catchall_2
    move-exception v2

    goto :goto_6

    :catchall_3
    move-exception v2

    move v3, v4

    goto :goto_5
.end method
