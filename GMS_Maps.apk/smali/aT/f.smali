.class public LaT/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/friend/bf;


# static fields
.field private static final h:I

.field private static final i:I


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Landroid/graphics/Bitmap;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x40

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaT/f;->h:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/Config;->c(I)I

    move-result v0

    sput v0, LaT/f;->i:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILandroid/graphics/Bitmap;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iput-object p2, p0, LaT/f;->b:Ljava/lang/String;

    iput p3, p0, LaT/f;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    if-eqz p4, :cond_1

    new-instance v0, Lan/f;

    invoke-direct {v0, p4}, Lan/f;-><init>(Landroid/graphics/Bitmap;)V

    sget v1, LaT/f;->i:I

    sget v2, LaT/f;->h:I

    invoke-static {v0, v1, v2}, Lam/j;->a(Lam/f;II)Lam/f;

    move-result-object v0

    check-cast v0, Lan/f;

    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eq p4, v1, :cond_0

    invoke-virtual {p4}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    invoke-virtual {v0}, Lan/f;->h()Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, LaT/g;

    invoke-direct {v2, p0, v0}, LaT/g;-><init>(LaT/f;Lan/f;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iput-object p2, p0, LaT/f;->b:Ljava/lang/String;

    iput p3, p0, LaT/f;->c:I

    iput-object p4, p0, LaT/f;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    iput-wide p5, p0, LaT/f;->g:J

    return-void
.end method

.method static synthetic a(LaT/f;I)I
    .locals 0

    iput p1, p0, LaT/f;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;
    .locals 1

    invoke-static {p0}, LaT/f;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaT/f;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    invoke-direct {p0}, LaT/f;->i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaT/f;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, LaT/f;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(LaT/f;)I
    .locals 1

    iget v0, p0, LaT/f;->c:I

    return v0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x4

    const/4 v0, 0x0

    invoke-static {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    invoke-static {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaT/f;
    .locals 7

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LaT/f;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/prefetch/android/k;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/prefetch/android/k;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {p0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x5

    invoke-static {p0, v5}, Lcom/google/googlenav/common/io/protocol/b;->f(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, LaT/f;-><init>(Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;Ljava/lang/String;ILjava/lang/String;J)V

    goto :goto_0
.end method

.method static synthetic c(LaT/f;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .locals 1

    iget-object v0, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method private i()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, LbM/r;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x1

    iget-object v2, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x2

    iget-object v2, p0, LaT/f;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    iget v2, p0, LaT/f;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x4

    iget-object v2, p0, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    iget-wide v2, p0, LaT/f;->g:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setLong(IJ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, LaT/f;->c:I

    return v0
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 2

    iget-boolean v0, p0, LaT/f;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x9

    invoke-static {p3, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1}, LaT/f;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LaT/f;->a(Ljava/lang/String;)V

    invoke-static {}, LaT/a;->q()LaT/a;

    move-result-object v0

    new-instance v1, LaT/l;

    invoke-direct {v1}, LaT/l;-><init>()V

    invoke-static {v0, v1}, LaT/a;->a(LaT/a;LaT/l;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LaT/f;->f:Z

    iput-object p1, p0, LaT/f;->b:Ljava/lang/String;

    return-void
.end method

.method public b()V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    check-cast v0, Lcom/google/googlenav/prefetch/android/k;

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->k()LaN/B;

    move-result-object v1

    const/16 v2, 0x57

    const-string v3, "stp"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lbm/r;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/friend/bg;

    invoke-direct {v3}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v3

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->i()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/friend/bg;->d(I)Lcom/google/googlenav/friend/bg;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/prefetch/android/k;->j()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/friend/bg;->e(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-static {}, LaM/f;->j()LaM/f;

    move-result-object v1

    invoke-virtual {v1}, LaM/f;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->c(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/friend/bg;->d(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/googlenav/friend/bg;->e(Z)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v0

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public b(J)V
    .locals 0

    iput-wide p1, p0, LaT/f;->g:J

    return-void
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, LaT/f;->g:J

    return-wide v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaT/f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;
    .locals 1

    iget-object v0, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, LaT/f;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, LaT/f;

    iget-object v1, p0, LaT/f;->b:Ljava/lang/String;

    invoke-virtual {p1}, LaT/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    iget-object v2, p1, LaT/f;->a:Lcom/google/android/apps/gmm/map/internal/store/prefetch/B;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LaT/f;->c:I

    iget v2, p1, LaT/f;->c:I

    if-ne v1, v2, :cond_0

    iget-wide v1, p0, LaT/f;->g:J

    iget-wide v3, p1, LaT/f;->g:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, LaT/f;->d:Ljava/lang/String;

    iget-object v2, p1, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaT/f;->d:Ljava/lang/String;

    return-object v0
.end method

.method public g()Ljava/io/File;
    .locals 3

    invoke-static {}, Lcom/google/googlenav/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LaT/a;->j()LaT/a;

    move-result-object v0

    invoke-static {v0}, LaT/a;->l(LaT/a;)Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/store/prefetch/x;->d()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OfflineMapArea_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaT/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public h()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, LaT/f;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method
