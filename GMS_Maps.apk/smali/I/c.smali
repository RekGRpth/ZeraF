.class public LI/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Lcom/google/android/location/internal/d;

.field private c:LI/f;

.field private d:Z

.field private e:I

.field private final f:LI/f;

.field private final g:LI/a;

.field private final h:Lcom/google/android/location/internal/a;

.field private final i:Landroid/os/HandlerThread;

.field private final j:LI/e;

.field private final k:Landroid/content/Context;

.field private final l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/location/LocationListener;Landroid/os/Looper;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LI/c;-><init>(Landroid/content/Context;ILandroid/location/LocationListener;ZLandroid/os/Looper;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/location/LocationListener;ZLandroid/os/Looper;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LI/c;->a:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, LI/c;->d:Z

    new-instance v0, LI/f;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LI/f;-><init>(LI/c;LI/d;)V

    iput-object v0, p0, LI/c;->f:LI/f;

    new-instance v0, LI/d;

    invoke-direct {v0, p0}, LI/d;-><init>(LI/c;)V

    iput-object v0, p0, LI/c;->h:Lcom/google/android/location/internal/a;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NLP Client"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LI/c;->i:Landroid/os/HandlerThread;

    iput p2, p0, LI/c;->e:I

    iput-object p1, p0, LI/c;->k:Landroid/content/Context;

    iput-boolean p4, p0, LI/c;->l:Z

    new-instance v0, LI/a;

    invoke-direct {v0, p3, p5}, LI/a;-><init>(Landroid/location/LocationListener;Landroid/os/Looper;)V

    iput-object v0, p0, LI/c;->g:LI/a;

    iget-object v0, p0, LI/c;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, LI/e;

    iget-object v1, p0, LI/c;->i:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p6}, LI/e;-><init>(LI/c;Landroid/os/Looper;I)V

    iput-object v0, p0, LI/c;->j:LI/e;

    iget-object v1, p0, LI/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->addListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    invoke-direct {p0}, LI/c;->e()Lcom/google/android/location/internal/d;

    move-result-object v0

    invoke-direct {p0, v0}, LI/c;->a(Lcom/google/android/location/internal/d;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(LI/c;)LI/a;
    .locals 1

    iget-object v0, p0, LI/c;->g:LI/a;

    return-object v0
.end method

.method static synthetic a(LI/c;Lcom/google/android/location/internal/d;)V
    .locals 0

    invoke-direct {p0, p1}, LI/c;->a(Lcom/google/android/location/internal/d;)V

    return-void
.end method

.method private a(Lcom/google/android/location/internal/d;)V
    .locals 4

    iput-object p1, p0, LI/c;->b:Lcom/google/android/location/internal/d;

    sget-object v0, Lcom/google/android/location/internal/d;->f:Lcom/google/android/location/internal/d;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, LI/c;->c:LI/f;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LI/c;->f:LI/f;

    iput-object v0, p0, LI/c;->c:LI/f;

    iget-object v0, p0, LI/c;->k:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/location/internal/d;->e:Landroid/content/Intent;

    iget-object v2, p0, LI/c;->c:LI/f;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method static synthetic b(LI/c;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, LI/c;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(LI/c;)LI/f;
    .locals 1

    iget-object v0, p0, LI/c;->c:LI/f;

    return-object v0
.end method

.method static synthetic d(LI/c;)LI/e;
    .locals 1

    iget-object v0, p0, LI/c;->j:LI/e;

    return-object v0
.end method

.method private d()Lcom/google/android/location/internal/INetworkLocationInternal;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, LI/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, p0, LI/c;->d:Z

    if-eqz v2, :cond_0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LI/c;->c:LI/f;

    if-eqz v2, :cond_1

    iget-object v0, p0, LI/c;->c:LI/f;

    invoke-static {v0}, LI/f;->a(LI/f;)Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private e()Lcom/google/android/location/internal/d;
    .locals 2

    sget-object v0, Lcom/google/android/location/internal/e;->b:Lcom/google/android/location/internal/e;

    iget-object v1, p0, LI/c;->k:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/location/internal/d;->a(Lcom/google/android/location/internal/e;Landroid/content/Context;)Lcom/google/android/location/internal/d;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/location/internal/d;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/location/internal/d;->f:Lcom/google/android/location/internal/d;

    goto :goto_0
.end method

.method static synthetic e(LI/c;)Z
    .locals 1

    iget-boolean v0, p0, LI/c;->d:Z

    return v0
.end method

.method static synthetic f(LI/c;)Z
    .locals 1

    iget-boolean v0, p0, LI/c;->l:Z

    return v0
.end method

.method static synthetic g(LI/c;)I
    .locals 1

    iget v0, p0, LI/c;->e:I

    return v0
.end method

.method static synthetic h(LI/c;)Lcom/google/android/location/internal/a;
    .locals 1

    iget-object v0, p0, LI/c;->h:Lcom/google/android/location/internal/a;

    return-object v0
.end method

.method static synthetic i(LI/c;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, LI/c;->k:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(LI/c;)Lcom/google/android/location/internal/d;
    .locals 1

    invoke-direct {p0}, LI/c;->e()Lcom/google/android/location/internal/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(LI/c;)Lcom/google/android/location/internal/d;
    .locals 1

    iget-object v0, p0, LI/c;->b:Lcom/google/android/location/internal/d;

    return-object v0
.end method

.method static synthetic l(LI/c;)Landroid/os/HandlerThread;
    .locals 1

    iget-object v0, p0, LI/c;->i:Landroid/os/HandlerThread;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    iget-object v1, p0, LI/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LI/c;->j:LI/e;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 4

    iget-object v1, p0, LI/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LI/c;->d:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    iput p1, p0, LI/c;->e:I

    iget-object v0, p0, LI/c;->c:LI/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, LI/c;->j:LI/e;

    const/4 v2, 0x1

    iget-object v3, p0, LI/c;->c:LI/f;

    invoke-static {v0, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/location/Location;)[B
    .locals 3

    const/4 v0, 0x0

    const-string v1, "network"

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, LI/c;->d()Lcom/google/android/location/internal/INetworkLocationInternal;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1, p1}, Lcom/google/android/location/internal/INetworkLocationInternal;->a(Landroid/location/Location;)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget v0, p0, LI/c;->e:I

    return v0
.end method

.method public c()V
    .locals 4

    iget-object v1, p0, LI/c;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LI/c;->d:Z

    if-eqz v0, :cond_0

    monitor-exit v1

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/google/android/location/internal/NlpPackageUpdateReceiver;->removeListener(Lcom/google/android/location/internal/NlpPackageUpdateReceiver$Listener;)V

    iget-object v0, p0, LI/c;->c:LI/f;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LI/c;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LI/c;->j:LI/e;

    const/4 v2, 0x6

    iget-object v3, p0, LI/c;->c:LI/f;

    invoke-static {v0, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    iget-object v0, p0, LI/c;->j:LI/e;

    const/4 v2, 0x2

    iget-object v3, p0, LI/c;->c:LI/f;

    invoke-static {v0, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    iget-object v0, p0, LI/c;->j:LI/e;

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LI/c;->d:Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
