.class public Lbi/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Stages cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lbi/d;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Lbi/g;
    .locals 1

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    return-object v0
.end method

.method public a(II)Lbi/h;
    .locals 1

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0, p2}, Lbi/g;->a(I)Lbi/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbi/t;)Lbi/h;
    .locals 2

    iget v0, p1, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->b:I

    invoke-virtual {p0, v0, v1}, Lbi/d;->a(II)Lbi/h;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbi/a;)Z
    .locals 2

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {p0, v0}, Lbi/d;->b(Lbi/t;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lbi/a;->a:Lbi/t;

    invoke-virtual {p0, v0}, Lbi/d;->e(Lbi/t;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p1, Lbi/a;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)I
    .locals 1

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->a()I

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0}, Lbi/g;->a()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lbi/d;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbi/g;

    invoke-virtual {v0, v1}, Lbi/g;->a(I)Lbi/h;

    move-result-object v0

    invoke-virtual {v0}, Lbi/h;->b()Lbi/q;

    move-result-object v0

    sget-object v2, Lbi/q;->a:Lbi/q;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method b(II)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbi/d;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lbi/d;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lbi/t;)Z
    .locals 2

    iget v0, p1, Lbi/t;->a:I

    invoke-virtual {p0}, Lbi/d;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p1, Lbi/t;->a:I

    if-ltz v0, :cond_0

    iget v0, p1, Lbi/t;->b:I

    iget v1, p1, Lbi/t;->a:I

    invoke-virtual {p0, v1}, Lbi/d;->b(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p1, Lbi/t;->b:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()F
    .locals 3

    new-instance v1, Lbi/v;

    invoke-direct {v1, p0}, Lbi/v;-><init>(Lbi/d;)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lbi/v;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lbi/v;->a()Lbi/t;

    move-result-object v2

    invoke-virtual {p0, v2}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v2

    invoke-virtual {v2}, Lbi/h;->y()F

    move-result v2

    add-float/2addr v0, v2

    goto :goto_0

    :cond_0
    return v0
.end method

.method c(I)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(II)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbi/d;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lbi/d;->d(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lbi/t;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p1, Lbi/t;->a:I

    invoke-virtual {p0}, Lbi/d;->a()I

    move-result v3

    if-lt v0, v3, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p0, p1}, Lbi/d;->a(Lbi/t;)Lbi/h;

    move-result-object v3

    iget v0, p1, Lbi/t;->a:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lbi/d;->a(I)Lbi/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Lbi/g;->a(I)Lbi/h;

    move-result-object v4

    invoke-virtual {v3}, Lbi/h;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v4}, Lbi/h;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v3}, Lbi/h;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lbi/h;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3}, Lbi/h;->b()Lbi/q;

    move-result-object v5

    sget-object v6, Lbi/q;->d:Lbi/q;

    if-ne v5, v6, :cond_2

    invoke-virtual {v4}, Lbi/h;->b()Lbi/q;

    move-result-object v5

    sget-object v6, Lbi/q;->b:Lbi/q;

    if-ne v5, v6, :cond_2

    invoke-virtual {v3}, Lbi/h;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lbi/h;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    :goto_2
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method d(I)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d(II)Z
    .locals 1

    invoke-virtual {p0, p1}, Lbi/d;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Lbi/t;)Z
    .locals 1

    iget v0, p1, Lbi/t;->a:I

    invoke-virtual {p0, v0}, Lbi/d;->c(I)Z

    move-result v0

    return v0
.end method

.method e(I)Z
    .locals 1

    invoke-virtual {p0}, Lbi/d;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e(Lbi/t;)Z
    .locals 2

    iget v0, p1, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->b:I

    invoke-virtual {p0, v0, v1}, Lbi/d;->c(II)Z

    move-result v0

    return v0
.end method

.method public f(Lbi/t;)Z
    .locals 1

    iget v0, p1, Lbi/t;->a:I

    invoke-virtual {p0, v0}, Lbi/d;->e(I)Z

    move-result v0

    return v0
.end method

.method public g(Lbi/t;)Z
    .locals 2

    iget v0, p1, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->b:I

    invoke-virtual {p0, v0, v1}, Lbi/d;->d(II)Z

    move-result v0

    return v0
.end method
