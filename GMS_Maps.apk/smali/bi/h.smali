.class public abstract Lbi/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lax/t;

.field protected final b:Lax/w;

.field protected final c:I

.field private final d:Lbi/q;

.field private final e:Ljava/util/List;

.field private f:Lcom/google/common/collect/ImmutableList;

.field private g:[F

.field private h:F

.field private i:I

.field private j:I

.field private k:I

.field private l:F


# direct methods
.method protected constructor <init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V
    .locals 2

    const/high16 v1, -0x40800000

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lbi/h;->h:F

    iput v0, p0, Lbi/h;->i:I

    iput v0, p0, Lbi/h;->j:I

    iput v0, p0, Lbi/h;->k:I

    iput v1, p0, Lbi/h;->l:F

    iput-object p1, p0, Lbi/h;->d:Lbi/q;

    iput-object p2, p0, Lbi/h;->b:Lax/w;

    iput-object p3, p0, Lbi/h;->a:Lax/t;

    iput p4, p0, Lbi/h;->c:I

    if-eqz p5, :cond_0

    invoke-static {p5}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lbi/h;->e:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lbi/h;->e:Ljava/util/List;

    goto :goto_0
.end method

.method private static C()F
    .locals 1

    invoke-static {}, LX/j;->a()LX/q;

    move-result-object v0

    iget v0, v0, LX/q;->y:F

    return v0
.end method

.method static synthetic a(Ljava/util/Date;Ljava/util/Date;)I
    .locals 1

    invoke-static {p0, p1}, Lbi/h;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    return v0
.end method

.method static a(LaN/B;LaN/B;Ljava/util/List;F)Lcom/google/common/collect/ImmutableList;
    .locals 4

    invoke-static {p2}, Lcom/google/common/collect/bx;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/B;

    invoke-static {p0, v0}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v3

    cmpg-float v3, v3, p3

    if-gez v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    move-object p0, v0

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/B;

    invoke-static {v0, p1}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    cmpg-float v0, v0, p3

    if-gez v0, :cond_2

    invoke-virtual {v1}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-static {v1}, Lcom/google/common/collect/ImmutableList;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method private a(Lax/m;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/h;->b:Lax/w;

    invoke-virtual {v0, p1}, Lax/w;->a(Lax/m;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lax/t;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lbi/h;->b(Lax/t;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Lax/y;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/googlenav/ui/bv;->b(Lax/y;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lau/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;
    .locals 1

    invoke-static {p0}, Lbi/h;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method static a(Lax/w;ILbi/h;)V
    .locals 7

    const/4 v2, 0x0

    invoke-virtual {p0, p1}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-virtual {v0}, Lax/m;->y()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {v0}, Lax/m;->r()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lax/m;->n()Z

    move-result v4

    if-eqz v4, :cond_3

    check-cast v0, Lax/a;

    invoke-virtual {v0}, Lax/a;->j()I

    move-result v3

    invoke-virtual {v0}, Lax/a;->i()I

    move-result v0

    int-to-float v1, v0

    :cond_0
    cmpl-float v0, v1, v2

    if-lez v0, :cond_1

    invoke-virtual {p2, v1}, Lbi/h;->a(F)V

    :cond_1
    if-lez v3, :cond_2

    invoke-virtual {p2, v3}, Lbi/h;->a(I)V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lax/w;->ae()I

    move-result v4

    if-ge v0, v4, :cond_0

    invoke-virtual {p0, v0}, Lax/w;->n(I)Lax/t;

    move-result-object v4

    invoke-virtual {v4}, Lax/t;->E()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    invoke-virtual {v4}, Lax/t;->x()I

    move-result v5

    add-int/2addr v3, v5

    invoke-virtual {v4}, Lax/t;->v()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/util/Date;Ljava/util/Date;)I
    .locals 4

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static b(Lax/t;)Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lax/t;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/google/googlenav/ui/bd;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lax/w;ILbi/h;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lbi/h;->c(Lax/w;ILbi/h;)V

    return-void
.end method

.method private static c(Lax/w;ILbi/h;)V
    .locals 2

    invoke-static {p0, p1, p2}, Lbi/h;->a(Lax/w;ILbi/h;)V

    invoke-virtual {p2}, Lbi/h;->r()F

    move-result v0

    const/high16 v1, -0x40800000

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lbi/h;->y()F

    move-result v0

    invoke-virtual {p2, v0}, Lbi/h;->a(F)V

    :cond_0
    iget v0, p2, Lbi/h;->k:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p2}, Lbi/h;->s()V

    :cond_1
    return-void
.end method


# virtual methods
.method public A()I
    .locals 1

    iget v0, p0, Lbi/h;->i:I

    return v0
.end method

.method public B()I
    .locals 1

    iget v0, p0, Lbi/h;->j:I

    return v0
.end method

.method public a(Ljava/util/Date;)J
    .locals 2

    invoke-virtual {p0}, Lbi/h;->n()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p1, v0}, Lbi/h;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public a()Lax/t;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    return-object v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Lbi/h;->l:F

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lbi/h;->k:I

    return-void
.end method

.method public b(F)I
    .locals 3

    invoke-virtual {p0}, Lbi/h;->y()F

    move-result v0

    mul-float v1, p1, v0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lbi/h;->c(I)F

    move-result v2

    cmpg-float v2, v2, v1

    if-gez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public b(I)LaN/B;
    .locals 5

    const/4 v4, -0x1

    invoke-virtual {p0}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v1

    if-gt v4, p1, :cond_0

    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "index not in range:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    if-ne p1, v4, :cond_1

    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lbi/h;->v()LaN/B;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-virtual {v1, p1}, Lcom/google/common/collect/ImmutableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/B;

    goto :goto_1
.end method

.method public b()Lbi/q;
    .locals 1

    iget-object v0, p0, Lbi/h;->d:Lbi/q;

    return-object v0
.end method

.method public c(I)F
    .locals 7

    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v5

    if-gt v6, p1, :cond_0

    if-gt p1, v5, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shapePointIndex not in range [0, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbi/h;->g:[F

    if-nez v0, :cond_1

    add-int/lit8 v0, v5, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lbi/h;->g:[F

    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    move v4, v2

    :goto_1
    if-gt v1, v5, :cond_1

    invoke-virtual {p0, v1}, Lbi/h;->b(I)LaN/B;

    move-result-object v3

    invoke-static {v0, v3}, LX/g;->a(LaN/B;LaN/B;)F

    move-result v0

    add-float/2addr v0, v4

    iget-object v4, p0, Lbi/h;->g:[F

    aput v0, v4, v1

    add-int/lit8 v1, v1, 0x1

    move v4, v0

    move-object v0, v3

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-ne p1, v6, :cond_2

    move v0, v2

    :goto_2
    return v0

    :cond_2
    iget-object v0, p0, Lbi/h;->g:[F

    aget v0, v0, p1

    goto :goto_2
.end method

.method public c(F)V
    .locals 0

    iput p1, p0, Lbi/h;->h:F

    return-void
.end method

.method public c()Z
    .locals 2

    sget-object v0, Lbi/i;->a:[I

    iget-object v1, p0, Lbi/h;->d:Lbi/q;

    invoke-virtual {v1}, Lbi/q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public d(I)F
    .locals 2

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lbi/h;->c(I)F

    move-result v0

    invoke-virtual {p0, p1}, Lbi/h;->c(I)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lbi/h;->c:I

    return v0
.end method

.method public e()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbi/h;->b:Lax/w;

    iget v1, p0, Lbi/h;->c:I

    invoke-virtual {v0, v1}, Lax/w;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lax/m;

    invoke-direct {p0, v0}, Lbi/h;->a(Lax/m;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(I)V
    .locals 0

    iput p1, p0, Lbi/h;->i:I

    return-void
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->z()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(I)V
    .locals 0

    iput p1, p0, Lbi/h;->j:I

    return-void
.end method

.method public g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->D()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public h()J
    .locals 2

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->y()J

    move-result-wide v0

    return-wide v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->m()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbi/h;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public abstract n()Ljava/util/Date;
.end method

.method public abstract o()Ljava/util/Date;
.end method

.method public p()Z
    .locals 2

    iget v0, p0, Lbi/h;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    invoke-virtual {p0}, Lbi/h;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbi/h;->k:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()F
    .locals 1

    iget v0, p0, Lbi/h;->l:F

    return v0
.end method

.method protected s()V
    .locals 2

    invoke-virtual {p0}, Lbi/h;->n()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0}, Lbi/h;->o()Ljava/util/Date;

    move-result-object v1

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbi/h;->n()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p0}, Lbi/h;->o()Ljava/util/Date;

    move-result-object v1

    invoke-static {v0, v1}, Lbi/h;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v0

    iput v0, p0, Lbi/h;->k:I

    goto :goto_0
.end method

.method protected t()Z
    .locals 1

    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbi/h;->v()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract u()LaN/B;
.end method

.method public abstract v()LaN/B;
.end method

.method public abstract w()Ljava/lang/String;
.end method

.method public x()Lcom/google/common/collect/ImmutableList;
    .locals 4

    iget-object v0, p0, Lbi/h;->f:Lcom/google/common/collect/ImmutableList;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbi/h;->u()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, Lbi/h;->v()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbi/h;->e:Ljava/util/List;

    invoke-static {}, Lbi/h;->C()F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lbi/h;->a(LaN/B;LaN/B;Ljava/util/List;F)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    iput-object v0, p0, Lbi/h;->f:Lcom/google/common/collect/ImmutableList;

    :cond_0
    iget-object v0, p0, Lbi/h;->f:Lcom/google/common/collect/ImmutableList;

    return-object v0
.end method

.method public y()F
    .locals 1

    invoke-virtual {p0}, Lbi/h;->x()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, Lbi/h;->c(I)F

    move-result v0

    return v0
.end method

.method public z()F
    .locals 1

    iget v0, p0, Lbi/h;->h:F

    return v0
.end method
