.class public Lbi/v;
.super Lcom/google/common/collect/dY;
.source "SourceFile"

# interfaces
.implements Lcom/google/common/collect/di;


# instance fields
.field private final a:Lbi/d;

.field private final b:Lbi/w;

.field private c:Z


# direct methods
.method public constructor <init>(Lbi/d;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/common/collect/dY;-><init>()V

    iput-object p1, p0, Lbi/v;->a:Lbi/d;

    new-instance v0, Lbi/w;

    invoke-direct {v0, v1, v1}, Lbi/w;-><init>(II)V

    iput-object v0, p0, Lbi/v;->b:Lbi/w;

    iput-boolean v1, p0, Lbi/v;->c:Z

    return-void
.end method

.method private a(II)Lbi/t;
    .locals 2

    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    invoke-virtual {v0, p1, p2}, Lbi/d;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    invoke-virtual {v0, p2}, Lbi/d;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 p1, p1, -0x1

    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    invoke-virtual {v0, p1}, Lbi/d;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    new-instance v1, Lbi/t;

    invoke-direct {v1, p1, v0}, Lbi/t;-><init>(II)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p2, -0x1

    goto :goto_1
.end method


# virtual methods
.method public a()Lbi/t;
    .locals 3

    iget-boolean v0, p0, Lbi/v;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbi/v;->c:Z

    :goto_0
    new-instance v0, Lbi/t;

    iget-object v1, p0, Lbi/v;->b:Lbi/w;

    iget v1, v1, Lbi/w;->a:I

    iget-object v2, p0, Lbi/v;->b:Lbi/w;

    iget v2, v2, Lbi/w;->b:I

    invoke-direct {v0, v1, v2}, Lbi/t;-><init>(II)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    iget-object v1, p0, Lbi/v;->b:Lbi/w;

    iget v1, v1, Lbi/w;->a:I

    iget-object v2, p0, Lbi/v;->b:Lbi/w;

    iget v2, v2, Lbi/w;->b:I

    invoke-virtual {v0, v1, v2}, Lbi/d;->d(II)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v1, v0, Lbi/w;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lbi/w;->b:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    iget-object v1, p0, Lbi/v;->b:Lbi/w;

    iget v1, v1, Lbi/w;->a:I

    invoke-virtual {v0, v1}, Lbi/d;->e(I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot increment from the last segment."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v1, v0, Lbi/w;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lbi/w;->a:I

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    const/4 v1, 0x0

    iput v1, v0, Lbi/w;->b:I

    goto :goto_0
.end method

.method public a(Lbi/t;)Lbi/v;
    .locals 2

    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    invoke-virtual {v0, p1}, Lbi/d;->b(Lbi/t;)Z

    move-result v0

    const-string v1, "Index out of range."

    invoke-static {v0, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v1, p1, Lbi/t;->a:I

    iput v1, v0, Lbi/w;->a:I

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v1, p1, Lbi/t;->b:I

    iput v1, v0, Lbi/w;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbi/v;->c:Z

    return-object p0
.end method

.method public b()Lbi/t;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbi/v;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    iget-boolean v1, p0, Lbi/v;->c:Z

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    new-instance v2, Lbi/t;

    invoke-direct {v2, v1, v0}, Lbi/t;-><init>(II)V

    return-object v2

    :cond_1
    iget-object v1, p0, Lbi/v;->a:Lbi/d;

    iget-object v2, p0, Lbi/v;->b:Lbi/w;

    iget v2, v2, Lbi/w;->a:I

    iget-object v3, p0, Lbi/v;->b:Lbi/w;

    iget v3, v3, Lbi/w;->b:I

    invoke-virtual {v1, v2, v3}, Lbi/d;->d(II)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbi/v;->b:Lbi/w;

    iget v1, v1, Lbi/w;->a:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v1, v0, Lbi/w;->a:I

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v0, v0, Lbi/w;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b(Lbi/t;)Lbi/v;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    invoke-virtual {v0, p1}, Lbi/d;->b(Lbi/t;)Z

    move-result v0

    const-string v1, "Index out of range."

    invoke-static {v0, v1}, Lcom/google/common/base/J;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lbi/v;->a:Lbi/d;

    iget v1, p1, Lbi/t;->a:I

    iget v2, p1, Lbi/t;->b:I

    invoke-virtual {v0, v1, v2}, Lbi/d;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iput v3, v0, Lbi/w;->a:I

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iput v3, v0, Lbi/w;->b:I

    iput-boolean v3, p0, Lbi/v;->c:Z

    :goto_0
    return-object p0

    :cond_0
    iget v0, p1, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->b:I

    invoke-direct {p0, v0, v1}, Lbi/v;->a(II)Lbi/t;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbi/v;->a(Lbi/t;)Lbi/v;

    move-result-object p0

    goto :goto_0
.end method

.method public c(Lbi/t;)Lbi/t;
    .locals 2

    iget v0, p1, Lbi/t;->a:I

    iget v1, p1, Lbi/t;->b:I

    invoke-direct {p0, v0, v1}, Lbi/v;->a(II)Lbi/t;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lbi/v;->b()Lbi/t;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 3

    iget-object v0, p0, Lbi/v;->b:Lbi/w;

    iget v0, v0, Lbi/w;->b:I

    iget-object v1, p0, Lbi/v;->a:Lbi/d;

    iget-object v2, p0, Lbi/v;->b:Lbi/w;

    iget v2, v2, Lbi/w;->a:I

    invoke-virtual {v1, v2}, Lbi/d;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lbi/v;->c:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lbi/v;->a:Lbi/d;

    invoke-virtual {v2}, Lbi/d;->a()I

    move-result v2

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lbi/v;->a:Lbi/d;

    iget-object v3, p0, Lbi/v;->b:Lbi/w;

    iget v3, v3, Lbi/w;->a:I

    iget-object v4, p0, Lbi/v;->b:Lbi/w;

    iget v4, v4, Lbi/w;->b:I

    invoke-virtual {v2, v3, v4}, Lbi/d;->c(II)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lbi/v;->a()Lbi/t;

    move-result-object v0

    return-object v0
.end method
