.class public Lbi/l;
.super Lbi/h;
.source "SourceFile"


# instance fields
.field private final d:Lax/t;


# direct methods
.method public constructor <init>(Lax/w;Lax/t;Lax/t;ILjava/util/List;)V
    .locals 6

    sget-object v1, Lbi/q;->a:Lbi/q;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lbi/h;-><init>(Lbi/q;Lax/w;Lax/t;ILjava/util/List;)V

    iput-object p3, p0, Lbi/l;->d:Lax/t;

    invoke-virtual {p0}, Lbi/l;->C()V

    return-void
.end method


# virtual methods
.method protected C()V
    .locals 2

    iget-object v0, p0, Lbi/l;->b:Lax/w;

    iget v1, p0, Lbi/l;->c:I

    invoke-static {v0, v1, p0}, Lbi/h;->b(Lax/w;ILbi/h;)V

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x4cc

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lbi/l;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {v0}, Lbi/h;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public o()Ljava/util/Date;
    .locals 7

    invoke-virtual {p0}, Lbi/l;->n()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbi/l;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbi/l;->n()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0}, Lbi/l;->q()I

    move-result v2

    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    int-to-long v1, v2

    const-wide/16 v5, 0x3e8

    mul-long/2addr v1, v5

    add-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()LaN/B;
    .locals 1

    iget-object v0, p0, Lbi/l;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->q()LaN/B;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbi/l;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->g()LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/l;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->q()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public v()LaN/B;
    .locals 1

    iget-object v0, p0, Lbi/l;->d:Lax/t;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbi/l;->b:Lax/w;

    invoke-virtual {v0}, Lax/w;->aC()LaN/B;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbi/l;->d:Lax/t;

    invoke-virtual {v0}, Lax/t;->g()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public w()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lbi/l;->a:Lax/t;

    invoke-virtual {v0}, Lax/t;->i()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lbi/l;->d:Lax/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbi/l;->d:Lax/t;

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lbi/l;->d:Lax/t;

    invoke-virtual {v0}, Lax/t;->l()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lbi/l;->b:Lax/w;

    invoke-virtual {v0}, Lax/w;->as()Lax/y;

    move-result-object v0

    invoke-static {v0}, Lbi/l;->a(Lax/y;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
