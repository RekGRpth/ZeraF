.class public LL/I;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LL/P;


# instance fields
.field private final a:Lcom/google/android/maps/driveabout/app/dO;

.field private final b:Lcom/google/android/maps/driveabout/app/NavigationActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/app/dO;Lcom/google/android/maps/driveabout/app/NavigationActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    iput-object p2, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    return-void
.end method

.method static synthetic a(LL/I;LL/C;)V
    .locals 0

    invoke-direct {p0, p1}, LL/I;->b(LL/C;)V

    return-void
.end method

.method static synthetic a(LL/I;LL/E;)V
    .locals 0

    invoke-direct {p0, p1}, LL/I;->b(LL/E;)V

    return-void
.end method

.method static synthetic a(LL/I;LL/F;)V
    .locals 0

    invoke-direct {p0, p1}, LL/I;->b(LL/F;)V

    return-void
.end method

.method private b(LL/C;)V
    .locals 4

    invoke-virtual {p1}, LL/C;->g()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mute"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {p1}, LL/C;->h()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/maps/driveabout/app/dO;->a(Z)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-string v1, "compassButton"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->q()V

    goto :goto_1

    :cond_2
    const-string v1, "back"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->s()V

    goto :goto_1

    :cond_3
    const-string v1, "routeOverview"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->j()V

    goto :goto_1

    :cond_4
    const-string v1, "layers"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    const-string v1, "clear"

    invoke-virtual {p1, v1}, LL/C;->b(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->b(Z)V

    goto :goto_1

    :cond_5
    const-string v1, "viewList"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->e()V

    goto :goto_1

    :cond_6
    const-string v1, "viewTraffic"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {p1}, LL/C;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->d(Z)V

    goto :goto_1

    :cond_7
    const-string v1, "backToLocation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->h()V

    goto :goto_1

    :cond_8
    const-string v1, "viewSatellite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {p1}, LL/C;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->c(Z)V

    goto :goto_1

    :cond_9
    const-string v1, "zoomIn"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->y()V

    goto/16 :goto_1

    :cond_a
    const-string v1, "zoomOut"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->z()V

    goto/16 :goto_1

    :cond_b
    const-string v1, "showStreetView"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->f()V

    goto/16 :goto_1

    :cond_c
    const-string v1, "routeAroundTrafficOverview"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->k()V

    goto/16 :goto_1

    :cond_d
    const-string v1, "routeAroundTrafficConfirm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->l()V

    goto/16 :goto_1

    :cond_e
    const-string v1, "routeAroundTrafficCancel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->m()V

    goto/16 :goto_1

    :cond_f
    const-string v1, "routeAroundTrafficTimeout"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/dO;->n()V

    goto/16 :goto_1

    :cond_10
    const-string v1, "FullUIEventPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unhandled Action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private b(LL/E;)V
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1}, LL/E;->h()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, LL/E;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {p1}, LL/E;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/maps/driveabout/app/eF;->a(Landroid/app/Activity;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FAIL: Unable to find view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LL/E;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    invoke-virtual {p1}, LL/E;->f()[Landroid/view/KeyEvent;

    move-result-object v2

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_4

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, LL/E;->f()[Landroid/view/KeyEvent;

    move-result-object v1

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    iget-object v4, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v4, v3}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e()Lcom/google/android/maps/driveabout/app/an;

    move-result-object v1

    invoke-virtual {p1}, LL/E;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/maps/driveabout/app/an;->a(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, LL/E;->f()[Landroid/view/KeyEvent;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v0, v3, :cond_4

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to deliver event to dialog: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LL/E;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method private b(LL/F;)V
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {p1, v1}, LL/F;->a(Lcom/google/android/maps/driveabout/app/NavigationActivity;)[Landroid/view/MotionEvent;

    move-result-object v1

    invoke-virtual {p1}, LL/F;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    iget-object v4, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v4, v3}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, LL/I;->b:Lcom/google/android/maps/driveabout/app/NavigationActivity;

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/app/NavigationActivity;->e()Lcom/google/android/maps/driveabout/app/an;

    move-result-object v2

    invoke-virtual {p1}, LL/F;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/maps/driveabout/app/an;->a(Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_1

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Landroid/app/Dialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to deliver event to dialog: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LL/F;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method public a(LL/C;)V
    .locals 2

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    new-instance v1, LL/J;

    invoke-direct {v1, p0, p1}, LL/J;-><init>(LL/I;LL/C;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(LL/E;)V
    .locals 2

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    new-instance v1, LL/K;

    invoke-direct {v1, p0, p1}, LL/K;-><init>(LL/I;LL/E;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(LL/F;)V
    .locals 2

    iget-object v0, p0, LL/I;->a:Lcom/google/android/maps/driveabout/app/dO;

    new-instance v1, LL/L;

    invoke-direct {v1, p0, p1}, LL/L;-><init>(LL/I;LL/F;)V

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/app/dO;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public a(LL/n;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
