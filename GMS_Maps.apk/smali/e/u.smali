.class public Le/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Le/x;


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Le/y;

    invoke-direct {v0}, Le/y;-><init>()V

    sput-object v0, Le/u;->a:Le/x;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_1

    new-instance v0, Le/w;

    invoke-direct {v0}, Le/w;-><init>()V

    sput-object v0, Le/u;->a:Le/x;

    goto :goto_0

    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_2

    new-instance v0, Le/v;

    invoke-direct {v0}, Le/v;-><init>()V

    sput-object v0, Le/u;->a:Le/x;

    goto :goto_0

    :cond_2
    new-instance v0, Le/z;

    invoke-direct {v0}, Le/z;-><init>()V

    sput-object v0, Le/u;->a:Le/x;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/u;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;I)V
    .locals 2

    sget-object v0, Le/u;->a:Le/x;

    iget-object v1, p0, Le/u;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Le/x;->a(Ljava/lang/Object;Landroid/view/View;I)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Le/u;

    iget-object v2, p0, Le/u;->b:Ljava/lang/Object;

    if-nez v2, :cond_4

    iget-object v2, p1, Le/u;->b:Ljava/lang/Object;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Le/u;->b:Ljava/lang/Object;

    iget-object v3, p1, Le/u;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Le/u;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Le/u;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
