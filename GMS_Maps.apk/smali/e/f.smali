.class public Le/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Le/h;


# instance fields
.field private final b:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Le/i;

    invoke-direct {v0}, Le/i;-><init>()V

    sput-object v0, Le/f;->a:Le/h;

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    new-instance v0, Le/g;

    invoke-direct {v0}, Le/g;-><init>()V

    sput-object v0, Le/f;->a:Le/h;

    goto :goto_0

    :cond_1
    new-instance v0, Le/j;

    invoke-direct {v0}, Le/j;-><init>()V

    sput-object v0, Le/f;->a:Le/h;

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le/f;->b:Ljava/lang/Object;

    return-void
.end method

.method public static a(Landroid/view/View;)Le/f;
    .locals 1

    sget-object v0, Le/f;->a:Le/h;

    invoke-interface {v0, p0}, Le/h;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Le/f;->a(Ljava/lang/Object;)Le/f;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/Object;)Le/f;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, Le/f;

    invoke-direct {v0, p0}, Le/f;-><init>(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b()Le/f;
    .locals 1

    sget-object v0, Le/f;->a:Le/h;

    invoke-interface {v0}, Le/h;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Le/f;->a(Ljava/lang/Object;)Le/f;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Le/f;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public a(I)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->a(Ljava/lang/Object;I)V

    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->a(Ljava/lang/Object;Landroid/graphics/Rect;)V

    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Le/h;->a(Ljava/lang/Object;Landroid/view/View;I)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->c(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->c(Ljava/lang/Object;Z)V

    return-void
.end method

.method public b(Landroid/graphics/Rect;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->c(Ljava/lang/Object;Landroid/graphics/Rect;)V

    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->a(Ljava/lang/Object;Landroid/view/View;)V

    return-void
.end method

.method public b(Landroid/view/View;I)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Le/h;->b(Ljava/lang/Object;Landroid/view/View;I)V

    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->a(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public b(Z)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->e(Ljava/lang/Object;Z)V

    return-void
.end method

.method public c()Ljava/lang/CharSequence;
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Le/h;->b(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/graphics/Rect;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->b(Ljava/lang/Object;Landroid/graphics/Rect;)V

    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->b(Ljava/lang/Object;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public c(Z)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->a(Ljava/lang/Object;Z)V

    return-void
.end method

.method public d()Ljava/lang/CharSequence;
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Le/h;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/graphics/Rect;)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->d(Ljava/lang/Object;Landroid/graphics/Rect;)V

    return-void
.end method

.method public d(Z)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->b(Ljava/lang/Object;Z)V

    return-void
.end method

.method public e()V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1}, Le/h;->c(Ljava/lang/Object;)V

    return-void
.end method

.method public e(Z)V
    .locals 2

    sget-object v0, Le/f;->a:Le/h;

    iget-object v1, p0, Le/f;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Le/h;->d(Ljava/lang/Object;Z)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Le/f;

    iget-object v2, p0, Le/f;->b:Ljava/lang/Object;

    if-nez v2, :cond_4

    iget-object v2, p1, Le/f;->b:Ljava/lang/Object;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Le/f;->b:Ljava/lang/Object;

    iget-object v3, p1, Le/f;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Le/f;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Le/f;->b:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
