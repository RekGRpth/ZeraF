.class public Lbf/aU;
.super Lbf/i;
.source "SourceFile"


# instance fields
.field private A:Lax/y;

.field private B:Lax/y;

.field private C:Lax/z;

.field private D:Lcom/google/googlenav/ba;

.field private E:I

.field private final F:Lbf/aV;

.field u:Lax/b;

.field private v:I

.field private w:I

.field private x:I

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;I)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lbf/i;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    const/4 v0, 0x0

    iput v0, p0, Lbf/aU;->E:I

    iput p6, p0, Lbf/aU;->E:I

    const-string v0, "locationMemory"

    invoke-static {v0}, Lax/z;->b(Ljava/lang/String;)Lax/z;

    move-result-object v0

    iput-object v0, p0, Lbf/aU;->C:Lax/z;

    new-instance v0, Lbf/aV;

    invoke-direct {v0, p0}, Lbf/aV;-><init>(Lbf/aU;)V

    iput-object v0, p0, Lbf/aU;->F:Lbf/aV;

    return-void
.end method

.method private bj()V
    .locals 12

    const/4 v9, 0x0

    const/4 v11, -0x1

    iget-boolean v0, p0, Lbf/aU;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    invoke-virtual {v0}, Lax/b;->C()[Lax/y;

    move-result-object v0

    move-object v7, v0

    :goto_0
    array-length v0, v7

    new-array v10, v0, [Lcom/google/googlenav/ai;

    move v8, v9

    :goto_1
    array-length v0, v7

    if-ge v8, v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ai;

    aget-object v1, v7, v8

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    aget-object v2, v7, v8

    invoke-virtual {v2}, Lax/y;->h()Ljava/lang/String;

    move-result-object v2

    aget-object v3, v7, v8

    invoke-virtual {v3}, Lax/y;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    aput-object v0, v10, v8

    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    invoke-virtual {v0}, Lax/b;->D()[Lax/y;

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-static {v10, v0, v11, v11}, Lcom/google/googlenav/aZ;->a([Lcom/google/googlenav/ai;LaN/H;II)Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aZ;->d(I)V

    iget v1, p0, Lbf/aU;->v:I

    invoke-virtual {p0, v0, v9, v1}, Lbf/aU;->a(Lcom/google/googlenav/aZ;ZI)V

    return-void
.end method

.method private bk()V
    .locals 7

    const v2, 0x7fffffff

    const/high16 v1, -0x80000000

    const/4 v0, 0x0

    move v3, v1

    move v4, v2

    :goto_0
    iget-object v5, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v5}, Lcom/google/googlenav/F;->f()I

    move-result v5

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v5, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, LaN/B;->c()I

    move-result v6

    invoke-static {v6, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v5}, LaN/B;->c()I

    move-result v6

    invoke-static {v6, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {v5}, LaN/B;->e()I

    move-result v6

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {v5}, LaN/B;->e()I

    move-result v5

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    sub-int v0, v3, v4

    iput v0, p0, Lbf/aU;->w:I

    sub-int v0, v1, v2

    iput v0, p0, Lbf/aU;->x:I

    return-void
.end method

.method private bl()V
    .locals 7

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->I()[Lcom/google/googlenav/ai;

    move-result-object v0

    array-length v0, v0

    :goto_0
    const/16 v1, 0x38

    const-string v2, "s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "m="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lbf/aU;->bm()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "c="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    goto :goto_0
.end method

.method private bm()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lbf/aU;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lbf/aU;->v:I

    if-nez v0, :cond_0

    const-string v0, "s"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "m"

    goto :goto_0

    :cond_1
    const-string v0, "w"

    goto :goto_0
.end method

.method private bn()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lbf/aU;->h(I)V

    return-void
.end method

.method private h(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/aU;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->e(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aO()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/aU;->d:LaN/u;

    iget-object v1, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    :cond_1
    const/4 v0, 0x1

    iget-object v1, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->q(Z)V

    :cond_2
    invoke-virtual {p0}, Lbf/aU;->d()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lbf/aU;->y:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    invoke-virtual {v0}, Lax/b;->C()[Lax/y;

    move-result-object v0

    aget-object v0, v0, p1

    iput-object v0, p0, Lbf/aU;->A:Lax/y;

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lax/y;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lbf/aU;->C:Lax/z;

    invoke-virtual {v1, v0}, Lax/z;->a(Lax/y;)Z

    :cond_3
    iget-boolean v0, p0, Lbf/aU;->z:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lbf/aU;->y:Z

    if-eqz v0, :cond_6

    invoke-direct {p0, v2}, Lbf/aU;->k(Z)V

    iput-boolean v2, p0, Lbf/aU;->y:Z

    invoke-direct {p0}, Lbf/aU;->bj()V

    :goto_2
    return-void

    :cond_4
    invoke-virtual {p0, p1}, Lbf/aU;->b(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    invoke-virtual {v0}, Lax/b;->D()[Lax/y;

    move-result-object v0

    aget-object v0, v0, p1

    iput-object v0, p0, Lbf/aU;->B:Lax/y;

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lbf/aU;->A:Lax/y;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    iget-object v1, p0, Lbf/aU;->A:Lax/y;

    invoke-virtual {v0, v1}, Lax/b;->a(Lax/y;)V

    :cond_7
    iget-object v0, p0, Lbf/aU;->B:Lax/y;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    iget-object v1, p0, Lbf/aU;->B:Lax/y;

    invoke-virtual {v0, v1}, Lax/b;->b(Lax/y;)V

    :cond_8
    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    invoke-virtual {v0}, Lax/b;->w()V

    :cond_9
    invoke-direct {p0, v2}, Lbf/aU;->k(Z)V

    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/aU;)V

    iget-object v0, p0, Lbf/aU;->D:Lcom/google/googlenav/ba;

    if-eqz v0, :cond_a

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a

    iget-object v1, p0, Lbf/aU;->D:Lcom/google/googlenav/ba;

    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/googlenav/ba;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/aZ;)V

    :cond_a
    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    goto :goto_2
.end method

.method private k(Z)V
    .locals 7

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->J()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/16 v1, 0x38

    const-string v2, "e"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "m="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0}, Lbf/aU;->bm()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "r="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "b"

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected U()I
    .locals 1

    const/16 v0, 0xf

    return v0
.end method

.method public a()I
    .locals 1

    iget v0, p0, Lbf/aU;->v:I

    return v0
.end method

.method public a(Lax/b;I)V
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iput-object p1, p0, Lbf/aU;->u:Lax/b;

    iput p2, p0, Lbf/aU;->v:I

    iput-object v0, p0, Lbf/aU;->A:Lax/y;

    iput-object v0, p0, Lbf/aU;->B:Lax/y;

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lbf/aU;->y:Z

    if-ne p2, v1, :cond_5

    invoke-virtual {p1}, Lax/b;->C()[Lax/y;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p1}, Lax/b;->D()[Lax/y;

    move-result-object v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_2
    iput-boolean v0, p0, Lbf/aU;->y:Z

    if-eqz v0, :cond_0

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    iput-boolean v2, p0, Lbf/aU;->z:Z

    :goto_3
    invoke-direct {p0}, Lbf/aU;->bj()V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    iput-boolean v1, p0, Lbf/aU;->z:Z

    goto :goto_3
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 3

    iput-object p1, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    instance-of v0, p1, Lcom/google/googlenav/aZ;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-interface {p1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-interface {p1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lbf/aU;->y()V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/ba;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p3}, Lbf/aU;->a(Lcom/google/googlenav/aZ;ZI)V

    iput-object p2, p0, Lbf/aU;->D:Lcom/google/googlenav/ba;

    return-void
.end method

.method public a(Lcom/google/googlenav/aZ;ZI)V
    .locals 3

    iput p3, p0, Lbf/aU;->v:I

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lbf/aU;->b(Lcom/google/googlenav/F;)V

    invoke-direct {p0}, Lbf/aU;->bl()V

    const/4 v0, 0x5

    if-eq p3, v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/aU;->b(I)V

    :cond_1
    if-eqz p2, :cond_2

    invoke-direct {p0}, Lbf/aU;->bk()V

    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->R()V

    iget-object v0, p0, Lbf/aU;->d:LaN/u;

    invoke-virtual {p0}, Lbf/aU;->s()Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->c(LaN/B;)V

    iget-object v0, p0, Lbf/aU;->d:LaN/u;

    iget v1, p0, Lbf/aU;->w:I

    iget v2, p0, Lbf/aU;->x:I

    invoke-virtual {v0, v1, v2}, LaN/u;->d(II)V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aU;->D:Lcom/google/googlenav/ba;

    const/4 v0, 0x6

    if-ne p3, v0, :cond_3

    invoke-direct {p0}, Lbf/aU;->bn()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/aU;->al()V

    invoke-virtual {p0}, Lbf/aU;->l()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lbf/aU;->ae()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p2}, Lbf/aU;->h(I)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/aZ;->a(Z)V

    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->b(Lbf/aU;)V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected ap()V
    .locals 0

    return-void
.end method

.method protected aq()V
    .locals 1

    iget-object v0, p0, Lbf/aU;->F:Lbf/aV;

    invoke-virtual {v0}, Lbf/aV;->a()Lcom/google/googlenav/ui/view/android/aL;

    move-result-object v0

    iput-object v0, p0, Lbf/aU;->r:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public au()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 2

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public be()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    iget-object v1, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public bf()Lcom/google/googlenav/ai;
    .locals 2

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lbf/aU;->f()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->I()[Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->J()I

    move-result v0

    aget-object v0, v1, v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    goto :goto_0
.end method

.method public bg()Lax/b;
    .locals 1

    iget-object v0, p0, Lbf/aU;->u:Lax/b;

    return-object v0
.end method

.method public bh()Z
    .locals 1

    iget-boolean v0, p0, Lbf/aU;->y:Z

    return v0
.end method

.method public bi()I
    .locals 1

    iget v0, p0, Lbf/aU;->E:I

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lbf/aU;->v:I

    if-eqz v0, :cond_0

    iget v0, p0, Lbf/aU;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lbf/aU;->v:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lbf/aU;->v:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lbf/aU;->v:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lcom/google/googlenav/aZ;
    .locals 1

    iget-object v0, p0, Lbf/aU;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/aZ;

    return-object v0
.end method

.method protected f(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lbf/aU;->h()V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    const/16 v2, 0x2a

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lbf/aU;->k(Z)V

    invoke-virtual {p0}, Lbf/aU;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbf/aU;->z:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbf/aU;->y:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lbf/aU;->y:Z

    invoke-direct {p0}, Lbf/aU;->bj()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    iget-object v0, p0, Lbf/aU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->c(Lbf/aU;)V

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/h;

    invoke-direct {v0, p0}, Lbh/h;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected m()V
    .locals 0

    return-void
.end method

.method protected n()V
    .locals 0

    return-void
.end method
