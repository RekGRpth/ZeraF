.class public Lbf/bE;
.super Lbf/y;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lbf/y;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;)V

    return-void
.end method


# virtual methods
.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x23d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aL()Lam/f;
    .locals 2

    iget-object v0, p0, Lbf/bE;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ag:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aT()Z
    .locals 1

    invoke-virtual {p0}, Lbf/bE;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lbf/y;->aT()Z

    :cond_0
    invoke-virtual {p0}, Lbf/bE;->bK()V

    const/4 v0, 0x1

    return v0
.end method

.method public aU()V
    .locals 1

    invoke-virtual {p0}, Lbf/bE;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lbf/y;->aU()V

    :cond_0
    invoke-virtual {p0}, Lbf/bE;->bL()V

    return-void
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x7

    return v0
.end method

.method public b(LaN/B;)I
    .locals 1

    invoke-virtual {p0}, Lbf/bE;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbf/y;->b(LaN/B;)I

    move-result v0

    goto :goto_0
.end method

.method protected bG()V
    .locals 0

    return-void
.end method

.method protected bK()V
    .locals 2

    iget-object v0, p0, Lbf/bE;->i:Lcom/google/googlenav/ui/X;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/X;->a(Z)V

    return-void
.end method

.method protected bL()V
    .locals 2

    iget-object v0, p0, Lbf/bE;->i:Lcom/google/googlenav/ui/X;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/googlenav/ui/X;->a(Z)V

    return-void
.end method

.method protected bw()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
