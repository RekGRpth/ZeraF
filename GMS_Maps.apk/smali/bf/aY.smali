.class public Lbf/aY;
.super Lcom/google/googlenav/ui/view/dialog/r;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbf/aV;


# direct methods
.method public constructor <init>(Lbf/aV;Lbf/aU;)V
    .locals 0

    iput-object p1, p0, Lbf/aY;->a:Lbf/aV;

    invoke-direct {p0, p2}, Lcom/google/googlenav/ui/view/dialog/r;-><init>(Lcom/google/googlenav/ui/e;)V

    return-void
.end method


# virtual methods
.method protected c()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lbf/aY;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04007c

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f100026

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v2, p0, Lbf/aY;->a:Lbf/aV;

    invoke-virtual {p0}, Lbf/aY;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lbf/aV;->a(Landroid/content/Context;)Lcom/google/googlenav/ui/view/android/J;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v2, Lbf/aX;

    iget-object v3, p0, Lbf/aY;->a:Lbf/aV;

    invoke-direct {v2, v3, v4}, Lbf/aX;-><init>(Lbf/aV;Lbf/aW;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v1
.end method

.method public w_()Ljava/lang/String;
    .locals 3

    const/16 v0, 0xec

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbf/aY;->a:Lbf/aV;

    invoke-static {v1}, Lbf/aV;->a(Lbf/aV;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method
