.class public Lbf/aZ;
.super Lbf/h;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/googlenav/ui/bi;

.field private e:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lbf/i;)V
    .locals 1

    invoke-direct {p0, p1}, Lbf/h;-><init>(Lbf/i;)V

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    iput-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    return-void
.end method

.method private a(I)I
    .locals 1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x4

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x45

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x39

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_2
        0x258 -> :sswitch_3
        0x25c -> :sswitch_1
        0x262 -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic a(Lbf/aZ;I)I
    .locals 1

    invoke-direct {p0, p1}, Lbf/aZ;->a(I)I

    move-result v0

    return v0
.end method

.method static a(Ljava/lang/String;Lbf/i;)Lcom/google/googlenav/ui/aW;
    .locals 2

    new-instance v0, Lbf/bc;

    invoke-direct {v0, p1}, Lbf/bc;-><init>(Lbf/i;)V

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->e:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/CharSequence;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->d()I

    move-result v0

    sget-object v4, Lbf/bb;->a:[I

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v2, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lbf/i;->a(LaN/B;I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    :goto_1
    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v4, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v4}, Lbf/i;->ba()Lcom/google/googlenav/ui/s;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/ui/s;->ad()Lcom/google/googlenav/aA;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/aA;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    :goto_2
    if-nez v2, :cond_0

    move v0, v1

    goto :goto_1

    :cond_1
    move v2, v3

    goto :goto_2

    :pswitch_3
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bs()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    :goto_3
    if-nez v2, :cond_0

    move v0, v1

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/bZ;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/bZ;->l()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-char v1, Lcom/google/googlenav/ui/bi;->bm:C

    invoke-static {v1}, Lcom/google/googlenav/ui/bi;->a(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method static a(Ljava/lang/String;Lcom/google/googlenav/cm;)Ljava/lang/String;
    .locals 11

    const/16 v10, 0x309

    const/16 v9, 0x308

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/cm;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1, v7}, Lcom/google/googlenav/cm;->d(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_1
    if-eqz v3, :cond_3

    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, ""

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    invoke-static {v1, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v4, 0xa

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v5, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v1, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "..."

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v3, :cond_4

    invoke-static {v10}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_1
    const/16 v0, 0x4ed

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/cm;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v6}, Lcom/google/googlenav/cm;->c(Z)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    :cond_3
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    invoke-static {v9}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v6

    aput-object v2, v3, v7

    invoke-static {v1, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;
    .locals 2

    invoke-direct {p0, p1, p2}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(I)Lam/f;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    invoke-virtual {p2}, Lcom/google/googlenav/settings/e;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(I)Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method private d()Landroid/view/LayoutInflater;
    .locals 2

    iget-object v0, p0, Lbf/aZ;->e:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/aZ;->d:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->O()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lbf/aZ;->e:Landroid/view/LayoutInflater;

    :cond_0
    iget-object v0, p0, Lbf/aZ;->e:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/googlenav/ai;)Lbj/F;
    .locals 1

    invoke-virtual {p0}, Lbf/aZ;->c()Lbf/m;

    move-result-object v0

    invoke-virtual {v0}, Lbf/m;->bh()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/googlenav/ui/bB;->a(Lcom/google/googlenav/ai;Z)Lcom/google/googlenav/ui/bB;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/ai;Landroid/view/View;)V
    .locals 12

    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v10, -0x1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_5

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aw()Lcom/google/googlenav/settings/e;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lbf/aZ;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;

    move-result-object v2

    move-object v3, v2

    move-object v2, v0

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_0
    new-instance v7, Lbf/ba;

    invoke-direct {v7, p0}, Lbf/ba;-><init>(Lbf/aZ;)V

    if-eqz v3, :cond_2

    invoke-direct {p0, p1, v0}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const v0, 0x7f10001a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    if-eq v4, v10, :cond_1

    new-instance v8, Lcom/google/googlenav/ui/view/a;

    iget-object v9, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v9}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/googlenav/F;->c()I

    move-result v9

    invoke-direct {v8, v4, v9}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v7, v8}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    :cond_1
    if-eq v4, v10, :cond_6

    move v4, v5

    :goto_1
    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/android/BubbleButton;->setEnabled(Z)V

    :cond_2
    if-eqz v2, :cond_4

    invoke-direct {p0, p1, v1}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v0, 0x7f10004e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/BubbleButton;

    if-eq v1, v10, :cond_3

    new-instance v4, Lcom/google/googlenav/ui/view/a;

    iget-object v8, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v8}, Lbf/i;->ar()Lcom/google/googlenav/F;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/googlenav/F;->c()I

    move-result v8

    invoke-direct {v4, v1, v8}, Lcom/google/googlenav/ui/view/a;-><init>(II)V

    invoke-static {v0, v7, v4}, Lcom/google/googlenav/ui/android/aG;->a(Landroid/view/View;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/view/t;)Lcom/google/googlenav/ui/android/aG;

    :cond_3
    if-eq v1, v10, :cond_7

    :goto_2
    invoke-virtual {v0, v5}, Lcom/google/googlenav/ui/android/BubbleButton;->setEnabled(Z)V

    :cond_4
    invoke-virtual {p0, p2, v3, v2}, Lbf/aZ;->a(Landroid/view/View;Lam/f;Lam/f;)V

    return-void

    :cond_5
    sget-object v1, Lcom/google/googlenav/settings/e;->a:Lcom/google/googlenav/settings/e;

    sget-object v0, Lcom/google/googlenav/settings/e;->c:Lcom/google/googlenav/settings/e;

    invoke-direct {p0, p1, v1}, Lbf/aZ;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;

    move-result-object v3

    invoke-direct {p0, p1, v0}, Lbf/aZ;->b(Lcom/google/googlenav/ai;Lcom/google/googlenav/settings/e;)Lam/f;

    move-result-object v2

    move-object v11, v0

    move-object v0, v1

    move-object v1, v11

    goto :goto_0

    :cond_6
    move v4, v6

    goto :goto_1

    :cond_7
    move v5, v6

    goto :goto_2
.end method

.method a(Lcom/google/googlenav/ai;Ljava/util/List;)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/googlenav/bZ;->c(Z)Lcom/google/googlenav/cm;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/cm;->c(Z)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lbf/aZ;->a(Ljava/lang/String;Lcom/google/googlenav/cm;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b()Lcom/google/googlenav/ui/view/d;
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {v0}, Lbf/i;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->c()B

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0}, Lbf/aZ;->a()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0x577

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->ad:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    move-object v3, v1

    move-object v1, v2

    :goto_1
    invoke-virtual {p0, v4, v1}, Lbf/aZ;->a(Landroid/view/View;Lcom/google/googlenav/ui/aW;)V

    invoke-virtual {p0, v4, v2}, Lbf/aZ;->a(Landroid/view/View;Lam/f;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v4, v1}, Lbf/aZ;->a(Landroid/view/View;Z)V

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v3, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0, v0}, Lbf/aZ;->b(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v3

    invoke-static {v2, v3}, Lbf/aZ;->a(Ljava/lang/String;Lcom/google/googlenav/bZ;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/googlenav/ui/aV;->m:Lcom/google/googlenav/ui/aV;

    invoke-static {v2, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0, v4, v1}, Lbf/aZ;->a(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lbf/aZ;->d(Lcom/google/googlenav/ai;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lbf/aZ;->b(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lbf/aZ;->e(Lcom/google/googlenav/ai;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lbf/aZ;->c(Landroid/view/View;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lbf/aZ;->c(Lcom/google/googlenav/ai;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v4, v1}, Lbf/aZ;->a(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, v0, v4}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Landroid/view/View;)V

    sget-object v0, Lbf/aZ;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbf/aZ;->c:Lbf/i;

    invoke-virtual {p0, v0, v1}, Lbf/aZ;->a(Landroid/view/ViewGroup;Lcom/google/googlenav/ui/view/c;)Lcom/google/googlenav/ui/view/d;

    move-result-object v2

    goto :goto_0

    :cond_3
    const/16 v1, 0x34

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/aV;->ad:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v3}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    move-object v3, v2

    goto :goto_1

    :cond_4
    move-object v1, v2

    move-object v3, v2

    goto :goto_1
.end method

.method protected b(Lcom/google/googlenav/ai;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c(Lcom/google/googlenav/ai;)Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lbf/aZ;->a(Lcom/google/googlenav/ai;)Lbj/F;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lbf/aZ;->d()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-interface {v1}, Lbj/F;->b()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-interface {v1, v0}, Lbj/F;->a(Landroid/view/View;)Lbj/bB;

    move-result-object v2

    iget-object v3, p0, Lbf/aZ;->c:Lbf/i;

    invoke-interface {v1, v3, v2}, Lbj/F;->a(Lcom/google/googlenav/ui/e;Lbj/bB;)V

    :cond_0
    return-object v0
.end method

.method protected final c()Lbf/m;
    .locals 1

    iget-object v0, p0, Lbf/aZ;->c:Lbf/i;

    check-cast v0, Lbf/m;

    return-object v0
.end method

.method protected d(Lcom/google/googlenav/ai;)Ljava/util/List;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lbf/aZ;->c()Lbf/m;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lbf/aZ;->c:Lbf/i;

    invoke-static {v0, v3}, Lbf/aZ;->a(Ljava/lang/String;Lbf/i;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v2}, Lbf/m;->bg()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aj()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x206

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->by()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x5f4

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bx()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x1e3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v3}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->y()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Lbf/m;->bm()Z

    move-result v0

    invoke-static {p1, v0}, Lbf/aS;->c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->h:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    return-object v1
.end method

.method protected e(Lcom/google/googlenav/ai;)Ljava/util/List;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bz()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x3c1

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/googlenav/ui/aV;->br:Lcom/google/googlenav/ui/aV;

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0, p1, v0}, Lbf/aZ;->a(Lcom/google/googlenav/ai;Ljava/util/List;)V

    return-object v0
.end method
