.class public Lbf/C;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements Lba/e;
.implements Lcom/google/googlenav/friend/bf;


# instance fields
.field private B:Z

.field private C:Z

.field private D:[Ljava/lang/String;

.field private E:Z

.field private F:Lcom/google/googlenav/friend/be;

.field private G:Lba/d;

.field private H:Z

.field private I:Z

.field private J:[Ljava/lang/String;

.field private K:Z

.field private L:Ljava/lang/String;

.field private M:Z

.field private N:Z

.field private O:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final P:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;Z[Ljava/lang/String;ZLcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    invoke-direct/range {p0 .. p5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbf/C;->P:Ljava/lang/Object;

    iput-boolean p6, p0, Lbf/C;->I:Z

    iput-object p7, p0, Lbf/C;->J:[Ljava/lang/String;

    iput-boolean p8, p0, Lbf/C;->M:Z

    iput-object p9, p0, Lbf/C;->O:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-eqz p7, :cond_0

    array-length v0, p7

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbf/C;->K:Z

    new-instance v0, Lbf/B;

    invoke-direct {v0, p0}, Lbf/B;-><init>(Lbf/C;)V

    iput-object v0, p0, Lbf/C;->A:Lbf/be;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lbf/C;->bu()Lcom/google/googlenav/ai;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/googlenav/ai;

    move v1, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;B)Lcom/google/googlenav/ai;

    move-result-object v0

    aput-object v0, v4, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v3, v4}, Lcom/google/googlenav/ai;->a([Lcom/google/googlenav/ai;)V

    iget-boolean v0, p0, Lbf/C;->M:Z

    if-nez v0, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v1, v1, v2

    aput-object v1, v0, v2

    invoke-virtual {p0}, Lbf/C;->bm()Z

    move-result v1

    invoke-static {v3, v1}, Lbf/aS;->c(Lcom/google/googlenav/ai;Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iput-object v0, p0, Lbf/C;->J:[Ljava/lang/String;

    iput-boolean v5, p0, Lbf/C;->K:Z

    :cond_1
    return-void
.end method

.method private static b(Ljava/lang/String;)LaN/B;
    .locals 5

    const v4, 0x49742400

    const/4 v0, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "http://maps.google.com/?q="

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "http://maps.google.com/?q="

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    new-instance v1, LaN/B;

    mul-float/2addr v2, v4

    float-to-int v2, v2

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, LaN/B;-><init>(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private bO()V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0}, Lbf/C;->b(B)V

    iput-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    iput-boolean v1, p0, Lbf/C;->E:Z

    iput-object v2, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    iput-object v2, p0, Lbf/C;->G:Lba/d;

    iget-boolean v2, p0, Lbf/C;->K:Z

    if-nez v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lbf/C;->H:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private bP()Lo/D;
    .locals 2

    iget-object v0, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo/D;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bQ()V
    .locals 1

    invoke-virtual {p0}, Lbf/C;->bI()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->A()V

    iget-object v0, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbf/C;->bR()V

    :cond_0
    invoke-virtual {p0}, Lbf/C;->m()V

    return-void
.end method

.method private bR()V
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iput-object v1, p0, Lbf/C;->G:Lba/d;

    iput-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    iput-boolean v4, p0, Lbf/C;->B:Z

    iput-boolean v5, p0, Lbf/C;->C:Z

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p0, p0, v1}, Lbf/C;->a(Lba/e;LaN/B;)Lba/d;

    move-result-object v1

    iput-object v1, p0, Lbf/C;->G:Lba/d;

    iget-boolean v1, p0, Lbf/C;->H:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/C;->G:Lba/d;

    sget v2, Lcom/google/googlenav/ui/bi;->bx:I

    sget v3, Lcom/google/googlenav/ui/bi;->by:I

    invoke-virtual {v1, v2, v3}, Lba/d;->a(II)V

    :cond_0
    iput-boolean v5, p0, Lbf/C;->B:Z

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbf/C;->d(LaN/B;)Lcom/google/googlenav/friend/be;

    move-result-object v1

    iput-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    iget-object v1, p0, Lbf/C;->G:Lba/d;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v1, v4}, Lba/d;->a(Z)V

    iget-object v1, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v1}, Lba/d;->o()V

    :cond_1
    iget-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    :cond_2
    return-void
.end method

.method private bS()V
    .locals 2

    iget-object v1, p0, Lbf/C;->P:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lbf/C;->C:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbf/C;->B:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/C;->ag()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lbf/C;->an()Z

    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lbf/C;->af()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/C;->bp()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private bT()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    const v2, 0x49742400

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v1

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public Z()V
    .locals 1

    iget-boolean v0, p0, Lbf/C;->I:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lbf/m;->Z()V

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    :cond_0
    return-void
.end method

.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lba/e;LaN/B;)Lba/d;
    .locals 1

    new-instance v0, Lba/d;

    invoke-direct {v0, p1, p2}, Lba/d;-><init>(Lba/e;LaN/B;)V

    return-object v0
.end method

.method public a(J)V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/C;->B:Z

    iput-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    invoke-direct {p0}, Lbf/C;->bS()V

    return-void
.end method

.method public a(Lba/d;)V
    .locals 1

    invoke-virtual {p0}, Lbf/C;->ax()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lbf/C;->G:Lba/d;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/C;->C:Z

    invoke-virtual {p1}, Lba/d;->k()Z

    move-result v0

    iput-boolean v0, p0, Lbf/C;->E:Z

    invoke-direct {p0}, Lbf/C;->bS()V

    goto :goto_0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    iput-object p1, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-direct {p0}, Lbf/C;->bO()V

    invoke-virtual {p0}, Lbf/C;->bI()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lbf/C;->bR()V

    :cond_0
    invoke-virtual {p0}, Lbf/C;->ax()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/C;->y()V

    :cond_1
    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/util/List;Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lbf/C;->ax()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lbf/C;->F:Lcom/google/googlenav/friend/be;

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lbf/C;->B:Z

    if-eqz p1, :cond_5

    invoke-static {p1, v3}, Lcom/google/googlenav/ui/aT;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;B)Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->b()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(Lo/D;)V

    :cond_3
    iget-object v1, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->d()B

    move-result v1

    new-instance v2, Lcom/google/googlenav/bl;

    invoke-direct {v2, v0}, Lcom/google/googlenav/bl;-><init>(Lcom/google/googlenav/ai;)V

    iput-object v2, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v1}, Lbf/C;->b(B)V

    invoke-virtual {p0}, Lbf/C;->R()V

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->E()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->F()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-array v0, v5, [Ljava/lang/String;

    const/16 v1, 0x5e8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, ""

    aput-object v1, v0, v4

    iput-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    :goto_2
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-direct {p0, p2}, Lbf/C;->a(Ljava/util/List;)V

    invoke-direct {p0}, Lbf/C;->bS()V

    goto :goto_0

    :cond_4
    new-array v2, v5, [Ljava/lang/String;

    aput-object v1, v2, v3

    aput-object v0, v2, v4

    iput-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .locals 0

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 11

    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    sparse-switch p1, :sswitch_data_0

    move v0, v6

    :goto_0
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    if-eqz v0, :cond_0

    new-array v1, v7, [Ljava/lang/String;

    const-string v2, "a=s"

    aput-object v2, v1, v6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "i="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    const-string v3, "c"

    invoke-static {v2, v3, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-nez v0, :cond_1

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :cond_1
    return v0

    :sswitch_0
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, Lbf/C;->b(ILjava/lang/Object;)V

    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lax/y;->b(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v5, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v8

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    const/16 v2, 0x18

    iget-object v3, p0, Lbf/C;->f:Lcom/google/googlenav/F;

    invoke-direct {v1, v2, v3}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    iget-object v2, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v5, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    move v0, v8

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v1, v1, v6

    iget-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v2, v2, v8

    invoke-static {v1, v2}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lax/y;->a(LaN/B;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v0

    goto :goto_1

    :sswitch_1
    const/16 v0, 0x25d

    if-ne p1, v0, :cond_4

    move v0, v7

    :goto_2
    const/16 v1, 0x45

    const-string v2, "n"

    const-string v3, "c"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lbf/C;->n()V

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v2

    invoke-static {v1, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v1

    :goto_3
    iget-object v2, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    const-string v3, "c"

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/googlenav/ui/s;->a(Lax/y;I[Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    move v0, v8

    goto/16 :goto_0

    :cond_4
    const/16 v0, 0x25e

    if-ne p1, v0, :cond_9

    move v0, v8

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v2, v2, v6

    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v8

    invoke-static {v2, v3}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v3

    invoke-static {v1, v5, v2, v3}, Lax/y;->a(LaN/B;Ljava/lang/String;Ljava/lang/String;Lo/D;)Lax/y;

    move-result-object v1

    goto :goto_3

    :sswitch_2
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "Street View"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lbf/C;->n()V

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(LaN/B;)V

    invoke-static {}, Lba/c;->a()V

    move v0, v8

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lbf/C;->n()V

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "9"

    invoke-virtual {p0}, Lbf/C;->s()Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v2

    invoke-virtual {v2}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v5, v1, v2, v8}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v8

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v6

    iget-object v1, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v1, v1, v8

    invoke-static {v0, v1}, Lcom/google/googlenav/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_4
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    invoke-direct {p0}, Lbf/C;->bP()Lo/D;

    move-result-object v2

    invoke-virtual {p0}, Lbf/C;->bN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lbf/C;->bI()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(LaN/B;Lo/D;Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v8

    goto/16 :goto_0

    :sswitch_5
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v9

    new-instance v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v1

    iget-boolean v3, p0, Lbf/C;->M:Z

    if-eqz v3, :cond_7

    const/16 v3, 0x532

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->a(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v4, v6

    invoke-static {v2}, Lcom/google/googlenav/ui/bd;->b(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-static {v3, v4}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v6

    iget-object v4, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v4, v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/googlenav/ai;-><init>(LaN/g;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-virtual {v9, v0}, Lcom/google/googlenav/ui/wizard/jv;->b(Lcom/google/googlenav/ai;)V

    move v0, v8

    goto/16 :goto_0

    :cond_7
    move-object v2, v5

    goto :goto_5

    :sswitch_6
    new-instance v1, LaR/D;

    iget-object v2, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-direct {p0}, Lbf/C;->bT()Ljava/lang/String;

    move-result-object v3

    move-object v4, v5

    invoke-direct/range {v1 .. v6}, LaR/D;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    invoke-virtual {p0}, Lbf/C;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v1, v0}, LaR/D;->a(LaN/B;)V

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    iget-object v2, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v2

    const-string v3, "p"

    invoke-interface {v0, v1, v5, v2, v3}, LaR/n;->a(LaR/D;LaR/H;Lbf/am;Ljava/lang/String;)LaR/D;

    invoke-virtual {p0}, Lbf/C;->bp()V

    move v0, v8

    goto/16 :goto_0

    :sswitch_7
    move v0, v6

    goto/16 :goto_0

    :cond_8
    move-object v3, v5

    goto/16 :goto_4

    :cond_9
    move v0, v6

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_5
        0xf -> :sswitch_7
        0x258 -> :sswitch_2
        0x25a -> :sswitch_3
        0x25b -> :sswitch_0
        0x25c -> :sswitch_1
        0x25d -> :sswitch_1
        0x25e -> :sswitch_1
        0x578 -> :sswitch_6
        0x5dc -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 1

    iget-object v0, p0, Lbf/C;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/C;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lbf/C;->bQ()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_0
.end method

.method public aE()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected aq()V
    .locals 0

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->B:Z

    return v0
.end method

.method public b(LaN/g;)Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbf/m;->b(LaN/g;)Z

    move-result v0

    goto :goto_0
.end method

.method public bG()Lam/f;
    .locals 1

    iget-object v0, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v0}, Lba/d;->m()Lam/f;

    move-result-object v0

    return-object v0
.end method

.method public bH()Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->H:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbf/C;->C:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/C;->G:Lba/d;

    invoke-virtual {v0}, Lba/d;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bI()Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->M:Z

    return v0
.end method

.method public bJ()Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbf/C;->N:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bK()Z
    .locals 1

    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->u()Z

    move-result v0

    return v0
.end method

.method public bL()Z
    .locals 1

    iget-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bM()Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/C;->bL()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v1

    :cond_0
    iget-object v0, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    iget-object v2, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-interface {v0, v2}, LaR/u;->a(Ljava/lang/String;)LaR/t;

    move-result-object v0

    check-cast v0, LaR/D;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaR/D;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method bN()Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    invoke-static {v0}, Lbf/C;->b(Ljava/lang/String;)LaN/B;

    move-result-object v2

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-nez v2, :cond_1

    :cond_0
    iget-object v0, p0, Lbf/C;->L:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v4

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    const-string v4, "+"

    invoke-static {v3, v4, v0}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)I

    :goto_1
    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lbf/C;->D:[Ljava/lang/String;

    aget-object v3, v3, v5

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    const-string v4, "+"

    invoke-static {v3, v4, v1}, Lau/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/StringBuilder;)I

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "http://maps.google.com/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "maps?f=q&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_3
    if-eqz v1, :cond_5

    if-eqz v0, :cond_4

    const-string v0, "+"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_5
    const-string v0, "@"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sll="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    invoke-virtual {v1}, LaN/B;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&sspn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->a()I

    move-result v1

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->b()I

    move-result v1

    invoke-static {v1}, Lau/b;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public bp()V
    .locals 2

    iget-object v0, p0, Lbf/C;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/C;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/C;->bA()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lbf/C;->v:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lbf/C;->v:I

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/C;->f(Z)V

    iget-object v0, p0, Lbf/C;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/android/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/Y;->h()V

    goto :goto_0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lbf/C;->M:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->R()[Lam/f;

    move-result-object v1

    aget-object v0, v1, v0

    invoke-interface {v0}, Lam/f;->b()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x4

    :cond_0
    return v0
.end method

.method public c(LaN/g;)Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->M:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbf/m;->c(LaN/g;)Z

    move-result v0

    goto :goto_0
.end method

.method public c()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbf/C;->D:[Ljava/lang/String;

    return-object v0
.end method

.method protected d(LaN/B;)Lcom/google/googlenav/friend/be;
    .locals 2

    new-instance v0, Lcom/google/googlenav/friend/bg;

    invoke-direct {v0}, Lcom/google/googlenav/friend/bg;-><init>()V

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->b(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/friend/bf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->h(I)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    iget-object v1, p0, Lbf/C;->O:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/friend/bg;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/friend/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/friend/bg;->a()Lcom/google/googlenav/friend/be;

    move-result-object v0

    return-object v0
.end method

.method public d(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->C:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lbf/C;->E:Z

    goto :goto_0
.end method

.method public d()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lbf/C;->J:[Ljava/lang/String;

    return-object v0
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .locals 0

    return-void
.end method

.method protected f()Z
    .locals 1

    iget-boolean v0, p0, Lbf/C;->K:Z

    return v0
.end method

.method protected f(Lat/a;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lbf/C;->ag()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/C;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/C;->ah()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lbf/C;->ag()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lbf/C;->bQ()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/g;

    invoke-direct {v0, p0}, Lbh/g;-><init>(Lbf/i;)V

    return-object v0
.end method

.method public k(Z)V
    .locals 0

    iput-boolean p1, p0, Lbf/C;->N:Z

    return-void
.end method

.method protected l()V
    .locals 0

    return-void
.end method
