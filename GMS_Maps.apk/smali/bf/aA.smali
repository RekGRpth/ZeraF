.class public Lbf/aA;
.super Lbf/i;
.source "SourceFile"


# instance fields
.field private A:Lcom/google/googlenav/ui/android/M;

.field private u:Z

.field private v:[Lcom/google/googlenav/ui/aI;

.field private w:Ljava/util/List;

.field private x:I

.field private y:Lcom/google/googlenav/ui/view/d;

.field private z:Lcom/google/googlenav/ui/view/d;


# direct methods
.method public constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .locals 6

    new-instance v5, Lcom/google/googlenav/n;

    new-instance v0, Lbf/aE;

    invoke-direct {v0}, Lbf/aE;-><init>()V

    new-instance v1, Lbf/aE;

    invoke-direct {v1}, Lbf/aE;-><init>()V

    invoke-direct {v5, v0, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbf/i;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/aA;->u:Z

    invoke-direct {p0}, Lbf/aA;->be()V

    invoke-direct {p0}, Lbf/aA;->bf()V

    return-void
.end method

.method private a(LaN/B;LaN/B;)V
    .locals 5

    if-nez p1, :cond_0

    invoke-static {p2}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lbf/aA;->w:Ljava/util/List;

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    new-instance v3, Lay/b;

    new-instance v4, Lbf/aD;

    invoke-direct {v4, p0, v2, v1}, Lbf/aD;-><init>(Lbf/aA;ILjava/util/List;)V

    invoke-direct {v3, v0, v4}, Lay/b;-><init>(Ljava/util/List;Lay/c;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Law/h;->c(Law/g;)V

    return-void

    :cond_0
    invoke-static {p1, p2}, Lay/d;->a(LaN/B;LaN/B;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lbf/aA;)V
    .locals 0

    invoke-direct {p0}, Lbf/aA;->bf()V

    return-void
.end method

.method static synthetic b(Lbf/aA;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lbf/aA;->w:Ljava/util/List;

    return-object v0
.end method

.method private be()V
    .locals 1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lbf/aA;->w:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lbf/aA;->x:I

    return-void
.end method

.method private bf()V
    .locals 4

    const/4 v3, 0x0

    iget-boolean v0, p0, Lbf/aA;->u:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/aA;->bg()V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/16 v1, 0xa

    new-instance v2, Lbf/aB;

    invoke-direct {v2, p0}, Lbf/aB;-><init>(Lbf/aA;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/aA;->z:Lcom/google/googlenav/ui/view/d;

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lbf/aA;->bh()V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/16 v1, 0x9

    new-instance v2, Lbf/aC;

    invoke-direct {v2, p0}, Lbf/aC;-><init>(Lbf/aA;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/view/e;->a(ILcom/google/googlenav/ui/view/c;Ljava/lang/String;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    iput-object v0, p0, Lbf/aA;->y:Lcom/google/googlenav/ui/view/d;

    goto :goto_0
.end method

.method private bg()V
    .locals 1

    iget-object v0, p0, Lbf/aA;->y:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aA;->y:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aA;->y:Lcom/google/googlenav/ui/view/d;

    :cond_0
    return-void
.end method

.method private bh()V
    .locals 1

    iget-object v0, p0, Lbf/aA;->z:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aA;->z:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aA;->z:Lcom/google/googlenav/ui/view/d;

    :cond_0
    return-void
.end method

.method private bi()V
    .locals 1

    iget-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->c()V

    iget-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/M;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    :cond_0
    return-void
.end method

.method private bj()V
    .locals 2

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v0

    invoke-virtual {v0}, Lbf/aE;->f()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lbf/aA;->bl()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lbf/aA;->bk()V

    goto :goto_0
.end method

.method private bk()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lbf/aA;->f()I

    move-result v0

    if-gtz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0, v7}, Lcom/google/googlenav/ui/l;->b(II)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/googlenav/ui/android/M;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    const v3, 0x7f100062

    invoke-direct {v0, v1, v3}, Lcom/google/googlenav/ui/android/M;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V

    iput-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    :cond_1
    iget v0, p0, Lbf/aA;->x:I

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aE;->f()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lbf/aA;->w:Ljava/util/List;

    invoke-static {v0}, Lay/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lay/d;->e(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lay/d;->f(Ljava/util/List;)D

    move-result-wide v3

    double-to-int v1, v3

    invoke-static {v1, v7}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lay/d;->c(Ljava/util/List;)Lay/a;

    move-result-object v3

    invoke-virtual {v3}, Lay/a;->b()D

    move-result-wide v4

    double-to-int v0, v4

    invoke-static {v0, v7}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    iget-object v5, p0, Lbf/aA;->w:Ljava/util/List;

    invoke-virtual {v4, v5, v8}, Lcom/google/googlenav/ui/android/M;->a(Ljava/util/List;Z)V

    invoke-virtual {p0}, Lbf/aA;->c()Lbf/aE;

    move-result-object v4

    invoke-virtual {v4}, Lbf/aE;->a()V

    new-instance v5, Lbf/aF;

    invoke-virtual {v3}, Lay/a;->a()LaN/B;

    move-result-object v3

    const/16 v6, 0x16

    invoke-direct {v5, v3, v6, v8}, Lbf/aF;-><init>(LaN/B;BI)V

    invoke-virtual {v4, v5}, Lbf/aE;->a(Lbf/aF;)V

    invoke-virtual {p0}, Lbf/aA;->R()V

    :goto_1
    iget-object v3, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    const/16 v4, 0x2b1

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    aput-object v2, v5, v7

    aput-object v1, v5, v8

    const/4 v1, 0x2

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/ui/android/M;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    const-string v1, "--"

    const-string v0, "--"

    goto :goto_1
.end method

.method private bl()V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/googlenav/ui/android/M;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    const v2, 0x7f100062

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ui/android/M;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;I)V

    iput-object v0, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    :cond_0
    iget v0, p0, Lbf/aA;->x:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lbf/aA;->w:Ljava/util/List;

    invoke-static {v0}, Lay/d;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay/a;

    invoke-virtual {v0}, Lay/a;->b()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/l;->c(II)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lbf/aA;->A:Lcom/google/googlenav/ui/android/M;

    const/16 v2, 0x115

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/android/M;->a(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const-string v0, "--"

    goto :goto_0
.end method

.method private bm()V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v0

    invoke-virtual {v0}, Lbf/aE;->f()I

    move-result v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aA;->v:[Lcom/google/googlenav/ui/aI;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v0

    invoke-virtual {v0}, Lbf/aE;->f()I

    move-result v0

    new-array v2, v0, [LaN/B;

    move v0, v1

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_1

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v3

    invoke-virtual {v3, v0}, Lbf/aE;->d(I)Lbf/aF;

    move-result-object v3

    invoke-virtual {v3}, Lbf/aF;->a()LaN/B;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/googlenav/ui/aI;

    new-instance v3, LaN/M;

    const v4, -0xd5ba98

    const/4 v5, 0x5

    invoke-direct {v3, v2, v4, v5}, LaN/M;-><init>([LaN/B;II)V

    aput-object v3, v0, v1

    iput-object v0, p0, Lbf/aA;->v:[Lcom/google/googlenav/ui/aI;

    iget-object v0, p0, Lbf/aA;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->n(Lbf/i;)V

    goto :goto_0
.end method

.method static synthetic c(Lbf/aA;)I
    .locals 2

    iget v0, p0, Lbf/aA;->x:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lbf/aA;->x:I

    return v0
.end method

.method static synthetic d(Lbf/aA;)V
    .locals 0

    invoke-direct {p0}, Lbf/aA;->bj()V

    return-void
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .locals 1

    invoke-super {p0}, Lbf/i;->X()Z

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    iput-object p1, p0, Lbf/aA;->f:Lcom/google/googlenav/F;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/aA;->b(I)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aA;->v:[Lcom/google/googlenav/ui/aI;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lbf/aA;->u:Z

    return v0
.end method

.method protected a(Lat/b;Z)Z
    .locals 3

    iget-boolean v0, p0, Lbf/aA;->u:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aA;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lat/b;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aA;->c:LaN/p;

    invoke-virtual {p1}, Lat/b;->k()I

    move-result v1

    invoke-virtual {p1}, Lat/b;->l()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LaN/p;->b(II)LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/aA;->d(LaN/B;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aC()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aU()V
    .locals 0

    invoke-super {p0}, Lbf/i;->aU()V

    invoke-direct {p0}, Lbf/aA;->bh()V

    invoke-direct {p0}, Lbf/aA;->bg()V

    invoke-direct {p0}, Lbf/aA;->bi()V

    return-void
.end method

.method protected ap()V
    .locals 0

    return-void
.end method

.method protected aq()V
    .locals 0

    return-void
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x13

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lbf/aE;
    .locals 1

    invoke-virtual {p0}, Lbf/aA;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/n;

    invoke-virtual {v0}, Lcom/google/googlenav/n;->b()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lbf/aE;

    return-object v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Lbf/aE;
    .locals 1

    invoke-virtual {p0}, Lbf/aA;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/n;

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lbf/aE;

    return-object v0
.end method

.method protected d()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/aA;->u:Z

    const/16 v0, 0x47

    const-string v1, "u-start"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LaE/g;->a:LaE/g;

    invoke-virtual {v3}, LaE/g;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public d(LaN/B;)Z
    .locals 5

    const/4 v0, 0x1

    iget-boolean v1, p0, Lbf/aA;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/aA;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lbf/i;->aj()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    new-instance v2, Lbf/aF;

    invoke-direct {v2, p1}, Lbf/aF;-><init>(LaN/B;)V

    invoke-virtual {v1, v2}, Lbf/aE;->a(Lbf/aF;)V

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aE;->f()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/16 v1, 0x47

    const-string v2, "u-add"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LaE/g;->a:LaE/g;

    invoke-virtual {v4}, LaE/g;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0}, Lbf/aA;->bm()V

    invoke-virtual {p0}, Lbf/aA;->R()V

    invoke-direct {p0}, Lbf/aA;->bj()V

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aE;->f()I

    move-result v1

    if-ne v1, v0, :cond_3

    const/4 v1, 0x0

    invoke-direct {p0, v1, p1}, Lbf/aA;->a(LaN/B;LaN/B;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v2

    invoke-virtual {v2}, Lbf/aE;->f()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v2}, Lbf/aE;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lbf/aA;->a(LaN/B;LaN/B;)V

    goto :goto_0
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 1

    iget-object v0, p0, Lbf/aA;->v:[Lcom/google/googlenav/ui/aI;

    return-object v0
.end method

.method public f()I
    .locals 4

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aE;->f()I

    move-result v1

    if-le v1, v0, :cond_0

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v1

    invoke-virtual {v1, v2}, Lbf/aE;->d(I)Lbf/aF;

    move-result-object v1

    invoke-virtual {v1}, Lbf/aF;->a()LaN/B;

    move-result-object v1

    move v3, v2

    :goto_0
    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v2

    invoke-virtual {v2}, Lbf/aE;->f()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v2

    invoke-virtual {v2, v0}, Lbf/aE;->d(I)Lbf/aF;

    move-result-object v2

    invoke-virtual {v2}, Lbf/aF;->a()LaN/B;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/l;->a(LaN/B;LaN/B;)I

    move-result v1

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    move v3, v1

    move-object v1, v2

    goto :goto_0

    :cond_0
    move v3, v2

    :cond_1
    return v3
.end method

.method protected f(Lat/a;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/f;

    invoke-direct {v0, p0}, Lbh/f;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected k(Z)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/aA;->u:Z

    if-eqz p1, :cond_0

    const/16 v0, 0x47

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u-stop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v2

    invoke-virtual {v2}, Lbf/aE;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, LaE/g;->a:LaE/g;

    invoke-virtual {v3}, LaE/g;->d()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lbf/aA;->b()Lbf/aE;

    move-result-object v0

    invoke-virtual {v0}, Lbf/aE;->a()V

    invoke-virtual {p0}, Lbf/aA;->c()Lbf/aE;

    move-result-object v0

    invoke-virtual {v0}, Lbf/aE;->a()V

    invoke-direct {p0}, Lbf/aA;->be()V

    invoke-virtual {p0}, Lbf/aA;->R()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/aA;->v:[Lcom/google/googlenav/ui/aI;

    iget-object v0, p0, Lbf/aA;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->n(Lbf/i;)V

    invoke-direct {p0}, Lbf/aA;->bi()V

    invoke-direct {p0}, Lbf/aA;->bf()V

    return-void
.end method
