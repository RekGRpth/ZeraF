.class Lbf/bQ;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbf/bS;


# instance fields
.field final synthetic a:Lbf/bK;


# direct methods
.method constructor <init>(Lbf/bK;)V
    .locals 0

    iput-object p1, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->g()V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->j()V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->b(Lbf/bK;)V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->c(Lbf/bK;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->k()V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->d(Lbf/bK;)Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->e()V

    return-void
.end method

.method public b()V
    .locals 4

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->e(Lbf/bK;)Lcom/google/googlenav/ui/view/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->d()V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->e(Lbf/bK;)Lcom/google/googlenav/ui/view/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/r;->f()V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->f(Lbf/bK;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->f(Lbf/bK;)Lcom/google/googlenav/ui/view/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_0
    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->g(Lbf/bK;)V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->h()V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    iget-object v0, v0, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->h()V

    :goto_0
    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->j()V

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v2}, Lbf/bK;->e(Lbf/bK;)Lcom/google/googlenav/ui/view/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/r;->j()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/view/e;->a(II)V

    return-void

    :cond_1
    const/4 v0, 0x1

    new-instance v1, Lbf/bR;

    iget-object v2, p0, Lbf/bQ;->a:Lbf/bK;

    iget-object v2, v2, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v2

    iget-object v3, p0, Lbf/bQ;->a:Lbf/bK;

    iget-object v3, v3, Lbf/bK;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v3}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3, v0}, Lbf/bR;-><init>(Lbf/bQ;Las/c;Lcom/google/googlenav/android/aa;Z)V

    invoke-virtual {v1}, Lbf/bR;->g()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->d(Lbf/bK;)Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->f()V

    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v0}, Lbf/bK;->d(Lbf/bK;)Lcom/google/googlenav/ui/view/android/bF;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/bF;->e()V

    iget-object v0, p0, Lbf/bQ;->a:Lbf/bK;

    iget-object v1, p0, Lbf/bQ;->a:Lbf/bK;

    invoke-static {v1}, Lbf/bK;->h(Lbf/bK;)Landroid/content/res/Configuration;

    move-result-object v1

    invoke-static {v0, v1}, Lbf/bK;->a(Lbf/bK;Landroid/content/res/Configuration;)V

    return-void
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
