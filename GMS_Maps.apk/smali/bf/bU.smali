.class public Lbf/bU;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaN/m;


# static fields
.field public static final C:Lcom/google/googlenav/ab;

.field private static final O:Lcom/google/googlenav/ab;

.field private static P:Lcom/google/googlenav/ab;


# instance fields
.field B:Lcom/google/googlenav/ui/android/ai;

.field private D:Lax/y;

.field private E:Lcom/google/googlenav/ui/view/u;

.field private F:LaN/k;

.field private G:Z

.field private H:Landroid/widget/ArrayAdapter;

.field private final I:Landroid/widget/AdapterView$OnItemClickListener;

.field private final J:Landroid/os/Handler;

.field private K:Lcom/google/googlenav/layer/s;

.field private final L:Landroid/os/HandlerThread;

.field private M:Landroid/os/Handler;

.field private final N:Ljava/lang/Runnable;

.field private final Q:Ljava/util/ArrayList;

.field private R:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/googlenav/ab;

    const/4 v1, -0x2

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    sput-object v0, Lbf/bU;->O:Lcom/google/googlenav/ab;

    new-instance v0, Lcom/google/googlenav/ab;

    const/4 v1, -0x1

    const/16 v2, 0x5d7

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    sput-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    sget-object v0, Lbf/bU;->O:Lcom/google/googlenav/ab;

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;LaN/k;Lcom/google/googlenav/F;Z)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    new-instance v0, Lbf/bV;

    invoke-direct {v0, p0}, Lbf/bV;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbf/bU;->J:Landroid/os/Handler;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "VehicleRequestThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbf/bU;->L:Landroid/os/HandlerThread;

    new-instance v0, Lbf/bW;

    invoke-direct {v0, p0}, Lbf/bW;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->N:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    iput-object p5, p0, Lbf/bU;->F:LaN/k;

    iput-boolean p7, p0, Lbf/bU;->G:Z

    return-void
.end method

.method private a(Landroid/content/Context;I)Landroid/view/View;
    .locals 2

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbf/bU;)Lax/y;
    .locals 1

    invoke-direct {p0}, Lbf/bU;->bX()Lax/y;

    move-result-object v0

    return-object v0
.end method

.method private a(IILjava/lang/Object;Lcom/google/googlenav/cm;)V
    .locals 1

    if-eqz p4, :cond_0

    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/googlenav/bZ;->a(Lcom/google/googlenav/cm;)V

    :cond_0
    invoke-direct {p0}, Lbf/bU;->bV()V

    invoke-virtual {p0, p2, p3}, Lbf/bU;->a(ILjava/lang/Object;)V

    return-void
.end method

.method private a(ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->f()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lbf/bU;->b(I)V

    check-cast p2, Lcom/google/googlenav/cm;

    invoke-direct {p0, p1, p3, p4, p2}, Lbf/bU;->a(IILjava/lang/Object;Lcom/google/googlenav/cm;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    const v0, 0x7f10045c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ai;

    iput-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    iget-object v1, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0, v1}, Lbf/bU;->a(Lcom/google/googlenav/ui/android/ai;Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private a(Lax/y;II)V
    .locals 6

    new-instance v0, Lcom/google/googlenav/aa;

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v5

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/aa;-><init>(Lcom/google/googlenav/Y;Lax/y;IILcom/google/googlenav/ui/bh;)V

    invoke-direct {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/aa;)V

    return-void
.end method

.method private a(Lbf/bG;Z)V
    .locals 6

    invoke-direct {p0}, Lbf/bU;->bW()V

    new-instance v0, Lbf/cf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbf/cf;-><init>(Lbf/bU;Lbf/bV;)V

    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1, p1, v0, p2}, Lcom/google/googlenav/ui/s;->a(Lbf/bG;Lcom/google/googlenav/bU;Z)Lcom/google/googlenav/bW;

    move-result-object v2

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x4c8

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    return-void
.end method

.method static synthetic a(Lbf/bU;Lcom/google/googlenav/aa;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/bU;->b(Lcom/google/googlenav/aa;)V

    return-void
.end method

.method static synthetic a(Lbf/bU;Lcom/google/googlenav/ab;)V
    .locals 0

    invoke-direct {p0, p1}, Lbf/bU;->b(Lcom/google/googlenav/ab;)V

    return-void
.end method

.method public static a(Lcom/google/googlenav/common/io/j;)V
    .locals 3

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lbf/bU;->O:Lcom/google/googlenav/ab;

    sget-object v1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    sget-object v2, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    sget-object v2, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v2}, Lcom/google/googlenav/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    const-string v1, "TRANSIT_VEHICLE_TYPE"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {p0, v1, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lbf/bU;)Lax/y;
    .locals 1

    iget-object v0, p0, Lbf/bU;->D:Lax/y;

    return-object v0
.end method

.method private b(Lcom/google/googlenav/aa;)V
    .locals 4

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    sget-object v1, Lbf/bU;->C:Lcom/google/googlenav/ab;

    sget-object v2, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget-object v3, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v3}, Lcom/google/googlenav/ab;->a()I

    move-result v3

    aput v3, v1, v2

    invoke-virtual {p1, v1}, Lcom/google/googlenav/aa;->a([I)V

    :cond_0
    invoke-virtual {v0, p1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private b(Lcom/google/googlenav/ab;)V
    .locals 2

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbg/q;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lbg/q;->a(Lcom/google/googlenav/ab;)V

    :cond_0
    return-void
.end method

.method public static bL()Lcom/google/googlenav/ab;
    .locals 2

    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    sget-object v1, Lbf/bU;->O:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lbf/bU;->cc()Lcom/google/googlenav/ab;

    move-result-object v0

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    :cond_0
    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    return-object v0
.end method

.method public static bM()V
    .locals 1

    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    return-void
.end method

.method private bP()V
    .locals 1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bU;->b(I)V

    invoke-virtual {p0}, Lbf/bU;->an()Z

    goto :goto_0
.end method

.method private bQ()V
    .locals 12

    const v10, 0x3f99999a

    const/4 v4, 0x0

    const/4 v0, 0x3

    iget-object v1, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-gez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v6

    invoke-virtual {v6}, LaN/B;->c()I

    move-result v7

    invoke-virtual {v6}, LaN/B;->e()I

    move-result v8

    move v3, v4

    move v2, v4

    move v1, v4

    :goto_1
    if-ge v3, v5, :cond_4

    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, v3}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v9

    if-nez v9, :cond_2

    move v0, v2

    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v9}, LaN/B;->c()I

    move-result v0

    sub-int v0, v7, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v1, :cond_7

    :goto_3
    invoke-virtual {v9}, LaN/B;->e()I

    move-result v1

    sub-int v1, v8, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v9, 0xaba9500

    if-le v1, v9, :cond_3

    const v9, 0x15752a00

    sub-int v1, v9, v1

    :cond_3
    if-le v1, v2, :cond_6

    move v11, v1

    move v1, v0

    move v0, v11

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    if-eqz v2, :cond_0

    :cond_5
    mul-int/lit8 v0, v1, 0x2

    int-to-float v0, v0

    mul-float/2addr v0, v10

    float-to-int v0, v0

    mul-int/lit8 v1, v2, 0x2

    int-to-float v1, v1

    mul-float/2addr v1, v10

    float-to-int v1, v1

    iget-object v2, p0, Lbf/bU;->d:LaN/u;

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v3

    invoke-virtual {v2, v0, v1, v4, v3}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v0

    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/Y;->b(LaN/Y;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1, v6, v0}, LaN/u;->d(LaN/B;LaN/Y;)V

    goto :goto_0

    :cond_6
    move v1, v0

    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method private bR()V
    .locals 5

    const/4 v3, 0x0

    invoke-direct {p0}, Lbf/bU;->bS()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/actionbar/a;->n()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0401be

    invoke-direct {p0, v1, v2}, Lbf/bU;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lbf/bU;->a(Landroid/view/View;)V

    invoke-direct {p0}, Lbf/bU;->bT()Landroid/app/ActionBar$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/actionbar/a;->b(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->o()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0401bf

    invoke-direct {p0, v0, v2}, Lbf/bU;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lbf/bU;->a(Landroid/view/View;)V

    const v0, 0x7f100009

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0, v3}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->c()Lcom/google/googlenav/actionbar/b;

    move-result-object v4

    invoke-virtual {v1, v0, v3, v4}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    invoke-direct {p0}, Lbf/bU;->bT()Landroid/app/ActionBar$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->g()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f100205

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ui/android/ai;

    iput-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    iget-object v1, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v0, v1}, Lbf/bU;->a(Lcom/google/googlenav/ui/android/ai;Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private bS()Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v0

    const v2, 0x7f10045c

    invoke-virtual {v0, v2}, Lcom/google/googlenav/actionbar/a;->a(I)Landroid/view/View;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    :cond_2
    return v1

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->aq()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->g()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const v0, 0x7f100205

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private bT()Landroid/app/ActionBar$LayoutParams;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    const/16 v1, 0x13

    invoke-direct {v0, v2, v2, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    return-object v0
.end method

.method private bU()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->j()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->o()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/high16 v2, 0x7f040000

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/SearchView;->setIconifiedByDefault(Z)V

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->c()Lcom/google/googlenav/actionbar/b;

    move-result-object v2

    invoke-virtual {v1, v0, v3, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/view/MenuItem;Lcom/google/googlenav/actionbar/b;)V

    invoke-direct {p0}, Lbf/bU;->bT()Landroid/app/ActionBar$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/actionbar/a;->a(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private bV()V
    .locals 2

    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->R()V

    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    :cond_0
    return-void
.end method

.method private bW()V
    .locals 3

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, Lbf/bU;->af()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x1f

    :goto_0
    iget-object v2, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v0, v2}, Lbf/bU;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    return-void

    :cond_0
    const/16 v0, 0x20

    goto :goto_0
.end method

.method private bX()Lax/y;
    .locals 2

    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    return-object v0
.end method

.method private bY()V
    .locals 3

    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v0

    const/16 v1, 0x4e20

    const/16 v2, 0xa

    invoke-direct {p0, v0, v1, v2}, Lbf/bU;->a(Lax/y;II)V

    return-void
.end method

.method private bZ()V
    .locals 6

    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/bU;->c:LaN/p;

    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    :cond_1
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, p0}, LaN/k;->a(LaN/m;)V

    iget-object v1, p0, Lbf/bU;->c:LaN/p;

    iget-object v2, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, v2}, LaN/p;->a(LaN/k;)V

    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {p0}, Lbf/bU;->aP()V

    :goto_1
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, Lbf/bU;->F:LaN/k;

    iget-object v4, p0, Lbf/bU;->c:LaN/p;

    iget-object v5, p0, Lbf/bU;->d:LaN/u;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lbf/bU;->cd()V

    goto :goto_1
.end method

.method static synthetic c(Lbf/bU;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(Lcom/google/googlenav/cp;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->n()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;I)V

    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bU;->h(I)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {p0, v1, v0}, Lbf/bU;->a(ILcom/google/googlenav/ai;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbf/bU;->f(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(LaN/B;)V

    goto :goto_0
.end method

.method private ca()V
    .locals 2

    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    iget-object v1, p0, Lbf/bU;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    iget-object v1, p0, Lbf/bU;->N:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private cb()V
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v2

    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    sget-object v3, Lbf/bU;->C:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->clear()V

    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private static cc()Lcom/google/googlenav/ab;
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "TRANSIT_VEHICLE_TYPE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_2

    :cond_0
    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    :try_start_0
    new-instance v1, Ljava/io/DataInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    const-string v3, "TRANSIT_VEHICLE_TYPE"

    invoke-interface {v0, v3}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v0, Lcom/google/googlenav/ab;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readInt()I

    move-result v2

    invoke-virtual {v1}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v1, Lbf/bU;->O:Lcom/google/googlenav/ab;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    goto :goto_1
.end method

.method private cd()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-static {p0}, Lbf/am;->m(Lbf/i;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    return-void
.end method

.method static synthetic d(Lbf/bU;)V
    .locals 0

    invoke-direct {p0}, Lbf/bU;->cb()V

    return-void
.end method

.method static synthetic e(Lbf/bU;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lbf/bU;->J:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lbf/bU;)V
    .locals 0

    invoke-direct {p0}, Lbf/bU;->ca()V

    return-void
.end method

.method private j(I)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbf/bU;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x22

    :goto_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/bU;->b(ILjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    iget-object v1, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v1}, LaN/u;->c()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->d()LaN/Y;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bU;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/u;->b(LaN/B;)V

    return-void

    :cond_0
    const/16 v0, 0x21

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lbf/bU;->an()Z

    goto :goto_1
.end method

.method private l(Lcom/google/googlenav/ai;)V
    .locals 2

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    const-string v1, "LayerTransit"

    invoke-virtual {v0, v1}, Lbf/am;->e(Ljava/lang/String;)Lbf/i;

    move-result-object v0

    check-cast v0, Lbg/q;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lbg/q;->b(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    check-cast p1, Lcom/google/googlenav/W;

    invoke-virtual {p1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .locals 2

    const/4 v0, 0x0

    invoke-super {p0}, Lbf/m;->X()Z

    iget-boolean v1, p0, Lbf/bU;->R:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->n()V

    iput-boolean v0, p0, Lbf/bU;->R:Z

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/bU;->Z()V

    :cond_0
    invoke-virtual {p0}, Lbf/bU;->R()V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public Z()V
    .locals 1

    invoke-super {p0}, Lbf/m;->Z()V

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->d(Lbf/i;)V

    return-void
.end method

.method public a()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    iget-object v0, p0, Lbf/bU;->I:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lbf/m;->a(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/aX;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->h()V

    :cond_0
    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ai;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    invoke-interface {v0}, Lcom/google/googlenav/ui/android/ai;->a()V

    :cond_1
    return-void
.end method

.method public a(Lax/y;)V
    .locals 0

    iput-object p1, p0, Lbf/bU;->D:Lax/y;

    return-void
.end method

.method public a(Lax/y;Lax/y;Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p0, p2}, Lbf/bU;->a(Lax/y;)V

    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/google/googlenav/ui/bv;->c(Lax/y;)Ljava/lang/String;

    move-result-object p3

    :cond_0
    new-instance v2, Lcom/google/googlenav/aa;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->Q()Lcom/google/googlenav/ui/bh;

    move-result-object v0

    invoke-direct {v2, p1, p3, v0}, Lcom/google/googlenav/aa;-><init>(Lax/y;Ljava/lang/String;Lcom/google/googlenav/ui/bh;)V

    invoke-virtual {v2, p2}, Lcom/google/googlenav/aa;->a(Lax/y;)V

    invoke-direct {p0, v2}, Lbf/bU;->b(Lcom/google/googlenav/aa;)V

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x4c9

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/googlenav/ui/wizard/z;->a:Lcom/google/googlenav/ui/wizard/A;

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Law/d;Lcom/google/googlenav/ui/wizard/A;J)V

    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    iput-object p1, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    check-cast p1, Lcom/google/googlenav/Y;

    invoke-virtual {p1, v0}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    invoke-virtual {p1}, Lcom/google/googlenav/Y;->g()Lax/y;

    move-result-object v0

    iput-object v0, p0, Lbf/bU;->D:Lax/y;

    invoke-direct {p0}, Lbf/bU;->cb()V

    return-void
.end method

.method public a(Lcom/google/googlenav/aa;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/googlenav/aa;->l()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/aa;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->d()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(B)V

    invoke-virtual {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/F;)V

    invoke-virtual {p0}, Lbf/bU;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->l()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v2

    invoke-virtual {v2}, Lax/y;->f()LaN/B;

    move-result-object v2

    invoke-virtual {v1, v2}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/F;)V

    invoke-direct {p0}, Lbf/bU;->bZ()V

    invoke-direct {p0}, Lbf/bU;->bQ()V

    invoke-direct {p0}, Lbf/bU;->bP()V

    iget-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    sget-object v1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_3

    iget-object v1, p0, Lbf/bU;->B:Lcom/google/googlenav/ui/android/ai;

    invoke-interface {v1, v0}, Lcom/google/googlenav/ui/android/ai;->setSelection(I)V

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->g()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_4

    const v1, 0x7f100203

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    :cond_4
    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/aX;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/bU;->b(B)V

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->l()V

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/googlenav/ab;)V
    .locals 6

    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sput-object p1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {p0}, Lbf/bU;->bI()V

    const/16 v0, 0x73

    const-string v1, "f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/googlenav/ab;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/cp;)V
    .locals 6

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/googlenav/layer/p;

    const-string v1, "LayerTransit"

    invoke-virtual {v2}, Lcom/google/googlenav/bZ;->h()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    new-instance v5, Lbf/bY;

    invoke-direct {v5, p0, p1}, Lbf/bY;-><init>(Lbf/bU;Lcom/google/googlenav/cp;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/layer/p;-><init>(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/googlenav/layer/q;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/ui/android/ai;Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 4

    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x1090009

    :goto_0
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/google/googlenav/ui/bi;->e()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    :cond_0
    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lbf/bU;->Q:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const v0, 0x7f0401cf

    goto :goto_0

    :cond_2
    new-instance v0, Lbf/cc;

    invoke-direct {v0, p0}, Lbf/cc;-><init>(Lbf/bU;)V

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ai;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ai;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lbf/bU;->H:Landroid/widget/ArrayAdapter;

    sget-object v1, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/android/ai;->setSelection(I)V

    :cond_3
    invoke-interface {p1, p2}, Lcom/google/googlenav/ui/android/ai;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 3

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->x()Lbf/bU;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->o()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ab;

    invoke-virtual {v0}, Lcom/google/googlenav/ab;->a()I

    move-result v2

    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    invoke-virtual {v0}, Lcom/google/googlenav/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeUTF(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lbf/bU;->cd()V

    :cond_1
    return-void
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 2

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v1

    :goto_0
    return v1

    :sswitch_0
    invoke-virtual {p0}, Lbf/bU;->f()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lbf/bU;->D:Lax/y;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bU;->D:Lax/y;

    invoke-virtual {v0}, Lax/y;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/Y;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v4, v2}, Lbf/bU;->a(Lax/y;Lax/y;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-direct {v2}, Lcom/google/googlenav/bg;-><init>()V

    iget-object v3, p0, Lbf/bU;->D:Lax/y;

    invoke-virtual {v3}, Lax/y;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/googlenav/bg;->i(Z)Lcom/google/googlenav/bg;

    move-result-object v2

    new-instance v3, Lbf/ca;

    invoke-direct {v3, p0}, Lbf/ca;-><init>(Lbf/bU;)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/ba;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lbf/bU;->D:Lax/y;

    if-nez v2, :cond_3

    move v0, v1

    :cond_3
    check-cast p3, Lbf/bG;

    invoke-direct {p0, p3, v0}, Lbf/bU;->a(Lbf/bG;Z)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lbf/bU;->bY()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lbf/bU;->bW()V

    const-string v0, "d"

    invoke-virtual {p0, v0}, Lbf/bU;->c(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lbf/bU;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_5

    const/16 v0, 0x22

    :cond_4
    :goto_1
    invoke-direct {p0, p2, p3, v0, v4}, Lbf/bU;->a(ILjava/lang/Object;ILjava/lang/Object;)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->d()B

    move-result v2

    if-ne v2, v1, :cond_4

    const/16 v0, 0x23

    goto :goto_1

    :sswitch_6
    if-ltz p2, :cond_6

    invoke-virtual {p0, p2}, Lbf/bU;->b(I)V

    :cond_6
    const-string v0, "m"

    invoke-virtual {p0, v0}, Lbf/bU;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-direct {p0, v0}, Lbf/bU;->j(I)V

    goto/16 :goto_0

    :sswitch_7
    invoke-static {}, Lcom/google/googlenav/K;->o()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "Street View"

    invoke-static {v0}, Laj/a;->b(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "st"

    invoke-virtual {p0, v2}, Lbf/bU;->c(Ljava/lang/String;)V

    check-cast v0, Lcom/google/googlenav/cp;

    invoke-direct {p0, v0}, Lbf/bU;->c(Lcom/google/googlenav/cp;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_6
        0xd5 -> :sswitch_0
        0x258 -> :sswitch_7
        0x25b -> :sswitch_4
        0x262 -> :sswitch_4
        0x264 -> :sswitch_1
        0xfa1 -> :sswitch_5
        0xfa3 -> :sswitch_3
        0xfa4 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 6

    const/4 v0, 0x1

    iget-object v1, p0, Lbf/bU;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bU;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Lcom/google/googlenav/ui/wizard/z;->a(I)V

    const/16 v1, 0x73

    const-string v2, "m"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "a=b"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "i="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/Y;->c()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v3}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x23

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/bU;->a(ILjava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 6

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/layer/m;)V

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->k()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-interface {p1}, Ljava/io/DataInput;->readInt()I

    move-result v3

    invoke-interface {p1}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/googlenav/ab;

    invoke-direct {v5, v3, v4}, Lcom/google/googlenav/ab;-><init>(ILjava/lang/String;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aG()I
    .locals 1

    const v0, 0x7f02021f

    return v0
.end method

.method public aH()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0x5d6

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0x2fd

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x23f

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected aT()Z
    .locals 6

    invoke-static {}, Lbf/bU;->bL()Lcom/google/googlenav/ab;

    move-result-object v0

    sput-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    sget-object v0, Lbf/bU;->P:Lcom/google/googlenav/ab;

    invoke-direct {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/ab;)V

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, p0}, LaN/k;->a(LaN/m;)V

    iget-object v1, p0, Lbf/bU;->c:LaN/p;

    iget-object v2, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v1, v2}, LaN/p;->a(LaN/k;)V

    new-instance v1, Lcom/google/googlenav/layer/s;

    invoke-direct {v1, p0, v0}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    :cond_0
    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, Lbf/bU;->F:LaN/k;

    iget-object v4, p0, Lbf/bU;->c:LaN/p;

    iget-object v5, p0, Lbf/bU;->d:LaN/u;

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-virtual {v1, v2}, Lcom/google/googlenav/Y;->a(Lcom/google/googlenav/T;)V

    iget-boolean v0, p0, Lbf/bU;->G:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbf/bU;->bQ()V

    :cond_1
    iget-object v0, p0, Lbf/bU;->L:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lbf/bU;->L:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    invoke-super {p0}, Lbf/m;->aT()Z

    move-result v0

    return v0
.end method

.method public aU()V
    .locals 2

    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bU;->M:Landroid/os/Handler;

    :cond_1
    invoke-direct {p0}, Lbf/bU;->bU()V

    sget-object v0, Lbf/bU;->C:Lcom/google/googlenav/ab;

    invoke-direct {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/ab;)V

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    iget-object v0, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbf/bU;->c:LaN/p;

    iget-object v1, p0, Lbf/bU;->F:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    :cond_2
    return-void
.end method

.method public aW()V
    .locals 1

    invoke-super {p0}, Lbf/m;->aW()V

    invoke-direct {p0}, Lbf/bU;->bR()V

    iget-boolean v0, p0, Lbf/bU;->G:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbf/bU;->bP()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/bU;->G:Z

    :cond_0
    return-void
.end method

.method protected am()V
    .locals 2

    invoke-virtual {p0}, Lbf/bU;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-nez v1, :cond_2

    check-cast v0, Lcom/google/googlenav/cp;

    invoke-virtual {v0}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bZ;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lbf/bU;->b(Lcom/google/googlenav/cp;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/cp;->j()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lbf/bU;->a(Lcom/google/googlenav/cp;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Law/g;

    goto :goto_0
.end method

.method protected aq()V
    .locals 1

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/aX;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    return-void
.end method

.method public synthetic ar()Lcom/google/googlenav/F;
    .locals 1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    return-object v0
.end method

.method public av()I
    .locals 1

    const/16 v0, 0x17

    return v0
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .locals 1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->l()Lcom/google/googlenav/layer/m;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    invoke-super {p0, p1}, Lbf/m;->b(I)V

    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-direct {p0, v0}, Lbf/bU;->l(Lcom/google/googlenav/ai;)V

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->d(Lbf/i;)V

    return-void
.end method

.method b(Lcom/google/googlenav/cp;)V
    .locals 5

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->av()Lcom/google/googlenav/bZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->e()Z

    move-result v1

    invoke-virtual {v0}, Lcom/google/googlenav/bZ;->f()Z

    move-result v2

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bZ;->a(Z)V

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bZ;->b(Z)V

    new-instance v3, Lcom/google/googlenav/ch;

    new-instance v4, Lbf/cd;

    invoke-direct {v4, p0, v0, v1, v2}, Lbf/cd;-><init>(Lbf/bU;Lcom/google/googlenav/bZ;ZZ)V

    invoke-direct {v3, v0, v4}, Lcom/google/googlenav/ch;-><init>(Lcom/google/googlenav/bZ;Lcom/google/googlenav/ci;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v3}, Law/h;->c(Law/g;)V

    return-void
.end method

.method public bG()Lcom/google/googlenav/ui/view/u;
    .locals 1

    iget-object v0, p0, Lbf/bU;->E:Lcom/google/googlenav/ui/view/u;

    if-nez v0, :cond_0

    new-instance v0, Lbf/cb;

    invoke-direct {v0, p0}, Lbf/cb;-><init>(Lbf/bU;)V

    iput-object v0, p0, Lbf/bU;->E:Lcom/google/googlenav/ui/view/u;

    :cond_0
    iget-object v0, p0, Lbf/bU;->E:Lcom/google/googlenav/ui/view/u;

    return-object v0
.end method

.method public bH()Z
    .locals 1

    iget-object v0, p0, Lbf/bU;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/Y;

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->h()Z

    move-result v0

    return v0
.end method

.method public bI()V
    .locals 3

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-direct {p0}, Lbf/bU;->bX()Lax/y;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->g()Lax/y;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lbf/bU;->a(Lax/y;Lax/y;Ljava/lang/String;)V

    return-void
.end method

.method public bJ()V
    .locals 1

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    return-void
.end method

.method public bK()V
    .locals 3

    const/16 v0, 0x73

    const-string v1, "f"

    const-string v2, "t=o"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bN()Lcom/google/googlenav/T;
    .locals 1

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/Y;->m()Lcom/google/googlenav/T;

    move-result-object v0

    return-object v0
.end method

.method public bO()Lcom/google/googlenav/Y;
    .locals 1

    invoke-super {p0}, Lbf/m;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/Y;

    return-object v0
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/bU;->R:Z

    return-void
.end method

.method public d()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->a()V

    invoke-virtual {p0}, Lbf/bU;->Z()V

    iget-object v0, p0, Lbf/bU;->d:LaN/u;

    invoke-virtual {v0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bU;->bO()Lcom/google/googlenav/Y;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/Y;->b()Lax/y;

    move-result-object v1

    invoke-virtual {v1}, Lax/y;->f()LaN/B;

    move-result-object v1

    invoke-virtual {v0, v1}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-static {v0, v2}, Lax/y;->a(LaN/B;Lo/D;)Lax/y;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/ui/s;->a(Lax/y;Lax/y;)Lcom/google/googlenav/aa;

    :cond_0
    return-void
.end method

.method public d(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lbf/m;->d(Lcom/google/googlenav/ai;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcom/google/googlenav/cp;

    invoke-virtual {p1}, Lcom/google/googlenav/cp;->k()Z

    move-result v0

    goto :goto_0
.end method

.method public f()V
    .locals 8

    const/4 v7, 0x0

    const/16 v0, 0xc3

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/ui/wizard/dg;->e()I

    move-result v1

    xor-int/lit8 v1, v1, 0x4

    xor-int/lit8 v1, v1, 0x8

    const/16 v2, 0x11a

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    const/16 v4, 0x5e3

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v5}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v5

    new-instance v6, Lcom/google/googlenav/ui/wizard/dF;

    invoke-direct {v6}, Lcom/google/googlenav/ui/wizard/dF;-><init>()V

    invoke-virtual {v6, v0}, Lcom/google/googlenav/ui/wizard/dF;->a(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(I)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dF;->b(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    new-instance v1, Lbf/ce;

    const/4 v6, 0x0

    invoke-direct {v1, p0, v6}, Lbf/ce;-><init>(Lbf/bU;Lbf/bV;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->a(Lcom/google/googlenav/ui/wizard/dy;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/ui/wizard/dF;->c(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/ui/wizard/dF;->d(Ljava/lang/String;)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/googlenav/ui/wizard/dF;->a(B)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/dF;->c(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/ui/wizard/dF;->d(Z)Lcom/google/googlenav/ui/wizard/dF;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/wizard/dF;)V

    return-void
.end method

.method protected f(Lat/a;)Z
    .locals 5

    const/16 v4, 0x23

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->a()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lbf/bU;->ae()Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0x73

    const-string v2, "m"

    const-string v3, "a=l"

    invoke-static {v1, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v4, v1}, Lbf/bU;->c(ILjava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lbf/bU;->aa()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/bU;->h()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0}, Lbf/bU;->ag()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v2}, Lbf/bU;->a(B)V

    :goto_0
    return-void

    :sswitch_0
    invoke-virtual {p0, v2, v3}, Lbf/bU;->c(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, v2, v3}, Lbf/bU;->b(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bU;->b(I)V

    :cond_0
    invoke-virtual {p0, v2, v3}, Lbf/bU;->a(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lbf/bU;->ag()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {p0, v1, v3}, Lbf/bU;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0, v2}, Lbf/bU;->a(B)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbf/bU;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->X()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_3
        0x21 -> :sswitch_2
        0x22 -> :sswitch_0
        0x23 -> :sswitch_1
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/bU;->K:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->av()Lcom/google/googlenav/bZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bZ;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast p1, Lcom/google/googlenav/cp;

    invoke-virtual {p0, p1}, Lbf/bU;->b(Lcom/google/googlenav/cp;)V

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/l;

    invoke-direct {v0, p0}, Lbh/l;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected l()V
    .locals 1

    invoke-super {p0}, Lbf/m;->l()V

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bU;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/aX;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/aX;->m()V

    :cond_0
    return-void
.end method

.method protected x()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
