.class Lbf/bt;
.super Lcom/google/googlenav/ui/view/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lbf/bk;


# direct methods
.method constructor <init>(Lbf/bk;)V
    .locals 0

    iput-object p1, p0, Lbf/bt;->a:Lbf/bk;

    invoke-direct {p0}, Lcom/google/googlenav/ui/view/b;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 7

    const/4 v6, 0x1

    const/4 v3, -0x1

    const/4 v5, 0x0

    const/16 v4, 0x9

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/googlenav/ui/android/ap;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/googlenav/ui/android/ap;

    invoke-virtual {p1}, Lcom/google/googlenav/ui/android/ap;->a()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v2, v1}, Lbf/bk;->b(I)V

    iget-object v2, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v2, v1}, Lbf/bk;->b(Z)V

    packed-switch v0, :pswitch_data_0

    :goto_1
    return v6

    :cond_0
    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    const/16 v2, 0xa

    iget-object v3, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v3}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lbf/bk;->a(ILcom/google/googlenav/ai;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    invoke-static {v0}, Lbf/bk;->c(Lbf/bk;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    const-string v2, "m"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v0, v4, v5}, Lbf/bk;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lbf/bk;->f(I)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    iget-object v1, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v1}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lbf/bk;->a(ILcom/google/googlenav/ai;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v3, v5}, Lbf/bk;->a(IILjava/lang/Object;)Z

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    const/16 v1, 0x8

    iget-object v2, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v2}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbf/bk;->a(ILcom/google/googlenav/ai;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    new-instance v1, Lcom/google/googlenav/ui/wizard/A;

    iget-object v2, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v2}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lcom/google/googlenav/ui/wizard/A;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Lbf/bk;->a(Lcom/google/googlenav/ui/wizard/A;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    const/4 v1, 0x7

    iget-object v2, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v2}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbf/bk;->a(ILcom/google/googlenav/ai;)V

    iget-object v0, p0, Lbf/bt;->a:Lbf/bk;

    invoke-virtual {v0, v6, v3, v5}, Lbf/bk;->a(IILjava/lang/Object;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
