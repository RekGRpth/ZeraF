.class public Lbf/bk;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaN/m;
.implements LaR/C;
.implements Lcom/google/android/maps/driveabout/vector/h;
.implements Lcom/google/googlenav/aT;
.implements Lcom/google/googlenav/bb;


# instance fields
.field protected volatile B:Z

.field protected C:I

.field protected D:I

.field private E:Z

.field private F:J

.field private G:Lcom/google/googlenav/ui/view/d;

.field private H:J

.field private I:Lcom/google/googlenav/ui/android/aq;

.field private J:J

.field private K:Lcom/google/googlenav/ui/view/d;

.field private L:LaN/H;

.field private M:Z

.field private N:Lcom/google/googlenav/ui/view/u;

.field private O:Ljava/lang/String;

.field private P:Lcom/google/googlenav/layer/m;

.field private Q:LaN/k;

.field private R:Lcom/google/googlenav/layer/s;

.field private S:Z

.field private T:Lai/b;

.field private U:Lai/b;

.field private V:Z

.field private W:Lbm/i;

.field private X:Lbf/ah;


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/layer/m;LaN/k;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    iput-boolean v0, p0, Lbf/bk;->B:Z

    iput-boolean v0, p0, Lbf/bk;->S:Z

    iput-boolean v0, p0, Lbf/bk;->V:Z

    invoke-direct {p0, p5, p6, p7}, Lbf/bk;->a(Lcom/google/googlenav/layer/m;LaN/k;I)V

    return-void
.end method

.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/n;Lcom/google/googlenav/layer/m;LaN/k;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct/range {p0 .. p5}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;Lcom/google/googlenav/F;)V

    iput-boolean v0, p0, Lbf/bk;->B:Z

    iput-boolean v0, p0, Lbf/bk;->S:Z

    iput-boolean v0, p0, Lbf/bk;->V:Z

    invoke-direct {p0, p6, p7, p8}, Lbf/bk;->a(Lcom/google/googlenav/layer/m;LaN/k;I)V

    iget-object v0, p1, Lcom/google/googlenav/ui/s;->f:Lbm/i;

    iput-object v0, p0, Lbf/bk;->W:Lbm/i;

    return-void
.end method

.method static synthetic a(Lbf/bk;)Lai/b;
    .locals 1

    iget-object v0, p0, Lbf/bk;->T:Lai/b;

    return-object v0
.end method

.method private a(LaW/R;)V
    .locals 4

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v2, Lcom/google/googlenav/bg;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {p1}, LaW/R;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    const/16 v3, 0x5f8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v2

    const-string v3, "20"

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void
.end method

.method static synthetic a(Lbf/bk;Lcom/google/googlenav/aZ;ZZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lbf/bk;->a(Lcom/google/googlenav/aZ;ZZ)V

    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;ZLjava/lang/String;)V
    .locals 4

    const/4 v3, 0x2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/c;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->aM()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    new-instance v1, Lai/b;

    new-instance v2, Lbf/bl;

    invoke-direct {v2, p0, p1, p2}, Lbf/bl;-><init>(Lbf/bk;Lcom/google/googlenav/aZ;Z)V

    invoke-direct {v1, v3, v0, v2}, Lai/b;-><init>(ILcom/google/googlenav/common/io/protocol/ProtoBuf;Lai/c;)V

    iput-object v1, p0, Lbf/bk;->T:Lai/b;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->T:Lai/b;

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private a(Lcom/google/googlenav/aZ;ZZ)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/aZ;)V

    invoke-virtual {p0, v1}, Lbf/bk;->b(I)V

    invoke-virtual {p0}, Lbf/bk;->R()V

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0, p1, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Lcom/google/googlenav/aZ;I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lbf/bk;->bG()V

    invoke-direct {p0}, Lbf/bk;->bZ()V

    iput-boolean v6, p0, Lbf/bk;->B:Z

    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->K()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const/16 v1, 0x5c0

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;)V

    :cond_2
    if-eqz p2, :cond_5

    invoke-static {p1}, Lbf/bk;->d(Lcom/google/googlenav/F;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    :cond_3
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    invoke-virtual {p0}, Lbf/bk;->an()Z

    :cond_5
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lbf/bk;->cl()V

    :cond_6
    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_7

    invoke-direct {p0, p1}, Lbf/bk;->i(Lcom/google/googlenav/aZ;)V

    :cond_7
    invoke-direct {p0, p1}, Lbf/bk;->j(Lcom/google/googlenav/aZ;)V

    if-eqz p2, :cond_8

    iput-boolean v7, p0, Lbf/bk;->M:Z

    invoke-virtual {p0, p3}, Lbf/bk;->k(Z)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lbf/bk;->bY()LaN/H;

    move-result-object v0

    iput-object v0, p0, Lbf/bk;->L:LaN/H;

    :cond_8
    invoke-virtual {p0, p1}, Lbf/bk;->c(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/bk;->c(ILjava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private a(Lcom/google/googlenav/be;Z)V
    .locals 5

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aK()Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :cond_0
    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :goto_0
    if-eqz p2, :cond_1

    iget-object v2, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Set;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0x5f8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v3, "20"

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/layer/m;LaN/k;I)V
    .locals 1

    iput-object p1, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    iput-object p2, p0, Lbf/bk;->Q:LaN/k;

    iput p3, p0, Lbf/bk;->C:I

    new-instance v0, Lcom/google/googlenav/layer/s;

    invoke-direct {v0, p0, p1}, Lcom/google/googlenav/layer/s;-><init>(Lbf/i;Lcom/google/googlenav/layer/m;)V

    iput-object v0, p0, Lbf/bk;->R:Lcom/google/googlenav/layer/s;

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/u;->a(LaR/C;)V

    :cond_0
    iget-object v0, p0, Lbf/bk;->t:Lbh/a;

    check-cast v0, Lbh/i;

    invoke-virtual {v0, p3}, Lbh/i;->c(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bk;->i(Z)V

    invoke-direct {p0}, Lbf/bk;->bW()V

    return-void
.end method

.method private b(ILcom/google/googlenav/ai;)V
    .locals 6

    const/4 v5, 0x0

    new-instance v2, Lbf/bu;

    invoke-direct {v2, p0, p1}, Lbf/bu;-><init>(Lbf/bk;I)V

    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbf/bk;->F:J

    new-instance v0, Lcom/google/googlenav/ui/android/an;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/an;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;Lam/f;)V

    iput-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    return-void
.end method

.method static synthetic b(Lbf/bk;)V
    .locals 0

    invoke-direct {p0}, Lbf/bk;->ca()V

    return-void
.end method

.method public static bV()J
    .locals 2

    const-wide/16 v0, 0x1388

    return-wide v0
.end method

.method private bW()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    instance-of v0, v1, Lbg/b;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lbg/b;

    invoke-virtual {v0}, Lbg/b;->ac()Lcom/google/android/maps/driveabout/vector/VectorMapView;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/VectorMapView;->v()Lcom/google/android/maps/driveabout/vector/aK;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/maps/driveabout/vector/aK;->o()Lcom/google/android/maps/driveabout/vector/aZ;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Lbf/ah;

    invoke-direct {v3, v0, v2}, Lbf/ah;-><init>(Lcom/google/android/maps/driveabout/vector/VectorMapView;Lcom/google/android/maps/driveabout/vector/aZ;)V

    iput-object v3, p0, Lbf/bk;->X:Lbf/ah;

    iget-object v0, p0, Lbf/bk;->X:Lbf/ah;

    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v4, v2, v4

    invoke-static {v2}, LR/a;->a([I)LR/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lbf/am;->a(Lbf/au;LR/a;)V

    goto :goto_0
.end method

.method private bX()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->H()Lbf/i;

    move-result-object v1

    if-eq v1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private bY()LaN/H;
    .locals 6

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->E()V

    iget-object v1, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->S()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->T()I

    move-result v3

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v4

    invoke-virtual {p0, v4}, Lbf/bk;->c(Z)I

    move-result v4

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/K;->ar()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v2

    invoke-virtual {p0, v2}, Lbf/bk;->c(Z)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p0}, Lbf/bk;->q()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    neg-int v2, v2

    neg-int v3, v3

    invoke-virtual {v0, v2, v3, v1}, LaN/B;->a(IILaN/Y;)LaN/B;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0, v1}, Lbf/bk;->a(LaN/B;LaN/Y;)LaN/B;

    move-result-object v0

    iget-object v2, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v2, v0, v1}, LaN/u;->d(LaN/B;LaN/Y;)V

    iget-object v2, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v2, v1}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v1

    invoke-virtual {v1, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    return-object v0
.end method

.method private bZ()V
    .locals 5

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    const/16 v4, 0x10

    invoke-virtual {p0, v3, v4}, Lbf/bk;->a(Lcom/google/googlenav/E;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_1

    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    :cond_1
    return-void
.end method

.method static synthetic c(Lbf/bk;)V
    .locals 0

    invoke-direct {p0}, Lbf/bk;->ch()V

    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v8, 0x4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->d()B

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    :cond_0
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-nez v0, :cond_3

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_1
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v1, Lcom/google/googlenav/bg;

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v3, 0x5f8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v1

    const-string v3, "20"

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/googlenav/bg;->e(I)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-static {p2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lcom/google/googlenav/bd;

    const-string v7, ""

    invoke-direct {v6, v2, v7, p2}, Lcom/google/googlenav/bd;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Lcom/google/googlenav/bc;

    const/16 v7, 0x130

    invoke-static {v7}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v7

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-static {v7, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v8, v1, v0}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private ca()V
    .locals 7

    const/4 v6, 0x3

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aJ()Ljava/util/Map;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    :cond_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v2, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    new-instance v3, Lcom/google/googlenav/bg;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->m()Lcom/google/googlenav/bf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/bg;-><init>(Lcom/google/googlenav/bf;)V

    invoke-virtual {v3, v0}, Lcom/google/googlenav/bg;->a(Ljava/util/Map;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0x5f8

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->c(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v3, "20"

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void

    :cond_1
    new-instance v3, Lcom/google/googlenav/bc;

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, v6, v4, v5}, Lcom/google/googlenav/bc;-><init>(ILjava/lang/String;Ljava/util/List;)V

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private cb()V
    .locals 3

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x15

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    :goto_0
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v1, v0}, Lbf/bk;->a(ILcom/google/googlenav/ai;)V

    invoke-direct {p0, v0}, Lbf/bk;->p(Lcom/google/googlenav/ai;)Lcom/google/googlenav/layer/m;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lbf/am;->c(Lcom/google/googlenav/layer/m;Z)Lbf/y;

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0, p0}, Lcom/google/googlenav/ui/s;->a(Lbf/i;)V

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lbf/bk;->n()V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x16

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    goto :goto_0

    :cond_3
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    goto :goto_0
.end method

.method private cc()V
    .locals 1

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    invoke-virtual {p0}, Lbf/bk;->an()Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bk;->b(Z)V

    return-void
.end method

.method private cd()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "n="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v2}, Lcom/google/googlenav/F;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&gmmsmh=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "u="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private ce()Z
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private cf()Z
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private cg()Lcom/google/googlenav/n;
    .locals 1

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    check-cast v0, Lcom/google/googlenav/n;

    return-object v0
.end method

.method private ch()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbf/bk;->l(Z)V

    return-void
.end method

.method private ci()V
    .locals 3

    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aq;->c()V

    const/4 v0, 0x2

    const-string v1, "pubalerts d"

    iget-object v2, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/aq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbf/bk;->H:J

    goto :goto_0
.end method

.method private cj()V
    .locals 6

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aP()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lbf/bk;->F:J

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/bk;->l(Z)V

    :cond_0
    return-void
.end method

.method private ck()V
    .locals 6

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lbf/bk;->H:J

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    const-string v1, "pubalerts dt"

    iget-object v2, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/aq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/bk;->ci()V

    :cond_0
    return-void
.end method

.method private cl()V
    .locals 4

    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_0
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aq()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lbf/am;->a(Lcom/google/googlenav/aZ;I)V

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x3a98

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbf/bk;->J:J

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v1, Lbf/bv;

    invoke-direct {v1, p0}, Lbf/bv;-><init>(Lbf/bk;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0xec

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->ap:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    :goto_1
    new-instance v2, Lcom/google/googlenav/ui/android/aA;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, Lcom/google/googlenav/ui/android/aA;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ui/aW;)V

    iput-object v2, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    const/16 v0, 0x38

    const-string v1, "v"

    invoke-static {v0, v1}, Lbm/m;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v1, Lbf/bn;

    invoke-direct {v1, p0}, Lbf/bn;-><init>(Lbf/bk;)V

    const/16 v0, 0xed

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/googlenav/ui/aV;->ap:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v2}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    goto :goto_1
.end method

.method private cm()V
    .locals 2

    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbf/bk;->J:J

    :cond_0
    return-void
.end method

.method private cn()V
    .locals 4

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lbf/bk;->J:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-direct {p0}, Lbf/bk;->cm()V

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lbf/bk;)V
    .locals 0

    invoke-direct {p0}, Lbf/bk;->cb()V

    return-void
.end method

.method public static d(Lcom/google/googlenav/F;)Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    invoke-interface {p0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-interface {p0, v1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic e(Lbf/bk;)V
    .locals 0

    invoke-direct {p0}, Lbf/bk;->cm()V

    return-void
.end method

.method private i(Lcom/google/googlenav/aZ;)V
    .locals 1

    invoke-direct {p0}, Lbf/bk;->bX()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lbf/bk;->o(Lcom/google/googlenav/ai;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lbf/bk;->g(Lcom/google/googlenav/aZ;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lbf/bk;->m(Lcom/google/googlenav/aZ;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lbf/bk;->l(Lcom/google/googlenav/aZ;)Z

    goto :goto_0
.end method

.method private j(Lcom/google/googlenav/aZ;)V
    .locals 3

    invoke-direct {p0}, Lbf/bk;->bX()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lbf/bk;->ci()V

    invoke-direct {p0, p1}, Lbf/bk;->k(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lbf/bk;->H:J

    new-instance v1, Lcom/google/googlenav/ui/android/aq;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/ui/android/aq;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ai;)V

    iput-object v1, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    iget-object v0, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/android/aq;->b()V

    const/4 v0, 0x2

    const-string v1, "pubalerts s"

    iget-object v2, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/android/aq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private k(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->ac()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private l(I)Lcom/google/googlenav/ai;
    .locals 1

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    return-object v0
.end method

.method private l(Z)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aQ()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbf/bk;->F:J

    :cond_1
    return-void
.end method

.method private l(Lcom/google/googlenav/aZ;)Z
    .locals 10

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v3

    check-cast v3, Lcom/google/googlenav/ai;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v3}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, v3, v0}, Lbf/bk;->c(Lcom/google/googlenav/ai;Z)Lam/f;

    move-result-object v9

    :goto_1
    new-instance v2, Lbf/bt;

    invoke-direct {v2, p0}, Lbf/bt;-><init>(Lbf/bk;)V

    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbf/bk;->F:J

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/googlenav/ui/android/ao;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v1

    invoke-virtual {p0, v3}, Lbf/bk;->j(Lcom/google/googlenav/ai;)Z

    move-result v4

    invoke-virtual {p0, v3}, Lbf/bk;->k(Lcom/google/googlenav/ai;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/googlenav/ui/android/ao;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lcom/google/googlenav/ai;ZZ)V

    iput-object v0, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v9, 0x0

    goto :goto_1

    :cond_4
    new-instance v4, Lcom/google/googlenav/ui/android/an;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v5

    move-object v6, v2

    move-object v7, p0

    move-object v8, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/googlenav/ui/android/an;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;Lam/f;)V

    iput-object v4, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    goto :goto_2
.end method

.method private m(I)I
    .locals 3

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-direct {p0, v0}, Lbf/bk;->l(I)Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ai;->ab()Z

    move-result v2

    if-eqz v2, :cond_1

    if-nez p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 p1, p1, -0x1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private m(Lcom/google/googlenav/aZ;)V
    .locals 2

    invoke-virtual {p0, p1}, Lbf/bk;->h(Lcom/google/googlenav/aZ;)I

    move-result v1

    const/4 v0, -0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-direct {p0, v1, v0}, Lbf/bk;->b(ILcom/google/googlenav/ai;)V

    goto :goto_0
.end method

.method private m(Lcom/google/googlenav/ai;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ai()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->c()B

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private n(Lcom/google/googlenav/ai;)I
    .locals 3

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    if-ne v2, p1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private o(Lcom/google/googlenav/ai;)V
    .locals 3

    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lbf/bk;->f(I)V

    new-instance v0, Lbf/bs;

    invoke-direct {v0, p0}, Lbf/bs;-><init>(Lbf/bk;)V

    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->c()V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lbf/bk;->F:J

    new-instance v1, Lcom/google/googlenav/ui/android/aC;

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/view/e;->d()Lcom/google/googlenav/ui/android/ButtonContainer;

    move-result-object v2

    invoke-direct {v1, v2, v0, p0, p1}, Lcom/google/googlenav/ui/android/aC;-><init>(Lcom/google/googlenav/ui/android/ButtonContainer;Lcom/google/googlenav/ui/view/c;Lbf/bk;Lcom/google/googlenav/ai;)V

    iput-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    return-void
.end method

.method private p(Lcom/google/googlenav/ai;)Lcom/google/googlenav/layer/m;
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/dY;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bO()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->bN()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "msid:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x0

    invoke-virtual {v0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v1, 0x11

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ap()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/dY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aD()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v2, 0x4

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v7, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v2, 0x17

    invoke-virtual {v0, v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, Lcom/google/googlenav/layer/m;

    invoke-direct {v1, v0}, Lcom/google/googlenav/layer/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-object v1
.end method


# virtual methods
.method public M()Z
    .locals 1

    invoke-direct {p0}, Lbf/bk;->ce()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->ah()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lbf/m;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected T()Ljava/lang/String;
    .locals 1

    const/16 v0, 0x4f9

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected U()I
    .locals 1

    const/16 v0, 0xf

    return v0
.end method

.method protected X()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lbf/m;->X()Z

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->h()[Lcom/google/googlenav/W;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lbf/bk;->S:Z

    if-eqz v3, :cond_2

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->a()V

    iput-boolean v2, p0, Lbf/bk;->S:Z

    invoke-virtual {p0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->Z()V

    :cond_0
    invoke-virtual {p0}, Lbf/bk;->R()V

    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public Y()V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lbf/bk;->M:Z

    if-eqz v2, :cond_0

    iput-boolean v0, p0, Lbf/bk;->M:Z

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lbf/bk;->L:LaN/H;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {v2}, LaN/H;->a()LaN/B;

    move-result-object v2

    iget-object v3, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v3}, LaN/u;->c()LaN/B;

    move-result-object v3

    iget-object v4, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LaN/B;->a(LaN/B;LaN/Y;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    iget-object v2, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {v2}, LaN/H;->b()LaN/Y;

    move-result-object v2

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    iget-object v3, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v3}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    if-ge v2, v3, :cond_1

    move v0, v1

    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lbf/bk;->L:LaN/H;

    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v1}, Lbf/bk;->e(Z)V

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lbf/bk;->b(Z)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lbf/bk;->cj()V

    invoke-direct {p0}, Lbf/bk;->cn()V

    invoke-direct {p0}, Lbf/bk;->ck()V

    invoke-super {p0}, Lbf/m;->Y()V

    goto :goto_0
.end method

.method public a()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget-boolean v0, p0, Lbf/bk;->V:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/bk;->W:Lbm/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bk;->W:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->b()V

    iget-object v0, p0, Lbf/bk;->W:Lbm/i;

    invoke-virtual {v0}, Lbm/i;->g()I

    move-result v0

    if-lez v0, :cond_0

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "s=a"

    aput-object v3, v1, v2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "t="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-static {v1}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stat"

    invoke-static {v5, v1, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    :cond_0
    iput-boolean v4, p0, Lbf/bk;->V:Z

    :cond_1
    return-void
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    iput-object p1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->az()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bk;->b(B)V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V
    .locals 7

    const/4 v6, 0x1

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(Lcom/google/googlenav/R;Lcom/google/googlenav/E;I)V

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->S()I

    move-result v2

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->T()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-virtual {v4}, LaN/Y;->a()I

    move-result v4

    invoke-static {v0, v2, v3, v4}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/R;

    :cond_0
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->d(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->e(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_2
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->f(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_3
    const/4 v0, 0x4

    if-eq p3, v0, :cond_4

    const/16 v0, 0xf

    if-ne p3, v0, :cond_5

    :cond_4
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aa()Lcom/google/googlenav/layer/a;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/googlenav/layer/a;->a()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(I)Lcom/google/googlenav/R;

    :cond_5
    :goto_0
    const/16 v0, 0x19

    if-eq p3, v0, :cond_6

    const/16 v0, 0x18

    if-ne p3, v0, :cond_7

    :cond_6
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_7

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aG()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/Integer;)Lcom/google/googlenav/R;

    :cond_7
    const/16 v0, 0x1f

    if-ne p3, v0, :cond_8

    if-eqz p2, :cond_8

    move-object v0, p2

    check-cast v0, Lcom/google/googlenav/ai;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->Y()I

    move-result v1

    sub-int v1, v4, v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "t="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->N()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->c(Ljava/lang/String;)Lcom/google/googlenav/R;

    :cond_8
    if-eqz p2, :cond_9

    invoke-interface {p2}, Lcom/google/googlenav/E;->d()I

    move-result v0

    if-nez v0, :cond_9

    check-cast p2, Lcom/google/googlenav/ai;

    invoke-virtual {p2}, Lcom/google/googlenav/ai;->bQ()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/util/List;)Lcom/google/googlenav/R;

    :cond_9
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/R;->a(Ljava/lang/String;)Lcom/google/googlenav/R;

    invoke-virtual {p1, v6}, Lcom/google/googlenav/R;->b(Z)Lcom/google/googlenav/R;

    :cond_a
    return-void

    :cond_b
    invoke-virtual {p1, v6}, Lcom/google/googlenav/R;->a(Z)Lcom/google/googlenav/R;

    goto/16 :goto_0
.end method

.method public a(Lcom/google/googlenav/aS;)V
    .locals 3

    invoke-virtual {p1}, Lcom/google/googlenav/aS;->i()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->O:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/googlenav/aS;->a(I)Lcom/google/googlenav/aU;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aU;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/bk;->O:Ljava/lang/String;

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    iget-object v1, p0, Lbf/bk;->O:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/googlenav/ui/aT;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/dialog/bT;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/google/googlenav/aZ;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/google/googlenav/ai;Ljava/util/List;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aE()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aK()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aL()Lai/d;

    move-result-object v1

    invoke-virtual {v1}, Lai/d;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/googlenav/ai;->l(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    sget-object v1, Lcom/google/googlenav/ui/aV;->ai:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aj:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    sget-object v0, Laz/j;->a:Laz/j;

    invoke-virtual {v0}, Laz/j;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aF()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aj:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aG()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->aj:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->ah:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->aH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->ah:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->a(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method protected a(Lcom/google/googlenav/ui/aW;)V
    .locals 3

    invoke-static {}, Lcom/google/googlenav/ui/view/e;->a()Lcom/google/googlenav/ui/view/e;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/googlenav/ui/aW;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Lcom/google/common/collect/bx;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/view/e;->a(Ljava/util/List;)V

    return-void
.end method

.method protected a(Ljava/io/DataOutput;)V
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/aZ;->b(Ljava/io/DataOutput;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;LaN/H;I)V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/google/googlenav/bg;

    invoke-direct {v0}, Lcom/google/googlenav/bg;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bg;->a(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->c(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/googlenav/bg;->d(I)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->a(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    const-string v1, "20"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->b(Ljava/lang/String;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/googlenav/bg;->a(Lcom/google/googlenav/bb;)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bg;->c(Z)Lcom/google/googlenav/bg;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/bg;->f(Z)Lcom/google/googlenav/bg;

    move-result-object v1

    invoke-virtual {p0}, Lbf/bk;->bU()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->p()Lcom/google/googlenav/ui/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/ak;->s()LaH/m;

    move-result-object v0

    invoke-interface {v0}, LaH/m;->s()LaH/h;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lbf/bk;->d:LaN/u;

    invoke-virtual {v2}, LaN/u;->f()LaN/H;

    move-result-object v2

    invoke-virtual {v2, v0}, LaN/H;->a(LaN/B;)LaN/H;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bg;->a(LaN/H;)Lcom/google/googlenav/bg;

    :cond_0
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/bg;->a()Lcom/google/googlenav/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/bf;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->O()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 1

    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0, p2}, Lcom/google/googlenav/layer/m;->a([Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/Vector;LaN/B;I)V
    .locals 9

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    const/4 v0, 0x0

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    invoke-virtual {v3, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/E;->c()B

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2, p2}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v4

    int-to-long v6, p3

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    invoke-static {p0, v2, v0, v4, v5}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    return-void

    :cond_4
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v6

    if-eqz v6, :cond_3

    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/googlenav/aZ;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0, v0, p2}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;)J

    move-result-wide v1

    int-to-long v7, p3

    cmp-long v7, v1, v7

    if-gez v7, :cond_5

    iget-boolean v7, p0, Lbf/bk;->o:Z

    if-eqz v7, :cond_6

    sget v7, Lbf/am;->j:I

    int-to-long v7, v7

    add-long/2addr v1, v7

    :cond_6
    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v7

    add-int/2addr v7, v4

    invoke-static {p0, v0, v7, v1, v2}, Lbf/ai;->a(Lbf/i;Lcom/google/googlenav/E;IJ)Lbf/ai;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, v0}, Lbf/m;->a(Z)V

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aF()V

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    iget-object v0, p0, Lbf/bk;->y:LaB/s;

    invoke-virtual {v0}, LaB/s;->a()V

    return-void
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 10

    const/4 v0, 0x7

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v5, 0x1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v5

    :cond_1
    :goto_1
    return v5

    :sswitch_0
    iput-boolean v9, p0, Lbf/bk;->B:Z

    iput p2, p0, Lbf/bk;->D:I

    const-string v0, "d"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lbf/bk;->j(I)V

    goto :goto_1

    :sswitch_1
    iget-object v1, p0, Lbf/bk;->U:Lai/b;

    invoke-virtual {v1}, Lai/b;->i()[Lcom/google/googlenav/ai;

    move-result-object v1

    if-eqz v1, :cond_1

    array-length v2, v1

    if-ge p2, v2, :cond_1

    iget-object v2, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v2

    new-instance v3, Lcom/google/googlenav/ui/wizard/A;

    invoke-direct {v3, v0}, Lcom/google/googlenav/ui/wizard/A;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    aget-object v1, v1, p2

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/ai;BZ)V

    goto :goto_1

    :sswitch_2
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p2}, Lcom/google/googlenav/F;->a(I)V

    iput p2, p0, Lbf/bk;->D:I

    invoke-virtual {p0}, Lbf/bk;->an()Z

    invoke-virtual {p0, v5}, Lbf/bk;->b(Z)V

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lbf/bk;->bO()V

    goto :goto_1

    :sswitch_4
    invoke-direct {p0}, Lbf/bk;->cb()V

    const/16 v0, 0x54

    const-string v1, "ac"

    const-string v2, "sl"

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_5
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "20"

    invoke-virtual {v0, v1, v2, v6, v9}, Lcom/google/googlenav/ui/s;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    :sswitch_6
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "b"

    invoke-static {v0, v1, p2, p0, v2}, Lcom/google/googlenav/ui/wizard/hy;->a(Lcom/google/googlenav/J;Lcom/google/googlenav/aZ;ILcom/google/googlenav/bb;Ljava/lang/String;)V

    goto :goto_1

    :sswitch_7
    const/16 v0, 0x59

    const-string v2, "m"

    const-string v3, "1"

    invoke-static {v0, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v9}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    goto :goto_1

    :sswitch_8
    const/16 v0, 0x59

    const-string v2, "m"

    const-string v3, "2"

    invoke-static {v0, v2, v3}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v5}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;Z)V

    goto/16 :goto_1

    :sswitch_9
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v9}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_1

    :sswitch_a
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0, v1, p0, v4}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_1

    :sswitch_b
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {v0, v1, p0, v2}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/aZ;Lcom/google/googlenav/bb;I)V

    goto/16 :goto_1

    :sswitch_c
    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge v0, v4, :cond_2

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v5

    goto/16 :goto_1

    :cond_2
    if-eqz p3, :cond_3

    check-cast p3, [Ljava/lang/String;

    check-cast p3, [Ljava/lang/String;

    aget-object v0, p3, v9

    aget-object v1, p3, v5

    invoke-direct {p0, v0, v1}, Lbf/bk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    invoke-direct {p0, v6, v6}, Lbf/bk;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_d
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v8

    const-string v0, "OpenNowNotification"

    invoke-interface {v8, v0}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    const/16 v1, 0x363

    invoke-static {v1}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x362

    invoke-static {v2}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x35d

    invoke-static {v3}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x6a

    invoke-static {v4}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v4

    new-instance v7, Lbf/bq;

    invoke-direct {v7, p0}, Lbf/bq;-><init>(Lbf/bk;)V

    invoke-virtual/range {v0 .. v7}, Lcom/google/googlenav/ui/wizard/jv;->a(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/googlenav/ui/wizard/bo;)V

    const-string v0, "OpenNowNotification"

    new-array v1, v5, [B

    aput-byte v5, v1, v9

    invoke-interface {v8, v0, v1}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    invoke-interface {v8}, Lcom/google/googlenav/common/io/j;->a()V

    goto/16 :goto_1

    :cond_4
    invoke-direct {p0}, Lbf/bk;->ca()V

    goto/16 :goto_1

    :sswitch_e
    if-eqz p3, :cond_1

    check-cast p3, Lcom/google/googlenav/be;

    invoke-direct {p0, p3, v5}, Lbf/bk;->a(Lcom/google/googlenav/be;Z)V

    goto/16 :goto_1

    :sswitch_f
    if-eqz p3, :cond_1

    check-cast p3, Lcom/google/googlenav/be;

    invoke-direct {p0, p3, v9}, Lbf/bk;->a(Lcom/google/googlenav/be;Z)V

    goto/16 :goto_1

    :sswitch_10
    if-eqz p3, :cond_0

    move-object v0, p3

    check-cast v0, LaW/R;

    invoke-direct {p0, v0}, Lbf/bk;->a(LaW/R;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_2
    invoke-virtual {p0, v0, v6}, Lbf/bk;->d(ILjava/lang/Object;)Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/ui/wizard/z;->a(Lcom/google/googlenav/ui/wizard/A;)V

    invoke-virtual {p0}, Lbf/bk;->W()V

    goto/16 :goto_1

    :cond_5
    const/16 v0, 0x8

    goto :goto_2

    :sswitch_12
    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v5

    goto/16 :goto_1

    :sswitch_13
    invoke-virtual {p0}, Lbf/bk;->bE()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->f()V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x2bc -> :sswitch_0
        0x2c4 -> :sswitch_4
        0x2c5 -> :sswitch_3
        0x2c6 -> :sswitch_6
        0x2c7 -> :sswitch_7
        0x2c8 -> :sswitch_9
        0x2ca -> :sswitch_a
        0x2cd -> :sswitch_d
        0x2d0 -> :sswitch_8
        0x2d1 -> :sswitch_c
        0x2d3 -> :sswitch_b
        0x2d4 -> :sswitch_1
        0x2d5 -> :sswitch_f
        0x2d6 -> :sswitch_e
        0x2d7 -> :sswitch_10
        0x2ef -> :sswitch_5
        0x3f9 -> :sswitch_11
        0x6a4 -> :sswitch_12
        0x713 -> :sswitch_c
        0x76e -> :sswitch_13
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/bk;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bk;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_1

    sget-object v0, Lbf/bk;->z:Lcom/google/googlenav/ui/view/android/au;

    if-eqz v0, :cond_0

    sget-object v0, Lbf/bk;->z:Lcom/google/googlenav/ui/view/android/au;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/android/au;->dismiss()V

    sput-object v1, Lbf/bk;->z:Lcom/google/googlenav/ui/view/android/au;

    :cond_0
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v1}, Lbf/bk;->a(ILjava/lang/Object;)V

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    const-string v1, "s"

    const-string v2, "c"

    invoke-virtual {p0}, Lbf/bk;->u()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lbf/bk;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_0
.end method

.method protected a(Ljava/io/DataInput;)Z
    .locals 8

    const/4 v7, 0x1

    invoke-static {p1}, Lcom/google/googlenav/aZ;->b(Ljava/io/DataInput;)Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/googlenav/aZ;->b(Z)V

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v1

    iput-object v1, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    new-instance v1, Lcom/google/googlenav/n;

    new-instance v2, Lcom/google/googlenav/T;

    iget-object v3, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    iget-object v4, p0, Lbf/bk;->Q:LaN/k;

    iget-object v5, p0, Lbf/bk;->c:LaN/p;

    iget-object v6, p0, Lbf/bk;->d:LaN/u;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/googlenav/T;-><init>(Lcom/google/googlenav/layer/m;LaN/o;LaN/p;LaN/u;)V

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    iput-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ab()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lbf/bp;

    invoke-direct {v2, p0, v0}, Lbf/bp;-><init>(Lbf/bk;Lcom/google/googlenav/aZ;)V

    invoke-virtual {v1, v2, v7}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    return v7
.end method

.method public aB()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aC()Z
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aD()Z
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    return v0
.end method

.method public aF()I
    .locals 2

    invoke-static {}, Lcom/google/googlenav/android/c;->a()Lcom/google/googlenav/android/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/c;->c()Lcom/google/googlenav/android/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/android/i;->f()Lcom/google/android/maps/MapsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/MapsActivity;->hasDataConnection()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f110014

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    const v0, 0x7f110013

    goto :goto_0

    :cond_1
    const v0, 0x7f110015

    goto :goto_0

    :cond_2
    invoke-super {p0}, Lbf/m;->aF()I

    move-result v0

    goto :goto_0
.end method

.method public aJ()Ljava/lang/CharSequence;
    .locals 1

    const/16 v0, 0x4fb

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public aK()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lbf/bk;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->al()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->L()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lbf/bk;->d(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public aL()Lam/f;
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ad:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/bi;->p()Lam/g;

    move-result-object v0

    sget-char v1, Lcom/google/googlenav/ui/bi;->ae:C

    invoke-interface {v0, v1}, Lam/g;->e(C)Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method public aM()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected aN()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public aT()Z
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lbf/bk;->C:I

    invoke-static {v0, v2}, Lbf/am;->a(IZ)V

    invoke-static {p0}, Lcom/google/googlenav/common/k;->a(Lcom/google/googlenav/common/h;)V

    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/bk;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->a(LaN/m;)V

    :cond_0
    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v1, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->a(LaN/k;)V

    :cond_1
    return v2
.end method

.method public aU()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-interface {v0, p0}, LaR/u;->b(LaR/C;)V

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbf/bk;->l(Z)V

    invoke-direct {p0}, Lbf/bk;->cm()V

    invoke-direct {p0}, Lbf/bk;->ci()V

    invoke-static {p0}, Lcom/google/googlenav/common/k;->c(Lcom/google/googlenav/common/h;)V

    iget v0, p0, Lbf/bk;->C:I

    invoke-static {v0, v1}, Lbf/am;->a(IZ)V

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/aZ;->a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->e(Lcom/google/googlenav/aZ;)V

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/T;->g()V

    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v1, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v1}, LaN/p;->b(LaN/k;)V

    :cond_1
    iget-object v0, p0, Lbf/bk;->X:Lbf/ah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->X:Lbf/ah;

    invoke-virtual {v0, v1}, Lbf/am;->a(Lbf/au;)V

    :cond_2
    iput-object v2, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-super {p0}, Lbf/m;->aU()V

    return-void
.end method

.method public aW()V
    .locals 2

    invoke-super {p0}, Lbf/m;->aW()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/googlenav/ui/aV;->bG:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/aW;)V

    :cond_1
    return-void
.end method

.method public aX()V
    .locals 2

    invoke-super {p0}, Lbf/m;->aX()V

    invoke-direct {p0}, Lbf/bk;->ch()V

    invoke-direct {p0}, Lbf/bk;->cm()V

    invoke-direct {p0}, Lbf/bk;->ci()V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/16 v0, 0x4f8

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bF:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/aW;)V

    :cond_1
    return-void
.end method

.method protected am()V
    .locals 8

    invoke-virtual {p0}, Lbf/bk;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/google/googlenav/W;

    const/16 v2, 0x43

    const-string v3, "o"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "l="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/googlenav/W;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "i="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v4}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lbf/bk;->R:Lcom/google/googlenav/layer/s;

    invoke-virtual {v1, v0}, Lcom/google/googlenav/layer/s;->b(Lcom/google/googlenav/ai;)Law/g;

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lbf/m;->am()V

    goto :goto_0
.end method

.method public ao()Z
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->aj()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {v0}, Lcom/google/googlenav/W;->q()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected aq()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbf/bk;->d(Z)V

    new-instance v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-direct {v0, p0}, Lcom/google/googlenav/ui/view/dialog/bT;-><init>(Lbf/bk;)V

    iput-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    invoke-virtual {p0}, Lbf/bk;->bG()V

    return-void
.end method

.method public au()Z
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lbf/m;->au()Z

    move-result v0

    goto :goto_0
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ay()Z
    .locals 1

    iget-boolean v0, p0, Lbf/bk;->p:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ar()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(LaN/B;)I
    .locals 7

    const/4 v6, 0x0

    const/4 v1, -0x1

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v2, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v2, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v2

    invoke-virtual {p0, v2, p1, v6}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-virtual {p0, v4, p1, v6}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->f()I

    move-result v2

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/googlenav/T;->a(LaN/B;)Ljava/util/Enumeration;

    move-result-object v4

    if-nez v4, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/W;

    invoke-virtual {p0, v0, p1, v6}, Lbf/bk;->a(Lcom/google/googlenav/E;LaN/B;Lam/e;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lcom/google/googlenav/W;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/T;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 2

    const/4 v0, 0x0

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->aI()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->d(B)I

    move-result v0

    neg-int v0, v0

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public b()Lcom/google/googlenav/layer/m;
    .locals 1

    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    return-object v0
.end method

.method protected b(ILjava/lang/Object;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lbf/m;->b(ILjava/lang/Object;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->n()Lcom/google/googlenav/ui/as;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/as;->d()V

    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbf/bk;->B:Z

    return-void
.end method

.method public b(Lcom/google/googlenav/aZ;Z)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bV()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->ad()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    sget-boolean v1, Lcom/google/googlenav/aZ;->a:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2, v0}, Lbf/bk;->a(Lcom/google/googlenav/aZ;ZLjava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p2, v2}, Lbf/bk;->a(Lcom/google/googlenav/aZ;ZZ)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bk;->e(Z)V

    invoke-virtual {p0, v0}, Lbf/bk;->d(Z)V

    return-void
.end method

.method protected b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x5

    const-string v1, "v"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "v"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected b(Z)V
    .locals 0

    invoke-direct {p0}, Lbf/bk;->cj()V

    invoke-direct {p0}, Lbf/bk;->cn()V

    invoke-direct {p0}, Lbf/bk;->ck()V

    invoke-super {p0, p1}, Lbf/m;->b(Z)V

    return-void
.end method

.method public b(Lcom/google/googlenav/ai;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->ab()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lbf/bk;->l(Lcom/google/googlenav/ai;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected bC()I
    .locals 1

    iget v0, p0, Lbf/bk;->D:I

    return v0
.end method

.method protected bG()V
    .locals 3

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ak()Las/c;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lbf/bo;

    invoke-direct {v1, p0, v0}, Lbf/bo;-><init>(Lbf/bk;Las/c;)V

    invoke-virtual {v1}, Lbf/bo;->g()V

    :cond_0
    invoke-virtual {p0}, Lbf/bk;->bU()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->aL()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v0

    invoke-virtual {v0}, LaN/H;->a()LaN/B;

    move-result-object v0

    new-instance v1, Lcom/google/googlenav/aS;

    const/4 v2, -0x1

    invoke-direct {v1, v0, v2}, Lcom/google/googlenav/aS;-><init>(LaN/B;I)V

    invoke-virtual {v1, p0}, Lcom/google/googlenav/aS;->a(Lcom/google/googlenav/aT;)V

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lbf/bk;->O:Ljava/lang/String;

    goto :goto_0
.end method

.method protected bH()Lcom/google/googlenav/aZ;
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Lcom/google/googlenav/aZ;->a(ILcom/google/googlenav/bb;)Lcom/google/googlenav/aZ;

    move-result-object v0

    return-object v0
.end method

.method public bI()Lbf/ah;
    .locals 1

    iget-object v0, p0, Lbf/bk;->X:Lbf/ah;

    return-object v0
.end method

.method public bJ()V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v1, v1, LaB/p;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    :cond_0
    iget-object v1, p0, Lbf/bk;->y:LaB/s;

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lbf/bk;->u:Lcom/google/common/base/x;

    invoke-virtual {v1, v2, v3, v0}, LaB/s;->a(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    return-void
.end method

.method public bK()V
    .locals 6

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    new-instance v2, Lbf/bw;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lbf/bw;-><init>(Lbf/bl;)V

    iget-object v3, p0, Lbf/bk;->y:LaB/s;

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aE()[Lcom/google/googlenav/ai;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, LaB/p;

    invoke-virtual {v3, v4, v2, v0}, LaB/s;->b(Ljava/lang/Iterable;Lcom/google/common/base/x;LaB/p;)V

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "c="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->f()I

    move-result v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "p="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbf/bw;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "s="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbf/bw;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "u="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lbf/bw;->c()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lbm/m;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x5e

    const-string v2, "r"

    invoke-static {v1, v2, v0}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bL()V
    .locals 3

    const/4 v0, 0x5

    const-string v1, "0"

    invoke-direct {p0}, Lbf/bk;->cd()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbm/m;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected bM()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->f()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bN()V
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lbf/bk;->b(Lcom/google/googlenav/aZ;Z)V

    return-void
.end method

.method protected bO()V
    .locals 4

    iget-boolean v0, p0, Lbf/bk;->B:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/bk;->B:Z

    invoke-virtual {p0}, Lbf/bk;->bH()Lcom/google/googlenav/aZ;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/googlenav/ui/s;->a(Lcom/google/googlenav/aZ;IZ)V

    goto :goto_0
.end method

.method public bP()Z
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ao()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x12c

    :goto_0
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->am()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->aq()I

    move-result v1

    if-ge v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/16 v0, 0x64

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bQ()Lcom/google/googlenav/aZ;
    .locals 1

    invoke-direct {p0}, Lbf/bk;->cg()Lcom/google/googlenav/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/n;->a()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/aZ;

    return-object v0
.end method

.method public bR()Lcom/google/googlenav/T;
    .locals 1

    invoke-direct {p0}, Lbf/bk;->cg()Lcom/google/googlenav/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/n;->b()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/T;

    return-object v0
.end method

.method public bS()I
    .locals 1

    iget v0, p0, Lbf/bk;->C:I

    return v0
.end method

.method public bT()Lcom/google/googlenav/ui/view/u;
    .locals 1

    iget-object v0, p0, Lbf/bk;->N:Lcom/google/googlenav/ui/view/u;

    if-nez v0, :cond_0

    new-instance v0, Lbf/br;

    invoke-direct {v0, p0}, Lbf/br;-><init>(Lbf/bk;)V

    iput-object v0, p0, Lbf/bk;->N:Lcom/google/googlenav/ui/view/u;

    :cond_0
    iget-object v0, p0, Lbf/bk;->N:Lcom/google/googlenav/ui/view/u;

    return-object v0
.end method

.method public bU()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aA()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aB()LaN/B;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->U()LaN/H;

    move-result-object v4

    invoke-virtual {v4}, LaN/H;->a()LaN/B;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/googlenav/ui/aT;->a(LaN/B;LaN/B;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->M()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->aL()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public bg()Z
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bh()Z
    .locals 1

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->m()Z

    move-result v0

    return v0
.end method

.method public bi()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bl()Z
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->z()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bm()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected bn()Ljava/lang/String;
    .locals 1

    const-string v0, "p"

    return-object v0
.end method

.method protected bo()V
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bu()Lcom/google/googlenav/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->af()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/aZ;->a(Lcom/google/googlenav/ai;)V

    :cond_0
    return-void
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 2

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bq()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->br()Lcom/google/googlenav/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/e;->f()I

    move-result v0

    :goto_0
    rsub-int/lit8 v0, v0, 0x1

    :goto_1
    return v0

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aI()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/bk;->a:Lcom/google/googlenav/ui/bi;

    invoke-interface {p1}, Lcom/google/googlenav/E;->c()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->c(B)I

    move-result v0

    neg-int v0, v0

    goto :goto_1

    :cond_0
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1

    :sswitch_3
    move-object v0, p1

    check-cast v0, Lcom/google/googlenav/ai;

    iget-object v1, p0, Lbf/bk;->t:Lbh/a;

    check-cast v1, Lbh/i;

    invoke-virtual {v1}, Lbh/i;->b()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ai;->a(B)V

    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1}, Lbf/bk;->f(Lcom/google/googlenav/E;)I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xc -> :sswitch_1
        0xf -> :sswitch_2
        0x10 -> :sswitch_3
        0x11 -> :sswitch_3
        0x12 -> :sswitch_3
        0x13 -> :sswitch_3
    .end sparse-switch
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbf/bk;->S:Z

    return-void
.end method

.method public c(Lcom/google/googlenav/F;)V
    .locals 3

    invoke-virtual {p0, p1}, Lbf/bk;->a(Lcom/google/googlenav/F;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbf/bk;->b(B)V

    invoke-virtual {p0}, Lbf/bk;->R()V

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->W()I

    move-result v1

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/T;->g()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lbf/bk;->B:Z

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ac()Lcom/google/googlenav/layer/m;

    move-result-object v0

    iput-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->b(LaN/m;)V

    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0}, LaN/k;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v2, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v2}, LaN/p;->b(LaN/k;)V

    :cond_0
    iget-object v0, p0, Lbf/bk;->P:Lcom/google/googlenav/layer/m;

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbf/bk;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, p0}, LaN/k;->a(LaN/m;)V

    :cond_1
    iget-object v0, p0, Lbf/bk;->c:LaN/p;

    iget-object v2, p0, Lbf/bk;->Q:LaN/k;

    invoke-virtual {v0, v2}, LaN/p;->a(LaN/k;)V

    :cond_2
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    instance-of v0, v0, Lcom/google/googlenav/ui/view/dialog/bT;

    if-eqz v0, :cond_4

    const/16 v0, 0xa

    if-eq v1, v0, :cond_3

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->l()V

    :cond_3
    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->m()V

    iget-object v0, p0, Lbf/bk;->r:Lcom/google/googlenav/ui/view/android/aL;

    check-cast v0, Lcom/google/googlenav/ui/view/dialog/bT;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/view/dialog/bT;->h()V

    :cond_4
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->aq()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->ar()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/googlenav/ui/aV;->bG:Lcom/google/googlenav/ui/aV;

    invoke-static {v0, v1}, Lcom/google/googlenav/ui/aW;->b(Ljava/lang/String;Lcom/google/googlenav/ui/aV;)Lcom/google/googlenav/ui/aW;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/aW;)V

    :cond_6
    invoke-direct {p0}, Lbf/bk;->bY()LaN/H;

    invoke-direct {p0}, Lbf/bk;->bZ()V

    return-void
.end method

.method protected c(Lat/a;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lbf/bk;->aa()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lbf/bk;->E:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lbf/bk;->E:Z

    invoke-virtual {p0}, Lbf/bk;->h()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method protected c(Lcom/google/googlenav/aZ;)Z
    .locals 2

    const-string v0, "19"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "20"

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->H()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bS()I

    move-result v0

    return v0
.end method

.method public d(Lcom/google/googlenav/aZ;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->Q()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->Q()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x207

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->W()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->C()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x47d

    invoke-static {v0}, Lcom/google/googlenav/X;->a(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->B()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/googlenav/aZ;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lau/b;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected e(Lcom/google/googlenav/aZ;)V
    .locals 2

    new-instance v0, Lcom/google/googlenav/n;

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/google/googlenav/n;-><init>(Lcom/google/googlenav/F;Lcom/google/googlenav/F;)V

    iput-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    return-void
.end method

.method protected e(Lcom/google/googlenav/ui/r;)V
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbf/bk;->a(Lcom/google/googlenav/ui/r;Lcom/google/googlenav/F;)V

    return-void
.end method

.method public e(Lat/a;)Z
    .locals 8

    const/16 v7, 0x36

    const/16 v6, 0x34

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v5

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    if-eq v5, v7, :cond_2

    if-ne v5, v6, :cond_0

    :cond_2
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->c()I

    move-result v0

    invoke-virtual {p0}, Lbf/bk;->ah()Z

    move-result v4

    if-eqz v4, :cond_6

    if-eq v0, v3, :cond_6

    :cond_3
    if-ltz v0, :cond_4

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ne v0, v3, :cond_5

    :cond_4
    move v0, v1

    :cond_5
    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/googlenav/aZ;->f()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    invoke-virtual {p0}, Lbf/bk;->an()Z

    invoke-virtual {p0, v2}, Lbf/bk;->b(Z)V

    move v1, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lbf/bk;->ah()Z

    move-result v4

    if-nez v4, :cond_7

    if-ne v5, v6, :cond_8

    invoke-direct {p0}, Lbf/bk;->ce()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    move v4, v0

    :goto_1
    if-ne v5, v6, :cond_9

    move v0, v3

    :goto_2
    add-int/2addr v0, v4

    if-ltz v0, :cond_3

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/aZ;->f()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/googlenav/E;->c()B

    move-result v4

    if-nez v4, :cond_3

    :cond_7
    move v4, v0

    goto :goto_1

    :cond_8
    if-ne v5, v7, :cond_7

    invoke-direct {p0}, Lbf/bk;->cf()Z

    move-result v4

    if-eqz v4, :cond_7

    move v4, v3

    goto :goto_1

    :cond_9
    move v0, v2

    goto :goto_2
.end method

.method public e()[Lcom/google/googlenav/ui/aI;
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->ai()[Lcom/google/googlenav/ui/aH;

    move-result-object v0

    return-object v0
.end method

.method public f(Lcom/google/googlenav/aZ;)Lcom/google/googlenav/ai;
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->bN()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected f()Z
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/aZ;->at()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f(Lat/a;)Z
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/16 v2, 0x23

    const/4 v0, 0x1

    invoke-virtual {p1}, Lat/a;->e()C

    move-result v1

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lbf/bk;->ae()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "m"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lbf/bk;->cc()V

    invoke-virtual {p0, v4, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "l"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x7

    invoke-virtual {p0, v1, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbf/bk;->bM()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "l"

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x9

    invoke-virtual {p0, v1, v3}, Lbf/bk;->c(ILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lbf/bk;->g(Lat/a;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lbf/bk;->d(Lat/a;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    if-ne v1, v4, :cond_4

    invoke-virtual {p0}, Lbf/bk;->aa()Z

    move-result v1

    if-eqz v1, :cond_4

    iput-boolean v0, p0, Lbf/bk;->E:Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lbf/m;->g(I)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lbf/bk;->b()Lcom/google/googlenav/layer/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/layer/m;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final g(Lat/a;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/bk;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v1, v5}, Lbf/bk;->b(ILjava/lang/Object;)V

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lbf/bk;->m(Lcom/google/googlenav/ai;)Z

    move-result v3

    if-nez v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_3

    invoke-virtual {p0}, Lbf/bk;->ag()Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v1, 0x9

    invoke-virtual {p0, v1, v5}, Lbf/bk;->a(ILjava/lang/Object;)V

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/ai;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    const-string v3, "s"

    const-string v4, "k"

    invoke-virtual {p0, v1, v3, v4, v0}, Lbf/bk;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public g(Lcom/google/googlenav/aZ;)Z
    .locals 2

    invoke-virtual {p0, p1}, Lbf/bk;->h(Lcom/google/googlenav/aZ;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Lcom/google/googlenav/aZ;)I
    .locals 2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/googlenav/aZ;->f()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/googlenav/aZ;->b(I)Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->aJ()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public h()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/bk;->bz()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, Lbf/bk;->B:Z

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0, v0}, Lbf/bk;->b(Lcom/google/googlenav/ui/wizard/A;)Z

    goto :goto_0

    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/bk;->i(Z)V

    invoke-virtual {p0, v2, v3}, Lbf/bk;->c(ILjava/lang/Object;)V

    invoke-virtual {p0, v2}, Lbf/bk;->i(Z)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0, v2, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/bk;->b(I)V

    :cond_2
    invoke-virtual {p0, v2, v3}, Lbf/bk;->a(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/i;->aR()V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->i(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    invoke-virtual {p0, v2, v3}, Lbf/bk;->b(ILjava/lang/Object;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->Y()V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->b(I)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->h(Lbf/i;)V

    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0}, Lbf/am;->H()Lbf/i;

    move-result-object v0

    check-cast v0, Lbf/by;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lbf/by;->bG()V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lbf/bk;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->y()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x6 -> :sswitch_4
        0x7 -> :sswitch_2
        0x8 -> :sswitch_0
        0x9 -> :sswitch_1
        0xf -> :sswitch_5
        0x10 -> :sswitch_6
        0x11 -> :sswitch_3
        0x1a -> :sswitch_7
        0x1c -> :sswitch_8
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->R:Lcom/google/googlenav/layer/s;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/layer/s;->a(Lcom/google/googlenav/ai;)Law/g;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/i;

    invoke-direct {v0, p0}, Lbh/i;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected j(I)V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "m"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lbf/bk;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lbf/bk;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-virtual {p0, p1}, Lbf/bk;->b(I)V

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lbf/bk;->af()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lbf/bk;->a(ILjava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v2}, Lbf/bk;->a(ILjava/lang/Object;)V

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/K;->an()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/bk;->g:Lcom/google/googlenav/ui/view/d;

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lbf/bk;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public k(I)V
    .locals 2

    iget v0, p0, Lbf/bk;->C:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbf/am;->a(IZ)V

    iput p1, p0, Lbf/bk;->C:I

    iget v0, p0, Lbf/bk;->C:I

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbf/am;->a(IZ)V

    iget-object v0, p0, Lbf/bk;->t:Lbh/a;

    check-cast v0, Lbh/i;

    invoke-virtual {v0, p1}, Lbh/i;->c(I)V

    return-void
.end method

.method protected k(Z)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l(Lcom/google/googlenav/ai;)I
    .locals 3

    const/4 v0, -0x1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-direct {p0, p1}, Lbf/bk;->n(Lcom/google/googlenav/ai;)I

    move-result v2

    if-eq v2, v0, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->ap()I

    move-result v0

    sub-int v0, v2, v0

    invoke-direct {p0, v0}, Lbf/bk;->m(I)I

    move-result v0

    :cond_0
    return v0
.end method

.method protected m()V
    .locals 0

    invoke-super {p0}, Lbf/m;->m()V

    invoke-virtual {p0}, Lbf/bk;->bL()V

    return-void
.end method

.method public q()I
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lbf/m;->q()I

    move-result v0

    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbf/bk;->G:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    :cond_0
    :goto_0
    iget-object v1, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbf/bk;->I:Lcom/google/googlenav/ui/android/aq;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/android/aq;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    :cond_1
    return v0

    :cond_2
    iget-object v1, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbf/bk;->K:Lcom/google/googlenav/ui/view/d;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/view/d;->f()[I

    move-result-object v1

    aget v1, v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected t()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v0

    invoke-static {v0}, Lcom/google/googlenav/bM;->a(Lcom/google/googlenav/aZ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[SearchLayer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbf/bk;->bQ()Lcom/google/googlenav/aZ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/aZ;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected v()V
    .locals 2

    invoke-virtual {p0}, Lbf/bk;->bR()Lcom/google/googlenav/T;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/T;->b(LaN/B;)V

    return-void
.end method
