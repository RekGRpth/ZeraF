.class public Lbf/by;
.super Lbf/m;
.source "SourceFile"

# interfaces
.implements LaM/h;
.implements Lbf/k;


# instance fields
.field protected B:Ljava/lang/String;

.field private C:Lcom/google/googlenav/ui/wizard/dy;

.field private D:I


# direct methods
.method protected constructor <init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Lbf/m;-><init>(Lcom/google/googlenav/ui/s;LaN/p;LaN/u;Lcom/google/googlenav/ui/X;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbf/by;->C:Lcom/google/googlenav/ui/wizard/dy;

    iput v3, p0, Lbf/by;->D:I

    invoke-virtual {p1}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    invoke-interface {v0}, LaR/n;->e()LaR/u;

    move-result-object v0

    invoke-static {}, LaR/l;->a()LaR/l;

    move-result-object v1

    invoke-virtual {v1}, LaR/l;->b()LaR/aa;

    move-result-object v1

    new-instance v2, Lcom/google/googlenav/bz;

    invoke-direct {v2, v0, v1, p0}, Lcom/google/googlenav/bz;-><init>(LaR/u;LaR/aa;Lbf/k;)V

    iput-object v2, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-virtual {p0, v3}, Lbf/by;->i(Z)V

    return-void
.end method

.method private bK()V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lbf/bC;

    invoke-direct {v2, p0}, Lbf/bC;-><init>(Lbf/by;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method


# virtual methods
.method public C_()V
    .locals 0

    invoke-direct {p0}, Lbf/by;->bK()V

    return-void
.end method

.method public D_()V
    .locals 0

    invoke-direct {p0}, Lbf/by;->bK()V

    return-void
.end method

.method public E_()V
    .locals 0

    return-void
.end method

.method public F()I
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/by;->az()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lbf/by;->D:I

    if-gez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, v0}, Lbf/by;->i(Z)V

    iget v0, p0, Lbf/by;->D:I

    goto :goto_0
.end method

.method public M_()V
    .locals 0

    invoke-direct {p0}, Lbf/by;->bK()V

    return-void
.end method

.method public N_()V
    .locals 0

    return-void
.end method

.method protected O()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected P()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected X()Z
    .locals 3

    const/4 v0, 0x0

    invoke-super {p0}, Lbf/m;->X()Z

    move-result v1

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/googlenav/bz;->a(Z)Z

    move-result v2

    if-nez v1, :cond_0

    if-eqz v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected a(Lcom/google/googlenav/F;)V
    .locals 1

    iput-object p1, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lbf/by;->b(I)V

    return-void
.end method

.method public a(Lcom/google/googlenav/ui/r;Ljava/lang/String;)V
    .locals 4

    invoke-virtual {p0, p2}, Lbf/by;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0}, Lbf/by;->ar()Lcom/google/googlenav/F;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/googlenav/F;->b(I)Lcom/google/googlenav/E;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    iget-object v2, p0, Lbf/by;->c:LaN/p;

    iget-object v3, p0, Lbf/by;->e:Landroid/graphics/Point;

    invoke-virtual {v2, v1, v3}, LaN/p;->a(LaN/B;Landroid/graphics/Point;)V

    invoke-virtual {p0, v0}, Lbf/by;->c(I)Lcom/google/googlenav/e;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lbf/by;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v0, p1, v1, v2}, Lcom/google/googlenav/e;->a(Lcom/google/googlenav/e;Lcom/google/googlenav/ui/r;II)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbf/by;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bz;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-virtual {p0, v0}, Lbf/by;->b(I)V

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v0

    invoke-virtual {v0, p0}, Lbf/am;->e(Lbf/i;)V

    invoke-virtual {p0}, Lbf/by;->bH()V

    iput v1, p0, Lbf/by;->D:I

    if-eqz p2, :cond_1

    const/16 v0, 0xe

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/by;->a(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lbf/by;->bJ()LaR/n;

    move-result-object v0

    const-string v1, "s"

    invoke-interface {v0, v1, p1}, LaR/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(IILjava/lang/Object;)Z
    .locals 2

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lbf/m;->a(IILjava/lang/Object;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->an()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lbf/by;->an()Z

    invoke-virtual {p0, v0}, Lbf/by;->b(Z)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v1}, Lcom/google/googlenav/F;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v1}, Lbf/by;->a(Ljava/lang/Object;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lbf/by;->W()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3f9 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/google/googlenav/ui/view/t;)Z
    .locals 2

    iget-object v0, p0, Lbf/by;->g:Lcom/google/googlenav/ui/view/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/by;->g:Lcom/google/googlenav/ui/view/d;

    if-ne p1, v0, :cond_0

    const/16 v0, 0x9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/by;->a(ILjava/lang/Object;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lbf/m;->a(Lcom/google/googlenav/ui/view/t;)Z

    move-result v0

    goto :goto_0
.end method

.method public aE()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aU()V
    .locals 1

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/bz;->a()V

    invoke-super {p0}, Lbf/m;->aU()V

    return-void
.end method

.method public aX()V
    .locals 1

    invoke-virtual {p0}, Lbf/by;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbf/by;->B:Ljava/lang/String;

    :cond_0
    invoke-super {p0}, Lbf/m;->aX()V

    return-void
.end method

.method protected am()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/googlenav/bz;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/by;->B()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LaR/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected aq()V
    .locals 4

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "stars"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    return-void
.end method

.method public av()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public b(Lcom/google/googlenav/E;)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;)I
    .locals 1

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected bC()I
    .locals 2

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 v0, -0x2

    :cond_0
    return v0
.end method

.method protected bG()V
    .locals 1

    invoke-virtual {p0}, Lbf/by;->bC()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/by;->j(I)V

    return-void
.end method

.method public bH()V
    .locals 2

    invoke-virtual {p0}, Lbf/by;->s()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->R()V

    iget-object v1, p0, Lbf/by;->d:LaN/u;

    invoke-virtual {v1, v0}, LaN/u;->c(LaN/B;)V

    :cond_0
    return-void
.end method

.method public bI()Lcom/google/googlenav/bz;
    .locals 1

    invoke-virtual {p0}, Lbf/by;->ar()Lcom/google/googlenav/F;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/bz;

    return-object v0
.end method

.method public bJ()LaR/n;
    .locals 1

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v0

    return-object v0
.end method

.method public bj()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public bk()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected bv()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c(Lcom/google/googlenav/E;)I
    .locals 1

    invoke-virtual {p0, p1}, Lbf/by;->f(Lcom/google/googlenav/E;)I

    move-result v0

    if-eqz v0, :cond_0

    div-int/lit8 v0, v0, 0x2

    rsub-int/lit8 v0, v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Lbf/i;)V
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v1

    new-instance v2, Lbf/bz;

    invoke-direct {v2, p0}, Lbf/bz;-><init>(Lbf/by;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/googlenav/bz;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1, p0}, Lbf/am;->e(Lbf/i;)V

    invoke-virtual {p0, v0}, Lbf/by;->b(I)V

    invoke-virtual {p0}, Lbf/by;->an()Z

    invoke-virtual {p0}, Lbf/by;->bH()V

    :cond_0
    return-void
.end method

.method public e(ILjava/lang/Object;)V
    .locals 4

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    const-string v2, "stars"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/jv;->a(Lcom/google/googlenav/ui/s;Ljava/lang/String;Z)V

    return-void
.end method

.method protected f(Lat/a;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0}, Lbf/by;->ah()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lbf/by;->ag()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lbf/by;->a(ILjava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lat/a;->c()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/ui/wizard/z;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbf/by;->h()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lbf/m;->g(I)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public h()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lbf/by;->bz()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v3, p0, Lbf/by;->C:Lcom/google/googlenav/ui/wizard/dy;

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ap()Lcom/google/googlenav/ui/wizard/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/z;->d()Lcom/google/googlenav/ui/wizard/A;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->a()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p0}, Lbf/by;->n()V

    goto :goto_0

    :sswitch_0
    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lbf/by;->b(ILjava/lang/Object;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/A;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lbf/by;->j(I)V

    goto :goto_0

    :sswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbf/by;->i(Z)V

    invoke-virtual {p0}, Lbf/by;->l()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lbf/by;->n()V

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->j()V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->av()Lcom/google/googlenav/ui/wizard/jv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jv;->L()Lcom/google/googlenav/ui/wizard/jC;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/ui/wizard/jC;->x()Lcom/google/googlenav/ui/wizard/dg;

    move-result-object v0

    const/16 v1, 0x32c

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/googlenav/ui/wizard/dg;->a(IILjava/lang/Object;)Z

    invoke-virtual {p0}, Lbf/by;->n()V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lbf/by;->Z()V

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    const-string v1, "15"

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/s;->i(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p0}, Lbf/by;->Z()V

    iget-object v0, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->y()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_5
        0x7 -> :sswitch_1
        0x9 -> :sswitch_0
        0xe -> :sswitch_2
        0x10 -> :sswitch_3
        0x16 -> :sswitch_4
        0x1c -> :sswitch_6
    .end sparse-switch
.end method

.method protected h(Lcom/google/googlenav/ai;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/bz;->b()Lcom/google/googlenav/by;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/googlenav/by;->f()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/googlenav/ai;->f()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1}, Lcom/google/googlenav/by;->i()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lbf/by;->bI()Lcom/google/googlenav/bz;

    move-result-object v0

    new-instance v1, Lbf/bA;

    invoke-direct {v1, p0}, Lbf/bA;-><init>(Lbf/by;)V

    invoke-virtual {v0, v1}, Lcom/google/googlenav/bz;->a(Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected i()Lbh/a;
    .locals 1

    new-instance v0, Lbh/j;

    invoke-direct {v0, p0}, Lbh/j;-><init>(Lbf/i;)V

    return-object v0
.end method

.method protected j(I)V
    .locals 2

    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->f()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0, p1}, Lcom/google/googlenav/F;->a(I)V

    invoke-virtual {p0}, Lbf/by;->an()Z

    invoke-virtual {p0}, Lbf/by;->bH()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lbf/by;->a(ILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected m()V
    .locals 3

    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    check-cast v0, Lcom/google/googlenav/ai;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v2}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v2

    invoke-interface {v2, v1}, LaR/n;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lbf/by;->b:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->am()LaR/n;

    move-result-object v1

    const-string v2, "o"

    invoke-virtual {v0}, Lcom/google/googlenav/ai;->B()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, LaR/n;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Lbf/m;->m()V

    goto :goto_0
.end method

.method protected n()V
    .locals 1

    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->c()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-super {p0}, Lbf/m;->n()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lbf/by;->Z()V

    goto :goto_0
.end method

.method public s()Lcom/google/googlenav/E;
    .locals 1

    iget-object v0, p0, Lbf/by;->f:Lcom/google/googlenav/F;

    invoke-interface {v0}, Lcom/google/googlenav/F;->e()Lcom/google/googlenav/E;

    move-result-object v0

    return-object v0
.end method
