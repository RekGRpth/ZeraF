.class public Lbf/aF;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/E;


# instance fields
.field private final a:LaN/B;

.field private b:B

.field private final c:I


# direct methods
.method public constructor <init>(LaN/B;)V
    .locals 2

    const/16 v0, 0x15

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lbf/aF;-><init>(LaN/B;BI)V

    return-void
.end method

.method public constructor <init>(LaN/B;BI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbf/aF;->a:LaN/B;

    iput-byte p2, p0, Lbf/aF;->b:B

    iput p3, p0, Lbf/aF;->c:I

    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lbf/aF;->a:LaN/B;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    return-object v0
.end method

.method public c()B
    .locals 1

    iget-byte v0, p0, Lbf/aF;->b:B

    return v0
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "MeasurementLayer"

    iget-object v0, p0, Lbf/aF;->a:LaN/B;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbf/aF;->a:LaN/B;

    invoke-virtual {v0}, LaN/B;->h()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, 0x10

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lbf/aF;->c:I

    return v0
.end method
