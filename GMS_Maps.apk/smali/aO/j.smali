.class public final enum LaO/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:LaO/j;

.field public static final enum b:LaO/j;

.field public static final enum c:LaO/j;

.field private static final synthetic d:[LaO/j;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, LaO/j;

    const-string v1, "ROUND"

    invoke-direct {v0, v1, v2}, LaO/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaO/j;->a:LaO/j;

    new-instance v0, LaO/j;

    const-string v1, "FLOOR"

    invoke-direct {v0, v1, v3}, LaO/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaO/j;->b:LaO/j;

    new-instance v0, LaO/j;

    const-string v1, "CEILING"

    invoke-direct {v0, v1, v4}, LaO/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaO/j;->c:LaO/j;

    const/4 v0, 0x3

    new-array v0, v0, [LaO/j;

    sget-object v1, LaO/j;->a:LaO/j;

    aput-object v1, v0, v2

    sget-object v1, LaO/j;->b:LaO/j;

    aput-object v1, v0, v3

    sget-object v1, LaO/j;->c:LaO/j;

    aput-object v1, v0, v4

    sput-object v0, LaO/j;->d:[LaO/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaO/j;
    .locals 1

    const-class v0, LaO/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaO/j;

    return-object v0
.end method

.method public static values()[LaO/j;
    .locals 1

    sget-object v0, LaO/j;->d:[LaO/j;

    invoke-virtual {v0}, [LaO/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaO/j;

    return-object v0
.end method
