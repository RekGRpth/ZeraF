.class public LaO/e;
.super LaN/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/maps/driveabout/vector/bm;


# instance fields
.field private final c:Lcom/google/android/maps/driveabout/vector/bk;

.field private final d:LaO/a;

.field private final e:LC/a;

.field private final f:[F

.field private final g:Lo/T;

.field private final h:Lo/T;

.field private final i:Lcom/google/googlenav/android/aa;

.field private j:F

.field private k:LaO/i;

.field private final l:Lcom/google/android/maps/driveabout/vector/aE;


# direct methods
.method public constructor <init>(Lcom/google/android/maps/driveabout/vector/bk;LaO/a;LaN/H;Lcom/google/googlenav/android/aa;)V
    .locals 5

    const/high16 v4, 0x3f800000

    invoke-direct {p0}, LaN/u;-><init>()V

    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, LaO/e;->f:[F

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LaO/e;->g:Lo/T;

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, LaO/e;->h:Lo/T;

    iput v4, p0, LaO/e;->j:F

    new-instance v0, LaO/f;

    invoke-direct {v0, p0}, LaO/f;-><init>(LaO/e;)V

    iput-object v0, p0, LaO/e;->l:Lcom/google/android/maps/driveabout/vector/aE;

    iput-object p1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {p1, p0}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/bm;)V

    iput-object p2, p0, LaO/e;->d:LaO/a;

    iput-object p4, p0, LaO/e;->i:Lcom/google/googlenav/android/aa;

    invoke-static {p3, v4}, LaO/e;->a(LaN/H;F)LC/b;

    move-result-object v0

    new-instance v1, LC/a;

    invoke-virtual {p2}, LaO/a;->t()I

    move-result v2

    invoke-virtual {p2}, LaO/a;->s()I

    move-result v3

    invoke-direct {v1, v0, v2, v3, v4}, LC/a;-><init>(LC/b;IIF)V

    iput-object v1, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->l:Lcom/google/android/maps/driveabout/vector/aE;

    invoke-virtual {p1, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/aE;)V

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V

    invoke-virtual {p2}, LaO/a;->t()I

    move-result v0

    invoke-virtual {p2}, LaO/a;->s()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LaO/e;->c(II)V

    sget-object v0, Lcom/google/googlenav/android/A;->a:Lcom/google/googlenav/android/A;

    invoke-virtual {p1, v0}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lcom/google/android/maps/driveabout/vector/bq;)V

    return-void
.end method

.method public static a(LaN/Y;F)F
    .locals 1

    invoke-virtual {p0}, LaN/Y;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    return v0
.end method

.method static a(LaN/H;F)LC/b;
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, LaN/H;->a()LaN/B;

    move-result-object v1

    new-instance v0, LC/b;

    invoke-static {v1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    invoke-virtual {p0}, LaN/H;->b()LaN/Y;

    move-result-object v2

    invoke-static {v2, p1}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    return-object v0
.end method

.method private declared-synchronized a(IILC/b;)LaN/B;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, p3}, LC/a;->a(LC/b;)V

    iget-object v0, p0, LaO/e;->e:LC/a;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, LC/a;->d(FF)Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LC/b;)LaN/B;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, p1}, LC/a;->a(LC/b;)V

    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0}, LC/a;->h()Lo/T;

    move-result-object v0

    invoke-static {v0}, LR/e;->b(Lo/T;)LaN/B;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(LaO/e;)LaO/a;
    .locals 1

    iget-object v0, p0, LaO/e;->d:LaO/a;

    return-object v0
.end method

.method static synthetic a(LaO/e;LaO/i;)LaO/i;
    .locals 0

    iput-object p1, p0, LaO/e;->k:LaO/i;

    return-object p1
.end method

.method private declared-synchronized a(LaN/B;LaN/Y;Z)V
    .locals 12

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    invoke-virtual {v4}, LC/b;->a()F

    move-result v0

    if-eqz p2, :cond_a

    iget v1, p0, LaO/e;->j:F

    invoke-static {p2, v1}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    if-eq v1, v3, :cond_a

    :goto_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, v4}, LC/a;->a(LC/b;)V

    iget v0, p0, LaO/e;->a:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, LaO/e;->b:I

    div-int/lit8 v1, v1, 0x2

    iget-object v3, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v3}, LR/e;->a(LaN/B;Lo/T;)V

    iget-object v3, p0, LaO/e;->e:LC/a;

    iget-object v5, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v3, v5}, LC/a;->b(Lo/T;)[I

    move-result-object v3

    const/4 v5, 0x0

    aget v5, v3, v5

    sub-int v7, v5, v0

    const/4 v0, 0x1

    aget v0, v3, v0

    sub-int v6, v0, v1

    int-to-double v0, v7

    int-to-double v8, v7

    mul-double/2addr v0, v8

    int-to-double v8, v6

    int-to-double v10, v6

    mul-double/2addr v8, v10

    add-double/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v8, v0

    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/b;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :cond_1
    const/4 v9, -0x1

    if-eqz p3, :cond_7

    :try_start_1
    iget-object v1, p0, LaO/e;->k:LaO/i;

    if-eqz v1, :cond_2

    iget-object v1, p0, LaO/e;->k:LaO/i;

    invoke-virtual {v1, v0}, LaO/i;->a(LC/b;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-direct {p0, v0, v8}, LaO/e;->a(LC/b;I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget-object v3, p0, LaO/e;->e:LC/a;

    invoke-virtual {v3}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    move v5, v1

    :goto_2
    iget-object v1, p0, LaO/e;->e:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_4

    const/4 v3, 0x1

    :goto_3
    const/4 v4, 0x1

    move-object v1, p0

    move v2, v5

    move v5, v7

    invoke-virtual/range {v1 .. v6}, LaO/e;->a(ZZZII)V

    new-instance v1, LaO/i;

    invoke-direct {v1, v0}, LaO/i;-><init>(LC/b;)V

    iput-object v1, p0, LaO/e;->k:LaO/i;

    iget-object v0, p0, LaO/e;->k:LaO/i;

    invoke-virtual {v0}, LaO/i;->b()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v1}, LC/b;->a()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_6

    const/4 v0, -0x1

    :goto_5
    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v2, p0, LaO/e;->k:LaO/i;

    invoke-virtual {v1, v2, v0, v9}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    const/4 v1, 0x0

    move v5, v1

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    :try_start_2
    invoke-static {v8}, LaO/e;->b(I)I

    move-result v0

    goto :goto_5

    :cond_7
    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget-object v3, p0, LaO/e;->e:LC/a;

    invoke-virtual {v3}, LC/a;->h()Lo/T;

    move-result-object v3

    invoke-virtual {v1, v3}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    move v5, v1

    :goto_6
    iget-object v1, p0, LaO/e;->e:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    cmpl-float v1, v2, v1

    if-eqz v1, :cond_9

    const/4 v3, 0x1

    :goto_7
    const/4 v4, 0x1

    move-object v1, p0

    move v2, v5

    move v5, v7

    invoke-virtual/range {v1 .. v6}, LaO/e;->a(ZZZII)V

    const/4 v1, -0x1

    iget-object v2, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v2, v0, v1, v9}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :cond_8
    const/4 v1, 0x0

    move v5, v1

    goto :goto_6

    :cond_9
    const/4 v3, 0x0

    goto :goto_7

    :cond_a
    move v2, v0

    goto/16 :goto_0
.end method

.method private declared-synchronized a(Lo/T;LC/b;Landroid/graphics/Point;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0, p2}, LC/a;->a(LC/b;)V

    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->f:[F

    invoke-virtual {v0, p1, v1}, LC/a;->a(Lo/T;[F)V

    iget-object v0, p0, LaO/e;->f:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, LaO/e;->f:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/graphics/Point;->set(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(LC/b;I)Z
    .locals 2

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {p1, v0}, LC/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LC/b;->a()F

    move-result v0

    iget-object v1, p0, LaO/e;->e:LC/a;

    invoke-virtual {v1}, LC/a;->r()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const/16 v0, 0xf

    if-ge p2, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LC/b;)LaN/Y;
    .locals 2

    invoke-virtual {p1}, LC/b;->a()F

    move-result v0

    sget-object v1, LaO/j;->a:LaO/j;

    invoke-virtual {p0, v0, v1}, LaO/e;->a(FLaO/j;)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(LaO/e;)Lcom/google/googlenav/android/aa;
    .locals 1

    iget-object v0, p0, LaO/e;->i:Lcom/google/googlenav/android/aa;

    return-object v0
.end method

.method private g()I
    .locals 5

    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0}, LC/a;->C()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->g()Lo/T;

    move-result-object v1

    invoke-virtual {v1}, Lo/T;->a()I

    move-result v1

    invoke-virtual {v0}, Lo/aQ;->f()Lo/T;

    move-result-object v2

    invoke-virtual {v2}, Lo/T;->a()I

    move-result v2

    invoke-virtual {v0}, Lo/aQ;->d()Lo/T;

    move-result-object v3

    invoke-virtual {v3}, Lo/T;->a()I

    move-result v3

    invoke-virtual {v0}, Lo/aQ;->e()Lo/T;

    move-result-object v0

    invoke-virtual {v0}, Lo/T;->a()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v4, v0

    return v0
.end method

.method private h()I
    .locals 3

    iget-object v0, p0, LaO/e;->e:LC/a;

    invoke-virtual {v0}, LC/a;->C()Lo/aQ;

    move-result-object v0

    invoke-virtual {v0}, Lo/aQ;->a()Lo/aR;

    move-result-object v0

    invoke-virtual {v0}, Lo/aR;->e()I

    move-result v0

    iget-object v1, p0, LaO/e;->g:Lo/T;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lo/T;->d(II)V

    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v0}, Lo/T;->c()I

    move-result v0

    return v0
.end method


# virtual methods
.method public declared-synchronized a(ILaN/B;)F
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p2, v0}, LR/e;->a(LaN/B;Lo/T;)V

    const/4 v0, 0x1

    iget-object v1, p0, LaO/e;->e:LC/a;

    iget-object v2, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v1, v2, v0}, LC/a;->a(Lo/T;Z)F

    move-result v0

    int-to-float v1, p1

    iget-object v2, p0, LaO/e;->e:LC/a;

    iget-object v3, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v3}, Lo/T;->e()D

    move-result-wide v3

    double-to-float v3, v3

    invoke-virtual {v2, v3, v0}, LC/a;->b(FF)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    mul-float/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    invoke-direct {p0}, LaO/e;->g()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/B;)I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lo/T;)F

    move-result v0

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/H;)I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->j:F

    invoke-static {p1, v1}, LaO/e;->a(LaN/H;F)LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    invoke-direct {p0}, LaO/e;->g()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(LaN/B;LaN/Y;II)LaN/B;
    .locals 6

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    const/4 v5, 0x0

    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget v2, p0, LaO/e;->j:F

    invoke-static {p2, v2}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    invoke-direct {p0, p3, p4, v0}, LaO/e;->a(IILC/b;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method protected a(FLaO/j;)LaN/Y;
    .locals 3

    const/high16 v0, 0x3f800000

    add-float/2addr v0, p1

    sget-object v1, LaO/h;->a:[I

    invoke-virtual {p2}, LaO/j;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    :goto_0
    const/16 v1, 0x16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    return-object v0

    :pswitch_0
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0

    :pswitch_1
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized a(F)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->a:I

    iget v2, p0, LaO/e;->b:I

    invoke-virtual {v0, v1, v2, p1}, LC/a;->a(IIF)V

    iput p1, p0, LaO/e;->j:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, LaO/e;->d:LaO/a;

    invoke-virtual {v0, p1}, LaO/a;->b(I)V

    return-void
.end method

.method public declared-synchronized a(II)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/bk;->b(FF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LaN/B;LaN/Y;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, LaO/e;->a(LaN/B;LaN/Y;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    iget-object v0, p0, LaO/e;->h:Lo/T;

    invoke-static {p3, v0}, LR/e;->a(LaN/B;Lo/T;)V

    const/4 v5, 0x0

    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget v2, p0, LaO/e;->j:F

    invoke-static {p2, v2}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    iget-object v1, p0, LaO/e;->h:Lo/T;

    invoke-direct {p0, v1, v0, p4}, LaO/e;->a(Lo/T;LC/b;Landroid/graphics/Point;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(LaN/B;Landroid/graphics/Point;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    iget-object v0, p0, LaO/e;->g:Lo/T;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, LaO/e;->a(Lo/T;LC/b;Landroid/graphics/Point;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized a(LaN/Y;II)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaO/e;->j:F

    invoke-static {p1, v0}, LaO/e;->a(LaN/Y;F)F

    move-result v0

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v1}, LC/b;->a()F

    move-result v1

    sub-float/2addr v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, LaO/e;->a(ZZZ)V

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    int-to-float v2, p2

    int-to-float v3, p3

    const/16 v4, 0x14a

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/maps/driveabout/vector/bk;->a(FFFI)F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lo/D;)V
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->a(Lo/D;)V

    return-void
.end method

.method public a(ZZZII)V
    .locals 0

    invoke-super/range {p0 .. p5}, LaN/u;->a(ZZZII)V

    return-void
.end method

.method public a([LaN/B;IIILaN/Y;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    aget-object v0, p1, p3

    invoke-virtual {p0, v0, p5}, LaO/e;->d(LaN/B;LaN/Y;)V

    goto :goto_0
.end method

.method public declared-synchronized b()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    invoke-direct {p0}, LaO/e;->h()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(LaN/H;)I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->j:F

    invoke-static {p1, v1}, LaO/e;->a(LaN/H;F)LC/b;

    move-result-object v1

    invoke-virtual {v0, v1}, LC/a;->a(LC/b;)V

    invoke-direct {p0}, LaO/e;->h()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(F)V
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/bk;->c(F)V

    return-void
.end method

.method protected declared-synchronized b(LaN/B;LaN/Y;)V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, LaO/e;->a(LaN/B;LaN/Y;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()LaN/B;
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-direct {p0, v0}, LaO/e;->a(LC/b;)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized c(II)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, LaN/u;->c(II)V

    iget-object v0, p0, LaO/e;->e:LC/a;

    iget v1, p0, LaO/e;->j:F

    invoke-virtual {v0, p1, p2, v1}, LC/a;->a(IIF)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized c(LaN/B;LaN/Y;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v4

    iget-object v0, p0, LaO/e;->g:Lo/T;

    invoke-static {p1, v0}, LR/e;->a(LaN/B;Lo/T;)V

    new-instance v0, LC/b;

    iget-object v1, p0, LaO/e;->g:Lo/T;

    iget v2, p0, LaO/e;->j:F

    invoke-static {p2, v2}, LaO/e;->a(LaN/Y;F)F

    move-result v2

    invoke-virtual {v4}, LC/b;->d()F

    move-result v3

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/maps/driveabout/vector/bk;->a(LC/c;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()LaN/Y;
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-direct {p0, v0}, LaO/e;->b(LC/b;)LaN/Y;

    move-result-object v0

    return-object v0
.end method

.method public e()F
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->e()F

    move-result v0

    return v0
.end method

.method public e(II)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object v0, p0

    move v3, v1

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, LaO/e;->a(ZZZII)V

    return-void
.end method

.method public f()LaN/H;
    .locals 6

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v2

    iget-object v0, p0, LaO/e;->d:LaO/a;

    invoke-virtual {v0}, LaO/a;->b()LaN/H;

    move-result-object v5

    new-instance v0, LaN/H;

    invoke-direct {p0, v2}, LaO/e;->a(LC/b;)LaN/B;

    move-result-object v1

    invoke-direct {p0, v2}, LaO/e;->b(LC/b;)LaN/Y;

    move-result-object v2

    invoke-virtual {v5}, LaN/H;->c()I

    move-result v3

    invoke-virtual {v5}, LaN/H;->g()Z

    move-result v4

    invoke-virtual {v5}, LaN/H;->h()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, LaN/H;-><init>(LaN/B;LaN/Y;IZZ)V

    return-object v0
.end method

.method public f(II)V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    move-object v0, p0

    move v3, v2

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, LaO/e;->a(ZZZII)V

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->h()V

    return-void
.end method

.method public j()Z
    .locals 3

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    sget-object v1, LaO/j;->b:LaO/j;

    invoke-virtual {p0, v0, v1}, LaO/e;->a(FLaO/j;)LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->c()LaN/Y;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LaN/Y;->a()I

    move-result v1

    invoke-virtual {p0}, LaO/e;->c()LaN/B;

    move-result-object v2

    invoke-virtual {p0, v2}, LaO/e;->a(LaN/B;)I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-static {}, Lcom/google/android/maps/driveabout/vector/bk;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->f()LC/b;

    move-result-object v0

    invoke-virtual {v0}, LC/b;->a()F

    move-result v0

    sget-object v1, LaO/j;->c:LaO/j;

    invoke-virtual {p0, v0, v1}, LaO/e;->a(FLaO/j;)LaN/Y;

    move-result-object v1

    invoke-virtual {v1}, LaN/Y;->d()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v1}, Lcom/google/android/maps/driveabout/vector/bk;->e()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    iget-object v0, p0, LaO/e;->c:Lcom/google/android/maps/driveabout/vector/bk;

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/vector/bk;->i()Z

    move-result v0

    return v0
.end method

.method public m()V
    .locals 0

    invoke-super {p0}, LaN/u;->m()V

    return-void
.end method
