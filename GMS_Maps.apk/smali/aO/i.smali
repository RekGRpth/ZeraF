.class LaO/i;
.super Lw/a;
.source "SourceFile"


# instance fields
.field private b:Z


# direct methods
.method public constructor <init>(LC/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lw/a;-><init>(LC/b;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(LC/a;)LC/c;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaO/i;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaO/i;->a:LC/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaO/i;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LC/b;)Z
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaO/i;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    new-instance v0, LC/b;

    invoke-virtual {p1}, LC/b;->c()Lo/T;

    move-result-object v1

    iget-object v2, p0, LaO/i;->a:LC/b;

    invoke-virtual {v2}, LC/b;->a()F

    move-result v2

    iget-object v3, p0, LaO/i;->a:LC/b;

    invoke-virtual {v3}, LC/b;->d()F

    move-result v3

    iget-object v4, p0, LaO/i;->a:LC/b;

    invoke-virtual {v4}, LC/b;->e()F

    move-result v4

    iget-object v5, p0, LaO/i;->a:LC/b;

    invoke-virtual {v5}, LC/b;->f()F

    move-result v5

    invoke-direct/range {v0 .. v5}, LC/b;-><init>(Lo/T;FFFF)V

    iput-object v0, p0, LaO/i;->a:LC/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaO/i;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
