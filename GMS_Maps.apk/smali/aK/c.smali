.class public LaK/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field protected b:Z

.field c:Lo/D;

.field d:Lo/D;

.field private e:I

.field private f:Ljava/lang/Object;

.field private g:Z

.field private h:I

.field private i:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, LaK/c;->e:I

    iput v1, p0, LaK/c;->a:I

    const/4 v0, 0x1

    iput-boolean v0, p0, LaK/c;->b:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaK/c;->f:Ljava/lang/Object;

    iput-boolean v1, p0, LaK/c;->g:Z

    iput-object v2, p0, LaK/c;->c:Lo/D;

    iput-object v2, p0, LaK/c;->d:Lo/D;

    iput v1, p0, LaK/c;->h:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaK/c;->i:Ljava/lang/Object;

    return-void
.end method

.method private i()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LaK/c;->f:Ljava/lang/Object;

    monitor-enter v2

    const/4 v3, 0x0

    :try_start_0
    iput v3, p0, LaK/c;->a:I

    iget-boolean v3, p0, LaK/c;->b:Z

    if-eqz v3, :cond_0

    iget v1, p0, LaK/c;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LaK/c;->e:I

    monitor-exit v2

    :goto_0
    return v0

    :cond_0
    iget v3, p0, LaK/c;->e:I

    const/4 v4, 0x4

    if-lt v3, v4, :cond_1

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget v0, p0, LaK/c;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaK/c;->e:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method private j()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, LaK/c;->f:Ljava/lang/Object;

    monitor-enter v2

    const/4 v3, 0x0

    :try_start_0
    iput v3, p0, LaK/c;->e:I

    iget-boolean v3, p0, LaK/c;->b:Z

    if-eqz v3, :cond_0

    iget v1, p0, LaK/c;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LaK/c;->a:I

    monitor-exit v2

    :goto_0
    return v0

    :cond_0
    iget v3, p0, LaK/c;->a:I

    const/4 v4, 0x2

    if-lt v3, v4, :cond_1

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget v0, p0, LaK/c;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaK/c;->a:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v1, p0, LaK/c;->f:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaK/c;->b:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LaK/c;->g()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lo/D;)V
    .locals 1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, LaK/c;->b(Lo/D;)V

    invoke-direct {p0}, LaK/c;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaK/c;->g()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, LaK/c;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaK/c;->h()V

    goto :goto_0
.end method

.method public b()V
    .locals 0

    invoke-virtual {p0}, LaK/c;->h()V

    return-void
.end method

.method b(Lo/D;)V
    .locals 3

    invoke-virtual {p0}, LaK/c;->f()Lo/D;

    move-result-object v0

    iget-object v1, p0, LaK/c;->i:Ljava/lang/Object;

    monitor-enter v1

    if-nez v0, :cond_0

    :try_start_0
    iput-object p1, p0, LaK/c;->c:Lo/D;

    :goto_0
    iput-object p1, p0, LaK/c;->d:Lo/D;

    monitor-exit v1

    return-void

    :cond_0
    invoke-virtual {v0, p1}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, LaK/c;->h:I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LaK/c;->d:Lo/D;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaK/c;->d:Lo/D;

    invoke-virtual {v0, p1}, Lo/D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, LaK/c;->h:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_2

    const/4 v0, 0x0

    iput v0, p0, LaK/c;->h:I

    iput-object p1, p0, LaK/c;->c:Lo/D;

    goto :goto_0

    :cond_2
    iget v0, p0, LaK/c;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaK/c;->h:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    iput v0, p0, LaK/c;->h:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v1, p0, LaK/c;->f:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LaK/c;->b:Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, LaK/c;->g:Z

    return v0
.end method

.method public e()Z
    .locals 2

    iget-object v1, p0, LaK/c;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, LaK/c;->b:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()Lo/D;
    .locals 2

    iget-object v1, p0, LaK/c;->i:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaK/c;->c:Lo/D;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method g()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, LaK/c;->g:Z

    return-void
.end method

.method h()V
    .locals 3

    const/4 v0, 0x0

    iput-boolean v0, p0, LaK/c;->g:Z

    iget-object v1, p0, LaK/c;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, LaK/c;->e:I

    const/4 v2, 0x4

    if-lt v0, v2, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaK/c;->b:Z

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
