.class public LaK/h;
.super LaH/a;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# static fields
.field private static i:LaK/e;


# instance fields
.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LaH/F;)V
    .locals 1

    invoke-direct {p0, p1, p2}, LaH/a;-><init>(Landroid/content/Context;LaH/F;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaK/h;->j:Z

    new-instance v0, LaK/e;

    invoke-direct {v0, p0, p1}, LaK/e;-><init>(LaK/h;Landroid/content/Context;)V

    sput-object v0, LaK/h;->i:LaK/e;

    return-void
.end method


# virtual methods
.method protected declared-synchronized b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LaH/a;->b()V

    iget-boolean v0, p0, LaK/h;->j:Z

    if-eqz v0, :cond_0

    sget-object v0, LaK/h;->i:LaK/e;

    invoke-virtual {v0}, LaK/e;->b()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaK/h;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized c()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaK/h;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, LaK/h;->j:Z

    if-nez v0, :cond_0

    sget-object v0, LaK/h;->i:LaK/e;

    invoke-virtual {v0}, LaK/e;->a()V

    invoke-super {p0}, LaH/a;->c()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LaK/h;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-super {p0}, LaH/a;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected c(Landroid/location/Location;)V
    .locals 0

    invoke-virtual {p0, p1}, LaK/h;->a(Landroid/location/Location;)V

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, LaK/h;->d:Z

    if-nez v0, :cond_1

    invoke-static {p1}, LaK/h;->b(Landroid/location/Location;)Lo/D;

    move-result-object v0

    sget-object v1, LaK/h;->i:LaK/e;

    invoke-virtual {v1, p1, v0}, LaK/e;->a(Landroid/location/Location;Lo/D;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, LaK/h;->c(Landroid/location/Location;)V

    goto :goto_0
.end method
