.class Laz/e;
.super Law/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Laz/c;


# direct methods
.method private constructor <init>(Laz/c;)V
    .locals 0

    iput-object p1, p0, Laz/e;->a:Laz/c;

    invoke-direct {p0}, Law/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Laz/c;Laz/d;)V
    .locals 0

    invoke-direct {p0, p1}, Laz/e;-><init>(Laz/c;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bH;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Laz/e;->a:Laz/c;

    invoke-static {v2}, Laz/c;->a(Laz/c;)[Laz/b;

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const/4 v2, 0x1

    iget-object v3, p0, Laz/e;->a:Laz/c;

    invoke-static {v3}, Laz/c;->a(Laz/c;)[Laz/b;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-interface {v3}, Laz/b;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v6, 0x1

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/bH;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    iget-object v0, p0, Laz/e;->a:Laz/c;

    invoke-static {v0}, Laz/c;->b(Laz/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return v6

    :cond_0
    invoke-virtual {v1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    iget-object v0, p0, Laz/e;->a:Laz/c;

    invoke-static {v0}, Laz/c;->c(Laz/c;)Ljava/util/Hashtable;

    move-result-object v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v4

    iget-object v5, p0, Laz/e;->a:Laz/c;

    invoke-static {v5, v4}, Laz/c;->a(Laz/c;I)Laz/b;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v4}, Laz/b;->d()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Laz/e;->a:Laz/c;

    invoke-static {v5, v4}, Laz/c;->a(Laz/c;Laz/b;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laz/b;

    iget-object v2, p0, Laz/e;->a:Laz/c;

    invoke-static {v2}, Laz/c;->d(Laz/c;)Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Laz/b;->c()V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Laz/e;->a:Laz/c;

    invoke-static {v0}, Laz/c;->e(Laz/c;)V

    goto :goto_0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x48

    return v0
.end method
