.class public Lav/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I

.field private static b:I


# instance fields
.field private c:Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;

.field private final d:Lcom/google/googlenav/ui/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xfa0

    sput v0, Lav/a;->a:I

    const/16 v0, 0xf

    sput v0, Lav/a;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/ui/s;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    return-void
.end method

.method static synthetic a(Lav/a;)Lcom/google/googlenav/ui/s;
    .locals 1

    iget-object v0, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    return-object v0
.end method

.method static synthetic a(Lav/a;Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;)Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;
    .locals 0

    iput-object p1, p0, Lav/a;->c:Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;

    return-object p1
.end method

.method static synthetic b(Lav/a;)Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;
    .locals 1

    iget-object v0, p0, Lav/a;->c:Lcom/google/googlenav/ui/view/ContentHintAnchoredPopup;

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/CharSequence;IIIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)V
    .locals 13

    new-instance v1, Lav/b;

    invoke-static {}, Lcom/google/googlenav/bH;->a()Las/c;

    move-result-object v3

    move-object v2, p0

    move v4, p1

    move-object v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lav/b;-><init>(Lav/a;Las/c;ILjava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)V

    sget v2, Lav/a;->a:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Las/d;->c(J)V

    sget v2, Lav/a;->b:I

    invoke-virtual {v1, v2}, Las/d;->b(I)V

    move/from16 v0, p7

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Las/d;->a(J)V

    invoke-virtual {v1}, Las/d;->g()V

    return-void
.end method

.method protected a()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->D()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v1}, Lcom/google/googlenav/ui/s;->al()Lbf/am;

    move-result-object v1

    invoke-virtual {v1}, Lbf/am;->C()Lbf/aJ;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lbf/am;->v()Lbf/O;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lbf/am;->w()Lbf/bK;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method a(ILjava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;I)Z
    .locals 13

    invoke-virtual {p0}, Lav/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v11, 0x1

    iget-object v0, p0, Lav/a;->d:Lcom/google/googlenav/ui/s;

    invoke-virtual {v0}, Lcom/google/googlenav/ui/s;->ab()Lcom/google/googlenav/android/aa;

    move-result-object v12

    new-instance v0, Lav/c;

    move-object v1, p0

    move/from16 v2, p9

    move/from16 v3, p3

    move-object v4, p2

    move v5, p1

    move/from16 v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lav/c;-><init>(Lav/a;IILjava/lang/CharSequence;IIIILcom/google/googlenav/ui/view/n;Lcom/google/googlenav/ui/view/o;)V

    invoke-virtual {v12, v0, v11}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    const/4 v0, 0x1

    goto :goto_0
.end method
