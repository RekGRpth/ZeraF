.class public Lbb/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static a:I


# instance fields
.field private b:Lbb/s;

.field private c:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x14

    sput v0, Lbb/z;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, ""

    invoke-static {v0}, Lbb/s;->a(Ljava/lang/String;)Lbb/s;

    move-result-object v0

    invoke-direct {p0, v0}, Lbb/z;-><init>(Lbb/s;)V

    return-void
.end method

.method public constructor <init>(Lbb/s;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbb/z;-><init>(Lbb/s;I)V

    return-void
.end method

.method public constructor <init>(Lbb/s;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lbb/z;->a:I

    if-le p2, v0, :cond_0

    sget p2, Lbb/z;->a:I

    :cond_0
    iput-object p1, p0, Lbb/z;->b:Lbb/s;

    if-nez p2, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    return-void

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/common/base/K;)Ljava/util/ArrayList;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-static {v1, p1}, Lcom/google/common/collect/S;->a(Ljava/util/Collection;Lcom/google/common/base/K;)Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method private static final a(Lbb/w;Lbb/w;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbb/w;->j()J

    move-result-wide v2

    invoke-virtual {p1}, Lbb/w;->j()J

    move-result-wide v4

    cmp-long v6, v2, v4

    if-lez v6, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lbb/w;->f()I

    move-result v2

    invoke-virtual {p1}, Lbb/w;->f()I

    move-result v3

    if-lt v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lbb/w;
    .locals 1

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public a(J)V
    .locals 1

    new-instance v0, Lbb/C;

    invoke-direct {v0, p0, p1, p2}, Lbb/C;-><init>(Lbb/z;J)V

    invoke-direct {p0, v0}, Lbb/z;->a(Lcom/google/common/base/K;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public a(Lbb/s;)V
    .locals 0

    iput-object p1, p0, Lbb/z;->b:Lbb/s;

    return-void
.end method

.method public a(Lbb/w;)V
    .locals 2

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, Lbb/z;->a:I

    if-lt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Lbb/z;)V
    .locals 2

    iget-object v0, p1, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    invoke-virtual {p0, v0}, Lbb/z;->b(Lbb/w;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Ljava/util/Set;)V
    .locals 1

    new-instance v0, Lbb/B;

    invoke-direct {v0, p0, p1}, Lbb/B;-><init>(Lbb/z;Ljava/util/Set;)V

    invoke-direct {p0, v0}, Lbb/z;->a(Lcom/google/common/base/K;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public b()Lbb/s;
    .locals 1

    iget-object v0, p0, Lbb/z;->b:Lbb/s;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    new-instance v0, Lbb/A;

    invoke-direct {v0, p0, p1}, Lbb/A;-><init>(Lbb/z;I)V

    invoke-direct {p0, v0}, Lbb/z;->a(Lcom/google/common/base/K;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public b(Lbb/w;)V
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lbb/z;->a(Lbb/w;Lbb/w;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    const/4 v1, 0x1

    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    :goto_1
    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lbb/w;->a(Lbb/w;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez v1, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move v0, v1

    move v1, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    goto :goto_1

    :cond_4
    if-nez v1, :cond_5

    invoke-virtual {p0, p1}, Lbb/z;->a(Lbb/w;)V

    :cond_5
    :goto_2
    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sget v1, Lbb/z;->a:I

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_2
.end method

.method public c()Lbb/z;
    .locals 3

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/z;

    iget-object v1, p0, Lbb/z;->b:Lbb/s;

    iput-object v1, v0, Lbb/z;->b:Lbb/s;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v1, v0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Superclass does not support clone"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lbb/z;->c()Lbb/z;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/hb;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    const/4 v3, 0x2

    invoke-virtual {v0}, Lbb/w;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lbb/z;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lbb/z;

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    iget-object v1, p1, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lbb/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbb/w;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lbb/w;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "; "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
