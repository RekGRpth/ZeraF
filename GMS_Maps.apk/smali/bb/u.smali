.class public Lbb/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lbb/u;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lbb/u;->b:I

    iput-boolean v1, p0, Lbb/u;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lbb/u;->d:I

    iput-boolean v1, p0, Lbb/u;->e:Z

    return-void
.end method


# virtual methods
.method public a()Lbb/s;
    .locals 10

    const/4 v2, 0x1

    iget v0, p0, Lbb/u;->b:I

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Integer;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/collect/dA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v3

    :goto_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v6

    :goto_1
    new-instance v0, Lbb/s;

    iget-object v1, p0, Lbb/u;->a:Ljava/lang/String;

    iget v2, p0, Lbb/u;->b:I

    iget v4, p0, Lbb/u;->d:I

    iget-boolean v5, p0, Lbb/u;->c:Z

    iget-boolean v8, p0, Lbb/u;->e:Z

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lbb/s;-><init>(Ljava/lang/String;ILjava/util/Set;IZJZLbb/t;)V

    return-object v0

    :cond_0
    invoke-static {}, Lbb/s;->a()Lcom/google/common/collect/ImmutableSet;

    move-result-object v3

    goto :goto_0

    :cond_1
    const-wide/16 v6, 0x0

    goto :goto_1
.end method

.method public a(I)Lbb/u;
    .locals 0

    iput p1, p0, Lbb/u;->b:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lbb/u;
    .locals 0

    iput-object p1, p0, Lbb/u;->a:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lbb/u;
    .locals 0

    iput-boolean p1, p0, Lbb/u;->c:Z

    return-object p0
.end method

.method public a([Ljava/lang/String;)Lbb/u;
    .locals 4

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    new-instance v0, Lbb/u;

    invoke-direct {v0}, Lbb/u;-><init>()V

    array-length v0, p1

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "At least one argument (= query) is required"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Lbb/u;->a(Ljava/lang/String;)Lbb/u;

    :try_start_0
    array-length v0, p1

    if-le v0, v1, :cond_1

    const/4 v0, 0x2

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lbb/u;->a(I)Lbb/u;

    :cond_1
    array-length v0, p1

    if-le v0, v2, :cond_2

    const/4 v0, 0x3

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lbb/u;->b(I)Lbb/u;

    :cond_2
    array-length v0, p1

    if-le v0, v3, :cond_3

    const/4 v0, 0x4

    aget-object v0, p1, v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lbb/u;->b(Z)Lbb/u;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    return-object p0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot parse feature type restrict"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b(I)Lbb/u;
    .locals 0

    iput p1, p0, Lbb/u;->d:I

    return-object p0
.end method

.method public b(Z)Lbb/u;
    .locals 0

    iput-boolean p1, p0, Lbb/u;->e:Z

    return-object p0
.end method
