.class LO/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:J

.field private c:Landroid/content/Context;

.field private final d:LO/J;

.field private e:LO/I;

.field private final f:Ljava/util/List;

.field private g:LO/g;

.field private h:LO/g;

.field private i:LO/g;

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z

.field private final m:Ljava/lang/Thread;

.field private n:LP/u;

.field private o:Z

.field private final p:Ljava/io/File;


# direct methods
.method constructor <init>(LO/J;LP/u;Ljava/lang/Thread;Ljava/io/File;Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, LO/r;->k:Z

    iput-boolean v0, p0, LO/r;->l:Z

    iput-object p1, p0, LO/r;->d:LO/J;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LO/r;->f:Ljava/util/List;

    new-instance v0, LO/I;

    invoke-direct {v0, p0}, LO/I;-><init>(LO/r;)V

    iput-object v0, p0, LO/r;->e:LO/I;

    iput-object p3, p0, LO/r;->m:Ljava/lang/Thread;

    iput-object p2, p0, LO/r;->n:LP/u;

    iput-object p4, p0, LO/r;->p:Ljava/io/File;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LO/r;->b:J

    iput-object p5, p0, LO/r;->c:Landroid/content/Context;

    return-void
.end method

.method private a(I)V
    .locals 1

    iget-object v0, p0, LO/r;->g:LO/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/r;->g:LO/g;

    invoke-virtual {v0}, LO/g;->q()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, LO/r;->g:LO/g;

    :cond_0
    iget-boolean v0, p0, LO/r;->a:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, LO/r;->p()V

    :cond_1
    return-void
.end method

.method private a(LO/g;I)V
    .locals 9

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, LO/g;->q()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1}, LO/g;->k()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v3

    array-length v3, v3

    if-lez v3, :cond_6

    iget-object v3, p0, LO/r;->e:LO/I;

    invoke-virtual {v3}, LO/I;->b()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_4

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v0

    array-length v0, v0

    if-le v0, v1, :cond_4

    move v0, v1

    :goto_2
    iget-object v3, p0, LO/r;->e:LO/I;

    invoke-virtual {v3}, LO/I;->a()LO/s;

    move-result-object v3

    invoke-virtual {v3}, LO/s;->f()[LO/z;

    move-result-object v4

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v5

    aget-object v5, v5, v2

    aget-object v6, v4, p2

    invoke-virtual {v6}, LO/z;->w()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v6}, LO/z;->w()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, LO/z;->a(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v3}, LO/s;->m()I

    move-result v3

    if-eqz v0, :cond_5

    new-array v3, v8, [LO/z;

    aput-object v5, v3, v2

    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v4

    aget-object v4, v4, v1

    aput-object v4, v3, v1

    :goto_3
    iget-object v4, p0, LO/r;->e:LO/I;

    invoke-virtual {v4, v3, v2, v1}, LO/I;->a([LO/z;IZ)V

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LO/r;->c()V

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    aput-object v5, v4, p2

    move v2, v3

    move-object v3, v4

    goto :goto_3

    :cond_6
    if-nez v0, :cond_0

    invoke-virtual {p0, v8, p1}, LO/r;->a(ILO/g;)V

    goto :goto_1
.end method

.method private a(LO/z;LO/z;I)Z
    .locals 3

    add-int/lit8 v0, p3, 0x1

    shr-int/lit8 v0, v0, 0x3

    add-int/2addr v0, p3

    invoke-virtual {p1}, LO/z;->p()I

    move-result v1

    invoke-virtual {p2}, LO/z;->p()I

    move-result v2

    sub-int/2addr v1, v2

    if-le v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LO/g;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LO/r;->i:LO/g;

    if-ne p1, v0, :cond_2

    iput-object v1, p0, LO/r;->i:LO/g;

    :cond_0
    :goto_0
    invoke-virtual {p1}, LO/g;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LO/r;->c:Landroid/content/Context;

    invoke-static {v0}, LO/M;->a(Landroid/content/Context;)LO/M;

    move-result-object v0

    invoke-virtual {p1}, LO/g;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, p1}, LO/M;->a(LO/g;)V

    :goto_1
    invoke-virtual {v0}, LO/M;->d()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, LO/r;->h:LO/g;

    if-ne p1, v0, :cond_0

    iput-object v1, p0, LO/r;->h:LO/g;

    goto :goto_0

    :cond_3
    invoke-virtual {v0, p1}, LO/M;->b(LO/g;)Z

    goto :goto_1
.end method

.method private b(Landroid/location/Location;)V
    .locals 3

    iget-object v0, p0, LO/r;->g:LO/g;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    invoke-virtual {v0}, LO/z;->C()LO/g;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, LO/r;->e:LO/I;

    invoke-virtual {v1}, LO/I;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, LO/r;->d:LO/J;

    invoke-virtual {v2, v0, v1}, LO/J;->a(LO/z;Landroid/location/Location;)LO/g;

    move-result-object v1

    invoke-virtual {v0, v1}, LO/z;->a(LO/g;)V

    goto :goto_0
.end method

.method private c(LO/g;)V
    .locals 8

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, LO/r;->g:LO/g;

    invoke-virtual {p1}, LO/g;->k()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LO/r;->n:LP/u;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LO/r;->a:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, LO/r;->p()V

    :cond_0
    invoke-virtual {p1}, LO/g;->d()[LO/z;

    move-result-object v1

    array-length v4, v1

    move v0, v3

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v0, v1

    if-lt v0, v6, :cond_8

    aget-object v0, v1, v2

    invoke-virtual {v0}, LO/z;->v()[LO/W;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v6, :cond_8

    aget-object v0, v1, v3

    aget-object v4, v1, v2

    invoke-virtual {p1}, LO/g;->j()I

    move-result v5

    invoke-direct {p0, v0, v4, v5}, LO/r;->a(LO/z;LO/z;I)Z

    move-result v0

    if-eqz v0, :cond_8

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [LO/z;

    array-length v4, v0

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_1
    array-length v1, v0

    if-lt v1, v6, :cond_4

    aget-object v1, v0, v3

    invoke-virtual {v1}, LO/z;->p()I

    move-result v1

    move v7, v2

    move v2, v1

    move v1, v7

    :goto_2
    array-length v4, v0

    if-ge v1, v4, :cond_3

    aget-object v4, v0, v1

    invoke-virtual {v4}, LO/z;->p()I

    move-result v4

    if-ge v4, v2, :cond_2

    aget-object v2, v0, v1

    invoke-virtual {v2}, LO/z;->p()I

    move-result v2

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    array-length v4, v0

    move v1, v3

    :goto_3
    if-ge v1, v4, :cond_4

    aget-object v5, v0, v1

    invoke-virtual {v5}, LO/z;->p()I

    move-result v6

    sub-int/2addr v6, v2

    invoke-virtual {v5, v6}, LO/z;->d(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    aget-object v1, v0, v3

    invoke-virtual {v1}, LO/z;->y()Z

    move-result v1

    if-eqz v1, :cond_5

    aget-object v1, v0, v3

    invoke-virtual {v1}, LO/z;->C()LO/g;

    move-result-object v1

    if-nez v1, :cond_5

    aget-object v1, v0, v3

    iget-object v2, p0, LO/r;->d:LO/J;

    aget-object v4, v0, v3

    invoke-virtual {v2, v4}, LO/J;->a(LO/z;)LO/g;

    move-result-object v2

    invoke-virtual {v1, v2}, LO/z;->a(LO/g;)V

    :cond_5
    iget-object v1, p0, LO/r;->e:LO/I;

    aget-object v2, v0, v3

    invoke-virtual {v2}, LO/z;->h()Z

    move-result v2

    invoke-virtual {v1, v0, v3, v2}, LO/I;->a([LO/z;IZ)V

    :cond_6
    :goto_4
    return-void

    :cond_7
    invoke-virtual {p1}, LO/g;->h()Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0, v6, p1}, LO/r;->a(ILO/g;)V

    goto :goto_4

    :cond_8
    move-object v0, v1

    goto :goto_1
.end method

.method private final o()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/r;->m:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Method must be called on NavigationThread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private p()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, LO/r;->a:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, LO/r;->b:J

    iget-object v0, p0, LO/r;->n:LP/u;

    invoke-virtual {v0}, LP/u;->b()V

    return-void
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->d(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(ILO/g;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, p1, p2, v1}, LO/q;->a(ILO/g;LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-eqz p2, :cond_1

    :try_start_1
    invoke-virtual {p2}, LO/g;->p()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    invoke-virtual {p2}, LO/g;->o()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, LO/g;->m()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, LO/g;->l()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_1
.end method

.method public a(LO/a;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, LO/a;->a()LO/g;

    move-result-object v0

    invoke-virtual {v0}, LO/g;->d()[LO/z;

    move-result-object v0

    iget-object v1, p0, LO/r;->e:LO/I;

    invoke-virtual {v1, v0, v2, v2}, LO/I;->a([LO/z;IZ)V

    return-void
.end method

.method public a(LO/g;)V
    .locals 3

    invoke-direct {p0}, LO/r;->o()V

    const/4 v0, 0x1

    iput-boolean v0, p0, LO/r;->o:Z

    invoke-virtual {p1}, LO/g;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LO/g;->r_()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-direct {p0, p1}, LO/r;->b(LO/g;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, LO/r;->g:LO/g;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, LO/r;->g:LO/g;

    invoke-direct {p0, v0}, LO/r;->c(LO/g;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {v2}, LO/z;->C()LO/g;

    move-result-object v2

    if-ne v2, p1, :cond_4

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LO/z;->a(LO/g;)V

    invoke-direct {p0, p1, v0}, LO/r;->a(LO/g;I)V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method declared-synchronized a(LO/j;I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    new-instance v0, LL/u;

    invoke-direct {v0, p1}, LL/u;-><init>(LO/j;)V

    invoke-static {v0}, Ll/f;->b(Ll/j;)V

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, p1, p2}, LO/q;->a(LO/j;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized a(LO/q;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LO/z;I)V
    .locals 5

    const-wide/16 v3, -0x1

    const/4 v2, 0x1

    invoke-direct {p0}, LO/r;->o()V

    iget-boolean v0, p0, LO/r;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LO/r;->a:Z

    if-nez p2, :cond_1

    iput-wide v3, p0, LO/r;->b:J

    invoke-virtual {p0, p1, v2}, LO/r;->a(LO/z;Z)V

    const-string v0, "j"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, LO/r;->b:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LO/r;->a(ILO/g;)V

    const-string v0, "j"

    invoke-static {v0, p2}, Lcom/google/android/maps/driveabout/app/dr;->a(Ljava/lang/String;I)V

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LO/r;->b:J

    goto :goto_0
.end method

.method public a(LO/z;Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, LO/r;->o()V

    if-eqz p2, :cond_1

    new-array v1, v4, [LO/z;

    aput-object p1, v1, v0

    iget-object v2, p0, LO/r;->e:LO/I;

    invoke-virtual {v2, v1, v0, v0}, LO/I;->a([LO/z;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, -0x1

    iget-object v2, p0, LO/r;->e:LO/I;

    invoke-virtual {v2}, LO/I;->a()LO/s;

    move-result-object v2

    invoke-virtual {v2}, LO/s;->f()[LO/z;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_4

    aget-object v3, v2, v0

    if-ne v3, p1, :cond_3

    :goto_2
    if-ltz v0, :cond_0

    invoke-virtual {p1}, LO/z;->y()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, LO/z;->C()LO/g;

    move-result-object v1

    if-nez v1, :cond_2

    iget-object v1, p0, LO/r;->d:LO/J;

    invoke-virtual {v1, p1}, LO/J;->a(LO/z;)LO/g;

    move-result-object v1

    invoke-virtual {p1, v1}, LO/z;->a(LO/g;)V

    :cond_2
    iget-object v1, p0, LO/r;->e:LO/I;

    invoke-virtual {v1, v2, v0, v4}, LO/I;->a([LO/z;IZ)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public a(LaH/h;)V
    .locals 6

    invoke-direct {p0}, LO/r;->o()V

    iget-boolean v0, p0, LO/r;->l:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LO/s;->i()LO/z;

    move-result-object v1

    iget-object v2, p0, LO/r;->n:LP/u;

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v1}, LO/z;->d()I

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, LO/r;->a:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, LO/r;->o:Z

    if-nez v2, :cond_2

    iget-object v2, p0, LO/r;->g:LO/g;

    if-nez v2, :cond_3

    :cond_2
    iget-object v1, p0, LO/r;->g:LO/g;

    if-nez v1, :cond_0

    iget-object v1, p0, LO/r;->d:LO/J;

    invoke-virtual {v0}, LO/s;->g()LO/z;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v1, p1, v0, v2}, LO/J;->a(LaH/h;LO/z;I)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->g:LO/g;

    invoke-virtual {p0}, LO/r;->g()V

    goto :goto_0

    :cond_3
    iget-wide v2, p0, LO/r;->b:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LO/r;->b:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, LO/r;->a:Z

    iget-object v0, p0, LO/r;->n:LP/u;

    invoke-virtual {v0, p1, v1}, LP/u;->a(LaH/h;LO/z;)V

    invoke-virtual {p0}, LO/r;->g()V

    goto :goto_0
.end method

.method public a(LaH/h;LO/z;)V
    .locals 2

    invoke-direct {p0}, LO/r;->o()V

    iget-object v0, p0, LO/r;->d:LO/J;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, p2, v1}, LO/J;->a(LaH/h;LO/z;I)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->g:LO/g;

    return-void
.end method

.method public a(LaH/h;[LO/U;II[LO/b;)V
    .locals 6

    invoke-direct {p0}, LO/r;->o()V

    iget-object v0, p0, LO/r;->d:LO/J;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LO/J;->a(LaH/h;[LO/U;II[LO/b;)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->g:LO/g;

    return-void
.end method

.method public a(Landroid/location/Location;)V
    .locals 9

    const/4 v8, 0x0

    invoke-direct {p0}, LO/r;->o()V

    iget-boolean v0, p0, LO/r;->j:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LO/r;->p:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/navgation_location"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0, p1}, LO/I;->a(Landroid/location/Location;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-static {}, LR/o;->a()LR/m;

    move-result-object v2

    iget-object v3, p0, LO/r;->e:LO/I;

    invoke-virtual {v3}, LO/I;->a()LO/s;

    move-result-object v3

    invoke-virtual {v3}, LO/s;->g()LO/z;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, LO/z;->B()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-boolean v4, p0, LO/r;->l:Z

    if-eqz v4, :cond_2

    invoke-virtual {v3}, LO/z;->z()J

    move-result-wide v4

    invoke-virtual {v2}, LR/m;->p()I

    move-result v6

    mul-int/lit16 v6, v6, 0x3e8

    int-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v4, v0, v4

    if-lez v4, :cond_2

    invoke-virtual {v3}, LO/z;->z()J

    move-result-wide v4

    invoke-virtual {v2}, LR/m;->q()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v6, v2

    add-long/2addr v4, v6

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    invoke-virtual {v3}, LO/z;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, LO/z;->A()V

    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0, v8}, LO/I;->a(I)Z

    :cond_1
    invoke-direct {p0, p1}, LO/r;->b(Landroid/location/Location;)V

    :cond_2
    iget-boolean v0, p0, LO/r;->j:Z

    if-eqz v0, :cond_3

    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    iput-boolean v8, p0, LO/r;->j:Z

    :cond_3
    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LO/r;->k:Z

    return-void
.end method

.method public a([LO/U;I[LO/b;)V
    .locals 1

    invoke-direct {p0}, LO/r;->o()V

    iget-object v0, p0, LO/r;->d:LO/J;

    invoke-virtual {v0, p1, p2, p3}, LO/J;->a([LO/U;I[LO/b;)LO/g;

    move-result-object v0

    iput-object v0, p0, LO/r;->i:LO/g;

    return-void
.end method

.method declared-synchronized b()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->f(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized b(LO/j;I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, p1, p2}, LO/q;->b(LO/j;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized b(LO/q;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, LO/r;->l:Z

    return-void
.end method

.method declared-synchronized c()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->g(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized d()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    invoke-virtual {v1}, LO/s;->h()LO/N;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->b(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized e()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->c(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized f()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->e(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method declared-synchronized g()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/q;

    invoke-interface {v0, v1}, LO/q;->a(LO/s;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    monitor-exit p0

    return-void
.end method

.method public h()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, LO/r;->o()V

    iput-boolean v1, p0, LO/r;->o:Z

    iget-boolean v0, p0, LO/r;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, LO/r;->a(ILO/g;)V

    :cond_0
    return-void
.end method

.method i()Z
    .locals 1

    iget-boolean v0, p0, LO/r;->l:Z

    return v0
.end method

.method public j()V
    .locals 5

    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->a()LO/s;

    move-result-object v0

    invoke-virtual {v0}, LO/s;->f()[LO/z;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {v3}, LO/z;->y()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, LO/z;->C()LO/g;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, p0, LO/r;->d:LO/J;

    invoke-virtual {v4, v3}, LO/J;->a(LO/z;)LO/g;

    move-result-object v4

    invoke-virtual {v3, v4}, LO/z;->a(LO/g;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public k()V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, LO/r;->a(I)V

    return-void
.end method

.method public l()V
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, LO/r;->a(I)V

    return-void
.end method

.method public m()V
    .locals 1

    iget-object v0, p0, LO/r;->e:LO/I;

    invoke-virtual {v0}, LO/I;->d()V

    return-void
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, LO/r;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, LO/r;->d:LO/J;

    invoke-virtual {v0}, LO/J;->a()V

    return-void
.end method
