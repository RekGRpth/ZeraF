.class public LO/g;
.super Law/a;
.source "SourceFile"


# static fields
.field private static l:Ljava/lang/String;


# instance fields
.field private final a:I

.field private final b:[LO/U;

.field private final c:[LO/b;

.field private final d:[B

.field private final e:Z

.field private final f:I

.field private final g:I

.field private final h:F

.field private final i:F

.field private final j:LO/U;

.field private final k:I

.field private m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private n:I

.field private o:[LO/z;

.field private p:[LO/U;

.field private q:[LO/b;

.field private final r:Landroid/location/Location;


# direct methods
.method private constructor <init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;)V
    .locals 1

    invoke-direct {p0}, Law/a;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LO/g;->n:I

    iput-object p1, p0, LO/g;->b:[LO/U;

    iput p2, p0, LO/g;->a:I

    iput p9, p0, LO/g;->k:I

    iput p3, p0, LO/g;->h:F

    iput p4, p0, LO/g;->i:F

    iput-boolean p5, p0, LO/g;->e:Z

    iput-object p6, p0, LO/g;->j:LO/U;

    iput p8, p0, LO/g;->g:I

    iput-object p10, p0, LO/g;->c:[LO/b;

    iput-object p11, p0, LO/g;->d:[B

    iput p7, p0, LO/g;->f:I

    iput-object p12, p0, LO/g;->r:Landroid/location/Location;

    sget-object v0, LO/g;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, LO/g;->v()V

    :cond_0
    return-void
.end method

.method synthetic constructor <init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;LO/h;)V
    .locals 0

    invoke-direct/range {p0 .. p12}, LO/g;-><init>([LO/U;IFFZLO/U;III[LO/b;[BLandroid/location/Location;)V

    return-void
.end method

.method static synthetic a(LO/g;I)I
    .locals 0

    iput p1, p0, LO/g;->n:I

    return p1
.end method

.method static synthetic a([LO/U;)Z
    .locals 1

    invoke-static {p0}, LO/g;->b([LO/U;)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)[LO/U;
    .locals 10

    const/16 v9, 0xf

    const/4 v8, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v3, v2, [LO/U;

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    const/4 v5, 0x7

    invoke-virtual {v4, v5, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    add-int/lit8 v6, v2, -0x1

    if-ne v0, v6, :cond_1

    new-instance v6, LO/U;

    invoke-direct {v6, v5, p1}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v6, v3, v0

    :goto_1
    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_0

    aget-object v5, v3, v0

    invoke-virtual {v4, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, LO/U;->a(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v6, LO/U;

    const/4 v7, 0x0

    invoke-direct {v6, v5, v7}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v6, v3, v0

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LO/U;)[LO/z;
    .locals 13

    const/16 v12, 0x8

    const/4 v2, 0x2

    const/4 v0, 0x0

    invoke-virtual {p1, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v10

    new-array v11, v10, [LO/z;

    const/16 v1, 0xd

    invoke-static {p1, v1, v0}, Lcom/google/googlenav/common/io/protocol/b;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;II)I

    move-result v7

    if-le v7, v2, :cond_0

    move v7, v0

    :cond_0
    if-ne v7, v2, :cond_1

    const/16 v1, 0x17

    invoke-static {p1, v1}, Lcom/google/googlenav/common/io/protocol/b;->h(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v7, 0x3

    :cond_1
    move v9, v0

    :goto_0
    if-ge v9, v10, :cond_2

    invoke-virtual {p1, v12, v9}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    iget v1, p0, LO/g;->k:I

    iget v3, p0, LO/g;->h:F

    iget v4, p0, LO/g;->i:F

    iget-boolean v5, p0, LO/g;->e:Z

    iget-object v6, p0, LO/g;->j:LO/U;

    iget-object v8, p0, LO/g;->q:[LO/b;

    move-object v2, p2

    invoke-static/range {v0 .. v8}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I[LO/U;FFZLO/U;I[LO/b;)LO/z;

    move-result-object v0

    aput-object v0, v11, v9

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_2
    return-object v11
.end method

.method private b(I)I
    .locals 2

    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    return v0
.end method

.method private static b([LO/U;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    array-length v2, p0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    aget-object v2, p0, v0

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    aget-object v3, p0, v1

    invoke-virtual {v3}, LO/U;->c()Lo/u;

    move-result-object v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lo/u;->a()I

    move-result v4

    invoke-virtual {v2}, Lo/u;->b()I

    move-result v2

    invoke-static {v4, v2}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    invoke-virtual {v3}, Lo/u;->a()I

    move-result v4

    invoke-virtual {v3}, Lo/u;->b()I

    move-result v3

    invoke-static {v4, v3}, Lo/T;->b(II)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v2}, Lo/T;->e()D

    move-result-wide v5

    div-double v2, v3, v5

    const-wide/high16 v4, 0x4000000000000000L

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/b;
    .locals 7

    const/16 v6, 0xe

    const/4 v2, 0x0

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-array v0, v3, [LO/b;

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-static {v4}, LO/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LO/b;

    move-result-object v5

    aput-object v5, v0, v1

    aget-object v5, v0, v1

    if-nez v5, :cond_1

    const-string v0, "DirectionsRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Option with no value: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v2, [LO/b;

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/U;
    .locals 8

    const/4 v7, 0x7

    const/4 v2, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v1, [LO/U;

    :cond_0
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    new-array v0, v3, [LO/U;

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    new-instance v5, LO/U;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private u()V
    .locals 3

    const/4 v2, 0x2

    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    iput v0, p0, LO/g;->n:I

    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, LO/g;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/b;

    move-result-object v0

    iput-object v0, p0, LO/g;->q:[LO/b;

    invoke-direct {p0}, LO/g;->w()V

    invoke-virtual {p0}, LO/g;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LO/g;->b:[LO/U;

    iget-object v1, p0, LO/g;->b:[LO/U;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, LO/U;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v1, v0}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)[LO/U;

    move-result-object v0

    iget-object v1, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0, v1, v0}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;[LO/U;)[LO/z;

    move-result-object v0

    iput-object v0, p0, LO/g;->o:[LO/z;

    iget v0, p0, LO/g;->k:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, LO/g;->o:[LO/z;

    array-length v0, v0

    if-ge v0, v2, :cond_0

    const/4 v0, 0x4

    iput v0, p0, LO/g;->n:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, LO/g;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {v0}, LO/g;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/U;

    move-result-object v0

    iput-object v0, p0, LO/g;->p:[LO/U;

    iget-object v0, p0, LO/g;->p:[LO/U;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x3

    iput v0, p0, LO/g;->n:I

    const/4 v0, 0x0

    iput-object v0, p0, LO/g;->p:[LO/U;

    goto :goto_0

    :cond_2
    const-string v0, "DirectionsRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Response status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LO/g;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private v()V
    .locals 1

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->i()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LO/g;->l:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method private w()V
    .locals 4

    const/4 v3, 0x2

    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    if-lt v1, v3, :cond_0

    const/4 v0, 0x0

    :goto_1
    add-int/lit8 v2, v1, -0x1

    if-ge v0, v2, :cond_2

    invoke-direct {p0, v0}, LO/g;->b(I)I

    move-result v2

    if-nez v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v1, -0x1

    invoke-direct {p0, v0}, LO/g;->b(I)I

    move-result v0

    if-ne v0, v3, :cond_0

    iput v3, p0, LO/g;->n:I

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, LO/g;->n:I

    return-void
.end method

.method public a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    iput-object p1, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-direct {p0}, LO/g;->u()V

    return-void
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    invoke-virtual {p0}, LO/g;->t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/googlenav/common/io/protocol/b;->a(Ljava/io/DataOutput;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method public a()Z
    .locals 2

    iget v0, p0, LO/g;->n:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 3

    :try_start_0
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v0, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-virtual {p0, v0}, LO/g;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    const-string v1, "DA:DirectionsRequest"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "DirectionsRequest: Handling request failed"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_1
    move-exception v0

    const-string v1, "DirectionsRequest"

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LJ/a;->b(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    const-string v1, "DA:DirectionsRequest"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    const-string v1, "DirectionsRequest: Parse failed"

    invoke-static {v1, v0}, LJ/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x1c

    return v0
.end method

.method public d()[LO/z;
    .locals 1

    iget-object v0, p0, LO/g;->o:[LO/z;

    return-object v0
.end method

.method public f()[LO/b;
    .locals 1

    iget-object v0, p0, LO/g;->q:[LO/b;

    return-object v0
.end method

.method public g()[LO/U;
    .locals 1

    iget-object v0, p0, LO/g;->p:[LO/U;

    return-object v0
.end method

.method public h()Z
    .locals 2

    iget v0, p0, LO/g;->k:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()I
    .locals 1

    iget v0, p0, LO/g;->f:I

    return v0
.end method

.method public k()Z
    .locals 1

    iget v0, p0, LO/g;->n:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Z
    .locals 2

    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Z
    .locals 2

    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 2

    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    iget v0, p0, LO/g;->n:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()I
    .locals 1

    iget v0, p0, LO/g;->k:I

    return v0
.end method

.method public r()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, LO/g;->m:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public r_()Z
    .locals 2

    iget v0, p0, LO/g;->k:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method t()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 9

    const v4, 0x186a0

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/aY;->c:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v0, p0, LO/g;->a:I

    invoke-virtual {v2, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addInt(II)V

    move v0, v1

    :goto_0
    iget-object v3, p0, LO/g;->b:[LO/U;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, LO/g;->b:[LO/U;

    aget-object v3, v3, v0

    invoke-virtual {v3}, LO/U;->l()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x5

    iget v3, p0, LO/g;->g:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget v0, p0, LO/g;->g:I

    if-le v0, v6, :cond_1

    const/16 v0, 0x16

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-static {}, Lcom/google/android/maps/driveabout/app/ci;->a()Lcom/google/android/maps/driveabout/app/ci;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/ci;->g()Lcom/google/android/maps/driveabout/app/NavigationService;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/maps/driveabout/app/NavigationService;->j()Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x20

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_1
    const/4 v0, 0x7

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xd

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xf

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x14

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xb

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x26

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x19

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x15

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x10

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1d

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x1f

    invoke-virtual {v2, v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBool(IZ)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x17

    const/16 v3, 0x32

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x18

    iget v3, p0, LO/g;->k:I

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, LO/g;->l:Ljava/lang/String;

    if-eqz v0, :cond_2

    const/16 v0, 0x23

    sget-object v3, LO/g;->l:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_2
    iget v0, p0, LO/g;->k:I

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/dp;->g:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget-object v3, p0, LO/g;->b:[LO/U;

    aget-object v3, v3, v1

    invoke-virtual {v3}, LO/U;->c()Lo/u;

    move-result-object v3

    invoke-static {v3}, LR/e;->b(Lo/u;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v0, v6, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-virtual {v0, v8, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v3, 0x9

    invoke-virtual {v2, v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_3
    iget-object v0, p0, LO/g;->c:[LO/b;

    if-eqz v0, :cond_4

    iget-object v0, p0, LO/g;->c:[LO/b;

    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    const/16 v5, 0xa

    invoke-virtual {v4}, LO/b;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, LO/g;->d:[B

    if-eqz v0, :cond_5

    const/16 v0, 0x2a

    iget-object v1, p0, LO/g;->d:[B

    invoke-virtual {v2, v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_5
    iget v0, p0, LO/g;->h:F

    cmpl-float v0, v0, v7

    if-gez v0, :cond_6

    iget v0, p0, LO/g;->i:F

    cmpl-float v0, v0, v7

    if-ltz v0, :cond_9

    :cond_6
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/aY;->A:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    iget v1, p0, LO/g;->h:F

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_7

    iget v1, p0, LO/g;->h:F

    float-to-int v1, v1

    invoke-virtual {v0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_7
    iget v1, p0, LO/g;->i:F

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_8

    iget v1, p0, LO/g;->i:F

    float-to-int v1, v1

    invoke-virtual {v0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    :cond_8
    const/16 v1, 0x11

    invoke-virtual {v2, v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    :cond_9
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " mode:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->a:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " action:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->k:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " startBearing:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->h:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, " startSpeed:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->i:F

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, " waypoints: ["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    :goto_0
    iget-object v3, p0, LO/g;->b:[LO/U;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, LO/g;->b:[LO/U;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v3, p0, LO/g;->b:[LO/U;

    aget-object v3, v3, v0

    iget-object v4, p0, LO/g;->j:LO/U;

    invoke-virtual {v3, v4}, LO/U;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "*"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, LO/g;->e:Z

    if-eqz v0, :cond_2

    const-string v0, " hasUncertainFromPoint:true"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v0, " maxTripCount:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, LO/g;->g:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iget-object v0, p0, LO/g;->c:[LO/b;

    if-eqz v0, :cond_3

    iget-object v0, p0, LO/g;->c:[LO/b;

    array-length v3, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v0, v1

    const-string v5, " "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget v0, p0, LO/g;->f:I

    if-eqz v0, :cond_4

    const-string v0, " previousAlternateExtraMeters:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LO/g;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_4
    const-string v0, " ] ]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
