.class LO/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Z


# direct methods
.method public constructor <init>(III)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, LO/n;-><init>(IIIZ)V

    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, LO/n;->a:I

    iput p2, p0, LO/n;->b:I

    iput-boolean p4, p0, LO/n;->d:Z

    if-eqz p3, :cond_0

    iput p3, p0, LO/n;->c:I

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f020168

    iput v0, p0, LO/n;->c:I

    goto :goto_0
.end method

.method static synthetic a(LO/n;)I
    .locals 1

    iget v0, p0, LO/n;->c:I

    return v0
.end method

.method static synthetic b(LO/n;)Z
    .locals 1

    iget-boolean v0, p0, LO/n;->d:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    new-instance v0, LO/o;

    invoke-direct {v0, p1, p0}, LO/o;-><init>(Landroid/content/Context;LO/n;)V

    return-object v0
.end method

.method public a(III)Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, LO/n;->a:I

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, LO/n;->b:I

    if-eqz v1, :cond_2

    iget v1, p0, LO/n;->b:I

    if-ne v1, p2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
