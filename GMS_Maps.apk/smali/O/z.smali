.class public final LO/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LO/G;


# static fields
.field private static final F:Ljava/util/Comparator;

.field public static final a:[Ljava/lang/String;


# instance fields
.field private A:Ljava/util/List;

.field private B:Z

.field private final C:Z

.field private D:Z

.field private E:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:[LO/N;

.field private f:Lo/X;

.field private g:Lo/aa;

.field private final h:[LO/W;

.field private i:I

.field private j:Ljava/util/ArrayList;

.field private k:Ljava/lang/String;

.field private volatile l:[LO/C;

.field private m:[D

.field private n:[D

.field private final o:I

.field private final p:I

.field private final q:Z

.field private final r:F

.field private final s:F

.field private t:J

.field private u:Z

.field private final v:I

.field private w:LO/g;

.field private x:[LO/b;

.field private y:[B

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TRAFFIC_STATUS_UNKNOWN"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "TRAFFIC_STATUS_BLACK"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TRAFFIC_STATUS_RED"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "TRAFFIC_STATUS_YELLOW"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "TRAFFIC_STATUS_GREEN"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "TRAFFIC_STATUS_IRRELEVANT"

    aput-object v2, v0, v1

    sput-object v0, LO/z;->a:[Ljava/lang/String;

    new-instance v0, LO/A;

    invoke-direct {v0}, LO/A;-><init>()V

    sput-object v0, LO/z;->F:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(II[LO/U;LO/U;FF[LO/N;Lo/X;Ljava/lang/String;IILjava/util/ArrayList;[LO/C;ZI[LO/b;[BILjava/util/List;Ljava/lang/String;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, LO/z;->i:I

    const/4 v1, 0x0

    iput-boolean v1, p0, LO/z;->u:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, LO/z;->D:Z

    const-string v1, ""

    iput-object v1, p0, LO/z;->E:Ljava/lang/String;

    iput p1, p0, LO/z;->b:I

    iput p2, p0, LO/z;->c:I

    iput p5, p0, LO/z;->r:F

    iput p6, p0, LO/z;->s:F

    iput-object p7, p0, LO/z;->e:[LO/N;

    iput-object p8, p0, LO/z;->f:Lo/X;

    iput-object p9, p0, LO/z;->k:Ljava/lang/String;

    iput p10, p0, LO/z;->p:I

    move-object/from16 v0, p12

    iput-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    move/from16 v0, p14

    iput-boolean v0, p0, LO/z;->d:Z

    move/from16 v0, p15

    iput v0, p0, LO/z;->v:I

    move-object/from16 v0, p16

    iput-object v0, p0, LO/z;->x:[LO/b;

    move-object/from16 v0, p17

    iput-object v0, p0, LO/z;->y:[B

    move/from16 v0, p18

    iput v0, p0, LO/z;->z:I

    if-eqz p19, :cond_2

    :goto_0
    move-object/from16 v0, p19

    iput-object v0, p0, LO/z;->A:Ljava/util/List;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, LO/z;->t:J

    array-length v1, p7

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, LO/z;->q:Z

    new-instance v1, Lo/aa;

    iget-object v2, p0, LO/z;->f:Lo/X;

    invoke-direct {v1, v2}, Lo/aa;-><init>(Lo/X;)V

    iput-object v1, p0, LO/z;->g:Lo/aa;

    invoke-direct {p0}, LO/z;->I()V

    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->c([LO/N;)V

    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->d([LO/N;)V

    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->a([LO/N;)Z

    move-result v1

    iput-boolean v1, p0, LO/z;->B:Z

    iget-object v1, p0, LO/z;->e:[LO/N;

    invoke-static {v1}, LO/z;->b([LO/N;)Z

    move-result v1

    iput-boolean v1, p0, LO/z;->C:Z

    if-eqz p3, :cond_4

    array-length v1, p3

    new-array v1, v1, [LO/W;

    iput-object v1, p0, LO/z;->h:[LO/W;

    const/4 v1, 0x0

    :goto_2
    array-length v2, p3

    if-ge v1, v2, :cond_5

    iget-object v2, p0, LO/z;->h:[LO/W;

    new-instance v3, LO/W;

    aget-object v4, p3, v1

    invoke-direct {v3, v4, p0}, LO/W;-><init>(LO/U;LO/z;)V

    aput-object v3, v2, v1

    if-eqz p4, :cond_0

    aget-object v2, p3, v1

    invoke-virtual {v2}, LO/U;->c()Lo/u;

    move-result-object v2

    invoke-virtual {p4}, LO/U;->c()Lo/u;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v1

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LO/W;->b(Z)V

    :cond_0
    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v1

    invoke-virtual {v2}, LO/W;->b()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, LO/z;->D:Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    new-instance p19, Ljava/util/ArrayList;

    invoke-direct/range {p19 .. p19}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    iput-object v1, p0, LO/z;->h:[LO/W;

    :cond_5
    invoke-virtual {p0}, LO/z;->t()Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    if-ne p2, v1, :cond_8

    :cond_6
    const/4 v1, 0x1

    :goto_3
    iput-boolean v1, p0, LO/z;->u:Z

    if-nez p13, :cond_9

    invoke-direct {p0}, LO/z;->H()V

    move/from16 v0, p11

    iput v0, p0, LO/z;->o:I

    :goto_4
    if-eqz p20, :cond_7

    move-object/from16 v0, p20

    iput-object v0, p0, LO/z;->E:Ljava/lang/String;

    :cond_7
    return-void

    :cond_8
    const/4 v1, 0x0

    goto :goto_3

    :cond_9
    move-object/from16 v0, p13

    iput-object v0, p0, LO/z;->l:[LO/C;

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v1, v2}, LO/z;->b(D)I

    move-result v1

    iput v1, p0, LO/z;->o:I

    goto :goto_4
.end method

.method private H()V
    .locals 5

    iget-object v0, p0, LO/z;->e:[LO/N;

    if-eqz v0, :cond_1

    iget-object v0, p0, LO/z;->e:[LO/N;

    array-length v0, v0

    new-array v2, v0, [LO/C;

    const/4 v1, 0x0

    iget-object v0, p0, LO/z;->e:[LO/N;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    new-instance v3, LO/C;

    iget-object v4, p0, LO/z;->e:[LO/N;

    aget-object v4, v4, v0

    invoke-virtual {v4}, LO/N;->z()I

    move-result v4

    invoke-direct {v3, v4, v1}, LO/C;-><init>(II)V

    aput-object v3, v2, v0

    iget-object v3, p0, LO/z;->e:[LO/N;

    aget-object v3, v3, v0

    invoke-virtual {v3}, LO/N;->f()I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iput-object v2, p0, LO/z;->l:[LO/C;

    :cond_1
    return-void
.end method

.method private I()V
    .locals 9

    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    iget-object v0, p0, LO/z;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    new-array v0, v0, [D

    iput-object v0, p0, LO/z;->m:[D

    iget-object v0, p0, LO/z;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    new-array v0, v0, [D

    iput-object v0, p0, LO/z;->n:[D

    iget-object v0, p0, LO/z;->f:Lo/X;

    invoke-virtual {v0}, Lo/X;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, LO/z;->m:[D

    aput-wide v1, v0, v3

    iget-object v0, p0, LO/z;->n:[D

    aput-wide v1, v0, v3

    const/4 v0, 0x1

    move-wide v3, v1

    :goto_0
    iget-object v5, p0, LO/z;->m:[D

    array-length v5, v5

    if-ge v0, v5, :cond_0

    iget-object v5, p0, LO/z;->f:Lo/X;

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {v5, v6}, Lo/X;->b(I)F

    move-result v5

    float-to-double v5, v5

    add-double/2addr v3, v5

    iget-object v7, p0, LO/z;->f:Lo/X;

    invoke-virtual {v7, v0}, Lo/X;->a(I)Lo/T;

    move-result-object v7

    invoke-virtual {v7}, Lo/T;->b()D

    move-result-wide v7

    invoke-static {v7, v8}, Lo/T;->a(D)D

    move-result-wide v7

    div-double/2addr v5, v7

    add-double/2addr v1, v5

    iget-object v5, p0, LO/z;->m:[D

    aput-wide v3, v5, v0

    iget-object v5, p0, LO/z;->n:[D

    aput-wide v1, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a(LO/N;)LO/j;
    .locals 4

    invoke-virtual {p0}, LO/N;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/j;

    invoke-virtual {v0}, LO/j;->a()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(IILO/U;LO/U;[LO/N;Lo/X;Ljava/lang/String;I[LO/b;ILjava/util/List;)LO/z;
    .locals 22

    const/4 v1, 0x0

    :goto_0
    move-object/from16 v0, p4

    array-length v2, v0

    if-ge v1, v2, :cond_2

    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->z()I

    move-result v2

    if-ltz v2, :cond_0

    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->z()I

    move-result v2

    invoke-virtual/range {p5 .. p5}, Lo/X;->b()I

    move-result v3

    if-lt v2, v3, :cond_1

    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid point index for step: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " point index: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v1, p4, v1

    invoke-virtual {v1}, LO/N;->z()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    new-array v4, v1, [LO/U;

    const/4 v1, 0x0

    aput-object p2, v4, v1

    const/4 v1, 0x1

    aput-object p3, v4, v1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p4

    array-length v2, v0

    if-ge v1, v2, :cond_3

    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->f()I

    move-result v2

    add-int/2addr v11, v2

    aget-object v2, p4, v1

    invoke-virtual {v2}, LO/N;->e()I

    move-result v2

    add-int/2addr v12, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    new-instance v1, LO/z;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v18, 0x0

    const/16 v21, 0x0

    move/from16 v2, p0

    move/from16 v3, p1

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    move/from16 v16, p7

    move-object/from16 v17, p8

    move/from16 v19, p9

    move-object/from16 v20, p10

    invoke-direct/range {v1 .. v21}, LO/z;-><init>(II[LO/U;LO/U;FF[LO/N;Lo/X;Ljava/lang/String;IILjava/util/ArrayList;[LO/C;ZI[LO/b;[BILjava/util/List;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I[LO/U;FFZLO/U;I[LO/b;)LO/z;
    .locals 22

    const/16 v1, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trips with multiple routes are not supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/16 v1, 0x15

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/16 v2, 0x14

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/googlenav/common/io/protocol/b;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)Ljava/lang/String;

    move-result-object v10

    const/16 v2, 0x1c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0x1c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v21

    :goto_1
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v2, 0xb

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v2, 0xb

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    :goto_2
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v11

    :goto_3
    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v12

    :goto_4
    invoke-static/range {p0 .. p0}, LO/z;->c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;

    move-result-object v20

    const/16 v4, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v18

    invoke-static {v3}, LO/z;->b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lo/u;

    move-result-object v4

    invoke-static {v4}, LO/z;->a([Lo/u;)Lo/X;

    move-result-object v9

    if-nez v1, :cond_6

    move-object/from16 v0, p2

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p2, v1

    invoke-static {v3, v9, v1}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;LO/U;)[LO/N;

    move-result-object v8

    :goto_5
    const/4 v1, 0x0

    aget-object v1, p2, v1

    move-object/from16 v0, p2

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v4, p2, v4

    invoke-static {v3, v1, v4}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LO/U;LO/U;)[LO/U;

    move-result-object v4

    invoke-static {v3, v9}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;)Ljava/util/ArrayList;

    move-result-object v13

    invoke-static {v3}, LO/z;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/C;

    move-result-object v14

    const/16 v19, -0x1

    new-instance v1, LO/z;

    move/from16 v3, p1

    move-object/from16 v5, p6

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v15, p5

    move/from16 v16, p7

    move-object/from16 v17, p8

    invoke-direct/range {v1 .. v21}, LO/z;-><init>(II[LO/U;LO/U;FF[LO/N;Lo/X;Ljava/lang/String;IILjava/util/ArrayList;[LO/C;ZI[LO/b;[BILjava/util/List;Ljava/lang/String;)V

    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    const-string v21, ""

    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    goto :goto_3

    :cond_5
    const/4 v12, 0x0

    goto :goto_4

    :cond_6
    const/4 v1, 0x0

    new-array v8, v1, [LO/N;

    goto :goto_5
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;)Ljava/util/ArrayList;
    .locals 9

    const/16 v8, 0xf

    const/4 v0, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v5

    move v1, v0

    move v2, v0

    move v3, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {p0, v8, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v6

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v0

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v7

    if-lt v0, v7, :cond_0

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    if-le v0, v3, :cond_1

    new-instance v7, LO/F;

    invoke-direct {v7, v3, v0, v2}, LO/F;-><init>(III)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v6, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    add-int/lit8 v1, v1, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_3

    new-instance v0, LO/F;

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v0, v3, v1, v2}, LO/F;-><init>(III)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    return-object v4
.end method

.method static a([Lo/u;)Lo/X;
    .locals 9

    const/high16 v8, 0x41000000

    array-length v3, p0

    new-instance v4, Lo/Z;

    invoke-direct {v4, v3}, Lo/Z;-><init>(I)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v2, p0, v0

    invoke-virtual {v2}, Lo/u;->a()I

    move-result v5

    invoke-virtual {v2}, Lo/u;->b()I

    move-result v2

    invoke-static {v5, v2}, Lo/T;->b(II)Lo/T;

    move-result-object v2

    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    add-int/lit8 v5, v0, 0x1

    if-ge v5, v3, :cond_0

    add-int/lit8 v5, v0, 0x1

    aget-object v5, p0, v5

    invoke-virtual {v5}, Lo/u;->a()I

    move-result v5

    add-int/lit8 v6, v0, 0x1

    aget-object v6, p0, v6

    invoke-virtual {v6}, Lo/u;->b()I

    move-result v6

    invoke-static {v5, v6}, Lo/T;->b(II)Lo/T;

    move-result-object v5

    invoke-virtual {v5, v2}, Lo/T;->c(Lo/T;)F

    move-result v6

    cmpl-float v7, v6, v8

    if-lez v7, :cond_0

    div-float v6, v8, v6

    invoke-static {v2, v5, v6, v2}, Lo/T;->a(Lo/T;Lo/T;FLo/T;)V

    :cond_0
    invoke-virtual {v2, v1}, Lo/T;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lo/T;->f()I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    invoke-virtual {v2, v1}, Lo/T;->a(I)V

    :cond_1
    invoke-virtual {v4, v2}, Lo/Z;->a(Lo/T;)Z

    add-int/lit8 v0, v0, 0x1

    move-object v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lo/Z;->d()Lo/X;

    move-result-object v0

    return-object v0
.end method

.method static a([LO/N;)Z
    .locals 6

    const/4 v1, 0x0

    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v0, p0, v2

    invoke-virtual {v0}, LO/N;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/Q;

    invoke-virtual {v0}, LO/Q;->a()I

    move-result v0

    const/4 v5, 0x2

    if-eq v0, v5, :cond_1

    const/4 v5, 0x3

    if-eq v0, v5, :cond_1

    const/16 v5, 0x9

    if-ne v0, v5, :cond_0

    :cond_1
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[LO/C;
    .locals 7

    const/16 v6, 0xc

    invoke-virtual {p0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-array v0, v2, [LO/C;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v6, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    const/16 v5, 0xe

    invoke-virtual {v3, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    new-instance v5, LO/C;

    invoke-direct {v5, v4, v3}, LO/C;-><init>(II)V

    aput-object v5, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-nez v2, :cond_1

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method

.method static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/X;LO/U;)[LO/N;
    .locals 12

    const/16 v11, 0xa

    const/4 v8, 0x0

    invoke-virtual {p0, v11}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v9

    new-array v10, v9, [LO/N;

    const/4 v0, 0x0

    move v2, v8

    move-object v1, v0

    :goto_0
    if-ge v2, v9, :cond_3

    invoke-virtual {p0, v11, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v1, :cond_4

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v3

    const/4 v4, 0x4

    invoke-static {v1, v4}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v4

    :goto_1
    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/googlenav/common/io/protocol/b;->e(Lcom/google/googlenav/common/io/protocol/ProtoBuf;I)I

    move-result v7

    invoke-virtual {p1}, Lo/X;->b()I

    move-result v1

    const/4 v5, 0x2

    if-lt v1, v5, :cond_2

    if-lez v7, :cond_0

    add-int/lit8 v1, v7, -0x1

    invoke-virtual {p1, v1}, Lo/X;->d(I)F

    move-result v1

    :goto_2
    invoke-virtual {p1}, Lo/X;->b()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v7, v5, :cond_1

    invoke-virtual {p1, v7}, Lo/X;->d(I)F

    move-result v5

    :goto_3
    move v6, v5

    move v5, v1

    :goto_4
    invoke-virtual {p1, v7}, Lo/X;->a(I)Lo/T;

    move-result-object v1

    move-object v7, p2

    invoke-static/range {v0 .. v7}, LO/N;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Lo/T;IIIFFLO/U;)LO/N;

    move-result-object v1

    aput-object v1, v10, v2

    add-int/lit8 v2, v2, 0x1

    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v7}, Lo/X;->d(I)F

    move-result v1

    goto :goto_2

    :cond_1
    add-int/lit8 v5, v7, -0x1

    invoke-virtual {p1, v5}, Lo/X;->d(I)F

    move-result v5

    goto :goto_3

    :cond_2
    const/4 v6, 0x0

    move v5, v6

    goto :goto_4

    :cond_3
    return-object v10

    :cond_4
    move v4, v8

    move v3, v8

    goto :goto_1
.end method

.method private static a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;LO/U;LO/U;)[LO/U;
    .locals 8

    const/16 v7, 0x10

    const/4 v0, 0x0

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    add-int/lit8 v2, v1, 0x2

    new-array v2, v2, [LO/U;

    new-instance v3, LO/U;

    invoke-direct {v3, p1}, LO/U;-><init>(LO/U;)V

    aput-object v3, v2, v0

    add-int/lit8 v3, v1, 0x1

    new-instance v4, LO/U;

    invoke-direct {v4, p2}, LO/U;-><init>(LO/U;)V

    aput-object v4, v2, v3

    :goto_0
    if-ge v0, v1, :cond_0

    add-int/lit8 v3, v0, 0x1

    new-instance v4, LO/U;

    invoke-virtual {p0, v7, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, LO/U;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Ljava/lang/String;)V

    aput-object v4, v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method private b(Lo/T;DZ)Ljava/util/List;
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, LO/z;->g:Lo/aa;

    const-wide/high16 v2, 0x3ff0000000000000L

    add-double v2, v2, p2

    double-to-int v2, v2

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lo/ad;->a(Lo/T;I)Lo/ad;

    move-result-object v2

    invoke-virtual {v1, v2}, Lo/aa;->a(Lo/ad;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lo/T;

    invoke-direct {v8}, Lo/T;-><init>()V

    new-instance v9, Lo/T;

    invoke-direct {v9}, Lo/T;-><init>()V

    new-instance v10, Lo/T;

    invoke-direct {v10}, Lo/T;-><init>()V

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lo/am;

    invoke-virtual {v1}, Lo/am;->a()I

    move-result v2

    add-int/lit8 v11, v2, -0x1

    const/4 v2, 0x0

    move v6, v2

    :goto_2
    if-ge v6, v11, :cond_4

    invoke-virtual {v1, v6, v8}, Lo/am;->a(ILo/T;)V

    add-int/lit8 v2, v6, 0x1

    invoke-virtual {v1, v2, v9}, Lo/am;->a(ILo/T;)V

    move-object/from16 v0, p1

    invoke-static {v8, v9, v0, v10}, Lo/T;->a(Lo/T;Lo/T;Lo/T;Lo/T;)F

    move-result v2

    float-to-double v12, v2

    cmpg-double v2, v12, p2

    if-gez v2, :cond_2

    const/4 v5, 0x0

    if-nez p4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    new-instance v2, LO/D;

    const/4 v5, 0x0

    invoke-direct {v2, v5}, LO/D;-><init>(LO/A;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    invoke-static {v2, v0}, LO/D;->a(LO/D;LO/z;)LO/z;

    invoke-static {v2, v12, v13}, LO/D;->a(LO/D;D)D

    invoke-virtual {v1}, Lo/am;->b()I

    move-result v5

    add-int/2addr v5, v6

    invoke-static {v2, v5}, LO/D;->a(LO/D;I)I

    invoke-static {v10}, Lo/T;->a(Lo/T;)Lo/T;

    move-result-object v5

    invoke-static {v2, v5}, LO/D;->a(LO/D;Lo/T;)Lo/T;

    invoke-static {v8, v9}, Lo/V;->b(Lo/T;Lo/T;)F

    move-result v5

    float-to-double v12, v5

    invoke-static {v2, v12, v13}, LO/D;->b(LO/D;D)D

    :cond_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LO/D;

    invoke-static {v2}, LO/D;->b(LO/D;)D

    move-result-wide v14

    cmpg-double v2, v12, v14

    if-gez v2, :cond_7

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LO/D;

    goto :goto_3

    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_5
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_6
    move-object v1, v4

    goto/16 :goto_0

    :cond_7
    move-object v2, v5

    goto :goto_3
.end method

.method static b([LO/N;)Z
    .locals 6

    const/4 v1, 0x0

    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v0, p0, v2

    invoke-virtual {v0}, LO/N;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/Q;

    invoke-virtual {v0}, LO/Q;->a()I

    move-result v0

    const/4 v5, 0x4

    if-ne v0, v5, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)[Lo/u;
    .locals 14

    const/16 v13, 0x9

    const/4 v12, 0x7

    const/4 v2, 0x0

    invoke-virtual {p0, v12}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v7

    move v0, v2

    move v1, v2

    :goto_0
    if-ge v0, v7, :cond_0

    invoke-virtual {p0, v12, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    invoke-virtual {v3, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v3

    array-length v3, v3

    div-int/lit8 v3, v3, 0x4

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-array v8, v1, [Lo/u;

    move v6, v2

    move v4, v2

    :goto_1
    if-ge v6, v7, :cond_2

    invoke-virtual {p0, v12, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v5

    const/16 v0, 0x8

    invoke-virtual {v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v1

    add-int/lit8 v0, v4, 0x1

    new-instance v9, Lo/u;

    invoke-direct {v9, v3, v1}, Lo/u;-><init>(II)V

    aput-object v9, v8, v4

    invoke-virtual {v5, v13}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v9

    if-eqz v9, :cond_1

    array-length v10, v9

    move v4, v3

    move v3, v1

    move v1, v2

    :goto_2
    if-ge v1, v10, :cond_1

    add-int/lit8 v5, v1, 0x1

    aget-byte v1, v9, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v11, v5, 0x1

    aget-byte v5, v9, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v1, v5

    int-to-short v1, v1

    add-int/2addr v4, v1

    add-int/lit8 v5, v11, 0x1

    aget-byte v1, v9, v11

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v11, v1, 0x8

    add-int/lit8 v1, v5, 0x1

    aget-byte v5, v9, v5

    and-int/lit16 v5, v5, 0xff

    add-int/2addr v5, v11

    int-to-short v5, v5

    add-int/2addr v3, v5

    add-int/lit8 v5, v0, 0x1

    new-instance v11, Lo/u;

    invoke-direct {v11, v4, v3}, Lo/u;-><init>(II)V

    aput-object v11, v8, v0

    move v0, v5

    goto :goto_2

    :cond_1
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v4, v0

    goto :goto_1

    :cond_2
    return-object v8
.end method

.method private static c(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Ljava/util/List;
    .locals 8

    const/16 v7, 0x1a

    const/4 v6, 0x2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v7}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v7, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v5, LO/S;

    invoke-direct {v5, v4, v0}, LO/S;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    return-object v2
.end method

.method private static c([LO/N;)V
    .locals 3

    const/4 v0, 0x1

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    aget-object v1, p0, v1

    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, LO/N;->a(LO/N;)V

    aget-object v1, p0, v0

    add-int/lit8 v2, v0, -0x1

    aget-object v2, p0, v2

    invoke-virtual {v1, v2}, LO/N;->b(LO/N;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static d([LO/N;)V
    .locals 4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_2

    aget-object v0, p0, v1

    invoke-virtual {v0}, LO/N;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/j;

    invoke-virtual {v0}, LO/j;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v3, v1, 0x1

    aget-object v3, p0, v3

    invoke-static {v3}, LO/z;->a(LO/N;)LO/j;

    move-result-object v3

    invoke-virtual {v0, v3}, LO/j;->a(LO/j;)V

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-direct {p0}, LO/z;->H()V

    return-void
.end method

.method public B()Z
    .locals 1

    iget-boolean v0, p0, LO/z;->u:Z

    return v0
.end method

.method C()LO/g;
    .locals 1

    iget-object v0, p0, LO/z;->w:LO/g;

    return-object v0
.end method

.method public D()[LO/b;
    .locals 1

    iget-object v0, p0, LO/z;->x:[LO/b;

    return-object v0
.end method

.method public E()Z
    .locals 1

    iget-boolean v0, p0, LO/z;->B:Z

    return v0
.end method

.method public F()Z
    .locals 1

    iget-boolean v0, p0, LO/z;->C:Z

    return v0
.end method

.method public G()Z
    .locals 1

    iget-boolean v0, p0, LO/z;->D:Z

    return v0
.end method

.method public a(LO/D;)D
    .locals 4

    iget-object v0, p0, LO/z;->m:[D

    invoke-virtual {p1}, LO/D;->e()I

    move-result v1

    aget-wide v0, v0, v1

    iget-object v2, p0, LO/z;->f:Lo/X;

    invoke-virtual {p1}, LO/D;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    invoke-static {p1}, LO/D;->a(LO/D;)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public a(D)I
    .locals 1

    iget-object v0, p0, LO/z;->m:[D

    invoke-static {v0, p1, p2}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v0

    if-gez v0, :cond_0

    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    :cond_0
    return v0
.end method

.method public a(Landroid/content/Context;II)LO/E;
    .locals 10

    const/4 v3, 0x0

    if-gez p2, :cond_0

    new-instance v0, LO/E;

    const-string v1, ""

    invoke-direct {v0, v1, v3}, LO/E;-><init>(Ljava/lang/String;LO/A;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    if-lez p2, :cond_1

    add-int/lit8 p2, p2, -0x1

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v3

    move-object v1, v3

    :goto_1
    invoke-virtual {p0}, LO/z;->k()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge p2, v4, :cond_4

    invoke-virtual {p0, p2}, LO/z;->a(I)LO/N;

    move-result-object v4

    invoke-virtual {v4}, LO/N;->x()LO/P;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    add-int/lit8 v4, p2, 0x1

    invoke-virtual {p0, v4}, LO/z;->a(I)LO/N;

    move-result-object v4

    invoke-virtual {v4}, LO/N;->e()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v8, v4, v2

    if-lez v8, :cond_2

    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, LO/P;->c()Ljava/lang/String;

    move-result-object v0

    move v2, v4

    :cond_2
    int-to-float v8, p3

    const/high16 v9, 0x3e800000

    mul-float/2addr v8, v9

    cmpl-float v4, v4, v8

    if-lez v4, :cond_3

    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v7}, LO/P;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v7}, LO/P;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v4, 0x1

    if-le v2, v4, :cond_5

    const v0, 0x7f0d0098

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0d0099

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, LO/E;

    invoke-static {v1, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LO/E;-><init>(Ljava/lang/String;Ljava/lang/String;LO/A;)V

    goto/16 :goto_0

    :cond_5
    if-eqz v1, :cond_6

    new-instance v2, LO/E;

    invoke-direct {v2, v1, v0, v3}, LO/E;-><init>(Ljava/lang/String;Ljava/lang/String;LO/A;)V

    move-object v0, v2

    goto/16 :goto_0

    :cond_6
    new-instance v0, LO/E;

    const v1, 0x7f0d00a6

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, LO/E;-><init>(Ljava/lang/String;LO/A;)V

    goto/16 :goto_0
.end method

.method public a(Lo/T;D)LO/H;
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3, v1}, LO/z;->b(Lo/T;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/H;

    goto :goto_0
.end method

.method public a(I)LO/N;
    .locals 1

    iget-object v0, p0, LO/z;->e:[LO/N;

    aget-object v0, v0, p1

    return-object v0
.end method

.method a(LO/g;)V
    .locals 0

    iput-object p1, p0, LO/z;->w:LO/g;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, LO/z;->k:Ljava/lang/String;

    return-void
.end method

.method public a()Z
    .locals 1

    iget v0, p0, LO/z;->z:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lo/T;DZ)[LO/D;
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LO/z;->b(Lo/T;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    new-array v0, v0, [LO/D;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [LO/D;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LO/D;

    if-eqz p4, :cond_0

    sget-object v1, LO/z;->F:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public b(I)D
    .locals 2

    iget-object v0, p0, LO/z;->m:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public b(LO/D;)D
    .locals 6

    iget-object v0, p0, LO/z;->n:[D

    invoke-virtual {p1}, LO/D;->e()I

    move-result v1

    aget-wide v0, v0, v1

    iget-object v2, p0, LO/z;->f:Lo/X;

    invoke-virtual {p1}, LO/D;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Lo/X;->a(I)Lo/T;

    move-result-object v2

    invoke-static {p1}, LO/D;->a(LO/D;)Lo/T;

    move-result-object v3

    invoke-virtual {v2, v3}, Lo/T;->c(Lo/T;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {p1}, LO/D;->a(LO/D;)Lo/T;

    move-result-object v4

    invoke-virtual {v4}, Lo/T;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Lo/T;->a(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public b(D)I
    .locals 11

    const/4 v1, 0x0

    iget-object v2, p0, LO/z;->l:[LO/C;

    new-instance v0, LO/C;

    invoke-virtual {p0, p1, p2}, LO/z;->a(D)I

    move-result v3

    invoke-direct {v0, v3, v1}, LO/C;-><init>(II)V

    new-instance v3, LO/B;

    invoke-direct {v3, p0}, LO/B;-><init>(LO/z;)V

    invoke-static {v2, v0, v3}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    if-gez v0, :cond_0

    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    :cond_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LO/z;->m:[D

    aget-object v3, v2, v0

    iget v3, v3, LO/C;->a:I

    aget-wide v3, v1, v3

    iget-object v1, p0, LO/z;->m:[D

    add-int/lit8 v5, v0, 0x1

    aget-object v5, v2, v5

    iget v5, v5, LO/C;->a:I

    aget-wide v5, v1, v5

    add-int/lit8 v1, v0, 0x1

    aget-object v1, v2, v1

    iget v1, v1, LO/C;->b:I

    int-to-double v7, v1

    sub-double v9, v5, p1

    sub-double v3, v5, v3

    div-double v3, v9, v3

    aget-object v1, v2, v0

    iget v1, v1, LO/C;->b:I

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v2, v0

    iget v0, v0, LO/C;->b:I

    sub-int v0, v1, v0

    int-to-double v0, v0

    mul-double/2addr v0, v3

    add-double/2addr v0, v7

    double-to-int v0, v0

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    iget v0, p0, LO/z;->c:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)D
    .locals 2

    iget-object v0, p0, LO/z;->n:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, LO/z;->z:I

    return v0
.end method

.method public c(D)Ljava/util/Collection;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, LO/z;->h:[LO/W;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LO/W;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LO/W;->m()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v2}, LO/W;->n()D

    move-result-wide v2

    cmpl-double v2, v2, p1

    if-lez v2, :cond_0

    iget-object v2, p0, LO/z;->h:[LO/W;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public d()I
    .locals 1

    iget v0, p0, LO/z;->b:I

    return v0
.end method

.method public d(I)V
    .locals 0

    iput p1, p0, LO/z;->i:I

    return-void
.end method

.method public e()F
    .locals 1

    iget v0, p0, LO/z;->r:F

    return v0
.end method

.method public f()F
    .locals 1

    iget v0, p0, LO/z;->s:F

    return v0
.end method

.method public g()Z
    .locals 1

    iget v0, p0, LO/z;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    iget v0, p0, LO/z;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, LO/z;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, LO/z;->d:Z

    return v0
.end method

.method public k()I
    .locals 1

    iget-object v0, p0, LO/z;->e:[LO/N;

    array-length v0, v0

    return v0
.end method

.method public l()LO/U;
    .locals 2

    iget-object v0, p0, LO/z;->h:[LO/W;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LO/z;->h:[LO/W;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public m()LO/U;
    .locals 2

    iget-object v0, p0, LO/z;->h:[LO/W;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LO/z;->h:[LO/W;

    iget-object v1, p0, LO/z;->h:[LO/W;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public n()Lo/X;
    .locals 1

    iget-object v0, p0, LO/z;->f:Lo/X;

    return-object v0
.end method

.method public o()I
    .locals 1

    iget v0, p0, LO/z;->o:I

    return v0
.end method

.method public p()I
    .locals 1

    iget v0, p0, LO/z;->p:I

    return v0
.end method

.method public q()I
    .locals 1

    iget v0, p0, LO/z;->v:I

    return v0
.end method

.method public r()I
    .locals 1

    iget v0, p0, LO/z;->i:I

    return v0
.end method

.method public s()Ljava/util/List;
    .locals 1

    iget-object v0, p0, LO/z;->A:Ljava/util/List;

    return-object v0
.end method

.method public t()Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v2, :cond_3

    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LO/F;

    invoke-virtual {v0}, LO/F;->a()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, LO/F;->a()I

    move-result v0

    const/4 v3, 0x5

    if-ne v0, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public u()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, LO/z;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public v()[LO/W;
    .locals 1

    iget-object v0, p0, LO/z;->h:[LO/W;

    return-object v0
.end method

.method public w()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LO/z;->k:Ljava/lang/String;

    return-object v0
.end method

.method public x()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LO/z;->E:Ljava/lang/String;

    return-object v0
.end method

.method public y()Z
    .locals 1

    iget-boolean v0, p0, LO/z;->q:Z

    return v0
.end method

.method public z()J
    .locals 2

    iget-wide v0, p0, LO/z;->t:J

    return-wide v0
.end method
