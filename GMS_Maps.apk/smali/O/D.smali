.class public LO/D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LO/H;


# instance fields
.field private a:LO/z;

.field private b:Lo/T;

.field private c:D

.field private d:D

.field private e:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LO/A;)V
    .locals 0

    invoke-direct {p0}, LO/D;-><init>()V

    return-void
.end method

.method static synthetic a(LO/D;D)D
    .locals 0

    iput-wide p1, p0, LO/D;->d:D

    return-wide p1
.end method

.method static synthetic a(LO/D;I)I
    .locals 0

    iput p1, p0, LO/D;->e:I

    return p1
.end method

.method static synthetic a(LO/D;LO/z;)LO/z;
    .locals 0

    iput-object p1, p0, LO/D;->a:LO/z;

    return-object p1
.end method

.method static synthetic a(LO/D;)Lo/T;
    .locals 1

    iget-object v0, p0, LO/D;->b:Lo/T;

    return-object v0
.end method

.method static synthetic a(LO/D;Lo/T;)Lo/T;
    .locals 0

    iput-object p1, p0, LO/D;->b:Lo/T;

    return-object p1
.end method

.method static synthetic b(LO/D;)D
    .locals 2

    iget-wide v0, p0, LO/D;->d:D

    return-wide v0
.end method

.method static synthetic b(LO/D;D)D
    .locals 0

    iput-wide p1, p0, LO/D;->c:D

    return-wide p1
.end method


# virtual methods
.method public a(D)LO/D;
    .locals 3

    new-instance v0, LO/D;

    invoke-direct {v0}, LO/D;-><init>()V

    iget-object v1, p0, LO/D;->a:LO/z;

    iput-object v1, v0, LO/D;->a:LO/z;

    iget-object v1, p0, LO/D;->b:Lo/T;

    iput-object v1, v0, LO/D;->b:Lo/T;

    iget-wide v1, p0, LO/D;->c:D

    iput-wide v1, v0, LO/D;->c:D

    iput-wide p1, v0, LO/D;->d:D

    iget v1, p0, LO/D;->e:I

    iput v1, v0, LO/D;->e:I

    return-object v0
.end method

.method public a()LO/z;
    .locals 1

    iget-object v0, p0, LO/D;->a:LO/z;

    return-object v0
.end method

.method public b()Lo/T;
    .locals 1

    iget-object v0, p0, LO/D;->b:Lo/T;

    return-object v0
.end method

.method public c()D
    .locals 2

    iget-wide v0, p0, LO/D;->c:D

    return-wide v0
.end method

.method public d()D
    .locals 2

    iget-wide v0, p0, LO/D;->d:D

    return-wide v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, LO/D;->e:I

    return v0
.end method
