.class public Lax/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static f:Lax/o;


# instance fields
.field private final a:Lcom/google/googlenav/common/io/j;

.field private volatile b:Z

.field private c:Ljava/util/Hashtable;

.field private d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

.field private final e:Ljava/lang/String;

.field private volatile g:Lax/r;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    iput-object v0, p0, Lax/o;->a:Lcom/google/googlenav/common/io/j;

    iput-boolean v1, p0, Lax/o;->b:Z

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0, v1}, Ljava/util/Hashtable;-><init>(I)V

    iput-object v0, p0, Lax/o;->c:Ljava/util/Hashtable;

    iput-object p1, p0, Lax/o;->e:Ljava/lang/String;

    return-void
.end method

.method public static declared-synchronized a()Lax/o;
    .locals 2

    const-class v1, Lax/o;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lax/o;->f:Lax/o;

    if-nez v0, :cond_0

    invoke-static {}, Lax/o;->b()V

    :cond_0
    sget-object v0, Lax/o;->f:Lax/o;

    invoke-virtual {v0}, Lax/o;->c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lax/o;->f:Lax/o;

    invoke-direct {v0}, Lax/o;->f()V

    :cond_1
    sget-object v0, Lax/o;->f:Lax/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lax/o;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 0

    invoke-direct {p0, p1}, Lax/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    return-void
.end method

.method static synthetic a(Lax/o;Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lax/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    return-void
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V
    .locals 4

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lax/o;->h()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lax/o;->a:Lcom/google/googlenav/common/io/j;

    invoke-interface {v2, v1, v0}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I

    move-result v0

    if-gtz v0, :cond_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error saving directions options: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V
    .locals 7

    const/4 v1, 0x0

    const/4 v6, 0x1

    iput-object p1, p0, Lax/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {p1, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v2

    new-instance v3, Ljava/util/Hashtable;

    invoke-direct {v3, v2}, Ljava/util/Hashtable;-><init>(I)V

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v6, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v3, p0, Lax/o;->c:Ljava/util/Hashtable;

    iput-boolean v1, p0, Lax/o;->b:Z

    if-nez p2, :cond_2

    iget-object v0, p0, Lax/o;->g:Lax/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lax/o;->g:Lax/r;

    invoke-interface {v0}, Lax/r;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lax/o;->g:Lax/r;

    :cond_1
    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/ag;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v0, v6, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lax/p;

    invoke-direct {v2, p0, v0}, Lax/p;-><init>(Lax/o;Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->run()V

    :cond_2
    return-void
.end method

.method public static declared-synchronized b()V
    .locals 3

    const-class v1, Lax/o;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->f()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lax/o;->f:Lax/o;

    if-eqz v2, :cond_0

    sget-object v2, Lax/o;->f:Lax/o;

    iget-object v2, v2, Lax/o;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Lax/o;

    invoke-direct {v2, v0}, Lax/o;-><init>(Ljava/lang/String;)V

    sput-object v2, Lax/o;->f:Lax/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private e()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lax/o;->b:Z

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    new-instance v1, Lax/q;

    invoke-direct {v1, p0}, Lax/q;-><init>(Lax/o;)V

    invoke-virtual {v0, v1}, Law/h;->c(Law/g;)V

    return-void
.end method

.method private f()V
    .locals 3

    const/4 v2, 0x1

    invoke-direct {p0}, Lax/o;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lax/o;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;Z)V

    :cond_0
    return-void
.end method

.method private g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lax/o;->a:Lcom/google/googlenav/common/io/j;

    invoke-direct {p0}, Lax/o;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v2

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lcom/google/wireless/googlenav/proto/j2me/ag;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    if-eqz v2, :cond_0

    array-length v3, v2

    if-nez v3, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-virtual {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OptionDefinitionBlock_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lax/o;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Ljava/util/Vector;
    .locals 11

    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lax/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    goto :goto_0

    :pswitch_0
    move v0, v1

    :goto_1
    iget-object v3, p0, Lax/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    new-instance v3, Ljava/util/Vector;

    invoke-direct {v3, v6}, Ljava/util/Vector;-><init>(I)V

    move v5, v1

    :goto_2
    if-ge v5, v6, :cond_3

    iget-object v4, p0, Lax/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    invoke-virtual {v4, v2, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v8

    move v4, v1

    :goto_3
    if-ge v4, v8, :cond_1

    invoke-virtual {v7, v10, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(II)I

    move-result v9

    if-ne v9, v0, :cond_2

    invoke-virtual {v3, v7}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :pswitch_1
    move v0, v2

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    move-object v0, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lax/r;)V
    .locals 0

    iput-object p1, p0, Lax/o;->g:Lax/r;

    return-void
.end method

.method public a([I)V
    .locals 3

    iget-boolean v0, p0, Lax/o;->b:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    aget v1, p1, v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lax/o;->c:Ljava/util/Hashtable;

    invoke-virtual {v2, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0}, Lax/o;->e()V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public c()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    iget-object v0, p0, Lax/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lax/o;->d:Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lax/o;->b:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lax/o;->e()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
