.class public Lax/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/googlenav/E;
.implements Lcom/google/googlenav/common/util/n;


# instance fields
.field protected a:Lax/a;

.field private b:Lax/t;

.field private c:I

.field private d:I

.field private e:I

.field private f:LaN/B;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Lax/t;IIILaN/B;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lax/m;->i:Z

    iput-object p1, p0, Lax/m;->b:Lax/t;

    iput p2, p0, Lax/m;->e:I

    iput p3, p0, Lax/m;->c:I

    iput p4, p0, Lax/m;->d:I

    iput-object p5, p0, Lax/m;->f:LaN/B;

    iput-object p6, p0, Lax/m;->g:Ljava/lang/String;

    iput v0, p0, Lax/m;->h:I

    const/4 v0, 0x0

    iput-object v0, p0, Lax/m;->a:Lax/a;

    return-void
.end method


# virtual methods
.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, Lax/m;->f:LaN/B;

    return-object v0
.end method

.method public a(LaN/B;)V
    .locals 0

    iput-object p1, p0, Lax/m;->f:LaN/B;

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lax/m;->i:Z

    return-void
.end method

.method public b()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lax/m;->b:Lax/t;

    invoke-virtual {v0}, Lax/t;->h()Lo/D;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/common/collect/ImmutableList;->f()Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/common/collect/ImmutableList;->a(Ljava/lang/Object;)Lcom/google/common/collect/ImmutableList;

    move-result-object v0

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lax/m;->h:I

    return-void
.end method

.method public c()B
    .locals 3

    const/4 v0, 0x3

    iget v1, p0, Lax/m;->e:I

    if-nez v1, :cond_1

    iget v1, p0, Lax/m;->c:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v0, 0x4

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lax/m;->c:I

    if-ne v1, v0, :cond_0

    const/4 v0, 0x5

    goto :goto_0
.end method

.method public d()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public g()Lcom/google/googlenav/common/util/l;
    .locals 3

    iget-object v0, p0, Lax/m;->b:Lax/t;

    invoke-virtual {v0}, Lax/t;->I()I

    move-result v0

    add-int/lit8 v0, v0, 0x21

    iget-object v1, p0, Lax/m;->g:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lax/m;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    new-instance v1, Lcom/google/googlenav/common/util/l;

    const-string v2, "DirectionsFeature"

    invoke-direct {v1, v2, v0}, Lcom/google/googlenav/common/util/l;-><init>(Ljava/lang/String;I)V

    return-object v1
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lax/m;->c:I

    return v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lax/m;->d:I

    return v0
.end method

.method public m()Lax/t;
    .locals 1

    iget-object v0, p0, Lax/m;->b:Lax/t;

    return-object v0
.end method

.method public n()Z
    .locals 2

    iget-object v0, p0, Lax/m;->b:Lax/t;

    invoke-virtual {v0}, Lax/t;->E()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lax/m;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()Z
    .locals 2

    iget v0, p0, Lax/m;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 1

    iget v0, p0, Lax/m;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 2

    iget v0, p0, Lax/m;->c:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 2

    iget v0, p0, Lax/m;->c:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lax/m;->g:Ljava/lang/String;

    return-object v0
.end method

.method public u()Z
    .locals 1

    invoke-virtual {p0}, Lax/m;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    iget-object v0, p0, Lax/m;->a:Lax/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lax/m;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()I
    .locals 1

    iget v0, p0, Lax/m;->h:I

    return v0
.end method

.method public x()Z
    .locals 1

    iget v0, p0, Lax/m;->h:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()I
    .locals 1

    iget v0, p0, Lax/m;->e:I

    return v0
.end method

.method public z()Z
    .locals 1

    iget-boolean v0, p0, Lax/m;->i:Z

    return v0
.end method
