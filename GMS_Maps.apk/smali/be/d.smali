.class public Lbe/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbj/H;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILandroid/view/View$OnClickListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbe/d;->a:Ljava/lang/String;

    iput p2, p0, Lbe/d;->b:I

    iput-object p3, p0, Lbe/d;->c:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public a(Landroid/view/View;)Lbj/bB;
    .locals 2

    new-instance v1, Lbe/f;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lbe/f;-><init>(Lbe/e;)V

    const v0, 0x7f100257

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v1, v0}, Lbe/f;->a(Lbe/f;Landroid/widget/TextView;)Landroid/widget/TextView;

    const v0, 0x7f100256

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lbe/f;->a(Lbe/f;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    return-object v1
.end method

.method public a(Lcom/google/googlenav/ui/e;Lbj/bB;)V
    .locals 2

    check-cast p2, Lbe/f;

    invoke-static {p2}, Lbe/f;->a(Lbe/f;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lbe/d;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Lbe/f;->b(Lbe/f;)Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p0, Lbe/d;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public b()I
    .locals 1

    const v0, 0x7f0400b6

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Landroid/view/View$OnClickListener;
    .locals 1

    iget-object v0, p0, Lbe/d;->c:Landroid/view/View$OnClickListener;

    return-object v0
.end method
