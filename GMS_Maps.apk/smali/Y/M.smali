.class LY/M;
.super Ljava/util/concurrent/locks/ReentrantLock;
.source "SourceFile"


# instance fields
.field final a:LY/n;

.field volatile b:I

.field c:I

.field d:I

.field e:I

.field volatile f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

.field final g:J

.field final h:Ljava/lang/ref/ReferenceQueue;

.field final i:Ljava/lang/ref/ReferenceQueue;

.field final j:Ljava/util/Queue;

.field final k:Ljava/util/concurrent/atomic/AtomicInteger;

.field final l:Ljava/util/Queue;

.field final m:Ljava/util/Queue;

.field final n:LY/c;


# direct methods
.method constructor <init>(LY/n;IJLY/c;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LY/M;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, LY/M;->a:LY/n;

    iput-wide p3, p0, LY/M;->g:J

    iput-object p5, p0, LY/M;->n:LY/c;

    invoke-virtual {p0, p2}, LY/M;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v0

    invoke-virtual {p0, v0}, LY/M;->a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V

    invoke-virtual {p1}, LY/n;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :goto_0
    iput-object v0, p0, LY/M;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, LY/n;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v1}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    :cond_0
    iput-object v1, p0, LY/M;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {p1}, LY/n;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    :goto_1
    iput-object v0, p0, LY/M;->j:Ljava/util/Queue;

    invoke-virtual {p1}, LY/n;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, LY/aj;

    invoke-direct {v0}, LY/aj;-><init>()V

    :goto_2
    iput-object v0, p0, LY/M;->l:Ljava/util/Queue;

    invoke-virtual {p1}, LY/n;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, LY/r;

    invoke-direct {v0}, LY/r;-><init>()V

    :goto_3
    iput-object v0, p0, LY/M;->m:Ljava/util/Queue;

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, LY/n;->q()Ljava/util/Queue;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-static {}, LY/n;->q()Ljava/util/Queue;

    move-result-object v0

    goto :goto_2

    :cond_4
    invoke-static {}, LY/n;->q()Ljava/util/Queue;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method a(Ljava/lang/Object;I)LY/I;
    .locals 6

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LY/M;->c(J)V

    iget-object v2, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_2

    invoke-interface {v1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1}, LY/L;->c()I

    move-result v5

    if-ne v5, p2, :cond_1

    if-eqz v4, :cond_1

    iget-object v5, p0, LY/M;->a:LY/n;

    iget-object v5, v5, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v5, p1, v4}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, LY/L;->a()LY/Z;

    move-result-object v2

    invoke-interface {v2}, LY/Z;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    :goto_1
    return-object v0

    :cond_0
    :try_start_1
    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    new-instance v0, LY/I;

    invoke-direct {v0, v2}, LY/I;-><init>(LY/Z;)V

    invoke-interface {v1, v0}, LY/L;->a(LY/Z;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-interface {v1}, LY/L;->b()LY/L;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget v1, p0, LY/M;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LY/M;->d:I

    new-instance v1, LY/I;

    invoke-direct {v1}, LY/I;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, LY/M;->a(Ljava/lang/Object;ILY/L;)LY/L;

    move-result-object v0

    invoke-interface {v0, v1}, LY/L;->a(LY/Z;)V

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method a(LY/L;LY/L;)LY/L;
    .locals 3

    invoke-interface {p1}, LY/L;->a()LY/Z;

    move-result-object v0

    iget-object v1, p0, LY/M;->a:LY/n;

    iget-object v1, v1, LY/n;->s:LY/u;

    invoke-virtual {v1, p0, p1, p2}, LY/u;->a(LY/M;LY/L;LY/L;)LY/L;

    move-result-object v1

    iget-object v2, p0, LY/M;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-interface {v0, v2, v1}, LY/Z;->a(Ljava/lang/ref/ReferenceQueue;LY/L;)LY/Z;

    move-result-object v0

    invoke-interface {v1, v0}, LY/L;->a(LY/Z;)V

    return-object v1
.end method

.method a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;
    .locals 1

    invoke-virtual {p0, p3, p4, p5, p6}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    iget-object v0, p0, LY/M;->l:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0, p2}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    invoke-interface {p5}, LY/Z;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p5, v0}, LY/Z;->a(Ljava/lang/Object;)V

    :goto_0
    return-object p1

    :cond_0
    invoke-virtual {p0, p1, p2}, LY/M;->b(LY/L;LY/L;)LY/L;

    move-result-object p1

    goto :goto_0
.end method

.method a(Ljava/lang/Object;IJ)LY/L;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, LY/M;->b(Ljava/lang/Object;I)LY/L;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LY/M;->a:LY/n;

    invoke-virtual {v2, v1, p3, p4}, LY/n;->b(LY/L;J)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, p3, p4}, LY/M;->a(J)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method a(Ljava/lang/Object;ILY/L;)LY/L;
    .locals 1

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->s:LY/u;

    invoke-virtual {v0, p0, p1, p2, p3}, LY/u;->a(LY/M;Ljava/lang/Object;ILY/L;)LY/L;

    move-result-object v0

    return-object v0
.end method

.method a(LY/L;Ljava/lang/Object;ILjava/lang/Object;JLY/k;)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LY/L;->h()J

    move-result-wide v0

    sub-long v0, p5, v0

    iget-object v2, p0, LY/M;->a:LY/n;

    iget-wide v2, v2, LY/n;->o:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0, p2, p3, p7}, LY/M;->c(Ljava/lang/Object;ILY/k;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p4, v0

    :cond_0
    return-object p4
.end method

.method a(LY/L;Ljava/lang/Object;LY/Z;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x1

    invoke-interface {p3}, LY/Z;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    invoke-static {p1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "Recursive load"

    invoke-static {v0, v2}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    :try_start_0
    invoke-interface {p3}, LY/Z;->e()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, LY/l;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CacheLoader returned null for key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LY/l;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v2, p0, LY/M;->n:LY/c;

    invoke-interface {v2, v1}, LY/c;->b(I)V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v2, p0, LY/M;->a:LY/n;

    iget-object v2, v2, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v2}, Lcom/google/common/base/ae;->a()J

    move-result-wide v2

    invoke-virtual {p0, p1, v2, v3}, LY/M;->a(LY/L;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v2, p0, LY/M;->n:LY/c;

    invoke-interface {v2, v1}, LY/c;->b(I)V

    return-object v0
.end method

.method a(Ljava/lang/Object;ILY/I;LY/k;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p3, p1, p4}, LY/I;->a(Ljava/lang/Object;LY/k;)Lae/i;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, LY/M;->a(Ljava/lang/Object;ILY/I;Lae/i;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/lang/Object;ILY/I;Lae/i;)Ljava/lang/Object;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p4}, Lae/q;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v0, LY/l;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CacheLoader returned null for key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LY/l;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    if-nez v1, :cond_0

    iget-object v1, p0, LY/M;->n:LY/c;

    invoke-virtual {p3}, LY/I;->f()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, LY/c;->b(J)V

    invoke-virtual {p0, p1, p2, p3}, LY/M;->a(Ljava/lang/Object;ILY/I;)Z

    :cond_0
    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LY/M;->n:LY/c;

    invoke-virtual {p3}, LY/I;->f()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LY/c;->a(J)V

    invoke-virtual {p0, p1, p2, p3, v1}, LY/M;->a(Ljava/lang/Object;ILY/I;Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    iget-object v0, p0, LY/M;->n:LY/c;

    invoke-virtual {p3}, LY/I;->f()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LY/c;->b(J)V

    invoke-virtual {p0, p1, p2, p3}, LY/M;->a(Ljava/lang/Object;ILY/I;)Z

    :cond_2
    return-object v1
.end method

.method a(Ljava/lang/Object;ILY/k;)Ljava/lang/Object;
    .locals 8

    :try_start_0
    iget v0, p0, LY/M;->b:I

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, LY/M;->b(Ljava/lang/Object;I)LY/L;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v5

    invoke-virtual {p0, v1, v5, v6}, LY/M;->c(LY/L;J)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, v1, v5, v6}, LY/M;->a(LY/L;J)V

    iget-object v0, p0, LY/M;->n:LY/c;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, LY/c;->a(I)V

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, LY/M;->a(LY/L;Ljava/lang/Object;ILjava/lang/Object;JLY/k;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, LY/M;->m()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, LY/L;->a()LY/Z;

    move-result-object v0

    invoke-interface {v0}, LY/Z;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1, p1, v0}, LY/M;->a(LY/L;Ljava/lang/Object;LY/Z;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, LY/M;->m()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {p0, p1, p2, p3}, LY/M;->b(Ljava/lang/Object;ILY/k;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, LY/M;->m()V

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v0

    :try_start_3
    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v2, v0, Ljava/lang/Error;

    if-eqz v2, :cond_2

    new-instance v1, Lae/d;

    check-cast v0, Ljava/lang/Error;

    invoke-direct {v1, v0}, Lae/d;-><init>(Ljava/lang/Error;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->m()V

    throw v0

    :cond_2
    :try_start_4
    instance-of v2, v0, Ljava/lang/RuntimeException;

    if-eqz v2, :cond_3

    new-instance v1, Lae/p;

    invoke-direct {v1, v0}, Lae/p;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 12

    const/4 v7, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, LY/M;->c(J)V

    iget-object v10, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v11, p2, v0

    invoke-virtual {v10, v11}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->c()I

    move-result v0

    if-ne v0, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v0, p1, v3}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    invoke-interface {v5}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {v5}, LY/Z;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    sget-object v6, LY/an;->c:LY/an;

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v10, v11, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v7

    :goto_1
    return-object v0

    :cond_1
    :try_start_1
    iget v1, p0, LY/M;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LY/M;->d:I

    sget-object v1, LY/an;->b:LY/an;

    invoke-virtual {p0, p1, p2, v5, v1}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-wide v5, v8

    invoke-virtual/range {v1 .. v6}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v7

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Z)Ljava/lang/Object;
    .locals 9

    const/4 v6, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, LY/M;->c(J)V

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, LY/M;->e:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, LY/M;->k()V

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, 0x1

    :cond_0
    iget-object v7, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_5

    invoke-interface {v1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1}, LY/L;->c()I

    move-result v3

    if-ne v3, p2, :cond_4

    if-eqz v2, :cond_4

    iget-object v3, p0, LY/M;->a:LY/n;

    iget-object v3, v3, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v3, p1, v2}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, LY/L;->a()LY/Z;

    move-result-object v0

    invoke-interface {v0}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_2

    iget v2, p0, LY/M;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LY/M;->d:I

    invoke-interface {v0}, LY/Z;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, LY/an;->c:LY/an;

    invoke-virtual {p0, p1, p2, v0, v2}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    iget v0, p0, LY/M;->b:I

    :goto_1
    iput v0, p0, LY/M;->b:I

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v6

    :goto_2
    return-object v0

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    :try_start_1
    invoke-virtual/range {v0 .. v5}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {p0, v1, v4, v5}, LY/M;->b(LY/L;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v7

    goto :goto_2

    :cond_3
    :try_start_2
    iget v2, p0, LY/M;->d:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LY/M;->d:I

    sget-object v2, LY/an;->b:LY/an;

    invoke-virtual {p0, p1, p2, v0, v2}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v7

    goto :goto_2

    :cond_4
    :try_start_3
    invoke-interface {v1}, LY/L;->b()LY/L;

    move-result-object v1

    goto :goto_0

    :cond_5
    iget v1, p0, LY/M;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LY/M;->d:I

    invoke-virtual {p0, p1, p2, v0}, LY/M;->a(Ljava/lang/Object;ILY/L;)LY/L;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {v7, v8, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->b:I

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v6

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .locals 1

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-direct {v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;-><init>(I)V

    return-object v0
.end method

.method a()V
    .locals 1

    invoke-virtual {p0}, LY/M;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, LY/M;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    throw v0
.end method

.method a(J)V
    .locals 1

    invoke-virtual {p0}, LY/M;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0, p1, p2}, LY/M;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    throw v0
.end method

.method a(LY/L;)V
    .locals 1

    sget-object v0, LY/an;->c:LY/an;

    invoke-virtual {p0, p1, v0}, LY/M;->a(LY/L;LY/an;)V

    iget-object v0, p0, LY/M;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method a(LY/L;IJ)V
    .locals 1

    invoke-virtual {p0}, LY/M;->h()V

    iget v0, p0, LY/M;->c:I

    add-int/2addr v0, p2

    iput v0, p0, LY/M;->c:I

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p3, p4}, LY/L;->a(J)V

    :cond_0
    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1, p3, p4}, LY/L;->b(J)V

    :cond_1
    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, LY/M;->l:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method a(LY/L;J)V
    .locals 1

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, p3}, LY/L;->a(J)V

    :cond_0
    iget-object v0, p0, LY/M;->j:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method a(LY/L;LY/an;)V
    .locals 3

    invoke-interface {p1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p1}, LY/L;->c()I

    move-result v1

    invoke-interface {p1}, LY/L;->a()LY/Z;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2, p2}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    return-void
.end method

.method a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V
    .locals 4

    invoke-interface {p1}, LY/L;->a()LY/Z;

    move-result-object v1

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->l:LY/av;

    invoke-interface {v0, p2, p3}, LY/av;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-ltz v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v3, "Weights must be non-negative"

    invoke-static {v0, v3}, Lcom/google/common/base/J;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->j:LY/P;

    invoke-virtual {v0, p0, p1, p3, v2}, LY/P;->a(LY/M;LY/L;Ljava/lang/Object;I)LY/Z;

    move-result-object v0

    invoke-interface {p1, v0}, LY/L;->a(LY/Z;)V

    invoke-virtual {p0, p1, v2, p4, p5}, LY/M;->a(LY/L;IJ)V

    invoke-interface {v1, p3}, LY/Z;->a(Ljava/lang/Object;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Ljava/lang/Object;ILY/Z;LY/an;)V
    .locals 2

    iget v0, p0, LY/M;->c:I

    invoke-interface {p3}, LY/Z;->a()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, LY/M;->c:I

    invoke-virtual {p4}, LY/an;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LY/M;->n:LY/c;

    invoke-interface {v0}, LY/c;->a()V

    :cond_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->p:Ljava/util/Queue;

    sget-object v1, LY/n;->w:Ljava/util/Queue;

    if-eq v0, v1, :cond_1

    invoke-interface {p3}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, LY/au;

    invoke-direct {v1, p1, v0, p4}, LY/au;-><init>(Ljava/lang/Object;Ljava/lang/Object;LY/an;)V

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->p:Ljava/util/Queue;

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method a(Ljava/util/concurrent/atomic/AtomicReferenceArray;)V
    .locals 4

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, LY/M;->e:I

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LY/M;->e:I

    int-to-long v0, v0

    iget-wide v2, p0, LY/M;->g:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget v0, p0, LY/M;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->e:I

    :cond_0
    iput-object p1, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    return-void
.end method

.method a(LY/L;I)Z
    .locals 9

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v7, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    sget-object v6, LY/an;->c:LY/an;

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    :goto_1
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method a(LY/L;ILY/an;)Z
    .locals 9

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v7, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v8, p2, v0

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_1

    if-ne v2, p1, :cond_0

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    move-object v0, p0

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-interface {v2}, LY/L;->b()LY/L;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Ljava/lang/Object;ILY/I;)Z
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v3, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v4, p2, v0

    invoke-virtual {v3, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2}, LY/L;->c()I

    move-result v6

    if-ne v6, p2, :cond_2

    if-eqz v5, :cond_2

    iget-object v6, p0, LY/M;->a:LY/n;

    iget-object v6, v6, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v6, p1, v5}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    if-ne v5, p3, :cond_1

    invoke-virtual {p3}, LY/I;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, LY/I;->g()LY/Z;

    move-result-object v0

    invoke-interface {v2, v0}, LY/L;->a(LY/Z;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    :goto_2
    return v0

    :cond_0
    :try_start_1
    invoke-virtual {p0, v0, v2}, LY/M;->b(LY/L;LY/L;)LY/L;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0

    :cond_1
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move v0, v1

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move v0, v1

    goto :goto_2
.end method

.method a(Ljava/lang/Object;ILY/I;Ljava/lang/Object;)Z
    .locals 11

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, LY/M;->c(J)V

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v7, v0, 0x1

    iget-object v8, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_5

    invoke-interface {v1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, LY/L;->c()I

    move-result v10

    if-ne v10, p2, :cond_4

    if-eqz v3, :cond_4

    iget-object v10, p0, LY/M;->a:LY/n;

    iget-object v10, v10, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v10, p1, v3}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, LY/L;->a()LY/Z;

    move-result-object v0

    invoke-interface {v0}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    if-ne p3, v0, :cond_3

    :cond_0
    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    invoke-virtual {p3}, LY/I;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v3, :cond_2

    sget-object v0, LY/an;->c:LY/an;

    :goto_1
    invoke-virtual {p0, p1, p2, p3, v0}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    add-int/lit8 v0, v7, -0x1

    move v7, v0

    :cond_1
    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    iput v7, p0, LY/M;->b:I

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move v0, v6

    :goto_2
    return v0

    :cond_2
    :try_start_1
    sget-object v0, LY/an;->b:LY/an;

    goto :goto_1

    :cond_3
    new-instance v0, LY/ah;

    const/4 v1, 0x0

    invoke-direct {v0, p4, v1}, LY/ah;-><init>(Ljava/lang/Object;I)V

    sget-object v1, LY/an;->b:LY/an;

    invoke-virtual {p0, p1, p2, v0, v1}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move v0, v2

    goto :goto_2

    :cond_4
    :try_start_2
    invoke-interface {v1}, LY/L;->b()LY/L;

    move-result-object v1

    goto :goto_0

    :cond_5
    iget v1, p0, LY/M;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LY/M;->d:I

    invoke-virtual {p0, p1, p2, v0}, LY/M;->a(Ljava/lang/Object;ILY/L;)LY/L;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p4

    invoke-virtual/range {v0 .. v5}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {v8, v9, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v7, p0, LY/M;->b:I

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move v0, v6

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method a(Ljava/lang/Object;ILY/Z;)Z
    .locals 9

    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    iget-object v7, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v8, p2, v1

    invoke-virtual {v7, v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->c()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, LY/M;->a:LY/n;

    iget-object v4, v4, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v4, p1, v3}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v4

    if-ne v4, p3, :cond_1

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    sget-object v6, LY/an;->c:LY/an;

    move-object v0, p0

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v8, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LY/M;->n()V

    :cond_0
    :goto_1
    return v0

    :cond_1
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :cond_2
    :try_start_1
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->isHeldByCurrentThread()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p0}, LY/M;->n()V

    :cond_4
    throw v0
.end method

.method a(Ljava/lang/Object;ILjava/lang/Object;Ljava/lang/Object;)Z
    .locals 11

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v7

    invoke-virtual {p0, v7, v8}, LY/M;->c(J)V

    iget-object v9, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v10, p2, v0

    invoke-virtual {v9, v10}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v0, p1, v3}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    invoke-interface {v5}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-interface {v5}, LY/Z;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, -0x1

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    sget-object v6, LY/an;->c:LY/an;

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v9, v10, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    :goto_1
    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, LY/M;->a:LY/n;

    iget-object v1, v1, LY/n;->h:Lcom/google/common/base/t;

    invoke-virtual {v1, p3, v0}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    sget-object v0, LY/an;->b:LY/an;

    invoke-virtual {p0, p1, p2, v5, v0}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    move-object v1, p0

    move-object v3, p1

    move-object v4, p4

    move-wide v5, v7

    invoke-virtual/range {v1 .. v6}, LY/M;->a(LY/L;Ljava/lang/Object;Ljava/lang/Object;J)V

    invoke-virtual {p0}, LY/M;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :cond_2
    :try_start_2
    invoke-virtual {p0, v2, v7, v8}, LY/M;->b(LY/L;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :cond_3
    :try_start_3
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method b(I)LY/L;
    .locals 2

    iget-object v0, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    return-object v0
.end method

.method b(LY/L;LY/L;)LY/L;
    .locals 3

    iget v1, p0, LY/M;->b:I

    invoke-interface {p2}, LY/L;->b()LY/L;

    move-result-object v0

    :goto_0
    if-eq p1, p2, :cond_1

    invoke-virtual {p0, p1}, LY/M;->b(LY/L;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, p1}, LY/M;->a(LY/L;)V

    add-int/lit8 v1, v1, -0x1

    :goto_1
    invoke-interface {p1}, LY/L;->b()LY/L;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v0}, LY/M;->a(LY/L;LY/L;)LY/L;

    move-result-object v0

    goto :goto_1

    :cond_1
    iput v1, p0, LY/M;->b:I

    return-object v0
.end method

.method b(Ljava/lang/Object;I)LY/L;
    .locals 3

    invoke-virtual {p0, p2}, LY/M;->b(I)LY/L;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    invoke-interface {v0}, LY/L;->c()I

    move-result v1

    if-eq v1, p2, :cond_1

    :cond_0
    :goto_1
    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, LY/L;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, LY/M;->a()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, LY/M;->a:LY/n;

    iget-object v2, v2, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v2, p1, v1}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_2
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method b(Ljava/lang/Object;ILY/I;LY/k;)Lae/i;
    .locals 6

    invoke-virtual {p3, p1, p4}, LY/I;->a(Ljava/lang/Object;LY/k;)Lae/i;

    move-result-object v5

    new-instance v0, LY/N;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LY/N;-><init>(LY/M;Ljava/lang/Object;ILY/I;Lae/i;)V

    sget-object v1, LY/n;->b:Lae/k;

    invoke-interface {v5, v0, v1}, Lae/i;->a(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-object v5
.end method

.method b(Ljava/lang/Object;ILY/k;)Ljava/lang/Object;
    .locals 18

    const/4 v7, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    invoke-virtual/range {p0 .. p0}, LY/M;->lock()V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LY/M;->a:LY/n;

    iget-object v4, v4, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v4}, Lcom/google/common/base/ae;->a()J

    move-result-wide v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, LY/M;->c(J)V

    move-object/from16 v0, p0

    iget v4, v0, LY/M;->b:I

    add-int/lit8 v12, v4, -0x1

    move-object/from16 v0, p0

    iget-object v13, v0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v13}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    and-int v14, p2, v4

    invoke-virtual {v13, v14}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LY/L;

    move-object v6, v4

    :goto_0
    if-eqz v6, :cond_7

    invoke-interface {v6}, LY/L;->d()Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v6}, LY/L;->c()I

    move-result v9

    move/from16 v0, p2

    if-ne v9, v0, :cond_3

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v9, v0, LY/M;->a:LY/n;

    iget-object v9, v9, LY/n;->g:Lcom/google/common/base/t;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v15}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v6}, LY/L;->a()LY/Z;

    move-result-object v9

    invoke-interface {v9}, LY/Z;->c()Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v7, 0x0

    move-object v8, v9

    :goto_1
    if-eqz v7, :cond_6

    new-instance v5, LY/I;

    invoke-direct {v5}, LY/I;-><init>()V

    if-nez v6, :cond_4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-virtual {v0, v1, v2, v4}, LY/M;->a(Ljava/lang/Object;ILY/L;)LY/L;

    move-result-object v4

    invoke-interface {v4, v5}, LY/L;->a(LY/Z;)V

    invoke-virtual {v13, v14, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v17, v5

    move-object v5, v4

    move-object/from16 v4, v17

    :goto_2
    invoke-virtual/range {p0 .. p0}, LY/M;->unlock()V

    invoke-virtual/range {p0 .. p0}, LY/M;->n()V

    if-eqz v7, :cond_5

    :try_start_1
    monitor-enter v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    invoke-virtual {v0, v1, v2, v4, v3}, LY/M;->a(Ljava/lang/Object;ILY/I;LY/k;)Ljava/lang/Object;

    move-result-object v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object/from16 v0, p0

    iget-object v5, v0, LY/M;->n:LY/c;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LY/c;->b(I)V

    :goto_3
    return-object v4

    :cond_0
    :try_start_3
    invoke-interface {v9}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_1

    sget-object v7, LY/an;->c:LY/an;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v15, v1, v9, v7}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, LY/M;->l:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v7, v0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v7, v6}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iput v12, v0, LY/M;->b:I

    move v7, v8

    move-object v8, v9

    goto :goto_1

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, LY/M;->a:LY/n;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v10, v11}, LY/n;->b(LY/L;J)Z

    move-result v16

    if-eqz v16, :cond_2

    sget-object v7, LY/an;->d:LY/an;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v15, v1, v9, v7}, LY/M;->a(Ljava/lang/Object;ILY/Z;LY/an;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v4

    invoke-virtual/range {p0 .. p0}, LY/M;->unlock()V

    invoke-virtual/range {p0 .. p0}, LY/M;->n()V

    throw v4

    :cond_2
    :try_start_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v10, v11}, LY/M;->b(LY/L;J)V

    move-object/from16 v0, p0

    iget-object v4, v0, LY/M;->n:LY/c;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LY/c;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-virtual/range {p0 .. p0}, LY/M;->unlock()V

    invoke-virtual/range {p0 .. p0}, LY/M;->n()V

    move-object v4, v7

    goto :goto_3

    :cond_3
    :try_start_5
    invoke-interface {v6}, LY/L;->b()LY/L;

    move-result-object v6

    goto/16 :goto_0

    :cond_4
    invoke-interface {v6, v5}, LY/L;->a(LY/Z;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-object v4, v5

    move-object v5, v6

    goto/16 :goto_2

    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :catchall_2
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, LY/M;->n:LY/c;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LY/c;->b(I)V

    throw v4

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v5, v1, v8}, LY/M;->a(LY/L;Ljava/lang/Object;LY/Z;)Ljava/lang/Object;

    move-result-object v4

    goto :goto_3

    :cond_6
    move-object v4, v5

    move-object v5, v6

    goto/16 :goto_2

    :cond_7
    move/from16 v17, v8

    move-object v8, v7

    move/from16 v7, v17

    goto/16 :goto_1
.end method

.method b()V
    .locals 1

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LY/M;->c()V

    :cond_0
    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LY/M;->d()V

    :cond_1
    return-void
.end method

.method b(J)V
    .locals 3

    invoke-virtual {p0}, LY/M;->h()V

    :cond_0
    iget-object v0, p0, LY/M;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    if-eqz v0, :cond_1

    iget-object v1, p0, LY/M;->a:LY/n;

    invoke-virtual {v1, v0, p1, p2}, LY/n;->b(LY/L;J)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, LY/L;->c()I

    move-result v1

    sget-object v2, LY/an;->d:LY/an;

    invoke-virtual {p0, v0, v1, v2}, LY/M;->a(LY/L;ILY/an;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    if-eqz v0, :cond_2

    iget-object v1, p0, LY/M;->a:LY/n;

    invoke-virtual {v1, v0, p1, p2}, LY/n;->b(LY/L;J)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, LY/L;->c()I

    move-result v1

    sget-object v2, LY/an;->d:LY/an;

    invoke-virtual {p0, v0, v1, v2}, LY/M;->a(LY/L;ILY/an;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    return-void
.end method

.method b(LY/L;J)V
    .locals 1

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1, p2, p3}, LY/L;->a(J)V

    :cond_0
    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method b(LY/L;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-interface {p1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, LY/L;->a()LY/Z;

    move-result-object v1

    invoke-interface {v1}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-interface {v1}, LY/Z;->d()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b(Ljava/lang/Object;ILjava/lang/Object;)Z
    .locals 10

    const/4 v7, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LY/M;->c(J)V

    iget v0, p0, LY/M;->b:I

    add-int/lit8 v0, v0, -0x1

    iget-object v8, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    and-int v9, p2, v0

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_4

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->c()I

    move-result v0

    if-ne v0, p2, :cond_3

    if-eqz v3, :cond_3

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v0, p1, v3}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    invoke-interface {v5}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v4, p0, LY/M;->a:LY/n;

    iget-object v4, v4, LY/n;->h:Lcom/google/common/base/t;

    invoke-virtual {v4, p3, v0}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v6, LY/an;->a:LY/an;

    :goto_1
    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I

    sget-object v0, LY/an;->a:LY/an;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v6, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move v7, v0

    :goto_3
    return v7

    :cond_0
    if-nez v0, :cond_1

    :try_start_1
    invoke-interface {v5}, LY/Z;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v6, LY/an;->c:LY/an;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_3

    :cond_2
    move v0, v7

    goto :goto_2

    :cond_3
    :try_start_2
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method c(LY/L;J)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-interface {p1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LY/M;->a()V

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, LY/L;->a()LY/Z;

    move-result-object v1

    invoke-interface {v1}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, LY/M;->a()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, LY/M;->a:LY/n;

    invoke-virtual {v2, p1, p2, p3}, LY/n;->b(LY/L;J)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, p2, p3}, LY/M;->a(J)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 8

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, LY/M;->b:I

    if-eqz v1, :cond_2

    iget-object v1, p0, LY/M;->a:LY/n;

    iget-object v1, v1, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v1}, Lcom/google/common/base/ae;->a()J

    move-result-wide v5

    invoke-virtual {p0, p1, p2, v5, v6}, LY/M;->a(Ljava/lang/Object;IJ)LY/L;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LY/M;->m()V

    :goto_0
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, LY/L;->a()LY/Z;

    move-result-object v2

    invoke-interface {v2}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0, v1, v5, v6}, LY/M;->a(LY/L;J)V

    invoke-interface {v1}, LY/L;->d()Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v7, v0, LY/n;->u:LY/k;

    move-object v0, p0

    move v3, p2

    invoke-virtual/range {v0 .. v7}, LY/M;->a(LY/L;Ljava/lang/Object;ILjava/lang/Object;JLY/k;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    invoke-virtual {p0}, LY/M;->m()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-virtual {p0}, LY/M;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    invoke-virtual {p0}, LY/M;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->m()V

    throw v0
.end method

.method c(Ljava/lang/Object;ILY/k;)Ljava/lang/Object;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2}, LY/M;->a(Ljava/lang/Object;I)LY/I;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2, v1, p3}, LY/M;->b(Ljava/lang/Object;ILY/I;LY/k;)Lae/i;

    move-result-object v1

    invoke-interface {v1}, Lae/i;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v1}, Lae/i;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method c()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LY/M;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, LY/L;

    iget-object v2, p0, LY/M;->a:LY/n;

    invoke-virtual {v2, v0}, LY/n;->a(LY/L;)V

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method c(J)V
    .locals 0

    invoke-virtual {p0, p1, p2}, LY/M;->d(J)V

    return-void
.end method

.method d()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LY/M;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, LY/Z;

    iget-object v2, p0, LY/M;->a:LY/n;

    invoke-virtual {v2, v0}, LY/n;->a(LY/Z;)V

    add-int/lit8 v0, v1, 0x1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method d(J)V
    .locals 2

    invoke-virtual {p0}, LY/M;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, LY/M;->b()V

    invoke-virtual {p0, p1, p2}, LY/M;->b(J)V

    iget-object v0, p0, LY/M;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    throw v0
.end method

.method d(Ljava/lang/Object;I)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, LY/M;->b:I

    if-eqz v1, :cond_2

    iget-object v1, p0, LY/M;->a:LY/n;

    iget-object v1, v1, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v1}, Lcom/google/common/base/ae;->a()J

    move-result-wide v1

    invoke-virtual {p0, p1, p2, v1, v2}, LY/M;->a(Ljava/lang/Object;IJ)LY/L;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, LY/M;->m()V

    :goto_0
    return v0

    :cond_0
    :try_start_1
    invoke-interface {v1}, LY/L;->a()LY/Z;

    move-result-object v1

    invoke-interface {v1}, LY/Z;->get()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p0}, LY/M;->m()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, LY/M;->m()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->m()V

    throw v0
.end method

.method e(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 10

    const/4 v0, 0x0

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v1, p0, LY/M;->a:LY/n;

    iget-object v1, v1, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v1}, Lcom/google/common/base/ae;->a()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, LY/M;->c(J)V

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    iget-object v8, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    and-int v9, p2, v1

    invoke-virtual {v8, v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LY/L;

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_3

    invoke-interface {v2}, LY/L;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, LY/L;->c()I

    move-result v4

    if-ne v4, p2, :cond_2

    if-eqz v3, :cond_2

    iget-object v4, p0, LY/M;->a:LY/n;

    iget-object v4, v4, LY/n;->g:Lcom/google/common/base/t;

    invoke-virtual {v4, p1, v3}, Lcom/google/common/base/t;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, LY/L;->a()LY/Z;

    move-result-object v5

    invoke-interface {v5}, LY/Z;->get()Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    sget-object v6, LY/an;->a:LY/an;

    :goto_1
    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v6}, LY/M;->a(LY/L;LY/L;Ljava/lang/Object;ILY/Z;LY/an;)LY/L;

    move-result-object v0

    iget v1, p0, LY/M;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v8, v9, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    iput v1, p0, LY/M;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    move-object v0, v7

    :goto_2
    return-object v0

    :cond_0
    :try_start_1
    invoke-interface {v5}, LY/Z;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v6, LY/an;->c:LY/an;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_2

    :cond_2
    :try_start_2
    invoke-interface {v2}, LY/L;->b()LY/L;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method e()V
    .locals 1

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LY/M;->f()V

    :cond_0
    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LY/M;->g()V

    :cond_1
    return-void
.end method

.method f()V
    .locals 1

    :cond_0
    iget-object v0, p0, LY/M;->h:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    return-void
.end method

.method g()V
    .locals 1

    :cond_0
    iget-object v0, p0, LY/M;->i:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    if-nez v0, :cond_0

    return-void
.end method

.method h()V
    .locals 2

    :cond_0
    :goto_0
    iget-object v0, p0, LY/M;->j:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    if-eqz v0, :cond_1

    iget-object v1, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method i()V
    .locals 4

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, LY/M;->h()V

    :cond_2
    iget v0, p0, LY/M;->c:I

    int-to-long v0, v0

    iget-wide v2, p0, LY/M;->g:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, LY/M;->j()LY/L;

    move-result-object v0

    invoke-interface {v0}, LY/L;->c()I

    move-result v1

    sget-object v2, LY/an;->e:LY/an;

    invoke-virtual {p0, v0, v1, v2}, LY/M;->a(LY/L;ILY/an;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method j()LY/L;
    .locals 3

    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    invoke-interface {v0}, LY/L;->a()LY/Z;

    move-result-object v2

    invoke-interface {v2}, LY/Z;->a()I

    move-result v2

    if-lez v2, :cond_0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method k()V
    .locals 11

    iget-object v7, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v7}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v8

    const/high16 v0, 0x40000000

    if-lt v8, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v5, p0, LY/M;->b:I

    shl-int/lit8 v0, v8, 0x1

    invoke-virtual {p0, v0}, LY/M;->a(I)Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, LY/M;->e:I

    invoke-virtual {v9}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v8, :cond_5

    invoke-virtual {v7, v6}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    if-eqz v0, :cond_7

    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v3

    invoke-interface {v0}, LY/L;->c()I

    move-result v1

    and-int v2, v1, v10

    if-nez v3, :cond_2

    invoke-virtual {v9, v2, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v1, v5

    :cond_1
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v4, v0

    :goto_3
    if-eqz v3, :cond_3

    invoke-interface {v3}, LY/L;->c()I

    move-result v1

    and-int/2addr v1, v10

    if-eq v1, v2, :cond_6

    move-object v2, v3

    :goto_4
    invoke-interface {v3}, LY/L;->b()LY/L;

    move-result-object v3

    move-object v4, v2

    move v2, v1

    goto :goto_3

    :cond_3
    invoke-virtual {v9, v2, v4}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move-object v2, v0

    move v1, v5

    :goto_5
    if-eq v2, v4, :cond_1

    invoke-virtual {p0, v2}, LY/M;->b(LY/L;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, LY/M;->a(LY/L;)V

    add-int/lit8 v0, v1, -0x1

    :goto_6
    invoke-interface {v2}, LY/L;->b()LY/L;

    move-result-object v1

    move-object v2, v1

    move v1, v0

    goto :goto_5

    :cond_4
    invoke-interface {v2}, LY/L;->c()I

    move-result v0

    and-int v3, v0, v10

    invoke-virtual {v9, v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    invoke-virtual {p0, v2, v0}, LY/M;->a(LY/L;LY/L;)LY/L;

    move-result-object v0

    invoke-virtual {v9, v3, v0}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    move v0, v1

    goto :goto_6

    :cond_5
    iput-object v9, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    iput v5, p0, LY/M;->b:I

    goto :goto_0

    :cond_6
    move v1, v2

    move-object v2, v4

    goto :goto_4

    :cond_7
    move v1, v5

    goto :goto_2
.end method

.method l()V
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, LY/M;->b:I

    if-eqz v0, :cond_4

    invoke-virtual {p0}, LY/M;->lock()V

    :try_start_0
    iget-object v3, p0, LY/M;->f:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    move v2, v1

    :goto_0
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_2

    invoke-virtual {v3, v2}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LY/L;

    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, LY/L;->a()LY/Z;

    move-result-object v4

    invoke-interface {v4}, LY/Z;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LY/an;->a:LY/an;

    invoke-virtual {p0, v0, v4}, LY/M;->a(LY/L;LY/an;)V

    :cond_0
    invoke-interface {v0}, LY/L;->b()LY/L;

    move-result-object v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_2
    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p0}, LY/M;->e()V

    iget-object v0, p0, LY/M;->l:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, LY/M;->m:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, LY/M;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget v0, p0, LY/M;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LY/M;->d:I

    const/4 v0, 0x0

    iput v0, p0, LY/M;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, LY/M;->unlock()V

    invoke-virtual {p0}, LY/M;->n()V

    throw v0
.end method

.method m()V
    .locals 1

    iget-object v0, p0, LY/M;->k:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    if-nez v0, :cond_0

    invoke-virtual {p0}, LY/M;->o()V

    :cond_0
    return-void
.end method

.method n()V
    .locals 0

    invoke-virtual {p0}, LY/M;->p()V

    return-void
.end method

.method o()V
    .locals 2

    iget-object v0, p0, LY/M;->a:LY/n;

    iget-object v0, v0, LY/n;->r:Lcom/google/common/base/ae;

    invoke-virtual {v0}, Lcom/google/common/base/ae;->a()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LY/M;->d(J)V

    invoke-virtual {p0}, LY/M;->p()V

    return-void
.end method

.method p()V
    .locals 1

    invoke-virtual {p0}, LY/M;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LY/M;->a:LY/n;

    invoke-virtual {v0}, LY/n;->r()V

    :cond_0
    return-void
.end method
