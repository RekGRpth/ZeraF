.class public LaH/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/F;


# instance fields
.field private final a:Landroid/content/Context;

.field private volatile b:I

.field private volatile c:Landroid/location/LocationListener;

.field private d:LI/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, LaH/z;->d:LI/c;

    iput-object p1, p0, LaH/z;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, LaH/z;->d:LI/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/z;->d:LI/c;

    invoke-virtual {v0}, LI/c;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, LaH/z;->d:LI/c;

    :cond_0
    return-void
.end method

.method public declared-synchronized a(ZLandroid/location/LocationListener;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "getMainLooper() returned null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v2, p0, LaH/z;->b:I

    iget-object v0, p0, LaH/z;->d:LI/c;

    if-eqz v0, :cond_3

    iget-object v0, p0, LaH/z;->c:Landroid/location/LocationListener;

    if-eq v0, p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "InternalNlpLocationProvider can not run with different listeners."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, LaH/z;->d:LI/c;

    invoke-virtual {v0}, LI/c;->b()I

    move-result v0

    if-eq v0, v2, :cond_2

    iget-object v0, p0, LaH/z;->d:LI/c;

    invoke-virtual {v0, v2}, LI/c;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_0
    monitor-exit p0

    return-void

    :cond_3
    :try_start_2
    new-instance v0, LI/c;

    iget-object v1, p0, LaH/z;->a:Landroid/content/Context;

    const/16 v6, 0x2d0

    move-object v3, p2

    move v4, p1

    invoke-direct/range {v0 .. v6}, LI/c;-><init>(Landroid/content/Context;ILandroid/location/LocationListener;ZLandroid/os/Looper;I)V

    iput-object v0, p0, LaH/z;->d:LI/c;

    iput-object p2, p0, LaH/z;->c:Landroid/location/LocationListener;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public a(Landroid/location/Location;)[B
    .locals 1

    iget-object v0, p0, LaH/z;->d:LI/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/z;->d:LI/c;

    invoke-virtual {v0, p1}, LI/c;->a(Landroid/location/Location;)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
