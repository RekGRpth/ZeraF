.class LaH/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Lo/af;

.field c:Lo/T;

.field d:LO/H;

.field e:D

.field f:I

.field g:Z


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x4010000000000000L

    iput-wide v0, p0, LaH/l;->e:D

    iput v2, p0, LaH/l;->f:I

    iput-boolean v2, p0, LaH/l;->g:Z

    return-void
.end method

.method constructor <init>(LaH/l;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x4010000000000000L

    iput-wide v0, p0, LaH/l;->e:D

    iput v2, p0, LaH/l;->f:I

    iput-boolean v2, p0, LaH/l;->g:Z

    iget-boolean v0, p1, LaH/l;->a:Z

    iput-boolean v0, p0, LaH/l;->a:Z

    iget-object v0, p1, LaH/l;->b:Lo/af;

    iput-object v0, p0, LaH/l;->b:Lo/af;

    iget-object v0, p1, LaH/l;->c:Lo/T;

    iput-object v0, p0, LaH/l;->c:Lo/T;

    iget-object v0, p1, LaH/l;->d:LO/H;

    iput-object v0, p0, LaH/l;->d:LO/H;

    iget-wide v0, p1, LaH/l;->e:D

    iput-wide v0, p0, LaH/l;->e:D

    iget v0, p1, LaH/l;->f:I

    iput v0, p0, LaH/l;->f:I

    iget-boolean v0, p1, LaH/l;->g:Z

    iput-boolean v0, p0, LaH/l;->g:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, LaH/l;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, LaH/l;

    iget-boolean v1, p0, LaH/l;->a:Z

    iget-boolean v2, p1, LaH/l;->a:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LaH/l;->b:Lo/af;

    iget-object v2, p1, LaH/l;->b:Lo/af;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaH/l;->c:Lo/T;

    iget-object v2, p1, LaH/l;->c:Lo/T;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LaH/l;->d:LO/H;

    iget-object v2, p1, LaH/l;->d:LO/H;

    invoke-static {v1, v2}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, LaH/l;->f:I

    iget v2, p1, LaH/l;->f:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, LaH/l;->g:Z

    iget-boolean v2, p1, LaH/l;->g:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
