.class public LaH/a;
.super LaH/b;
.source "SourceFile"

# interfaces
.implements Landroid/location/LocationListener;


# instance fields
.field protected a:LaH/G;

.field private final i:LaH/F;

.field private j:LaH/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;LaH/F;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, LaH/b;-><init>(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, LaH/a;->j:LaH/h;

    iput-object p2, p0, LaH/a;->i:LaH/F;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    const-string v0, "network"

    return-object v0
.end method

.method protected a(LaH/h;)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaH/a;->a:LaH/G;

    invoke-virtual {v0, p1}, LaH/G;->a(Landroid/location/Location;)V

    iput-object p1, p0, LaH/a;->j:LaH/h;

    invoke-virtual {p0, p1}, LaH/a;->b(LaH/h;)V

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;)V
    .locals 1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, LaH/a;->a(LaH/h;)V

    return-void
.end method

.method protected b()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaH/a;->i:LaH/F;

    invoke-interface {v0}, LaH/F;->a()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, LaH/a;->m()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected c()V
    .locals 2

    new-instance v0, LaH/G;

    const-string v1, ""

    invoke-direct {v0, v1}, LaH/G;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LaH/a;->a:LaH/G;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LaH/a;->i:LaH/F;

    iget-boolean v0, p0, LaH/a;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0, p0}, LaH/F;->a(ZLandroid/location/LocationListener;)V

    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()LaH/h;
    .locals 1

    iget-object v0, p0, LaH/a;->j:LaH/h;

    return-object v0
.end method

.method public declared-synchronized e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LaH/a;->i:LaH/F;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaH/a;->j:LaH/h;

    if-eqz v1, :cond_0

    iget-object v1, p0, LaH/a;->i:LaH/F;

    iget-object v2, p0, LaH/a;->j:LaH/h;

    invoke-interface {v1, v2}, LaH/F;->a(Landroid/location/Location;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_1
    new-instance v2, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v3, Lby/w;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-virtual {v2, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->parse([B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "RMI"

    invoke-static {v2, v1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 0

    invoke-virtual {p0, p1}, LaH/a;->a(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0

    return-void
.end method
