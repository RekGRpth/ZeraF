.class public LaH/E;
.super Law/a;
.source "SourceFile"


# static fields
.field private static a:LaH/E;

.field private static j:Ljava/lang/Boolean;


# instance fields
.field private b:I

.field private c:LaN/B;

.field private d:LaN/B;

.field private e:I

.field private f:I

.field private g:[J

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Law/a;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [J

    iput-object v0, p0, LaH/E;->g:[J

    iput v1, p0, LaH/E;->h:I

    iput v1, p0, LaH/E;->i:I

    invoke-virtual {p0}, LaH/E;->m()V

    return-void
.end method

.method private static a(Lcom/google/googlenav/common/io/j;)Lcom/google/googlenav/common/io/m;
    .locals 1

    instance-of v0, p0, Lcom/google/googlenav/common/io/n;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/googlenav/common/io/n;

    const-string v0, "savedLocationShiftCoefficients_lock"

    invoke-interface {p0, v0}, Lcom/google/googlenav/common/io/n;->a(Ljava/lang/String;)Lcom/google/googlenav/common/io/m;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/googlenav/common/io/m;)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Lcom/google/googlenav/common/io/m;->a()Z

    :cond_0
    return-void
.end method

.method private a(LaN/B;I)Z
    .locals 6

    const-wide/32 v4, 0x15752a00

    const/4 v0, 0x0

    iget-object v1, p0, LaH/E;->c:LaN/B;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, LaN/B;->c()I

    move-result v1

    iget-object v2, p0, LaH/E;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->c()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, p2, :cond_0

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v1

    iget-object v2, p0, LaH/E;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->e()I

    move-result v2

    sub-int/2addr v1, v2

    :goto_1
    if-gez v1, :cond_2

    int-to-long v1, v1

    add-long/2addr v1, v4

    long-to-int v1, v1

    goto :goto_1

    :cond_2
    int-to-long v2, v1

    sub-long v2, v4, v2

    long-to-int v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v1, p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(LaN/B;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, LaH/E;->j:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    sget-object v0, LaH/E;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v2

    const v3, 0x2dc6c0

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, LaN/B;->c()I

    move-result v2

    const v3, 0x337f980

    if-gt v2, v3, :cond_0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v2

    const v3, 0x44aa200

    if-lt v2, v3, :cond_0

    invoke-virtual {p0}, LaN/B;->e()I

    move-result v2

    const v3, 0x81b3200

    if-gt v2, v3, :cond_0

    invoke-static {}, LaI/b;->c()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    const/16 v3, 0x1cc

    if-eq v2, v3, :cond_2

    const/16 v3, 0x460

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static k()LaH/E;
    .locals 1

    sget-object v0, LaH/E;->a:LaH/E;

    if-nez v0, :cond_0

    invoke-static {}, LaH/E;->p()V

    :cond_0
    sget-object v0, LaH/E;->a:LaH/E;

    return-object v0
.end method

.method private static declared-synchronized p()V
    .locals 2

    const-class v1, LaH/E;

    monitor-enter v1

    :try_start_0
    new-instance v0, LaH/E;

    invoke-direct {v0}, LaH/E;-><init>()V

    sput-object v0, LaH/E;->a:LaH/E;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(Ljava/io/DataOutput;)V
    .locals 3

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v1, Lcom/google/wireless/googlenav/proto/j2me/eo;->a:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v1, 0x2

    iget-object v2, p0, LaH/E;->d:LaN/B;

    invoke-virtual {v2}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    check-cast p1, Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    return-void
.end method

.method public a(LaN/B;)Z
    .locals 1

    iget v0, p0, LaH/E;->e:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v0}, LaH/E;->a(LaN/B;I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/eo;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {v2, p1}, Lcom/google/googlenav/common/io/protocol/b;->a(Lcom/google/googlenav/common/io/protocol/ProtoBufType;Ljava/io/DataInput;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaH/E;->b:I

    iget v2, p0, LaH/E;->b:I

    if-nez v2, :cond_0

    move v2, v1

    :goto_1
    const/4 v4, 0x6

    if-ge v2, v4, :cond_2

    iget-object v4, p0, LaH/E;->g:[J

    const/4 v5, 0x2

    invoke-virtual {v3, v5, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(II)J

    move-result-wide v5

    aput-wide v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaH/E;->f:I

    const/4 v2, 0x5

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    iput v2, p0, LaH/E;->e:I

    const/4 v2, 0x3

    invoke-virtual {v3, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-static {v2}, LaN/B;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/B;

    move-result-object v2

    iput-object v2, p0, LaH/E;->c:LaN/B;

    iget v2, p0, LaH/E;->b:I

    if-nez v2, :cond_3

    invoke-virtual {p0}, LaH/E;->o()V

    :cond_3
    iget v2, p0, LaH/E;->b:I

    if-nez v2, :cond_4

    :goto_2
    move v1, v0

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public b()I
    .locals 1

    const/16 v0, 0x35

    return v0
.end method

.method public b(LaN/B;)Z
    .locals 1

    iget v0, p0, LaH/E;->f:I

    div-int/lit8 v0, v0, 0x2

    invoke-direct {p0, p1, v0}, LaH/E;->a(LaN/B;I)Z

    move-result v0

    return v0
.end method

.method public c(LaN/B;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, LaH/E;->c:LaN/B;

    invoke-virtual {p1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaH/E;->d:LaN/B;

    invoke-virtual {p1, v0}, LaN/B;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, LaH/E;->d:LaN/B;

    invoke-static {}, Law/h;->a()Law/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Law/h;->c(Law/g;)V

    goto :goto_0
.end method

.method public d(LaN/B;)LaN/B;
    .locals 10

    const-wide/32 v8, 0xf4240

    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    iget-object v2, p0, LaH/E;->g:[J

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    iget-object v2, p0, LaH/E;->g:[J

    const/4 v3, 0x2

    aget-wide v2, v2, v3

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    div-long/2addr v0, v8

    iget-object v2, p0, LaH/E;->g:[J

    const/4 v3, 0x3

    aget-wide v2, v2, v3

    iget-object v4, p0, LaH/E;->g:[J

    const/4 v5, 0x4

    aget-wide v4, v4, v5

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    iget-object v4, p0, LaH/E;->g:[J

    const/4 v5, 0x5

    aget-wide v4, v4, v5

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    div-long/2addr v2, v8

    invoke-virtual {p0, p1}, LaH/E;->a(LaN/B;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0, p1}, LaH/E;->c(LaN/B;)V

    :cond_0
    long-to-int v4, v0

    invoke-virtual {p1}, LaN/B;->c()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, LaH/E;->h:I

    long-to-int v4, v2

    invoke-virtual {p1}, LaN/B;->e()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, p0, LaH/E;->i:I

    new-instance v4, LaN/B;

    long-to-int v0, v0

    long-to-int v1, v2

    invoke-direct {v4, v0, v1}, LaN/B;-><init>(II)V

    return-object v4
.end method

.method public l()V
    .locals 8

    const-wide/32 v6, 0xf4240

    const/4 v5, 0x1

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, LaH/E;->g:[J

    aput-wide v3, v0, v2

    iget-object v0, p0, LaH/E;->g:[J

    aput-wide v6, v0, v5

    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x2

    aput-wide v3, v0, v1

    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x3

    aput-wide v3, v0, v1

    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x4

    aput-wide v3, v0, v1

    iget-object v0, p0, LaH/E;->g:[J

    const/4 v1, 0x5

    aput-wide v6, v0, v1

    iput v2, p0, LaH/E;->e:I

    iput v2, p0, LaH/E;->f:I

    iput v2, p0, LaH/E;->h:I

    iput v2, p0, LaH/E;->i:I

    iput v5, p0, LaH/E;->b:I

    return-void
.end method

.method public declared-synchronized m()V
    .locals 4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-static {v1}, LaH/E;->a(Lcom/google/googlenav/common/io/j;)Lcom/google/googlenav/common/io/m;

    move-result-object v2

    const-string v0, "savedLocationShiftCoefficients"

    invoke-static {v1, v0}, Lcom/google/googlenav/common/j;->a(Lcom/google/googlenav/common/io/j;Ljava/lang/String;)Ljava/io/DataInput;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {p0, v0}, LaH/E;->a(Ljava/io/DataInput;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    :try_start_2
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p0}, LaH/E;->l()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_3
    const-string v3, "savedLocationShiftCoefficients"

    invoke-interface {v1, v3}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 5

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/eo;->b:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/4 v0, 0x1

    iget v2, p0, LaH/E;->b:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, LaH/E;->g:[J

    aget-wide v3, v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addLong(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    iget-object v2, p0, LaH/E;->c:LaN/B;

    invoke-virtual {v2}, LaN/B;->g()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setProtoBuf(ILcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x4

    iget v2, p0, LaH/E;->f:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x5

    iget v2, p0, LaH/E;->e:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    return-object v1
.end method

.method public declared-synchronized o()V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-static {v1}, LaH/E;->a(Lcom/google/googlenav/common/io/j;)Lcom/google/googlenav/common/io/m;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0}, LaH/E;->n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    check-cast v0, Ljava/io/DataOutputStream;

    invoke-virtual {v4, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->outputWithSizeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v3, "savedLocationShiftCoefficients"

    invoke-interface {v1, v0, v3}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "savedLocationShiftCoefficients"

    invoke-interface {v1, v0}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    invoke-static {v2}, LaH/E;->a(Lcom/google/googlenav/common/io/m;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public t_()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
