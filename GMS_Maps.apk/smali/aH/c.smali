.class public abstract LaH/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaH/s;
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Ljava/util/concurrent/CopyOnWriteArrayList;

.field protected c:LaH/h;

.field protected d:Z

.field protected e:Z

.field protected volatile f:Z

.field protected final g:Ljava/lang/Object;

.field protected h:J


# direct methods
.method protected constructor <init>(Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/c;->d:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/c;->f:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LaH/c;->g:Ljava/lang/Object;

    const-wide/16 v0, 0x1388

    iput-wide v0, p0, LaH/c;->h:J

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    iput-boolean p1, p0, LaH/c;->e:Z

    invoke-direct {p0}, LaH/c;->u()V

    return-void
.end method

.method private a(LaH/h;)LaH/h;
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, LaH/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LaH/j;->a(J)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, LaH/j;

    invoke-direct {v0}, LaH/j;-><init>()V

    invoke-virtual {v0, p1}, LaH/j;->a(Landroid/location/Location;)LaH/j;

    move-result-object v0

    invoke-virtual {p0}, LaH/c;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object p1

    :cond_1
    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz p1, :cond_2

    iput-object p1, p0, LaH/c;->c:LaH/h;

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, LaH/c;->e:Z

    return-object v0
.end method

.method private u()V
    .locals 8

    const/4 v7, 0x0

    iput-object v7, p0, LaH/c;->c:LaH/h;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->n()Lcom/google/googlenav/common/j;

    move-result-object v0

    invoke-direct {p0}, LaH/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/j;->b(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    if-eqz v0, :cond_1

    :try_start_0
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v1

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v3

    sub-long/2addr v3, v1

    const-wide/32 v5, 0xafc80

    cmp-long v3, v3, v5

    if-gez v3, :cond_0

    invoke-interface {v0}, Ljava/io/DataInput;->readInt()I

    move-result v3

    invoke-static {v0}, LaN/B;->a(Ljava/io/DataInput;)LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v4, LaH/j;

    invoke-direct {v4}, LaH/j;-><init>()V

    invoke-virtual {p0}, LaH/c;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LaH/j;->a(Ljava/lang/String;)LaH/j;

    move-result-object v4

    invoke-virtual {v4, v0}, LaH/j;->a(LaN/B;)LaH/j;

    move-result-object v0

    int-to-float v3, v3

    invoke-virtual {v0, v3}, LaH/j;->a(F)LaH/j;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, LaH/j;->a(J)LaH/j;

    move-result-object v0

    invoke-virtual {v0}, LaH/j;->d()LaH/h;

    move-result-object v0

    invoke-virtual {p0, v0}, LaH/c;->b(LaH/h;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    invoke-direct {p0}, LaH/c;->v()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v7}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private v()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LastLocation_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private w()V
    .locals 4

    invoke-virtual {p0}, LaH/c;->q()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Ljava/io/DataOutput;->writeLong(J)V

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getAccuracy()F

    move-result v2

    float-to-int v2, v2

    invoke-interface {v1, v2}, Ljava/io/DataOutput;->writeInt(I)V

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->a()LaN/B;

    move-result-object v2

    invoke-static {v2, v1}, LaN/B;->a(LaN/B;Ljava/io/DataOutput;)V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v1

    invoke-direct {p0}, LaH/c;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private x()V
    .locals 4

    iget-boolean v0, p0, LaH/c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc80

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, LaH/c;->m()V

    :cond_0
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public a(I)V
    .locals 3

    iget-object v1, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/y;

    invoke-interface {v0, p1, p0}, LaH/y;->a(ILaH/s;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(LaH/y;)V
    .locals 1

    iget-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected abstract b()V
.end method

.method protected final b(LaH/h;)V
    .locals 4

    invoke-direct {p0, p1}, LaH/c;->a(LaH/h;)LaH/h;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaH/h;->a()LaN/B;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v2, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, LaH/c;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaH/y;

    invoke-interface {v0, v1, p0}, LaH/y;->a(LaN/B;LaH/s;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected abstract c()V
.end method

.method public d()LaH/h;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized f()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaH/c;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaH/c;->f:Z

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "BaseLocationProvider"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaH/c;->f:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, LaH/c;->f:Z

    iget-object v1, p0, LaH/c;->g:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, LaH/c;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {p0}, LaH/c;->b()V

    invoke-direct {p0}, LaH/c;->w()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public i()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public j()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected l()I
    .locals 1

    invoke-direct {p0}, LaH/c;->x()V

    iget-boolean v0, p0, LaH/c;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v0}, LaH/h;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v0}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    :goto_0
    return v0

    :cond_0
    const v0, 0x1869f

    goto :goto_0
.end method

.method public m()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LaH/c;->c:LaH/h;

    return-void
.end method

.method public n()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public o()Z
    .locals 1

    iget-boolean v0, p0, LaH/c;->e:Z

    return v0
.end method

.method public p()Z
    .locals 1

    invoke-virtual {p0}, LaH/c;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaH/c;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Z
    .locals 4

    iget-object v0, p0, LaH/c;->c:LaH/h;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaH/c;->t()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v0

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v2}, LaH/h;->getTime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xafc80

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()LaH/h;
    .locals 1

    iget-object v0, p0, LaH/c;->c:LaH/h;

    return-object v0
.end method

.method public run()V
    .locals 1

    :try_start_0
    invoke-virtual {p0}, LaH/c;->c()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LaH/c;->a(I)V

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    iget-boolean v0, p0, LaH/c;->e:Z

    return v0
.end method

.method public t()Lcom/google/googlenav/common/a;
    .locals 1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LaH/c;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", location: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LaH/c;->c:LaH/h;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
