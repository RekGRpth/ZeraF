.class public final LaH/h;
.super Landroid/location/Location;
.source "SourceFile"


# instance fields
.field private a:LaN/B;

.field private b:Lo/D;

.field private c:Z

.field private d:J

.field private e:LaH/l;

.field private f:LaH/k;


# direct methods
.method private constructor <init>(LaH/j;)V
    .locals 2

    invoke-static {p1}, LaH/j;->a(LaH/j;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, LaH/j;->b(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, LaH/j;->c(LaH/j;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setAccuracy(F)V

    :cond_0
    invoke-static {p1}, LaH/j;->d(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LaH/j;->e(LaH/j;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setAltitude(D)V

    :cond_1
    invoke-static {p1}, LaH/j;->f(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, LaH/j;->g(LaH/j;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setBearing(F)V

    :cond_2
    invoke-static {p1}, LaH/j;->h(LaH/j;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLatitude(D)V

    invoke-static {p1}, LaH/j;->i(LaH/j;)D

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setLongitude(D)V

    invoke-static {p1}, LaH/j;->j(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, LaH/j;->k(LaH/j;)F

    move-result v0

    invoke-super {p0, v0}, Landroid/location/Location;->setSpeed(F)V

    :cond_3
    invoke-static {p1}, LaH/j;->l(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, LaH/j;->m(LaH/j;)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Landroid/location/Location;->setTime(J)V

    :cond_4
    invoke-static {p1}, LaH/j;->l(LaH/j;)Z

    move-result v0

    iput-boolean v0, p0, LaH/h;->c:Z

    invoke-static {p1}, LaH/j;->n(LaH/j;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p1}, LaH/j;->o(LaH/j;)J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, LaH/h;->d:J

    invoke-static {p1}, LaH/j;->p(LaH/j;)Landroid/os/Bundle;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/location/Location;->setExtras(Landroid/os/Bundle;)V

    invoke-static {p1}, LaH/j;->q(LaH/j;)LaN/B;

    move-result-object v0

    iput-object v0, p0, LaH/h;->a:LaN/B;

    invoke-static {p1}, LaH/j;->r(LaH/j;)Lo/D;

    move-result-object v0

    iput-object v0, p0, LaH/h;->b:Lo/D;

    invoke-static {p1}, LaH/j;->s(LaH/j;)LaH/l;

    move-result-object v0

    iput-object v0, p0, LaH/h;->e:LaH/l;

    invoke-static {p1}, LaH/j;->t(LaH/j;)LaH/k;

    move-result-object v0

    iput-object v0, p0, LaH/h;->f:LaH/k;

    return-void

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto :goto_0
.end method

.method synthetic constructor <init>(LaH/j;LaH/i;)V
    .locals 0

    invoke-direct {p0, p1}, LaH/h;-><init>(LaH/j;)V

    return-void
.end method

.method public static a(Landroid/location/Location;)I
    .locals 2

    const v0, 0x1869f

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public static a(LaH/h;)LaN/B;
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LaH/h;->a()LaN/B;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ZDZD)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    cmpl-double v2, p2, p5

    if-nez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    if-nez p4, :cond_0

    move v1, v0

    goto :goto_0
.end method

.method private a(ZJZJ)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    if-nez p4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    cmp-long v2, p2, p5

    if-nez v2, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    if-nez p4, :cond_0

    move v1, v0

    goto :goto_0
.end method

.method public static b(Landroid/location/Location;)I
    .locals 2

    const/4 v0, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method static synthetic b(LaH/h;)J
    .locals 2

    iget-wide v0, p0, LaH/h;->d:J

    return-wide v0
.end method

.method public static c(Landroid/location/Location;)I
    .locals 2

    const/4 v0, -0x1

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method static synthetic c(LaH/h;)LaH/l;
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    return-object v0
.end method

.method static synthetic d(LaH/h;)LaH/k;
    .locals 1

    iget-object v0, p0, LaH/h;->f:LaH/k;

    return-object v0
.end method

.method public static d(Landroid/location/Location;)Lo/D;
    .locals 1

    if-eqz p0, :cond_0

    instance-of v0, p0, LaH/h;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    check-cast p0, LaH/h;

    invoke-virtual {p0}, LaH/h;->b()Lo/D;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lo/T;)F
    .locals 9

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lo/T;->b()D

    move-result-wide v4

    invoke-virtual {p1}, Lo/T;->d()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, LaH/h;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public a(Lo/u;)F
    .locals 11

    const-wide v9, 0x3eb0c6f7a0b5ed8dL

    const/4 v0, 0x2

    new-array v8, v0, [F

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lo/u;->a()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v9

    invoke-virtual {p1}, Lo/u;->b()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v9

    invoke-static/range {v0 .. v8}, LaH/h;->distanceBetween(DDDD[F)V

    const/4 v0, 0x1

    aget v0, v8, v0

    return v0
.end method

.method public a()LaN/B;
    .locals 1

    iget-object v0, p0, LaH/h;->a:LaN/B;

    return-object v0
.end method

.method public b(Lo/u;)F
    .locals 11

    const-wide v9, 0x3eb0c6f7a0b5ed8dL

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lo/u;->a()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v9

    invoke-virtual {p1}, Lo/u;->b()I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v9

    invoke-static/range {v0 .. v8}, LaH/h;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public b()Lo/D;
    .locals 1

    iget-object v0, p0, LaH/h;->b:Lo/D;

    return-object v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, LaH/h;->b:Lo/D;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, LaH/h;->c:Z

    return v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, LaH/h;->d:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    instance-of v0, p1, LaH/h;

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    return v0

    :cond_0
    check-cast p1, LaH/h;

    invoke-virtual {p1}, LaH/h;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->a()LaN/B;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v7

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, LaH/h;->b()Lo/D;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->b()Lo/D;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v7

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, LaH/h;->hasAccuracy()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, LaH/h;->hasAccuracy()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getAccuracy()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v7

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, LaH/h;->hasAltitude()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getAltitude()D

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->hasAltitude()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getAltitude()D

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v7

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, LaH/h;->hasBearing()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getBearing()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, LaH/h;->hasBearing()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getBearing()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v7

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, LaH/h;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v7

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1}, LaH/h;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v7

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p1}, LaH/h;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v5

    move-object v0, p0

    move v1, v8

    move v4, v8

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v7

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v7

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, LaH/h;->hasSpeed()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getSpeed()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {p0}, LaH/h;->hasSpeed()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getSpeed()F

    move-result v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZDZD)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v7

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p1}, LaH/h;->d()Z

    move-result v1

    invoke-virtual {p1}, LaH/h;->getTime()J

    move-result-wide v2

    invoke-virtual {p0}, LaH/h;->d()Z

    move-result v4

    invoke-virtual {p0}, LaH/h;->getTime()J

    move-result-wide v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, LaH/h;->a(ZJZJ)Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v7

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, LaH/h;->e()J

    move-result-wide v0

    invoke-virtual {p0}, LaH/h;->e()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_c

    move v0, v7

    goto/16 :goto_0

    :cond_c
    iget-object v0, p1, LaH/h;->e:LaH/l;

    iget-object v1, p0, LaH/h;->e:LaH/l;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v7

    goto/16 :goto_0

    :cond_d
    iget-object v0, p1, LaH/h;->f:LaH/k;

    iget-object v1, p0, LaH/h;->f:LaH/k;

    invoke-static {v0, v1}, Lcom/google/common/base/E;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v7

    goto/16 :goto_0

    :cond_e
    move v0, v8

    goto/16 :goto_0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, LaH/h;->f:LaH/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->f:LaH/k;

    iget-boolean v0, v0, LaH/k;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, LaH/h;->f:LaH/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->f:LaH/k;

    iget-boolean v0, v0, LaH/k;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->f:LaH/k;

    iget-boolean v0, v0, LaH/k;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LaH/h;->a:LaN/B;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LaH/h;->b:Lo/D;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, LaH/h;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LaH/h;->e:LaH/l;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LaH/h;->f:LaH/k;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/common/base/E;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getTime()J

    move-result-wide v1

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getBearing()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getAltitude()D

    move-result-wide v1

    double-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, LaH/h;->getSpeed()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i()Z
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-boolean v0, v0, LaH/l;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget v0, v0, LaH/l;->f:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-boolean v0, v0, LaH/l;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Lo/af;
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-object v0, v0, LaH/l;->b:Lo/af;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()Lo/T;
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-object v0, v0, LaH/l;->c:Lo/T;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()LO/H;
    .locals 1

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-object v0, v0, LaH/l;->d:LO/H;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 4

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-wide v0, v0, LaH/l;->e:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()D
    .locals 2

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaH/h;->e:LaH/l;

    iget-wide v0, v0, LaH/l;->e:D

    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L

    goto :goto_0
.end method

.method public q()Lo/u;
    .locals 8

    const-wide v6, 0x412e848000000000L

    const-wide/high16 v4, 0x3fe0000000000000L

    new-instance v0, Lo/u;

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v6

    add-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lo/u;-><init>(II)V

    return-object v0
.end method

.method public r()LaN/B;
    .locals 8

    const-wide v6, 0x412e848000000000L

    const-wide/high16 v4, 0x3fe0000000000000L

    new-instance v0, LaN/B;

    invoke-virtual {p0}, LaH/h;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v6

    add-double/2addr v1, v4

    double-to-int v1, v1

    invoke-virtual {p0}, LaH/h;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, LaN/B;-><init>(II)V

    return-object v0
.end method

.method public setAccuracy(F)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAltitude(D)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBearing(F)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLatitude(D)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLongitude(D)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setSpeed(F)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTime(J)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    const-string v0, "GmmLocation["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "source = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaH/h;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", point = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, LaH/h;->a:LaN/B;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", accuracy = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, LaH/h;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaH/h;->getAccuracy()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", speed = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, LaH/h;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaH/h;->getSpeed()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m/s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", bearing = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, LaH/h;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaH/h;->getBearing()F

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " degrees"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", time = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, LaH/h;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", relativetime = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v3, Ljava/util/Date;

    invoke-virtual {p0}, LaH/h;->e()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", level = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LaH/h;->b:Lo/D;

    if-eqz v0, :cond_5

    iget-object v0, p0, LaH/h;->b:Lo/D;

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-object v0, p0, LaH/h;->e:LaH/l;

    if-eqz v0, :cond_0

    const-string v0, ", RouteSnappingInfo["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", onRoad = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->e:LaH/l;

    iget-boolean v2, v2, LaH/l;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", onRteCon = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->e:LaH/l;

    iget-wide v2, v2, LaH/l;->e:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isProjected = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->e:LaH/l;

    iget-boolean v2, v2, LaH/l;->g:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, LaH/h;->f:LaH/k;

    if-eqz v0, :cond_1

    const-string v0, ", RouteSnappingInfo["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isGps = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->f:LaH/k;

    iget-boolean v2, v2, LaH/k;->a:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", isGpsAccurate = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->f:LaH/k;

    iget-boolean v2, v2, LaH/k;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", numSatInFix = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaH/h;->f:LaH/k;

    iget v2, v2, LaH/k;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    const-string v0, "n/a"

    goto/16 :goto_0

    :cond_3
    const-string v0, "n/a"

    goto/16 :goto_1

    :cond_4
    const-string v0, "n/a"

    goto/16 :goto_2

    :cond_5
    const-string v0, "n/a"

    goto/16 :goto_3
.end method
