.class public LaH/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Lcom/google/googlenav/common/io/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, LaH/g;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    sput-object v0, LaH/g;->b:Lcom/google/googlenav/common/io/j;

    return-void
.end method

.method public static declared-synchronized a()Ljava/lang/String;
    .locals 3

    const-class v1, LaH/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, LaH/g;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, LaH/g;->b:Lcom/google/googlenav/common/io/j;

    const-string v2, "GlsPlatformKey"

    invoke-interface {v0, v2}, Lcom/google/googlenav/common/io/j;->a_(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    sput-object v2, LaH/g;->a:Ljava/lang/String;

    :cond_0
    :goto_0
    sget-object v0, LaH/g;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_1
    const-string v0, ""

    sput-object v0, LaH/g;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    const-class v1, LaH/g;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lau/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LaH/g;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    :try_start_1
    sget-object v0, LaH/g;->b:Lcom/google/googlenav/common/io/j;

    const-string v2, "GlsPlatformKey"

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/googlenav/common/io/j;->a(Ljava/lang/String;[B)Z

    sget-object v0, LaH/g;->b:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0}, Lcom/google/googlenav/common/io/j;->a()V

    sput-object p0, LaH/g;->a:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
