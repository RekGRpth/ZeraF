.class public LaB/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:LaB/m;

.field private static final i:Ljava/lang/Object;


# instance fields
.field protected final a:Ljava/util/Hashtable;

.field private final b:Ljava/util/concurrent/ConcurrentMap;

.field private c:Ljava/util/Vector;

.field private volatile d:Z

.field private final f:Lcom/google/googlenav/android/aa;

.field private final g:Las/c;

.field private final h:Ljava/util/Hashtable;

.field private final j:Ljava/util/Vector;

.field private final k:LaB/q;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v1, 0x0

    new-instance v0, LaB/m;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, LaB/m;-><init>(Lam/f;Lam/f;Lam/f;Lam/f;Ljava/lang/String;Ljava/lang/Long;)V

    sput-object v0, LaB/a;->e:LaB/m;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LaB/a;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/googlenav/android/aa;Las/c;LaB/q;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaB/a;->a:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaB/a;->c:Ljava/util/Vector;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaB/a;->h:Ljava/util/Hashtable;

    iput-object p1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    iput-object p2, p0, LaB/a;->g:Las/c;

    iput-object p3, p0, LaB/a;->k:LaB/q;

    new-instance v0, Lcom/google/common/collect/bE;

    invoke-direct {v0}, Lcom/google/common/collect/bE;-><init>()V

    invoke-virtual {v0}, Lcom/google/common/collect/bE;->k()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-direct {p0}, LaB/a;->e()V

    return-void
.end method

.method static synthetic a(LaB/a;)V
    .locals 0

    invoke-direct {p0}, LaB/a;->c()V

    return-void
.end method

.method static synthetic a(LaB/a;LaB/p;)V
    .locals 0

    invoke-direct {p0, p1}, LaB/a;->c(LaB/p;)V

    return-void
.end method

.method static synthetic a(LaB/a;Ljava/lang/Long;LaB/m;)V
    .locals 0

    invoke-direct {p0, p1, p2}, LaB/a;->a(Ljava/lang/Long;LaB/m;)V

    return-void
.end method

.method static synthetic a(LaB/a;Ljava/util/Vector;)V
    .locals 0

    invoke-direct {p0, p1}, LaB/a;->b(Ljava/util/Vector;)V

    return-void
.end method

.method private a(Ljava/lang/Long;LaB/m;)V
    .locals 2

    invoke-virtual {p2}, LaB/m;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_0
    iget-object v0, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_1
    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    sget-object v1, LaB/a;->e:LaB/m;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private a(Ljava/util/Vector;)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/p;

    invoke-interface {v0}, LaB/p;->Q_()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(LaB/a;Z)Z
    .locals 0

    iput-boolean p1, p0, LaB/a;->d:Z

    return p1
.end method

.method private static a(LaB/m;Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, LaB/m;->b()LaB/n;

    move-result-object v2

    invoke-virtual {v2}, LaB/n;->a()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eqz p1, :cond_1

    invoke-virtual {p0}, LaB/m;->b()LaB/n;

    move-result-object v2

    invoke-virtual {v2}, LaB/n;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method static synthetic b()LaB/m;
    .locals 1

    sget-object v0, LaB/a;->e:LaB/m;

    return-object v0
.end method

.method static synthetic b(LaB/a;)Las/c;
    .locals 1

    iget-object v0, p0, LaB/a;->g:Las/c;

    return-object v0
.end method

.method static synthetic b(LaB/a;Ljava/util/Vector;)V
    .locals 0

    invoke-direct {p0, p1}, LaB/a;->a(Ljava/util/Vector;)V

    return-void
.end method

.method private b(Ljava/util/Vector;)V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/m;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, LaB/m;->a()Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v3, v0}, LaB/a;->a(Ljava/lang/Long;LaB/m;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private b(Ljava/util/Vector;LaB/p;)Z
    .locals 8

    const/4 v2, 0x0

    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_3

    invoke-virtual {p1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/n;

    iget-object v1, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaB/m;

    if-nez v1, :cond_2

    iget-object v1, p0, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LaB/n;

    if-eqz v1, :cond_1

    invoke-virtual {v5, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    :goto_1
    iget-object v1, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v0

    sget-object v7, LaB/a;->i:Ljava/lang/Object;

    invoke-virtual {v1, v0, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v4, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, LaB/n;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, LaB/a;->a(LaB/m;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v4, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v1, p0, LaB/a;->h:Ljava/util/Hashtable;

    invoke-virtual {v0}, LaB/n;->b()Ljava/lang/Long;

    move-result-object v0

    sget-object v7, LaB/a;->i:Ljava/lang/Object;

    invoke-virtual {v1, v0, v7}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    invoke-virtual {v5}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_4
    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v5}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, LaB/h;

    iget-object v2, p0, LaB/a;->g:Las/c;

    new-instance v3, LaB/b;

    invoke-direct {v3, p0, p2}, LaB/b;-><init>(LaB/a;LaB/p;)V

    invoke-direct {v1, v2, v5, p0, v3}, LaB/h;-><init>(Las/c;Ljava/util/Vector;LaB/a;LaB/i;)V

    invoke-virtual {v1}, LaB/h;->g()V

    :cond_5
    invoke-virtual {v4}, Ljava/util/Vector;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v1, LaB/c;

    invoke-direct {v1, p0, p2}, LaB/c;-><init>(LaB/a;LaB/p;)V

    iget-object v2, p0, LaB/a;->k:LaB/q;

    invoke-interface {v2, v4, v1}, LaB/q;->a(Ljava/util/Vector;Lcom/google/googlenav/friend/aA;)V

    :cond_6
    return v0

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method static synthetic c(LaB/a;)Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic c(LaB/a;Ljava/util/Vector;)Ljava/util/Vector;
    .locals 0

    iput-object p1, p0, LaB/a;->c:Ljava/util/Vector;

    return-object p1
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    new-instance v2, LaB/e;

    invoke-direct {v2, p0}, LaB/e;-><init>(LaB/a;)V

    invoke-virtual {v1, v2, v0}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-direct {p0, v0}, LaB/a;->a(Ljava/util/Vector;)V

    goto :goto_0
.end method

.method private c(LaB/p;)V
    .locals 4

    const/4 v3, 0x1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0, v3}, Ljava/util/Vector;-><init>(I)V

    if-eqz p1, :cond_1

    iget-object v1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    if-eqz v1, :cond_1

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-object v1, p0, LaB/a;->f:Lcom/google/googlenav/android/aa;

    new-instance v2, LaB/d;

    invoke-direct {v2, p0, v0}, LaB/d;-><init>(LaB/a;Ljava/util/Vector;)V

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, LaB/a;->a(Ljava/util/Vector;)V

    goto :goto_0
.end method

.method private d()V
    .locals 4

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iget-object v1, p0, LaB/a;->a:Ljava/util/Hashtable;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    new-instance v1, LaB/f;

    iget-object v2, p0, LaB/a;->g:Las/c;

    invoke-direct {v1, p0, v2, v0}, LaB/f;-><init>(LaB/a;Las/c;Ljava/util/Vector;)V

    invoke-virtual {v1}, LaB/f;->g()V

    return-void
.end method

.method static synthetic d(LaB/a;)V
    .locals 0

    invoke-direct {p0}, LaB/a;->d()V

    return-void
.end method

.method private e()V
    .locals 2

    new-instance v0, LaB/g;

    iget-object v1, p0, LaB/a;->g:Las/c;

    invoke-direct {v0, p0, v1}, LaB/g;-><init>(LaB/a;Las/c;)V

    invoke-virtual {v0}, LaB/g;->g()V

    return-void
.end method

.method static synthetic e(LaB/a;)V
    .locals 0

    invoke-direct {p0}, LaB/a;->f()V

    return-void
.end method

.method static synthetic f(LaB/a;)Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, LaB/a;->c:Ljava/util/Vector;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, LaB/a;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    const-string v1, "PROTO_CLIENT_SAVED_PHOTO_CACHE"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const-string v1, "PHOTO_"

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/m;

    sget-object v3, LaB/a;->e:LaB/m;

    if-eq v0, v3, :cond_1

    invoke-virtual {v0}, LaB/m;->i()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/Long;)LaB/m;
    .locals 1

    iget-object v0, p0, LaB/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaB/m;

    return-object v0
.end method

.method public a(Lcom/google/googlenav/ui/friend/E;)Lam/f;
    .locals 2

    invoke-virtual {p1}, Lcom/google/googlenav/ui/friend/E;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, LaB/a;->a(Ljava/lang/Long;)LaB/m;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, LaB/a;->e:LaB/m;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/googlenav/ui/friend/E;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {v0}, LaB/m;->f()Lam/f;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0}, LaB/m;->e()Lam/f;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, LaB/m;->d()Lam/f;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0}, LaB/m;->c()Lam/f;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(LaB/p;)V
    .locals 1

    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public a(Ljava/util/Vector;LaB/p;)Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, LaB/a;->d:Z

    if-nez v1, :cond_0

    iget-object v1, p0, LaB/a;->c:Ljava/util/Vector;

    invoke-static {p1, v0, v1}, Lcom/google/googlenav/common/util/a;->a(Ljava/util/List;ILjava/util/List;)V

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2}, LaB/a;->b(Ljava/util/Vector;LaB/p;)Z

    move-result v0

    goto :goto_0
.end method

.method public b(LaB/p;)V
    .locals 1

    iget-object v0, p0, LaB/a;->j:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    return-void
.end method
