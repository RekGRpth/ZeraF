.class public Lbg/a;
.super Lcom/google/android/maps/driveabout/vector/D;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/googlenav/ui/aI;

.field private c:I

.field private d:Lo/X;

.field private e:Lo/X;

.field private f:Ljava/util/List;

.field private g:F

.field private h:Lcom/google/android/maps/driveabout/vector/D;

.field private final i:Lo/T;

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/googlenav/ui/aI;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/driveabout/vector/D;-><init>()V

    new-instance v0, Lo/T;

    invoke-direct {v0}, Lo/T;-><init>()V

    iput-object v0, p0, Lbg/a;->i:Lo/T;

    iput-object p1, p0, Lbg/a;->a:Landroid/content/Context;

    iput-object p2, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    return-void
.end method

.method private a(Lcom/google/googlenav/ui/aH;)Ljava/util/List;
    .locals 10

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/googlenav/ui/aH;->o()[[LaN/B;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/google/common/collect/bx;->a()Ljava/util/ArrayList;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    array-length v0, v4

    invoke-static {v0}, Lcom/google/common/collect/bx;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, v4, v3

    new-instance v7, Lo/Z;

    array-length v1, v6

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v7, v1}, Lo/Z;-><init>(I)V

    array-length v8, v6

    move v1, v2

    :goto_1
    if-ge v1, v8, :cond_2

    aget-object v9, v6, v1

    invoke-static {v9}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v9

    invoke-virtual {v7, v9}, Lo/Z;->a(Lo/T;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    aget-object v1, v6, v2

    invoke-static {v1}, LR/e;->a(LaN/B;)Lo/T;

    move-result-object v1

    invoke-virtual {v7, v1}, Lo/Z;->a(Lo/T;)Z

    invoke-virtual {v7}, Lo/Z;->d()Lo/X;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/ui/aH;Z)Lo/X;
    .locals 7

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/googlenav/ui/aH;->j()[LaN/B;

    move-result-object v2

    new-instance v3, Lo/Z;

    array-length v0, v2

    invoke-direct {v3, v0}, Lo/Z;-><init>(I)V

    new-instance v4, Lo/T;

    invoke-direct {v4}, Lo/T;-><init>()V

    array-length v5, v2

    move v0, v1

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v2, v0

    invoke-static {v6, v4}, LR/e;->a(LaN/B;Lo/T;)V

    invoke-virtual {v3, v4}, Lo/Z;->a(Lo/T;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    aget-object v0, v2, v1

    invoke-static {v0, v4}, LR/e;->a(LaN/B;Lo/T;)V

    invoke-virtual {v3, v4}, Lo/Z;->a(Lo/T;)Z

    :cond_1
    invoke-virtual {v3}, Lo/Z;->d()Lo/X;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/googlenav/ui/aH;)I
    .locals 1

    iget v0, p0, Lbg/a;->g:F

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/googlenav/ui/aH;->a(LaN/Y;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V
    .locals 1

    invoke-virtual {p0, p1}, Lbg/a;->d(LD/a;)V

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;LC/a;Lcom/google/android/maps/driveabout/vector/r;)V

    :cond_0
    return-void
.end method

.method public b(LC/a;LD/a;)Z
    .locals 2

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    iget v1, p0, Lbg/a;->g:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, LC/a;->r()F

    move-result v0

    iput v0, p0, Lbg/a;->g:F

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    instance-of v0, v0, Lcom/google/googlenav/ui/aG;

    if-nez v0, :cond_1

    iget v1, p0, Lbg/a;->j:I

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    check-cast v0, Lcom/google/googlenav/ui/aH;

    invoke-direct {p0, v0}, Lbg/a;->b(Lcom/google/googlenav/ui/aH;)I

    move-result v0

    iput v0, p0, Lbg/a;->j:I

    iget v0, p0, Lbg/a;->j:I

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    instance-of v0, v0, LaN/M;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    check-cast v0, LaN/M;

    invoke-virtual {v0}, LaN/M;->l()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    :cond_1
    invoke-virtual {p0, p2}, Lbg/a;->d(LD/a;)V

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/driveabout/vector/D;->b(LC/a;LD/a;)Z

    move-result v0

    return v0
.end method

.method public c(LD/a;)V
    .locals 1

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/D;->c(LD/a;)V

    :cond_0
    return-void
.end method

.method d(LD/a;)V
    .locals 8

    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iget v2, p0, Lbg/a;->c:I

    if-ne v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    invoke-virtual {v0, p1}, Lcom/google/android/maps/driveabout/vector/D;->a(LD/a;)V

    :cond_1
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    instance-of v0, v0, Lcom/google/googlenav/ui/aG;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    move-object v6, v0

    check-cast v6, Lcom/google/googlenav/ui/aG;

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->k()I

    move-result v3

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->m()I

    move-result v4

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->c()LaN/B;

    move-result-object v0

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->b()I

    move-result v2

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->d()Lo/D;

    move-result-object v5

    if-nez v5, :cond_3

    move-object v5, v7

    :goto_1
    if-eqz v0, :cond_2

    const/4 v7, -0x1

    if-ne v3, v7, :cond_4

    :cond_2
    iget-object v0, p0, Lbg/a;->i:Lo/T;

    invoke-virtual {v0, v1, v1}, Lo/T;->d(II)V

    move v2, v1

    move v4, v1

    move v3, v1

    :goto_2
    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    instance-of v0, v0, Lcom/google/android/maps/driveabout/vector/a;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    check-cast v0, Lcom/google/android/maps/driveabout/vector/a;

    iget-object v1, p0, Lbg/a;->i:Lo/T;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/T;I)V

    invoke-virtual {v0, v5}, Lcom/google/android/maps/driveabout/vector/a;->a(Lo/r;)V

    invoke-virtual {v0, v3}, Lcom/google/android/maps/driveabout/vector/a;->b(I)V

    invoke-virtual {v0, v4}, Lcom/google/android/maps/driveabout/vector/a;->c(I)V

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/driveabout/vector/a;->a(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iput v0, p0, Lbg/a;->c:I

    goto :goto_0

    :cond_3
    invoke-virtual {v5}, Lo/D;->a()Lo/r;

    move-result-object v5

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lbg/a;->i:Lo/T;

    invoke-static {v0, v1}, LR/e;->a(LaN/B;Lo/T;)V

    goto :goto_2

    :cond_5
    new-instance v0, Lcom/google/android/maps/driveabout/vector/a;

    iget-object v1, p0, Lbg/a;->i:Lo/T;

    invoke-interface {v6}, Lcom/google/googlenav/ui/aG;->n()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/maps/driveabout/vector/a;-><init>(Lo/T;IIILo/r;Ljava/lang/String;)V

    iput-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    move-object v5, v0

    check-cast v5, Lcom/google/googlenav/ui/aH;

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->j()[LaN/B;

    move-result-object v0

    array-length v0, v0

    if-gt v0, v3, :cond_7

    iput-object v7, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto :goto_3

    :cond_7
    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->l()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lbg/a;->e:Lo/X;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iget v1, p0, Lbg/a;->c:I

    if-eq v0, v1, :cond_9

    :cond_8
    invoke-direct {p0, v5, v3}, Lbg/a;->a(Lcom/google/googlenav/ui/aH;Z)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lbg/a;->e:Lo/X;

    invoke-direct {p0, v5}, Lbg/a;->a(Lcom/google/googlenav/ui/aH;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbg/a;->f:Ljava/util/List;

    :cond_9
    new-instance v0, Lcom/google/android/maps/driveabout/vector/G;

    iget-object v1, p0, Lbg/a;->e:Lo/X;

    iget-object v2, p0, Lbg/a;->f:Ljava/util/List;

    move-object v3, v5

    check-cast v3, LaN/M;

    invoke-virtual {v3}, LaN/M;->f()I

    move-result v3

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->k()I

    move-result v4

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->m()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/maps/driveabout/vector/G;-><init>(Lo/X;Ljava/util/List;III)V

    iput-object v0, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lbg/a;->d:Lo/X;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lbg/a;->b:Lcom/google/googlenav/ui/aI;

    invoke-interface {v0}, Lcom/google/googlenav/ui/aI;->i()I

    move-result v0

    iget v2, p0, Lbg/a;->c:I

    if-eq v0, v2, :cond_c

    :cond_b
    invoke-direct {p0, v5, v1}, Lbg/a;->a(Lcom/google/googlenav/ui/aH;Z)Lo/X;

    move-result-object v0

    iput-object v0, p0, Lbg/a;->d:Lo/X;

    :cond_c
    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->c()Lo/D;

    move-result-object v0

    if-nez v0, :cond_d

    move-object v0, v7

    :goto_4
    new-instance v1, Lcom/google/android/maps/driveabout/vector/z;

    iget-object v2, p0, Lbg/a;->d:Lo/X;

    iget v3, p0, Lbg/a;->j:I

    int-to-float v3, v3

    invoke-interface {v5}, Lcom/google/googlenav/ui/aH;->k()I

    move-result v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/maps/driveabout/vector/z;-><init>(Lo/X;FILo/r;)V

    iput-object v1, p0, Lbg/a;->h:Lcom/google/android/maps/driveabout/vector/D;

    goto/16 :goto_3

    :cond_d
    invoke-virtual {v0}, Lo/D;->a()Lo/r;

    move-result-object v0

    goto :goto_4
.end method

.method public p()Lcom/google/android/maps/driveabout/vector/E;
    .locals 1

    sget-object v0, Lcom/google/android/maps/driveabout/vector/E;->h:Lcom/google/android/maps/driveabout/vector/E;

    return-object v0
.end method
