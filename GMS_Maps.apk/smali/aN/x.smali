.class LaN/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements LaN/K;


# instance fields
.field private final a:Lcom/google/googlenav/common/io/j;

.field private final b:Ljava/lang/String;

.field private final c:LaN/D;

.field private d:I

.field private e:I

.field private f:I

.field private final g:LaN/z;

.field private final h:Ljava/util/Vector;

.field private final i:Ljava/util/Hashtable;

.field private j:I

.field private k:I

.field private l:Z

.field private m:I

.field private n:J

.field private o:J

.field private p:Z


# direct methods
.method constructor <init>(LaN/D;Ljava/lang/String;II)V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->m()Lcom/google/googlenav/common/io/j;

    move-result-object v0

    iput-object v0, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    iput v1, p0, LaN/x;->e:I

    iput v1, p0, LaN/x;->f:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LaN/x;->i:Ljava/util/Hashtable;

    iput v2, p0, LaN/x;->j:I

    iput v2, p0, LaN/x;->k:I

    iput-object p1, p0, LaN/x;->c:LaN/D;

    iput-object p2, p0, LaN/x;->b:Ljava/lang/String;

    add-int/lit16 v0, p3, -0x7d0

    iput v0, p0, LaN/x;->m:I

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/x;->n:J

    const-wide/16 v2, 0x841

    add-long/2addr v0, v2

    iput-wide v0, p0, LaN/x;->o:J

    add-int/lit8 v0, p4, -0x1

    iput v0, p0, LaN/x;->d:I

    iput-boolean v4, p0, LaN/x;->l:Z

    invoke-direct {p0}, LaN/x;->j()V

    iput-boolean v4, p0, LaN/x;->p:Z

    new-instance v0, LaN/z;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LaN/z;-><init>(LaN/x;LaN/y;)V

    iput-object v0, p0, LaN/x;->g:LaN/z;

    return-void
.end method

.method private a(LaN/f;LaN/P;)LaN/I;
    .locals 3

    invoke-virtual {p0, p1}, LaN/x;->a(LaN/f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, LaN/f;->a(Ljava/lang/String;LaN/P;)LaN/I;

    move-result-object v0

    if-nez v0, :cond_0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, LaN/f;->c()I

    move-result v1

    invoke-direct {p0, v1}, LaN/x;->b(I)I

    move-result v2

    invoke-direct {p0, p1, v2}, LaN/x;->a(LaN/f;I)V

    iget-object v2, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    invoke-virtual {p0, v1}, LaN/x;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    monitor-exit p0

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(LaN/x;LaN/f;LaN/P;)LaN/I;
    .locals 1

    invoke-direct {p0, p1, p2}, LaN/x;->a(LaN/f;LaN/P;)LaN/I;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(LaN/x;LaN/P;)LaN/e;
    .locals 1

    invoke-direct {p0, p1}, LaN/x;->b(LaN/P;)LaN/e;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Hashtable;I)LaN/f;
    .locals 7

    new-instance v3, LaN/f;

    invoke-direct {v3}, LaN/f;-><init>()V

    const/4 v1, 0x1

    iget-object v0, p0, LaN/x;->c:LaN/D;

    invoke-virtual {v0}, LaN/D;->i()[LaN/P;

    move-result-object v4

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_0

    aget-object v0, v4, v2

    iget-object v5, p0, LaN/x;->i:Ljava/util/Hashtable;

    invoke-virtual {v5, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, LaN/e;

    invoke-direct {v5, v0}, LaN/e;-><init>(LaN/I;)V

    invoke-virtual {v5}, LaN/e;->c()I

    move-result v0

    add-int v6, v1, v0

    if-gt v6, p2, :cond_1

    invoke-virtual {v3, v5}, LaN/f;->a(LaN/e;)Z

    move-result v5

    if-eqz v5, :cond_1

    add-int/2addr v0, v1

    :goto_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object v3

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(LaN/f;I)V
    .locals 4

    invoke-virtual {p1}, LaN/f;->d()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, LaN/f;->a()I

    move-result v1

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/x;->l:Z

    invoke-virtual {p1}, LaN/f;->e()V

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0, p2}, Ljava/util/Vector;->removeElementAt(I)V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, LaN/f;->a(I)LaN/e;

    move-result-object v2

    iget-object v3, p0, LaN/x;->i:Ljava/util/Hashtable;

    invoke-virtual {v2}, LaN/e;->a()LaN/P;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private declared-synchronized a(LaN/f;[BI)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaN/x;->k:I

    if-nez v0, :cond_0

    iget-object v0, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    const/4 v1, 0x0

    new-array v1, v1, [B

    iget-object v2, p0, LaN/x;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/googlenav/common/io/j;->b([BLjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :try_start_1
    invoke-virtual {p0, p3}, LaN/x;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p3, p2}, LaN/f;->a(Ljava/lang/String;I[B)V

    invoke-direct {p0, p1}, LaN/x;->b(LaN/f;)V
    :try_end_1
    .catch Lcom/google/googlenav/common/io/k; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    :try_start_2
    invoke-direct {p0, v0, v1}, LaN/x;->a(Lcom/google/googlenav/common/io/k;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v1, "FLASH"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private a(Lcom/google/googlenav/common/io/k;Z)V
    .locals 4

    invoke-virtual {p0}, LaN/x;->f()I

    move-result v1

    invoke-virtual {p0}, LaN/x;->a()I

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FLASH "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "B "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "R"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p2, :cond_1

    const-string v0, " catalog"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p1}, Lcom/google/googlenav/common/io/k;->a()I

    move-result v0

    const/4 v3, -0x2

    if-ne v0, v3, :cond_0

    invoke-direct {p0}, LaN/x;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit16 v0, v1, -0x3e8

    iput v0, p0, LaN/x;->m:I

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    iput v2, p0, LaN/x;->d:I

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    aput-object v1, p1, v0

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private b(I)I
    .locals 3

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/f;

    invoke-virtual {v0}, LaN/f;->c()I

    move-result v0

    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private b(LaN/P;)LaN/e;
    .locals 1

    iget-object v0, p0, LaN/x;->i:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/f;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p1}, LaN/f;->a(LaN/P;)LaN/e;

    move-result-object v0

    goto :goto_0
.end method

.method private b(LaN/f;)V
    .locals 4

    invoke-virtual {p1}, LaN/f;->a()I

    move-result v1

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/x;->l:Z

    iget v0, p0, LaN/x;->j:I

    invoke-virtual {p1}, LaN/f;->c()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LaN/x;->j:I

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, LaN/f;->a(I)LaN/e;

    move-result-object v2

    iget-object v3, p0, LaN/x;->i:Ljava/util/Hashtable;

    invoke-virtual {v2}, LaN/e;->a()LaN/P;

    move-result-object v2

    invoke-virtual {v3, v2, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private c(I)LaN/f;
    .locals 1

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/f;

    return-object v0
.end method

.method private declared-synchronized j()V
    .locals 8

    const/4 v2, 0x0

    const/16 v1, 0xa

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaN/x;->l:Z

    iget-object v0, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    iget-object v3, p0, LaN/x;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/googlenav/common/io/j;->d(Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v3}, Lcom/google/googlenav/common/io/i;->a([B)Ljava/io/DataInput;

    move-result-object v4

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v1, :cond_2

    :goto_0
    :try_start_1
    iget-boolean v2, p0, LaN/x;->l:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LaN/x;->c()V

    :cond_0
    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/x;->l:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    invoke-interface {v4}, Ljava/io/DataInput;->readBoolean()Z

    invoke-interface {v4}, Ljava/io/DataInput;->readShort()S

    move-result v5

    iput v5, p0, LaN/x;->e:I

    invoke-interface {v4}, Ljava/io/DataInput;->readShort()S

    move-result v5

    iput v5, p0, LaN/x;->f:I

    invoke-interface {v4}, Ljava/io/DataInput;->readInt()I

    move-result v5

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-static {v4}, LaN/f;->a(Ljava/io/DataInput;)LaN/f;

    move-result-object v6

    invoke-direct {p0, v6}, LaN/x;->b(LaN/f;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    array-length v2, v3

    iput v2, p0, LaN/x;->k:I

    const/4 v2, 0x0

    iput-boolean v2, p0, LaN/x;->l:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    move-object v7, v2

    move v2, v0

    move-object v0, v7

    :goto_2
    :try_start_3
    const-string v3, "FLASH"

    invoke-static {v3, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_1
    move-exception v0

    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private k()Z
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LaN/x;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_Test"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-interface {v2, v3, v1}, Lcom/google/googlenav/common/io/j;->a([BLjava/lang/String;)I

    iget-object v2, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    invoke-interface {v2, v1}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/google/googlenav/common/io/k; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method a()I
    .locals 1

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method a(Ljava/util/Hashtable;)I
    .locals 19

    const/4 v2, 0x2

    new-array v12, v2, [I

    fill-array-data v12, :array_0

    const/4 v2, 0x2

    new-array v13, v2, [LaN/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v13, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v13, v2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v14

    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, LaN/x;->p:Z

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, LaN/x;->d()Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, LaN/x;->p:Z

    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, LaN/x;->m:I

    invoke-virtual/range {p0 .. p0}, LaN/x;->f()I

    move-result v3

    sub-int v11, v2, v3

    const v2, 0x11940

    if-lt v11, v2, :cond_1

    invoke-virtual/range {p0 .. p0}, LaN/x;->a()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, LaN/x;->d:I

    if-lt v2, v3, :cond_8

    :cond_1
    const/4 v10, -0x1

    const/4 v8, -0x1

    const-wide/high16 v6, -0x8000000000000000L

    const-wide/high16 v2, -0x8000000000000000L

    invoke-virtual/range {p0 .. p0}, LaN/x;->a()I

    move-result v16

    const/4 v9, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v9, v0, :cond_5

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, LaN/x;->c(I)LaN/f;

    move-result-object v4

    invoke-virtual {v4, v14, v15}, LaN/f;->a(J)J

    move-result-wide v4

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_2

    cmp-long v17, v4, v2

    if-lez v17, :cond_16

    :cond_2
    const/4 v2, -0x1

    if-eq v10, v2, :cond_3

    cmp-long v2, v4, v6

    if-lez v2, :cond_4

    :cond_3
    move-wide v2, v6

    move-wide v6, v4

    move v4, v10

    move v10, v9

    :goto_1
    add-int/lit8 v9, v9, 0x1

    move v8, v4

    goto :goto_0

    :cond_4
    move-wide v2, v4

    move v4, v9

    goto :goto_1

    :cond_5
    const/4 v2, -0x1

    if-eq v10, v2, :cond_15

    const/4 v2, 0x0

    aput v10, v12, v2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, LaN/x;->c(I)LaN/f;

    move-result-object v3

    aput-object v3, v13, v2

    const/4 v2, 0x0

    aget-object v2, v13, v2

    invoke-virtual {v2}, LaN/f;->b()I

    move-result v2

    add-int/2addr v2, v11

    :goto_2
    const v3, 0x11940

    if-ge v2, v3, :cond_6

    const/4 v3, -0x1

    if-eq v8, v3, :cond_6

    const/4 v3, 0x1

    aput v8, v12, v3

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, LaN/x;->c(I)LaN/f;

    move-result-object v4

    aput-object v4, v13, v3

    const/4 v3, 0x1

    aget-object v3, v13, v3

    invoke-virtual {v3}, LaN/f;->b()I

    move-result v3

    add-int/2addr v2, v3

    :cond_6
    const v3, 0x11940

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    :goto_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v3, 0x1770

    if-ge v2, v3, :cond_9

    const/4 v2, 0x0

    aget-object v2, v13, v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    const/4 v3, 0x0

    aget-object v3, v13, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LaN/x;->a(LaN/f;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const/4 v2, 0x0

    aget-object v2, v13, v2

    const/4 v3, 0x0

    aget v3, v12, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, LaN/x;->a(LaN/f;I)V

    :cond_7
    const/4 v2, 0x1

    :goto_4
    return v2

    :cond_8
    const v2, 0x11940

    goto :goto_3

    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_9
    monitor-enter p1

    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, LaN/x;->c:LaN/D;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LaN/D;->b(Z)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, LaN/x;->a(Ljava/util/Hashtable;I)LaN/f;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v5

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, LaN/x;->c:LaN/D;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LaN/D;->b(Z)V

    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-virtual {v5}, LaN/f;->b()I

    move-result v6

    const/16 v2, 0x1770

    if-lt v6, v2, :cond_13

    const/4 v2, -0x1

    monitor-enter p0

    :try_start_4
    move-object/from16 v0, p0

    iget v3, v0, LaN/x;->m:I

    invoke-virtual/range {p0 .. p0}, LaN/x;->f()I

    move-result v4

    sub-int v7, v3, v4

    const/4 v3, 0x2

    new-array v8, v3, [Z

    fill-array-data v8, :array_1

    const/4 v4, 0x0

    const/4 v3, 0x0

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    :goto_5
    const/4 v9, 0x2

    if-ge v4, v9, :cond_b

    aget-object v9, v13, v4

    if-eqz v9, :cond_a

    aget-object v9, v13, v4

    invoke-virtual {v9}, LaN/f;->d()Z

    move-result v9

    if-eqz v9, :cond_a

    aget-object v9, v13, v4

    invoke-virtual {v9, v14, v15}, LaN/f;->a(J)J

    move-result-wide v9

    invoke-virtual {v5, v14, v15}, LaN/f;->a(J)J

    move-result-wide v16

    cmp-long v9, v9, v16

    if-lez v9, :cond_a

    const/4 v9, 0x1

    aput-boolean v9, v8, v4

    aget-object v9, v13, v4

    invoke-virtual {v9}, LaN/f;->b()I

    move-result v9

    add-int/2addr v3, v9

    :cond_a
    add-int v9, v7, v3

    if-gt v6, v9, :cond_d

    :cond_b
    invoke-virtual/range {p0 .. p0}, LaN/x;->a()I

    move-result v4

    add-int/2addr v3, v7

    if-le v6, v3, :cond_e

    const/4 v3, 0x2

    move/from16 v18, v2

    move v2, v3

    move/from16 v3, v18

    :goto_6
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    if-ltz v3, :cond_c

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, LaN/f;->a(Ljava/util/Hashtable;)[B

    move-result-object v4

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v4, v3}, LaN/x;->a(LaN/f;[BI)V

    :cond_c
    :goto_7
    invoke-virtual/range {p0 .. p0}, LaN/x;->b()Z

    goto/16 :goto_4

    :catchall_1
    move-exception v2

    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, LaN/x;->c:LaN/D;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LaN/D;->b(Z)V

    throw v2

    :catchall_2
    move-exception v2

    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v2

    :cond_d
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_e
    const/4 v3, 0x0

    :try_start_6
    aget-boolean v3, v8, v3

    if-eqz v3, :cond_11

    if-gt v6, v7, :cond_f

    move-object/from16 v0, p0

    iget v3, v0, LaN/x;->d:I

    if-lt v4, v3, :cond_11

    :cond_f
    const/4 v3, 0x4

    const/4 v2, 0x0

    aget-object v2, v13, v2

    invoke-virtual {v2}, LaN/f;->c()I

    move-result v2

    const/4 v4, 0x0

    aget-object v4, v13, v4

    const/4 v6, 0x0

    aget v6, v12, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, LaN/x;->a(LaN/f;I)V

    const/4 v4, 0x1

    aget-boolean v4, v8, v4

    if-eqz v4, :cond_14

    const/4 v4, 0x0

    aget v4, v12, v4

    const/4 v6, 0x1

    aget v6, v12, v6

    if-ge v4, v6, :cond_10

    const/4 v4, 0x1

    aget v6, v12, v4

    add-int/lit8 v6, v6, -0x1

    aput v6, v12, v4

    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    const/4 v6, 0x1

    aget-object v6, v13, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LaN/x;->a(LaN/f;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    const/4 v4, 0x1

    aget-object v4, v13, v4

    const/4 v6, 0x1

    aget v6, v12, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, LaN/x;->a(LaN/f;I)V

    move/from16 v18, v2

    move v2, v3

    move/from16 v3, v18

    goto :goto_6

    :cond_11
    move-object/from16 v0, p0

    iget v3, v0, LaN/x;->d:I

    if-ge v4, v3, :cond_12

    const/4 v3, 0x3

    move-object/from16 v0, p0

    iget v2, v0, LaN/x;->j:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, LaN/x;->j:I

    move/from16 v18, v2

    move v2, v3

    move/from16 v3, v18

    goto/16 :goto_6

    :cond_12
    const/4 v3, 0x5

    move/from16 v18, v2

    move v2, v3

    move/from16 v3, v18

    goto/16 :goto_6

    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v2

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_14
    move/from16 v18, v2

    move v2, v3

    move/from16 v3, v18

    goto/16 :goto_6

    :cond_15
    move v2, v11

    goto/16 :goto_2

    :cond_16
    move v4, v8

    goto/16 :goto_1

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x0t
    .end array-data
.end method

.method public a(LaN/P;)LaN/I;
    .locals 4

    const/4 v0, 0x0

    invoke-direct {p0, p1}, LaN/x;->b(LaN/P;)LaN/e;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LaN/x;->g:LaN/z;

    invoke-virtual {v0, v1, p1}, LaN/z;->a(LaN/e;LaN/P;)LaN/I;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LaN/e;->a(J)V

    :cond_0
    return-object v0
.end method

.method a(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaN/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(LaN/f;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, LaN/f;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LaN/x;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, LaN/x;->i()Z

    invoke-virtual {p0}, LaN/x;->b()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v1, "FLASH"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(II)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v2, -0x1

    iget v0, p0, LaN/x;->e:I

    if-eq p1, v0, :cond_0

    iget v0, p0, LaN/x;->e:I

    if-ne v0, v2, :cond_1

    :cond_0
    iget v0, p0, LaN/x;->f:I

    if-eq p2, v0, :cond_3

    iget v0, p0, LaN/x;->f:I

    if-eq v0, v2, :cond_3

    :cond_1
    move v0, v1

    :goto_0
    iput p1, p0, LaN/x;->e:I

    iput p2, p0, LaN/x;->f:I

    if-eqz v0, :cond_2

    invoke-virtual {p0}, LaN/x;->c()V

    iput-boolean v1, p0, LaN/x;->l:Z

    :cond_2
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method declared-synchronized b()Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, LaN/x;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v2, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v3

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Ljava/io/DataOutputStream;

    invoke-direct {v5, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0xa

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeBoolean(Z)V

    iget v2, p0, LaN/x;->e:I

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    iget v2, p0, LaN/x;->f:I

    invoke-virtual {v5, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-virtual {v5, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-direct {p0, v2}, LaN/x;->c(I)LaN/f;

    move-result-object v6

    invoke-virtual {v6, v5}, LaN/f;->a(Ljava/io/DataOutput;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :try_start_2
    iget-object v3, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    iget-object v4, p0, LaN/x;->b:Ljava/lang/String;

    invoke-interface {v3, v2, v4}, Lcom/google/googlenav/common/io/j;->a([BLjava/lang/String;)I
    :try_end_2
    .catch Lcom/google/googlenav/common/io/k; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    :try_start_3
    array-length v1, v2

    iput v1, p0, LaN/x;->k:I

    const/4 v1, 0x0

    iput-boolean v1, p0, LaN/x;->l:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    const/4 v3, 0x1

    :try_start_4
    invoke-direct {p0, v0, v3}, LaN/x;->a(Lcom/google/googlenav/common/io/k;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    goto :goto_2
.end method

.method public declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/x;->i:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    const/4 v0, 0x0

    iput v0, p0, LaN/x;->k:I

    const/4 v0, 0x0

    iput v0, p0, LaN/x;->j:I

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/x;->l:Z

    iget-object v0, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    iget-object v1, p0, LaN/x;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/googlenav/common/io/j;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized d()Z
    .locals 6

    const/4 v1, 0x0

    monitor-enter p0

    const/4 v2, 0x1

    :try_start_0
    iget-object v0, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    iget-object v3, p0, LaN/x;->b:Ljava/lang/String;

    invoke-interface {v0, v3}, Lcom/google/googlenav/common/io/j;->e(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LaN/x;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_0

    iget-object v0, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v0, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/f;

    invoke-virtual {p0, v0}, LaN/x;->a(LaN/f;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, LaN/x;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-direct {p0, v0, v3}, LaN/x;->a(LaN/f;I)V

    move v0, v1

    :goto_1
    add-int/lit8 v2, v3, -0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_0
    if-eqz v4, :cond_4

    iget-object v0, p0, LaN/x;->b:Ljava/lang/String;

    invoke-static {v0, v4}, LaN/x;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    move v0, v2

    move v2, v1

    :goto_2
    array-length v5, v4

    if-ge v2, v5, :cond_2

    aget-object v5, v4, v2

    if-eqz v5, :cond_1

    iget-object v0, p0, LaN/x;->a:Lcom/google/googlenav/common/io/j;

    invoke-interface {v0, v5}, Lcom/google/googlenav/common/io/j;->b(Ljava/lang/String;)Z

    move v0, v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v2, v3

    :goto_3
    invoke-virtual {p0}, LaN/x;->a()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-lez v3, :cond_3

    if-nez v2, :cond_3

    :goto_4
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v1, v0

    goto :goto_4

    :cond_4
    move v0, v2

    move v2, v1

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method declared-synchronized e()I
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, LaN/x;->h:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-direct {p0, v0}, LaN/x;->c(I)LaN/f;

    move-result-object v3

    invoke-virtual {v3}, LaN/f;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    monitor-exit p0

    return v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()I
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, LaN/x;->k:I

    invoke-virtual {p0}, LaN/x;->e()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/x;->n:J

    return-void
.end method

.method public h()V
    .locals 2

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v0

    iput-wide v0, p0, LaN/x;->n:J

    return-void
.end method

.method public i()Z
    .locals 10

    const-wide/16 v8, 0x841

    const/4 v0, 0x1

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    iget-object v3, p0, LaN/x;->c:LaN/D;

    invoke-virtual {v3}, LaN/D;->a()Ljava/util/Hashtable;

    move-result-object v3

    iget-wide v4, p0, LaN/x;->o:J

    cmp-long v4, v4, v1

    if-gez v4, :cond_1

    iget-wide v4, p0, LaN/x;->n:J

    const-wide/16 v6, 0x5dc

    add-long/2addr v4, v6

    cmp-long v1, v4, v1

    if-gez v1, :cond_1

    :try_start_0
    invoke-virtual {p0, v3}, LaN/x;->a(Ljava/util/Hashtable;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    :cond_0
    :goto_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    add-long/2addr v1, v8

    iput-wide v1, p0, LaN/x;->o:J

    :cond_1
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->c()J

    move-result-wide v1

    add-long/2addr v1, v8

    iput-wide v1, p0, LaN/x;->o:J

    throw v0
.end method
