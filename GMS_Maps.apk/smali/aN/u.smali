.class public abstract LaN/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:I

.field protected b:I

.field private final c:Ljava/util/Set;

.field private final d:Landroid/graphics/Point;

.field private final e:Landroid/graphics/Point;

.field private f:LaN/Y;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LaN/u;->c:Ljava/util/Set;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LaN/u;->d:Landroid/graphics/Point;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LaN/u;->e:Landroid/graphics/Point;

    return-void
.end method

.method public static a(LaN/H;LaN/H;)I
    .locals 5

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, LaN/H;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LaN/H;->b()LaN/Y;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p1}, LaN/H;->a()LaN/B;

    move-result-object v1

    invoke-virtual {v0}, LaN/B;->e()I

    move-result v2

    invoke-virtual {p0}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-static {v2, v3}, LaN/B;->a(ILaN/Y;)I

    move-result v2

    invoke-virtual {v0}, LaN/B;->c()I

    move-result v0

    invoke-virtual {p0}, LaN/H;->b()LaN/Y;

    move-result-object v3

    invoke-static {v0, v3}, LaN/B;->b(ILaN/Y;)I

    move-result v0

    invoke-virtual {v1}, LaN/B;->e()I

    move-result v3

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-static {v3, v4}, LaN/B;->a(ILaN/Y;)I

    move-result v3

    invoke-virtual {v1}, LaN/B;->c()I

    move-result v1

    invoke-virtual {p1}, LaN/H;->b()LaN/Y;

    move-result-object v4

    invoke-static {v1, v4}, LaN/B;->b(ILaN/Y;)I

    move-result v1

    sub-int/2addr v2, v3

    sub-int/2addr v0, v1

    mul-int v1, v2, v2

    mul-int/2addr v0, v0

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method public static b(I)I
    .locals 2

    const/16 v0, 0x50

    invoke-static {v0, p0}, Ljava/lang/Math;->min(II)I

    move-result v0

    mul-int/lit8 v0, v0, 0xa

    add-int/lit16 v0, v0, 0xc8

    const/16 v1, 0x2bc

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public abstract a(ILaN/B;)F
.end method

.method public abstract a()I
.end method

.method public a(ILaN/H;)I
    .locals 3

    invoke-virtual {p0, p2}, LaN/u;->a(LaN/H;)I

    move-result v0

    int-to-float v1, p1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->F()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract a(LaN/B;)I
.end method

.method public abstract a(LaN/H;)I
.end method

.method protected abstract a(LaN/B;LaN/Y;II)LaN/B;
.end method

.method public a(Lcom/google/googlenav/E;LaN/B;IIIIIIILaN/Y;)LaN/B;
    .locals 11

    invoke-interface {p1}, Lcom/google/googlenav/E;->a()LaN/B;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object p2

    :cond_0
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p10

    invoke-virtual {p0, p2, v0, p2, v5}, LaN/u;->a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V

    move-object/from16 v0, p10

    invoke-virtual {p0, p2, v0, v1, v4}, LaN/u;->a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V

    iget v1, v4, Landroid/graphics/Point;->x:I

    iget v2, v5, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, v2

    add-int v6, v1, p7

    iget v1, v4, Landroid/graphics/Point;->y:I

    iget v2, v5, Landroid/graphics/Point;->y:I

    sub-int v7, v1, v2

    move/from16 v0, p5

    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    div-int/lit8 v8, v1, 0x2

    add-int v9, p4, p6

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/K;->al()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    :goto_1
    const/4 v2, 0x0

    neg-int v3, v6

    add-int v3, v3, p9

    add-int/2addr v3, v8

    iget v10, p0, LaN/u;->a:I

    div-int/lit8 v10, v10, 0x2

    if-gt v3, v10, :cond_1

    add-int v3, v6, v8

    iget v10, p0, LaN/u;->a:I

    div-int/lit8 v10, v10, 0x2

    if-le v3, v10, :cond_5

    :cond_1
    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_2

    iget v2, v4, Landroid/graphics/Point;->x:I

    iget v3, v5, Landroid/graphics/Point;->x:I

    div-int/lit8 v4, p9, 0x2

    add-int/2addr v3, v4

    if-le v2, v3, :cond_6

    iget v2, p0, LaN/u;->a:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, v6

    sub-int/2addr v2, v8

    neg-int v2, v2

    :cond_2
    :goto_3
    const/4 v3, 0x0

    neg-int v4, v7

    add-int v4, v4, p8

    add-int/2addr v4, v9

    iget v6, p0, LaN/u;->b:I

    div-int/lit8 v6, v6, 0x2

    if-gt v4, v6, :cond_3

    add-int v4, v7, v1

    iget v6, p0, LaN/u;->b:I

    div-int/lit8 v6, v6, 0x2

    if-le v4, v6, :cond_7

    :cond_3
    const/4 v4, 0x1

    :goto_4
    if-eqz v4, :cond_9

    if-lez v7, :cond_8

    iget v3, p0, LaN/u;->b:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v7, v3

    add-int/2addr v1, v3

    :goto_5
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    iget v4, v5, Landroid/graphics/Point;->x:I

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Point;->x:I

    iget v2, v5, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v2

    iput v1, v3, Landroid/graphics/Point;->y:I

    iget v1, v3, Landroid/graphics/Point;->x:I

    iget v2, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p10

    invoke-virtual {p0, p2, v0, v1, v2}, LaN/u;->a(LaN/B;LaN/Y;II)LaN/B;

    move-result-object p2

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    :cond_6
    iget v2, p0, LaN/u;->a:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v6

    sub-int/2addr v2, v8

    sub-int v2, v2, p9

    goto :goto_3

    :cond_7
    const/4 v4, 0x0

    goto :goto_4

    :cond_8
    iget v1, p0, LaN/u;->b:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v7

    sub-int/2addr v1, v9

    sub-int v1, v1, p8

    goto :goto_5

    :cond_9
    move v1, v3

    goto :goto_5
.end method

.method public a(IIII)LaN/Y;
    .locals 4

    invoke-virtual {p0}, LaN/u;->f()LaN/H;

    move-result-object v1

    invoke-virtual {v1}, LaN/H;->a()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/u;->a(LaN/B;)I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, LaN/Y;->e()I

    move-result v0

    invoke-static {v0}, LaN/Y;->b(I)LaN/Y;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v0}, LaN/H;->a(LaN/Y;)LaN/H;

    move-result-object v2

    invoke-virtual {p0, p4, v2}, LaN/u;->a(ILaN/H;)I

    move-result v3

    if-gt p1, v3, :cond_1

    invoke-virtual {p0, p3, v2}, LaN/u;->b(ILaN/H;)I

    move-result v3

    if-gt p2, v3, :cond_1

    goto :goto_0

    :cond_2
    move-object v2, v1

    goto :goto_1
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public abstract a(LaN/B;LaN/Y;)V
.end method

.method protected abstract a(LaN/B;LaN/Y;LaN/B;Landroid/graphics/Point;)V
.end method

.method protected abstract a(LaN/B;Landroid/graphics/Point;)V
.end method

.method public final a(LaN/Y;)V
    .locals 1

    invoke-virtual {p0}, LaN/u;->q()V

    invoke-virtual {p0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, LaN/u;->e(LaN/B;LaN/Y;)V

    return-void
.end method

.method protected abstract a(LaN/Y;II)V
.end method

.method public a(LaN/v;)V
    .locals 2

    iget-object v1, p0, LaN/u;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/u;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract a(Lo/D;)V
.end method

.method protected a(ZZZ)V
    .locals 6

    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, LaN/u;->a(ZZZII)V

    return-void
.end method

.method protected a(ZZZII)V
    .locals 7

    iget-object v1, p0, LaN/u;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/u;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/v;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, LaN/v;->a(ZZZII)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-void
.end method

.method public abstract a([LaN/B;IIILaN/Y;)V
.end method

.method public declared-synchronized a(LaN/B;LaN/B;I)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/u;->d:Landroid/graphics/Point;

    invoke-virtual {p0, p1, v0}, LaN/u;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, LaN/u;->e:Landroid/graphics/Point;

    invoke-virtual {p0, p2, v0}, LaN/u;->a(LaN/B;Landroid/graphics/Point;)V

    iget-object v0, p0, LaN/u;->d:Landroid/graphics/Point;

    iget-object v1, p0, LaN/u;->e:Landroid/graphics/Point;

    invoke-static {v0, v1, p3}, Lam/r;->a(Landroid/graphics/Point;Landroid/graphics/Point;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)Z
    .locals 2

    iget v0, p0, LaN/u;->a:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, LaN/u;->b:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, LaN/u;->a(ZII)Z

    move-result v0

    return v0
.end method

.method public final a(ZII)Z
    .locals 3

    iget-object v0, p0, LaN/u;->f:LaN/Y;

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    iput-object v0, p0, LaN/u;->f:LaN/Y;

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, LaN/u;->f:LaN/Y;

    invoke-virtual {v0}, LaN/Y;->d()LaN/Y;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LaN/Y;->a()I

    move-result v1

    iget-object v2, p0, LaN/u;->f:LaN/Y;

    invoke-virtual {v2}, LaN/Y;->a()I

    move-result v2

    if-ne v1, v2, :cond_3

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return v0

    :cond_2
    iget-object v0, p0, LaN/u;->f:LaN/Y;

    invoke-virtual {v0}, LaN/Y;->c()LaN/Y;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0, p2, p3}, LaN/u;->a(LaN/Y;II)V

    iput-object v0, p0, LaN/u;->f:LaN/Y;

    const/4 v0, 0x1

    goto :goto_1
.end method

.method public abstract b()I
.end method

.method public b(ILaN/H;)I
    .locals 3

    invoke-virtual {p0, p2}, LaN/u;->b(LaN/H;)I

    move-result v0

    int-to-float v1, p1

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/googlenav/ui/bi;->E()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    int-to-float v2, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public abstract b(LaN/H;)I
.end method

.method public b(II)LaN/B;
    .locals 2

    invoke-virtual {p0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, LaN/u;->d()LaN/Y;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1, p2}, LaN/u;->a(LaN/B;LaN/Y;II)LaN/B;

    move-result-object v0

    return-object v0
.end method

.method public final b(LaN/B;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LaN/u;->b(LaN/B;LaN/Y;)V

    return-void
.end method

.method protected abstract b(LaN/B;LaN/Y;)V
.end method

.method public b(LaN/v;)V
    .locals 2

    iget-object v1, p0, LaN/u;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/u;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public abstract c()LaN/B;
.end method

.method public c(II)V
    .locals 0

    iput p1, p0, LaN/u;->a:I

    iput p2, p0, LaN/u;->b:I

    return-void
.end method

.method public final c(LaN/B;)V
    .locals 1

    invoke-virtual {p0}, LaN/u;->d()LaN/Y;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, LaN/u;->e(LaN/B;LaN/Y;)V

    return-void
.end method

.method protected abstract c(LaN/B;LaN/Y;)V
.end method

.method public abstract d()LaN/Y;
.end method

.method public final d(II)V
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/google/googlenav/actionbar/a;->a()Lcom/google/googlenav/actionbar/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/actionbar/a;->k()I

    move-result v1

    invoke-virtual {p0, p1, p2, v0, v1}, LaN/u;->a(IIII)LaN/Y;

    move-result-object v0

    invoke-virtual {p0, v0}, LaN/u;->a(LaN/Y;)V

    return-void
.end method

.method public final d(LaN/B;LaN/Y;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, LaN/u;->b(LaN/B;LaN/Y;)V

    return-void
.end method

.method public d(LaN/B;)Z
    .locals 3

    invoke-virtual {p0, p1}, LaN/u;->f(LaN/B;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    if-ltz v1, :cond_0

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, LaN/u;->a:I

    if-ge v1, v2, :cond_0

    iget v1, v0, Landroid/graphics/Point;->y:I

    if-ltz v1, :cond_0

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/u;->b:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract e()F
.end method

.method public final e(LaN/B;LaN/Y;)V
    .locals 0

    invoke-virtual {p0, p1, p2}, LaN/u;->c(LaN/B;LaN/Y;)V

    return-void
.end method

.method public e(LaN/B;)Z
    .locals 3

    invoke-virtual {p0, p1}, LaN/u;->f(LaN/B;)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, LaN/u;->a:I

    neg-int v2, v2

    if-lt v1, v2, :cond_0

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p0, LaN/u;->a:I

    mul-int/lit8 v2, v2, 0x2

    if-ge v1, v2, :cond_0

    iget v1, v0, Landroid/graphics/Point;->y:I

    iget v2, p0, LaN/u;->b:I

    neg-int v2, v2

    if-lt v1, v2, :cond_0

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, p0, LaN/u;->b:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract f()LaN/H;
.end method

.method public f(LaN/B;)Landroid/graphics/Point;
    .locals 1

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p0, p1, v0}, LaN/u;->a(LaN/B;Landroid/graphics/Point;)V

    return-object v0
.end method

.method public abstract i()V
.end method

.method public abstract j()Z
.end method

.method public abstract k()Z
.end method

.method public abstract l()Z
.end method

.method protected m()V
    .locals 2

    iget-object v1, p0, LaN/u;->c:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LaN/u;->c:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/common/collect/ImmutableSet;->a(Ljava/util/Collection;)Lcom/google/common/collect/ImmutableSet;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lcom/google/common/collect/ImmutableSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/v;

    invoke-interface {v0}, LaN/v;->i()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-void
.end method

.method public n()I
    .locals 1

    iget v0, p0, LaN/u;->a:I

    return v0
.end method

.method public o()I
    .locals 1

    iget v0, p0, LaN/u;->b:I

    return v0
.end method

.method public p()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    invoke-virtual {p0}, LaN/u;->c()LaN/B;

    move-result-object v0

    invoke-virtual {p0}, LaN/u;->a()I

    move-result v1

    invoke-virtual {p0}, LaN/u;->b()I

    move-result v2

    invoke-virtual {p0}, LaN/u;->d()LaN/Y;

    move-result-object v3

    invoke-virtual {v3}, LaN/Y;->a()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, LaN/C;->a(LaN/B;III)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    return-object v0
.end method

.method public final q()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, LaN/u;->f:LaN/Y;

    return-void
.end method
