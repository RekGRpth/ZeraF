.class public final LaN/N;
.super LaN/M;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:[[LaN/B;

.field private final c:LaN/B;


# direct methods
.method public constructor <init>([LaN/B;III[[LaN/B;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, LaN/M;-><init>([LaN/B;II)V

    iput p4, p0, LaN/N;->a:I

    iput-object p5, p0, LaN/N;->b:[[LaN/B;

    invoke-direct {p0}, LaN/N;->p()LaN/B;

    move-result-object v0

    iput-object v0, p0, LaN/N;->c:LaN/B;

    return-void
.end method

.method public static b(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)LaN/N;
    .locals 9

    const/16 v8, 0xc

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v2

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v3

    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getInt(I)I

    move-result v4

    const/4 v0, -0x1

    if-ne v2, v0, :cond_0

    move v2, v4

    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(I)[B

    move-result-object v0

    invoke-static {v0}, LaN/N;->a([B)[LaN/B;

    move-result-object v1

    const/4 v0, 0x0

    check-cast v0, [[LaN/B;

    invoke-virtual {p0, v8}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v6

    if-lez v6, :cond_1

    new-array v5, v6, [[LaN/B;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_2

    invoke-virtual {p0, v8, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getBytes(II)[B

    move-result-object v7

    invoke-static {v7}, LaN/M;->a([B)[LaN/B;

    move-result-object v7

    aput-object v7, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object v5, v0

    :cond_2
    new-instance v0, LaN/N;

    invoke-direct/range {v0 .. v5}, LaN/N;-><init>([LaN/B;III[[LaN/B;)V

    return-object v0
.end method

.method public static b([JLaN/B;LaN/Y;)Z
    .locals 12

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-nez p0, :cond_0

    :goto_0
    return v3

    :cond_0
    invoke-virtual {p1, p2}, LaN/B;->a(LaN/Y;)I

    move-result v5

    invoke-virtual {p1, p2}, LaN/B;->b(LaN/Y;)I

    move-result v6

    move v0, v1

    move v2, v3

    move v4, v3

    :goto_1
    array-length v7, p0

    add-int/lit8 v7, v7, -0x1

    if-ge v2, v7, :cond_5

    aget-wide v7, p0, v2

    invoke-static {v7, v8}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v7

    aget-wide v8, p0, v2

    invoke-static {v8, v9}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v8

    aget-wide v9, p0, v0

    invoke-static {v9, v10}, Lcom/google/googlenav/ui/aR;->a(J)I

    move-result v9

    aget-wide v10, p0, v0

    invoke-static {v10, v11}, Lcom/google/googlenav/ui/aR;->b(J)I

    move-result v10

    if-gt v8, v6, :cond_1

    if-lt v6, v10, :cond_2

    :cond_1
    if-gt v10, v6, :cond_3

    if-ge v6, v8, :cond_3

    :cond_2
    sub-int/2addr v9, v7

    sub-int v11, v6, v8

    mul-int/2addr v9, v11

    sub-int v8, v10, v8

    div-int v8, v9, v8

    add-int/2addr v7, v8

    if-ge v5, v7, :cond_3

    if-nez v4, :cond_4

    move v4, v1

    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v4, v3

    goto :goto_2

    :cond_5
    move v3, v4

    goto :goto_0
.end method

.method private p()LaN/B;
    .locals 9

    const-wide/16 v1, 0x0

    invoke-virtual {p0}, LaN/N;->j()[LaN/B;

    move-result-object v5

    array-length v0, v5

    add-int/lit8 v6, v0, -0x1

    const/4 v0, 0x0

    move-wide v3, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    invoke-virtual {v7}, LaN/B;->c()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v3, v7

    aget-object v7, v5, v0

    invoke-virtual {v7}, LaN/B;->e()I

    move-result v7

    int-to-long v7, v7

    add-long/2addr v1, v7

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    int-to-long v7, v6

    div-long/2addr v3, v7

    int-to-long v5, v6

    div-long v0, v1, v5

    new-instance v2, LaN/B;

    long-to-int v3, v3

    long-to-int v0, v0

    invoke-direct {v2, v3, v0}, LaN/B;-><init>(II)V

    return-object v2
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public b()LaN/B;
    .locals 1

    iget-object v0, p0, LaN/N;->c:LaN/B;

    return-object v0
.end method

.method public e()Lcom/google/googlenav/common/io/protocol/ProtoBuf;
    .locals 4

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    const/16 v0, 0x9

    invoke-virtual {p0}, LaN/N;->k()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0x8

    invoke-virtual {p0}, LaN/N;->f()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xb

    iget v2, p0, LaN/N;->a:I

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setInt(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    const/16 v0, 0xa

    invoke-virtual {p0}, LaN/N;->j()[LaN/B;

    move-result-object v2

    invoke-static {v2}, LaN/N;->a([LaN/B;)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->setBytes(I[B)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    iget-object v0, p0, LaN/N;->b:[[LaN/B;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, LaN/N;->b:[[LaN/B;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    const/16 v2, 0xc

    iget-object v3, p0, LaN/N;->b:[[LaN/B;

    aget-object v3, v3, v0

    invoke-static {v3}, LaN/N;->a([LaN/B;)[B

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->addBytes(I[B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public l()Z
    .locals 2

    iget v0, p0, LaN/N;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()I
    .locals 1

    iget v0, p0, LaN/N;->a:I

    return v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    const-string v0, "polygon"

    return-object v0
.end method

.method public o()[[LaN/B;
    .locals 1

    iget-object v0, p0, LaN/N;->b:[[LaN/B;

    return-object v0
.end method
