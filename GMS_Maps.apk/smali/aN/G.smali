.class LaN/G;
.super LaN/d;
.source "SourceFile"


# instance fields
.field final synthetic b:LaN/D;

.field private c:Ljava/util/Vector;

.field private d:Ljava/util/Vector;

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>(LaN/D;B)V
    .locals 1

    iput-object p1, p0, LaN/G;->b:LaN/D;

    invoke-static {p1}, LaN/D;->b(LaN/D;)I

    move-result v0

    invoke-direct {p0, v0, p2}, LaN/d;-><init>(IB)V

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, LaN/G;->d:Ljava/util/Vector;

    const/4 v0, 0x1

    iput-boolean v0, p0, LaN/G;->f:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/G;->e:Z

    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 2

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iget-object v1, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-static {v1, p1, v0}, Lcom/google/googlenav/common/util/a;->a(Ljava/util/List;ILjava/util/List;)V

    iput-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    iget-object v0, p0, LaN/G;->b:LaN/D;

    invoke-virtual {v0}, LaN/D;->b()V

    return-void
.end method

.method protected a(II)V
    .locals 1

    iget-object v0, p0, LaN/G;->b:LaN/D;

    invoke-virtual {v0, p1, p2}, LaN/D;->a(II)V

    return-void
.end method

.method declared-synchronized a(LaN/I;I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaN/G;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Adding tiles to closed request!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->indexOf(Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_2
    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    move v1, v0

    :goto_1
    if-lez v1, :cond_3

    iget-object v0, p0, LaN/G;->d:Ljava/util/Vector;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p2, v0, :cond_4

    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    iget-object v0, p0, LaN/G;->d:Ljava/util/Vector;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    :cond_3
    if-nez v1, :cond_1

    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    iget-object v0, p0, LaN/G;->d:Ljava/util/Vector;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 3

    iget-object v0, p0, LaN/G;->b:LaN/D;

    invoke-static {v0}, LaN/D;->c(LaN/D;)I

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, LaN/G;->e:Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    iput-object v0, p0, LaN/G;->d:Ljava/util/Vector;

    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    new-array v2, v0, [LaN/P;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    invoke-virtual {v0}, LaN/I;->c()LaN/P;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    invoke-virtual {p0, v2, p1}, LaN/G;->a([LaN/P;Ljava/io/DataOutput;)V

    return-void
.end method

.method protected a(ILaN/P;[B)Z
    .locals 5

    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaN/I;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, LaN/I;->c()LaN/P;

    move-result-object v1

    invoke-virtual {v1, p2}, LaN/P;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, p3}, LaN/I;->a([B)V

    invoke-virtual {v0}, LaN/I;->g()J

    move-result-wide v1

    int-to-long v3, p1

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, LaN/I;->a(J)V

    iget-object v0, p0, LaN/G;->b:LaN/D;

    invoke-static {v0}, LaN/D;->e(LaN/D;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lat/d;

    invoke-interface {v0}, Lat/d;->q()V

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/io/DataInput;)Z
    .locals 1

    iget-object v0, p0, LaN/G;->b:LaN/D;

    invoke-static {v0}, LaN/D;->d(LaN/D;)I

    invoke-super {p0, p1}, LaN/d;->a(Ljava/io/DataInput;)Z

    iget-object v0, p0, LaN/G;->c:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t_()Z
    .locals 1

    iget-boolean v0, p0, LaN/G;->f:Z

    return v0
.end method
