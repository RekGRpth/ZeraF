.class public LaN/I;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;

.field private static b:I

.field private static final c:[B

.field private static o:Lam/f;

.field private static p:Lam/f;

.field private static q:Lam/f;


# instance fields
.field private d:[B

.field private e:Lam/f;

.field private f:Z

.field private g:Lam/f;

.field private h:J

.field private i:J

.field private j:J

.field private k:I

.field private final l:LaN/P;

.field private final m:Z

.field private n:Z

.field private r:LaN/n;

.field private s:[Ljava/lang/String;

.field private t:[Ljava/lang/String;

.field private u:I

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, LaN/I;->a:Ljava/util/Map;

    const/4 v0, 0x1

    sput v0, LaN/I;->b:I

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, LaN/I;->c:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x43t
        0x4at
        0x50t
        0x47t
    .end array-data
.end method

.method public constructor <init>(LaN/P;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LaN/I;-><init>(LaN/P;Lam/f;Z)V

    return-void
.end method

.method public constructor <init>(LaN/P;Lam/f;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LaN/I;-><init>(LaN/P;Lam/f;Z)V

    return-void
.end method

.method public constructor <init>(LaN/P;Lam/f;Z)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LaN/I;->u:I

    iput-boolean v1, p0, LaN/I;->w:Z

    iput-boolean v1, p0, LaN/I;->x:Z

    iput-boolean v1, p0, LaN/I;->y:Z

    invoke-static {p2}, LaN/I;->a(Lam/f;)V

    iput-object p1, p0, LaN/I;->l:LaN/P;

    iput-boolean p3, p0, LaN/I;->m:Z

    invoke-direct {p0, p2, v1}, LaN/I;->a(Lam/f;Z)V

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LaN/I;->f:Z

    const-wide/16 v2, 0x0

    iput-wide v2, p0, LaN/I;->h:J

    iput v1, p0, LaN/I;->v:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>(LaN/P;[B)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, LaN/I;->u:I

    iput-boolean v2, p0, LaN/I;->w:Z

    iput-boolean v2, p0, LaN/I;->x:Z

    iput-boolean v2, p0, LaN/I;->y:Z

    iput-object p1, p0, LaN/I;->l:LaN/P;

    iput-boolean v2, p0, LaN/I;->m:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LaN/I;->h:J

    iput-boolean v2, p0, LaN/I;->f:Z

    invoke-virtual {p0, p2}, LaN/I;->a([B)V

    return-void
.end method

.method private static A()Lam/f;
    .locals 3

    const/16 v2, 0x100

    sget-object v0, LaN/I;->q:Lam/f;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const v1, 0x7f0202d5

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->b(I)Lam/f;

    move-result-object v0

    sput-object v0, LaN/I;->q:Lam/f;

    sget-object v0, LaN/I;->q:Lam/f;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    invoke-interface {v0, v2, v2}, Lam/h;->a(II)Lam/f;

    move-result-object v0

    sput-object v0, LaN/I;->q:Lam/f;

    :cond_0
    sget-object v0, LaN/I;->q:Lam/f;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)LaN/I;
    .locals 3

    invoke-static {p0}, LaN/P;->a(Ljava/io/DataInput;)LaN/P;

    move-result-object v0

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v1

    new-array v1, v1, [B

    invoke-interface {p0, v1}, Ljava/io/DataInput;->readFully([B)V

    new-instance v2, LaN/I;

    invoke-direct {v2, v0, v1}, LaN/I;-><init>(LaN/P;[B)V

    return-object v2
.end method

.method private a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)LaN/n;
    .locals 6

    const/4 v5, 0x3

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1, v5}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    new-array v2, v1, [Lcom/google/googlenav/layer/j;

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    new-instance v3, Lcom/google/googlenav/layer/j;

    invoke-virtual {p1, v5, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(II)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/googlenav/layer/j;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, LaN/n;

    iget-object v1, p0, LaN/I;->l:LaN/P;

    invoke-direct {v0, v1}, LaN/n;-><init>(LaN/P;)V

    invoke-virtual {v0, v2, p2, p3}, LaN/n;->a([Lcom/google/googlenav/layer/j;J)V

    goto :goto_0
.end method

.method private static a(Lam/f;)V
    .locals 3

    const/16 v1, 0x100

    if-eqz p0, :cond_1

    invoke-interface {p0}, Lam/f;->b()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lam/f;->a()I

    move-result v0

    if-eq v0, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wrong image size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lam/f;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Lam/f;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method private declared-synchronized a(Lam/f;Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/I;->e:Lam/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->e:Lam/f;

    invoke-interface {v0}, Lam/f;->f()V

    iget-boolean v0, p0, LaN/I;->x:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->e:Lam/f;

    invoke-interface {v0}, Lam/f;->d()V

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LaN/I;->e:Lam/f;

    iput-boolean p2, p0, LaN/I;->x:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-interface {p1}, Lam/f;->e()Lam/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b([B)Lam/f;
    .locals 6

    const/16 v5, 0x100

    const/4 v4, 0x0

    array-length v0, p0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    invoke-static {}, LaN/I;->A()Lam/f;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    aget-byte v0, p0, v4

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v0, v0, 0x10

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int v1, v0, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v0, LaN/I;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lam/f;

    if-nez v0, :cond_0

    :cond_2
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    invoke-interface {v0, v5, v5, v4}, Lam/h;->a(IIZ)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->c()Lam/e;

    move-result-object v3

    invoke-interface {v3, v1}, Lam/e;->a(I)V

    invoke-interface {v3, v4, v4, v5, v5}, Lam/e;->b(IIII)V

    sget-object v1, LaN/I;->a:Ljava/util/Map;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static b(I)V
    .locals 0

    sput p0, LaN/I;->b:I

    return-void
.end method

.method private declared-synchronized b(Lam/f;Z)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/I;->g:Lam/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->g:Lam/f;

    invoke-interface {v0}, Lam/f;->f()V

    iget-boolean v0, p0, LaN/I;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->g:Lam/f;

    invoke-interface {v0}, Lam/f;->d()V

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LaN/I;->g:Lam/f;

    iput-boolean p2, p0, LaN/I;->y:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-interface {p1}, Lam/f;->e()Lam/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(J)Lam/f;
    .locals 3

    const/4 v0, 0x0

    monitor-enter p0

    const-wide/high16 v1, -0x8000000000000000L

    cmp-long v1, p1, v1

    if-nez v1, :cond_2

    :try_start_0
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->v()Lcom/google/googlenav/common/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/googlenav/common/a;->b()J

    move-result-wide v1

    iput-wide v1, p0, LaN/I;->h:J

    :goto_0
    iget-object v1, p0, LaN/I;->e:Lam/f;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LaN/I;->f:Z

    if-eqz v1, :cond_4

    :cond_0
    invoke-virtual {p0}, LaN/I;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    :try_start_1
    invoke-virtual {p0, p1, p2}, LaN/I;->b(J)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v0

    :goto_1
    :try_start_2
    iget-object v0, p0, LaN/I;->e:Lam/f;

    if-nez v0, :cond_3

    invoke-direct {p0}, LaN/I;->y()Lam/f;

    move-result-object v0

    :goto_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/googlenav/common/k;->b()V

    :cond_1
    return-object v0

    :cond_2
    :try_start_3
    iput-wide p1, p0, LaN/I;->h:J

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    :try_start_4
    iget-object v0, p0, LaN/I;->e:Lam/f;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method private static c([B)Lam/f;
    .locals 4

    :try_start_0
    invoke-static {p0}, Lao/c;->a([B)[B

    move-result-object v0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, v0

    invoke-interface {v1, v0, v2, v3}, Lam/h;->a([BII)Lam/f;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "MAP"

    invoke-static {v1, v0}, Lbm/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, LaN/I;->A()Lam/f;

    move-result-object v0

    goto :goto_0
.end method

.method public static w()I
    .locals 1

    sget v0, LaN/I;->b:I

    return v0
.end method

.method private y()Lam/f;
    .locals 1

    sget-object v0, LaN/I;->p:Lam/f;

    if-eqz v0, :cond_0

    sget-object v0, LaN/I;->o:Lam/f;

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, LaN/I;->z()V

    :cond_1
    iget-boolean v0, p0, LaN/I;->m:Z

    if-eqz v0, :cond_2

    sget-object v0, LaN/I;->p:Lam/f;

    :goto_0
    return-object v0

    :cond_2
    sget-object v0, LaN/I;->o:Lam/f;

    goto :goto_0
.end method

.method private static z()V
    .locals 3

    const/16 v2, 0x100

    invoke-static {}, Lcom/google/googlenav/ui/bi;->d()Lcom/google/googlenav/ui/bi;

    move-result-object v0

    const v1, 0x7f0202a6

    invoke-virtual {v0, v1}, Lcom/google/googlenav/ui/bi;->b(I)Lam/f;

    move-result-object v0

    sput-object v0, LaN/I;->o:Lam/f;

    sget-object v0, LaN/I;->o:Lam/f;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v2, v2, v1}, Lam/h;->a(IIZ)Lam/f;

    move-result-object v0

    sput-object v0, LaN/I;->o:Lam/f;

    :cond_0
    sget-object v0, LaN/I;->o:Lam/f;

    sput-object v0, LaN/I;->p:Lam/f;

    return-void
.end method


# virtual methods
.method public declared-synchronized a(IIIIII)Lam/f;
    .locals 7

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LaN/I;->f()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    :try_start_1
    invoke-direct {p0, v0, v1}, LaN/I;->c(J)Lam/f;

    move-result-object v0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lam/f;->a(IIIIII)Lam/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LaN/I;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/I;->f:Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/I;->a(Lam/f;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, LaN/I;->e:Lam/f;

    iget-boolean v1, p0, LaN/I;->x:Z

    invoke-direct {p0, v0, v1}, LaN/I;->b(Lam/f;Z)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/I;->x:Z

    :cond_0
    iput p1, p0, LaN/I;->v:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(J)V
    .locals 0

    iput-wide p1, p0, LaN/I;->h:J

    return-void
.end method

.method public a(JJ)V
    .locals 6

    const-wide/16 v4, 0x0

    iget v0, p0, LaN/I;->k:I

    if-nez v0, :cond_0

    iget-wide v0, p0, LaN/I;->j:J

    cmp-long v0, v0, p3

    if-eqz v0, :cond_0

    iget-wide v0, p0, LaN/I;->j:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iput-wide v4, p0, LaN/I;->i:J

    :cond_0
    iget-wide v0, p0, LaN/I;->i:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    iput-wide p1, p0, LaN/I;->i:J

    :cond_1
    iput-wide p1, p0, LaN/I;->j:J

    invoke-virtual {p0}, LaN/I;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LaN/I;->f:Z

    if-nez v0, :cond_2

    iget v0, p0, LaN/I;->k:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_2

    iget v0, p0, LaN/I;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LaN/I;->k:I

    :cond_2
    return-void
.end method

.method public declared-synchronized a(Lam/f;IZ)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LaN/I;->a(Lam/f;)V

    invoke-direct {p0, p1, p3}, LaN/I;->a(Lam/f;Z)V

    invoke-virtual {p0, p2}, LaN/I;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/io/DataOutput;)V
    .locals 1

    iget-object v0, p0, LaN/I;->l:LaN/P;

    invoke-virtual {v0, p1}, LaN/P;->a(Ljava/io/DataOutput;)V

    iget-object v0, p0, LaN/I;->d:[B

    array-length v0, v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeShort(I)V

    iget-object v0, p0, LaN/I;->d:[B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, LaN/I;->w:Z

    return-void
.end method

.method public declared-synchronized a([B)V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LaN/I;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tile already complete"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, LaN/I;->v:I

    iput-object p1, p0, LaN/I;->d:[B

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/I;->b(Lam/f;Z)V

    iget-boolean v0, p0, LaN/I;->f:Z

    if-nez v0, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/I;->a(Lam/f;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lam/e;JIIZ)Z
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LaN/I;->f()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p6, :cond_1

    :cond_0
    invoke-direct {p0, p2, p3}, LaN/I;->c(J)Lam/f;

    move-result-object v0

    invoke-interface {p1, v0, p4, p5}, Lam/e;->a(Lam/f;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized b(J)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, LaN/I;->e:Lam/f;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, LaN/I;->f:Z

    if-eqz v2, :cond_2

    :cond_0
    new-instance v2, LaN/J;

    invoke-direct {v2}, LaN/J;-><init>()V

    iget-object v3, p0, LaN/I;->d:[B

    invoke-virtual {v2, v3}, LaN/J;->a([B)[B

    move-result-object v3

    invoke-virtual {v2}, LaN/J;->a()Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v4

    invoke-direct {p0, v4, p1, p2}, LaN/I;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;J)LaN/n;

    move-result-object v4

    iput-object v4, p0, LaN/I;->r:LaN/n;

    invoke-static {}, Lcom/google/googlenav/K;->a()Lcom/google/googlenav/K;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/googlenav/K;->ao()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, LaN/J;->b()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LaN/I;->s:[Ljava/lang/String;

    invoke-virtual {v2}, LaN/J;->c()[Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LaN/I;->t:[Ljava/lang/String;

    invoke-virtual {v2}, LaN/J;->d()I

    move-result v2

    iput v2, p0, LaN/I;->u:I

    :cond_1
    array-length v2, v3

    if-nez v2, :cond_3

    invoke-static {}, LaN/I;->A()Lam/f;

    move-result-object v0

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, LaN/I;->a(Lam/f;IZ)V

    const/4 v0, 0x0

    iput-boolean v0, p0, LaN/I;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    :try_start_1
    array-length v2, v3

    const/4 v4, 0x3

    if-ne v2, v4, :cond_4

    invoke-static {v3}, LaN/I;->b([B)Lam/f;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    sget-object v4, LaN/I;->c:[B

    invoke-static {v3, v2, v4}, LaN/J;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v3}, LaN/I;->c([B)Lam/f;

    move-result-object v2

    sget-object v3, LaN/I;->q:Lam/f;

    if-eq v2, v3, :cond_5

    :goto_1
    move v1, v0

    move-object v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    invoke-static {}, Lcom/google/googlenav/common/Config;->a()Lcom/google/googlenav/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/googlenav/common/Config;->o()Lam/h;

    move-result-object v1

    const/4 v2, 0x0

    array-length v4, v3

    invoke-interface {v1, v3, v2, v4}, Lam/h;->a([BII)Lam/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    move v5, v0

    move-object v0, v1

    move v1, v5

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()[B
    .locals 1

    iget-object v0, p0, LaN/I;->d:[B

    return-object v0
.end method

.method public c()LaN/P;
    .locals 1

    iget-object v0, p0, LaN/I;->l:LaN/P;

    return-object v0
.end method

.method public declared-synchronized d()V
    .locals 2

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, LaN/I;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LaN/I;->v()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, LaN/I;->e:Lam/f;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/I;->a(Lam/f;Z)V

    :cond_1
    iget-object v0, p0, LaN/I;->g:Lam/f;

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/I;->b(Lam/f;Z)V

    :cond_2
    const/4 v0, 0x0

    iput v0, p0, LaN/I;->v:I

    const/4 v0, 0x0

    iput-object v0, p0, LaN/I;->r:LaN/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, LaN/I;->d:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, LaN/I;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, LaN/I;

    iget-object v2, p0, LaN/I;->l:LaN/P;

    if-nez v2, :cond_3

    iget-object v2, p1, LaN/I;->l:LaN/P;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, LaN/I;->l:LaN/P;

    iget-object v1, p1, LaN/I;->l:LaN/P;

    invoke-virtual {v0, v1}, LaN/P;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, LaN/I;->e:Lam/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, LaN/I;->h:J

    return-wide v0
.end method

.method public h()J
    .locals 2

    iget-wide v0, p0, LaN/I;->i:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, LaN/I;->l:LaN/P;

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->l:LaN/P;

    invoke-virtual {v0}, LaN/P;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 1

    iget v0, p0, LaN/I;->k:I

    return v0
.end method

.method public j()I
    .locals 1

    iget-object v0, p0, LaN/I;->d:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->d:[B

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized k()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LaN/I;->g:Lam/f;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LaN/I;->a(Lam/f;Z)V

    const/4 v0, 0x0

    iput v0, p0, LaN/I;->v:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, LaN/I;->v:I

    return v0
.end method

.method public m()Z
    .locals 1

    iget-boolean v0, p0, LaN/I;->f:Z

    return v0
.end method

.method public declared-synchronized n()Lam/f;
    .locals 2

    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    :try_start_0
    invoke-direct {p0, v0, v1}, LaN/I;->c(J)Lam/f;

    move-result-object v0

    invoke-interface {v0}, Lam/f;->e()Lam/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public o()V
    .locals 2

    const-wide/high16 v0, -0x8000000000000000L

    invoke-direct {p0, v0, v1}, LaN/I;->c(J)Lam/f;

    return-void
.end method

.method public p()LaN/n;
    .locals 1

    iget-object v0, p0, LaN/I;->r:LaN/n;

    return-object v0
.end method

.method public q()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaN/I;->s:[Ljava/lang/String;

    return-object v0
.end method

.method public r()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LaN/I;->t:[Ljava/lang/String;

    return-object v0
.end method

.method public s()I
    .locals 1

    iget v0, p0, LaN/I;->u:I

    return v0
.end method

.method public t()Z
    .locals 2

    iget v0, p0, LaN/I;->v:I

    if-eqz v0, :cond_0

    iget-object v0, p0, LaN/I;->g:Lam/f;

    iget-object v1, p0, LaN/I;->e:Lam/f;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaN/I;->l:LaN/P;

    invoke-virtual {v1}, LaN/P;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LaN/I;->d:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "B"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LaN/I;->d:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "B?"

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    iget-boolean v0, p0, LaN/I;->n:Z

    return v0
.end method

.method public v()Z
    .locals 1

    iget-boolean v0, p0, LaN/I;->w:Z

    return v0
.end method

.method public declared-synchronized x()I
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iget-boolean v1, p0, LaN/I;->y:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LaN/I;->g:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I

    move-result v1

    add-int/2addr v0, v1

    :cond_0
    iget-boolean v1, p0, LaN/I;->x:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LaN/I;->e:Lam/f;

    invoke-interface {v1}, Lam/f;->g()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
