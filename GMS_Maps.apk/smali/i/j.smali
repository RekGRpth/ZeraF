.class public Li/j;
.super Li/a;
.source "SourceFile"


# static fields
.field private static final e:Li/e;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Li/k;

    const/16 v1, 0x64

    const-string v2, "ShortChunkArrayManager"

    invoke-direct {v0, v1, v2}, Li/k;-><init>(ILjava/lang/String;)V

    sput-object v0, Li/j;->e:Li/e;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    const/16 v0, 0xb

    sget-object v1, Li/j;->e:Li/e;

    invoke-direct {p0, p1, v0, v1}, Li/a;-><init>(IILi/e;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/nio/ShortBuffer;)V
    .locals 4

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget v0, p0, Li/j;->b:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Li/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [S

    const/16 v3, 0x800

    invoke-virtual {p1, v0, v2, v3}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v0, p0, Li/j;->b:I

    iget-object v1, p0, Li/j;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Li/j;->c:Ljava/lang/Object;

    check-cast v0, [S

    iget v1, p0, Li/j;->d:I

    invoke-virtual {p1, v0, v2, v1}, Ljava/nio/ShortBuffer;->put([SII)Ljava/nio/ShortBuffer;

    :cond_1
    return-void
.end method
