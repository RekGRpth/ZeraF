.class public abstract Lbm/j;
.super Las/d;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/googlenav/android/aa;

.field private final b:Z

.field private f:Ljava/lang/Runnable;

.field private final g:Ljava/util/concurrent/locks/Lock;

.field private h:Z


# direct methods
.method public constructor <init>(Las/c;Lcom/google/googlenav/android/aa;Z)V
    .locals 1

    invoke-direct {p0, p1}, Las/d;-><init>(Las/c;)V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lbm/j;->g:Ljava/util/concurrent/locks/Lock;

    iput-object p2, p0, Lbm/j;->a:Lcom/google/googlenav/android/aa;

    iput-boolean p3, p0, Lbm/j;->b:Z

    invoke-direct {p0}, Lbm/j;->m()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lbm/j;->f:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lbm/j;)Ljava/util/concurrent/locks/Lock;
    .locals 1

    iget-object v0, p0, Lbm/j;->g:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic a(Lbm/j;Z)Z
    .locals 0

    iput-boolean p1, p0, Lbm/j;->h:Z

    return p1
.end method

.method static synthetic b(Lbm/j;)Z
    .locals 1

    iget-boolean v0, p0, Lbm/j;->h:Z

    return v0
.end method

.method private m()Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lbm/k;

    invoke-direct {v0, p0}, Lbm/k;-><init>(Lbm/j;)V

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 3

    iget-object v0, p0, Lbm/j;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-boolean v0, p0, Lbm/j;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbm/j;->h:Z

    iget-object v0, p0, Lbm/j;->a:Lcom/google/googlenav/android/aa;

    iget-object v1, p0, Lbm/j;->f:Ljava/lang/Runnable;

    iget-boolean v2, p0, Lbm/j;->b:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/googlenav/android/aa;->a(Ljava/lang/Runnable;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    iget-object v0, p0, Lbm/j;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbm/j;->g:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method protected abstract b()V
.end method
