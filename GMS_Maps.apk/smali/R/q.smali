.class LR/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Law/q;


# instance fields
.field private final a:Law/h;

.field private final b:Ljava/lang/Runnable;

.field private final c:Z


# direct methods
.method public constructor <init>(Law/h;Ljava/lang/Runnable;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, LR/q;->a:Law/h;

    iput-object p2, p0, LR/q;->b:Ljava/lang/Runnable;

    invoke-virtual {p1}, Law/h;->f()Z

    move-result v0

    iput-boolean v0, p0, LR/q;->c:Z

    iget-boolean v0, p0, LR/q;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Law/h;->h()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(IZLjava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    if-eqz p2, :cond_2

    :goto_0
    if-nez v0, :cond_1

    invoke-static {}, LR/o;->g()Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, LR/q;->a:Law/h;

    invoke-virtual {v0, p0}, Law/h;->b(Law/q;)V

    iget-object v0, p0, LR/q;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, LR/q;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    const-class v1, LR/o;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, LR/o;->a(Z)Z

    const-class v0, LR/o;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Law/g;)V
    .locals 1

    instance-of v0, p1, LR/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, LR/q;->a:Law/h;

    invoke-virtual {v0, p0}, Law/h;->b(Law/q;)V

    iget-boolean v0, p0, LR/q;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LR/q;->a:Law/h;

    invoke-virtual {v0}, Law/h;->g()V

    :cond_0
    return-void
.end method

.method public b(Law/g;)V
    .locals 0

    return-void
.end method

.method public k()V
    .locals 0

    return-void
.end method
