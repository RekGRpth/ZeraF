.class public final LR/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Z

.field private static volatile b:Z

.field private static volatile c:Z

.field private static volatile d:LR/m;

.field private static volatile e:Lcom/google/android/maps/driveabout/vector/bF;

.field private static volatile f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, LR/m;

    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    invoke-direct {v0, v1}, LR/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v0, LR/o;->d:LR/m;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()LR/m;
    .locals 2

    const-class v0, LR/o;

    monitor-enter v0

    :try_start_0
    sget-object v1, LR/o;->d:LR/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static synthetic a(LR/m;)LR/m;
    .locals 0

    sput-object p0, LR/o;->d:LR/m;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/maps/driveabout/vector/bF;)Lcom/google/android/maps/driveabout/vector/bF;
    .locals 0

    sput-object p0, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;

    return-object p0
.end method

.method static synthetic a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1, p2, p3, p4}, LR/o;->b(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V
    .locals 6

    sget-boolean v0, LR/o;->a:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, LR/o;->a:Z

    new-instance v0, LR/p;

    const-string v1, "ParameterManagerLoad"

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LR/p;-><init>(Ljava/lang/String;Landroid/content/Context;Law/h;Ljava/lang/Runnable;Z)V

    invoke-virtual {v0}, LR/p;->start()V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0

    sput-boolean p0, LR/o;->f:Z

    return p0
.end method

.method public static declared-synchronized b()LR/m;
    .locals 2

    const-class v1, LR/o;

    monitor-enter v1

    :goto_0
    :try_start_0
    sget-boolean v0, LR/o;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, LR/o;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    const-class v0, LR/o;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    sget-boolean v0, LR/o;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, LR/o;->d:LR/m;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    monitor-exit v1

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Landroid/content/Context;Law/h;Ljava/lang/Runnable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const/4 v4, 0x0

    const-class v7, LR/o;

    monitor-enter v7

    :try_start_0
    const-string v0, "ParameterManager.initializeInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p0, p3, v0}, LJ/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    if-eqz v1, :cond_5

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    const/4 v2, 0x1

    sput-boolean v2, LR/o;->c:Z

    :goto_0
    new-instance v2, LR/m;

    invoke-direct {v2, v0}, LR/m;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)V

    sput-object v2, LR/o;->d:LR/m;

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :goto_1
    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-static {p0, p4, v0}, LJ/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/googlenav/common/io/protocol/ProtoBufType;)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getProtoBuf(I)Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/maps/driveabout/vector/bF;->a(Lcom/google/googlenav/common/io/protocol/ProtoBuf;)Lcom/google/android/maps/driveabout/vector/bF;

    move-result-object v1

    sput-object v1, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    :cond_0
    const-class v0, LR/o;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_1
    if-eqz p1, :cond_4

    sget-object v0, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;

    if-nez v0, :cond_3

    invoke-virtual {p1}, Law/h;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    :cond_2
    const/4 v0, 0x1

    sput-boolean v0, LR/o;->f:Z

    const-class v0, LR/o;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_3
    new-instance v0, LR/q;

    invoke-direct {v0, p1, p2}, LR/q;-><init>(Law/h;Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Law/h;->a(Law/q;)V

    new-instance v0, LR/r;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, LR/r;-><init>(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;LR/p;)V

    invoke-virtual {p1, v0}, Law/h;->c(Law/g;)V

    :cond_4
    const-string v0, "ParameterManager.initializeInternal"

    invoke-static {v0}, Lcom/google/googlenav/common/util/o;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v7

    return-void

    :cond_5
    :try_start_1
    new-instance v1, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v0, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->d:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v1, v0}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V

    new-instance v0, Lcom/google/googlenav/common/io/protocol/ProtoBuf;

    sget-object v2, Lcom/google/wireless/googlenav/proto/j2me/ClientParameters;->i:Lcom/google/googlenav/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v2}, Lcom/google/googlenav/common/io/protocol/ProtoBuf;-><init>(Lcom/google/googlenav/common/io/protocol/ProtoBufType;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    :cond_6
    move-object v2, v4

    goto :goto_1
.end method

.method static synthetic b(Z)Z
    .locals 0

    sput-boolean p0, LR/o;->b:Z

    return p0
.end method

.method public static c()Z
    .locals 1

    sget-boolean v0, LR/o;->b:Z

    if-nez v0, :cond_0

    sget-boolean v0, LR/o;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized d()Lcom/google/android/maps/driveabout/vector/bF;
    .locals 2

    const-class v1, LR/o;

    monitor-enter v1

    :goto_0
    :try_start_0
    sget-object v0, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;

    if-nez v0, :cond_0

    sget-boolean v0, LR/o;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    const-class v0, LR/o;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    sget-object v0, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static e()Z
    .locals 1

    sget-object v0, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, LR/o;->f:Z

    sput-boolean v0, LR/o;->a:Z

    sput-boolean v0, LR/o;->b:Z

    sput-boolean v0, LR/o;->c:Z

    return-void
.end method

.method static synthetic g()Lcom/google/android/maps/driveabout/vector/bF;
    .locals 1

    sget-object v0, LR/o;->e:Lcom/google/android/maps/driveabout/vector/bF;

    return-object v0
.end method
